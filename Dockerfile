#For Gradle
FROM gradle:7.2.0-jdk8 AS build

WORKDIR /usr/local/pool

# Only copy dependency-related files
#COPY build.gradle gradle.properties settings.gradle /usr/local/pool/

# Only download dependencies
# Eat the expected build failure since no source code has been copied yet
RUN gradle clean build --no-daemon > /dev/null 2>&1 || true

COPY . /usr/local/pool

RUN gradle clean build -x test --no-daemon

# For Java 8,
FROM  openjdk:8u302-jre-slim-buster

ARG JAR_FILE=pool-0.1.jar

RUN mkdir /pool

WORKDIR /pool

COPY --from=build /usr/local/pool/build/libs/${JAR_FILE} /pool/app.jar

ENTRYPOINT ["java","-jar","app.jar"]