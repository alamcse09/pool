package com.dgt.paribahanpool;

import com.dgt.paribahanpool.config.AppProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.datatables.repository.DataTablesRepositoryFactoryBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableCaching
@EnableScheduling
@SpringBootApplication
@EnableAspectJAutoProxy
@EnableConfigurationProperties( value = { AppProperties.class } )
@EnableJpaRepositories( repositoryFactoryBeanClass = DataTablesRepositoryFactoryBean.class, basePackages = "com.dgt.paribahanpool" )
public class ParibahanPoolApplication {

    public static void main(String[] args) {
        SpringApplication.run(ParibahanPoolApplication.class, args);
    }

}
