package com.dgt.paribahanpool.annotations;

import com.dgt.paribahanpool.enums.FrontEndLibrary;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target( { ElementType.METHOD } )
@Retention( RetentionPolicy.RUNTIME )
public @interface AddFrontEndLibrary {

    FrontEndLibrary[] libraries() default {};
}
