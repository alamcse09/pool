package com.dgt.paribahanpool.aspects;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Aspect
@Component
@RequiredArgsConstructor( onConstructor_ = { @Autowired } )
public class AddFrontEndLibraryAspect {

    private final MvcUtil mvcUtil;

    @Before( "@annotation( addFrontEndLibrary )" )
    public void addTitleAndContent( JoinPoint joinPoint, AddFrontEndLibrary addFrontEndLibrary ){

        Object[] args = joinPoint.getArgs();
        Model model = null;

        for( Object arg: args ){

            if( arg instanceof Model && !( arg instanceof RedirectAttributes) ){

                model = (Model) arg;
            }
        }

        FrontEndLibrary[] libraries = addFrontEndLibrary.libraries();
        mvcUtil.addCssAndJsByLibraryName( model, libraries );
    }
}
