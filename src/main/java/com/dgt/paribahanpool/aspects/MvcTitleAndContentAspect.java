package com.dgt.paribahanpool.aspects;

import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.RequiredParamMissingException;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
@RequiredArgsConstructor( onConstructor_ = { @Autowired } )
public class MvcTitleAndContentAspect {

    private final MvcUtil mvcUtil;

    @Before( "@annotation( titleAndContent )" )
    public void addTitleAndContent( JoinPoint joinPoint, TitleAndContent titleAndContent ) throws RequiredParamMissingException {

        Object[] args = joinPoint.getArgs();
        Model model = null;

        for( Object arg: args ){

            if( arg instanceof Model && !( arg instanceof RedirectAttributes ) ){

                model = (Model) arg;
            }
        }

        if( model == null ){

            throw new RequiredParamMissingException( "error.required.param.missing" );
        }

        String title = titleAndContent.title();
        String content = titleAndContent.content();
        Menu activeMenu = titleAndContent.activeMenu();

        mvcUtil.addTitleAndContent( model, title, content, activeMenu );
    }
}
