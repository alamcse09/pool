package com.dgt.paribahanpool.auditlog;

import com.dgt.paribahanpool.security.model.UserPrincipal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
public class SecurityAuditAware implements AuditorAware<Long> {

    @Override
    public Optional<Long> getCurrentAuditor() {

        log.debug( "Current Auditor called" );

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if ( authentication == null || !authentication.isAuthenticated() ){
            return Optional.empty();
        }

        if( authentication.getPrincipal() instanceof String ){

            return Optional.empty();
        }
        if( ( (UserPrincipal) authentication.getPrincipal() ).getUser() != null ){

            log.debug( "Auditor found, value {}", ( (UserPrincipal) authentication.getPrincipal() ).getUser().getId() );

            return Optional.of( ( (UserPrincipal) authentication.getPrincipal() ).getUser().getId() );
        }

        log.debug( "No auditor found." );
        return Optional.empty();
    }
}
