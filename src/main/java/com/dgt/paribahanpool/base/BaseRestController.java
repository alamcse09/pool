package com.dgt.paribahanpool.base;

import com.dgt.paribahanpool.exceptions.*;
import com.dgt.paribahanpool.locale.LocalizeUtil;
import com.dgt.paribahanpool.log.model.AuditLog;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.log.service.AuditLogService;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.validation.FieldErrorResponse;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.sql.BatchUpdateException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public abstract class BaseRestController {

    @Autowired
    protected Gson gson;

    @Autowired
    protected LocalizeUtil localizeUtil;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private MvcUtil mvcUtil;

    @Autowired
    private LocaleResolver localeResolver;

    protected String getLang( HttpServletRequest request ){

        return localeResolver.resolveLocale( request ).getLanguage();
    }

    protected AuditLog log(LogEvent logEvent, Long relatedEntityId, String description, HttpServletRequest request ){

        return auditLogService.save(
                getLoggedInUser().getUser().getId(),
                logEvent,
                mvcUtil.getClientIP( request ),
                relatedEntityId,
                description
        );
    }

    public UserPrincipal getLoggedInUser(){

        return ( UserPrincipal ) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    protected void handleBindingResult( BindingResult bindingResult ) throws BadRequestException {

        if( bindingResult.hasErrors() ) {

            List<FieldErrorResponse> fieldErrorResponseList =
                    bindingResult.getAllErrors().stream()
                    .map(
                            error -> {

                                FieldErrorResponse fieldErrorResponse = new FieldErrorResponse();

                                if( error instanceof FieldError) {

                                    FieldError fieldError = ( FieldError ) error;
                                    BeanUtils.copyProperties( fieldError, fieldErrorResponse );
                                }
                                else{

                                    BeanUtils.copyProperties( error, fieldErrorResponse );
                                }

                                return fieldErrorResponse;
                    })
                    .collect( Collectors.toList() );

            String errorMessage = gson.toJson( fieldErrorResponseList );

            throw new BadRequestException( errorMessage );
        }
    }

    @ResponseBody
    @ExceptionHandler( { DataIntegrityViolationException.class } )
    Object handleDataIntegrityViolationException( HttpServletRequest request, Exception ex ){

        Throwable cause = ex.getCause();
        if( cause != null && cause instanceof org.hibernate.exception.ConstraintViolationException ) {

            Throwable cause2 = cause.getCause();
            if( cause2 != null && cause2 instanceof BatchUpdateException ){

                return handleBatchUpdateException( request, (Exception) cause2 );
            }
            else
                return handleBadRequest( request, (Exception) cause );
        }
        else{

            return handleNotFoundException( request, ex );
        }
    }

    @ResponseBody
    @ExceptionHandler( { BatchUpdateException.class } )
    ResponseEntity<RestResponse> handleBatchUpdateException( HttpServletRequest request, Exception ex ){

        log.error( "Exception: ", ex );

        BatchUpdateException batchUpdateException = (BatchUpdateException) ex;
        Iterator<Throwable> iterator = batchUpdateException.iterator();

        List<String> errors = new ArrayList<>();
        while( iterator.hasNext() ){

            errors.add( iterator.next().getMessage() );
        }

        String message = "";

        RestResponse restErrorResponse = RestResponse.builder()
                .message( message )
                .time( LocalDateTime.now() )
                .success( false )
                .build();

        return ResponseEntity.badRequest().body( restErrorResponse );
    }
    @ResponseBody
    @ExceptionHandler( { NotFoundException.class })
    ResponseEntity<RestResponse> handleNotFoundException( HttpServletRequest request, Exception e ){

        String key = getKeyFromException( e );
        String message = localizeUtil.getMessageFromMessageSource( key, "Not found" , request );

        log.error( "Exception: ", e );
        RestResponse restErrorResponse = RestResponse.builder()
                .message( message )
                .time( LocalDateTime.now() )
                .success( false )
                .build();

        return ResponseEntity.status( HttpStatus.NOT_FOUND ).body( restErrorResponse );
    }


    @ResponseBody
    @ExceptionHandler( { BadRequestException.class } )
    ResponseEntity<RestResponse> handleBadRequest( HttpServletRequest request, Exception ex ){

        log.error( "Exception: ", ex );
        RestResponse restErrorResponse = RestResponse.builder()
                .message( ex.getMessage() )
                .time( LocalDateTime.now() )
                .success( false )
                .build();

        return ResponseEntity.badRequest().body( restErrorResponse );
    }

    @ResponseBody
    @ExceptionHandler( {ExpiredJwtException.class} )
    ResponseEntity<RestResponse> handleExpiredJwt(HttpServletRequest request, ExpiredJwtException ex ){

        log.error( "Exception: ", ex );
        RestResponse restErrorResponse = RestResponse.builder()
                .message( ex.getMessage() )
                .time( LocalDateTime.now() )
                .success( false )
                .build();

        return ResponseEntity.status( HttpStatus.UNAUTHORIZED ).body( restErrorResponse );
    }

    @ResponseBody
    @ExceptionHandler( AccessDeniedException.class )
    ResponseEntity<RestResponse> handleAccessDeniedRequest( HttpServletRequest request, Exception ex ) {

        log.error( "Exception: ", ex );
        RestResponse restErrorResponse = RestResponse.builder()
                .message( ex.getMessage() )
                .success( false )
                .build();
        return ResponseEntity.status( HttpStatus.FORBIDDEN ).body( restErrorResponse );
    }

    @ResponseBody
    @ResponseStatus( HttpStatus.INTERNAL_SERVER_ERROR )
    @ExceptionHandler( value = { Exception.class, InternalServerException.class } )
    RestResponse handleException( HttpServletRequest request, Exception ex ) {

        log.error( "Exception: ", ex );
        return RestResponse.builder()
                .message( ex.getMessage() )
                .success( false )
                .build();
    }

    protected String getKeyFromException( Exception ex ){

        Throwable cause = ex.getCause();
        if( cause != null ) {
            while (cause.getCause() != null) {
                cause = cause.getCause();
            }
        }
        String key = cause == null? ex.getMessage(): cause.getMessage();

        return key;
    }
}
