package com.dgt.paribahanpool.base;

import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HandleBindingResultParams {

    String key;
    Object object;
    String title;
    String content;
    Menu activeMenu;
    FrontEndLibrary[] frontEndLibraries;
}
