package com.dgt.paribahanpool.base;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.config.AppProperties;
import com.dgt.paribahanpool.exceptions.PermissionDeniedException;
import com.dgt.paribahanpool.locale.LocalizeUtil;
import com.dgt.paribahanpool.log.model.AuditLog;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.log.service.AuditLogService;
import com.dgt.paribahanpool.notification.service.NotificationService;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@Slf4j
abstract public class MVCController {

    @Autowired
    protected Environment environment;

    @Autowired
    private LocaleResolver localeResolver;

    @Autowired
    private LocalizeUtil localizeUtil;

    @Autowired
    private MvcUtil mvcUtil;

    @Autowired
    private AppProperties appProperties;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private NotificationService notificationService;

    protected String viewRoot = "layout";

    @ModelAttribute
    public void initializeModel( HttpServletRequest request, Model model ){

        String lang = localeResolver.resolveLocale( request ).getLanguage();
        UserPrincipal userPrincipal = ( UserPrincipal ) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        model.addAttribute( "lang", lang );
        model.addAttribute( "loggedInUser", userPrincipal );
        model.addAttribute( "ppUrl", userPrincipal.getPpDocId() == null? request.getContextPath() + "/doc/group/" + AppConstants.DEFAULT_PP_GROUP_ID : request.getContextPath() + "/doc/group/" + userPrincipal.getPpDocId() );
        model.addAttribute( "host", appProperties.getServer().getHost() );
        model.addAttribute( "port", appProperties.getServer().getPort() );

        model.addAttribute( "chooseMechanicInchargeActionId", AppConstants.NOC_APP_ENCASHMENT_CHOOSE_MECHANIC_INCHARGE_ACTION_ID);
        model.addAttribute( "doFineActionId", AppConstants.NOC_APP_ENCASHMENT_DO_FINE_ACTION_ID);
        model.addAttribute( "collectFineActionId", AppConstants.NOC_APP_ENCASHMENT_COLLECT_FINE_ACTION_ID);
        model.addAttribute( "chooseVehicleActionId", AppConstants.VEHICLE_REQUISITION_CHOOSE_ACTION_ID );
        model.addAttribute("notifications", notificationService.getNotificationResponseForUser( getLoggedInUser().getUser(), PageRequest.of( 0, 20, Sort.by( Sort.Direction.DESC, "notificationTime" ) ) ) );
        model.addAttribute( "unseenNotificationCount", notificationService.countUnseenNotification( getLoggedInUser().getUser() ) );
    }

    protected String getLang( HttpServletRequest request ){

        return localeResolver.resolveLocale( request ).getLanguage();
    }

    protected AuditLog log( LogEvent logEvent, Long relatedEntityId, String description, HttpServletRequest request ){

        return auditLogService.save(
                getLoggedInUser().getUser().getId(),
                logEvent,
                mvcUtil.getClientIP( request ),
                relatedEntityId,
                description
        );
    }

    public UserPrincipal getLoggedInUser(){

        return ( UserPrincipal ) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    @ExceptionHandler( PermissionDeniedException.class )
    public String handlePermissionDeniedException(
            Exception ex,
            HttpServletRequest req,
            Model model
    ) {

        log.error( "Request: " + req.getRequestURL() + " raised " + ex );

        model.addAttribute( "exception", ex );
        model.addAttribute( "url", req.getRequestURL() );
        model.addAttribute( "message", ex.getMessage() );

        return "error/403";
    }

    @ExceptionHandler( Exception.class )
    public String handleError(
            Exception ex,
            HttpServletRequest req,
            Model model
    ) {

        log.error( "Request: " + req.getRequestURL() + " raised ", ex );

        Throwable rootCause = ex;

        String key = getKeyFromException( ex );
        String message = localizeUtil.getMessageFromMessageSource( key, "Required parameter missing" , req );

        model.addAttribute( "exception", ex );
        model.addAttribute( "url", req.getRequestURL() );
        model.addAttribute( "message", message );

        return "error/error_exception";
    }

    protected String getKeyFromException( Exception ex ){

        Throwable cause = ex.getCause();
        if( cause != null ) {
            while (cause.getCause() != null) {
                cause = cause.getCause();
            }
        }
        String key = cause == null? ex.getMessage(): cause.getMessage();

        return key;
    }

    protected String getLanguage( HttpServletRequest request ){

        return localeResolver.resolveLocale( request ).getLanguage();
    }

    protected void setLocaleLanguageInModel( HttpServletRequest request, Model model ) {

        String lang = localeResolver.resolveLocale( request ).getLanguage();
        model.addAttribute( "lang", lang );
    }

    protected void addSuccessMessage(RedirectAttributes redirectAttributes, String message ){

        mvcUtil.addSuccessMessage( redirectAttributes, message );
    }

    protected void addErrorMessage( RedirectAttributes redirectAttributes, String message ){

        mvcUtil.addErrorMessage( redirectAttributes, message );
    }

    protected void addSuccessMessage( Model model, String message ) {

        mvcUtil.addSuccessMessage( model, message );
    }

    protected void addErrorMessage( Model model, String message ) {

        mvcUtil.addErrorMessage( model, message );
    }
}
