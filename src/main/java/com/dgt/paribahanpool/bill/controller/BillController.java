package com.dgt.paribahanpool.bill.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.bill.model.BillAddRequest;
import com.dgt.paribahanpool.bill.model.BillDistributionAddRequest;
import com.dgt.paribahanpool.bill.model.ChequeIssueAddRequest;
import com.dgt.paribahanpool.bill.service.BillModelService;
import com.dgt.paribahanpool.bill.service.BillValidationsService;
import com.dgt.paribahanpool.bill.service.ChequeValidationService;
import com.dgt.paribahanpool.bill.service.DistributionValidationService;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/bill" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BillController extends MVCController {

    private final BillModelService billModelService;
    private final BillValidationsService billValidationsService;
    private final ChequeValidationService chequeValidationService;
    private final DistributionValidationService distributionValidationService;

    @GetMapping( "/create" )
    @TitleAndContent( title = "title.bill.create", content = "bill/bill-create", activeMenu = Menu.VEHICLE_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.BILL_ADD_FORM } )
    public String create(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add vehicle form" );
        return viewRoot;
    }

    @GetMapping( "/view" )
    @TitleAndContent( title = "title.bill.view", content = "bill/bill-view" )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.BILL_VIEW_FORM } )
    public String view(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add vehicle form" );
        return viewRoot;
    }

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.bill.add", content = "bill/bill-add", activeMenu = Menu.BILL_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.BILL_ADD_FORM } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        billModelService.addBillGet( model );
        return viewRoot;
    }

    @PostMapping( "/add" )
    public String addBillPost(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid BillAddRequest billAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !billValidationsService.handleBindingResultForAddFormPost( model, bindingResult, billAddRequest ) ){

            return viewRoot;
        }

        billModelService.addBillPost( billAddRequest, redirectAttributes );
        return "redirect:/bill/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.bill.search", content = "bill/bill-search", activeMenu = Menu.BILL_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.BILL_SEARCH } )
    public String getBillSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Bill Search page called" );
        return viewRoot;
    }

    @GetMapping( "/preview/{id}" )
    @TitleAndContent( title = "title.bill.preview", content = "bill/bill-preview", activeMenu = Menu.BILL_PREVIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.BILL_PREVIEW } )
    public String preview(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ){

        if( billValidationsService.view( redirectAttributes, id ) ) {

            billModelService.view( model, id, getLoggedInUser() );
            return viewRoot;
        }

        return "redirect:/bill/search";
    }

    @PostMapping( "/take-action" )
    @TitleAndContent( title = "title.bill.search", content = "bill/bill-search", activeMenu = Menu.BILL_SEARCH )
    public String takeAction(
            @RequestParam( "id" ) Long id,
            @RequestParam( "action" ) Long actionId,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ) throws Exception {

        if( billValidationsService.takeActionValidation( actionId, redirectAttributes ) ) {

            billModelService.takeAction( redirectAttributes, id, actionId, getLoggedInUser() );
        }

        return "redirect:/bill/search";
    }

    @GetMapping( "/chequeissue/{billId}" )
    @TitleAndContent( title = "title.bill.cheque.issue", content = "bill/cheque-issue-add", activeMenu = Menu.CHEQUE_ISSUE )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.CHEQUE_ISSUE_ADD_FORM } )
    public String ChequeIssueAdd(
            @PathVariable( "billId" ) Long billId,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ){

        billModelService.addChequeIssueGet( model, billId );
        return viewRoot;
    }

    @PostMapping( "/chequeissue" )
    public String addChequePost(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid ChequeIssueAddRequest chequeIssueAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !chequeValidationService.handleBindingResultForAddFormPost( model, bindingResult, chequeIssueAddRequest ) ){

            return viewRoot;
        }

        billModelService.addChequeIssuePost(model, chequeIssueAddRequest, redirectAttributes );
        return "redirect:/bill/search";
    }

    @GetMapping( "/distribute/{billId}" )
    @TitleAndContent( title = "title.bill.cheque.distribution", content = "bill/cheque-distribution", activeMenu = Menu.CHEQUE_DISTRIBUTION )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.CHEQUE_DISTRIBUTION_ADD_FORM } )
    public String Distribution(
            @PathVariable( "billId" ) Long billId,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ){
        billModelService.addBillDistributeGet(model,billId);
        return viewRoot;
    }

    @PostMapping( "/distribute" )
    public String addDistributePost(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid BillDistributionAddRequest billDistributionAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !distributionValidationService.handleBindingResultForAddFormPost( model, bindingResult, billDistributionAddRequest ) ){

            return viewRoot;
        }

        billModelService.addDistributePost(model,billDistributionAddRequest, redirectAttributes );
        return "redirect:/bill/search";
    }
}