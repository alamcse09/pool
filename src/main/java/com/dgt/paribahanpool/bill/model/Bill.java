package com.dgt.paribahanpool.bill.model;

import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.WorkflowEntity;
import lombok.Data;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table( name = "bill")
@SQLDelete( sql = "UPDATE bill set is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class Bill extends AuditableEntity implements WorkflowEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "bill_provider_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String billProviderName;

    @Column( name = "bill_creation_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate billCreationDate;

    @Column( name = "bill_submit_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate billSubmitDate;

    @Column( name = "bill_submitter" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String billSubmitter;

    @Column( name = "organization_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String organizationName;

    @Column( name = "office_id" )
    private Long officeId;

    @Column( name = "finance_group_id" )
    private Long financeGroupId;

    @Column( name = "finance_code" )
    private String financeCode;

    @Column( name = "voucher_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String voucherNumber;

    @Column( name = "bill_amount" )
    private Double billAmount;

    @Column( name = "details", columnDefinition = "TEXT" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String billDetails;

    @Column( name = "vat" )
    private Double vat;

    @Column( name = "tax" )
    private Double tax;

    @Column( name = "last_year_bill_amount" )
    private Double lastYearBillAmount;

    @Column( name = "document_group_id" )
    private Long docGroupId;

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinTable(
            name = "bill_bill_cutting_record_map",
            joinColumns = @JoinColumn( name = "bill_id", foreignKey = @ForeignKey( name = "fk_bill_cutting_record_bill_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "bill_cutting_record_id", foreignKey = @ForeignKey( name = "fk_bill_id_bill_cutting_record_map_bill_cutting_record_id" ) )
    )
    private Set<BillCuttingRecord> billCuttingRecordSet = new HashSet<>();

    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "state_id", foreignKey = @ForeignKey( name = "bill_state_id" ) )
    private State state;
}
