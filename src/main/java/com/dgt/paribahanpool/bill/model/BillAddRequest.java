package com.dgt.paribahanpool.bill.model;

import com.dgt.paribahanpool.budget.model.BudgetCodeViewDetails;
import com.dgt.paribahanpool.budget.model.BudgetOfficeViewResponse;
import com.dgt.paribahanpool.budget.model.FinancialGroupViewResponse;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.marine.model.MarineServiceInfoAddRequest;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Data
public class BillAddRequest {

    private Long id;

    @NotBlank( message = "{validation.common.required}" )
    private String billProviderName;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate billCreationDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate billSubmitDate;

    private String billSubmitter;

    private String organizationName;

    private List<BudgetOfficeViewResponse> officeViewResponseList;

    private Long officeId;

    private Long financeGroupId;

    private String financeCode;

    private String voucherNumber;

    private Double billAmount;

    @NotBlank( message = "{validation.common.required}" )
    private String billDetails;

    private Double vat;

    private Double tax;

    private Double lastYearBillAmount;

    private MultipartFile[] files;

    private List<DocumentMetadata> documents = Collections.EMPTY_LIST;

    private Map<Long, List<BudgetCodeViewDetails>> budgetCodeViewDetailsMap;

    private String budgetCodeViewDetailsMapJson;

    private List<FinancialGroupViewResponse> financialGroupViewResponses;

    List<BillCuttingInfoAddRequest> billCuttingInfoAddRequestList;
}
