package com.dgt.paribahanpool.bill.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class BillCuttingInfoAddRequest {

    private Long id;

    @NotNull( message = "{validation.common.required}" )
    private String financeCode;

    @NotNull( message = "{validation.common.required}" )
    private Double amount;
}
