package com.dgt.paribahanpool.bill.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "bill_cutting_record" )
public class BillCuttingRecord {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "finance_code" )
    private String financeCode;

    @Column( name = "bill_amount" )
    private Double amount;
}
