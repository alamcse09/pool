package com.dgt.paribahanpool.bill.model;

import lombok.Data;

@Data
public class BillCuttingViewResponse {

    private Long id;
    private String financeCode;
    private String name;
    private String amount;
}
