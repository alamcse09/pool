package com.dgt.paribahanpool.bill.model;

import com.dgt.paribahanpool.workflow.model.StateResponse;
import lombok.Data;

import java.time.LocalDate;

@Data
public class BillSearchResponse {

    private Long id;

    private String billProviderName;

    private LocalDate billCreationDate;

    private LocalDate billSubmitDate;

    private String billSubmitter;

    private String organizationName;

    private Long officeId;

    private Long financeGroupId;

    private String financeCode;

    private String voucherNumber;

    private Double billAmount;

    private Double vat;

    private Double tax;

    private Double lastYearBillAmount;

    private StateResponse stateResponse;
}
