package com.dgt.paribahanpool.bill.model;

import com.dgt.paribahanpool.budget.model.BudgetOfficeViewResponse;
import com.dgt.paribahanpool.budget.model.FinancialGroupViewResponse;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class BillViewResponse {

    private Long id;
    private Long stateId;
    private String billProviderName;
    private LocalDate billCreationDate;
    private LocalDate billSubmitDate;
    private String billSubmitter;
    private String organizationName;
    private BudgetOfficeViewResponse budgetOfficeViewResponse;
    private FinancialGroupViewResponse financialGroupViewResponse;
    private String financeCode;
    private String voucherNumber;
    private String billAmount;
    private String vat;
    private String tax;
    private String deduction;
    private String netBill;
    private Double lastYearBillAmount;
    private String billDetails;

    private List<BillCuttingViewResponse> billCuttingViewResponseList;
}
