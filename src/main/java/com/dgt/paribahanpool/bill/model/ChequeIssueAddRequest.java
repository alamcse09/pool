package com.dgt.paribahanpool.bill.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
public class ChequeIssueAddRequest {

    private Long id;

    private Long billId;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate chequeIssueDate;

    private String accountNumber;

    private String chequeNumber;

    private String vendorName;

    private String tokenNumber;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate tokenDate;

    private String receiver;

    private Double amountOfMoney;

    private String description;

    private MultipartFile[] files;

    private List<DocumentMetadata> documents = Collections.EMPTY_LIST;
}