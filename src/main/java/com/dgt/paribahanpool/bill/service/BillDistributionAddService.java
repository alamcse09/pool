package com.dgt.paribahanpool.bill.service;

import com.dgt.paribahanpool.bill.model.BillDistribution;
import com.dgt.paribahanpool.bill.model.BillDistributionAddRequest;
import com.dgt.paribahanpool.document.service.DocumentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BillDistributionAddService {

    private final DistributionRepository distributionRepository;

    public Optional<BillDistribution> findById(Long id ){

        return distributionRepository.findById( id );
    }

    public BillDistribution save(BillDistribution distributeModel){

        return distributionRepository.save( distributeModel );
    }

    @Transactional
    public void save(BillDistributionAddRequest billDistributionAddRequest){
        BillDistribution billDistribution = new BillDistribution();

        if (billDistributionAddRequest.getId() != null){
            billDistribution = findById( billDistributionAddRequest.getId() ).get();
        }

        billDistribution = getDistributionFromBillDistributionAddRequest(billDistribution,billDistributionAddRequest);

        save(billDistribution);

    }

    private BillDistribution getDistributionFromBillDistributionAddRequest(BillDistribution billDistribution, BillDistributionAddRequest billDistributionAddRequest){
        billDistribution.setBillId(billDistributionAddRequest.getBillId());
        billDistribution.setChequeDistributionDate(billDistributionAddRequest.getChequeDistributionDate());
        billDistribution.setTypeOfDistribution(billDistributionAddRequest.getTypeOfDistribution());
        billDistribution.setVendorName(billDistributionAddRequest.getVendorName());
        billDistribution.setTokenNumber(billDistributionAddRequest.getTokenNumber());
        billDistribution.setTokenDate(billDistributionAddRequest.getTokenDate());
        billDistribution.setReceiver(billDistributionAddRequest.getReceiver());
        billDistribution.setAmountOfMoney(billDistributionAddRequest.getAmountOfMoney());
        billDistribution.setDescription(billDistributionAddRequest.getDescription());
        return billDistribution;
    }
}
