package com.dgt.paribahanpool.bill.service;

import com.dgt.paribahanpool.bill.model.*;
import com.dgt.paribahanpool.budget.model.*;
import com.dgt.paribahanpool.budget.service.BudgetCodeService;
import com.dgt.paribahanpool.budget.service.BudgetOfficeService;
import com.dgt.paribahanpool.budget.service.BudgetUsedService;
import com.dgt.paribahanpool.budget.service.BudgetYear;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.util.NameResponse;
import com.dgt.paribahanpool.util.Util;
import com.dgt.paribahanpool.workflow.model.StateActionMapResponse;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.*;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BillModelService {

    private final BudgetOfficeService budgetOfficeService;
    private final BudgetCodeService budgetCodeService;
    private final BudgetUsedService budgetUsedService;
    private final StateActionMapService stateActionMapService;
    private final BillService billService;
    private final ChequeIssueAddRequestService chequeIssueAddRequestService;
    private final ChequeRepository chequeRepository;
    private final BillDistributionAddService billDistributionAddService;
    private final Gson gson;
    private final MvcUtil mvcUtil;

    public void addBillGet(Model model) {

        BillAddRequest billAddRequest = new BillAddRequest();

        List<BudgetOfficeViewResponse> budgetOfficeList = budgetOfficeService.getBudgetOfficeViewResponseList();
        Map<Long, List<BudgetCodeViewDetails>> budgetCodeViewDetailsMap = budgetCodeService.getBudgetCodeWithDetailsMap();
        List<FinancialGroupViewResponse> financialGroupViewResponses = budgetCodeService.getFinancialGroup();

        billAddRequest.setOfficeViewResponseList( budgetOfficeList );
        billAddRequest.setBudgetCodeViewDetailsMap( budgetCodeViewDetailsMap );
        billAddRequest.setBudgetCodeViewDetailsMapJson( gson.toJson( budgetCodeViewDetailsMap ) );
        billAddRequest.setFinancialGroupViewResponses( financialGroupViewResponses );

        BudgetYear budgetYear = BudgetYear.getBudgetYearFromDate(LocalDate.now());
        Map<String,Double> budgetCodeYearMap = new HashMap<>();
        List<BudgetCodeSearchResponse> allLeafBudgetCodes = new ArrayList<>();
        for(BudgetCode budgetCode : budgetCodeService.findBudgetCodesByIsLeaf(true)){

            BudgetCodeSearchResponse budgetCodeSearchResponse = new BudgetCodeSearchResponse();
            budgetCodeSearchResponse.setCode( budgetCode.getCode() );
            budgetCodeSearchResponse.setNameBn( budgetCode.getCode() + " (" + budgetCode.getNameBn() + ")" );

            allLeafBudgetCodes.add( budgetCodeSearchResponse );
            if (!budgetUsedService.findByBudgetCodeAndFromYearAndToYear(budgetCode,budgetYear.getFromYear(),budgetYear.getToYear()).isEmpty()){
                budgetCodeYearMap.put(budgetCode.getCode(),budgetUsedService.findByBudgetCodeAndFromYearAndToYear(budgetCode,budgetYear.getFromYear(),budgetYear.getToYear()).get(0).getUsedAmount());
            }else{
                budgetCodeYearMap.put(budgetCode.getCode(),0.0);
            }
        }
        JSONObject budgetCodeYearMapJsonObject = new JSONObject(budgetCodeYearMap);
        model.addAttribute("allLeafBudgetCodes",allLeafBudgetCodes);
        model.addAttribute("billAddRequest", billAddRequest );
        model.addAttribute("budgetCodeYearMapJsonObject", budgetCodeYearMapJsonObject);
    }

    public void addBillPost(BillAddRequest billAddRequest, RedirectAttributes redirectAttributes) {

        billService.save( billAddRequest );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.bill.add" );
    }

    @Transactional
    public void view(Model model, Long id, UserPrincipal loggedInUser) {

        Optional<Bill> optionalBill = billService.findById( id );

        if( optionalBill.isPresent() ) {

            BillViewResponse billViewResponse = billService.getBillViewResponseFromBill( optionalBill.get() );
            model.addAttribute( "billViewResponse", billViewResponse );
            model.addAttribute("operationCode", Util.makeDigitBangla(billViewResponse.getId().toString()).toCharArray());

            if( billViewResponse.getStateId() != null ){

                List<StateActionMapResponse> stateActionMapList = stateActionMapService.findStateActionMapResponseListByCurrentStateAndRoleIdSet( billViewResponse.getStateId(), loggedInUser.getRoleIds() );
                model.addAttribute( "stateActionList", stateActionMapList );
            }
        }
    }

    public void takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, UserPrincipal loggedInUser) throws Exception {

        billService.takeAction( id, actionId, loggedInUser );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.common.action.success" );
    }

    public void addChequeIssueGet(Model model, Long billId) {

        ChequeIssueAddRequest chequeIssueAddRequest = new ChequeIssueAddRequest();
        chequeIssueAddRequest.setBillId(billId);
        model.addAttribute("chequeIssueAddRequest", chequeIssueAddRequest);
    }

    public void addChequeIssuePost(Model model,ChequeIssueAddRequest chequeIssueAddRequest, RedirectAttributes redirectAttributes) {
        chequeIssueAddRequestService.save(chequeIssueAddRequest);
//        new Notification("event",
//                "HelloNotification","Bangla Notification",
//                "Hello World !","Bangla Notification").pushNotificationToModel(model);
        mvcUtil.addSuccessMessage( redirectAttributes, "success.cheque.add" );
    }

    public void addBillDistributeGet(Model model,Long billId) {
        BillDistributionAddRequest billDistributionAddRequest = new BillDistributionAddRequest();
        List<Cheque> cheques = chequeRepository.findChequeByBillId(billId);
        if(cheques.size()>0){
            Cheque cheque = cheques.get(cheques.size()-1);
            billDistributionAddRequest.setBillId(cheque.getBillId());
            billDistributionAddRequest.setChequeDistributionDate(LocalDate.now());
            billDistributionAddRequest.setTypeOfDistribution("Cheque");
            billDistributionAddRequest.setAmountOfMoney(cheque.getAmountOfMoney());
            billDistributionAddRequest.setVendorName(cheque.getVendorName());
            billDistributionAddRequest.setTokenNumber(cheque.getTokenNumber());
            billDistributionAddRequest.setTokenDate(cheque.getTokenDate());
            billDistributionAddRequest.setReceiver(cheque.getReceiver());
            model.addAttribute("billDistributionAddRequest", billDistributionAddRequest);
        }else{
            billDistributionAddRequest.setBillId(new Long(-1));
            model.addAttribute("billDistributionAddRequest", billDistributionAddRequest);
        }
    }

    public void addDistributePost(Model model,BillDistributionAddRequest billDistributionAddRequest, RedirectAttributes redirectAttributes) {
        billDistributionAddService.save(billDistributionAddRequest);
        mvcUtil.addSuccessMessage( redirectAttributes, "success.distribution.add" );
    }
}
