package com.dgt.paribahanpool.bill.service;

import com.dgt.paribahanpool.bill.model.Bill;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface BillRepository extends DataTablesRepository<Bill,Long> {
}
