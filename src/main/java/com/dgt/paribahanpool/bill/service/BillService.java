package com.dgt.paribahanpool.bill.service;

import com.dgt.paribahanpool.bill.model.*;
import com.dgt.paribahanpool.budget.model.BudgetCode;
import com.dgt.paribahanpool.budget.model.BudgetOffice;
import com.dgt.paribahanpool.budget.model.FinancialGroupViewResponse;
import com.dgt.paribahanpool.budget.service.BudgetCodeService;
import com.dgt.paribahanpool.budget.service.BudgetOfficeService;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.document.model.Document;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.util.Util;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.WorkflowService;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BillService implements WorkflowService<Bill> {

    private final StateActionMapService stateActionMapService;
    private final DocumentService documentService;
    private final BillRepository billRepository;
    private final StateService stateService;
    private final BudgetCodeService budgetCodeService;
    private final BudgetOfficeService budgetOfficeService;

    public Optional<Bill> findById(Long id ){

        return billRepository.findById( id );
    }

    public Bill save( Bill bill ){

        return billRepository.save( bill );
    }

    @Transactional
    public void save(BillAddRequest billAddRequest) {

        Bill bill = new Bill();

        if( billAddRequest.getId() != null ){

            bill = findById( billAddRequest.getId() ).get();
        }

        bill = getBillFromBillAddRequest( bill, billAddRequest );

        save( bill );
    }

    private Bill getBillFromBillAddRequest(Bill bill, BillAddRequest billAddRequest) {

        bill.setBillProviderName( billAddRequest.getBillProviderName() );
        bill.setBillCreationDate( billAddRequest.getBillCreationDate() );
        bill.setBillSubmitDate( billAddRequest.getBillSubmitDate() );
        bill.setBillSubmitter( billAddRequest.getBillSubmitter() );
        bill.setOrganizationName( billAddRequest.getOrganizationName() );
        bill.setOfficeId( billAddRequest.getOfficeId() );
        bill.setFinanceCode( billAddRequest.getFinanceCode() );
        bill.setFinanceGroupId( billAddRequest.getFinanceGroupId() );
        bill.setVoucherNumber( billAddRequest.getVoucherNumber() );
        bill.setVat( billAddRequest.getVat() );
        bill.setTax( billAddRequest.getTax() );
        bill.setBillAmount( billAddRequest.getBillAmount() );
        bill.setLastYearBillAmount( billAddRequest.getLastYearBillAmount() );
        bill.setBillDetails( billAddRequest.getBillDetails() );

        if( billAddRequest.getBillCuttingInfoAddRequestList() != null ){

            Set<BillCuttingRecord> billCuttingRecordSet =
                    billAddRequest.getBillCuttingInfoAddRequestList()
                            .stream()
                            .map( billCuttingInfoAddRequest -> getBillCuttingRecordFromBillAddRequest( billCuttingInfoAddRequest, bill ) )
                            .filter( billCuttingRecord -> billCuttingRecord != null )
                            .collect( Collectors.toSet() );

            bill.getBillCuttingRecordSet().clear();
            bill.getBillCuttingRecordSet().addAll( billCuttingRecordSet );
        }

        if( bill.getState() == null ){

            Long startStateId = AppConstants.BILL_CREATE_INITIAL_STATE_ID;

            State state = stateService.findStateReferenceById( startStateId );
            bill.setState( state );
        }

        if( bill.getDocGroupId() == null ) {

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( billAddRequest.getFiles(), Collections.EMPTY_MAP );
            bill.setDocGroupId(documentUploadedResponse.getDocGroupId());
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( billAddRequest.getFiles(), bill.getDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        return bill;
    }

    private BillCuttingRecord getBillCuttingRecordFromBillAddRequest(BillCuttingInfoAddRequest billCuttingInfoAddRequest, Bill bill) {

        BillCuttingRecord billCuttingRecord = new BillCuttingRecord();

        billCuttingRecord.setFinanceCode( billCuttingInfoAddRequest.getFinanceCode() );
        billCuttingRecord.setAmount( billCuttingInfoAddRequest.getAmount() );

        return billCuttingRecord;
    }

    public DataTablesOutput<BillSearchResponse> searchForDatatable(DataTablesInput dataTablesInput) {

        return billRepository.findAll( dataTablesInput, this::getBillSearchResponseFromBill );
    }

    private BillSearchResponse getBillSearchResponseFromBill( Bill bill ) {

        BillSearchResponse billSearchResponse = new BillSearchResponse();
        billSearchResponse.setId( bill.getId() );
        billSearchResponse.setBillProviderName( bill.getBillProviderName() );
        billSearchResponse.setBillSubmitter( bill.getBillSubmitter() );
        billSearchResponse.setOrganizationName( bill.getOrganizationName() );
        billSearchResponse.setFinanceGroupId( bill.getFinanceGroupId() );
        billSearchResponse.setFinanceCode( bill.getFinanceCode() );
        billSearchResponse.setBillAmount( bill.getBillAmount() );
        billSearchResponse.setVoucherNumber( bill.getVoucherNumber() );

        State state = bill.getState();
        if( state != null ){

            billSearchResponse.setStateResponse( state.getResponse() );
        }

        return  billSearchResponse;
    }

    public BillViewResponse getBillViewResponseFromBill(Bill bill) {

        BillViewResponse billViewResponse = new BillViewResponse();

        FinancialGroupViewResponse financialGroupViewResponse = new FinancialGroupViewResponse();

        Optional<BudgetOffice> budgetOfficeOptional = budgetOfficeService.findById( bill.getOfficeId() );

//        Optional<BudgetCode> budgetCodeOptional = budgetCodeService.findById( bill.getFinanceGroupId() );

//        if( budgetCodeOptional.isPresent() ){
//
//            financialGroupViewResponse = budgetCodeService.getFinancialGroupViewResponseFromBudgetCode( budgetCodeOptional.get() );
//        }

        Double deduction = 0.0D;

        List<BillCuttingViewResponse> billCuttingViewResponseList = new ArrayList<>();

        for( BillCuttingRecord billCuttingRecord : bill.getBillCuttingRecordSet() ){

            BillCuttingViewResponse billCuttingViewResponse = new BillCuttingViewResponse();

            Optional<BudgetCode> optionalBudgetCode = budgetCodeService.findByCode( billCuttingRecord.getFinanceCode() );

            billCuttingViewResponse.setId( billCuttingRecord.getId() );

            if( optionalBudgetCode.isPresent() ) {

                billCuttingViewResponse.setFinanceCode( Util.makeDigitBangla(optionalBudgetCode.get().getCode()) );
                billCuttingViewResponse.setName( optionalBudgetCode.get().getNameBn() );
            }

            billCuttingViewResponse.setAmount( Util.makeDigitBangla(billCuttingRecord.getAmount()) );

            deduction += billCuttingRecord.getAmount();

            billCuttingViewResponseList.add( billCuttingViewResponse );
        }

        billViewResponse.setBillCuttingViewResponseList( billCuttingViewResponseList );

        billViewResponse.setStateId( bill.getState() != null? bill.getState().getId(): null);
        billViewResponse.setId( bill.getId() );
        billViewResponse.setBillProviderName( bill.getBillProviderName() );
        billViewResponse.setBillCreationDate( bill.getBillCreationDate() );
        billViewResponse.setBillSubmitDate( bill.getBillSubmitDate() );
        billViewResponse.setBillSubmitter( bill.getBillSubmitter() );
        billViewResponse.setOrganizationName( bill.getOrganizationName() );
        billViewResponse.setFinanceCode(Util.makeDigitBangla(bill.getFinanceCode().toString()) );
        billViewResponse.setVoucherNumber( bill.getVoucherNumber() );
        billViewResponse.setBillAmount( Util.makeDigitBangla(bill.getBillAmount().toString()) );
//        billViewResponse.setVat( Util.makeDigitBangla(bill.getVat().toString()) );
//        billViewResponse.setTax( Util.makeDigitBangla(bill.getTax().toString()) );
        billViewResponse.setDeduction( Util.makeDigitBangla(deduction) );
        billViewResponse.setNetBill( Util.makeDigitBangla(new Double(bill.getBillAmount() - deduction).toString()) );
        billViewResponse.setLastYearBillAmount( bill.getLastYearBillAmount() );
//        billViewResponse.setFinancialGroupViewResponse( financialGroupViewResponse );
        billViewResponse.setBillDetails( bill.getBillDetails() );

        if( budgetOfficeOptional.isPresent() ){

            billViewResponse.setBudgetOfficeViewResponse( budgetOfficeService.getBudgetOfficeViewResponseFromBudgetOffice( budgetOfficeOptional.get() ) );
        }

        return billViewResponse;
    }

    public void takeAction(Long id, Long actionId, UserPrincipal loggedInUser) throws Exception {

        stateActionMapService.takeAction( this, id, actionId, loggedInUser, null);
    }

    public void delete(Long id) {
        Optional<Bill> billToDelete =  findById( id );

        if( billToDelete.isPresent() ) {

            Bill bill = billToDelete.get();
            bill.setIsDeleted( true );
            save( bill );
        }
    }

    public boolean existById(Long id) {
        return billRepository.existsById(id);
    }
}
