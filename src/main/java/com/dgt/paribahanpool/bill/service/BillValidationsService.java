package com.dgt.paribahanpool.bill.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.bill.model.BillAddRequest;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BillValidationsService {

    private final MvcUtil mvcUtil;
    private final BillService billService;

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, BillAddRequest billAddRequest) {

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "billAddRequest" )
                        .object( billAddRequest )
                        .title( "title.bill.add" )
                        .content( "bill/bill-add" )
                        .activeMenu( Menu.BILL_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.BILL_ADD_FORM } )
                        .build()
        );
    }

    public boolean view(RedirectAttributes redirectAttributes, Long id) {

        return true;
    }

    public boolean takeActionValidation(Long actionId, RedirectAttributes redirectAttributes) {

        return true;
    }

    public RestValidationResult delete(Long id) throws NotFoundException {
        if( !billService.existById( id ) ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }
}
