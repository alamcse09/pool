package com.dgt.paribahanpool.bill.service;

import com.dgt.paribahanpool.bill.model.Bill;
import com.dgt.paribahanpool.bill.model.Cheque;
import com.dgt.paribahanpool.bill.model.ChequeIssueAddRequest;
import com.dgt.paribahanpool.document.service.DocumentService;
import liquibase.diff.output.changelog.core.ChangedSequenceChangeGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class ChequeIssueAddRequestService {

    private final DocumentService documentService;
    private final ChequeRepository chequeRepository;

    public Optional<Cheque> findById(Long id ){

        return chequeRepository.findById( id );
    }

    public List<Cheque> findChequeByBillId(Long billId){
        return chequeRepository.findChequeByBillId(billId);
    }

    public Cheque save(Cheque cheque){
        return chequeRepository.save( cheque );
    }

    @Transactional
    public void save(ChequeIssueAddRequest chequeIssueAddRequest){
        Cheque cheque = new Cheque();

        if (chequeIssueAddRequest.getId() != null){
            cheque = findById( chequeIssueAddRequest.getId() ).get();
        }

        cheque = getChequeFromChequeIssueRequest(cheque,chequeIssueAddRequest);

        save(cheque);

    }

    private Cheque getChequeFromChequeIssueRequest(Cheque cheque, ChequeIssueAddRequest chequeIssueAddRequest){
        cheque.setBillId(chequeIssueAddRequest.getBillId());
        cheque.setChequeIssueDate(chequeIssueAddRequest.getChequeIssueDate());
        cheque.setAccountNumber(chequeIssueAddRequest.getAccountNumber());
        cheque.setChequeNumber(chequeIssueAddRequest.getChequeNumber());
        cheque.setVendorName(chequeIssueAddRequest.getVendorName());
        cheque.setTokenNumber(chequeIssueAddRequest.getTokenNumber());
        cheque.setTokenDate(chequeIssueAddRequest.getTokenDate());
        cheque.setReceiver(chequeIssueAddRequest.getReceiver());
        cheque.setAmountOfMoney(chequeIssueAddRequest.getAmountOfMoney());
        cheque.setDescription(chequeIssueAddRequest.getDescription());
        return cheque;
    }
}
