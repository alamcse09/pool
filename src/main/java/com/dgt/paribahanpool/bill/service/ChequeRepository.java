package com.dgt.paribahanpool.bill.service;

import com.dgt.paribahanpool.bill.model.Cheque;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

import java.util.List;

public interface ChequeRepository extends DataTablesRepository<Cheque,Long> {
    List<Cheque> findChequeByBillId(Long billId);
}
