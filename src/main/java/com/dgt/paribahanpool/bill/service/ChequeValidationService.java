package com.dgt.paribahanpool.bill.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.bill.model.BillAddRequest;
import com.dgt.paribahanpool.bill.model.ChequeIssueAddRequest;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class ChequeValidationService {

    private final MvcUtil mvcUtil;

    public boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, ChequeIssueAddRequest chequeIssueAddRequest) {

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "chequeIssueAddRequest" )
                .object( chequeIssueAddRequest )
                .title( "title.bill.cheque.issue" )
                .content( "bill/cheque-issue-add" )
                .activeMenu( Menu.CHEQUE_ISSUE )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.CHEQUE_ISSUE_ADD_FORM } )
                .build();

        boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );
        return isValid;
    }

    public boolean view(RedirectAttributes redirectAttributes, Long id) {

        return true;
    }

    public boolean takeActionValidation(Long actionId, RedirectAttributes redirectAttributes) {

        return true;
    }

    private Boolean setModelWithError(Model model, HandleBindingResultParams params, String errorMessage) {
        mvcUtil.addErrorMessage( model, errorMessage );
        model.addAttribute( params.getKey(), params.getObject() );
        mvcUtil.addTitleAndContent( model, params.getTitle(), params.getContent(), params.getActiveMenu() );

        if( params.getFrontEndLibraries() != null ){

            mvcUtil.addCssAndJsByLibraryName( model, params.getFrontEndLibraries() );
        }

        return false;
    }
}