package com.dgt.paribahanpool.bill.service;

import com.dgt.paribahanpool.bill.model.BillDistribution;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface DistributionRepository extends DataTablesRepository<BillDistribution,Long> {
}
