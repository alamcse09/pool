package com.dgt.paribahanpool.budget.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.budget.model.BudgetAddRequest;
import com.dgt.paribahanpool.budget.model.BudgetCodeAddRequest;
import com.dgt.paribahanpool.budget.service.*;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.enums.BudgetStateType;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.excel.ExcelBuilder;
import com.dgt.paribahanpool.excel.model.ExcelDataImpl;
import com.dgt.paribahanpool.excel.model.SheetCell;
import com.dgt.paribahanpool.log.model.LogEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Map;


@Slf4j
@Controller
@RequestMapping("/budget")
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BudgetController extends MVCController {

    private final BudgetCodeModelService budgetCodeModelService;
    private final BudgetCodeValidationService budgetCodeValidationService;
    private final BudgetModelService budgetModelService;
    private final BudgetValidationService budgetValidationService;
    private final BudgetService budgetService;
    private final DocumentService documentService;

    @GetMapping("/report")
    @TitleAndContent(title = "title.budget.report", content = "budget/budget-report", activeMenu = Menu.BUDGET_REPORT)
    @AddFrontEndLibrary(libraries = {FrontEndLibrary.BUDGET_REPORT})
    public String report(
            HttpServletRequest request,
            Model model
    ) {

        log.debug("Rendering Add vehicle form");
        return viewRoot;
    }

    @GetMapping( "/code-search" )
    @TitleAndContent( title = "title.budget.code-search", content = "budget/budget-code-search", activeMenu = Menu.BUDGET_CODE_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.BUDGET_CODE_SEARCH } )
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Budget Search page called" );
        return viewRoot;
    }

    @GetMapping( "/code-add" )
    @TitleAndContent( title = "title.budget.code-add", content = "budget/budget-code-add", activeMenu = Menu.BUDGET_CODE_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.BUDGET_CODE_ADD } )
    public String addCode(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add Budget Code form" );
        budgetCodeModelService.addBudgetCode( model );
        return viewRoot;
    }

    @PostMapping( "/code-add" )
    public String addCodePost(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid BudgetCodeAddRequest budgetCodeAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !budgetCodeValidationService.handleBindingResultForAddFormPost( model, bindingResult, budgetCodeAddRequest ) ){

            return viewRoot;
        }

        budgetCodeModelService.addCodePost( budgetCodeAddRequest, redirectAttributes );
        log( LogEvent.BUDGET_CODE_ADD, budgetCodeAddRequest.getId(), "Budget Code Added", request );
        return "redirect:/budget/code-search";
    }

    @GetMapping( "/add" )
    @TitleAndContent( content = "budget/budget-add" )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.BUDGET_ADD } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add Budget form" );
        budgetModelService.addBudget( model, request );
        return viewRoot;
    }

    @PostMapping( "/add" )
    public String addBudget(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid BudgetAddRequest budgetAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        budgetAddRequest.setFrom(new Integer(budgetAddRequest.getBudgetYear().split("-")[0]));
        budgetAddRequest.setTo(new Integer(budgetAddRequest.getBudgetYear().split("-")[1]));

        if( !budgetValidationService.handleBindingResultForAddFormPost( model, bindingResult, budgetAddRequest ) ){

            return viewRoot;
        }

        if( budgetValidationService.addBudgetPost( redirectAttributes, budgetAddRequest, null ) ){

            budgetModelService.addBudgetPost( budgetAddRequest, redirectAttributes, null );
            log( LogEvent.BUDGET_ADD, budgetAddRequest.getId(), "Budget Added", request );
        }

        return "redirect:/budget/search?state=INITIAL_BUDGET&active=BUDGET_SEARCH";
    }

    @GetMapping( "/search" )
    @TitleAndContent( content = "budget/budget-search" )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.BUDGET_SEARCH } )
    public String getBudgetSearchPage(
            @RequestParam( "state" ) BudgetStateType stateType,
            @RequestParam( "active" ) Menu activeMenu,
            HttpServletRequest request,
            Model model
    ){

        budgetModelService.addPageTitleOnSearchPage( model, stateType, request );
        model.addAttribute( "activeMenu", activeMenu );
        model.addAttribute( "stateType", stateType );
        log.debug( "Budget Search page called" );
        return viewRoot;
    }

    @GetMapping( "/{id}" )
    @TitleAndContent( title = "title.budget.view", content = "budget/budget-view", activeMenu = Menu.BUDGET_VIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.JQUERY_PRINT, FrontEndLibrary.BUDGET_VIEW } )
    public String view(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ) {

        if( budgetValidationService.view( redirectAttributes, id ) ){

            budgetModelService.view( model, id );
            return viewRoot;
        }

        return "redirect:/budget/search?state=FINAL_BUDGET&active=BUDGET_FINAL_AMOUNT";
    }

    @GetMapping( "/edit/{id}" )
    @TitleAndContent( content = "budget/budget-add" )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.BUDGET_ADD } )
    public String editBudget(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable( "id" ) Long budgetId,
            @RequestParam( "state" ) BudgetStateType stateType,
            @RequestParam( "active" ) Menu activeMenu
    ) {

        log.debug( "Rendering Edit Budget Form" );

        if( budgetValidationService.editGetBudget( redirectAttributes, budgetId, stateType ) ){

            budgetModelService.editBudget( model, budgetId, activeMenu, request );
            return viewRoot;
        }


        return "redirect:/budget/search?state=" + stateType;
    }

    @PostMapping( "/edit/{id}" )
    public String postEditPage(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid BudgetAddRequest budgetAddRequest,
            BindingResult bindingResult,
            @PathVariable( "id" ) Long budgetId,
            @RequestParam( "state" ) BudgetStateType stateType,
            HttpServletRequest request
    ){

        budgetAddRequest.setFrom(new Integer(budgetAddRequest.getBudgetYear().split("-")[0]));
        budgetAddRequest.setTo(new Integer(budgetAddRequest.getBudgetYear().split("-")[1]));

        if( !budgetValidationService.handleBindingResultForAddFormPost( model, bindingResult, budgetAddRequest ) ){

            return viewRoot;
        }

        if( budgetValidationService.editBudget( budgetAddRequest, redirectAttributes, budgetId, stateType ) ){

            budgetModelService.addBudgetPost( budgetAddRequest, redirectAttributes, stateType );
            log( LogEvent.BUDGET_EDIT, budgetAddRequest.getId(), "Budget New State Amount Added", request );

            if( stateType == BudgetStateType.INITIAL_BUDGET ) {

                return "redirect:/budget/search?state=REVISED_BUDGET&active=BUDGET_REVISED_AMOUNT";
            }
            else if( stateType == BudgetStateType.REVISED_BUDGET ) {

                return "redirect:/budget/search?state=FINAL_BUDGET&active=BUDGET_FINAL_AMOUNT";
            }
        }

        return "redirect:/budget/search?state=" + stateType;
    }

    @GetMapping( "/excel/{id}" )
    public ResponseEntity<byte[]> getExcel(

            @PathVariable( "id" ) Long budgetId

    ) throws IOException {

        Map<String,List<SheetCell>> sheetListData = budgetService.getSheetDataByBudgetId( budgetId );

        byte[] data = ExcelBuilder.createExcel( new ExcelDataImpl( sheetListData ) ).exportToByteArray();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set( "Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" );
        httpHeaders.set( "Content-Disposition", "attachment; filename=\"budget_details_office.xlxs\"" );
        return new ResponseEntity<>( data, httpHeaders, HttpStatus.OK );
    }
}
