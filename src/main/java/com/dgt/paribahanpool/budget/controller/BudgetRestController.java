package com.dgt.paribahanpool.budget.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.budget.model.BudgetCodeSearchResponse;
import com.dgt.paribahanpool.budget.model.BudgetSearchResponse;
import com.dgt.paribahanpool.budget.service.BudgetCodeService;
import com.dgt.paribahanpool.budget.service.BudgetService;
import com.dgt.paribahanpool.budget.service.BudgetValidationService;
import com.dgt.paribahanpool.enums.BudgetStateType;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/budget" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BudgetRestController extends BaseRestController {

    private final BudgetCodeService budgetCodeService;
    private final BudgetService budgetService;
    private final BudgetValidationService budgetValidationService;

    @GetMapping(
            value = "/code-search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<BudgetCodeSearchResponse> searchBudgetCod(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return budgetCodeService.searchForCodeDatatable( dataTablesInput );
    }

    @GetMapping(
            value = "/search/{state}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<BudgetSearchResponse> searchBudget(
            @PathVariable( "state" ) BudgetStateType stateType,
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return budgetService.searchForDatatable( dataTablesInput, stateType );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = budgetValidationService.delete( id );

        if( restValidationResult.getSuccess() ){

            budgetService.delete( id );
            log( LogEvent.BUDGET_DELETED, id, "", request );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }

        return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
    }
}
