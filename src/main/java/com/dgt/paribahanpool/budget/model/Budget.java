package com.dgt.paribahanpool.budget.model;

import com.dgt.paribahanpool.enums.BudgetStateType;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table( name = "budget" )
@SQLDelete( sql = "UPDATE budget SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class Budget extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "from_year" )
    private Integer from;

    @Column( name = "to_year" )
    private Integer to;

    @Column( name = "state_type" )
    private BudgetStateType stateType;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH} )
    @JoinColumn( name = "budget_office_id", foreignKey = @ForeignKey( name = "fk_budget_budget_office_id" ) )
    private BudgetOffice budgetOffice;

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(
            name = "budget_budget_details_map",
            joinColumns = @JoinColumn( name = "budget_id", foreignKey = @ForeignKey( name = "fk_budget_budget_details_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "budget_details_id", foreignKey = @ForeignKey( name = "fk_budget_id_budget_details_map_budget_details_id" ) )
    )
    private List<BudgetDetails> budgetDetailsList = new ArrayList<>();
}
