package com.dgt.paribahanpool.budget.model;

import com.dgt.paribahanpool.enums.BudgetStateType;
import lombok.Data;

import javax.validation.constraints.Min;
import java.util.List;

@Data
public class BudgetAddRequest {

    private Long id;

    private List<BudgetOfficeViewResponse> officeViewResponseList;
    private Long officeId;

    private Integer from;
    private Integer to;

    private String budgetYear;
    private List<String> budgetYearList;

    private Integer fromMinuesTwo;
    private Integer fromMinuesOne;

    private BudgetStateType stateType;
    private List<BudgetCodeViewResponse> budgetCodeViewResponseList;
    private String budgetDetailsAddRequestList;
    private List<BudgetDetailsAddRequest> budgetDetailsAddRequests;
}
