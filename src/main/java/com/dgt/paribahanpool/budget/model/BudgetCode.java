package com.dgt.paribahanpool.budget.model;

import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@Entity
@Table( name = "budget_code" )
@SQLDelete( sql = "UPDATE budget_code SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class BudgetCode extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "code" )
    private String code;

    @Column( name = "name_en" )
    private String nameEn;

    @Column( name = "name_bn" )
    private String nameBn;

    @Column( name = "discription_en", columnDefinition = "TEXT")
    private String descriptionEn;

    @Column( name = "discription_bn", columnDefinition = "TEXT" )
    private String descriptionBn;

    @Column( name = "vat" )
    private Double vat;

    @Column( name = "tax" )
    private Double tax;

    @Column( name = "is_leaf" )
    private Boolean isLeaf = true;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "parent_id", foreignKey = @ForeignKey( name = "fk_budget_code_parent_id" ) )
    private BudgetCode parentBudgetCode;
}
