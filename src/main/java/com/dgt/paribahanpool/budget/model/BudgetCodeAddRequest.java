package com.dgt.paribahanpool.budget.model;

import lombok.Data;

import javax.validation.constraints.Digits;
import java.util.List;

@Data
public class BudgetCodeAddRequest {

    private Long id;

    @Digits( integer = 18, fraction = 0, message = "{validation.common.numeric}" )
    private String code;
    private String nameEn;
    private String nameBn;
    private String descriptionEn;
    private String descriptionBn;
    private Long parentCodeId;
    private List<BudgetCodeViewResponse> budgetCodeViewResponseList;
}
