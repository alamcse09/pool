package com.dgt.paribahanpool.budget.model;

import lombok.Data;

@Data
public class BudgetCodeSearchResponse {

    private Long id;
    private String code;
    private String nameEn;
    private String nameBn;
    private String parentBudgetCode;
    private Boolean isLeaf;
}
