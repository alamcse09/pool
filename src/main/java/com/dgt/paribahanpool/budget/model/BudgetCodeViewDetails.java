package com.dgt.paribahanpool.budget.model;

import lombok.Data;

import java.util.List;

@Data
public class BudgetCodeViewDetails {

    private Long id;
    private String financialCode;
    private Double vat;
    private Double tax;
    private Double lastYearBillAmount;

}
