package com.dgt.paribahanpool.budget.model;

import lombok.Data;

@Data
public class BudgetCodeViewResponse extends BudgetCodeSearchResponse {

    private Object metadata;
}
