package com.dgt.paribahanpool.budget.model;

import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.util.model.VersionableAuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@Entity
@Table( name = "budget_details" )
@SQLDelete( sql = "UPDATE budget_details SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class BudgetDetails extends VersionableAuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "initial_amount" )
    private Integer initialAmount;

    @Column( name = "revised_amount" )
    private Integer revisedAmount;

    @Column( name = "final_amount" )
    private Integer finalAmount;

    @Column( name = "used_amount" )
    private Integer usedAmount;

    @Column( name = "estimate" )
    private Integer estimate;

    @Column( name = "projection_of_next_year" )
    private Integer projectionOfNextYear;

    @Column( name = "projection_of_next_two_year" )
    private Integer projectionOfNextTwoYear;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "budget_code_id", foreignKey = @ForeignKey( name = "fk_budget_details_budget_code_id" ) )
    private BudgetCode budgetCode;
}
