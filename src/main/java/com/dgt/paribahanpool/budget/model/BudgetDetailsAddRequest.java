package com.dgt.paribahanpool.budget.model;

import lombok.Data;

@Data
public class BudgetDetailsAddRequest {

    private Long id;

    private Long budgetCodeId;
    private Integer initialAmount;
    private Integer revisedAmount;
    private Integer finalAmount;

    private Integer estimate;
    private Integer projectionOfNextYear;
    private Integer projectionOfNextTwoYear;
}
