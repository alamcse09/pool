package com.dgt.paribahanpool.budget.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BudgetDetailsViewResponse {

    private Long id;

    private String code;
    private String codeNameBn;
    private String codeNameEn;

    private Integer initialAmountForFromMinusZero;
    private Integer initialAmountForFromMinusOne;
    private Integer initialAmountForFromMinusTwo;

    private Integer initialAmount;
    private Integer revisedAmount;
    private Integer finalAmount;

    private Integer estimate;
    private Integer projectionOfNextYear;
    private Integer projectionOfNextTwoYear;

    private Boolean isLeaf;
}
