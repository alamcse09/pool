package com.dgt.paribahanpool.budget.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BudgetOfficeViewResponse {

    private Long id;
    private String nameEn;
    private String nameBn;

    private Object metadata;
}
