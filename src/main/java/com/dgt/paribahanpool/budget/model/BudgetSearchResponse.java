package com.dgt.paribahanpool.budget.model;

import com.dgt.paribahanpool.enums.BudgetStateType;
import com.dgt.paribahanpool.util.NameResponse;
import lombok.Data;

@Data
public class BudgetSearchResponse {

    private Long id;

    private BudgetOfficeViewResponse budgetOffice;
    private Integer to;
    private Integer from;
    private BudgetStateType stateType;
}
