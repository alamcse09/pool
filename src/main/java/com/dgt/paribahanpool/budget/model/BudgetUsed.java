package com.dgt.paribahanpool.budget.model;

import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.util.model.VersionableAuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@Entity
@Table( name = "budget_used" )
public class BudgetUsed extends VersionableAuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "from_year" )
    private Integer fromYear;

    @Column( name = "to_year" )
    private Integer toYear;

    @Column( name = "used_amount" )
    private Double usedAmount;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "budget_code_id", foreignKey = @ForeignKey( name = "fk_budget_used_budget_code_id" ) )
    private BudgetCode budgetCode;
}
