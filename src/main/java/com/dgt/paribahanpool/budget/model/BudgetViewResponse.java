package com.dgt.paribahanpool.budget.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class BudgetViewResponse {

    private Long id;
    private String officeNameBn;
    private Integer from;
    private Integer to;
    private List<BudgetDetailsViewResponse> budgetDetailsAddRequestList;
}
