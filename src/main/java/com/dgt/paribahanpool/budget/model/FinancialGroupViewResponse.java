package com.dgt.paribahanpool.budget.model;

import lombok.Data;

@Data
public class FinancialGroupViewResponse {
    private Long id;
    private String nameEn;
    private String nameBn;
}
