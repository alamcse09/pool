package com.dgt.paribahanpool.budget.service;

import com.dgt.paribahanpool.budget.model.BudgetCodeAddRequest;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BudgetCodeModelService {

    private final BudgetCodeService budgetCodeService;
    private final MvcUtil mvcUtil;

    public void addBudgetCode( Model model ){

        BudgetCodeAddRequest budgetCodeAddRequest = budgetCodeService.getBudgetCodeAddRequestFromBudgetCode();

        model.addAttribute( "budgetCodeAddRequest", budgetCodeAddRequest );
    }

    public void addCodePost( BudgetCodeAddRequest budgetCodeAddRequest, RedirectAttributes redirectAttributes ) {

        budgetCodeService.save( budgetCodeAddRequest );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.budget.code.add" );
    }
}
