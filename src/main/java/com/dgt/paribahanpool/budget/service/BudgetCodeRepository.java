package com.dgt.paribahanpool.budget.service;

import com.dgt.paribahanpool.budget.model.BudgetCode;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BudgetCodeRepository extends DataTablesRepository<BudgetCode, Long> , JpaRepository<BudgetCode,Long> {

    List<BudgetCode> findAllByParentBudgetCode(BudgetCode budgetCode);
    List<BudgetCode> findBudgetCodesByIsLeaf(Boolean isLeaf);
    Optional<BudgetCode> findByCode(String code);
}
