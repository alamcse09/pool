package com.dgt.paribahanpool.budget.service;

import com.dgt.paribahanpool.budget.model.*;
import com.dgt.paribahanpool.util.Util;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BudgetCodeService {

    private final BudgetCodeRepository budgetCodeRepository;
    private final BudgetUsedService budgetUsedService;

    public Optional<BudgetCode> findByCode( String code ){

        return budgetCodeRepository.findByCode( code );
    }

    public Optional<BudgetCode> findById( Long id ){

        return budgetCodeRepository.findById( id );
    }

    public List<BudgetCode> findByParentBudgetCode( BudgetCode budgetCode ) {

        return budgetCodeRepository.findAllByParentBudgetCode(budgetCode);
    }

    public Iterable<BudgetCode> findAll(){

        return budgetCodeRepository.findAll(Sort.by(Sort.Direction.ASC, "code"));
    }

    public BudgetCode save( BudgetCode budgetCode ){

        return budgetCodeRepository.save( budgetCode );
    }

    public DataTablesOutput<BudgetCodeSearchResponse> searchForCodeDatatable( DataTablesInput dataTablesInput ) {

        return budgetCodeRepository.findAll( dataTablesInput, this::getBudgetCodeSearchResponseFromBudgetCode );
    }

    private BudgetCodeSearchResponse getBudgetCodeSearchResponseFromBudgetCode( BudgetCode budgetCode ) {

        BudgetCodeSearchResponse budgetCodeSearchResponse = new BudgetCodeSearchResponse();

        budgetCodeSearchResponse.setCode( Util.makeDigitBangla( budgetCode.getCode() ) );
        budgetCodeSearchResponse.setNameBn( budgetCode.getNameBn() );
        budgetCodeSearchResponse.setNameEn( budgetCode.getNameEn() );

        if( budgetCode.getParentBudgetCode() != null && budgetCode.getParentBudgetCode().getId() > 0 ){

            BudgetCode parentBudgetCode = budgetCode.getParentBudgetCode();
            budgetCodeSearchResponse.setParentBudgetCode( Util.makeDigitBangla( parentBudgetCode.getCode() ) );
        }

        return budgetCodeSearchResponse;
    }

    public BudgetCodeAddRequest getBudgetCodeAddRequestFromBudgetCode( ) {

        BudgetCodeAddRequest budgetCodeAddRequest = new BudgetCodeAddRequest();

        List<BudgetCodeViewResponse> budgetCodeViewResponseList = getBudgetCodeViewResponseList();

        budgetCodeAddRequest.setBudgetCodeViewResponseList( budgetCodeViewResponseList );

        return budgetCodeAddRequest;
    }

    public List<BudgetCodeViewResponse> getBudgetCodeViewResponseList() {

        Iterable<BudgetCode> budgetCodeIterable = findAll();

        List<BudgetCode> budgetCodeList = Lists.newArrayList( budgetCodeIterable );

        List<BudgetCodeViewResponse> budgetCodeViewResponseList = new ArrayList<>();

        for( BudgetCode budgetCode: budgetCodeList ){

            BudgetCodeViewResponse budgetCodeViewResponse = new BudgetCodeViewResponse();

            budgetCodeViewResponse.setId( budgetCode.getId() );
            budgetCodeViewResponse.setCode( Util.makeDigitBangla( budgetCode.getCode() ) );
            budgetCodeViewResponse.setNameEn( budgetCode.getNameEn() + " ( " + budgetCode.getCode() + " ) " );
            budgetCodeViewResponse.setNameBn( budgetCode.getNameBn() + " ( " + Util.makeDigitBangla( budgetCode.getCode() ) + " ) " );
            budgetCodeViewResponse.setIsLeaf( budgetCode.getIsLeaf() );

            budgetCodeViewResponseList.add( budgetCodeViewResponse );
        }

        return budgetCodeViewResponseList;
    }

    public List<FinancialGroupViewResponse> getFinancialGroup() {

        Iterable<BudgetCode> budgetCodeIterable = findAll();
        List<BudgetCode> budgetCodeList = Lists.newArrayList( budgetCodeIterable );

        return   budgetCodeList.stream()
                .filter( budgetCode -> budgetCode.getParentBudgetCode()==null)
                .map( this:: getFinancialGroupViewResponseFromBudgetCode)
                .collect(Collectors.toList());

    }

    public FinancialGroupViewResponse getFinancialGroupViewResponseFromBudgetCode(BudgetCode budgetCode) {
        FinancialGroupViewResponse financialGroupViewResponse = new FinancialGroupViewResponse();

        financialGroupViewResponse.setId( budgetCode.getId() );
        financialGroupViewResponse.setNameBn(Util.makeDigitBangla( budgetCode.getCode() ) );
        financialGroupViewResponse.setNameEn( budgetCode.getCode() );

        return financialGroupViewResponse;
    }

    public Map<Long, List<BudgetCodeViewDetails>> getBudgetCodeWithDetailsMap() {

        Iterable<BudgetCode> budgetCodeIterable = findAll();
        List<BudgetCode> budgetCodeList = Lists.newArrayList( budgetCodeIterable );

        Map<Long, List<BudgetCodeViewDetails>> map = new HashMap<>();

        for(BudgetCode budgetCode: budgetCodeList) {

            if(budgetCode.getParentBudgetCode() == null) {

                BudgetCodeViewDetails budgetCodeViewDetails = new BudgetCodeViewDetails();
                List<BudgetCode> childBudgetCodes = findByParentBudgetCode( budgetCode );

                List<BudgetCodeViewDetails> budgetCodeViewDetailsList = new ArrayList<>();

                for(BudgetCode childBudgetCode: childBudgetCodes) {
                    BudgetCodeViewDetails budgetCodeViewDetail = new BudgetCodeViewDetails();
                    budgetCodeViewDetail.setId( childBudgetCode.getId() );
                    budgetCodeViewDetail.setFinancialCode( childBudgetCode.getCode() );
                    budgetCodeViewDetail.setVat( childBudgetCode.getVat() );
                    budgetCodeViewDetail.setTax( childBudgetCode.getTax() );
                    budgetCodeViewDetail.setLastYearBillAmount( getLastYearBillAmount( childBudgetCode ));
                    budgetCodeViewDetailsList.add( budgetCodeViewDetail );
                }

                map.put( budgetCode.getId() , budgetCodeViewDetailsList );

            }
        }
        return map;
    }

    public List<BudgetCode> findBudgetCodesByIsLeaf(Boolean isLeaf){
        return budgetCodeRepository.findBudgetCodesByIsLeaf(isLeaf);
    }

    @Transactional
    public void save( BudgetCodeAddRequest budgetCodeAddRequest ) {

        BudgetCode budgetCode = new BudgetCode();

        if( budgetCodeAddRequest.getId() != null ){

            budgetCode = findById( budgetCodeAddRequest.getId() ).get();
        }

        budgetCode = getBudgetCodeFromBudgetCodeAddRequest( budgetCode, budgetCodeAddRequest );

        save( budgetCode );
    }

    private BudgetCode getBudgetCodeFromBudgetCodeAddRequest( BudgetCode budgetCode, BudgetCodeAddRequest budgetCodeAddRequest ) {

        budgetCode.setCode( budgetCodeAddRequest.getCode() );
        budgetCode.setNameBn( budgetCodeAddRequest.getNameBn() );
        budgetCode.setNameEn( budgetCodeAddRequest.getNameEn() );
        budgetCode.setDescriptionBn( budgetCodeAddRequest.getDescriptionBn() );
        budgetCode.setDescriptionEn( budgetCodeAddRequest.getDescriptionEn() );

        if( budgetCodeAddRequest.getParentCodeId() != null ) {

            BudgetCode parentBudgetCode = findById(budgetCodeAddRequest.getParentCodeId()).get();
            budgetCode.setParentBudgetCode(parentBudgetCode);
        }

        budgetCode.setIsLeaf( false );

        return budgetCode;
    }

    private double getLastYearBillAmount(BudgetCode budgetCode) {

        int year = Calendar.getInstance().get(Calendar.YEAR);
        List<BudgetUsed> budgetUsedList = budgetUsedService.findByBudgetCodeAndFromYearAndToYear( budgetCode, year-1, year);

        if(budgetUsedList.size()>0) {

            return budgetUsedList.get(0).getUsedAmount();
        }
        return 0;
    }

    public BudgetCode findReference( Long budgetCodeId ) {

        return budgetCodeRepository.getById( budgetCodeId );
    }

    public BudgetCodeViewResponse getBudgetCodeViewResponseFromBudgetCodeId( Long budgetCodeId ) {

        BudgetCode budgetCode = findById( budgetCodeId ).get();

        BudgetCodeViewResponse budgetCodeViewResponse = new BudgetCodeViewResponse();

        budgetCodeViewResponse.setId( budgetCode.getId() );
        budgetCodeViewResponse.setCode( budgetCode.getCode() );
        budgetCodeViewResponse.setNameEn( budgetCode.getNameEn() + " ( " + budgetCode.getCode() + " ) " );
        budgetCodeViewResponse.setNameBn( budgetCode.getNameBn() + " ( " + budgetCode.getCode() + " ) " );

        return budgetCodeViewResponse;
    }

    public List<Long> findParentBudgetCodeByBudgetCodeId( List<Long> budgetCodeList, Long id ) {

        BudgetCode budgetCode = findById( id ).get();

        if( budgetCode.getParentBudgetCode() == null ){

            return budgetCodeList;
        }

        findParentBudgetCodeByBudgetCodeId( budgetCodeList, budgetCode.getParentBudgetCode().getId() );
        budgetCodeList.add( budgetCode.getParentBudgetCode().getId() );
        return budgetCodeList;
    }
}
