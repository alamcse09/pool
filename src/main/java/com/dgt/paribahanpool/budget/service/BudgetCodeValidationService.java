package com.dgt.paribahanpool.budget.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.budget.model.BudgetCodeAddRequest;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.meeting.model.MeetingAddRequest;
import com.dgt.paribahanpool.meeting.service.MeetingService;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BudgetCodeValidationService {

    private final MvcUtil mvcUtil;
    private final MeetingService meetingService;


    public Boolean handleBindingResultForAddFormPost( Model model, BindingResult bindingResult, BudgetCodeAddRequest budgetCodeAddRequest ) {

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "budgetCodeAddRequest" )
                        .object( budgetCodeAddRequest )
                        .title( "title.budget.code-add" )
                        .content( "budget/budget-code-add" )
                        .activeMenu( Menu.BUDGET_CODE_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.BUDGET_CODE_ADD } )
                        .build()
        );
    }
}
