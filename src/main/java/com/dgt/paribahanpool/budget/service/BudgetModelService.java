package com.dgt.paribahanpool.budget.service;

import com.dgt.paribahanpool.budget.model.*;
import com.dgt.paribahanpool.enums.BudgetStateType;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.locale.LocalizeUtil;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BudgetModelService {

    private final BudgetService budgetService;
    private final MvcUtil mvcUtil;

    @Autowired
    private LocalizeUtil localizeUtil;

    public void addBudget( Model model, HttpServletRequest request ) {

        BudgetAddRequest budgetAddRequest = budgetService.getBudgetAddRequest();

        model.addAttribute( "activeMenu", Menu.BUDGET_ADD );

        addPageTitle( model, null, request );

        model.addAttribute( "budgetAddRequest", budgetAddRequest );
    }

    public void addBudgetPost( BudgetAddRequest budgetAddRequest, RedirectAttributes redirectAttributes, BudgetStateType stateType ) {

        budgetService.save( budgetAddRequest, stateType );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.budget.add" );
    }

    public void view( Model model, Long id ) {

        BudgetViewResponse budgetViewResponse = budgetService.getBudgetViewResponseFromBudget( id );
        model.addAttribute( "budgetViewResponse", budgetViewResponse );
    }

    @Transactional
    public void editBudget(Model model, Long budgetId, Menu activeMenu, HttpServletRequest request) {

        Budget budget = budgetService.findById( budgetId ).get();

        BudgetAddRequest budgetAddRequest = budgetService.getBudgetAddRequestFromBudget( budget );

        List<BudgetDetailsViewResponse> budgetDetailsViewResponseList = budgetService.getBudgetDetailsViewResponseListFromYearAndOfficeId( budget.getFrom(), budget.getBudgetOffice().getId(), budget.getBudgetDetailsList() );

        model.addAttribute( "budgetAddRequest", budgetAddRequest );

        model.addAttribute( "budgetDetailsViewResponseList", budgetDetailsViewResponseList );

        model.addAttribute( "stateType", budgetAddRequest.getStateType() );

        model.addAttribute( "activeMenu", activeMenu );

        addPageTitle( model, budgetAddRequest.getStateType() , request );

    }

    private void addPageTitle( Model model, BudgetStateType budgetStateType, HttpServletRequest request ) {

        String key = new String();

        if( budgetStateType == null ){

            key = "title.budget.add";
        }

        else if( budgetStateType == BudgetStateType.INITIAL_BUDGET ){

            key = "title.budget.add.revised";
        }

        else if( budgetStateType == BudgetStateType.REVISED_BUDGET || budgetStateType == BudgetStateType.FINAL_BUDGET ){

            key = "title.budget.add.final";
        }

        model.addAttribute( "pageTitle", key );
    }

    public void addPageTitleOnSearchPage( Model model, BudgetStateType budgetStateType, HttpServletRequest request ) {

        String key = new String();

        if( budgetStateType == BudgetStateType.INITIAL_BUDGET ){

            key = "title.budget.search";
        }

        if( budgetStateType == BudgetStateType.REVISED_BUDGET ){

            key = "title.budget.add.revised.search";
        }

        if( budgetStateType == BudgetStateType.FINAL_BUDGET ){

            key = "title.budget.add.final.search";
        }

        model.addAttribute( "pageTitle", key );
    }
}

