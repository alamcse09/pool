package com.dgt.paribahanpool.budget.service;

import com.dgt.paribahanpool.budget.model.BudgetOffice;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface BudgetOfficeRepository extends DataTablesRepository<BudgetOffice, Long> {
}
