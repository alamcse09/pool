package com.dgt.paribahanpool.budget.service;


import com.dgt.paribahanpool.budget.model.BudgetOffice;
import com.dgt.paribahanpool.budget.model.BudgetOfficeViewResponse;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BudgetOfficeService {

    private final BudgetOfficeRepository budgetOfficeRepository;


    public Iterable<BudgetOffice> findAll() {

        return budgetOfficeRepository.findAll();
    }

    public Optional<BudgetOffice> findById( Long officeId ) {

        return budgetOfficeRepository.findById( officeId );
    }

    public List<BudgetOfficeViewResponse> getBudgetOfficeViewResponseList(){

        Iterable<BudgetOffice> budgetOfficeIterable = findAll();

        List<BudgetOffice> budgetOfficeList = Lists.newArrayList( budgetOfficeIterable );

        return budgetOfficeList
                .stream()
                .map( this::getBudgetOfficeViewResponseFromBudgetOffice )
                .collect( Collectors.toList() );
    }

    public BudgetOfficeViewResponse getBudgetOfficeViewResponseFromBudgetOffice( BudgetOffice budgetOffice ) {

        return BudgetOfficeViewResponse.builder()
                .id( budgetOffice.getId() )
                .nameEn( budgetOffice.getNameEn() )
                .nameBn(budgetOffice.getNameBn() )
                .build();
    }
}
