package com.dgt.paribahanpool.budget.service;

import com.dgt.paribahanpool.budget.model.Budget;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface BudgetRepository extends DataTablesRepository<Budget, Long> {
}
