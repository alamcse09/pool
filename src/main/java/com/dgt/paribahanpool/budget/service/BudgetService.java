package com.dgt.paribahanpool.budget.service;

import com.dgt.paribahanpool.budget.model.*;
import com.dgt.paribahanpool.enums.BudgetStateType;
import com.dgt.paribahanpool.excel.model.SheetCell;
import com.dgt.paribahanpool.util.Util;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.dgt.paribahanpool.budget.service.BudgetSpecification.*;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BudgetService {

    private final BudgetCodeService budgetCodeService;
    private final BudgetOfficeService budgetOfficeService;
    private final BudgetRepository budgetRepository;
    private final Gson gson;

    public Budget save( Budget budget ){

        return budgetRepository.save( budget );
    }

    @Transactional
    public void save( BudgetAddRequest budgetAddRequest, BudgetStateType stateType ) {

        Budget budget = new Budget();

        if( budgetAddRequest.getId() != null ){

            budget = findById( budgetAddRequest.getId() ).get();
            budget = setDetailListAndStateFromBudgetAddRequest( budget, budgetAddRequest, stateType );
        }

        else {

            budget = getBudgetFromBudgetAddRequest( budgetAddRequest, stateType);
        }

        save( budget );
    }

    private Budget getBudgetFromBudgetAddRequest( BudgetAddRequest budgetAddRequest, BudgetStateType stateType ) {

        BudgetOffice budgetOffice = budgetOfficeService.findById( budgetAddRequest.getOfficeId() ).get();

        Budget budget = new Budget();

        budget = setDetailListAndStateFromBudgetAddRequest( budget, budgetAddRequest, stateType );

        budget.setBudgetOffice( budgetOffice );
        budget.setFrom( budgetAddRequest.getFrom() );
        budget.setTo( budgetAddRequest.getTo() );

        return budget;
    }

    private Budget setDetailListAndStateFromBudgetAddRequest( Budget budget, BudgetAddRequest budgetAddRequest, BudgetStateType stateType ) {

        List<Map<String,String>> budgetDetailsAddRequestMapList = gson.fromJson( budgetAddRequest.getBudgetDetailsAddRequestList(), new TypeToken<List<Map<String,String>>>(){}.getType() );

        List<BudgetDetails> budgetDetailsListFromRequest = Collections.EMPTY_LIST;
        List<BudgetDetails> budgetDetailsList =  budget.getBudgetDetailsList();

        if( budgetDetailsAddRequestMapList != null ) {

            budgetDetailsListFromRequest = budgetDetailsAddRequestMapList.stream()
                    .map(
                            codeAmountMap -> {

                                BudgetDetails budgetDetails = new BudgetDetails();

                                if( budgetDetailsList != null ){

                                    Optional<BudgetDetails> budgetDetailsOptional = budgetDetailsList.stream().filter( bd -> bd.getBudgetCode().getId().equals( Long.valueOf( codeAmountMap.get( "budgetCodeId" ) ) ) ).findFirst();
                                    if( budgetDetailsOptional.isPresent() ){

                                        budgetDetails = budgetDetailsOptional.get();
                                        budgetDetailsList.remove( budgetDetails );
                                    }
                                }

                                BudgetCode budgetCode = budgetCodeService.findReference(Long.parseLong(codeAmountMap.get("budgetCodeId")));

                                budgetDetails.setBudgetCode(budgetCode);

                                if( stateType == null){

                                    budgetDetails.setInitialAmount( Integer.parseInt(codeAmountMap.get("amount")) );
                                }
                                else if ( stateType == BudgetStateType.INITIAL_BUDGET ) {

                                    budgetDetails.setRevisedAmount(Integer.parseInt(codeAmountMap.get("amount")));

                                    if( codeAmountMap.get("estimate") != null ) {

                                        budgetDetails.setEstimate( Integer.parseInt(codeAmountMap.get("estimate")) );
                                    }

                                    if( codeAmountMap.get("projectionOfNextYear") != null ) {

                                        budgetDetails.setEstimate( Integer.parseInt(codeAmountMap.get("projectionOfNextYear")) );
                                    }

                                    if( codeAmountMap.get("projectionOfNextTwoYear") != null ) {

                                        budgetDetails.setEstimate( Integer.parseInt(codeAmountMap.get("projectionOfNextTwoYear")) );
                                    }
                                } else if ( stateType == BudgetStateType.REVISED_BUDGET ) {

                                    budgetDetails.setFinalAmount(Integer.parseInt(codeAmountMap.get("amount")));

                                    if( codeAmountMap.get("estimate") != null ) {

                                        budgetDetails.setEstimate( Integer.parseInt(codeAmountMap.get("estimate")) );
                                    }

                                    if( codeAmountMap.get("projectionOfNextYear") != null ) {

                                        budgetDetails.setEstimate( Integer.parseInt(codeAmountMap.get("projectionOfNextYear")) );
                                    }

                                    if( codeAmountMap.get("projectionOfNextTwoYear") != null ) {

                                        budgetDetails.setEstimate( Integer.parseInt(codeAmountMap.get("projectionOfNextTwoYear")) );
                                    }
                                }

                                return budgetDetails;
                            })
                    .collect(Collectors.toList());
        }

        budget.getBudgetDetailsList().addAll( budgetDetailsListFromRequest );

        if( budget.getStateType() == null ){

            budget.setStateType( BudgetStateType.INITIAL_BUDGET );
        }
        else if( budget.getStateType() == BudgetStateType.INITIAL_BUDGET ){

            budget.setStateType( BudgetStateType.REVISED_BUDGET );
        }
        else if( budget.getStateType() == BudgetStateType.REVISED_BUDGET ){

            budget.setStateType( BudgetStateType.FINAL_BUDGET );
        }

        return budget;
    }

    private BudgetDetails getBudgetDetailsFromBudgetDetailsAddRequest( BudgetDetailsAddRequest budgetDetailsAddRequest, Budget budget ) {

        BudgetDetails budgetDetails = new BudgetDetails();

        BudgetCode budgetCode = budgetCodeService.findById( budgetDetailsAddRequest.getBudgetCodeId() ).get();

        budgetDetails.setBudgetCode( budgetCode );
        budgetDetails.setInitialAmount( budgetDetailsAddRequest.getInitialAmount() );

        return budgetDetails;
    }

    public Optional<Budget> findById(Long id ) {

        return budgetRepository.findById( id );
    }

    public BudgetAddRequest getBudgetAddRequest() {

        BudgetAddRequest budgetAddRequest = new BudgetAddRequest();

        List<BudgetOfficeViewResponse> budgetOfficeList = budgetOfficeService.getBudgetOfficeViewResponseList();

        List<BudgetCodeViewResponse> budgetCodeViewResponseList = budgetCodeService.getBudgetCodeViewResponseList();

        budgetAddRequest.setOfficeViewResponseList( budgetOfficeList );

        budgetAddRequest.setBudgetCodeViewResponseList( budgetCodeViewResponseList );

        List<String> budgetYearList = new ArrayList<>();
        Integer thisYear = LocalDate.now().getYear();
        for (int k=thisYear-5;k<thisYear+5;k++){
            budgetYearList.add(k+"-"+(k+1));
        }
        budgetAddRequest.setBudgetYearList(budgetYearList);

        return budgetAddRequest;
    }

    public DataTablesOutput<BudgetSearchResponse> searchForDatatable( DataTablesInput dataTablesInput, BudgetStateType budgetStateType ) {

        Specification<Budget> filterByStateType = filterByStateType( budgetStateType );

        return budgetRepository.findAll( dataTablesInput, null, filterByStateType, this::getBudgetSearchResponseFromBudget );
    }

    public BudgetSearchResponse getBudgetSearchResponseFromBudget( Budget budget ) {

        BudgetSearchResponse budgetSearchResponse = new BudgetSearchResponse();

        BudgetOfficeViewResponse budgetOfficeViewResponse = BudgetOfficeViewResponse.builder()
                .nameEn( budget.getBudgetOffice().getNameEn() )
                .nameBn( budget.getBudgetOffice().getNameBn() )
                .build();

        budgetSearchResponse.setId( budget.getId() );
        budgetSearchResponse.setBudgetOffice( budgetOfficeViewResponse );
        budgetSearchResponse.setTo( budget.getTo() );
        budgetSearchResponse.setStateType( budget.getStateType() );
        budgetSearchResponse.setFrom( budget.getFrom() );

        return budgetSearchResponse;
    }

    public Boolean existsById( Long id ) {

        return budgetRepository.existsById( id );
    }

    public void delete( Long id ) {

        budgetRepository.deleteById( id );
    }

    @Transactional
    public BudgetViewResponse getBudgetViewResponseFromBudget( Long id ) {

        Budget budget = findById( id ).get();

        BudgetViewResponse budgetViewResponse = BudgetViewResponse.builder()
                .id( budget.getId() )
                .officeNameBn( budget.getBudgetOffice().getNameBn() )
                .to( budget.getTo() )
                .from( budget.getFrom() )
                .budgetDetailsAddRequestList(
                        budget.getBudgetDetailsList().stream()
                        .map(
                                budgetDetails ->
                                        BudgetDetailsViewResponse.builder()
                                .id( budgetDetails.getId() )
                                .code( Util.makeDigitBangla( budgetDetails.getBudgetCode().getCode() ) )
                                .codeNameBn( budgetDetails.getBudgetCode().getNameBn() )
                                .codeNameEn( budgetDetails.getBudgetCode().getNameEn() )
                                .initialAmount( budgetDetails.getInitialAmount() )
                                .revisedAmount( budgetDetails.getRevisedAmount() )
                                .finalAmount( budgetDetails.getFinalAmount() )
                                .build()
                        )
                        .collect( Collectors.toList() )
                )
                .build();

        return budgetViewResponse;
    }

    @Transactional
    public BudgetAddRequest getBudgetAddRequestFromBudget( Budget budget ) {

        BudgetAddRequest budgetAddRequest = getBudgetAddRequest();

        budgetAddRequest.setId( budget.getId() );
        budgetAddRequest.setOfficeId( budget.getBudgetOffice().getId() );
        budgetAddRequest.setFrom( Integer.parseInt( Util.makeDigitBangla( ( String.valueOf( budget.getFrom() ) ) ) ) );
        budgetAddRequest.setTo( budget.getTo() );
        budgetAddRequest.setStateType( budget.getStateType() );
        budgetAddRequest.setFromMinuesOne( Integer.parseInt( Util.makeDigitBangla( String.valueOf( budget.getFrom() - 1 ) ) ) );
        budgetAddRequest.setFromMinuesTwo( Integer.parseInt( Util.makeDigitBangla( String.valueOf( budget.getFrom() - 2 ) ) ) );

        List<BudgetDetailsAddRequest> budgetDetailsAddRequestList =
                budget.getBudgetDetailsList()
                .stream()
                .map( budgetDetails -> getBudgetDetailsAddRequestsFromBudgetDetails( budgetDetails ) )
                .collect( Collectors.toList() );

        budgetAddRequest.setBudgetDetailsAddRequests( budgetDetailsAddRequestList );

        List<BudgetCodeViewResponse> budgetCodeViewResponseList = new ArrayList<>();

        for( BudgetDetailsAddRequest budgetDetailsAddRequest: budgetDetailsAddRequestList ){

            BudgetCodeViewResponse budgetCodeViewResponse = budgetCodeService.getBudgetCodeViewResponseFromBudgetCodeId( budgetDetailsAddRequest.getBudgetCodeId() );
            budgetCodeViewResponseList.add( budgetCodeViewResponse );
        }

        budgetAddRequest.setBudgetCodeViewResponseList( budgetCodeViewResponseList );

        return budgetAddRequest;
    }

    public List<BudgetDetailsViewResponse> getBudgetDetailsViewResponseListFromYearAndOfficeId( Integer from, Long officeId, List<BudgetDetails> budgetDetailsList ) {

        List<Budget> budgetListForMinusZero = budgetRepository.findAll( findByFrom( from ).and( findByOfficeId( officeId ) )  );
        List<Budget> budgetListForMinusOne = budgetRepository.findAll( findByFrom( from - 1 ).and( findByOfficeId( officeId ) ) );
        List<Budget> budgetListForMinusTwo = budgetRepository.findAll( findByFrom( from - 2 ).and( findByOfficeId( officeId ) )  );

        Budget budgetListForFromMinusZero = new Budget();
        Budget budgetListForFromMinusOne = new Budget();
        Budget budgetListForFromMinusTwo = new Budget();

        if( budgetListForMinusZero.size() > 0 ) {

            budgetListForFromMinusZero = budgetListForMinusZero.get(0);
        }

        if( budgetListForMinusOne.size() > 0 ) {

            budgetListForFromMinusOne = budgetListForMinusOne.get(0);
        }

        if( budgetListForMinusTwo.size() > 0 ) {

            budgetListForFromMinusTwo = budgetListForMinusTwo.get(0);
        }
        List<BudgetDetails> budgetDetailsListForFromMinusZero = new ArrayList<>();
        List<BudgetDetails> budgetDetailsListForFromMinusOne = new ArrayList<>();
        List<BudgetDetails> budgetDetailsListForFromMinusTwo = new ArrayList<>();

        for( BudgetDetails budgetDetails: budgetListForFromMinusZero.getBudgetDetailsList() ){

            budgetDetailsListForFromMinusZero.add( budgetDetails );
        }

        for( BudgetDetails budgetDetails: budgetListForFromMinusOne.getBudgetDetailsList() ){

            budgetDetailsListForFromMinusOne.add( budgetDetails );
        }

        for( BudgetDetails budgetDetails: budgetListForFromMinusTwo.getBudgetDetailsList() ){

            budgetDetailsListForFromMinusTwo.add( budgetDetails );
        }

        List<BudgetDetailsViewResponse> budgetDetailsViewResponseList = new ArrayList<>();

        for( BudgetDetails budgetDetails : budgetDetailsList ){

            BudgetDetailsViewResponse budgetDetailsViewResponse = BudgetDetailsViewResponse.builder()
                    .id( budgetDetails.getId())
                    .code( budgetDetails.getBudgetCode().getCode() )
                    .codeNameBn( budgetDetails.getBudgetCode().getNameBn() )
                    .codeNameEn( budgetDetails.getBudgetCode().getNameEn() )
                    .finalAmount( budgetDetails.getFinalAmount() )
                    .estimate( budgetDetails.getEstimate() )
                    .projectionOfNextYear( budgetDetails.getProjectionOfNextYear() )
                    .projectionOfNextTwoYear( budgetDetails.getProjectionOfNextTwoYear() )
                    .isLeaf( budgetDetails.getBudgetCode().getIsLeaf() )
                    .build();

            budgetDetailsViewResponseList.add( budgetDetailsViewResponse );

        }

        for( BudgetDetails budgetDetails: budgetDetailsListForFromMinusZero ){

            Long id = budgetDetails.getId();

           for( BudgetDetailsViewResponse budgetDetailsViewResponse : budgetDetailsViewResponseList ){

               if( budgetDetailsViewResponse.getId().equals( id ) ){

                   budgetDetailsViewResponse.setInitialAmountForFromMinusZero( budgetDetails.getUsedAmount() );
               }
           }
        }

        for( BudgetDetails budgetDetails: budgetDetailsListForFromMinusOne ){

            Long id = budgetDetails.getId();

            for( BudgetDetailsViewResponse budgetDetailsViewResponse : budgetDetailsViewResponseList ){

                if( budgetDetailsViewResponse.getId().equals( id ) ){

                    budgetDetailsViewResponse.setInitialAmountForFromMinusOne( budgetDetails.getUsedAmount() );
                }
            }
        }

        for( BudgetDetails budgetDetails: budgetDetailsListForFromMinusTwo ){

            Long id = budgetDetails.getId();

            for( BudgetDetailsViewResponse budgetDetailsViewResponse : budgetDetailsViewResponseList ){

                if( budgetDetailsViewResponse.getId().equals( id ) ){

                    budgetDetailsViewResponse.setInitialAmountForFromMinusTwo( budgetDetails.getUsedAmount() );
                }
            }
        }

        return budgetDetailsViewResponseList;
    }

    private BudgetDetailsAddRequest getBudgetDetailsAddRequestsFromBudgetDetails( BudgetDetails budgetDetails ) {

        BudgetDetailsAddRequest budgetDetailsAddRequest = new BudgetDetailsAddRequest();

        budgetDetailsAddRequest.setId( budgetDetails.getId() );
        budgetDetailsAddRequest.setBudgetCodeId( budgetDetails.getBudgetCode().getId() );
        budgetDetailsAddRequest.setInitialAmount( budgetDetails.getInitialAmount() );
        budgetDetailsAddRequest.setRevisedAmount( budgetDetails.getRevisedAmount() );
        budgetDetailsAddRequest.setFinalAmount( budgetDetails.getFinalAmount() );
        budgetDetailsAddRequest.setEstimate( budgetDetails.getEstimate() );
        budgetDetailsAddRequest.setProjectionOfNextYear( budgetDetails.getProjectionOfNextYear() );
        budgetDetailsAddRequest.setProjectionOfNextTwoYear( budgetDetails.getProjectionOfNextTwoYear() );

        return budgetDetailsAddRequest;
    }

    @Transactional
    public Map<String,List<SheetCell>> getSheetDataByBudgetId( Long id ) {

        Budget budget = findById( id ).get();

        List<BudgetDetailsViewResponse> budgetDetailsViewResponseOnlyLeafList = getBudgetDetailsViewResponseListFromYearAndOfficeId( budget.getFrom(), budget.getBudgetOffice().getId(), budget.getBudgetDetailsList() );

        List<BudgetDetailsViewResponse> budgetDetailsViewResponseList = getWithParentBudgetDetailsViewResponseListFromBudgetDetailsViewResponseOnlyLeafList( budgetDetailsViewResponseOnlyLeafList, budget );

        String budgetOfficeName = budget.getBudgetOffice().getNameBn();

        Map<String,List<SheetCell>> excelData = new HashMap<>();

        Integer colNumber = budgetDetailsViewResponseList.get(0).getClass().getFields().length;
        Integer rowNumber = budgetDetailsViewResponseList.size();

        String fromBn = Util.makeDigitBangla( budget.getFrom().toString() );
        String toBn = Util.makeDigitBangla( budget.getTo().toString() );

        String fromMinusTwoBn = Util.makeDigitBangla(  (budget.getFrom() - 2)  + "" );
        String toMinusTwoBn = Util.makeDigitBangla(  (budget.getFrom() - 2) + "" );

        String fromMinusOneBn = Util.makeDigitBangla(  (budget.getFrom() - 1) + "" );
        String toMinusOneBn = Util.makeDigitBangla(  (budget.getFrom() - 1) + "" );


        String fromToColumnName = fromBn + "-" + toBn + " অর্থ বছরের বাজেট";

        String[][] sheetData = new String[rowNumber+2][colNumber+1];

        List<SheetCell> sheetCells = new ArrayList<>();

        Integer rowCount = 1;

        sheetCells.add( new SheetCell( budgetOfficeName, rowCount, 1, 1, 5 ) );

        rowCount += 3;

        sheetCells.add( new SheetCell( "", rowCount, 1, 1, 4 ) );
        sheetCells.add( new SheetCell( "( অংক সমূহ হাজার টাকায় )", rowCount, 5, 1, 1 ) );

        rowCount += 1;

        SheetCell colNameCell1 = new SheetCell( "প্রতিষ্ঠানের নাম", rowCount, 1, 1, 1 );
        SheetCell colNameCell2 = new SheetCell( "অর্থনৈতিক কোড ও বিবরণ", rowCount, 2, 1, 1 );
        SheetCell colNameCell3 = new SheetCell( "প্রকৃত ব্যয়", rowCount, 3, 1, 2 );
        SheetCell colNameCell4 = new SheetCell( fromToColumnName, rowCount, 5, 1, 1 );

        sheetCells.add( colNameCell1 );
        sheetCells.add( colNameCell2 );
        sheetCells.add( colNameCell3 );
        sheetCells.add( colNameCell4 );

        rowCount += 1;

        sheetCells.add( new SheetCell( budgetOfficeName, rowCount, 1, 1, 1 ) );
        sheetCells.add( new SheetCell( fromMinusTwoBn + "-" + toMinusTwoBn, rowCount, 3, 1, 1 ) );
        sheetCells.add( new SheetCell( fromMinusOneBn + "-" + toMinusOneBn, rowCount, 4, 1, 1 ) );

        rowCount += 1;


        for( Integer row = rowCount; row < rowNumber+rowCount; row++ ){

            BudgetDetailsViewResponse budgetDetailsViewResponse = budgetDetailsViewResponseList.get( row-rowCount );

            Integer col = 2;

            if( budgetDetailsViewResponse.getIsLeaf() == false ) {

                sheetCells.add( new SheetCell( Util.makeDigitBangla( budgetDetailsViewResponse.getCode() ), row, col-1, 1, 1 ) );
                sheetCells.add( new SheetCell( budgetDetailsViewResponse.getCodeNameBn(), row, col, 1, 1 ) );
            }

            else {

                sheetCells.add(new SheetCell( Util.makeDigitBangla( budgetDetailsViewResponse.getCode() ) + "-" + budgetDetailsViewResponse.getCodeNameBn(), row, col++, 1, 1));
                sheetCells.add(new SheetCell( budgetDetailsViewResponse.getInitialAmountForFromMinusTwo() == null ? "N/A" : budgetDetailsViewResponse.getInitialAmountForFromMinusTwo().toString(), row, col++, 1, 1));
                sheetCells.add(new SheetCell( budgetDetailsViewResponse.getInitialAmountForFromMinusOne() == null ? "N/A" : budgetDetailsViewResponse.getInitialAmountForFromMinusOne().toString() , row, col++, 1, 1));
                sheetCells.add(new SheetCell( budgetDetailsViewResponse.getFinalAmount() == null ? "N/A" : budgetDetailsViewResponse.getFinalAmount().toString(), row, col++, 1, 1));
            }
        }

        excelData.put( "budget_details_office_name", sheetCells );

        return excelData;
    }

    private List<BudgetDetailsViewResponse> getWithParentBudgetDetailsViewResponseListFromBudgetDetailsViewResponseOnlyLeafList( List<BudgetDetailsViewResponse> budgetDetailsViewResponseOnlyLeafList, Budget budget ) {

        List<BudgetDetailsViewResponse> budgetDetailsViewResponseList = new ArrayList<>();
        List<Long> previousParentBudgetCodeList = new ArrayList<>();

        Integer index = 0;

        for( BudgetDetailsViewResponse budgetDetailsViewResponse : budgetDetailsViewResponseOnlyLeafList ){

            List<Long> parentBudgetCodeList = budgetCodeService.findParentBudgetCodeByBudgetCodeId( new ArrayList<>(), budget.getBudgetDetailsList().get( index ).getBudgetCode().getId() );

            List<Long> removedParentBudgetCodeList = (List<Long>) CollectionUtils.subtract( parentBudgetCodeList, previousParentBudgetCodeList );

            for( Long budgetCodeId : removedParentBudgetCodeList ){

                BudgetCode budgetCode = budgetCodeService.findById( budgetCodeId ).get();

                BudgetDetailsViewResponse parentBudgetDetailsViewResponse = BudgetDetailsViewResponse.builder()
                        .code( budgetCode.getCode() )
                        .codeNameBn( budgetCode.getNameBn() )
                        .codeNameEn( budgetCode.getNameEn() )
                        .isLeaf( false )
                        .build();

                budgetDetailsViewResponseList.add( parentBudgetDetailsViewResponse );
            }

            previousParentBudgetCodeList = parentBudgetCodeList;

            budgetDetailsViewResponseList.add( budgetDetailsViewResponse );

            index+= 1;
        }

        return budgetDetailsViewResponseList;
    }
}
