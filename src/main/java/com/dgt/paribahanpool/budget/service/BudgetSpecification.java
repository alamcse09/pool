package com.dgt.paribahanpool.budget.service;

import com.dgt.paribahanpool.budget.model.Budget;
import com.dgt.paribahanpool.budget.model.BudgetOffice_;
import com.dgt.paribahanpool.budget.model.Budget_;
import com.dgt.paribahanpool.enums.BudgetStateType;
import org.springframework.data.jpa.domain.Specification;

public class BudgetSpecification {

    public static Specification<Budget> filterByStateType( BudgetStateType stateType ){

        if( stateType == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(Budget_.STATE_TYPE), stateType );
    }

    public static Specification<Budget> findByFrom( Integer from ){

        if( from == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(Budget_.FROM), from );
    }

    public static Specification<Budget> findByOfficeId( Long officeId ){

        if( officeId == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Budget_.BUDGET_OFFICE ).get( BudgetOffice_.ID ), officeId );
    }
}
