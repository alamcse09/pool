package com.dgt.paribahanpool.budget.service;

import com.dgt.paribahanpool.budget.model.BudgetCode;
import com.dgt.paribahanpool.budget.model.BudgetUsed;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BudgetUsedRepository extends JpaRepository<BudgetUsed, Long> {

    List<BudgetUsed> findBudgetUsedByBudgetCodeAndFromYearAndToYear(BudgetCode budgetCode, Integer fromYear, Integer toYear );

}
