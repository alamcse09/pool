package com.dgt.paribahanpool.budget.service;

import com.dgt.paribahanpool.budget.model.BudgetCode;
import com.dgt.paribahanpool.budget.model.BudgetUsed;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BudgetUsedService {
    
    private final BudgetUsedRepository budgetUsedRepository;
    
    public BudgetUsed save( BudgetUsed budgetUsed ) {
        
        return  budgetUsedRepository.save( budgetUsed );
    }
    
    public Optional<BudgetUsed> findById(Long id ) {
        
        return budgetUsedRepository.findById( id );
    }
    
    public List<BudgetUsed> findByBudgetCodeAndFromYearAndToYear(BudgetCode budgetCode, Integer fromYear, Integer toYear) {
        
         return budgetUsedRepository.findBudgetUsedByBudgetCodeAndFromYearAndToYear(budgetCode, fromYear, toYear);
    }
    
}
