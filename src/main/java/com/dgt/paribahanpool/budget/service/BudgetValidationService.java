package com.dgt.paribahanpool.budget.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.budget.model.Budget;
import com.dgt.paribahanpool.budget.model.BudgetAddRequest;
import com.dgt.paribahanpool.budget.model.BudgetCodeAddRequest;
import com.dgt.paribahanpool.enums.BudgetStateType;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.util.MvcUtil;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BudgetValidationService {

    private final MvcUtil mvcUtil;
    private final BudgetService budgetService;
    private final Gson gson;

    public Boolean handleBindingResultForAddFormPost( Model model, BindingResult bindingResult, BudgetAddRequest budgetAddRequest ) {

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "budgetAddRequest" )
                        .object( budgetAddRequest )
                        .title( "title.budget.add" )
                        .content( "budget/budget-add" )
                        .activeMenu( Menu.BUDGET_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.BUDGET_ADD } )
                        .build()
        );
    }

    public Boolean addBudgetPost( RedirectAttributes redirectAttributes, BudgetAddRequest budgetAddRequest, BudgetStateType budgetStateType ) {

        Integer from = budgetAddRequest.getFrom();
        Integer to = budgetAddRequest.getTo();

        if( to <= from || (to - from) > 1 ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.budget.not-valid-budget-year" );
            return false;
        }

        List<Map<String,String>> budgetDetailsAddRequestMapList = gson.fromJson( budgetAddRequest.getBudgetDetailsAddRequestList(), new TypeToken<List<Map<String,String>>>(){}.getType() );

        if( budgetDetailsAddRequestMapList != null ) {

            budgetDetailsAddRequestMapList.stream()
                    .map(
                            codeAmountMap -> {

                                if( Double.parseDouble(codeAmountMap.get("amount") ) < 0 ){

                                    mvcUtil.addErrorMessage( redirectAttributes, "validation.budget.not-valid-amount" );
                                    return false;
                                }
                                else if( codeAmountMap.get("estimate") != null && Double.parseDouble(codeAmountMap.get("estimate")) < 0 ){

                                    mvcUtil.addErrorMessage( redirectAttributes, "validation.budget.not-valid-estimate" );
                                    return false;
                                }
                                else if( codeAmountMap.get("projectionOfNextYear") != null && Double.parseDouble(codeAmountMap.get("projectionOfNextYear")) < 0 ){

                                    mvcUtil.addErrorMessage( redirectAttributes, "validation.budget.not-valid-projection" );
                                    return false;
                                }
                                else if( codeAmountMap.get("projectionOfNextTwoYear") != null && Double.parseDouble(codeAmountMap.get("projectionOfNextTwoYear")) < 0 ){

                                    mvcUtil.addErrorMessage( redirectAttributes, "validation.budget.not-valid-next-projection" );
                                    return false;
                                }

                                return null;
                            }
                    );
        }

        return true;
    }

    public RestValidationResult delete( Long id ) throws NotFoundException {

        Boolean exist = budgetService.existsById( id );
        if( !exist ){

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean view( RedirectAttributes redirectAttributes, Long id ) {

        Optional<Budget> budgetOptional = budgetService.findById( id );

        if( !budgetOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    public Boolean editGetBudget( RedirectAttributes redirectAttributes, Long budgetId, BudgetStateType stateType ) {

        Optional<Budget> budgetOptional = budgetService.findById( budgetId );

        if( !budgetOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        else if( budgetOptional.get().getStateType() != stateType ){

            mvcUtil.addErrorMessage( redirectAttributes, "budget.edit.wrong-state" );
            return false;
        }

        return true;
    }



    public Boolean editBudget( BudgetAddRequest budgetAddRequest, RedirectAttributes redirectAttributes, Long budgetId, BudgetStateType stateType ) {

        Boolean is_validated = editGetBudget( redirectAttributes, budgetId, stateType );

        List<Map<String,String>> budgetDetailsAddRequestMapList = gson.fromJson( budgetAddRequest.getBudgetDetailsAddRequestList(), new TypeToken<List<Map<String,String>>>(){}.getType() );

        if( budgetDetailsAddRequestMapList != null ) {

            budgetDetailsAddRequestMapList.stream()
                    .map(
                            codeAmountMap -> {

                                if( Double.parseDouble(codeAmountMap.get("amount") ) < 0 ){

                                    mvcUtil.addErrorMessage( redirectAttributes, "validation.budget.not-valid-amount" );
                                    return false;
                                }
                                else if( codeAmountMap.get("estimate") != null && Double.parseDouble(codeAmountMap.get("estimate")) < 0 ){

                                    mvcUtil.addErrorMessage( redirectAttributes, "validation.budget.not-valid-estimate" );
                                    return false;
                                }
                                else if( codeAmountMap.get("projectionOfNextYear") != null && Double.parseDouble(codeAmountMap.get("projectionOfNextYear")) < 0 ){

                                    mvcUtil.addErrorMessage( redirectAttributes, "validation.budget.not-valid-projection" );
                                    return false;
                                }
                                else if( codeAmountMap.get("projectionOfNextTwoYear") != null && Double.parseDouble(codeAmountMap.get("projectionOfNextTwoYear")) < 0 ){

                                    mvcUtil.addErrorMessage( redirectAttributes, "validation.budget.not-valid-next-projection" );
                                    return false;
                                }

                                return null;
                            }
                    );
        }

        return (true || is_validated);
    }
}
