package com.dgt.paribahanpool.budget.service;

import lombok.Data;

import java.time.LocalDate;
import java.time.Month;

@Data
public class BudgetYear {
    private Integer fromYear;
    private Integer toYear;

    public static BudgetYear getBudgetYearFromDate(LocalDate date){
        BudgetYear budgetYear = new BudgetYear();

        if (date.getMonth().getValue() > Month.JUNE.getValue()){
            budgetYear.fromYear = date.getYear();
        }else{
            budgetYear.fromYear = date.getYear()-1;
        }

        if (date.getMonth().getValue() > Month.JUNE.getValue()){
            budgetYear.toYear = date.getYear()+1;
        }else{
            budgetYear.toYear = date.getYear();
        }

        return budgetYear;
    }
}
