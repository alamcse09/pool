package com.dgt.paribahanpool.color.model;

import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "color")
public class Color {

    @Id()
    private Long id;

    @Column( name = "color_name_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String colorNameEn;

    @Column( name = "color_name_bn" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String colorNameBn;
}
