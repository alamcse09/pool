package com.dgt.paribahanpool.color.model;

import lombok.Data;

import javax.persistence.Id;

@Data
public class ColorViewResponse {

    @Id
    private Long id;

    private String colorNameEn;
    private String colorNameBn;
}
