package com.dgt.paribahanpool.color.service;

import com.dgt.paribahanpool.color.model.Color;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface ColorRepository extends DataTablesRepository<Color,Long> {
}
