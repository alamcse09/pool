package com.dgt.paribahanpool.color.service;

import com.dgt.paribahanpool.color.model.Color;
import com.dgt.paribahanpool.color.model.ColorViewResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class ColorService {

    private final ColorRepository colorRepository;

    public List<Color> getAllColors(){
        return (List<Color>) colorRepository.findAll();
    }

    public ColorViewResponse getColorViewResponseFromColor(Color color){
        ColorViewResponse colorViewResponse = new ColorViewResponse();

        colorViewResponse.setId(color.getId());
        colorViewResponse.setColorNameEn(color.getColorNameEn());
        colorViewResponse.setColorNameBn(color.getColorNameBn());

        return colorViewResponse;
    }

    public Optional<Color> getColorById(Long id){
        return colorRepository.findById(id);
    }

    public List<ColorViewResponse> getColorViewResponsesList(){
        List<Color> allColors = getAllColors();
        List<ColorViewResponse> colorViewResponsesList = new ArrayList<>();

        for (Color color:allColors){
            colorViewResponsesList.add(getColorViewResponseFromColor(color));
        }
        return colorViewResponsesList;
    }

}
