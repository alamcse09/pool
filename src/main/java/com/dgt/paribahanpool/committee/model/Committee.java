package com.dgt.paribahanpool.committee.model;

import com.dgt.paribahanpool.enums.CommitteePositionType;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table( name = "committee" )
public class Committee extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @NotNull
    @Column( name = "name" )
    private String name;

    @Column( name = "name_en" )
    private String nameEn;

    @NotNull
    @Column( name = "desination" )
    private String designation;

    @NotNull
    @Column( name = "position_in_committee" )
    private CommitteePositionType positionInCommitte;
}
