package com.dgt.paribahanpool.committee.model;

import com.dgt.paribahanpool.enums.CommitteePositionType;
import lombok.Data;

@Data
public class CommitteeAddRequest {

    private Long id;
    private String nameBn;
    private String nameEn;
    private String designation;
    private CommitteePositionType positionInCommittee;
}
