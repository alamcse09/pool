package com.dgt.paribahanpool.committee.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.workflow.model.WorkflowAdditionalData;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;

@Data
public class CommitteeData implements WorkflowAdditionalData {

    private transient MultipartFile[] committeeDecisionFiles;

    List<DocumentMetadata> documents = Collections.EMPTY_LIST;
}
