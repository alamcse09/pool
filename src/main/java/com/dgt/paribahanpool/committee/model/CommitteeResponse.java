package com.dgt.paribahanpool.committee.model;

import com.dgt.paribahanpool.enums.CommitteePositionType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CommitteeResponse {

    private Long id;
    private String nameEn;
    private String nameBn;
    private String designationName;
    private CommitteePositionType positionInCommittee;
}
