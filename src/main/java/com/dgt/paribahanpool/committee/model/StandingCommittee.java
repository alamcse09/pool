package com.dgt.paribahanpool.committee.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "standing_committee" )
public class StandingCommittee {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "committee_type" )
    private CommitteeType committeeType;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "committee_id", foreignKey = @ForeignKey( name = "fk_standing_committee_committee_id" ) )
    private Committee committeeMember;
}
