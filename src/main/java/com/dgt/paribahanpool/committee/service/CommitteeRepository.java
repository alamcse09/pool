package com.dgt.paribahanpool.committee.service;

import com.dgt.paribahanpool.committee.model.Committee;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface CommitteeRepository extends DataTablesRepository<Committee, Long> {
}
