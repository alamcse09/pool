package com.dgt.paribahanpool.committee.service;

import com.dgt.paribahanpool.committee.model.*;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class CommitteeService {

    private final StandingCommitteeRepository standingCommitteeRepository;
    private final Gson gson;

    public Committee getCommitteeFromCommitteeAddRequest( CommitteeAddRequest committeeAddRequest ) {

        Committee committee = new Committee();

        committee.setName( committeeAddRequest.getNameBn() );
        committee.setNameEn(committeeAddRequest.getNameEn() );
        committee.setDesignation( committeeAddRequest.getDesignation() );
        committee.setPositionInCommitte( committeeAddRequest.getPositionInCommittee() );

        return committee;
    }

    @Transactional
    public List<Committee> findCommitteeListForStandingCommittee( CommitteeType committeeType ){

        List<StandingCommittee> standingCommitteeList = standingCommitteeRepository.findByCommitteeType( committeeType );
        return standingCommitteeList.stream().map( StandingCommittee::getCommitteeMember).collect( Collectors.toList() );
    }

    @Transactional
    public List<CommitteeResponse> findCommitteeResponseListOfStandingCommittee( CommitteeType committeeType ){

        return findCommitteeListForStandingCommittee( committeeType ).stream().map(
                committee -> CommitteeResponse.builder()
                        .id( committee.getId() )
                        .nameEn( committee.getNameEn() )
                        .nameBn( committee.getName() )
                        .designationName( committee.getDesignation() )
                        .positionInCommittee( committee.getPositionInCommitte() )
                        .build()
        )
                .collect(Collectors.toList() );
    }

    @Transactional
    public String getCommitteeResponseListAsJson( CommitteeType committeeType ){

        return gson.toJson( findCommitteeResponseListOfStandingCommittee( committeeType ) );
    }
}
