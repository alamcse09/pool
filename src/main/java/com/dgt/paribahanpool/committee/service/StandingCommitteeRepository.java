package com.dgt.paribahanpool.committee.service;

import com.dgt.paribahanpool.committee.model.CommitteeType;
import com.dgt.paribahanpool.committee.model.StandingCommittee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StandingCommitteeRepository extends JpaRepository<StandingCommittee, Long> {

    List<StandingCommittee> findByCommitteeType(CommitteeType committeeType);
}
