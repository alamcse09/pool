package com.dgt.paribahanpool.complain.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.complain.model.Complains;
import com.dgt.paribahanpool.complain.model.ComplainsAddRequest;
import com.dgt.paribahanpool.complain.model.ComplainsData;
import com.dgt.paribahanpool.complain.service.ComplainsModelService;
import com.dgt.paribahanpool.complain.service.ComplainsValidationService;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.noc_application.model.NocAdditionalActionData;
import com.dgt.paribahanpool.user.model.PublicUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/complains" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class ComplainsController extends MVCController {

    private final ComplainsModelService complainsModelService;
    private final ComplainsValidationService complainsValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.complains.add", content = "complains/complains-add", activeMenu = Menu.COMPLAINS_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.COMPLAINS_ADD} )
    public String add(
            HttpServletRequest httpServletRequest,
            Model model
    ) {

        log.debug( "Rendering Add Complains form" );
        complainsModelService.addComplainsGet( model );
        return viewRoot;
    }

    @PostMapping( "/add" )
    public String postComplainsAdd(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid ComplainsAddRequest complainsAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
            ) {

        if( !complainsValidationService.handleBindingResultForAddFormPost( model, bindingResult, complainsAddRequest ) ) {

            return viewRoot;
        }

        if( complainsValidationService.addComplainsPost( redirectAttributes, complainsAddRequest ) ){

            complainsModelService.addComplainsPost( complainsAddRequest, redirectAttributes, getLoggedInUser().getUser().getId() );
            log( LogEvent.COMPLAIN_ADDED, complainsAddRequest.getId(), "Complain Added", request );
        }

        return "redirect:/complains/my-search";
    }

    @GetMapping( "/my-search" )
    @TitleAndContent( title = "title.complains.search", content = "complains/complains-my-search", activeMenu = Menu.COMPLAINS_MY_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.COMPLAINS_MY_SEARCH } )
    public String getMySearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Complains My Search page called" );
        return viewRoot;
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.complains.search", content = "complains/complains-search", activeMenu = Menu.COMPLAINS_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.COMPLAINS_SEARCH } )
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Complains Search page called" );
        return viewRoot;
    }

    @GetMapping( "/{id}" )
    @TitleAndContent( title = "title.complains.view", content = "complains/complains-view", activeMenu = Menu.COMPLAINS_VIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.JQUERY_PRINT, FrontEndLibrary.COMPLAINS_VIEW } )
    public String view(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ){

        if( complainsValidationService.view( redirectAttributes, id ) ){

            complainsModelService.view( model, id, getLoggedInUser() );
            return viewRoot;
        }

        return "redirect:/complains/search";
    }

    @PostMapping( "/take-action" )
    public String takeAction(
            @RequestParam( "id" ) Long id,
            @RequestParam( "action" ) Long actionId,
            @Valid ComplainsData complainsData,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ) throws Exception {

        if( complainsValidationService.takeAction( redirectAttributes, id, actionId, getLoggedInUser(), complainsData ) ){

            complainsModelService.takeAction( redirectAttributes, id, actionId, complainsData, getLoggedInUser() );
            log( LogEvent.COMPLAINS_TAKE_ACTION, actionId, "Complains Take action", request );
        }
        else{

            return "redirect:/complains/" + id;
        }

        return "redirect:/complains/search";
    }

}
