package com.dgt.paribahanpool.complain.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.complain.model.ComplainsSearchViewResponse;
import com.dgt.paribahanpool.complain.service.ComplainsService;
import com.dgt.paribahanpool.complain.service.ComplainsValidationService;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/complains" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class ComplainsRestController extends BaseRestController {

    private final ComplainsService complainsService;
    private final ComplainsValidationService complainsValidationService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<ComplainsSearchViewResponse> searchComplains(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return complainsService.searchForDatatable( dataTablesInput );
    }

    @GetMapping(
            value = "/my-search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<ComplainsSearchViewResponse> searchMyComplains(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return complainsService.searchForMyDatatable( dataTablesInput, getLoggedInUser().getUser().getId() );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = complainsValidationService.delete( id );
        if( restValidationResult.getSuccess() ){

            complainsService.delete( id );
            log( LogEvent.COMPLAIN_DELETED, id, "", request );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }
}
