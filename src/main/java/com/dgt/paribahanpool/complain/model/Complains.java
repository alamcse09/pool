package com.dgt.paribahanpool.complain.model;

import com.dgt.paribahanpool.committee.model.Committee;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.WorkflowEntity;
import lombok.Data;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table( name = "complains")
@SQLDelete( sql = "UPDATE complains SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class Complains extends AuditableEntity implements WorkflowEntity {

    @Id
    @Column( name = "id" )
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;

    @Column( name = "name_bn" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String nameBn;

    @Column( name = "name_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String nameEn;

    @Column( name = "occupation" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String occupation;

    @Column( name = "permanent_address" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String permanentAddress;

    @Column( name = "current_address" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String currentAddress;

    @Column( name = "phone_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String phoneNumber;

    @Column( name = "details", columnDefinition = "TEXT" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String details;

    @Column( name = "misconduct" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String misconduct;

    @Column( name = "discard" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String discard;

    @Column( name = "other_objections" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String otherObjections;

    @Column( name = "document_group_id" )
    private Long docGroupId;

    @Column( name = "decision_document_group_id" )
    private Long decisionDocGroupId;

    @Column( name = "decision_judgement_document_group_id" )
    private Long decisionJudgementDocGroupId;

    @Column( name = "complain_document_group_id" )
    private Long complainDocGroupId;

    @Column( name = "complain_reason_document_group_id" )
    private Long complainReasonDocGroupId;

    @Column( name = "complain_reply_document_group_id" )
    private Long complainReplyDocGroupId;

    @ManyToOne
    @JoinColumn( name = "accused_user_id", foreignKey = @ForeignKey( name = "fk_complains_accused_user_id" ) )
    private User accusedUser;

    @OneToOne
    @JoinColumn( name = "state_id", foreignKey = @ForeignKey( name = "fk_complains_state_id" ) )
    private State state;

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinTable(
            name = "complains_committee_member_map",
            joinColumns = @JoinColumn( name = "complains_id", foreignKey = @ForeignKey( name = "fk_committee_member_complains_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "committee_member_id", foreignKey = @ForeignKey( name = "fk_complains_id_committee_member_map_member_record_id" ) )
    )
    private Set<Committee> committeeMemberSet = new HashSet<>();
}
