package com.dgt.paribahanpool.complain.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.userlevel.model.RoleAddRequest;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Data
public class ComplainsAddRequest {

    private Long id;

    @NotBlank( message = "{validation.common.required}" )
    private String nameBn;

    @NotBlank( message = "{validation.common.required}" )
    private String nameEn;

    @NotBlank( message = "{validation.common.required}" )
    private String occupation;

    @NotBlank( message = "{validation.common.required}" )
    private String permanentAddress;

    @NotBlank( message = "{validation.common.required}" )
    private String currentAddress;

    @Digits( integer = 11, fraction = 0, message = "{validation.common.numeric}" )
    private String phoneNumber;

    private UserType userType;

    private Long userId;

    List<UserResponse> showEmployeeList;

    List<UserResponse> showMechanicList;

    private String details;

    private MultipartFile[] files;

    List<DocumentMetadata> documents = Collections.EMPTY_LIST;

    private String misconduct;

    private String discard;

    private String otherObjections;
}
