package com.dgt.paribahanpool.complain.model;

import com.dgt.paribahanpool.committee.model.CommitteeAddRequest;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.workflow.model.WorkflowAdditionalData;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;

@Data
public class ComplainsData implements WorkflowAdditionalData {

    private transient MultipartFile[] showCauseFiles;

    List<DocumentMetadata> showCauseDocuments = Collections.EMPTY_LIST;

    List<CommitteeAddRequest> committeeAddRequestList;

    private Long actionTakerMemberId;

    private transient MultipartFile[] showReasonFiles;

    List<DocumentMetadata> showReasonDocuments = Collections.EMPTY_LIST;

    private transient MultipartFile[] showInvestigationRequestFiles;

    List<DocumentMetadata> showInvestigationRequestDocuments = Collections.EMPTY_LIST;

    private transient MultipartFile[] showGenerateNormalComplainFiles;

    List<DocumentMetadata> showGenerateNormalComplainDocuments = Collections.EMPTY_LIST;

    private transient MultipartFile[] showGenerateJudgeComplainFiles;

    List<DocumentMetadata> showGenerateJudgeComplainDocuments = Collections.EMPTY_LIST;
}
