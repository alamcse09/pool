package com.dgt.paribahanpool.complain.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.workflow.model.StateResponse;
import lombok.Data;

import java.util.List;

@Data
public class ComplainsSearchViewResponse {

    private Long id;
    private String nameBn;
    private String occupation;
    private String currentAddress;
    private String phoneNumber;
    private String details;
    List<DocumentMetadata> documents;
    List<DocumentMetadata> decisionDocuments;
    List<DocumentMetadata> complainDocuments;
    List<DocumentMetadata> complainReasonDocuments;
    List<DocumentMetadata> complainReplyDocuments;

    private StateResponse stateResponse;
}
