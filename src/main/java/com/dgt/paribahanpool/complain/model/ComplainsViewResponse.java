package com.dgt.paribahanpool.complain.model;

import com.dgt.paribahanpool.committee.model.CommitteeResponse;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.AccusedPersonType;
import com.dgt.paribahanpool.user.model.UserResponse;
import lombok.Data;

import java.util.List;

@Data
public class ComplainsViewResponse {

    private Long id;
    private String nameBn;
    private String nameEn;
    private String occupation;
    private String permanentAddress;
    private String currentAddress;
    private String details;
    private String accusedNameBn;
    private AccusedPersonType accusedDesignation;
    private Long stateId;

    private List<UserResponse> showEmployeeList;
    private Long actionTakerMemberId;

    private List<CommitteeResponse> committeeResponseList;
    List<DocumentMetadata> documents;
    List<DocumentMetadata> decisionDocuments;
    List<DocumentMetadata> complainDocuments;
    List<DocumentMetadata> complainReasonDocuments;
    List<DocumentMetadata> complainReplyDocuments;
}
