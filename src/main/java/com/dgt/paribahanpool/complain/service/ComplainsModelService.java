package com.dgt.paribahanpool.complain.service;

import com.dgt.paribahanpool.complain.model.Complains;
import com.dgt.paribahanpool.complain.model.ComplainsAddRequest;
import com.dgt.paribahanpool.complain.model.ComplainsData;
import com.dgt.paribahanpool.complain.model.ComplainsViewResponse;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.workflow.model.StateActionMapResponse;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class ComplainsModelService {

    private final UserService userService;
    private final ComplainsService complainsService;
    private final MvcUtil mvcUtil;
    private final StateActionMapService stateActionMapService;

    public void addComplainsGet( Model model ){

        List<UserResponse> userEmployeeResponseList = userService.getUserResponseList( UserType.SYSTEM_USER );

        List<UserResponse> userMechanicResponseList = userService.getUserResponseList( UserType.MECHANIC );

        ComplainsAddRequest complainsAddRequest = new ComplainsAddRequest();

        complainsAddRequest.setShowEmployeeList( userEmployeeResponseList );

        complainsAddRequest.setShowMechanicList( userMechanicResponseList );

        model.addAttribute( "complainsAddRequest", complainsAddRequest );
    }

    public void addComplainsPost( ComplainsAddRequest complainsAddRequest, RedirectAttributes redirectAttributes, Long userId ) {

        complainsService.save( complainsAddRequest, userId );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.complain.add" );
    }

    @Transactional
    public void view( Model model, Long id, UserPrincipal loggedInUser ) {

        Complains complains = complainsService.findById( id ).get();

        ComplainsViewResponse complainsViewResponse = complainsService.getComplainsViewResponseFromComplains( complains );
        model.addAttribute( "complainsViewResponse", complainsViewResponse );

        if( complains.getState() != null ){

            List<StateActionMapResponse> stateActionMapResponseList = stateActionMapService.findStateActionMapResponseListByCurrentStateAndRoleIdSet( complainsViewResponse.getStateId(), loggedInUser.getRoleIds() );
            model.addAttribute( "stateActionList", stateActionMapResponseList );
        }
    }

    public void takeAction( RedirectAttributes redirectAttributes, Long id, Long actionId, ComplainsData complainsData, UserPrincipal loggedInUser ) throws Exception {

        complainsService.takeAction( id, actionId, complainsData, loggedInUser );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.common.action.success" );
    }
}
