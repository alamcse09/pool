package com.dgt.paribahanpool.complain.service;

import com.dgt.paribahanpool.complain.model.Complains;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComplainsRepository extends DataTablesRepository< Complains, Long > {

}
