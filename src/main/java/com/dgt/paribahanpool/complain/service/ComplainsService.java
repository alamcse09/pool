package com.dgt.paribahanpool.complain.service;

import com.dgt.paribahanpool.committee.model.Committee;
import com.dgt.paribahanpool.committee.model.CommitteeResponse;
import com.dgt.paribahanpool.committee.service.CommitteeService;
import com.dgt.paribahanpool.complain.model.*;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.document.model.Document;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.enums.AccusedPersonType;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.StateResponse;
import com.dgt.paribahanpool.workflow.model.WorkflowAdditionalData;
import com.dgt.paribahanpool.workflow.model.WorkflowService;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.dgt.paribahanpool.complain.service.ComplainsSpecification.filterByAccusedUserId;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class ComplainsService implements WorkflowService<Complains> {

    private final UserService userService;
    private final ComplainsRepository complainsRepository;
    private final DocumentService documentService;
    private final StateService stateService;
    private final StateActionMapService stateActionMapService;
    private final CommitteeService committeeService;

    public Optional<Complains> findById(Long id ){

        return complainsRepository.findById( id );
    }

    public Complains save( Complains complains ){

        return complainsRepository.save( complains );
    }

    @Transactional
    public void save( ComplainsAddRequest complainsAddRequest, Long userId ) {

        Complains complains = new Complains();

        if( complainsAddRequest.getId() != null ){

            complains = findById( complainsAddRequest.getId() ).get();
        }

        complains = getComplainsFromComplainsAddRequest( complains, complainsAddRequest, userId );

        save( complains );
    }

    private Complains getComplainsFromComplainsAddRequest( Complains complains, ComplainsAddRequest complainsAddRequest, Long userId ) {

        User user = userService.findById( complainsAddRequest.getUserId() ).get();

        complains.setNameBn( complainsAddRequest.getNameBn() );
        complains.setNameEn( complainsAddRequest.getNameEn() );
        complains.setOccupation( complainsAddRequest.getOccupation() );
        complains.setPermanentAddress( complainsAddRequest.getPermanentAddress() );
        complains.setCurrentAddress( complainsAddRequest.getCurrentAddress() );
        complains.setPhoneNumber( complainsAddRequest.getPhoneNumber() );
        complains.setDetails( complainsAddRequest.getDetails() );
        complains.setAccusedUser( user );
        complains.setCreatedBy( userId );
        complains.setModifiedBy( userId );
        complains.setCreatedAt( LocalDateTime.now() );
        complains.setModifiedAt( LocalDateTime.now() );

        if( complains.getState() == null ){

            Long startStateId = AppConstants.FORWARD_TO_COMMISSIONER;

            State state = stateService.findStateReferenceById( startStateId );

            complains.setState( state );
        }

        if( complains.getDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( complainsAddRequest.getFiles(), Collections.EMPTY_MAP );
            complains.setDocGroupId( documentUploadedResponse.getDocGroupId() );
        }

        else{

            List<Document> documentList = documentService.buildFromMultipartFile( complainsAddRequest.getFiles(), complains.getDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        return complains;
    }

    public DataTablesOutput<ComplainsSearchViewResponse> searchForDatatable( DataTablesInput dataTablesInput ) {

        return complainsRepository.findAll( dataTablesInput, this::getComplainsSearchResponseFromComplains );
    }

    public ComplainsSearchViewResponse getComplainsSearchResponseFromComplains( Complains complains ){

        ComplainsSearchViewResponse complainsSearchViewResponse = new ComplainsSearchViewResponse();

        complainsSearchViewResponse.setId( complains.getId() );
        complainsSearchViewResponse.setNameBn( complains.getNameBn() );
        complainsSearchViewResponse.setOccupation( complains.getOccupation() );
        complainsSearchViewResponse.setCurrentAddress( complains.getCurrentAddress() );
        complainsSearchViewResponse.setPhoneNumber( complains.getPhoneNumber() );
        complainsSearchViewResponse.setDetails( complains.getDetails() );

        List<DocumentMetadata> documentdataList = documentService.findDocumentMetaDataListByGroupId( complains.getDocGroupId() );
        complainsSearchViewResponse.setDocuments( documentdataList );

        List<DocumentMetadata> complainDocumentdataList = documentService.findDocumentMetaDataListByGroupId( complains.getComplainDocGroupId() );
        complainsSearchViewResponse.setComplainDocuments( complainDocumentdataList );

        List<DocumentMetadata> complainReasonDocumentdataList = documentService.findDocumentMetaDataListByGroupId( complains.getComplainReasonDocGroupId() );
        complainsSearchViewResponse.setComplainReasonDocuments( complainReasonDocumentdataList );

        List<DocumentMetadata> complainReplyDocumentdataList = documentService.findDocumentMetaDataListByGroupId( complains.getComplainReplyDocGroupId() );
        complainsSearchViewResponse.setComplainReplyDocuments( complainReplyDocumentdataList );

        List<DocumentMetadata> decisionDocumentdataList = documentService.findDocumentMetaDataListByGroupId( complains.getDecisionDocGroupId() );
        complainsSearchViewResponse.setDecisionDocuments( decisionDocumentdataList );

        State state = complains.getState();
        if( state != null ){

            StateResponse stateResponse = StateResponse.builder()
                    .id( state.getId() )
                    .name( state.getName() )
                    .nameEn( state.getNameEn() )
                    .stateType( state.getStateType() )
                    .build();
            complainsSearchViewResponse.setStateResponse( stateResponse );
        }

        return complainsSearchViewResponse;
    }

    public Boolean existById( Long id ) {

        return complainsRepository.existsById( id );
    }

    public void delete( Long id ) {

        complainsRepository.deleteById( id );
    }

    public DataTablesOutput<ComplainsSearchViewResponse> searchForMyDatatable( DataTablesInput dataTablesInput, Long userId ) {

        return complainsRepository.findAll( dataTablesInput,  filterByAccusedUserId( userId ), null,  this::getComplainsSearchResponseFromComplains );
    }

    public ComplainsViewResponse getComplainsViewResponseFromComplains( Complains complains ) {

        ComplainsViewResponse complainsViewResponse = new ComplainsViewResponse();

        List<UserResponse> userResponseList = userService.getUserResponseList( UserType.SYSTEM_USER );

        List<CommitteeResponse> committeeResponseList = complains.getCommitteeMemberSet()
                .stream()
                .map(
                        committee -> CommitteeResponse.builder()
                                .id( committee.getId() )
                                .nameEn( committee.getNameEn() )
                                .nameBn( committee.getName() )
                                .designationName( committee.getDesignation() )
                                .positionInCommittee( committee.getPositionInCommitte() )
                                .build()
                )
                .collect(Collectors.toList() );

        List<DocumentMetadata> documents = documentService.findDocumentMetaDataListByGroupId( complains.getDocGroupId() );
        List<DocumentMetadata> decisionDocuments = documentService.findDocumentMetaDataListByGroupId( complains.getDecisionDocGroupId() );
        List<DocumentMetadata> complainDocuments = documentService.findDocumentMetaDataListByGroupId( complains.getComplainDocGroupId() );
        List<DocumentMetadata> complainReasonDocuments = documentService.findDocumentMetaDataListByGroupId( complains.getComplainReasonDocGroupId() );
        List<DocumentMetadata> complainReplyDocuments = documentService.findDocumentMetaDataListByGroupId( complains.getComplainReplyDocGroupId() );

        complainsViewResponse.setId( complains.getId() );
        complainsViewResponse.setStateId( complains.getState() != null? complains.getState().getId(): null );
        complainsViewResponse.setNameBn( complains.getNameBn() );
        complainsViewResponse.setNameEn(complains.getNameEn() );
        complainsViewResponse.setOccupation( complains.getOccupation() );
        complainsViewResponse.setPermanentAddress( complains.getPermanentAddress() );
        complainsViewResponse.setCurrentAddress( complains.getCurrentAddress() );
        complainsViewResponse.setDetails( complains.getDetails() );
        complainsViewResponse.setAccusedNameBn( complains.getAccusedUser().getUserType() != UserType.MECHANIC ? complains.getAccusedUser().getEmployee().getName() : complains.getAccusedUser().getMechanic().getName() );
        complainsViewResponse.setAccusedDesignation( complains.getAccusedUser().getUserType() != UserType.MECHANIC ? AccusedPersonType.SYSTEM_USER : AccusedPersonType.MECHANIC );
        complainsViewResponse.setShowEmployeeList( userResponseList );
        complainsViewResponse.setCommitteeResponseList( committeeResponseList );
        complainsViewResponse.setDocuments( documents );
        complainsViewResponse.setDecisionDocuments( decisionDocuments );
        complainsViewResponse.setComplainDocuments( complainDocuments );
        complainsViewResponse.setComplainReasonDocuments( complainReasonDocuments );
        complainsViewResponse.setComplainReplyDocuments( complainReplyDocuments );

        return complainsViewResponse;
    }

    @Transactional
    public void takeAction(Long id, Long actionId, ComplainsData complainsData, UserPrincipal loggedInUser) throws Exception {

        stateActionMapService.takeAction( this, id, actionId, loggedInUser, (workflowEntity ) -> setAdditionalData( workflowEntity, actionId, complainsData ) );
    }

    private WorkflowAdditionalData setAdditionalData( Object workflowEntity, Long actionId, WorkflowAdditionalData additionalActionData ) {

        Complains complain = (Complains) workflowEntity;

        ComplainsData complainsData = (ComplainsData) additionalActionData;

        if( actionId.equals( AppConstants.UPLOAD_SHOW_CAUSE_LETTER) ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( complainsData.getShowCauseFiles(), Collections.EMPTY_MAP );
            complain.setComplainReasonDocGroupId( documentUploadedResponse.getDocGroupId() );
        }

        if( actionId.equals( AppConstants.UPLOAD_RESPONSE_LETTER) ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( complainsData.getShowReasonFiles(), Collections.EMPTY_MAP );
            complain.setComplainReplyDocGroupId( documentUploadedResponse.getDocGroupId() );
        }

        if( actionId.equals( AppConstants.CREATE_COMMITTEE_COMPLAIN_ACTION) ){

            Set<Committee> committeeSet =
                    complainsData.getCommitteeAddRequestList()
                            .stream()
                            .map( committeeAddRequest -> committeeService.getCommitteeFromCommitteeAddRequest( committeeAddRequest ) )
                            .filter( committee -> committee != null )
                            .collect( Collectors.toSet() );

            complain.getCommitteeMemberSet().clear();
            complain.getCommitteeMemberSet().addAll( committeeSet );
        }

        if( actionId.equals( AppConstants.REQUEST_FOR_INVESTIGATION_REPORT) ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( complainsData.getShowInvestigationRequestFiles(), Collections.EMPTY_MAP );
            complain.setComplainDocGroupId( documentUploadedResponse.getDocGroupId() );
        }

        if( actionId.equals( AppConstants.NORMAL_ISSUE) ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( complainsData.getShowGenerateNormalComplainFiles(), Collections.EMPTY_MAP );
            complain.setDecisionDocGroupId( documentUploadedResponse.getDocGroupId() );
        }

        if( actionId.equals( AppConstants.UPLOAD_REPORT_WITH_JUDGEMENT) ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( complainsData.getShowGenerateJudgeComplainFiles(), Collections.EMPTY_MAP );
            complain.setDecisionJudgementDocGroupId( documentUploadedResponse.getDocGroupId() );
        }

        return additionalActionData;
    }
}
