package com.dgt.paribahanpool.complain.service;

import com.dgt.paribahanpool.complain.model.Complains;
import com.dgt.paribahanpool.complain.model.Complains_;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.model.User_;
import org.springframework.data.jpa.domain.Specification;

public class ComplainsSpecification {

    public static Specification<Complains> filterByAccusedUserId( Long userId ){

        if( userId == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(Complains_.ACCUSED_USER ).get( User_.ID ), userId );
    }
}
