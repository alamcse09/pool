package com.dgt.paribahanpool.complain.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.complain.model.Complains;
import com.dgt.paribahanpool.complain.model.ComplainsAddRequest;
import com.dgt.paribahanpool.complain.model.ComplainsData;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class ComplainsValidationService {

    private final MvcUtil mvcUtil;
    private final ComplainsService complainsService;
    private final StateActionMapService stateActionMapService;

    public Boolean handleBindingResultForAddFormPost( Model model, BindingResult bindingResult, ComplainsAddRequest complainsAddRequest ) {

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "complainsAddRequest" )
                        .object( complainsAddRequest )
                        .title( "title.complains.add" )
                        .content( "complains/complains-add" )
                        .activeMenu( Menu.COMPLAINS_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.COMPLAINS_ADD } )
                        .build()
        );
    }

    public Boolean addComplainsPost( RedirectAttributes redirectAttributes, ComplainsAddRequest complainsAddRequest ) {

        if( complainsAddRequest.getId() != null ){

            Optional<Complains> complainsOptional = complainsService.findById( complainsAddRequest.getId() );

            if( !complainsOptional.isPresent() ){

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
                return false;
            }
        }

        return true;
    }

    public RestValidationResult delete( Long id ) throws NotFoundException {

        Boolean exist = complainsService.existById( id );
        if( !exist ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean view( RedirectAttributes redirectAttributes, Long id ) {

        Optional<Complains> complainsOptional = complainsService.findById( id );

        if( !complainsOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    public Boolean takeAction( RedirectAttributes redirectAttributes, Long id, Long actionId, UserPrincipal loggedInUser, ComplainsData complainsData ) {

        Optional<Complains> complainsOptional = complainsService.findById( id );

        if( !complainsOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }
        else{

            Complains complains = complainsOptional.get();
            State state = complains.getState();

            if( !stateActionMapService.actionAllowed( state.getId(), loggedInUser.getRoleIds(), actionId ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.common.action.not-allowed" );
                return false;
            }

            if( actionId.equals( AppConstants.UPLOAD_SHOW_CAUSE_LETTER) && ( complainsData.getShowCauseFiles() == null || complainsData.getShowCauseFiles().length < 0 ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.complains.add.no-show-cause-report" );
                return false;
            }

            if( actionId.equals( AppConstants.UPLOAD_RESPONSE_LETTER) && ( complainsData.getShowReasonFiles() == null || complainsData.getShowReasonFiles().length < 0 ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.complains.add.no-show-reason-report" );
                return false;
            }

            if( actionId.equals( AppConstants.CREATE_COMMITTEE_COMPLAIN_ACTION) && ( complainsData.getCommitteeAddRequestList() == null || complainsData.getCommitteeAddRequestList().size() < 0 || complainsData.getActionTakerMemberId() == null )  ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.complains.add.committee-not-properly-created" );
                return false;
            }

            if( actionId.equals( AppConstants.REQUEST_FOR_INVESTIGATION_REPORT) && ( complainsData.getShowInvestigationRequestFiles() == null || complainsData.getShowInvestigationRequestFiles().length < 0 ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.complains.add.no-investigation-request-report" );
                return false;
            }

            if( actionId.equals( AppConstants.NORMAL_ISSUE) && ( complainsData.getShowGenerateNormalComplainFiles() == null || complainsData.getShowGenerateNormalComplainFiles().length < 0 ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.complains.add.no-normal-judgement-report" );
                return false;
            }

            if( actionId.equals( AppConstants.UPLOAD_REPORT_WITH_JUDGEMENT) && ( complainsData.getShowGenerateJudgeComplainFiles() == null || complainsData.getShowGenerateJudgeComplainFiles().length < 0 ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.complains.add.no-legal-judgement-report" );
                return false;
            }

            return true;
        }
    }
}
