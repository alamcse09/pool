package com.dgt.paribahanpool.config;

import ch.mfrey.thymeleaf.extras.cache.CacheDialect;
import com.dgt.paribahanpool.exceptions.UserDisabledException;
import com.dgt.paribahanpool.util.converter.GsonLocalDate;
import com.dgt.paribahanpool.util.converter.GsonLocalDateTime;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.support.DatabaseStartupValidator;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.thymeleaf.TemplateEngine;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.lang.reflect.Modifier;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Stream;

@Configuration
@PropertySource( "classpath:config/front-end-library.properties")
public class AppConfig {

    @Autowired
    public AppConfig( TemplateEngine templateEngine ){

        templateEngine.addDialect( new CacheDialect() );
    }

    @Bean
    public DatabaseStartupValidator databaseStartupValidator( DataSource dataSource ){

        DatabaseStartupValidator dsv = new DatabaseStartupValidator();
        dsv.setDataSource( dataSource );
        dsv.setInterval( 15 );
        dsv.setTimeout( 120 );

        return dsv;
    }

    @Bean
    public static BeanFactoryPostProcessor dependsOnPostProcessor() {
        return bf -> {

            String[] jpa = bf.getBeanNamesForType(EntityManagerFactory.class);
            Stream.of(jpa)
                    .map(bf::getBeanDefinition)
                    .forEach(it -> it.setDependsOn("databaseStartupValidator"));
        };
    }


    @Bean
    public AuthenticationFailureHandler exceptionAuthHandler() {

        ExceptionMappingAuthenticationFailureHandler exceptionMappingAuthenticationFailureHandler = new ExceptionMappingAuthenticationFailureHandler();

        Map<String,String> exceptionUrlMap = new HashMap<>();

        exceptionUrlMap.put( UsernameNotFoundException.class.getName(), "/login?error=true&msg=error.login.bad-credential" );
        exceptionUrlMap.put( BadCredentialsException.class.getName(), "/login?error=true&msg=error.login.bad-credential" );
        exceptionUrlMap.put( UserDisabledException.class.getName(), "/login?error=true&msg=error.login.disabled" );

        exceptionMappingAuthenticationFailureHandler.setExceptionMappings( exceptionUrlMap );

        return exceptionMappingAuthenticationFailureHandler;
    }

    @Bean
    public LocaleResolver localeResolver() {

        SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
        sessionLocaleResolver.setDefaultLocale( Locale.forLanguageTag( "bn" ) );
        return sessionLocaleResolver;
    }

    @Bean
    public LocalValidatorFactoryBean getValidator( MessageSource messageSource ) {

        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource( messageSource );
        return bean;
    }

    @Bean
    @Qualifier( "notificationTaskExecutor" )
    public TaskExecutor notificationTaskExecutor(){

        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize( 10 );
        return taskExecutor;
    }

    @Bean
    public Gson gson(){

        return new Gson();
    }

    @Bean
    @Qualifier( "state_action_service" )
    public Gson stateActionServiceGson(){

        final GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.STATIC, Modifier.TRANSIENT, Modifier.VOLATILE)
                .registerTypeAdapter( LocalDateTime.class, new GsonLocalDateTime() )
                .registerTypeAdapter( LocalDate.class, new GsonLocalDate() );

        return builder.create();
    }

    @Bean
    public Docket api() {
        return new Docket( DocumentationType.OAS_30 )
                .apiInfo(apiInfo())
                .securityContexts(Arrays.asList(securityContext()))
                .securitySchemes(Arrays.asList(apiKey()))
                .select()
                .apis( RequestHandlerSelectors.basePackage( "com.dgt.paribahanpool" ) )
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Paribahan Pool",
                "Api for managing paribahan pool platform",
                "1.0-beta",
                "Terms of service",
                new Contact("Alam", "revesoft.com", "maalam@revesoft.com"),
                "License of API",
                "API license URL",
                Collections.emptyList()
        );
    }

    private ApiKey apiKey() {
        return new ApiKey("Authorization", "Authorization", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth()).build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList( new SecurityReference("Authorization", authorizationScopes ) );
    }
}
