package com.dgt.paribahanpool.config;

import com.dgt.paribahanpool.util.NameResponse;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class AppConstants {

    public static final Long NOC_APP_PLR_INIT_STATE_ID = 1L;
    public static final Long NOC_APP_PLR_APPROVE_ACTION_ID = 6L;

    public static final Long NOC_APP_ENCASHMENT_INIT_STATE_ID = 8L;

    public static final Long NOC_APP_ENCASHMENT_CHOOSE_MECHANIC_INCHARGE_ACTION_ID = 9L;
    public static final Long NOC_APP_ENCASHMENT_DO_FINE_ACTION_ID =10L;
    public static final Long NOC_APP_ENCASHMENT_COLLECT_FINE_ACTION_ID =11L;
    public static final Long NOC_APP_ENCASHMENT_APPROVE_ACTION_ID =16L;

    public static final Long DEFAULT_PP_GROUP_ID = 1L;

    public static final Long VEHICLE_REQUISITION_INIT_STATE_ID = 34L;

    public static final Long VEHICLE_REPLACE_INIT_STATE_ID = 27L;

    public static final Long MARINE_SERVICE_APPLICATION_INIT_STATE_ID = 85L;

    public static final Long LEAVE_APPLICATION_MARINE_INIT_STATE_ID = 18L;

    public static final Long LEAVE_APPLICATION_HeadOffice_INIT_STATE_ID = 21L;

    public static final Long LEAVE_APPLICATION_RAndH_INIT_STATE_ID = 24L;

    public static final Long LEAVE_APPLICATION_MARINE_APPROVE_ACTION_ID = 20L;

    public static final Long LEAVE_APPLICATION_HeadOffice_APPROVE_ACTION_ID = 18L;

    public static final Long LEAVE_APPLICATION_RAndH_APPROVE_ACTION_ID = 22L;

    public static final Long PUBLIC_USER_ID = 10L;

    public static final Long JOB_APPROVED_STATE_ID = 41L;

    public static final Long CREATE_JOB_ACTION_ID = 32L;

    public static final Long SUBMIT_JOB_ACTION_ID = 38L;

    public static final Long CHOOSE_MECHANIC_IN_CHARGE_ACTION_ID = 36L;

    public static final Long CAT_B_MECHANIC_IN_CHARGE_ASSIGN_STATE_ID = 46L;

    public static final Long VEHICLE_SERVICE_APPROVE_ACTION_ID = 99L;

    public static final Long VEHICLE_SERVICE_APPROVAL_FROM_MOPA_ACTION_ID = 34L;

    public static final Long VEHICLE_SERVICE_APPROVAL_FROM_FM_ACTION_ID = 35L;

    public static final Long VEHICLE_SERVICE_DISPOSITION_ACTION_ID = 78L;

    public static final Long VEHICLE_REQUISITION_CHOOSE_ACTION_ID = 25L;
    public static final Long VEHICILE_REQUISITION_DD_ACTION_ID = 26L;
    public static final Long VEHICILE_REQUISITION_DIRECTOR_ACTION_ID = 27L;
    public static final Long VEHICLE_REQUISITION_APPROVE_ACTION_ID = 29L;
    public static final Long VEHICLE_REQUISITION_REJECT_ACTION_ID = 28L;

    public static final Long CHOOSE_VEHICLE_STATE_ID = 36L;
    public static final Long VEHICLE_REQUISITION_DD_STATE_ID = 29L;
    public static final Long VEHICLE_REQUISITION_DIRECTOR_STATE_ID = 30L;

    public static final Long VEHICLE_SERVICE_APPLICATION_REJECTION_ACTION_ID = 28L; // TODO: NOT FIXED YET

    public static final String MECHANIC_DESIGNATION_BN = "মেকানিক";
    public static final String MECHANIC_DESIGNATION_EN = "Mechanic";

    public static final Long MECHANIC_IN_CHARGE_DESIGNATION_ID = 12L;
    public static final Long WORKSHOP_MANAGER_DESIGNATION_ID = 13L;
    public static final Long JOB_ASSISTANT_DESIGNATION_ID = 14L;

    public static final Long BILL_CREATE_INITIAL_STATE_ID = 48L;

    public static final Long INCIDENT_REPORT_INSTANTIATED = 131L;
    public static final Long FORWARDED_TO_ACTION_TAKER = 61L;

    public static final Long CREATE_COMMITTEE = 134L;
    public static final Long UPLOAD_INQUIRY_REPORT = 51L;
    public static final Long UPLOAD_CHALAN_REPORT = 71L;
    public static final Long INCIDENT_DO_FINE_ACTION_ID = 140L;
    public static final Long INCIDENT_COLLECT_FINE_ACTION_ID = 144L;
    public static final Long INCIDENT_SENT_FOR_SERVICE_ACTION_ID = 92L;
    public static final Long INCIDENT_FORWARD_TO_DA_TO_UPLOAD_INVES_REP_ACTION_ID = 145L;
    public static final Long INCIDENT_FORWARD_BACK_TO_DA_ACTION_ID = 117L;
    public static final Long INCIDENT_UPLOAD_VERDICT_REPORT = 137L;
    public static final Long INCIDENT_FORWARD_TO_UD_ACTION_ID = 116L;

    public static final Long FORWARD_TO_COMMISSIONER = 63L;
    public static final Long FORWARD_TO_ACCUSED_USER = 64L;
    public static final Long CREATE_COMMITTEE_COMPLAIN = 65L;
    public static final Long REQUEST_FOR_INVESTIGATION_REPORT_UPLOADED = 66L;
    public static final Long LEGAL_ISSUE = 67L;
    public static final Long INCIDENT_FORWARD_TO_COMMISSIONER = 68L;
    public static final Long INCIDENT_ACTION_TAKEN = 69L;

    public static final Long UPLOAD_SHOW_CAUSE_LETTER = 52L;
    public static final Long UPLOAD_RESPONSE_LETTER = 53L;
    public static final Long CREATE_COMMITTEE_COMPLAIN_ACTION = 54L;
    public static final Long REQUEST_FOR_INVESTIGATION_REPORT = 55L;
    public static final Long NORMAL_ISSUE = 57L;
    public static final Long UPLOAD_REPORT_WITH_JUDGEMENT = 58L;
    public static final Long TRAINING_BATCH_NO = 10L;

    public static final Long MARINE_SERVICE_APP_CREATE_COMMITTEE = 74L;
    public static final Long MARINE_SERVICE_APP_COMMITTEE_SUBMIT_REPORT = 75L;

    public static final Long MARINE_SERVICE_DISPOSITION_APP_INIT_STATE_ID = 90L;

    public static final Long MARINE_SERVICE_DISPOSITION_APP_CREATE_COMMITTEE = 79L;
    public static final Long MARINE_SERVICE_DISPOSITION_APP_COMMITTEE_SUBMIT_REPORT = 80L;

    public static final Long VEHICLE_SERVICE_CREATE_JOBS_ACTION_ID = 84L;
    public static final Long VEHICLE_SERVICE_CHOOSE_FOREMAN_ACTION_ID = 83L;
    public static final Long CHOOSE_WORKSHOP_MANAGER_ACTION_ID = 90L;
    public static final Long VEHICLE_SERVICE_ASSIGN_SECTION_LEADERS_ACTION_ID = 91L;
    public static final Long VEHICLE_SERVICE_FORWARD_TO_PORIDORSHOK_FROM_FOREMAN = 98L;
    public static final Long VEHICLE_SERVICE_PORIDORSHOK_ACTION_ID = 156L;
    public static final Long VEHICLE_SERVICE_CHOOSE_JOB_ASSISTANT_ACTION_ID = 155L;

    public static final Long VEHICLE_SERVICE_INIT_STATE_ID = 163L;
    public static final Long VEHICLE_SERVICE_FOREMAN_STATE_ID = 98L;
    public static final Long VEHICLE_SERVICE_FOREMAN_APPROVE_STATE_ID = 168L;

    public static final Long WARRANTY_SENT_TO_VENDOR_STATE_ID = 108L;
    public static final Long WARRANTY_APPROVE_STATE_ID = 109L;
    public static final Long WARRANTY_REJECT_STATE_ID = 110L;

    public static final Long WARRANTY_APPROVE_ACTION_ID = 93L;
    public static final Long WARRANTY_REJECT_ACTION_ID = 94L;

    public static final Long PARTS_DISPOSITION_SENT_TO_VENDOR_STATE_ID = 95L;
    public static final Long PARTS_DISPOSITION_APPROVE_STATE_ID = 111L;
    public static final Long PARTS_DISPOSITION_REJECT_STATE_ID = 112L;

    public static final Long PARTS_DISPOSITION_APPROVED_ACTION_ID = 95L;
    public static final Long PARTS_DISPOSITION_REJECT_ACTION_ID = 96L;

    public static final Long JOB_CREATED_SENT_TO_FOREMAN_ACTION_ID = 85L;

    public static final Long NOC_START_STATE_ID = 115L;

    public static final Long NOC_SENT_TO_MECHANIC_INCHARGE_ACTION_ID = 105L;
    public static final Long NOC_SENT_TO_ATO_ACTION_ID = 107L;
    public static final Long NOC_SENT_TO_UD_ACTION_ID = 108L;
    public static final Long NOC_SENT_TO_AD_ACTION_ID = 109L;
    public static final Long NOC_ADD_MISSING_VEHICLE_ACTION_ID = 106L;
    public static final Long NOC_UPLOAD_NOC_ISSUE_ACTION_ID = 115L;
    public static final Long NOC_APPROVE = 112L;

    public static final Long DRIVER_REQUISITION_APPLIED_STATE_ID = 153L;
    public static final Long DRIVER_REQUISITION_DD_STATE_ID = 155L;
    public static final Long DRIVER_REQUISITION_DIRECTOR_STATE_ID = 156L;

    public static final Long DRIVER_REQUSITION_REJECT_ACTION_ID = 151L;
    public static final Long DRIVER_REJECTION_APPROVE_ACTION_ID = 153l;
    public static final Long DRIVER_REQUSITION_CHOOSE_DRIVER_ACTION_ID = 148L;

    public static final Map<String, NameResponse> tableNameToModuleMap = new HashMap<>();

    static{

        tableNameToModuleMap.put( "complains", NameResponse.builder().nameEn( "Discipline Application" ).nameBn( "শৃঙ্খলা আবেদন" ).build() );
        tableNameToModuleMap.put( "marine_service_application", NameResponse.builder().nameEn( "Marine Service Application" ).nameBn( "মেরিন রক্ষনাবেক্ষন আবেদন" ).build() );
        tableNameToModuleMap.put( "noc_application", NameResponse.builder().nameEn( "NOC Application" ).nameBn( "অনাপত্তিপত্রে আবেদন" ).build() );
        tableNameToModuleMap.put( "incident", NameResponse.builder().nameEn( "Incident" ).nameBn( "দূর্ঘটনা" ).build() );
        tableNameToModuleMap.put( "marine_service_disposition_application", NameResponse.builder().nameEn( "Marine Disposition Application" ).nameBn( "মেরিন বাতিলকরন আবেদন" ).build() );
        tableNameToModuleMap.put( "parts_disposition", NameResponse.builder().nameEn( "Parts Disposition Application" ).nameBn( "যন্ত্রাংশ বাতিলকরন আবেদন" ).build() );
        tableNameToModuleMap.put( "vehicle_requistion_application", NameResponse.builder().nameEn( "Vehicle Requisition Application" ).nameBn( "যানবাহন বরাদ্দের আবেদন" ).build() );
        tableNameToModuleMap.put( "vehicle_service_application", NameResponse.builder().nameEn( "Vehicle Service Application" ).nameBn( "যানবাহন রক্ষনাবেক্ষন আবেদন" ).build() );
    }
}