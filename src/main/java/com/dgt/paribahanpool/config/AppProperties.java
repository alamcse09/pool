package com.dgt.paribahanpool.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties( prefix = "app" )
public class AppProperties {

    private final SessionProperties session = new SessionProperties();
    private final RememberMe rememberMe = new RememberMe();
    private final FileUploadProperties fileupload = new FileUploadProperties();
    private final EmailProperties email = new EmailProperties();
    private final ServerConfig server = new ServerConfig();
    private final AlertReciever alertReciever = new AlertReciever();
    private Integer tokentimeout = 5;


    @Data
    public static final class RememberMe {

        private Integer validitySeconds;
        private String token;
    }

    @Data
    public static final class SessionProperties {

        private Integer maxSession;
    }

    @Data
    public static final class FileUploadProperties{

        private String root;
    }

    @Data
    public static final class EmailProperties{

        private String from;
    }

    @Data
    public static final class ServerConfig{

        private String host;
        private String port;
    }

    @Data
    public static final class AlertReciever{

        private String email;
        private String phoneNumber;
    }
}
