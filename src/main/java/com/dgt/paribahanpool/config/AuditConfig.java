package com.dgt.paribahanpool.config;

import com.dgt.paribahanpool.auditlog.SecurityAuditAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.time.LocalDateTime;
import java.util.Optional;

@Configuration
@EnableJpaAuditing(
        auditorAwareRef = "auditorProvider",
        dateTimeProviderRef = "dateTimeProvider"
)
public class AuditConfig {

    @Bean( name = "auditorProvider" )
    public AuditorAware<Long> auditorProvider() {

        return new SecurityAuditAware();
    }

    @Bean( name = "dateTimeProvider" )
    public DateTimeProvider dateTimeProvider(){

        return ()-> Optional.of( LocalDateTime.now() );
    }
}
