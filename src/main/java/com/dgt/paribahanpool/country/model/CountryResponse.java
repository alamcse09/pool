package com.dgt.paribahanpool.country.model;

import lombok.Data;

@Data
public class CountryResponse {

    private Long id;
    private String countryNameEn;
    private String countryNameBn;
}
