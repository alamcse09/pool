package com.dgt.paribahanpool.country.service;

import com.dgt.paribahanpool.country.model.Country;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface CountryRepository extends DataTablesRepository<Country,Long> {
}
