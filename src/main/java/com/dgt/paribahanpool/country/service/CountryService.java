package com.dgt.paribahanpool.country.service;

import com.dgt.paribahanpool.country.model.Country;
import com.dgt.paribahanpool.country.model.CountryResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class CountryService {

    private final CountryRepository countryRepository;

    public List<CountryResponse> getAllCountries(){
        List<Country> allCountries = (List<Country>) countryRepository.findAll();
        List<CountryResponse> allCountryList = new ArrayList<>();
        for (Country country : allCountries){
            allCountryList.add(getCountryResponseFromCountry(country));
        }
        return allCountryList;
     }

     public CountryResponse getCountryResponseById(Long id){
        Country country = countryRepository.findById(id).get();
        return getCountryResponseFromCountry(country);
     }

     private CountryResponse getCountryResponseFromCountry(Country country){
        CountryResponse countryResponse = new CountryResponse();
        countryResponse.setId(country.getId());
        countryResponse.setCountryNameEn(country.getCountryNameEn());
        countryResponse.setCountryNameBn(country.getCountryNameBn());
        return countryResponse;
     }
}
