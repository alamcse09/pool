package com.dgt.paribahanpool.dashboard;

import lombok.Data;

import java.util.List;

@Data
public class Dashboard {

    private Long vehicleCount;

    private Long marineCount;

    private Long freeVehicleCount;

    private Long assignedVehicleCount;

    private Long driverCount;

    private Long employeeCount;

    private Long publicUserCount;

    List<Long> serviceCounts;

    List<Integer> monthListPerServiceCounts;
}
