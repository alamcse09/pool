package com.dgt.paribahanpool.dashboard.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.dashboard.service.DashboardService;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.EmailDataBadRequestException;
import com.dgt.paribahanpool.notification.service.EmailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Controller
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class DashboardController extends MVCController {

    private final DashboardService dashboardService;

    @GetMapping( "" )
    @TitleAndContent( title = "title.dashboard", content = "dashboard/dashboard", activeMenu = Menu.DASHBOARD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.DASHBOARD } )
    public String dashboard(
            Model model,
            HttpServletRequest request
    ) throws EmailDataBadRequestException {

        dashboardService.getDashboard( model );
        return viewRoot;
    }
}
