package com.dgt.paribahanpool.dashboard.service;

import com.dgt.paribahanpool.dashboard.Dashboard;
import com.dgt.paribahanpool.driver.service.DriverService;
import com.dgt.paribahanpool.employee.service.EmployeeService;
import com.dgt.paribahanpool.marine.service.MarineService;
import com.dgt.paribahanpool.user.service.PublicInfoService;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import com.dgt.paribahanpool.vehicle.service.VehicleServiceApplicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DashboardService {

    private final VehicleService vehicleService;
    private final MarineService marineService;
    private final DriverService driverService;
    private final EmployeeService employeeService;
    private final PublicInfoService publicInfoService;
    private final VehicleServiceApplicationService vehicleServiceApplicationService;

    @Transactional
    public void getDashboard( Model model ) {

        Dashboard dashboard = new Dashboard();

        Long vehicleCount = vehicleService.getNumberOfVehicle();

        Long marineCount = marineService.getNumberOfMarine();

        Long freeVehicleCount = vehicleService.getNumberOfFreeVehicle();

        Long assignedVehicleCount = vehicleCount - freeVehicleCount;

        Long driverCount = driverService.getNumberOfDriverCount();

        Long employeeCount = employeeService.getNumberOfEmployeeCount();

        Long publicUserCount = publicInfoService.getNumberOfPublicUser();

        List<Object[]>  vehicleServiceListGroupByMonths = vehicleServiceApplicationService.getListOfServiceGroupByMonth( LocalDateTime.now() , LocalDateTime.now().minusYears( 1L ) );

        List<Long> serviceCounts = new ArrayList<>();

        List<Integer> monthListPerServiceCounts = new ArrayList<>();

        for( int i = 0; i<vehicleServiceListGroupByMonths.size(); i++  ){

            serviceCounts.add((Long) vehicleServiceListGroupByMonths.get(i)[0]);
        }

        for( int i = 0; i<vehicleServiceListGroupByMonths.size(); i++  ){

            monthListPerServiceCounts.add((Integer) vehicleServiceListGroupByMonths.get(i)[1]);
        }

        dashboard.setVehicleCount( vehicleCount );
        dashboard.setMarineCount( marineCount );
        dashboard.setFreeVehicleCount( freeVehicleCount );
        dashboard.setAssignedVehicleCount( assignedVehicleCount );
        dashboard.setDriverCount( driverCount );
        dashboard.setEmployeeCount( employeeCount );
        dashboard.setPublicUserCount( publicUserCount );
        dashboard.setServiceCounts( serviceCounts );
        dashboard.setMonthListPerServiceCounts( monthListPerServiceCounts );

        model.addAttribute( "dashboard", dashboard );
    }
}
