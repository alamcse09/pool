package com.dgt.paribahanpool.designation.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.designation.model.Designation;
import com.dgt.paribahanpool.designation.model.DesignationAddRequest;
import com.dgt.paribahanpool.designation.service.DesignationModelService;
import com.dgt.paribahanpool.designation.service.DesignationValidationService;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/designation" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DesignationController extends MVCController {

    private final DesignationModelService designationModelService;
    private final DesignationValidationService designationValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.designation.add", content = "designation/designation-add", activeMenu = Menu.DESIGNATION_ADD )
    @AddFrontEndLibrary( libraries = {FrontEndLibrary.DESIGNATION_ADD } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add Designation Form" );
        designationModelService.addDesignationGet( model, getLoggedInUser().getUser().getUserLevel().getLevel() );
        return viewRoot;
    }

    @PostMapping( "/add" )
    public String addDesignation(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid DesignationAddRequest designationAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ) {

        if( !designationValidationService.handleBindingResultForAddFormPost( model, bindingResult, designationAddRequest, getLoggedInUser().getUser().getUserLevel().getLevel() ) ) {

            return viewRoot;
        }

        if( designationValidationService.addDesignationPost( designationAddRequest, redirectAttributes, getLoggedInUser().getUser().getUserLevel().getLevel() ) ) {

            Designation designation = designationModelService.addDesignationPost(designationAddRequest, redirectAttributes);
            log(LogEvent.DESIGNATION_ADD, designation.getId(), "Designation Added", request);

            return "redirect:/designation/search";
        }

        return "redirect:/designation/add";
    }

    @GetMapping( "/edit/{designationId}" )
    @TitleAndContent( title = "title.designation.edit", content = "designation/designation-add", activeMenu = Menu.DESIGNATION_ADD )
    @AddFrontEndLibrary( libraries = {FrontEndLibrary.DESIGNATION_ADD } )
    public String edit(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable( "designationId" ) Long designationId
    ){

        log.debug( "Rendering Edit Incident Form" );

        if( designationValidationService.editDesignation( redirectAttributes, designationId, getLoggedInUser().getUser().getUserLevel().getLevel() ) ){

            designationModelService.editDesignation( model, designationId, getLoggedInUser().getUser().getUserLevel().getLevel() );
            return viewRoot;
        }

        return "redirect:/designation/search";
    }

    @PostMapping( "/edit/{designationId}" )
    public String editDesignation(
            Model model,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            @Valid DesignationAddRequest designationAddRequest,
            BindingResult bindingResult,
            @PathVariable( "designationId" ) Long designationId
    ) {

        if( !designationValidationService.handleBindingResultForEditFormPost( model, bindingResult, designationAddRequest ) ) {

            return viewRoot;
        }

        if( !designationValidationService.addDesignationPost( designationAddRequest, redirectAttributes, getLoggedInUser().getUser().getUserLevel().getLevel() ) ){

            return "redirect:/designation/edit/{designationId}";
        }

        if( designationValidationService.editDesignationPost( redirectAttributes, designationAddRequest ) ) {

            Designation designation = designationModelService.editDesignationPost( designationAddRequest, redirectAttributes );
            log( LogEvent.DESIGNATION_EDIT, designation.getId(), "Designation Edited", request );

        }

        return "redirect:/designation/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.designation.search", content = "designation/designation-search", activeMenu = Menu.DESIGNATION_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.DESIGNATION_SEARCH })
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Designation Search page called" );
        return viewRoot;
    }

}
