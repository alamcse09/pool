package com.dgt.paribahanpool.designation.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.designation.model.DesignationSearchResponse;
import com.dgt.paribahanpool.designation.service.DesignationService;
import com.dgt.paribahanpool.designation.service.DesignationValidationService;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping( "/api/designation" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DesignationRestController extends BaseRestController {

    private final DesignationService designationService;
    private final DesignationValidationService designationValidationService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<DesignationSearchResponse> searchDesignation(
            DataTablesInput dataTablesInput
    ) {

        log.debug( "Request params, {}", dataTablesInput );
        return designationService.searchForDatatable( dataTablesInput );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id
    ) throws NotFoundException {

        RestValidationResult restValidationResult = designationValidationService.delete( id );
        if( restValidationResult.getSuccess() ){

            designationService.delete( id );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }

        return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
    }
}
