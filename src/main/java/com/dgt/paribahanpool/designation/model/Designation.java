package com.dgt.paribahanpool.designation.model;

import com.dgt.paribahanpool.userlevel.model.UserLevel;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@Entity
@Table( name = "designation" )
@SQLDelete( sql = "UPDATE designation set is_deleted = true where id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class Designation extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "name_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String nameEn;

    @Column( name = "name_bn" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String nameBn;

    @OneToOne
    @JoinColumn( name = "parent_id", foreignKey = @ForeignKey( name = "fk_designation_designation_id" ) )
    private Designation parentDesignation;

    @OneToOne
    @JoinColumn( name = "user_level_id", foreignKey = @ForeignKey( name = "fk_designation_user_level_id" ) )
    private UserLevel userLevel;
}
