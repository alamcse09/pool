package com.dgt.paribahanpool.designation.model;

import com.dgt.paribahanpool.userlevel.model.UserLevelResponse;
import com.dgt.paribahanpool.userlevel.model.UserLevelSearchResponse;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class DesignationAddRequest {

    private Long id;

    @NotBlank( message = "{validation.common.required}" )
    private String nameEn;

    @NotBlank( message = "{validation.common.required}" )
    private String nameBn;

    private Long parentId;

    private List<DesignationViewResponse> designationLists;

    @NotNull( message = "{validation.common.required}" )
    private Long userLevelId;

    private List<UserLevelResponse> userLevelResponseList;
}
