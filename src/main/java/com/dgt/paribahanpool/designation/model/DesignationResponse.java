package com.dgt.paribahanpool.designation.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DesignationResponse {

    private Long id;
    private String nameEn;
    private String nameBn;
}
