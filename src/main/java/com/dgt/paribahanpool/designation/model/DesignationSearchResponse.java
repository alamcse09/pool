package com.dgt.paribahanpool.designation.model;

import com.dgt.paribahanpool.util.NameResponse;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DesignationSearchResponse {

    private Long id;
    private String nameEn;
    private String nameBn;
    private NameResponse parentName;
    private Long level;
}
