package com.dgt.paribahanpool.designation.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;


@Data
@Builder
public class DesignationViewResponse {

    private Long id;
    private String nameEn;
    private String nameBn;
    private Long parentId;
    private List<DesignationAddRequest> showDesignation;

}
