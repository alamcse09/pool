package com.dgt.paribahanpool.designation.service;

import com.dgt.paribahanpool.designation.model.Designation;
import com.dgt.paribahanpool.designation.model.DesignationAddRequest;
import com.dgt.paribahanpool.userlevel.model.UserLevelResponse;
import com.dgt.paribahanpool.userlevel.model.UserLevelSearchResponse;
import com.dgt.paribahanpool.userlevel.service.UserLevelService;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DesignationModelService {

    private final DesignationService designationService;
    private final UserLevelService userLevelService;
    private final MvcUtil mvcUtil;

    public void addDesignationGet( Model model, Long currentUserLevelLevel ){

        DesignationAddRequest designationAddRequest = designationService.getDesignationAddRequest( currentUserLevelLevel );
        model.addAttribute( "designationAddRequest", designationAddRequest );
    }

    public Designation addDesignationPost( DesignationAddRequest designationAddRequest, RedirectAttributes redirectAttributes ){

        Designation designation = designationService.save( designationAddRequest );

        mvcUtil.addSuccessMessage( redirectAttributes, "success.designation.add" );

        return designation;
    }

    public void editDesignation( Model model, Long designationId, Long currentUserLevelLevel ) {

        Optional<Designation> designationOptional = designationService.findById( designationId );

        if( designationOptional.isPresent() ){

            DesignationAddRequest designationAddRequest = designationService.getDesignationAddRequestFromDesignation( designationOptional.get(), currentUserLevelLevel );
            model.addAttribute( "designationAddRequest", designationAddRequest );
        }
    }

    public Designation editDesignationPost(DesignationAddRequest designationAddRequest, RedirectAttributes redirectAttributes) {

        Designation designation = designationService.save( designationAddRequest );

        mvcUtil.addSuccessMessage( redirectAttributes, "success.designation.edit" );

        return designation;
    }
}
