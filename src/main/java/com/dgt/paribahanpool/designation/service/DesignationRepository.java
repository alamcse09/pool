package com.dgt.paribahanpool.designation.service;

import com.dgt.paribahanpool.designation.model.Designation;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DesignationRepository extends DataTablesRepository<Designation, Long>, JpaRepository<Designation,Long> {
}
