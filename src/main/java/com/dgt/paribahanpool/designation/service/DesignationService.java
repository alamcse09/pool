package com.dgt.paribahanpool.designation.service;

import com.dgt.paribahanpool.designation.model.*;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.userlevel.model.UserLevel;
import com.dgt.paribahanpool.userlevel.model.UserLevelResponse;
import com.dgt.paribahanpool.userlevel.service.UserLevelService;
import com.dgt.paribahanpool.util.NameResponse;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DesignationService {

    private final DesignationRepository designationRepository;
    private final UserLevelService userLevelService;

    public Optional<Designation> findById( Long id ){

        return designationRepository.findById( id );
    }

    public Designation save( Designation designation ){

        return designationRepository.save( designation );
    }

    public Iterable<Designation> findAll(){

        return designationRepository.findAll();
    }

    public DesignationAddRequest getDesignationAddRequest( Long currentUserLevelLevel ){

        DesignationAddRequest designationAddRequest = new DesignationAddRequest();

        List<DesignationViewResponse> designationViewResponseList = getDesignationViewResponseList();
        List<UserLevelResponse> userLevelResponseList = userLevelService.getUserLevelResponseListByLevel( currentUserLevelLevel );

        designationAddRequest.setDesignationLists( designationViewResponseList );
        designationAddRequest.setUserLevelResponseList( userLevelResponseList );

        return designationAddRequest;
    }

    public List<DesignationViewResponse> getDesignationViewResponseList() {

        Iterable<Designation> designationIterable = findAll();
        List<Designation> designationList = Lists.newArrayList( designationIterable );

        return getDesignationViewResponseFromDesignationList( designationList );
    }

    private List<DesignationViewResponse> getDesignationViewResponseFromDesignationList( List<Designation> designationList ) {

        return designationList
                .stream()
                .map( this::getDesignationViewResponseFromDesignation )
                .collect( Collectors.toList() );
    }

    private DesignationViewResponse getDesignationViewResponseFromDesignation( Designation designation ) {

        return DesignationViewResponse.builder()
                .id( designation.getId() )
                .nameBn( designation.getNameBn() )
                .nameEn( designation.getNameEn() )
                .parentId( designation.getParentDesignation() == null? null: designation.getParentDesignation().getId() )
                .build();
    }


    @Transactional
    public Designation save( DesignationAddRequest designationAddRequest ){

        Designation designation = new Designation();

        if( designationAddRequest.getId() != null ){

            designation = findById( designationAddRequest.getId() ).get();
        }

        designation = getDesignationFromDesignationAddRequest( designation, designationAddRequest );

        return save( designation );
    }

    public Designation getDesignationFromDesignationAddRequest( Designation designation, DesignationAddRequest designationAddRequest ){

        designation.setNameBn( designationAddRequest.getNameBn() );
        designation.setNameEn( designationAddRequest.getNameEn() );

        if( designationAddRequest.getParentId() != null && designationAddRequest.getParentId() > 0 ) {

            Designation parentDesignation = findReference(designationAddRequest.getParentId());
            designation.setParentDesignation( parentDesignation );
        }

        if( designationAddRequest.getUserLevelId() != null && designationAddRequest.getUserLevelId() > 0 ){

            UserLevel userLevel = userLevelService.findReference( designationAddRequest.getUserLevelId() );
            designation.setUserLevel( userLevel );
        }

        return designation;
    }

    public DesignationAddRequest getDesignationAddRequestFromDesignation( Designation designation, Long currentUserLevelLevel ) {

        DesignationAddRequest designationAddRequest = new DesignationAddRequest();

        designationAddRequest.setId( designation.getId() );
        designationAddRequest.setNameBn( designation.getNameBn() );
        designationAddRequest.setNameEn( designation.getNameEn() );
        designationAddRequest.setParentId( designation.getParentDesignation() == null? null: designation.getParentDesignation().getId() );

        List<DesignationViewResponse> designationViewResponseList = getDesignationViewResponseList();
        List<UserLevelResponse> userLevelResponseList = userLevelService.getUserLevelResponseListByLevel( currentUserLevelLevel );

        designationAddRequest.setDesignationLists( designationViewResponseList );
        designationAddRequest.setUserLevelResponseList( userLevelResponseList );

        designationAddRequest.setUserLevelId( designation.getUserLevel() == null? null: designation.getUserLevel().getId() );

        return designationAddRequest;
    }

    public <T> List<T> findAll( Function<Designation, T> converter ) {

        List<T> dataList = new ArrayList<>();
        Iterable<Designation> designationList = designationRepository.findAll();

        for (Designation designation : designationList) {

            dataList.add( converter.apply( designation ) );
        }

        return dataList;
    }

    public Designation findReference(Long designationId) {

        return designationRepository.getById( designationId );
    }

    public DataTablesOutput<DesignationSearchResponse> searchForDatatable( DataTablesInput dataTablesInput ) {

        return designationRepository.findAll( dataTablesInput, this::getDesignationSearchResponseFromDesignation );
    }

    private DesignationSearchResponse getDesignationSearchResponseFromDesignation( Designation designation ) {

        NameResponse parentNameResponse = NameResponse.builder()
                .nameBn( (designation.getParentDesignation() == null) ? "N/A" : designation.getParentDesignation().getNameBn() )
                .nameEn( (designation.getParentDesignation() == null) ? "N/A" : designation.getParentDesignation().getNameEn() )
                .build();

        return DesignationSearchResponse
                .builder()
                .id( designation.getId() )
                .nameEn( designation.getNameEn() )
                .nameBn( designation.getNameBn() )
                .parentName( parentNameResponse )
                .level( designation.getUserLevel().getLevel() )
                .build();
    }

    public Boolean existsById( Long id ) {

        return designationRepository.existsById( id );
    }

    public void delete( Long id ) {

        designationRepository.deleteById( id );
    }

    public static DesignationResponse getDesignationResponseFromDesignation( Designation designation ){

        return DesignationResponse.builder()
                .nameBn( designation.getNameBn() )
                .nameEn( designation.getNameEn() )
                .id( designation.getId() )
                .build();
    }
}
