package com.dgt.paribahanpool.designation.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.designation.model.Designation;
import com.dgt.paribahanpool.designation.model.DesignationAddRequest;
import com.dgt.paribahanpool.designation.model.DesignationViewResponse;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.userlevel.model.UserLevel;
import com.dgt.paribahanpool.userlevel.model.UserLevelResponse;
import com.dgt.paribahanpool.userlevel.service.UserLevelService;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DesignationValidationService {

    private final MvcUtil mvcUtil;
    private final DesignationService designationService;
    private final UserLevelService userLevelService;

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, DesignationAddRequest designationAddRequest, Long level ){

        Boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                .builder()
                .key( "designationAddRequest" )
                .object( designationAddRequest )
                .title( "title.designation.add" )
                .content( "designation/designation-add" )
                .activeMenu( Menu.DESIGNATION_ADD )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.DESIGNATION_ADD } )
                .build()
        );

        if( !isValid ){

            List<DesignationViewResponse> designationViewResponseList = designationService.getDesignationViewResponseList();
            List<UserLevelResponse> userLevelResponseList = userLevelService.getUserLevelResponseListByLevel( level );

            designationAddRequest.setDesignationLists( designationViewResponseList );
            designationAddRequest.setUserLevelResponseList( userLevelResponseList );

            model.addAttribute( "designationAddRequest", designationAddRequest );
        }

        return isValid;
    }

    public Boolean handleBindingResultForEditFormPost( Model model, BindingResult bindingResult, DesignationAddRequest designationAddRequest ){

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "designationAddRequest" )
                        .object( designationAddRequest )
                        .title( "title.designation.edit" )
                        .content( "designation/designation-add" )
                        .activeMenu( Menu.DESIGNATION_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.DESIGNATION_ADD } )
                        .build()
        );
    }

    public Boolean editDesignation( RedirectAttributes redirectAttributes, Long designationId, Long currentUserLevelLevel ) {

        Optional<Designation> designationOptional = designationService.findById( designationId );

        if( !designationOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;

        } else{

            Designation designation = designationOptional.get();

            if( designation.getUserLevel().getLevel() <= currentUserLevelLevel ){

                mvcUtil.addErrorMessage( redirectAttributes, "validation.user-level.higher-level.edit" );
                return false;
            }

            return true;
        }
    }


    public Boolean editDesignationPost(RedirectAttributes redirectAttributes, DesignationAddRequest designationAddRequest ) {

        Optional<Designation> designationOptional = designationService.findById( designationAddRequest.getId() );

        if( !designationOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    public RestValidationResult delete( Long id ) throws NotFoundException {

        Boolean exist = designationService.existsById( id );
        if( !exist ){

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean addDesignationPost( DesignationAddRequest designationAddRequest, RedirectAttributes redirectAttributes, Long currentUserLevel ) {

        Optional<UserLevel> userLevelOptional = userLevelService.findById( designationAddRequest.getUserLevelId() );

        if( userLevelOptional.isPresent() ){

            UserLevel userLevel = userLevelOptional.get();
            return higherLevelValidation( designationAddRequest, redirectAttributes, currentUserLevel, userLevel.getLevel() );
        }
        else{

            mvcUtil.addErrorMessage( redirectAttributes, "validation.user-level.required" );
            return false;
        }
    }

    private Boolean higherLevelValidation( DesignationAddRequest designationAddRequest, RedirectAttributes redirectAttributes, Long currentUserLevel, Long level ) {

        if( level <= currentUserLevel ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.user-level.higher-level" );
            return false;
        }

        return true;
    }
}
