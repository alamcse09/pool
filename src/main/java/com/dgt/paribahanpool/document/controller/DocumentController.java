package com.dgt.paribahanpool.document.controller;

import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.document.model.DocumentData;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.exceptions.BadRequestException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping( "/doc" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DocumentController extends MVCController {

    private final DocumentService documentService;

    @GetMapping( value = "/{id}" )
    public ResponseEntity<byte[]> getFile(

            @PathVariable( "id" ) Long id

    ) throws IOException, BadRequestException {

        HttpHeaders httpHeaders = new HttpHeaders();

        Optional<DocumentData> documentDataOptional = documentService.findDocumentDataById( id );

        if( documentDataOptional.isPresent() ){

            DocumentData doc = documentDataOptional.get();
            httpHeaders.set( "Content-Type", doc.getContentType() );
            return new ResponseEntity<>( doc.getData(), httpHeaders, HttpStatus.OK );
        }
        else{

            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/group/{id}" )
    public ResponseEntity<byte[]> getSingleFileByGroupId(

            @PathVariable( "id" ) Long id

    ) throws IOException, BadRequestException {

        HttpHeaders httpHeaders = new HttpHeaders();

        Optional<DocumentData> documentDataOptional = documentService.findSingleDocumentByDataGroupId( id );

        if( documentDataOptional.isPresent() ){

            DocumentData doc = documentDataOptional.get();
            httpHeaders.set( "Content-Type", doc.getContentType() );
            return new ResponseEntity<>( doc.getData(), httpHeaders, HttpStatus.OK );
        }
        else{

            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }
}
