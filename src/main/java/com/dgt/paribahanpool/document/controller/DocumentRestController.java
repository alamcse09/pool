package com.dgt.paribahanpool.document.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.rest.RestResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Slf4j
@RestController
@RequestMapping( "/api/doc" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired } )
public class DocumentRestController extends BaseRestController {

    private final DocumentService documentService;

    @DeleteMapping( "{id}" )
    public ResponseEntity<RestResponse> deleteDoc(

            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws IOException {

        documentService.deleteById( id );
        return ResponseEntity.ok(
                RestResponse.builder()
                        .message( localizeUtil.getMessageFromMessageSource( "success.file.deleted.success", "success.file.deleted.success", request ) )
                        .success( true )
                        .build()
        );
    }
}
