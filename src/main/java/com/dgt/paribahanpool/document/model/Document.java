package com.dgt.paribahanpool.document.model;

import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table( name = "document" )
@ToString( exclude = { "data" } )
@EqualsAndHashCode( exclude = { "data" } )
public class Document extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name="id" )
    private Long id;

    @Column( name = "doc_group_id" )
    private Long docGroupId;

    @Column( name="name", length = 4000 )
    private String name;

    @Column( name = "content_type", length = 1000 )
    private String contentType;

    @Column( name = "document_type" )
    private DocumentType documentType;

    @Column( name = "path", length = 2048 )
    private String path;

    @Column( name="data", columnDefinition = "LONGBLOB" )
    private byte[] data;
}
