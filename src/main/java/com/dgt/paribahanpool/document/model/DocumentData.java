package com.dgt.paribahanpool.document.model;

import lombok.Data;

@Data
public class DocumentData{

    private Long id;
    private Long docGroupId;
    private String name;
    private String contentType;
    private DocumentType documentType;
    private String path;
    private byte[] data;
}
