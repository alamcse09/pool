package com.dgt.paribahanpool.document.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "doc_group" )
public class DocumentGroup {

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "doc_group_sequence_generator" )
    @SequenceGenerator( name = "doc_group_sequence_generator", sequenceName = "doc_group_sequencce" )
    @Column( name = "id" )
    private Long id;
}
