package com.dgt.paribahanpool.document.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.SqlResultSetMapping;

@Data
@AllArgsConstructor
public class DocumentMetadata {

    private Long id;
    private Long docGroupId;
    private String name;
    private String contentType;
    private DocumentType documentType;
    private String path;
}
