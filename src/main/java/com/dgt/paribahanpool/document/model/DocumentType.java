package com.dgt.paribahanpool.document.model;

public enum DocumentType {

    DRIVING_LICENSE(1),
    NOC(2);

    private Integer value;

    DocumentType( Integer value ){

        this.value = value;
    }
}
