package com.dgt.paribahanpool.document.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class DocumentUploadedResponse {

    private List<Document> documentList;
    private Document document;
    private Long docGroupId;
}
