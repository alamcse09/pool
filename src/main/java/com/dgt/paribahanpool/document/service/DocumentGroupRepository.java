package com.dgt.paribahanpool.document.service;

import com.dgt.paribahanpool.document.model.DocumentGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentGroupRepository extends JpaRepository<DocumentGroup, Long> {
}
