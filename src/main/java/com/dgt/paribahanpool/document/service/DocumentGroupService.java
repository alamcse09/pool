package com.dgt.paribahanpool.document.service;

import com.dgt.paribahanpool.document.model.DocumentGroup;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired } )
public class DocumentGroupService {

    private final DocumentGroupRepository documentGroupRepository;

    public DocumentGroup save( DocumentGroup documentGroup ){

        return documentGroupRepository.save( documentGroup );
    }

    public Long getNextDocGroupId(){

        return save( new DocumentGroup() ).getId();
    }
}
