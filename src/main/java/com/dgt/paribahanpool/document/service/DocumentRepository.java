package com.dgt.paribahanpool.document.service;

import com.dgt.paribahanpool.document.model.Document;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface DocumentRepository extends JpaRepository<Document,Long> {

    String metadataSelectQuery = "SELECT new com.dgt.paribahanpool.document.model.DocumentMetadata( id, docGroupId, name, contentType, documentType, path ) FROM Document";

    @Query( value = metadataSelectQuery + " WHERE docGroupId = :groupId and isDeleted = false" )
    List<DocumentMetadata> findMetadataByDocGroupId( @Param( "groupId" ) Long groupId );

    @Query( value = metadataSelectQuery + " WHERE id = :id and isDeleted = false" )
    Optional<DocumentMetadata> findMetadataById( @Param( "id" ) Long id );

    Optional<Document> findByIdAndIsDeleted( Long id, Boolean isDeleted );

    List<Document> findByDocGroupIdAndIsDeleted( Long docGroupId, Boolean isDeleted );
}
