package com.dgt.paribahanpool.document.service;

import com.dgt.paribahanpool.config.AppProperties;
import com.dgt.paribahanpool.document.model.*;
import com.dgt.paribahanpool.exceptions.BadRequestException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired } )
public class DocumentService {

    private final DocumentRepository documentRepository;
    private final DocumentGroupService documentGroupService;
    private final AppProperties appProperties;

    private Path rootPath;

    @PostConstruct
    public void postConstruct() throws IOException {

        rootPath = Files.createDirectories( Paths.get( appProperties.getFileupload().getRoot() ) );
    }

    private Document save( Document document ){

        return documentRepository.save( document );
    }

    public List<Document> save( List<Document> documentList ){

        return documentRepository.saveAll( documentList );
    }

    public Optional<Document> findById( Long id ) {

        return documentRepository.findByIdAndIsDeleted( id, false );
    }

    public void deleteById( Long id ) throws IOException {

        Optional<Document> documentOptional = findById( id );

        if( documentOptional.isPresent() ){

            Document document = documentOptional.get();

            documentRepository.delete( document );

            Path finalPath = rootPath.resolve( document.getPath() );
            Files.delete( finalPath );
        }
    }

    public void sofDeleteById( Long id ){

        Optional<Document> documentOptional = findById( id );

        if( documentOptional.isPresent() ) {

            Document document = documentOptional.get();
            document.setIsDeleted(true);
            save(document);
        }
    }

    public Optional<DocumentData> findDocumentDataById( Long id ) throws IOException {

        Optional<Document> documentOptional = findById( id );

        if( documentOptional.isPresent() ){

            Document document = documentOptional.get();
            return Optional.of( getDocumentDataFromDocument( document ) );
        }

        return Optional.empty();
    }

    public Optional<DocumentData> findSingleDocumentByDataGroupId( Long id ) throws IOException, BadRequestException {

        List<DocumentMetadata> documentMetadataList = findDocumentMetaDataListByGroupId( id );

        if( documentMetadataList.size() > 1 || documentMetadataList.size() == 0 ){

            throw new BadRequestException( "Multiple or no document found with given group id" );
        }

        DocumentMetadata documentMetadata = documentMetadataList.get( 0 );
        return findDocumentDataById( documentMetadata.getId() );
    }

    public List<DocumentMetadata> findDocumentMetaDataListByGroupId(Long groupId ){

        return documentRepository.findMetadataByDocGroupId( groupId );
    }

    public List<DocumentData> findDocumentDataListByGroupId( Long docGroupId ){

        List<Document> documentList = documentRepository.findByDocGroupIdAndIsDeleted( docGroupId, false );
        return documentList.stream()
                .map(
                        document -> {
                            try{
                                DocumentData documentData = getDocumentDataFromDocument( document );
                                return documentData;
                            }
                            catch (IOException ex ){
                                return null;
                            }
                        }
                )
                .filter( documentData -> documentData != null )
                .collect(Collectors.toList());
    }

    public DocumentData getDocumentDataFromDocument( Document document ) throws IOException {

        Path finalPath = rootPath.resolve( document.getPath() );
        byte[] data = Files.readAllBytes( finalPath );

        DocumentData documentData = new DocumentData();

        documentData.setName( document.getName() );
        documentData.setContentType( document.getContentType() );
        documentData.setDocumentType( document.getDocumentType() );
        documentData.setDocGroupId( document.getDocGroupId() );
        documentData.setData( data );

        return documentData;
    }

    public Optional<DocumentMetadata> findDocumentMetadataById(Long id ) throws BadRequestException{

        return documentRepository.findMetadataById( id );
    }

    public List<DocumentMetadata> getDocumentListByGroupId(Long groupId){

        return documentRepository.findMetadataByDocGroupId( groupId );
    }

    public DocumentUploadedResponse buildFromMultipartFile( MultipartFile file, DocumentType documentType ) throws IOException {

        Long docGroupId = documentGroupService.getNextDocGroupId();
        Document document = buildFromMultipartFile( file, docGroupId, documentType );

        if( document != null ) {
            save(document);
            return DocumentUploadedResponse.builder().document( document ).docGroupId( docGroupId ).build();
        }

        return DocumentUploadedResponse.builder().docGroupId( docGroupId ).build();
    }

    public Document buildFromMultipartFile( MultipartFile file, Long docGroupId, DocumentType documentType ) throws IOException {

        if( file.isEmpty() )
            return null;

        String path = LocalDate.now().format( DateTimeFormatter.ofPattern( "dd_MM_yyyy" ) ) + "/" + UUID.randomUUID() + "_" + file.getOriginalFilename();
        Path finalPath = rootPath.resolve( path );
        Files.createDirectories( finalPath.getParent() );
        Files.createFile( finalPath );
        Files.copy( file.getInputStream(), finalPath, StandardCopyOption.REPLACE_EXISTING );

        Document document =
                Document
                        .builder()
                        .name( file.getOriginalFilename() )
                        .contentType( file.getContentType() )
                        .documentType( documentType )
                        .docGroupId( docGroupId )
                        .path( path )
                        .build();

        return document;
    }

    public DocumentUploadedResponse buildFromMultipartFile( MultipartFile[] files, Map<MultipartFile, DocumentType> fileDocumentTypeMap ){

        Long docGroupId = documentGroupService.getNextDocGroupId();
        List<Document> documentList = buildFromMultipartFile( files, docGroupId, fileDocumentTypeMap );

        save( documentList );

        return DocumentUploadedResponse.builder().documentList( documentList ).docGroupId( docGroupId ).build();
    }

    public List<Document> buildFromMultipartFile( MultipartFile[] files, Long docGroupId, Map<MultipartFile, DocumentType> fileDocumentTypeMap ){

        return Arrays
                .asList( files )
                .stream()
                .filter( file -> !file.isEmpty() )
                .map(
                        file -> {
                            try {
                                return buildFromMultipartFile( file, docGroupId, fileDocumentTypeMap.get( file ) );
                            } catch (IOException e) {

                                log.error( "Can not read byte data from file. {}", file, e );
                            }
                            return new Document();
                        }
                )
                .filter( document -> document.getPath() != null || document.getData() != null )
                .collect( Collectors.toList() );
    }
}
