package com.dgt.paribahanpool.driver.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.driver.model.Driver;
import com.dgt.paribahanpool.driver.model.DriverAddRequest;
import com.dgt.paribahanpool.driver.model.DriverRequisitionAddRequest;
import com.dgt.paribahanpool.driver.model.DriverRequisitionAdditionalData;
import com.dgt.paribahanpool.driver.service.DriverModelService;
import com.dgt.paribahanpool.driver.service.DriverRequisitionService;
import com.dgt.paribahanpool.driver.service.DriverValidationService;
import com.dgt.paribahanpool.driver_assign.model.DriverAssignAddRequest;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.vehicle.model.RequisitionAdditionalActionData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/driver" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DriverController extends MVCController {

    private final DriverModelService driverModelService;
    private final DriverValidationService driverValidationService;
    private final DriverRequisitionService driverRequisitionService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.driver.add", content = "driver/driver-add", activeMenu = Menu.DRIVER_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.TAB_WIZARD, FrontEndLibrary.DRIVER_ADD_FORM } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering driver add page" );
        driverModelService.addDriverGet( model );
        return viewRoot;
    }

    @PostMapping( value = "/add" )
    public String addDriver(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid DriverAddRequest driverAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !driverValidationService.handleBindingResultForAddFormPost( model, bindingResult, driverAddRequest ) ){

            return viewRoot;
        }

        if( driverValidationService.addDriverPost( redirectAttributes, driverAddRequest ) ){

            Driver driver = driverModelService.addDriverPost( driverAddRequest, redirectAttributes );
            log( LogEvent.DRIVER_ADDED, driver.getId(), "Driver Added", request );
        }

        return "redirect:/driver/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.driver.search", content = "driver/driver-search", activeMenu = Menu.DRIVER_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.DRIVER_SEARCH } )
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Driver Search page called" );
        return viewRoot;
    }

    @GetMapping( "/edit/{driverId}" )
    @TitleAndContent( title = "title.driver.edit", content = "driver/driver-add", activeMenu = Menu.DRIVER_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.TAB_WIZARD, FrontEndLibrary.DRIVER_ADD_FORM } )
    public String edit(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable( "driverId" ) Long driverId
    ){

        log.debug( "Rendering edit Driver Form" );

        if( driverValidationService.editDriver( redirectAttributes, driverId ) ) {

            driverModelService.editDriver( model, driverId );
            return viewRoot;
        }

        return "redirect:/driver/search";
    }

    @PostMapping( "/edit" )
    public String editDriver(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid DriverAddRequest driverAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ) {

        if( !driverValidationService.handleBindingResultForEditFormPost( model, bindingResult, driverAddRequest ) ){

            return viewRoot;
        }

        if( driverValidationService.addDriverPost( redirectAttributes, driverAddRequest ) ){

            Driver driver = driverModelService.addDriverPost( driverAddRequest, redirectAttributes );
            log( LogEvent.DRIVER_EDITED, driver.getId(), "Driver Edited", request );
        }

        return "redirect:/driver/search";
    }

    @GetMapping( "/add-driver-requisition" )
    @TitleAndContent( title = "title.driver.requisition.add", content = "driver/driver-requisition-add", activeMenu = Menu.DRIVER_REQUISITION_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.TAB_WIZARD, FrontEndLibrary.DRIVER_REQUISITION_ADD_FORM } )
    public String addDriverRequisiion(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering driver add page" );
        driverModelService.addDriverRequisitionGet( model );
        return viewRoot;
    }

    @PostMapping( value = "/add-driver-requisition" )
    public String addDriverRequisitionPost(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid DriverRequisitionAddRequest driverRequisitionAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !driverValidationService.handleBindingResultForRequsitionAddFormPost( model, bindingResult, driverRequisitionAddRequest ) ){

            return viewRoot;
        }

        if( driverValidationService.addDriverRequisitionPost( redirectAttributes, driverRequisitionAddRequest ) ){

            driverModelService.addDriverRequisitionPost( driverRequisitionAddRequest, redirectAttributes );
        }

        return "redirect:/driver/driver-requisition/search";
    }
    @GetMapping( "/driver-requisition/search" )
    @TitleAndContent( title = "title.driver.requisition.search", content = "driver/driver-requisition-search", activeMenu = Menu.DRIVER_REQUISITION_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.DRIVER_REQUISTION_SEARCH } )
    public String getDriverRequisitionSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Driver Requisition Search page called" );
        return viewRoot;
    }

    @GetMapping( "/driver-requisition/view/{id}" )
    @TitleAndContent( title = "title.driver.requisition.view", content = "driver/driver-requisition-view", activeMenu = Menu.DRIVER_REQUISITION_VIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.DRIVER_REQUISTION_VIEW } )
    public String getDriverRequisitionView(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable( "id" ) Long id
    ){

        log.debug( "Rendering Driver Requisition View Page" );

        if( driverValidationService.getDriverRequisitionExist( redirectAttributes, id ) ) {

            driverModelService.getDriverRequisition( model, id, getLoggedInUser() );
            return viewRoot;
        }

        return "redirect:/driver/driver-requisition/search";
    }

    @PostMapping( "/driver-requisition/take-action" )
    public String takeAction(
            @RequestParam( "id" ) Long id,
            @RequestParam( "action" ) Long actionId,
            DriverRequisitionAdditionalData driverRequisitionAdditionalData,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ) throws Exception {

        if( driverValidationService.takeAction( redirectAttributes, id, actionId, getLoggedInUser() ) ){

            driverModelService.takeAction( redirectAttributes, id, actionId, driverRequisitionAdditionalData, getLoggedInUser() );
        }
        else{

            return "redirect:/driver/driver-requisition/view/" + id;
        }

        return "redirect:/driver/driver-requisition/search";
    }

}
