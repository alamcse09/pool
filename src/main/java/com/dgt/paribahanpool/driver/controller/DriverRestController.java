package com.dgt.paribahanpool.driver.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.driver.model.DriverRequisitionSearchResponse;
import com.dgt.paribahanpool.driver.service.DriverRequisitionService;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.driver.model.DriverSearchResponse;
import com.dgt.paribahanpool.driver.service.DriverService;
import com.dgt.paribahanpool.driver.service.DriverValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/driver" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DriverRestController extends BaseRestController {

    private final DriverService driverService;
    private final DriverValidationService driverValidationService;
    private final DriverRequisitionService driverRequisitionService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<DriverSearchResponse> searchDriver(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return driverService.searchForDatatable( dataTablesInput );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = driverValidationService.delete( id );

        if( restValidationResult.getSuccess() ){

            driverService.delete( id );
            log( LogEvent.DRIVER_DELETED, id, "", request );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @GetMapping(
            value = "/driver-requisition-search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<DriverRequisitionSearchResponse> searchDriverRequisition(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return driverRequisitionService.searchForDatatable( dataTablesInput );
    }

    @DeleteMapping( "/requisition/{id}" )
    public RestResponse deleteRequisition(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = driverValidationService.deleteRequisition( id );

        if( restValidationResult.getSuccess() ){

            driverRequisitionService.delete( id );
            log( LogEvent.DRIVER_DELETED, id, "", request );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }
}
