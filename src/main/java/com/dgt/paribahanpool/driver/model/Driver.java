package com.dgt.paribahanpool.driver.model;

import com.dgt.paribahanpool.enums.*;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(
        name = "driver",
        uniqueConstraints = {
                @UniqueConstraint( columnNames = { "driver_id" }, name = "uk_driver_driver_id" )
        }
)
@ToString( exclude = { "driverFitnessRecordSet", "driverTransferRecordSet", "driverComplaintRecordSet" } )
@EqualsAndHashCode( exclude = { "driverFitnessRecordSet", "driverTransferRecordSet", "driverComplaintRecordSet" } )
@SQLDelete( sql = "UPDATE driver set is_deleted = true where id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class Driver extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "driver_id" )
    private Long driverId;

    @Column( name = "name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String name;

    @Column( name = "name_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String nameEn;

    @Column( name = "father_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String fatherName;

    @Column( name = "father_name_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String fatherNameEn;

    @Column( name = "mother_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String motherName;

    @Column( name = "mother_name_bn" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String motherNameEn;

    @Column( name = "permanent_address" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String permanentAddress;

    @Column( name = "current_address" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String currentAddress;

    @Column( name = "marital_status" )
    private MaritalStatus maritalStatus;

    @Column( name = "gender_type" )
    private GenderType genderType;

    @Column( name = "driver_type" )
    private DriverType driverType;

    @Column( name = "date_of_birth" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate dateOfBirth;

    @Column( name = "blood_group" )
    private BloodGroup bloodGroup;

    @Column( name = "phone_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String phoneNumber;

    @Column( name = "nid_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String nidNumber;

    @Column( name = "driver_document_group_id" )
    private Long driverDocGroupId;

    @Column( name = "educational_qualification" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String educationalQualification;

    @Column( name = "enrolment_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate enrolmentDate;

    @Column( name = "retirement_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate retirementDate;

    @Column( name = "passport_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String passportNumber;

    @Column( name = "licence_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String licenceNumber;

    @Column( name = "driving_license_type" )
    private DrivingLicenseType drivingLicenseType;

    @Column( name = "licence_expiry_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate licenceExpiryDate;

    @Column( name = "total_experience_duration" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String totalExperienceDuration;

    @Column( name = "police_verification_document_group_id" )
    private Long policeVerificationDocGroupId;

    @OneToOne
    @JoinColumn( name = "current_geolocation_id", foreignKey = @ForeignKey( name = "fk_driver_current_geolocation_id" ) )
    private GeoLocation currentGeolocation;

    @OneToOne
    @JoinColumn( name = "permanent_geolocation_id", foreignKey = @ForeignKey( name = "fk_driver_permanent_geolocation_id" ) )
    private GeoLocation permanentGeolocation;

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(
            name = "driver_driver_fitness_record_map",
            joinColumns = @JoinColumn( name = "driver_id", foreignKey = @ForeignKey( name = "fk_driver_fitness_record_driver_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "driver_fitness_record_id", foreignKey = @ForeignKey( name = "fk_driver_id_driver_fitness_record_map_fitness_record_id" ) )
    )
    private Set<DriverFitnessRecord> driverFitnessRecordSet = new HashSet<>();

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(
            name = "driver_driver_transfer_record_map",
            joinColumns = @JoinColumn( name = "driver_id", foreignKey = @ForeignKey( name = "fk_driver_transfer_record_driver_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "driver_transfer_record_id", foreignKey = @ForeignKey( name = "fk_driver_id_driver_transfer_record_map_transfer_record_id" ) )
    )
    private Set<DriverTransferRecord> driverTransferRecordSet = new HashSet<>();

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(
            name = "driver_driver_complaint_record_map",
            joinColumns = @JoinColumn( name = "driver_id", foreignKey = @ForeignKey( name = "fk_driver_complaint_record_driver_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "driver_complaint_record_id", foreignKey = @ForeignKey( name = "fk_driver_id_driver_complaint_record_map_complaint_record_id" ) )
    )
    private Set<DriverComplaintRecord> driverComplaintRecordSet = new HashSet<>();

    @Column( name = "average_ratings" )
    private Double averageRatings;

    @Column( name = "total_ratings" )
    private Double totalRatings;

    @Column( name = "count_of_people_rated" )
    private Double countOfPeopleRated;

    @Column( name = "comment" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String comment;
}