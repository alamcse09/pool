package com.dgt.paribahanpool.driver.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.*;
import com.dgt.paribahanpool.geolocation.model.GeoLocationResponse;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.validation.annotation.Age;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
public class DriverAddRequest {

    public Long id;

    @NotNull( message = "{validation.common.required}")
    public Long driverId;

//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphabet}" )
    @NotBlank( message = "{validation.common.required}" )
    public String name;

    @NotBlank( message = "{validation.common.required}" )
    public String nameEn;

//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphabet}" )
    @NotBlank( message = "{validation.common.required}" )
    public String fatherName;

    @NotBlank( message = "{validation.common.required}" )
    public String fatherNameEn;

//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphabet}" )
    @NotBlank( message = "{validation.common.required}" )
    public String motherName;

    @NotBlank( message = "{validation.common.required}" )
    public String motherNameEn;

    private List<GeoLocationResponse> districtLists;

    @NotBlank( message = "{validation.common.required}" )
    public String permanentAddress;

    @NotNull( message = "{validation.common.required}" )
    private Long permanentGeolocationId;

    @NotBlank( message = "{validation.common.required}" )
    public String currentAddress;

    @NotNull( message = "{validation.common.required}" )
    private Long currentGeolocationId;

    private MaritalStatus maritalStatus;

    private GenderType genderType;

    private DriverType driverType;

    @Age( min = 18, max = 90 )
    @PastOrPresent( message = "{validation.common.date.past}" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    @NotNull( message = "{validation.common.required}")
    public LocalDate dateOfBirth;

    private BloodGroup bloodGroup;

    @Digits( integer = 11, fraction = 0, message = "{validation.common.numeric}" )
    @NotBlank( message = "{validation.common.required}")
    public String phoneNumber;

    @Digits( integer = 17, fraction = 0, message = "{validation.common.numeric}" )
    @NotBlank( message = "{validation.common.required}")
    public String nidNumber;

    private MultipartFile[] driverFiles;

    List<DocumentMetadata> driverDocuments = Collections.EMPTY_LIST;

    private String educationalQualification;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    public LocalDate enrolmentDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    public LocalDate retirementDate;

    private String passportNumber;

    @NotBlank( message = "{validation.common.required}")
    private String licenceNumber;

    private DrivingLicenseType drivingLicenseType;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    @NotNull( message = "{validation.common.required}")
    public LocalDate licenceExpiryDate;

    @NotBlank( message = "{validation.common.required}")
    private String totalExperienceDuration;

    private MultipartFile[] policeVerificationFiles;

    List<DocumentMetadata> policeVerificationDocuments = Collections.EMPTY_LIST;

    @Valid
    List<DriverFitnessInfoAddRequest> driverFitnessInfoAddRequests;

    @Valid
    List<DriverTransferInfoAddRequest> driverTransferInfoAddRequests;

    @Valid
    List<DriverComplaintInfoAddRequest> driverComplaintInfoAddRequests;

    private MultipartFile[] complaintFiles;

    private Double averageRatings;

    private Double totalRatings;

    private Double countOfPeopleRated;

    private String comment;

}
