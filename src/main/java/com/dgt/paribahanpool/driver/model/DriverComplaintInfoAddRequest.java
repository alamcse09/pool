package com.dgt.paribahanpool.driver.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
public class DriverComplaintInfoAddRequest {

    private Long id;

    @NotBlank( message = "{validation.common.required}" )
    private String complaintId;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate complaintDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate punishmentDate;

    @NotBlank( message = "{validation.common.required}" )
    private String complaintDescription;

    @NotBlank( message = "{validation.common.required}" )
    private String punishmentDescription;

    @NotBlank( message = "{validation.common.required}" )
    private String others;

    @NotBlank( message = "{validation.common.required}" )
    private String comments;

    private MultipartFile[] complaintFiles;

    List<DocumentMetadata> complaintDocuments = Collections.EMPTY_LIST;
}
