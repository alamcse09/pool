package com.dgt.paribahanpool.driver.model;

import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class DriverFitnessInfoAddRequest {

    private Long id;

    @NotBlank( message = "{validation.common.required}" )
    private String driverFitness;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate fitnessDate;
}
