package com.dgt.paribahanpool.driver.model;

import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table( name = "driver_fitness_record" )
public class DriverFitnessRecord {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column( name ="id" )
    private Long id;

    @Column( name = "driver_fitness" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String driverFitness;

    @Column( name = "fitness_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate fitnessDate;
}
