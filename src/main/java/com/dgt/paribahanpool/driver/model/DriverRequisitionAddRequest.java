package com.dgt.paribahanpool.driver.model;

import com.dgt.paribahanpool.ministry.model.MinistryResponse;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

@Data
public class DriverRequisitionAddRequest {

    private Long id;

    private Long ministryId;

    private Integer count;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate fromDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate toDate;

    List<MinistryResponse> ministryResponseList;
}
