package com.dgt.paribahanpool.driver.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.workflow.model.WorkflowAdditionalData;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;
import java.util.Set;

@Data
public class DriverRequisitionAdditionalData implements WorkflowAdditionalData {

    Set<Long> driverId;

    private transient MultipartFile[] rejectReasonsReportFiles;
    private List<DocumentMetadata> rejectReasonsReportDocuments = Collections.EMPTY_LIST;
}
