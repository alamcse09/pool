package com.dgt.paribahanpool.driver.model;

import com.dgt.paribahanpool.enums.DriverType;
import com.dgt.paribahanpool.ministry.model.Ministry;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.WorkflowEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table( name = "driver_requisition_application" )
@SQLDelete( sql = "UPDATE driver_requisition_application set is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class DriverRequisitionApplication extends AuditableEntity implements WorkflowEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "count" )
    private Integer count;

    @Column( name = "from_date" )
    private LocalDate fromDate;

    @Column( name = "to_date" )
    private LocalDate toDate;

    @Column( name = "reject_reason_report_group_id" )
    private Long rejectReasonReportGroupId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.EAGER )
    @JoinColumn( name = "ministry_id", foreignKey = @ForeignKey( name = "fk_driver_requisition_app_ministry_id" ) )
    private Ministry ministry;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "state_id", foreignKey = @ForeignKey( name = "fk_driver_requisition_app_state_id" ) )
    private State state;

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinTable(
            name = "driver_requisition_application_map",
            joinColumns = @JoinColumn( name = "driver_requisition_application_id", foreignKey = @ForeignKey( name = "fk_requisition_driver_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "driver_id", foreignKey = @ForeignKey( name = "fk_driver_id_driver_requisition_application_id" ) )
    )
    private Set<Driver> driverSet = new HashSet<>();
}
