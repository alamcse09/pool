package com.dgt.paribahanpool.driver.model;

import com.dgt.paribahanpool.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table( name = "driver_requisition_history"  )
public class DriverRequisitionHistory {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "assigned_at" )
    private LocalDateTime assignedAt;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "approved_by_id", foreignKey = @ForeignKey( name = "fk_driver_requisition_approved_by_id" ) )
    private User approvedBy;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "driver_requisition_application_id", foreignKey = @ForeignKey( name = "fk_driver_requisition_history_driver_requisition_application" ) )
    private DriverRequisitionApplication driverRequisitionApplication;
}
