package com.dgt.paribahanpool.driver.model;

import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.workflow.model.StateResponse;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class DriverRequisitionSearchResponse {

    private Long id;
    private String ministry;
    private Integer count;
    private LocalDate fromDate;
    private LocalDate toDate;
    private StateResponse stateResponse;
}
