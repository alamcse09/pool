package com.dgt.paribahanpool.driver.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class DriverRequisitionViewResponse {

    private Long id;
    private String ministry;
    private Integer count;
    private LocalDate fromDate;
    private LocalDate toDate;
    public Long stateId;
}
