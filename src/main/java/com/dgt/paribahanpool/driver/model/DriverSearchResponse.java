package com.dgt.paribahanpool.driver.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.DriverType;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class DriverSearchResponse {

    private Long id;
    public Long driverId;
    public String name;
    public String fatherName;
    public String motherName;
    public String phoneNumber;
    public String permanentAddress;
    private DriverType driverType;
    private String totalExperienceDuration;
    public LocalDate enrolmentDate;
    public LocalDate dateOfBirth;
    private List<DocumentMetadata> documentMetadataList;
    public String nidNumber;
}
