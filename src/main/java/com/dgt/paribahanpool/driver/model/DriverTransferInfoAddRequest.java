package com.dgt.paribahanpool.driver.model;

import com.dgt.paribahanpool.enums.DriverType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
public class DriverTransferInfoAddRequest {

    private Long id;

    @PastOrPresent( message = "{validation.common.date.past_or_present}" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    @NotNull( message = "{validation.common.required}")
    private LocalDate startDate;

    @PastOrPresent( message = "{validation.common.date.past_or_present}" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate endDate;

    @NotBlank( message = "{validation.common.required}" )
    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    public String registrationNo;

    @NotBlank( message = "{validation.common.required}" )
    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    public String speedboatEngineNo;

//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphabet}" )
    @NotBlank( message = "{validation.common.required}" )
    public String userName;

//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphabet}" )
    @NotBlank( message = "{validation.common.required}" )
    private String designation;

    @NotBlank( message = "{validation.common.required}" )
    private String workplace;

    @Digits( integer = 11, fraction = 0, message = "{validation.common.numeric}" )
    @NotNull( message = "{validation.common.required}")
    public String phoneNumber;

    public DriverType driveableType;
}
