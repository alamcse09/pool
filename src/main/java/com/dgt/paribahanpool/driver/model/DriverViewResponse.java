package com.dgt.paribahanpool.driver.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DriverViewResponse {

    private Long driverId;
    private String nameBn;
}
