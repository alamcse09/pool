package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.driver.model.DriverComplaintRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DriverComplaintRecordRepository extends JpaRepository<DriverComplaintRecord, Long> {
}
