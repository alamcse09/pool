package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.driver.model.DriverComplaintRecord;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class DriverComplaintRecordService {

    private final DriverComplaintRecordRepository driverComplaintRecordRepository;

    public Optional<DriverComplaintRecord> findById( Long id ){

        return driverComplaintRecordRepository.findById( id );
    }
}
