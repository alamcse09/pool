package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.driver.model.DriverFitnessRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DriverFitnessRecordRepository extends JpaRepository<DriverFitnessRecord, Long> {
}
