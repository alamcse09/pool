package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.driver.model.DriverFitnessRecord;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class DriverFitnessRecordService {

    private final DriverFitnessRecordRepository driverFitnessRecordRepository;

    public Optional<DriverFitnessRecord> findById( Long id ){

        return driverFitnessRecordRepository.findById( id );
    }
}
