package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.driver.model.*;
import com.dgt.paribahanpool.driver_assign.service.DriverAssignService;
import com.dgt.paribahanpool.enums.GeolocationType;
import com.dgt.paribahanpool.geolocation.model.GeoLocationResponse;
import com.dgt.paribahanpool.geolocation.service.GeoLocationService;
import com.dgt.paribahanpool.ministry.model.MinistryResponse;
import com.dgt.paribahanpool.ministry.service.MinistryService;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.workflow.model.StateActionMapResponse;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DriverModelService {

    private final DriverService driverService;
    private final MvcUtil mvcUtil;
    private final GeoLocationService geoLocationService;
    private final MinistryService ministryService;
    private final DriverRequisitionService driverRequisitionService;
    private final StateActionMapService stateActionMapService;
    private final DriverAssignService driverAssignService;

    public void addDriverGet( Model model ){

        DriverAddRequest driverAddRequest = new DriverAddRequest();
        driverAddRequest.setEducationalQualification("Driver");
        driverAddRequest.setTotalExperienceDuration("6 months");
        List<GeoLocationResponse> districtList = geoLocationService.findByGeoLocationType( GeolocationType.DISTRICT );

        driverAddRequest.setDistrictLists( districtList );

        model.addAttribute( "driverAddRequest", driverAddRequest );
    }

    public Driver addDriverPost( DriverAddRequest driverAddRequest, RedirectAttributes redirectAttributes ){

        Driver driver = driverService.save( driverAddRequest );

        if( driverAddRequest.getId() != null ){

            mvcUtil.addSuccessMessage( redirectAttributes, "success.driver.edit" );
        }
        else{

            mvcUtil.addSuccessMessage( redirectAttributes, "success.driver.add" );
        }

        return driver;
    }

    @Transactional
    public void editDriver( Model model, Long driverId ){

        Optional<Driver> driver = driverService.findById( driverId );
        if( driver.isPresent() ){
            DriverAddRequest driverAddRequest = driverService.getDriverAddRequestFromDriver( driver.get() );
            List<GeoLocationResponse> districtList = geoLocationService.findByGeoLocationType( GeolocationType.DISTRICT );
            driverAddRequest.setDistrictLists( districtList );
            model.addAttribute( "driverAddRequest", driverAddRequest );
        }
    }

    public void addDriverRequisitionGet(Model model) {

        DriverRequisitionAddRequest driverRequisitionAddRequest = new DriverRequisitionAddRequest();
        List<MinistryResponse> ministryResponseList = ministryService.findAllMinistryResponse();
        driverRequisitionAddRequest.setMinistryResponseList(ministryResponseList);
        model.addAttribute("driverRequisitionAddRequest",driverRequisitionAddRequest);
    }

    public void addDriverRequisitionPost(DriverRequisitionAddRequest driverRequisitionAddRequest, RedirectAttributes redirectAttributes) {

        driverRequisitionService.save(driverRequisitionAddRequest);
        mvcUtil.addSuccessMessage( redirectAttributes, "success.driver.requisition.add" );
    }

    @Transactional
    public void getDriverRequisition(Model model, Long id, UserPrincipal loggedInUser) {

        DriverRequisitionViewResponse driverRequisitionViewResponse = driverRequisitionService.getDriverRequisitionViewResponseFromId( id );
        model.addAttribute("driverRequisitionViewResponse", driverRequisitionViewResponse);

        Optional<DriverRequisitionApplication> optionalDriverRequisitionApplication = driverRequisitionService.findById( id );

        if( optionalDriverRequisitionApplication.isPresent() && optionalDriverRequisitionApplication.get().getState() != null) {

            DriverRequisitionApplication driverRequisitionApplication = optionalDriverRequisitionApplication.get();
            model.addAttribute("stateId", driverRequisitionApplication.getState().getId());

            List<StateActionMapResponse> stateActionMapList = stateActionMapService.findStateActionMapResponseListByCurrentStateAndRoleIdSet( driverRequisitionApplication.getState().getId(), loggedInUser.getRoleIds() );
            model.addAttribute( "stateActionList", stateActionMapList );

            List<DriverViewResponse> driverViewResponseList = driverAssignService.getListOfFreeDriverViewResponse();
            model.addAttribute( "driverViewResponseList", driverViewResponseList );
        }
    }

    public void takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, DriverRequisitionAdditionalData driverRequisitionAdditionalData, UserPrincipal loggedInUser) throws Exception {

        driverRequisitionService.takeAction( id, actionId, driverRequisitionAdditionalData, loggedInUser );

        if( actionId == AppConstants.DRIVER_REJECTION_APPROVE_ACTION_ID ){

            mvcUtil.addSuccessMessage(redirectAttributes, "success.vehicle.requisition.given");
        }
        else {

            mvcUtil.addSuccessMessage(redirectAttributes, "success.common.action.success");
        }
    }
}
