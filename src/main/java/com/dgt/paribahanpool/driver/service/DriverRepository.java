package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.driver.model.Driver;
import com.dgt.paribahanpool.driver.model.DriverSearchResponse;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DriverRepository extends DataTablesRepository<Driver, Long>, JpaRepository<Driver,Long> {

    Optional<Driver> findByDriverId( Long id );

    List<Driver> findByCurrentGeolocation(GeoLocation geoLocation);
}
