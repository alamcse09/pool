package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.driver.model.DriverRequisitionHistory;
import com.dgt.paribahanpool.vehicle.model.VehicleOwnerHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DriverRequisitionHistoryRepository extends JpaRepository<DriverRequisitionHistory, Long> {
}
