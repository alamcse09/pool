package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.driver.model.DriverRequisitionApplication;
import com.dgt.paribahanpool.driver.model.DriverRequisitionHistory;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class DriverRequisitionHistoryService {

    private final DriverRequisitionHistoryRepository driverRequisitionHistoryRepository;
    private final UserService userService;

    public DriverRequisitionHistory save( DriverRequisitionHistory driverRequisitionHistory ){

        return driverRequisitionHistoryRepository.save( driverRequisitionHistory );
    }

    public void saveRequisition(DriverRequisitionApplication driverRequisitionApplication, UserPrincipal loggedInUser) {

        DriverRequisitionHistory driverRequisitionHistory = new DriverRequisitionHistory();

        Optional<User> optionalUser = userService.findById( loggedInUser.getUser().getId() );

        if( optionalUser.isPresent() ) {

            driverRequisitionHistory.setApprovedBy(optionalUser.get());
        }
        driverRequisitionHistory.setAssignedAt( LocalDateTime.now() );
        driverRequisitionHistory.setDriverRequisitionApplication( driverRequisitionApplication );

        save( driverRequisitionHistory );
    }
}
