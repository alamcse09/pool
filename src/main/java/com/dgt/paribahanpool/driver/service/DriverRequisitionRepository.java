package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.driver.model.DriverRequisitionApplication;
import com.dgt.paribahanpool.driver.model.DriverRequisitionSearchResponse;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DriverRequisitionRepository extends DataTablesRepository<DriverRequisitionApplication,Long> {
}
