package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.driver.model.*;
import com.dgt.paribahanpool.ministry.model.Ministry;
import com.dgt.paribahanpool.ministry.service.MinistryService;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.vehicle.model.VehicleRequisitionApplication;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.StateResponse;
import com.dgt.paribahanpool.workflow.model.WorkflowEntity;
import com.dgt.paribahanpool.workflow.model.WorkflowService;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class DriverRequisitionService implements WorkflowService<DriverRequisitionApplication> {

    private final DriverRequisitionRepository driverRequisitionRepository;
    private final MinistryService ministryService;
    private final StateService stateService;
    private final StateActionMapService stateActionMapService;
    private final DriverService driverService;
    private final DocumentService documentService;
    private final DriverRequisitionHistoryService driverRequisitionHistoryService;

    public Optional<DriverRequisitionApplication> findById(Long id) {
        return driverRequisitionRepository.findById( id );
    }

    public DriverRequisitionApplication save(DriverRequisitionApplication driverRequisitionApplication ){

        return driverRequisitionRepository.save( driverRequisitionApplication );
    }

    public DriverRequisitionApplication save( DriverRequisitionAddRequest driverRequisitionAddRequest ){

        DriverRequisitionApplication driverRequisitionApplication = getDriverRequistionApplicationFromDriverRequisitionAddRequest( driverRequisitionAddRequest );

        return save( driverRequisitionApplication );
    }

    private DriverRequisitionApplication getDriverRequistionApplicationFromDriverRequisitionAddRequest(DriverRequisitionAddRequest driverRequisitionAddRequest) {

        Ministry ministry = ministryService.getReference( driverRequisitionAddRequest.getMinistryId() );

        DriverRequisitionApplication driverRequisitionApplication = new DriverRequisitionApplication();

        driverRequisitionApplication.setFromDate( driverRequisitionAddRequest.getFromDate() );
        driverRequisitionApplication.setToDate( driverRequisitionAddRequest.getToDate() );
        driverRequisitionApplication.setCount( driverRequisitionAddRequest.getCount() );
        driverRequisitionApplication.setMinistry( ministry );

        if( driverRequisitionApplication.getState() == null ){

            State state = stateService.findStateReferenceById(AppConstants.DRIVER_REQUISITION_APPLIED_STATE_ID);
            driverRequisitionApplication.setState( state );
        }

        return driverRequisitionApplication;
    }

    public DataTablesOutput<DriverRequisitionSearchResponse> searchForDatatable(DataTablesInput dataTablesInput) {
        return driverRequisitionRepository.findAll(dataTablesInput, this::getDriverRequisitionSearchResponseFromDriverRequisitionApplication);
    }

    public DriverRequisitionSearchResponse getDriverRequisitionSearchResponseFromDriverRequisitionApplication(DriverRequisitionApplication driverRequisitionApplication){

        DriverRequisitionSearchResponse driverRequisitionSearchResponse = new DriverRequisitionSearchResponse();

        driverRequisitionSearchResponse.setId(driverRequisitionApplication.getId());
        driverRequisitionSearchResponse.setFromDate(driverRequisitionApplication.getFromDate());
        driverRequisitionSearchResponse.setToDate(driverRequisitionApplication.getToDate());
        driverRequisitionSearchResponse.setCount(driverRequisitionApplication.getCount());
        driverRequisitionSearchResponse.setMinistry(driverRequisitionApplication.getMinistry().getNameBn());

        State state = driverRequisitionApplication.getState();
        if( state != null ) {

            StateResponse stateResponse = StateResponse.builder()
                    .id(state.getId())
                    .name(state.getName())
                    .nameEn(state.getNameEn())
                    .stateType(state.getStateType())
                    .build();

            driverRequisitionSearchResponse.setStateResponse(stateResponse);
        }

        return driverRequisitionSearchResponse;
    }

    public void delete(Long id) {
        driverRequisitionRepository.deleteById(id);
    }

    public Boolean existById(Long id) {
        return driverRequisitionRepository.existsById( id );
    }

    public DriverRequisitionViewResponse getDriverRequisitionViewResponseFromId(Long id) {

        DriverRequisitionViewResponse driverRequisitionViewResponse = new DriverRequisitionViewResponse();

        Optional<DriverRequisitionApplication> optionalDriverRequisitionApplication = findById( id );

        if( optionalDriverRequisitionApplication.isPresent() ){

            DriverRequisitionApplication driverRequisitionApplication = optionalDriverRequisitionApplication.get();

            driverRequisitionViewResponse.setId(driverRequisitionApplication.getId());
            driverRequisitionViewResponse.setFromDate(driverRequisitionApplication.getFromDate());
            driverRequisitionViewResponse.setToDate(driverRequisitionApplication.getToDate());
            driverRequisitionViewResponse.setCount(driverRequisitionApplication.getCount());
            driverRequisitionViewResponse.setMinistry(driverRequisitionApplication.getMinistry().getNameBn());
            driverRequisitionViewResponse.setStateId( driverRequisitionApplication.getState() != null? driverRequisitionApplication.getState().getId(): null );

        }

        return driverRequisitionViewResponse;
    }

    @Transactional
    public void takeAction(Long id, Long actionId, DriverRequisitionAdditionalData driverRequisitionAdditionalData, UserPrincipal loggedInUser) {

        stateActionMapService.takeAction( this, id, actionId, loggedInUser, ( workflowEntity ) ->  setAdditionalData( workflowEntity, actionId, driverRequisitionAdditionalData, loggedInUser ) );
    }

    private DriverRequisitionAdditionalData setAdditionalData(WorkflowEntity workflowEntity, Long actionId, DriverRequisitionAdditionalData driverRequisitionAdditionalData, UserPrincipal loggedInUser) {

        DriverRequisitionApplication driverRequisitionApplication = ( DriverRequisitionApplication ) workflowEntity;

        if( actionId == AppConstants.DRIVER_REQUSITION_CHOOSE_DRIVER_ACTION_ID && driverRequisitionAdditionalData.getDriverId() != null && driverRequisitionAdditionalData.getDriverId().size() > 0){

            Set<Driver> driverSet = new HashSet<>();

            driverRequisitionAdditionalData.getDriverId().forEach( (driverID) -> {

                Driver driver = driverService.findReferenceById( driverID );
                driverSet.add( driver );
            });

            driverRequisitionApplication.getDriverSet().addAll( driverSet );
        } else if( actionId == AppConstants.DRIVER_REQUSITION_REJECT_ACTION_ID ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( driverRequisitionAdditionalData.getRejectReasonsReportFiles(), Collections.EMPTY_MAP );
            driverRequisitionApplication.setRejectReasonReportGroupId( documentUploadedResponse.getDocGroupId() );
        } else if( actionId == AppConstants.DRIVER_REJECTION_APPROVE_ACTION_ID ){

            driverRequisitionHistoryService.saveRequisition( ((DriverRequisitionApplication) workflowEntity), loggedInUser );
        }

        return driverRequisitionAdditionalData;
    }
}
