package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.document.model.Document;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.driver.model.*;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.geolocation.service.GeoLocationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DriverService {

    private final DriverRepository driverRepository;

    private final DocumentService documentService;
    private final DriverFitnessRecordService driverFitnessRecordService;
    private final DriverTransferRecordService driverTransferRecordService;
    private final DriverComplaintRecordService driverComplaintRecordService;
    private final GeoLocationService geoLocationService;

    public Optional<Driver> findById( Long id ){

        return driverRepository.findById( id );
    }

    @Transactional
    public Driver save( DriverAddRequest driverAddRequest ){

        Driver driver = new Driver();

        if( driverAddRequest.getId() != null ){

            driver = findById( driverAddRequest.getId() ).get();
        }

        driver = getDriverFromDriverAddRequest( driver, driverAddRequest );

        return save( driver );
    }

    public Driver save( Driver driver ){

        return driverRepository.save( driver );
    }

    public Set<DriverFitnessRecord> getDriverFitnessRecordSet( Driver driver, DriverAddRequest driverAddRequest ){

        Set<DriverFitnessRecord> driverFitnessRecordSet =
                driverAddRequest.getDriverFitnessInfoAddRequests()
                        .stream()
                        .map( driverFitnessInfoAddRequest -> getDriverFitnessRecordFromDriverFitnessInfoAddRequest( driverFitnessInfoAddRequest, driver ) )
                        .filter( driverFitnessRecord -> driverFitnessRecord != null )
                        .collect( Collectors.toSet() );

        return driverFitnessRecordSet;
    }

    public Set<DriverTransferRecord> getDriverTransferRecord( Driver driver, DriverAddRequest driverAddRequest ){

        Set<DriverTransferRecord> driverTransferRecordSet =
                driverAddRequest.getDriverTransferInfoAddRequests()
                        .stream()
                        .map( driverTransferInfoAddRequest -> getDriverTransferRecordFromDriverTransferInfoAddRequest( driverTransferInfoAddRequest, driver ) )
                        .filter( driverTransferRecord -> driverTransferRecord != null )
                        .collect( Collectors.toSet() );

        return driverTransferRecordSet;
    }

    public Set<DriverComplaintRecord> getDriverComplaintRecordSet( Driver driver, DriverAddRequest driverAddRequest){

        Set<DriverComplaintRecord> driverComplaintRecordSet =
                driverAddRequest.getDriverComplaintInfoAddRequests()
                        .stream()
                        .map( driverComplaintInfoAddRequest -> getDriverComplaintRecordFromDriverComplaintInfoAddRequest( driverComplaintInfoAddRequest, driver ) )
                        .filter( driverComplaintRecord -> driverComplaintRecord != null )
                        .collect( Collectors.toSet() );

        return driverComplaintRecordSet;
    }

    public void delete( Long id ) {

        driverRepository.deleteById( id );
    }

    public Boolean existById( Long id ) {

        return driverRepository.existsById( id );
    }

    public Driver getDriverFromDriverAddRequest( Driver driver, DriverAddRequest driverAddRequest ){

        driver.setDriverId( driverAddRequest.getDriverId() );
        driver.setName( driverAddRequest.getName() );
        driver.setNameEn( driverAddRequest.getNameEn() );
        driver.setFatherName( driverAddRequest.getFatherName() );
        driver.setFatherNameEn( driverAddRequest.getFatherNameEn() );
        driver.setMotherName( driverAddRequest.getMotherName() );
        driver.setMotherNameEn( driverAddRequest.getMotherNameEn() );
        driver.setPermanentAddress( driverAddRequest.getPermanentAddress() );
        driver.setCurrentAddress( driverAddRequest.getCurrentAddress() );
        driver.setMaritalStatus( driverAddRequest.getMaritalStatus() );
        driver.setGenderType( driverAddRequest.getGenderType() );
        driver.setDriverType( driverAddRequest.getDriverType() );
        driver.setDateOfBirth( driverAddRequest.getDateOfBirth() );
        driver.setBloodGroup( driverAddRequest.getBloodGroup() );
        driver.setPhoneNumber( driverAddRequest.getPhoneNumber() );
        driver.setNidNumber( driverAddRequest.getNidNumber() );
        driver.setEducationalQualification( driverAddRequest.getEducationalQualification() );
        driver.setEnrolmentDate( driverAddRequest.getEnrolmentDate() );
        driver.setRetirementDate( driverAddRequest.getRetirementDate() );
        driver.setPassportNumber( driverAddRequest.getPassportNumber() );
        driver.setLicenceNumber( driverAddRequest.getLicenceNumber() );
        driver.setLicenceExpiryDate( driverAddRequest.getLicenceExpiryDate() );
        driver.setTotalExperienceDuration( driverAddRequest.getTotalExperienceDuration() );
        driver.setDrivingLicenseType(driverAddRequest.getDrivingLicenseType());

        Optional<GeoLocation> optionalCurrentGeoLocation = geoLocationService.findById( driverAddRequest.getCurrentGeolocationId() );

        if( optionalCurrentGeoLocation.isPresent() ){

            driver.setCurrentGeolocation( optionalCurrentGeoLocation.get() );
        }

        Optional<GeoLocation> optionalPermanentGeoLocation = geoLocationService.findById( driverAddRequest.getPermanentGeolocationId() );

        if( optionalPermanentGeoLocation.isPresent() ){

            driver.setPermanentGeolocation( optionalPermanentGeoLocation.get() );
        }


        if( driver.getDriverDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( driverAddRequest.getDriverFiles(), Collections.EMPTY_MAP );
            driver.setDriverDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( driverAddRequest.getDriverFiles(), driver.getDriverDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        if( driver.getPoliceVerificationDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( driverAddRequest.getPoliceVerificationFiles(), Collections.EMPTY_MAP );
            driver.setPoliceVerificationDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( driverAddRequest.getPoliceVerificationFiles(), driver.getPoliceVerificationDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        if( driverAddRequest.getDriverFitnessInfoAddRequests() != null ){

            Set<DriverFitnessRecord> driverFitnessRecordSet = getDriverFitnessRecordSet( driver, driverAddRequest );
            driver.getDriverFitnessRecordSet().clear();
            driver.getDriverFitnessRecordSet().addAll( driverFitnessRecordSet );
        }

        if( driverAddRequest.getDriverTransferInfoAddRequests() != null ){

            Set<DriverTransferRecord> driverTransferRecordSet = getDriverTransferRecord( driver, driverAddRequest );
            driver.getDriverTransferRecordSet().clear();
            driver.getDriverTransferRecordSet().addAll( driverTransferRecordSet );
        }

        if( driverAddRequest.getDriverComplaintInfoAddRequests() != null ){

            Set<DriverComplaintRecord> driverComplaintRecordSet = getDriverComplaintRecordSet( driver, driverAddRequest );
            driver.getDriverComplaintRecordSet().clear();
            driver.getDriverComplaintRecordSet().addAll( driverComplaintRecordSet );
        }

        return driver;
    }

    public DriverFitnessRecord getDriverFitnessRecordFromDriverFitnessInfoAddRequest( DriverFitnessInfoAddRequest driverFitnessInfoAddRequest, Driver driver ){

        DriverFitnessRecord driverFitnessRecord = new DriverFitnessRecord();

        if( driverFitnessInfoAddRequest.getId() != null && driverFitnessInfoAddRequest.getId() > 0 ){

            Optional<DriverFitnessRecord> driverFitnessRecordOptional = driverFitnessRecordService.findById( driverFitnessInfoAddRequest.getId() );
            if( driverFitnessRecordOptional.isPresent() ){

                driverFitnessRecord = driverFitnessRecordOptional.get();
            }
            else{

                return null;
            }
        }

        driverFitnessRecord.setDriverFitness( driverFitnessInfoAddRequest.getDriverFitness() );
        driverFitnessRecord.setFitnessDate( driverFitnessInfoAddRequest.getFitnessDate() );

        return driverFitnessRecord;
    }

    public DriverTransferRecord getDriverTransferRecordFromDriverTransferInfoAddRequest( DriverTransferInfoAddRequest driverTransferInfoAddRequest, Driver driver ){

        DriverTransferRecord driverTransferRecord = new DriverTransferRecord();

        if( driverTransferInfoAddRequest.getId() != null && driverTransferInfoAddRequest.getId() > 0 ){

            Optional<DriverTransferRecord> driverTransferRecordOptional = driverTransferRecordService.findById( driverTransferInfoAddRequest.getId() );
            if( driverTransferRecordOptional.isPresent() ){

                driverTransferRecord = driverTransferRecordOptional.get();
            }
            else {
                return null;
            }
        }

        driverTransferRecord.setStartDate( driverTransferInfoAddRequest.getStartDate() );
        driverTransferRecord.setEndDate( driverTransferInfoAddRequest.getEndDate() );
        driverTransferRecord.setRegistrationNo( driverTransferInfoAddRequest.getRegistrationNo() );
        driverTransferRecord.setSpeedboatEngineNo( driverTransferInfoAddRequest.getSpeedboatEngineNo() );
        driverTransferRecord.setUserName( driverTransferInfoAddRequest.getUserName() );
        driverTransferRecord.setDesignation( driverTransferInfoAddRequest.getDesignation() );
        driverTransferRecord.setWorkplace( driverTransferInfoAddRequest.getWorkplace() );
        driverTransferRecord.setPhoneNumber( driverTransferInfoAddRequest.getPhoneNumber() );
        driverTransferRecord.setDriveableType( driverTransferInfoAddRequest.getDriveableType() );

        return driverTransferRecord;
    }

    public DriverComplaintRecord getDriverComplaintRecordFromDriverComplaintInfoAddRequest( DriverComplaintInfoAddRequest driverComplaintInfoAddRequest, Driver driver ){

        DriverComplaintRecord driverComplaintRecord = new DriverComplaintRecord();

        if( driverComplaintInfoAddRequest.getId() != null && driverComplaintInfoAddRequest.getId() > 0 ){

            Optional<DriverComplaintRecord> driverComplaintRecordOptional = driverComplaintRecordService.findById( driverComplaintInfoAddRequest.getId() );
            if( driverComplaintRecordOptional.isPresent() ){

                driverComplaintRecord = driverComplaintRecordOptional.get();
            }
            else{

                return null;
            }
        }

        driverComplaintRecord.setComplaintId( driverComplaintInfoAddRequest.getComplaintId() );
        driverComplaintRecord.setComplaintDate( driverComplaintInfoAddRequest.getComplaintDate() );
        driverComplaintRecord.setComplaintDescription( driverComplaintInfoAddRequest.getComplaintDescription() );
        driverComplaintRecord.setPunishmentDate( driverComplaintInfoAddRequest.getPunishmentDate() );
        driverComplaintRecord.setPunishmentDescription( driverComplaintInfoAddRequest.getPunishmentDescription() );
        driverComplaintRecord.setOthers( driverComplaintInfoAddRequest.getOthers() );
        driverComplaintRecord.setComments( driverComplaintInfoAddRequest.getComments() );

        if( driverComplaintRecord.getComplaintDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( driverComplaintInfoAddRequest.getComplaintFiles(), Collections.EMPTY_MAP );
            driverComplaintRecord.setComplaintDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( driverComplaintInfoAddRequest.getComplaintFiles(), driverComplaintRecord.getComplaintDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        return driverComplaintRecord;
    }

    public DriverAddRequest getDriverAddRequestFromDriver( Driver driver ) {

        DriverAddRequest driverAddRequest = new DriverAddRequest();

        driverAddRequest.setId(driver.getId());
        driverAddRequest.setDriverId(driver.getDriverId());
        driverAddRequest.setName(driver.getName());
        driverAddRequest.setNameEn( driver.getNameEn() );
        driverAddRequest.setFatherName(driver.getFatherName());
        driverAddRequest.setFatherNameEn( driver.getFatherNameEn() );
        driverAddRequest.setMotherName(driver.getMotherName());
        driverAddRequest.setMotherNameEn( driver.getMotherNameEn() );
        driverAddRequest.setPermanentAddress(driver.getPermanentAddress());
        driverAddRequest.setCurrentAddress(driver.getCurrentAddress());
        driverAddRequest.setMaritalStatus(driver.getMaritalStatus());
        driverAddRequest.setGenderType(driver.getGenderType());
        driverAddRequest.setDriverType(driver.getDriverType());
        driverAddRequest.setDateOfBirth(driver.getDateOfBirth());
        driverAddRequest.setBloodGroup(driver.getBloodGroup());
        driverAddRequest.setPhoneNumber(driver.getPhoneNumber());
        driverAddRequest.setNidNumber(driver.getNidNumber());
        driverAddRequest.setEducationalQualification(driver.getEducationalQualification());
        driverAddRequest.setEnrolmentDate(driver.getEnrolmentDate());
        driverAddRequest.setRetirementDate(driver.getRetirementDate());
        driverAddRequest.setPassportNumber(driver.getPassportNumber());
        driverAddRequest.setLicenceNumber(driver.getLicenceNumber());
        driverAddRequest.setLicenceExpiryDate(driver.getLicenceExpiryDate());
        driverAddRequest.setTotalExperienceDuration(driver.getTotalExperienceDuration());
        driverAddRequest.setCurrentGeolocationId( driver.getCurrentGeolocation() == null? 0L: driver.getCurrentGeolocation().getId());
        driverAddRequest.setPermanentGeolocationId( driver.getPermanentGeolocation() == null? 0L: driver.getPermanentGeolocation().getId());
        driverAddRequest.setDrivingLicenseType(driver.getDrivingLicenseType());

        if ( driver.getDriverDocGroupId() != null && driver.getDriverDocGroupId() > 0 ) {

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( driver.getDriverDocGroupId() );
            driverAddRequest.setDriverDocuments( documentMetadataList );
        }

        if (driver.getPoliceVerificationDocGroupId() != null && driver.getPoliceVerificationDocGroupId() > 0 ) {

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId(driver.getPoliceVerificationDocGroupId());
            driverAddRequest.setPoliceVerificationDocuments(documentMetadataList);
        }

        if ( driver.getDriverFitnessRecordSet() != null ) {

            List<DriverFitnessInfoAddRequest> driverFitnessInfoAddRequests =
                    driver.getDriverFitnessRecordSet()
                            .stream()
                            .map( driverFitnessRecord -> getDriverFitnessInfoRequestFromDriverFitnessRecord( driverFitnessRecord, driverAddRequest ) )
                            .collect( Collectors.toList() );

            driverAddRequest.setDriverFitnessInfoAddRequests( driverFitnessInfoAddRequests );
        }

        if( driver.getDriverTransferRecordSet() != null ){

            List<DriverTransferInfoAddRequest> driverTransferInfoAddRequests =
                    driver.getDriverTransferRecordSet()
                            .stream()
                            .map( driverTransferRecord -> getDriverTransferInfoRequestFromDriverTransferRecord( driverTransferRecord, driverAddRequest ) )
                            .collect( Collectors.toList() );

            driverAddRequest.setDriverTransferInfoAddRequests( driverTransferInfoAddRequests );
        }

        if( driver.getDriverComplaintRecordSet() != null ){

            List<DriverComplaintInfoAddRequest> driverComplaintInfoAddRequests =
                    driver.getDriverComplaintRecordSet()
                    .stream()
                    .map( driverComplaintRecord -> getDriverComplaintInfoRequestFromDriverComplaintRecord( driverComplaintRecord, driverAddRequest ) )
                    .collect( Collectors.toList() );

            driverAddRequest.setDriverComplaintInfoAddRequests( driverComplaintInfoAddRequests );

        }

        return driverAddRequest;
    }

    public DriverFitnessInfoAddRequest getDriverFitnessInfoRequestFromDriverFitnessRecord( DriverFitnessRecord driverFitnessRecord, DriverAddRequest driverAddRequest ){

        DriverFitnessInfoAddRequest driverFitnessInfoAddRequest = new DriverFitnessInfoAddRequest();

        driverFitnessInfoAddRequest.setId( driverFitnessRecord.getId() );
        driverFitnessInfoAddRequest.setDriverFitness( driverFitnessRecord.getDriverFitness() );
        driverFitnessInfoAddRequest.setFitnessDate( driverFitnessRecord.getFitnessDate() );

        return driverFitnessInfoAddRequest;
    }

    public DriverTransferInfoAddRequest getDriverTransferInfoRequestFromDriverTransferRecord( DriverTransferRecord driverTransferRecord, DriverAddRequest driverAddRequest ){

        DriverTransferInfoAddRequest driverTransferInfoAddRequest = new DriverTransferInfoAddRequest();

        driverTransferInfoAddRequest.setId( driverTransferRecord.getId() );
        driverTransferInfoAddRequest.setStartDate( driverTransferRecord.getStartDate() );
        driverTransferInfoAddRequest.setEndDate( driverTransferRecord.getEndDate() );
        driverTransferInfoAddRequest.setRegistrationNo( driverTransferRecord.getRegistrationNo() );
        driverTransferInfoAddRequest.setSpeedboatEngineNo( driverTransferRecord.getSpeedboatEngineNo() );
        driverTransferInfoAddRequest.setUserName( driverTransferRecord.getUserName() );
        driverTransferInfoAddRequest.setDesignation( driverTransferRecord.getDesignation() );
        driverTransferInfoAddRequest.setWorkplace( driverTransferRecord.getWorkplace() );
        driverTransferInfoAddRequest.setPhoneNumber( driverTransferRecord.getPhoneNumber() );
        driverTransferInfoAddRequest.setDriveableType( driverTransferRecord.getDriveableType() );

        return driverTransferInfoAddRequest;
    }

    public DriverComplaintInfoAddRequest getDriverComplaintInfoRequestFromDriverComplaintRecord( DriverComplaintRecord driverComplaintRecord, DriverAddRequest driverAddRequest ) {

        DriverComplaintInfoAddRequest driverComplaintInfoAddRequest = new DriverComplaintInfoAddRequest();

        driverComplaintInfoAddRequest.setId( driverComplaintRecord.getId() );
        driverComplaintInfoAddRequest.setComplaintId( driverComplaintRecord.getComplaintId() );
        driverComplaintInfoAddRequest.setComplaintDate( driverComplaintRecord.getComplaintDate() );
        driverComplaintInfoAddRequest.setComplaintDescription( driverComplaintRecord.getComplaintDescription() );
        driverComplaintInfoAddRequest.setPunishmentDate( driverComplaintRecord.getPunishmentDate() );
        driverComplaintInfoAddRequest.setPunishmentDescription( driverComplaintRecord.getPunishmentDescription() );
        driverComplaintInfoAddRequest.setOthers( driverComplaintRecord.getOthers() );
        driverComplaintInfoAddRequest.setComments( driverComplaintRecord.getComments() );

        if( driverComplaintRecord.getComplaintDocGroupId() != null && driverComplaintRecord.getComplaintDocGroupId() > 0 ) {

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( driverComplaintRecord.getComplaintDocGroupId() );
            driverComplaintInfoAddRequest.setComplaintDocuments( documentMetadataList );
        }

        return driverComplaintInfoAddRequest;
    }


    public List<Driver> getDriverByDriverId( Long driverId ){

        Specification<Driver> driverSpecification = DriverSpecification.filterByDriverId( driverId );
        return driverRepository.findAll( driverSpecification );
    }

    public DriverSearchResponse getDriverSearchResponseFromDriver( Driver driver ){

        DriverSearchResponse driverSearchResponse = new DriverSearchResponse();

        driverSearchResponse.setId( driver.getId() );
        driverSearchResponse.setDriverId( driver.getDriverId() );
        driverSearchResponse.setName( driver.getName() );
        driverSearchResponse.setFatherName( driver.getFatherName() );
        driverSearchResponse.setMotherName( driver.getMotherName() );
        driverSearchResponse.setPhoneNumber( driver.getPhoneNumber() );
        driverSearchResponse.setPermanentAddress( driver.getPermanentAddress() );
        driverSearchResponse.setDriverType( driver.getDriverType() );
        driverSearchResponse.setTotalExperienceDuration( driver.getTotalExperienceDuration() );
        driverSearchResponse.setEnrolmentDate( driver.getEnrolmentDate() );
        driverSearchResponse.setDateOfBirth( driver.getDateOfBirth() );
        driverSearchResponse.setNidNumber( driver.getNidNumber() );

        List<DocumentMetadata> documentDataList = documentService.findDocumentMetaDataListByGroupId( driver.getDriverDocGroupId() );
        driverSearchResponse.setDocumentMetadataList( documentDataList );

        return driverSearchResponse;
    }

    public DataTablesOutput<DriverSearchResponse> searchForDatatable( DataTablesInput dataTablesInput ) {

        return driverRepository.findAll( dataTablesInput, this::getDriverSearchResponseFromDriver  );
    }

    public Optional<Driver> findByDriverId(Long id) {

        return driverRepository.findByDriverId( id );
    }

    public Long getNumberOfDriverCount() {

        return driverRepository.count();
    }

    public Iterable<Driver> findAll() {

        return driverRepository.findAll();
    }

    public List<DriverViewResponse> getDriverViewResponseListFromDriver(List<Driver> getAllFreeDriver) {

        return getAllFreeDriver
                .stream()
                .map( this::getDriverViewResponseFromDriver)
                .collect( Collectors.toList() );
    }

    private DriverViewResponse getDriverViewResponseFromDriver(Driver driver) {

        return DriverViewResponse.builder()
                .driverId( driver.getDriverId() )
                .nameBn( driver.getName() + " (আইডি : " + driver.getDriverId() + ")" )
                .build();
    }

    @Transactional
    public List<DriverSearchResponse> getDriverByDistrictId(Long districtId) {
        List<DriverSearchResponse> driverSearchResponses = new ArrayList<>();
        List<Driver> drivers = driverRepository.findByCurrentGeolocation(geoLocationService.findById(districtId).get());
        for (Driver driver : drivers){
            driverSearchResponses.add(getDriverSearchResponseFromDriver(driver));
        }
        return driverSearchResponses;
    }

    public Boolean existsById(Long id) {

        return driverRepository.existsById( id );
    }

    public Driver findReferenceById(Long driverID) {

        return driverRepository.getById( driverID );
    }
}
