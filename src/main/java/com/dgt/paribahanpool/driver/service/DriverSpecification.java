package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.driver.model.Driver;
import com.dgt.paribahanpool.driver.model.Driver_;
import org.springframework.data.jpa.domain.Specification;

public class DriverSpecification {

    public static Specification<Driver> filterByDriverId( Long driverId ){

        if( driverId == null )
            return Specification.where( null );

        return ( (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(Driver_.DRIVER_ID), driverId.longValue() ) );
    }

    public static Specification<Driver> filterByIsDeleted( Boolean isDeleted ){

        if( isDeleted == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Driver_.IS_DELETED ), isDeleted );
    }
}
