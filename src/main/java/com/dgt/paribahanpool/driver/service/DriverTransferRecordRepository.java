package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.driver.model.DriverTransferRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DriverTransferRecordRepository extends JpaRepository<DriverTransferRecord, Long> {
}
