package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.driver.model.DriverTransferRecord;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class DriverTransferRecordService {

    private final DriverTransferRecordRepository driverTransferRecordRepository;

    public Optional<DriverTransferRecord> findById( Long id ){

        return driverTransferRecordRepository.findById( id );
    }

}
