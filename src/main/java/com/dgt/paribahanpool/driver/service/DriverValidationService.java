package com.dgt.paribahanpool.driver.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.driver.model.Driver;
import com.dgt.paribahanpool.driver.model.DriverAddRequest;
import com.dgt.paribahanpool.driver.model.DriverRequisitionAddRequest;
import com.dgt.paribahanpool.driver.model.DriverRequisitionApplication;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.GeolocationType;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.geolocation.model.GeoLocationResponse;
import com.dgt.paribahanpool.geolocation.service.GeoLocationRepository;
import com.dgt.paribahanpool.geolocation.service.GeoLocationService;
import com.dgt.paribahanpool.mechanic.model.Mechanic;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.VehicleRequisitionApplication;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DriverValidationService {

    private final MvcUtil mvcUtil;
    private final DriverService driverService;
    private final GeoLocationService geoLocationService;
    private final DriverRequisitionService driverRequisitionService;
    private final StateActionMapService stateActionMapService;

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, DriverAddRequest driverAddRequest ){

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "driverAddRequest" )
                .object( driverAddRequest )
                .title( "title.driver.add" )
                .content( "driver/driver-add" )
                .activeMenu( Menu.DRIVER_ADD )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.DRIVER_ADD_FORM } )
                .build();

        boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        if( isValid ){

            isValid = isValid & validateAddDriverUniqueConstraints( model, driverAddRequest.getId(), driverAddRequest.getDriverId(), params );
        }

        if( !isValid ){

            List<GeoLocationResponse> districtList = geoLocationService.findByGeoLocationType( GeolocationType.DISTRICT );
            driverAddRequest.setDistrictLists( districtList );
        }

        return isValid;
    }

    public boolean validateAddDriverUniqueConstraints( Model model, Long id, Long driverId, HandleBindingResultParams params ){

        if( id == null ){

            if( driverService.getDriverByDriverId( driverId ).size() > 0 ){

                return setModelWithError( model, params, "error.driver.driver_id.already.exist" );
            }
        }

        return true;
    }

    public Boolean handleBindingResultForEditFormPost( Model model, BindingResult bindingResult, DriverAddRequest driverAddRequest ){

        HandleBindingResultParams params = HandleBindingResultParams
                        .builder()
                        .key( "driverAddRequest" )
                        .object( driverAddRequest )
                        .title( "title.driver.edit" )
                        .content( "driver/driver-add" )
                        .activeMenu( Menu.DRIVER_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.DRIVER_ADD_FORM } )
                        .build();

        boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        if( !isValid )
            return false;

        return validateEditDriverUniqueConstraints( model, driverAddRequest.getId(), driverAddRequest.getDriverId(), params );
    }

    public Boolean validateEditDriverUniqueConstraints( Model model, Long id, Long driverId, HandleBindingResultParams params ){

        List<Driver> driverList = driverService.getDriverByDriverId( driverId );

        if( driverList.size() > 0 && !driverList.get(0).getId().equals( id ) ){

            return setModelWithError( model, params, "error.driver.driver_id.already.exist" );
        }

        return true;
    }

    public Boolean addDriverPost(RedirectAttributes redirectAttributes, DriverAddRequest driverAddRequest ){

        if( driverAddRequest.getId() != null ){

            Optional<Driver> driver = driverService.findById( driverAddRequest.getId() );

            if( !driver.isPresent() ){

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
                return false;
            }
        }

        return true;
    }

    private boolean setModelWithError( Model model, HandleBindingResultParams params, String errorMessage ){

        mvcUtil.addErrorMessage( model, errorMessage );
        model.addAttribute( params.getKey(), params.getObject() );
        mvcUtil.addTitleAndContent( model, params.getTitle(), params.getContent(), params.getActiveMenu() );

        if( params.getFrontEndLibraries() != null ){

            mvcUtil.addCssAndJsByLibraryName( model, params.getFrontEndLibraries() );
        }

        return false;
    }

    public RestValidationResult delete( Long id ) throws NotFoundException {

        Boolean exist = driverService.existById( id );
        if( !exist ){

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean editDriver( RedirectAttributes redirectAttributes, Long driverId ){

        Optional<Driver> driver = driverService.findById( driverId );

        if( !driver.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    public boolean addDriverRequisitionPost(RedirectAttributes redirectAttributes, DriverRequisitionAddRequest driverRequisitionAddRequest) {
        if( driverRequisitionAddRequest.getId() != null ){

            Optional<Driver> driver = driverService.findById( driverRequisitionAddRequest.getId() );

            if( !driver.isPresent() ){

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
                return false;
            }
        }

        return true;
    }

    public boolean handleBindingResultForRequsitionAddFormPost(Model model, BindingResult bindingResult, DriverRequisitionAddRequest driverRequisitionAddRequest) {
        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "driverRequisitionAddRequest" )
                .object( driverRequisitionAddRequest )
                .title( "title.driver.add" )
                .content( "driver/driver-add" )
                .activeMenu( Menu.DRIVER_ADD )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.DRIVER_ADD_FORM } )
                .build();

        boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        return isValid;
    }

    public RestValidationResult deleteRequisition(Long id) {
        Boolean exist = driverRequisitionService.existById( id );
        if( !exist ){

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean getDriverRequisitionExist(RedirectAttributes redirectAttributes, Long id) {
        if (driverRequisitionService.existById(id)){
            return true;
        }else{
            return false;
        }
    }

    @Transactional
    public Boolean takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, UserPrincipal loggedInUser) {

        Optional<DriverRequisitionApplication> optionalDriverRequisitionApplication = driverRequisitionService.findById( id );

        if( !optionalDriverRequisitionApplication.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }
        else{

            DriverRequisitionApplication driverRequisitionApplication = optionalDriverRequisitionApplication.get();
            State state = driverRequisitionApplication.getState();

            if( !stateActionMapService.actionAllowed( state.getId(), loggedInUser.getRoleIds(), actionId ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.common.action.not-allowed" );
                return false;
            }

            return true;
        }
    }
}
