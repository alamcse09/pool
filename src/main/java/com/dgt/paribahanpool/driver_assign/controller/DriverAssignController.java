package com.dgt.paribahanpool.driver_assign.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.driver_assign.model.DriverAssignAddRequest;
import com.dgt.paribahanpool.driver_assign.service.DriverAssignModelService;
import com.dgt.paribahanpool.driver_assign.service.DriverAssignValidationService;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/driver-assign" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired})
public class DriverAssignController extends MVCController {

    private final DriverAssignModelService driverAssignModelService;
    private final DriverAssignValidationService driverAssignValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.driver.assign.add", content = "driver-assign/driver-assign-add", activeMenu = Menu.DRIVER_ASSIGN_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.DRIVER_ASSIGN_ADD } )
    public String add(

            HttpServletRequest httpServletRequest,
            Model model
    ){

        log.debug( "Rendering Add Driver Assign form" );
        driverAssignModelService.addDriverAssignGet( model );
        return viewRoot;
    }

    @PostMapping( value = "/add" )
    public String addDriverAssign(

            Model model,
            RedirectAttributes redirectAttributes,
            @Valid DriverAssignAddRequest driverAssignAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !driverAssignValidationService.handleBindingResultForAddFormPost( model, bindingResult, driverAssignAddRequest ) ){

            return viewRoot;
        }

        if( driverAssignValidationService.assignDriver( redirectAttributes, driverAssignAddRequest ) ) {

            Vehicle vehicle = driverAssignModelService.addDriverAssignPost(redirectAttributes, driverAssignAddRequest);
            log( LogEvent.DRIVER_ASSIGN_ADD, vehicle.getId(), "Driver Assigned to a Vehicle", request );

            return "redirect:/driver-assign/search";
        }

        return "redirect:/driver-assign/add";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.driver.assign.search", content = "driver-assign/driver-assign-search", activeMenu = Menu.DRIVE_ASSIGN_SEARCH )
    @AddFrontEndLibrary( libraries = {FrontEndLibrary.DRIVER_ASSIGN_SEARCH} )
    public String getSearchPage(

            HttpServletRequest request,
            Model model
    ) {

        log.debug( "Driver-Assign Search page called" );
        return viewRoot;
    }
}
