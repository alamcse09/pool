package com.dgt.paribahanpool.driver_assign.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.driver_assign.model.DriverAssignDriverResponse;
import com.dgt.paribahanpool.driver_assign.model.DriverAssignSearchResponse;
import com.dgt.paribahanpool.driver_assign.model.DriverAssignVehicleResponse;
import com.dgt.paribahanpool.driver_assign.service.DriverAssignService;
import com.dgt.paribahanpool.driver_assign.service.DriverAssignValidationService;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/driver-assign" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DriverAssignRestController extends BaseRestController {

    private final DriverAssignValidationService driverAssignValidationService;
    private final DriverAssignService driverAssignService;
    private final VehicleService vehicleService;

    @GetMapping( "/search" )
    public DataTablesOutput<DriverAssignSearchResponse> search(
            DataTablesInput dataTablesInput
    ){

        return vehicleService.searchForDataTable( dataTablesInput );
    }

    @GetMapping( "/registrationNo/{number}" )
    public RestResponse getFromRegistrationNo(
            @PathVariable( "number" ) String number
    ) throws NotFoundException {

        RestValidationResult restValidationResult = driverAssignValidationService.findByRegistrationNo( number );
        if( restValidationResult.getSuccess() ){

            DriverAssignVehicleResponse driverAssignVehicleResponse = driverAssignService.getDriverAssignVehicleResponseFromRegistrationNo( number, false );
            return RestResponse.builder().success( true ).payload( driverAssignVehicleResponse ).build();
        }
        else{
            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @GetMapping( "/driverId/{id}" )
    public RestResponse getFromDriverId(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = driverAssignValidationService.findByDriverIdAndIsDeleted( id, false );
        if( restValidationResult.getSuccess() ){

            DriverAssignDriverResponse driverAssignDriverResponse = driverAssignService.getDriverAssignDriverResponseFromDriverId( id, request, false );
            return RestResponse.builder().success( true ).payload( driverAssignDriverResponse ).build();
        }
        else{
            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @DeleteMapping( "/driver" )
    public RestResponse deassignDriver(

            @RequestParam( "vehicleId" ) Long vehicleId,
            @RequestParam( "driverId" ) Long driverId
    ){

        RestValidationResult restValidationResult = driverAssignValidationService.deassignDriver( vehicleId, driverId );

        if( restValidationResult.getSuccess() ){

            vehicleService.removeDriver( vehicleId, driverId );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else {

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

}
