package com.dgt.paribahanpool.driver_assign.model;

import com.dgt.paribahanpool.enums.DriverType;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "driver_assign" )
public class DriverAssign extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "reg_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String registrationNo;

    @Column( name = "chassis_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String chassisNo;

    @Column( name = "driver_id" )
    private Long driverId;

    @Column( name = "name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String name;

    @Column( name = "driver_type" )
    private DriverType driverType;

    @Column( name = "user_designation" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String userName;

    @Column( name = "user_workplace" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String userWorkplace;
}
