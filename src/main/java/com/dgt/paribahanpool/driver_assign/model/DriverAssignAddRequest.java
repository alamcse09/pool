package com.dgt.paribahanpool.driver_assign.model;

import com.dgt.paribahanpool.driver.model.DriverViewResponse;
import com.dgt.paribahanpool.enums.DriverType;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@Data
public class DriverAssignAddRequest {

    @Min( value = 1, message = "{validation.common.min}" )
    @NotNull( message = "{validation.common.required}")
    public Long driverId_id;

    @Min( value = 1, message = "{validation.common.min}" )
    @NotNull( message = "{validation.common.required}")
    public Long vehicleId;

    @NotNull( message = "{validation.common.required}")
    public Long driverId;

    @NotBlank( message = "{validation.common.required}" )
//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    public String registrationNo;

    @NotBlank( message = "{validation.common.required}" )
//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    public String chassisNo;

    public String driverType;

//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphabet}" )
    @NotBlank( message = "{validation.common.required}" )
    public String name;

    public String userName;

    public String userWorkplace;

    private List<DriverViewResponse> driverViewResponseList;
}
