package com.dgt.paribahanpool.driver_assign.model;

import lombok.Data;

@Data
public class DriverAssignDriverResponse {

    public Long id;
    public Long driverId;
    public String name;
    public String driverType;
}
