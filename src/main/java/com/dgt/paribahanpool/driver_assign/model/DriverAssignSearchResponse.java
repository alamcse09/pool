package com.dgt.paribahanpool.driver_assign.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DriverAssignSearchResponse {

    private DriverSearchResponse driver;
    private String registrationNo;
    private String chassisNo;
    private String engineNo;
    private Long id;
}
