package com.dgt.paribahanpool.driver_assign.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DriverAssignVehicleResponse {

    private Long vehicleId;
    private String chassisNo;

    private String userNameEn;
    private String userNameBn;
    private String userDesignation;
    private String userDesignationEn;
    private String userPhone;
    private String userOfficeNameEn;
    private String userOfficeNameBn;

    private String driverNameEn;
    private String driverNameBn;
    private String driverPhoneNo;
}
