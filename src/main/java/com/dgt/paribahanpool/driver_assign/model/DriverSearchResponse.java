package com.dgt.paribahanpool.driver_assign.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DriverSearchResponse {

    private Long id;
    private String name;
    public String phoneNumber;
    private String educationalQualification;
    private String totalExperienceDuration;
}
