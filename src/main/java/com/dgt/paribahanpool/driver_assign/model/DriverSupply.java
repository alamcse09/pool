package com.dgt.paribahanpool.driver_assign.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class DriverSupply {

    Long id;

    Long driverId;

    Integer serial;

    String driverName;

    String driverPhoneNumber;

    String userName;

    LocalDate fromDate;

    LocalDate toDate;

    String assignedTo;
}
