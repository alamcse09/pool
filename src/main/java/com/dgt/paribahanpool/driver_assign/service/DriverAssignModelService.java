package com.dgt.paribahanpool.driver_assign.service;

import com.dgt.paribahanpool.driver.model.DriverViewResponse;
import com.dgt.paribahanpool.driver_assign.model.DriverAssignAddRequest;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DriverAssignModelService {

    private final DriverAssignService driverAssignService;
    private final MvcUtil mvcUtil;

    @Transactional
    public void addDriverAssignGet( Model model ){

        List<DriverViewResponse> driverViewResponseList = driverAssignService.getListOfFreeDriverViewResponse();

        DriverAssignAddRequest driverAssignAddRequest = new DriverAssignAddRequest();

        driverAssignAddRequest.setDriverViewResponseList( driverViewResponseList );

        model.addAttribute( "driverAssignAddRequest", driverAssignAddRequest );
    }

    public Vehicle addDriverAssignPost(RedirectAttributes redirectAttributes, DriverAssignAddRequest driverAssignAddRequest ) {

        Vehicle vehicle = driverAssignService.save( driverAssignAddRequest );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.driver.assign.add" );

        return vehicle;
    }
}
