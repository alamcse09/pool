package com.dgt.paribahanpool.driver_assign.service;

import com.dgt.paribahanpool.driver.model.Driver;
import com.dgt.paribahanpool.driver.model.DriverViewResponse;
import com.dgt.paribahanpool.driver.service.DriverService;
import com.dgt.paribahanpool.driver_assign.model.DriverAssignAddRequest;
import com.dgt.paribahanpool.driver_assign.model.DriverAssignDriverResponse;
import com.dgt.paribahanpool.driver_assign.model.DriverAssignVehicleResponse;
import com.dgt.paribahanpool.enums.DriverType;
import com.dgt.paribahanpool.enums.OwnerType;
import com.dgt.paribahanpool.locale.LocalizeUtil;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.model.VehicleOwner;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DriverAssignService {

    private final DriverService driverService;
    private final VehicleService vehicleService;
    private final LocalizeUtil localizeUtil;

    @Transactional
    public Vehicle save( DriverAssignAddRequest driverAssignAddRequest ){

        Optional<Vehicle> vehicleOptional = vehicleService.findById( driverAssignAddRequest.getVehicleId() );
        Optional<Driver> driverOptional = driverService.findById( driverAssignAddRequest.getDriverId_id() );

        Vehicle vehicle = vehicleOptional.get();
        Driver driver = driverOptional.get();

        vehicle.setDriver( driver );

        return vehicleService.save( vehicle );

    }

    @Transactional( propagation = Propagation.REQUIRED )
    public DriverAssignVehicleResponse getDriverAssignVehicleResponseFromRegistrationNo( String number, Boolean isDeleted ) {

        DriverAssignVehicleResponse.DriverAssignVehicleResponseBuilder driverAssignVehicleResponseBuilder = DriverAssignVehicleResponse.builder();

        Optional<Vehicle> vehicleOptional = vehicleService.findByRegistrationNo( number );

        if( vehicleOptional.isPresent() ) {

            Vehicle vehicle = vehicleOptional.get();
            Driver driver = vehicle.getDriver();
            VehicleOwner vehicleOwner = vehicle.getVehicleOwner();

            String ownerNameEn = "";
            String ownerNameBn = "";
            String ownerDesignation = "";
            String ownerDesignationEn = "";
            String ownerPhone = "";
            String ownerOfficeNameEn = "";
            String ownerOfficeNameBn = "";

            if( vehicleOwner != null ){

                if( vehicleOwner.getOwnerType() == OwnerType.USER ){

                    ownerNameBn = vehicleOwner.getUser().getPublicUserInfo().getName();
                    ownerNameEn = vehicleOwner.getUser().getPublicUserInfo().getNameEn();
                    ownerDesignation = vehicleOwner.getUser().getPublicUserInfo().getDesignation();
                    ownerDesignationEn = vehicleOwner.getUser().getPublicUserInfo().getDesignationEn();
                    ownerPhone = vehicleOwner.getUser().getPublicUserInfo().getPhoneNo();
                    ownerOfficeNameBn = vehicleOwner.getUser().getPublicUserInfo().getWorkplace();
                    ownerOfficeNameEn = vehicleOwner.getUser().getPublicUserInfo().getWorkplaceEn();
                }
                else{

                    ownerOfficeNameBn = vehicleOwner.getGeoLocation().getNameBn();
                    ownerOfficeNameEn = vehicleOwner.getGeoLocation().getNameEn();
                }
            }

            driverAssignVehicleResponseBuilder
                    .chassisNo( vehicle.getChassisNo() )
                    .vehicleId( vehicle.getId() )
                    .driverNameBn( driver != null? driver.getName():"" )
                    .driverNameEn( driver != null? driver.getNameEn():"" )
                    .driverPhoneNo( driver != null? driver.getPhoneNumber(): "" )
                    .userNameEn( ownerNameEn )
                    .userNameBn( ownerNameBn )
                    .userDesignation( ownerDesignation )
                    .userDesignationEn( ownerDesignationEn )
                    .userPhone( ownerPhone )
                    .userOfficeNameBn( ownerOfficeNameBn )
                    .userOfficeNameEn( ownerOfficeNameEn );
        }

        return driverAssignVehicleResponseBuilder.build();
    }

    public DriverAssignDriverResponse getDriverAssignDriverResponseFromDriverId(Long id, HttpServletRequest request, Boolean isDeleted){

        DriverAssignDriverResponse driverAssignDriverResponse = new DriverAssignDriverResponse();

        Optional<Driver> driverOptional = driverService.findByDriverId( id );

        if( driverOptional.isPresent() ) {

            Driver driver = driverOptional.get();

            driverAssignDriverResponse.setId( driver.getId() );
            driverAssignDriverResponse.setDriverId(driver.getDriverId());
            driverAssignDriverResponse.setName(driver.getName());
            driverAssignDriverResponse.setDriverType( localizeUtil.getMessageFromMessageSource(DriverType.getLabel( driver.getDriverType() ), request ) );
        }

        return driverAssignDriverResponse;
    }

    public List<DriverViewResponse> getListOfFreeDriverViewResponse() {

        List<Driver> driverList =  Lists.newArrayList(driverService.findAll());

        List<Driver> getAllFreeDriver = vehicleService.getAllFreeDrivers( driverList  );

        return driverService.getDriverViewResponseListFromDriver( getAllFreeDriver );
    }
}
