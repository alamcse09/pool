package com.dgt.paribahanpool.driver_assign.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.driver.model.Driver;
import com.dgt.paribahanpool.driver.service.DriverService;
import com.dgt.paribahanpool.driver_assign.model.DriverAssignAddRequest;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class DriverAssignValidationService {

    private final VehicleService vehicleService;
    private final DriverService driverService;
    private final MvcUtil mvcUtil;

    public Boolean handleBindingResultForAddFormPost( Model model, BindingResult bindingResult, DriverAssignAddRequest driverAssignAddRequest ) {

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "driverAssignAddRequest" )
                        .object( driverAssignAddRequest )
                        .title( "title.driver.assign.add" )
                        .content( "driver-assign/driver-assign-add" )
                        .activeMenu( Menu.DRIVER_ASSIGN_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{FrontEndLibrary.DRIVER_ASSIGN_ADD} )
                        .build()
        );
    }

    public RestValidationResult findByRegistrationNo(String number ) throws NotFoundException {

        Optional<Vehicle> vehicleOptional= vehicleService.findByRegistrationNo( number );
        if( !vehicleOptional.isPresent() ){

            throw new NotFoundException( "validation.driver.assign.registrationNoNotFound" );
        }

        return RestValidationResult.valid();
    }

    public RestValidationResult findByDriverIdAndIsDeleted( Long id, Boolean isDeleted ) throws NotFoundException {

        Optional<Driver> driverOptional = driverService.findByDriverId( id );
        if( !driverOptional.isPresent() ){

            throw new NotFoundException( "validation.driver.assign.driverIdNotFound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean assignDriver(RedirectAttributes redirectAttributes, DriverAssignAddRequest driverAssignAddRequest) {

        Optional<Vehicle> vehicleOptional = vehicleService.findById( driverAssignAddRequest.getVehicleId() );

        if( !vehicleOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.driver.assign.registrationNoNotFound" );
            return false;
        }

        Optional<Driver> driverOptional = driverService.findById( driverAssignAddRequest.getDriverId_id() );

        if( !driverOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.driver.assign.driverIdNotFound" );
            return false;
        }

        Boolean driverAlreadyAssigned = vehicleService.existsByDriverId( driverOptional.get().getId() );

        if( driverAlreadyAssigned ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.driver.assign.already-assigned" );
            return false;
        }

        return true;
    }

    public RestValidationResult deassignDriver(Long vehicleId, Long driverId) {

        Optional<Vehicle> vehicleOptional = vehicleService.findById( vehicleId );

        if( vehicleOptional.isPresent() ){

            Vehicle vehicle = vehicleOptional.get();
            Driver driver = vehicle.getDriver();

            if( driver != null && !driver.getId().equals( driverId ) ){

                return RestValidationResult.builder().success( false ).message( "error.driver.assign.driver-not-assigned-to-this-vehicle" ).build();
            }

            if( driver == null ){

                return RestValidationResult.builder().success( false ).message( "error.driver.assign.driver-not-exist-in-vehicle" ).build();
            }

            return RestValidationResult.valid();
        }
        else{

            return RestValidationResult.builder().success( false ).message( "error.driver.assign.vehicle-not-exist" ).build();
        }
    }
}
