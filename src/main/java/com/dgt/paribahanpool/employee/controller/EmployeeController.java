package com.dgt.paribahanpool.employee.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.employee.model.EmployeeAddRequest;
import com.dgt.paribahanpool.employee.service.EmployeeModelService;
import com.dgt.paribahanpool.employee.service.EmployeeService;
import com.dgt.paribahanpool.employee.service.EmployeeValidationService;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.enums.Quota;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.mechanic.model.MechanicAddRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/employee" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired })
public class EmployeeController extends MVCController {

    private final EmployeeValidationService employeeValidationService;
    private final EmployeeModelService employeeModelService;
    private final EmployeeService employeeService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.employee.add", content = "employee/employee-add", activeMenu = Menu.EMPLOYEE_ADD)
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.EMPLOYEE_ADD_FORM,FrontEndLibrary.TAB_WIZARD })
    public String addEmployee(
            Model model,
            HttpServletRequest httpServletRequest
    ) {

        employeeModelService.addEmployeeGet( model );
        return viewRoot;
    }

    @PostMapping("/add")
    public String addEmployee(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid EmployeeAddRequest employeeAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ) {

        if( !employeeValidationService.handleBindingResultForAddFormPost(model, bindingResult, employeeAddRequest)) {

            return viewRoot;
        }
        if (employeeAddRequest.getQuota() == Quota.FREEDOM_FIGHTER){
            employeeAddRequest.setRetirementDate(employeeAddRequest.getDateOfBirth().plusYears(60));
        }else{
            employeeAddRequest.setRetirementDate(employeeAddRequest.getDateOfBirth().plusYears(59));
        }
        employeeModelService.addEmployeePost( employeeAddRequest, redirectAttributes );
        log( LogEvent.EMPLOYEE_ADDED, employeeAddRequest.getId(), "Employee Added", request );

        return "redirect:/employee/search";

    }

    @GetMapping( "/edit/{id}" )
    @TitleAndContent( title = "title.employee.add", content = "employee/employee-add", activeMenu = Menu.EMPLOYEE_ADD)
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.EMPLOYEE_ADD_FORM,FrontEndLibrary.TAB_WIZARD })
    public String editEmployee(
            HttpServletRequest request,
            RedirectAttributes redirectAttributes,
            @PathVariable("id") Long id,
            Model model
    ) {

        if( employeeValidationService.editEmployee( id, redirectAttributes )) {

            employeeModelService.editEmployeeGet( id, model );
            return viewRoot;
        }

        return "redirect:/employee/add";
    }

    @PostMapping( "/edit" )
    public String editEmployee(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid EmployeeAddRequest employeeAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ) {

        if( !employeeValidationService.handleBindingResultForAddFormPost(model, bindingResult, employeeAddRequest)) {

            return viewRoot;
        }

        if( employeeValidationService.editEmployee( employeeAddRequest.getId(), redirectAttributes )) {

            employeeModelService.addEmployeePost( employeeAddRequest, redirectAttributes );
            log( LogEvent.EMPLOYEE_EDITED, employeeAddRequest.getId(), "Employee Edited", request );
        }
        return "redirect:/employee/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.employee.search", content = "employee/employee-search", activeMenu = Menu.EMPLOYEE_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.EMPLOYEE_SEARCH })
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Employee Search page called" );
        return viewRoot;
    }
}
