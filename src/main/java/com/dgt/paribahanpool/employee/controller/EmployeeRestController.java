package com.dgt.paribahanpool.employee.controller;

import com.dgt.paribahanpool.employee.model.EmployeeSearchResponse;
import com.dgt.paribahanpool.employee.service.EmployeeService;
import com.dgt.paribahanpool.employee.service.EmployeeValidationService;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping( "/api/employee" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class EmployeeRestController {

    private final EmployeeService employeeService;
    private final EmployeeValidationService employeeValidationService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<EmployeeSearchResponse> searchEmployee(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return employeeService.searchForDatatable( dataTablesInput );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id
    ) throws NotFoundException {

        RestValidationResult restValidationResult = employeeValidationService.delete( id );

        if( restValidationResult.getSuccess() ) {

            employeeService.delete( id );

            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        } else {

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }
}
