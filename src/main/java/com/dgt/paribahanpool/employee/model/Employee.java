package com.dgt.paribahanpool.employee.model;

import com.dgt.paribahanpool.complain.model.Complains;
import com.dgt.paribahanpool.designation.model.Designation;
import com.dgt.paribahanpool.enums.*;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table( name = "employee")
@ToString( exclude = { "employeeEducationalInfoSet", "employeeTrainingInfoSet", "complainsSet" } )
@EqualsAndHashCode( exclude = { "employeeEducationalInfoSet", "employeeTrainingInfoSet", "complainsSet" } )
public class Employee extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String name;

    @Column( name = "name_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String nameEn;

    @Column( name = "employee_id" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String employeeID;

    @Column( name = "personal_id" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String personalID;

    @Column( name = "father_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String fatherName;

    @Column( name = "mother_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String motherName;

    @Column( name = "current_working_place" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String currentWorkingPlace;

    @Column( name = "present_address" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String presentAddress;

    @Column( name = "permanent_address" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String permanentAddress;

    @Column( name = "permanent_order_no" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String permanentOrderNo;

    @Column( name = "marital_status" )
    private MaritalStatus maritalStatus;

    @Column( name = "gender" )
    private Gender gender;

    @Column( name = "date_of_birth" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate dateOfBirth;

    @Column( name = "passport_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String passportNumber;

    @Column( name = "blood_group" )
    private BloodGroup bloodGroup;

    @Column( name = "nid" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String nid;

    @Column( name = "mobile_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String mobileNumber;

    @Column( name = "password" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String password;

    @Column( name = "password_again" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String passwordAgain;

    @Column( name = "email" )
    private String email;

    @Column( name = "quota" )
    private Quota quota;

    @Column( name = "number_of_children" )
    private Integer numberOfChildren;

    @Column( name = "appointment_order" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String appointmentOrder;

    @Column( name = "appointment_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate appointmentDate;

    @Column( name = "joining_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate joiningDate;

    @Column( name = "joining_designation" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String joiningDesignation;

    @Column( name = "office_ministry" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String officeMinistry;

    @Column( name = "job_type" )
    private JobType jobType;

    @Column( name = "permanent_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate permanentDate;

    @Column( name = "reporting_manager" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String reportingManager;

    @Column( name = "retirement_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate retirementDate;

    @Column( name = "achievement_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String achievementName;

    @Column( name = "achievement_year" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String achievementYear;

    @Column( name = "achievement_description" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String achievementDescription;

    @Column( name = "joining_grade" )
    private Grade joiningGrade;

    @Column( name = "initial_grade_salary_of_join" )
    private Integer initialGradeSalaryOfJoin;

    @Column( name = "additional_increment_number_of_join" )
    private Integer additionalIncrementNumberOfJoin;

    @Column( name = "step_of_join" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String stepOfJoin;

    @Column( name = "actual_salary_of_join" )
    private Integer actualSalaryOfJoin;

    @Column( name = "current_grade" )
    private Grade currentGrade;

    @Column( name = "current_initial_grade_salary" )
    private Integer currentInitialGradeSalary;

    @Column( name = "current_additional_increment_number" )
    private Integer currentAdditionalIncrementNumber;

    @Column( name = "current_step" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String currentStep;

    @Column( name = "transfer_order_no" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String transferOrderNo;

    @Column( name = "previous_post" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String previousPost;

    @Column( name = "promoted_post" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String promotedPost;

    @Column( name = "promotion_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate promotionDate;

    @Column( name = "transfer_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate transferDate;

    @Column( name = "promotion_order_no" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String promotionOrderNo;

    @Column( name = "current_actual_salary" )
    private Integer currentActualSalary;

    @Column( name = "comments" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String comments;

    @Column( name = "employee_doc_group_id")
    private Long employeeDocGroupId;

    @Column( name = "is_permanent")
    private Integer isPermanent;

    @OneToOne
    @JoinColumn( name = "designation_id", foreignKey = @ForeignKey( name = "fk_employee_desination_id" ) )
    private Designation designation;

    @OneToOne
    @JoinColumn( name = "current_geolocation_id", foreignKey = @ForeignKey( name = "fk_employee_current_geolocation_id" ) )
    private GeoLocation currentGeolocation;

    @OneToOne
    @JoinColumn( name = "current_work_district_id", foreignKey = @ForeignKey( name = "fk_employee_current_work_district_id" ) )
    private GeoLocation currentWorkDistrict;

    @OneToOne
    @JoinColumn( name = "promoted_from_branch", foreignKey = @ForeignKey( name = "fk_employee_promoted_from_geolocation_id" ) )
    private GeoLocation promotedFromBranch;

    @OneToOne
    @JoinColumn( name = "promoted_to_branch", foreignKey = @ForeignKey( name = "fk_employee_promoted_to_geolocation_id" ) )
    private GeoLocation promotedToBranch;

    @OneToOne
    @JoinColumn( name = "transferred_from_branch", foreignKey = @ForeignKey( name = "fk_employee_transferred_from_geolocation_id" ) )
    private GeoLocation transferredFromBranch;

    @OneToOne
    @JoinColumn( name = "transferred_to_branch", foreignKey = @ForeignKey( name = "fk_employee_transferred_to_geolocation_id" ) )
    private GeoLocation transferredToBranch;

    @OneToOne
    @JoinColumn( name = "permanent_geolocation_id", foreignKey = @ForeignKey( name = "fk_employee_permanent_geolocation_id" ) )
    private GeoLocation permanentGeolocation;

    @OneToMany( cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinTable(
            name = "employee_employee_educational_info_map",
            joinColumns = @JoinColumn( name = "employee_id", foreignKey = @ForeignKey( name = "fk_employee_educational_info_employee_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "employee_educational_info_id", foreignKey = @ForeignKey( name = "fk_employee_id_employee_educational_info_map_id" ) )
    )
    private Set<EmployeeEducationalInfo> employeeEducationalInfoSet = new HashSet<>();

    @OneToMany( cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinTable(
            name = "employee_employee_training_info_map",
            joinColumns = @JoinColumn( name = "employee_id", foreignKey = @ForeignKey( name = "fk_employee_training_info_employee_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "employee_training_info_id", foreignKey = @ForeignKey( name = "fk_employee_id_employee_training_info_map_id" ) )
    )
    private Set<EmployeeTrainingInfo> employeeTrainingInfoSet = new HashSet<>();

    @OneToMany( cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinTable(
            name = "employee_complains_map",
            joinColumns = @JoinColumn( name = "employee_id", foreignKey = @ForeignKey( name = "fk_complains_employee_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "complains_id", foreignKey = @ForeignKey( name = "fk_employee_id_complains_map_id" ) )
    )
    private Set<Complains> complainsSet = new HashSet<>();

    @Column( name = "transfer_info" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String transferInfo;

    @Column( name = "promotion_info" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String promotionInfo;

    @Column( name = "attendance_and_leave_info" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String attendanceAndLeaveInfo;

    @Column( name = "achievements" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String achievements;

    @Column( name = "achievement_document_group_id" )
    private Long achievementDocGroupId;

    @Column( name = "transfer_document_group_id" )
    private Long transferDocGroupId;

    @Column( name = "promotion_document_group_id" )
    private Long promotionDocGroupId;

    @Column( name = "appointment_document_group_id" )
    private Long appointmentDocGroupId;

    @Column( name = "current_job_document_group_id" )
    private Long currentJobDocGroupId;
}
