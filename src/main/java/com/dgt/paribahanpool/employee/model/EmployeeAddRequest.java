package com.dgt.paribahanpool.employee.model;

import com.dgt.paribahanpool.complain.model.ComplainsAddRequest;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.*;
import com.dgt.paribahanpool.geolocation.model.GeoLocationResponse;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
public class EmployeeAddRequest {

    private Long id;

    @NotBlank( message = "{validation.common.required}" )
    private String name;

    @NotBlank( message = "{validation.common.required}" )
//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphabet}" )
    private String nameEn;

    private String currentWorkingPlace;

    private String permanentOrderNo;

//    @NotBlank( message = "{validation.common.required}" )
    private String employeeID;

    @NotBlank( message = "{validation.common.required}" )
    private String personalID;

//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphabet}" )
    @NotBlank( message = "{validation.common.required}" )
    private String fatherName;

//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphabet}" )
    @NotBlank( message = "{validation.common.required}" )
    private String motherName;

    private String presentAddress;

    private String permanentAddress;

    private MaritalStatus maritalStatus;

    private Gender gender;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate dateOfBirth;

    private String passportNumber;

    private BloodGroup bloodGroup;

    private String nid;

    private String mobileNumber;

    private String password;

    private String passwordAgain;

    @Email( message = "{validation.common.email}")
    private String email;

    private Quota quota;

    private Integer numberOfChildren;

    private String appointmentOrder;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate appointmentDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate joiningDate;

    private String joiningDesignation;

    private String officeMinistry;

    private JobType jobType;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate permanentDate;

    private String reportingManager;


    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate retirementDate;

    private String achievementName;

    private String achievementYear;

    private String achievementDescription;

    private Grade joiningGrade;

    private Integer initialGradeSalaryOfJoin;

    private Integer additionalIncrementNumberOfJoin;

    private String stepOfJoin;

    private Integer actualSalaryOfJoin;

    private Grade currentGrade;

    private Integer currentInitialGradeSalary;

    private Integer currentAdditionalIncrementNumber;

    private String currentStep;

    private Integer currentActualSalary;

    private String comments;

    private List<GeoLocationResponse> districtLists;

    @NotNull( message = "{validation.common.required}" )
    private Long currentGeolocationId;

    @NotNull( message = "{validation.common.required}" )
    private Long permanentGeolocationId;

    public MultipartFile[] employeeFiles;

    private Integer isPermanent;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate transferDate;

    private String transferOrderNo;

    private Long transferredFromBranch;

    private Long transferredToBranch;

    private String promotionOrderNo;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate promotionDate;

    private String previousPost;

    private String promotedPost;

    private Long promotedFromBranch;

    private Long promotedToBranch;

    private LeaveType leaveType;

    private Integer casualLeaveCount = 0;
    private Integer yearlyLeaveCount = 0;
    private Integer earnedLeaveCount = 0;
    private Integer medicalLeaveCount = 0;
    private Integer abroadLeaveCount = 0;
    private Integer amusementLeaveCount = 0;
    private Integer unpaidLeaveCount = 0;
    private Integer maternityLeaveCount = 0;
    private Integer otherLeaveCount = 0;

    private Long currentWorkDistrict;

    List<DocumentMetadata> documents = Collections.EMPTY_LIST;

    @Valid
    private List<EmployeeEducationalInfoAddRequest> employeeEducationalInfoAddRequests;

    @Valid
    private List<EmployeeTrainingInfoAddRequest> employeeTrainingInfoAddRequests;

    @Valid
    private List<ComplainsAddRequest> complainsAddRequests;

    @NotNull( message = "{validation.common.required}")
    @Min( value = 1, message = "{validation.common.min}")
    private Long designationId;

    public MultipartFile[] educationFiles;

    public MultipartFile[] trainingFiles;

    List<DocumentMetadata> trainingDocuments = Collections.EMPTY_LIST;

    List<DocumentMetadata> appointmentDocuments = Collections.EMPTY_LIST;

    private MultipartFile[] appointmentFiles;

    private MultipartFile[] achievementFiles;

    List<DocumentMetadata> achievementDocuments = Collections.EMPTY_LIST;

    private MultipartFile[] promotionFiles;

    List<DocumentMetadata> promotionDocuments = Collections.EMPTY_LIST;

    private MultipartFile[] transferFiles;

    List<DocumentMetadata> transferDocuments = Collections.EMPTY_LIST;

    private MultipartFile[] currentJobFiles;

    List<DocumentMetadata> currentJobDocuments = Collections.EMPTY_LIST;
}
