package com.dgt.paribahanpool.employee.model;

import com.dgt.paribahanpool.enums.EducationLevel;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table( name = "employee_educational_info")
public class EmployeeEducationalInfo {

    @Id
    @Column( name = "id" )
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;

    @Column( name = "education_level" )
    private EducationLevel educationLevel;

    @Column( name = "degree_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String degreeName;

    @Column( name = "educational_institute" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String educationalInstitute;

    @Column( name = "subject" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String subject;

    @Column( name = "board" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String board;

    @Column( name = "passing_year" )
    private Integer passingYear;

    @Column( name = "result" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String result;

    @Column( name = "education_doc_group_id")
    private Long educationDocGroupId;
}
