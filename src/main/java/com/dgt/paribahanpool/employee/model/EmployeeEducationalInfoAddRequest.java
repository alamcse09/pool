package com.dgt.paribahanpool.employee.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.EducationLevel;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
public class EmployeeEducationalInfoAddRequest {

    private Long id;

    private EducationLevel educationLevel;

    private String degreeName;

    private String educationalInstitute;

    private String subject;

    private String board;

    private Integer passingYear;

    private String result;

    public MultipartFile[] educationFiles;

    public List<DocumentMetadata> educationDocuments = Collections.EMPTY_LIST;
}
