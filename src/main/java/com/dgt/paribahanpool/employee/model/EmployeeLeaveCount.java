package com.dgt.paribahanpool.employee.model;

import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@Entity
@Table( name = "employee_leave_count")
@SQLDelete( sql = "UPDATE employee_leave_count SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class EmployeeLeaveCount extends AuditableEntity {

    @Id
    private Long id;

    @Column( name = "employee_id")
    private Long employeeId;

    @Column( name = "casual_leave_count")
    private Integer casualLeaveCount;

    @Column( name = "yearly_leave_count")
    private Integer yearlyLeaveCount;

    @Column( name = "earned_leave_count")
    private Integer earnedLeaveCount;

    @Column( name = "medical_leave_count")
    private Integer medicalLeaveCount;

    @Column( name = "abroad_leave_count")
    private Integer abroadLeaveCount;

    @Column( name = "amusement_leave_count")
    private Integer amusementLeaveCount;

    @Column( name = "unpaid_leave_count")
    private Integer unpaidLeaveCount;

    @Column( name = "maternity_leave_count")
    private Integer maternityLeaveCount;

    @Column( name = "other_leave_count")
    private Integer otherLeaveCount;

}
