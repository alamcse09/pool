package com.dgt.paribahanpool.employee.model;

import com.dgt.paribahanpool.enums.BloodGroup;
import com.dgt.paribahanpool.enums.Gender;
import com.dgt.paribahanpool.enums.MaritalStatus;
import com.dgt.paribahanpool.enums.Quota;
import lombok.Data;

import java.time.LocalDate;

@Data
public class EmployeeSearchResponse {

    private Long id;

    private String name;

    private String employeeID;

    private String personalID;

    private String fatherName;

    private String motherName;

    private String presentAddress;

    private String permanentAddress;

    private MaritalStatus maritalStatus;

    private Gender gender;

    private LocalDate dateOfBirth;

    private String passportNumber;

    private BloodGroup bloodGroup;

    private String nid;

    private String mobileNumber;

    private String email;

    private Quota quota;

    private Integer numberOfChildren;
}
