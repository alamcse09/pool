package com.dgt.paribahanpool.employee.model;

import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table( name = "employee_training_info")
public class EmployeeTrainingInfo {

    @Id
    @Column( name = "id")
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;

    @Column( name = "training_name")
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String trainingName;

    @Column( name = "training_subject")
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String trainingSubject;

    @Column( name = "organization_name")
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String organizationName;

    @Column( name = "training_doc_group_id")
    private Long trainingDocGroupId;

    @Column( name = "training_date_from" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate trainingDateFrom;

    @Column( name = "training_date_to" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate trainingDateTo;
}
