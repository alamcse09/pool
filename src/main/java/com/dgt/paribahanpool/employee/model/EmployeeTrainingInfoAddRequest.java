package com.dgt.paribahanpool.employee.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
public class EmployeeTrainingInfoAddRequest {

    private Long id;

    private String trainingName;

    private String trainingSubject;

    private String organizationName;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate trainingDate;

    public MultipartFile[] trainingFiles;

    List<DocumentMetadata> trainingDocuments = Collections.EMPTY_LIST;

    private LocalDate trainingDateFrom;
    private LocalDate trainingDateTo;
}
