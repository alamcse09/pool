package com.dgt.paribahanpool.employee.service;

import com.dgt.paribahanpool.employee.model.EmployeeEducationalInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeEducationalInfoRepository extends JpaRepository<EmployeeEducationalInfo, Long> {
}
