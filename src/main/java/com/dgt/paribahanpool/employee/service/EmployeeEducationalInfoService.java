package com.dgt.paribahanpool.employee.service;

import com.dgt.paribahanpool.employee.model.EmployeeEducationalInfo;
import com.dgt.paribahanpool.marine.model.MarineServiceRecord;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class EmployeeEducationalInfoService {

    private final EmployeeEducationalInfoRepository employeeEducationalInfoRepository;

    public Optional<EmployeeEducationalInfo> findById(Long id ){

        return employeeEducationalInfoRepository.findById( id );
    }
}
