package com.dgt.paribahanpool.employee.service;

import com.dgt.paribahanpool.employee.model.EmployeeLeaveCount;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

import java.util.Optional;

public interface EmployeeLeaveCountRepository extends DataTablesRepository<EmployeeLeaveCount,Long> {

    Optional<EmployeeLeaveCount> findEmployeeLeaveCountByEmployeeId(Long employeeId);
}
