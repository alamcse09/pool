package com.dgt.paribahanpool.employee.service;

import com.dgt.paribahanpool.employee.model.EmployeeLeaveCount;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class EmployeeLeaveCountService {

    private final EmployeeLeaveCountRepository employeeLeaveCountRepository;


    public Optional<EmployeeLeaveCount> getLeaveCountByEmployeeId(Long employeeId){
        return employeeLeaveCountRepository.findEmployeeLeaveCountByEmployeeId(employeeId);
    }

    public void save(EmployeeLeaveCount employeeLeaveCount) {
        employeeLeaveCountRepository.save(employeeLeaveCount);
    }
}
