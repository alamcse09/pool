package com.dgt.paribahanpool.employee.service;

import com.dgt.paribahanpool.designation.model.DesignationResponse;
import com.dgt.paribahanpool.designation.service.DesignationService;
import com.dgt.paribahanpool.employee.model.Employee;
import com.dgt.paribahanpool.employee.model.EmployeeAddRequest;
import com.dgt.paribahanpool.employee.model.EmployeeLeaveCount;
import com.dgt.paribahanpool.enums.GeolocationType;
import com.dgt.paribahanpool.geolocation.model.GeoLocationResponse;
import com.dgt.paribahanpool.geolocation.service.GeoLocationService;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class EmployeeModelService {

    private final EmployeeService employeeService;
    private final DesignationService designationService;
    private final MvcUtil mvcUtil;
    private final GeoLocationService geoLocationService;
    private final EmployeeLeaveCountService employeeLeaveCountService;

    public void addEmployeeGet( Model model ) {

        List<DesignationResponse> designationResponseList = designationService.findAll( DesignationService::getDesignationResponseFromDesignation );

        EmployeeAddRequest employeeAddRequest = new EmployeeAddRequest();

        List<GeoLocationResponse> districtList = geoLocationService.findByGeoLocationType( GeolocationType.DISTRICT );

        employeeAddRequest.setDistrictLists( districtList );

        model.addAttribute( "designationList", designationResponseList );
        model.addAttribute("employeeAddRequest", employeeAddRequest);
    }

    public void addEmployeePost(EmployeeAddRequest employeeAddRequest, RedirectAttributes redirectAttributes) {

        Employee employee = employeeService.save( employeeAddRequest );

        EmployeeLeaveCount employeeLeaveCount = employeeService.getEmployeeLeaveCountFromEmployeeAddRequest(employee, employeeAddRequest);
        employeeLeaveCountService.save(employeeLeaveCount);

        mvcUtil.addSuccessMessage( redirectAttributes, "success.employee.add" );
    }

    @Transactional
    public void editEmployeeGet( Long id, Model model ) {

        Optional<Employee> employee = employeeService.findById( id );

        if( employee.isPresent() ) {

            List<DesignationResponse> designationResponseList = designationService.findAll( DesignationService::getDesignationResponseFromDesignation );

            model.addAttribute( "designationList", designationResponseList );
            model.addAttribute("employeeAddRequest", employeeService.getEmployeeAddRequestFromEmployee( employee.get() ));
        }
    }
}
