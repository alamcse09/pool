package com.dgt.paribahanpool.employee.service;

import com.dgt.paribahanpool.employee.model.Employee;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepository extends DataTablesRepository<Employee, Long> {

    @Query( "Select count(*), e.currentGrade From Employee e GROUP BY e.currentGrade order by e.currentGrade" )
    List<Object[]> countEmployeeByCurrentGrade();
}
