package com.dgt.paribahanpool.employee.service;

import com.dgt.paribahanpool.complain.model.Complains;
import com.dgt.paribahanpool.complain.model.ComplainsAddRequest;
import com.dgt.paribahanpool.complain.service.ComplainsService;
import com.dgt.paribahanpool.designation.model.Designation;
import com.dgt.paribahanpool.designation.service.DesignationService;
import com.dgt.paribahanpool.document.model.Document;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.employee.model.*;
import com.dgt.paribahanpool.enums.GeolocationType;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.geolocation.model.GeoLocationResponse;
import com.dgt.paribahanpool.geolocation.service.GeoLocationService;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class EmployeeService {
    
    private final EmployeeRepository employeeRepository;
    private final EmployeeEducationalInfoService employeeEducationalInfoService;
    private final EmployeeTrainingInfoService employeeTrainingInfoService;
    private final DocumentService documentService;
    private final ComplainsService complainsService;
    private final UserService userService;
    private final DesignationService designationService;
    private final GeoLocationService geoLocationService;
    private final EmployeeLeaveCountService employeeLeaveCountService;

    public Optional<Employee> findById(Long id ) {

        return employeeRepository.findById( id );
    }
    
    public Employee save( Employee employee ) {
        
        return employeeRepository.save( employee );
    }

    public boolean existById( Long id ) {

        return employeeRepository.existsById(id);
    }

    @Transactional
    public Employee save(EmployeeAddRequest employeeAddRequest) {
        
        Employee employee = new Employee();
        User user = null;

        if( employeeAddRequest.getId() != null ) {

            employee = employeeRepository.findById( employeeAddRequest.getId() ).get();
            user = userService.editUserFromEmployeeService( employee, employeeAddRequest );
        } else {

            user = userService.createUserFromEmployeeService( employeeAddRequest );
        }

        employee = getEmployeeFromEmployeeAddRequest( employee, employeeAddRequest, user );
        user.setEmployee( employee );
        userService.save( user );

        return save( employee );
    }

    public EmployeeLeaveCount getEmployeeLeaveCountFromEmployeeAddRequest(Employee employee, EmployeeAddRequest employeeAddRequest) {
        EmployeeLeaveCount employeeLeaveCount = new EmployeeLeaveCount();

        employeeLeaveCount.setId(employee.getId());
        employeeLeaveCount.setEmployeeId(employee.getId());
        employeeLeaveCount.setCasualLeaveCount(employeeAddRequest.getCasualLeaveCount());
        employeeLeaveCount.setAbroadLeaveCount(employeeAddRequest.getAbroadLeaveCount());
        employeeLeaveCount.setAmusementLeaveCount(employeeAddRequest.getAmusementLeaveCount());
        employeeLeaveCount.setEarnedLeaveCount(employeeAddRequest.getEarnedLeaveCount());
        employeeLeaveCount.setMedicalLeaveCount(employeeAddRequest.getMedicalLeaveCount());
        employeeLeaveCount.setUnpaidLeaveCount(employeeAddRequest.getUnpaidLeaveCount());
        employeeLeaveCount.setMaternityLeaveCount(employeeAddRequest.getMaternityLeaveCount());
        employeeLeaveCount.setYearlyLeaveCount(employeeAddRequest.getYearlyLeaveCount());
        employeeLeaveCount.setOtherLeaveCount(employeeAddRequest.getOtherLeaveCount());

        return employeeLeaveCount;
    }

    private Employee getEmployeeFromEmployeeAddRequest(Employee employee, EmployeeAddRequest employeeAddRequest, User user) {

        employee.setEmployeeID( employeeAddRequest.getEmployeeID() );
        employee.setPersonalID( employeeAddRequest.getPersonalID() );
        employee.setName( employeeAddRequest.getName() );
        employee.setNameEn( employeeAddRequest.getNameEn() );
        employee.setFatherName( employeeAddRequest.getFatherName() );
        employee.setMotherName( employeeAddRequest.getMotherName() );
        employee.setPresentAddress( employeeAddRequest.getPresentAddress() );
        employee.setPermanentAddress( employeeAddRequest.getPermanentAddress() );
        employee.setMaritalStatus( employeeAddRequest.getMaritalStatus() );
        employee.setGender( employeeAddRequest.getGender() );
        employee.setDateOfBirth( employeeAddRequest.getDateOfBirth() );
        employee.setPassportNumber( employeeAddRequest.getPassportNumber() );
        employee.setBloodGroup( employeeAddRequest.getBloodGroup() );
        employee.setNid( employeeAddRequest.getNid() );
        employee.setMobileNumber( employeeAddRequest.getMobileNumber() );
        employee.setEmail( employeeAddRequest.getEmail() );
        employee.setQuota( employeeAddRequest.getQuota() );
        employee.setNumberOfChildren( employeeAddRequest.getNumberOfChildren() );
        employee.setAppointmentOrder( employeeAddRequest.getAppointmentOrder() );
        employee.setAppointmentDate( employeeAddRequest.getAppointmentDate() );
        employee.setJoiningDate( employeeAddRequest.getJoiningDate() );
        employee.setJoiningDesignation( employeeAddRequest.getJoiningDesignation() );
        employee.setOfficeMinistry( employeeAddRequest.getOfficeMinistry() );
        employee.setJobType( employeeAddRequest.getJobType() );
        employee.setPermanentDate( employeeAddRequest.getPermanentDate() );
        employee.setReportingManager( employeeAddRequest.getReportingManager() );
        employee.setRetirementDate(employeeAddRequest.getRetirementDate());
        employee.setAchievementName( employeeAddRequest.getAchievementName() );
        employee.setAchievementYear( employeeAddRequest.getAchievementYear() );
        employee.setAchievementDescription( employeeAddRequest.getAchievementDescription() );
        employee.setJoiningGrade( employeeAddRequest.getJoiningGrade() );
        employee.setInitialGradeSalaryOfJoin( employeeAddRequest.getInitialGradeSalaryOfJoin() );
        employee.setAdditionalIncrementNumberOfJoin( employeeAddRequest.getAdditionalIncrementNumberOfJoin() );
        employee.setStepOfJoin( employeeAddRequest.getStepOfJoin() );
        employee.setActualSalaryOfJoin( employeeAddRequest.getActualSalaryOfJoin() );
        employee.setCurrentGrade( employeeAddRequest.getCurrentGrade() );
        employee.setCurrentInitialGradeSalary( employeeAddRequest.getCurrentInitialGradeSalary() );
        employee.setCurrentAdditionalIncrementNumber( employeeAddRequest.getCurrentAdditionalIncrementNumber() );
        employee.setCurrentStep( employeeAddRequest.getCurrentStep() );
        employee.setCurrentActualSalary( employeeAddRequest.getCurrentActualSalary() );
        employee.setComments( employeeAddRequest.getComments() );
        employee.setIsPermanent( employeeAddRequest.getIsPermanent() );
        employee.setPermanentOrderNo( employeeAddRequest.getPermanentOrderNo() );
        employee.setCurrentWorkingPlace( employeeAddRequest.getCurrentWorkingPlace() );
        employee.setTransferOrderNo(employeeAddRequest.getTransferOrderNo());
        employee.setTransferDate(employeeAddRequest.getTransferDate());
        employee.setPromotionOrderNo(employeeAddRequest.getPromotionOrderNo());
        employee.setPromotionDate(employeeAddRequest.getPromotionDate());
        employee.setPreviousPost(employeeAddRequest.getPreviousPost());
        employee.setPromotedPost(employeeAddRequest.getPromotedPost());

        Optional<GeoLocation> optionalCurrentGeoLocation = geoLocationService.findById( employeeAddRequest.getCurrentGeolocationId() );

        if( optionalCurrentGeoLocation.isPresent() ){

            employee.setCurrentGeolocation( optionalCurrentGeoLocation.get() );
        }

        Optional<GeoLocation> optionalPermanentGeoLocation = geoLocationService.findById( employeeAddRequest.getPermanentGeolocationId() );

        if( optionalPermanentGeoLocation.isPresent() ){

            employee.setPermanentGeolocation( optionalPermanentGeoLocation.get() );
        }

        Optional<GeoLocation> optionalTransferredFromBranch = geoLocationService.findById( employeeAddRequest.getTransferredFromBranch() );

        if( optionalPermanentGeoLocation.isPresent() ){

            employee.setTransferredFromBranch( optionalTransferredFromBranch.get() );
        }

        Optional<GeoLocation> optionalTransferredToBranch = geoLocationService.findById( employeeAddRequest.getTransferredToBranch() );

        if( optionalPermanentGeoLocation.isPresent() ){

            employee.setTransferredToBranch( optionalTransferredToBranch.get() );
        }

        Optional<GeoLocation> optionalCurrentWorkDistrict = geoLocationService.findById( employeeAddRequest.getCurrentWorkDistrict() );

        if( optionalPermanentGeoLocation.isPresent() ){

            employee.setCurrentWorkDistrict( optionalCurrentWorkDistrict.get() );
        }

        Optional<GeoLocation> optionalPromotedFromBranch = geoLocationService.findById( employeeAddRequest.getPromotedFromBranch() );

        if( optionalPermanentGeoLocation.isPresent() ){

            employee.setPromotedFromBranch( optionalPromotedFromBranch.get() );
        }

        Optional<GeoLocation> optionalPromotedToBranch = geoLocationService.findById( employeeAddRequest.getPromotedToBranch() );

        if( optionalPermanentGeoLocation.isPresent() ){

            employee.setPromotedToBranch( optionalPromotedToBranch.get() );
        }

        Designation designation = designationService.findReference( employeeAddRequest.getDesignationId() );
        employee.setDesignation( designation );

        if( designation.getUserLevel() != null ){

            user.setUserLevel( designation.getUserLevel() );
        }

        if( employeeAddRequest.getEmployeeEducationalInfoAddRequests() != null ) {

            Set<EmployeeEducationalInfo> employeeEducationalInfoSet =
                    employeeAddRequest.getEmployeeEducationalInfoAddRequests()
                    .stream()
                    .map( this::getEducationInfoFromEducationInfoAddRequest )
                    .filter( edu -> edu!=null)
                    .collect( Collectors.toSet() );

            employee.getEmployeeEducationalInfoSet().clear();
            employee.getEmployeeEducationalInfoSet().addAll( employeeEducationalInfoSet );
        }

        if( employeeAddRequest.getEmployeeTrainingInfoAddRequests() != null ) {

            Set<EmployeeTrainingInfo> employeeTrainingInfoSet =
                    employeeAddRequest.getEmployeeTrainingInfoAddRequests()
                    .stream()
                    .map(this::getTrainingInfoFromTrainingInfoAddRequest)
                    .filter(e -> e!=null)
                    .collect(Collectors.toSet());

            employee.getEmployeeTrainingInfoSet().clear();
            employee.getEmployeeTrainingInfoSet().addAll( employeeTrainingInfoSet );
        }

        if( employeeAddRequest.getComplainsAddRequests() != null) {

            Set<Complains> complainsSet =
                    employeeAddRequest.getComplainsAddRequests()
                    .stream()
                    .map(this::getComplainsFromComplainsAddRequest)
                    .filter(e -> e!=null)
                    .collect(Collectors.toSet());

            employee.getComplainsSet().clear();
            employee.getComplainsSet().addAll(complainsSet);
        }

        if( employee.getEmployeeDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( employeeAddRequest.getEmployeeFiles(), Collections.EMPTY_MAP );
            employee.setEmployeeDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( employeeAddRequest.getEmployeeFiles(), employee.getEmployeeDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        if( employee.getAchievementDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( employeeAddRequest.getAchievementFiles(), Collections.EMPTY_MAP );
            employee.setAchievementDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( employeeAddRequest.getAchievementFiles(), employee.getAchievementDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        if( employee.getAppointmentDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( employeeAddRequest.getAppointmentFiles(), Collections.EMPTY_MAP );
            employee.setAppointmentDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( employeeAddRequest.getAppointmentFiles(), employee.getAppointmentDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        if( employee.getCurrentJobDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( employeeAddRequest.getCurrentJobFiles(), Collections.EMPTY_MAP );
            employee.setCurrentJobDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( employeeAddRequest.getCurrentJobFiles(), employee.getCurrentJobDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        if( employee.getTransferDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( employeeAddRequest.getTransferFiles(), Collections.EMPTY_MAP );
            employee.setTransferDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( employeeAddRequest.getTransferFiles(), employee.getTransferDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        if( employee.getPromotionDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( employeeAddRequest.getPromotionFiles(), Collections.EMPTY_MAP );
            employee.setPromotionDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( employeeAddRequest.getPromotionFiles(), employee.getPromotionDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        return employee;
    }

    private Complains getComplainsFromComplainsAddRequest(ComplainsAddRequest complainsAddRequest) {

        Complains complains = new Complains();

        if( complainsAddRequest.getId() != null ) {

            Optional<Complains> complain = complainsService.findById( complainsAddRequest.getId() );

            if( complain.isPresent() ) {

                complains = complain.get();
            } else
                return null;
        }

        complains.setDiscard( complainsAddRequest.getDiscard() );
        complains.setMisconduct( complainsAddRequest.getMisconduct() );
        complains.setOtherObjections( complainsAddRequest.getOtherObjections() );

        return complains;
    }

    private EmployeeEducationalInfo getEducationInfoFromEducationInfoAddRequest(EmployeeEducationalInfoAddRequest employeeEducationalInfoAddRequest) {

        EmployeeEducationalInfo employeeEducationalInfo = new EmployeeEducationalInfo();

        if( employeeEducationalInfoAddRequest.getId() != null ) {

            Optional<EmployeeEducationalInfo> educationalInfo = employeeEducationalInfoService.findById( employeeEducationalInfoAddRequest.getId() );

            if(educationalInfo.isPresent()) {

                employeeEducationalInfo = educationalInfo.get();
            } else {

                return null;
            }
        }

        employeeEducationalInfo.setEducationLevel( employeeEducationalInfoAddRequest.getEducationLevel() );
        employeeEducationalInfo.setEducationalInstitute( employeeEducationalInfoAddRequest.getEducationalInstitute() );
        employeeEducationalInfo.setDegreeName( employeeEducationalInfoAddRequest.getDegreeName() );
        employeeEducationalInfo.setPassingYear( employeeEducationalInfoAddRequest.getPassingYear() );
        employeeEducationalInfo.setSubject( employeeEducationalInfoAddRequest.getSubject() );
        employeeEducationalInfo.setResult( employeeEducationalInfoAddRequest.getResult() );
        employeeEducationalInfo.setBoard( employeeEducationalInfoAddRequest.getBoard() );

        if( employeeEducationalInfo.getEducationDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( employeeEducationalInfoAddRequest.getEducationFiles(), Collections.EMPTY_MAP );
            employeeEducationalInfo.setEducationDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( employeeEducationalInfoAddRequest.getEducationFiles(), employeeEducationalInfo.getEducationDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }


        return employeeEducationalInfo;
    }

    private EmployeeTrainingInfo getTrainingInfoFromTrainingInfoAddRequest(EmployeeTrainingInfoAddRequest employeeTrainingInfoAddRequest) {

        EmployeeTrainingInfo employeeTrainingInfo = new EmployeeTrainingInfo();

        if( employeeTrainingInfoAddRequest.getId() != null ) {

            Optional<EmployeeTrainingInfo> trainingInfo = employeeTrainingInfoService.findById( employeeTrainingInfoAddRequest.getId() );

            if(trainingInfo.isPresent()) {

                employeeTrainingInfo = trainingInfo.get();
            } else {

                return null;
            }
        }

        employeeTrainingInfo.setTrainingName( employeeTrainingInfoAddRequest.getTrainingName() );
        employeeTrainingInfo.setTrainingSubject( employeeTrainingInfoAddRequest.getTrainingSubject() );
        employeeTrainingInfo.setOrganizationName( employeeTrainingInfoAddRequest.getOrganizationName() );
        employeeTrainingInfo.setTrainingDateFrom(employeeTrainingInfoAddRequest.getTrainingDateFrom());
        employeeTrainingInfo.setTrainingDateTo(employeeTrainingInfoAddRequest.getTrainingDateTo());

        if( employeeTrainingInfo.getTrainingDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( employeeTrainingInfoAddRequest.getTrainingFiles(), Collections.EMPTY_MAP );
            employeeTrainingInfo.setTrainingDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( employeeTrainingInfoAddRequest.getTrainingFiles(), employeeTrainingInfo.getTrainingDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }


        return employeeTrainingInfo;
    }

    public EmployeeAddRequest getEmployeeAddRequestFromEmployee(Employee employee) {

        EmployeeAddRequest employeeAddRequest = new EmployeeAddRequest();

        employeeAddRequest.setId( employee.getId() );
        employeeAddRequest.setEmployeeID( employee.getEmployeeID() );
        employeeAddRequest.setPersonalID( employee.getPersonalID() );
        employeeAddRequest.setName( employee.getName() );
        employeeAddRequest.setNameEn( employee.getNameEn() );
        employeeAddRequest.setFatherName( employee.getFatherName() );
        employeeAddRequest.setMotherName( employee.getMotherName() );
        employeeAddRequest.setPresentAddress( employee.getPresentAddress() );
        employeeAddRequest.setPermanentAddress( employee.getPermanentAddress() );
        employeeAddRequest.setMaritalStatus( employee.getMaritalStatus() );
        employeeAddRequest.setGender( employee.getGender() );
        employeeAddRequest.setDateOfBirth( employee.getDateOfBirth() );
        employeeAddRequest.setPassportNumber( employee.getPassportNumber() );
        employeeAddRequest.setBloodGroup( employee.getBloodGroup() );
        employeeAddRequest.setNid( employee.getNid() );
        employeeAddRequest.setMobileNumber( employee.getMobileNumber() );
        employeeAddRequest.setEmail( employee.getEmail() );
        employeeAddRequest.setQuota( employee.getQuota() );
        employeeAddRequest.setNumberOfChildren( employee.getNumberOfChildren() );
        employeeAddRequest.setAppointmentOrder( employee.getAppointmentOrder() );
        employeeAddRequest.setAppointmentDate( employee.getAppointmentDate() );
        employeeAddRequest.setJoiningDate( employee.getJoiningDate() );
        employeeAddRequest.setJoiningDesignation( employee.getJoiningDesignation() );
        employeeAddRequest.setOfficeMinistry( employee.getOfficeMinistry() );
        employeeAddRequest.setJobType( employee.getJobType() );
        employeeAddRequest.setPermanentDate( employee.getPermanentDate() );
        employeeAddRequest.setReportingManager( employee.getReportingManager() );
        employeeAddRequest.setRetirementDate( employee.getRetirementDate() );
        employeeAddRequest.setAchievementName( employee.getAchievementName() );
        employeeAddRequest.setAchievementYear( employee.getAchievementYear() );
        employeeAddRequest.setAchievementDescription( employee.getAchievementDescription() );
        employeeAddRequest.setJoiningGrade( employee.getJoiningGrade() );
        employeeAddRequest.setInitialGradeSalaryOfJoin( employee.getInitialGradeSalaryOfJoin() );
        employeeAddRequest.setAdditionalIncrementNumberOfJoin( employee.getAdditionalIncrementNumberOfJoin() );
        employeeAddRequest.setStepOfJoin( employee.getStepOfJoin() );
        employeeAddRequest.setActualSalaryOfJoin( employee.getActualSalaryOfJoin() );
        employeeAddRequest.setCurrentGrade( employee.getCurrentGrade() );
        employeeAddRequest.setCurrentInitialGradeSalary( employee.getCurrentInitialGradeSalary() );
        employeeAddRequest.setCurrentAdditionalIncrementNumber( employee.getCurrentAdditionalIncrementNumber() );
        employeeAddRequest.setCurrentStep( employee.getCurrentStep() );
        employeeAddRequest.setCurrentActualSalary( employee.getCurrentActualSalary() );
        employeeAddRequest.setComments( employee.getComments() );
        employeeAddRequest.setCurrentWorkingPlace( employee.getCurrentWorkingPlace() );
        employeeAddRequest.setPermanentOrderNo( employee.getPermanentOrderNo() );
        employeeAddRequest.setTransferOrderNo(employee.getTransferOrderNo());
        employeeAddRequest.setTransferDate(employee.getTransferDate());
        employeeAddRequest.setPromotionDate(employee.getPromotionDate());
        employeeAddRequest.setPromotionOrderNo(employee.getPromotionOrderNo());
        employeeAddRequest.setPreviousPost(employee.getPreviousPost());
        employeeAddRequest.setPromotedPost(employee.getPromotedPost());
        employeeAddRequest.setCurrentGeolocationId( employee.getCurrentGeolocation() == null? -1: employee.getCurrentGeolocation().getId());
        employeeAddRequest.setPermanentGeolocationId( employee.getPermanentGeolocation() == null? -1: employee.getPermanentGeolocation().getId());
        employeeAddRequest.setCurrentWorkDistrict( employee.getCurrentWorkDistrict() == null? -1: employee.getCurrentWorkDistrict().getId());
        employeeAddRequest.setTransferredFromBranch( employee.getTransferredFromBranch() == null? -1: employee.getTransferredFromBranch().getId());
        employeeAddRequest.setTransferredToBranch( employee.getTransferredToBranch() == null? -1: employee.getTransferredToBranch().getId());
        employeeAddRequest.setPromotedFromBranch( employee.getPromotedFromBranch() == null? -1: employee.getPromotedFromBranch().getId());
        employeeAddRequest.setPromotedToBranch( employee.getPromotedToBranch() == null? -1: employee.getPromotedToBranch().getId());

        employeeAddRequest.setDesignationId(employee.getDesignation() == null? -1: employee.getDesignation().getId());


        List<GeoLocationResponse> districtList = geoLocationService.findByGeoLocationType( GeolocationType.DISTRICT );

        employeeAddRequest.setDistrictLists( districtList );

        if( employee.getEmployeeEducationalInfoSet() != null) {

            List<EmployeeEducationalInfoAddRequest> employeeEducationalInfoAddRequests =
                    employee.getEmployeeEducationalInfoSet()
                    .stream()
                    .map( this::getEducationInfoAddRequestFromEducationInfo )
                    .collect( Collectors.toList() );

            employeeAddRequest.setEmployeeEducationalInfoAddRequests( employeeEducationalInfoAddRequests );
        }

        if( employee.getEmployeeTrainingInfoSet() != null ) {

            List<EmployeeTrainingInfoAddRequest> employeeTrainingInfoAddRequests =
                    employee.getEmployeeTrainingInfoSet()
                    .stream()
                    .map( this::getTrainingInfoAddRequestFromTrainingInfo )
                    .collect( Collectors.toList() );

            employeeAddRequest.setEmployeeTrainingInfoAddRequests( employeeTrainingInfoAddRequests );
        }

        if( employee.getComplainsSet() != null ) {

            List<ComplainsAddRequest> complainsAddRequests =
                    employee.getComplainsSet()
                    .stream()
                    .map( this::getComplainsAddRequestFromComplains )
                    .collect( Collectors.toList() );

            employeeAddRequest.setComplainsAddRequests( complainsAddRequests );
        }

        if( employee.getEmployeeDocGroupId() != null && employee.getEmployeeDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( employee.getEmployeeDocGroupId() );
            employeeAddRequest.setDocuments( documentMetadataList );
        }

        if( employee.getAchievementDocGroupId() != null && employee.getAchievementDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( employee.getAchievementDocGroupId() );
            employeeAddRequest.setAchievementDocuments( documentMetadataList );
        }

        if( employee.getAppointmentDocGroupId() != null && employee.getAppointmentDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( employee.getAppointmentDocGroupId() );
            employeeAddRequest.setAppointmentDocuments( documentMetadataList );
        }

        if( employee.getCurrentJobDocGroupId() != null && employee.getCurrentJobDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( employee.getCurrentJobDocGroupId() );
            employeeAddRequest.setCurrentJobDocuments( documentMetadataList );
        }

        if( employee.getTransferDocGroupId() != null && employee.getTransferDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( employee.getTransferDocGroupId() );
            employeeAddRequest.setTransferDocuments( documentMetadataList );
        }

        if( employee.getPromotionDocGroupId() != null && employee.getPromotionDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( employee.getPromotionDocGroupId() );
            employeeAddRequest.setPromotionDocuments( documentMetadataList );
        }

        Optional<EmployeeLeaveCount> employeeLeaveCountOptional = employeeLeaveCountService.getLeaveCountByEmployeeId(employee.getId());

        if( employeeLeaveCountOptional.isPresent() ) {

            EmployeeLeaveCount employeeLeaveCount = employeeLeaveCountOptional.get();

            employeeAddRequest.setAbroadLeaveCount(employeeLeaveCount.getAbroadLeaveCount());
            employeeAddRequest.setAmusementLeaveCount(employeeLeaveCount.getAmusementLeaveCount());
            employeeAddRequest.setCasualLeaveCount(employeeLeaveCount.getCasualLeaveCount());
            employeeAddRequest.setEarnedLeaveCount(employeeLeaveCount.getEarnedLeaveCount());
            employeeAddRequest.setMedicalLeaveCount(employeeLeaveCount.getMedicalLeaveCount());
            employeeAddRequest.setMaternityLeaveCount(employeeLeaveCount.getMaternityLeaveCount());
            employeeAddRequest.setOtherLeaveCount(employeeLeaveCount.getOtherLeaveCount());
            employeeAddRequest.setUnpaidLeaveCount(employeeLeaveCount.getUnpaidLeaveCount());
            employeeAddRequest.setYearlyLeaveCount(employeeLeaveCount.getYearlyLeaveCount());
        }

        return employeeAddRequest;

    }

    private ComplainsAddRequest getComplainsAddRequestFromComplains(Complains complains) {

        ComplainsAddRequest complainsAddRequest = new ComplainsAddRequest();

        complainsAddRequest.setId( complains.getId() );
        complainsAddRequest.setDiscard( complains.getDiscard() );
        complainsAddRequest.setMisconduct( complains.getMisconduct() );
        complainsAddRequest.setOtherObjections( complains.getOtherObjections() );

        return complainsAddRequest;
    }

    private EmployeeTrainingInfoAddRequest getTrainingInfoAddRequestFromTrainingInfo( EmployeeTrainingInfo employeeTrainingInfo ) {

        EmployeeTrainingInfoAddRequest employeeTrainingInfoAddRequest = new EmployeeTrainingInfoAddRequest();

        employeeTrainingInfoAddRequest.setId( employeeTrainingInfo.getId() );
        employeeTrainingInfoAddRequest.setTrainingName( employeeTrainingInfo.getTrainingName() );
        employeeTrainingInfoAddRequest.setTrainingSubject( employeeTrainingInfo.getTrainingSubject() );
        employeeTrainingInfoAddRequest.setTrainingDateFrom(employeeTrainingInfo.getTrainingDateFrom());
        employeeTrainingInfoAddRequest.setTrainingDateTo(employeeTrainingInfo.getTrainingDateTo());
        employeeTrainingInfoAddRequest.setOrganizationName( employeeTrainingInfo.getOrganizationName() );

        if( employeeTrainingInfo.getTrainingDocGroupId() != null && employeeTrainingInfo.getTrainingDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( employeeTrainingInfo.getTrainingDocGroupId() );
            employeeTrainingInfoAddRequest.setTrainingDocuments( documentMetadataList );
        }

        return employeeTrainingInfoAddRequest;
    }

    private EmployeeEducationalInfoAddRequest getEducationInfoAddRequestFromEducationInfo(EmployeeEducationalInfo employeeEducationalInfo) {

        EmployeeEducationalInfoAddRequest employeeEducationalInfoAddRequest = new EmployeeEducationalInfoAddRequest();

        employeeEducationalInfoAddRequest.setId( employeeEducationalInfo.getId() );
        employeeEducationalInfoAddRequest.setEducationLevel( employeeEducationalInfo.getEducationLevel() );
        employeeEducationalInfoAddRequest.setEducationalInstitute( employeeEducationalInfo.getEducationalInstitute() );
        employeeEducationalInfoAddRequest.setDegreeName( employeeEducationalInfo.getDegreeName() );
        employeeEducationalInfoAddRequest.setPassingYear( employeeEducationalInfo.getPassingYear() );
        employeeEducationalInfoAddRequest.setSubject( employeeEducationalInfo.getSubject() );
        employeeEducationalInfoAddRequest.setResult( employeeEducationalInfo.getResult() );
        employeeEducationalInfoAddRequest.setBoard( employeeEducationalInfo.getBoard() );

        if( employeeEducationalInfo.getEducationDocGroupId() != null && employeeEducationalInfo.getEducationDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( employeeEducationalInfo.getEducationDocGroupId() );
            employeeEducationalInfoAddRequest.setEducationDocuments( documentMetadataList );
        }

        return employeeEducationalInfoAddRequest;
    }

    public DataTablesOutput<EmployeeSearchResponse> searchForDatatable(DataTablesInput dataTablesInput) {

        Specification<Employee> employeeSpecification = EmployeeSpecification.filterByIsDeleted( false );

        return employeeRepository.findAll( dataTablesInput, null, employeeSpecification, this::getEmployeeSearchResponseFromEmployee );
    }

    private EmployeeSearchResponse getEmployeeSearchResponseFromEmployee(Employee employee) {

        EmployeeSearchResponse employeeSearchResponse = new EmployeeSearchResponse();

        employeeSearchResponse.setId( employee.getId() );
        employeeSearchResponse.setEmployeeID( employee.getEmployeeID() );
        employeeSearchResponse.setName( employee.getName() );
        employeeSearchResponse.setPersonalID( employee.getPersonalID() );
        employeeSearchResponse.setFatherName( employee.getFatherName() );
        employeeSearchResponse.setMotherName( employee.getMotherName() );
        employeeSearchResponse.setPresentAddress( employee.getPresentAddress() );
        employeeSearchResponse.setPermanentAddress( employee.getPermanentAddress() );
        employeeSearchResponse.setMaritalStatus( employee.getMaritalStatus() );
        employeeSearchResponse.setGender( employee.getGender() );
        employeeSearchResponse.setDateOfBirth( employee.getDateOfBirth() );
        employeeSearchResponse.setPassportNumber( employee.getPassportNumber() );
        employeeSearchResponse.setBloodGroup( employee.getBloodGroup() );
        employeeSearchResponse.setNid( employee.getNid() );
        employeeSearchResponse.setMobileNumber( employee.getMobileNumber() );
        employeeSearchResponse.setEmail( employee.getEmail() );
        employeeSearchResponse.setQuota( employee.getQuota() );
        employeeSearchResponse.setNumberOfChildren( employee.getNumberOfChildren() );

        return employeeSearchResponse;
    }

    public void delete( Long id ) {

        Optional<Employee> employeeToDelete = findById( id );

        if( employeeToDelete.isPresent() ) {

            Employee employee = employeeToDelete.get();
            employee.setIsDeleted( true );
            save( employee );
        }
    }

    public Long getNumberOfEmployeeCount() {

        return employeeRepository.count();
    }

    public List<Object[]> getEmployeeCountGroupByGrade() {

        return employeeRepository.countEmployeeByCurrentGrade();
    }
}
