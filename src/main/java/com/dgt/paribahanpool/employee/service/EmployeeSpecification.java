package com.dgt.paribahanpool.employee.service;

import com.dgt.paribahanpool.employee.model.Employee;
import com.dgt.paribahanpool.employee.model.Employee_;
import org.springframework.data.jpa.domain.Specification;

public class EmployeeSpecification {

    public static Specification<Employee> filterByIsDeleted(Boolean isDeleted ){

        if( isDeleted == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Employee_.IS_DELETED ), isDeleted );
    }
}
