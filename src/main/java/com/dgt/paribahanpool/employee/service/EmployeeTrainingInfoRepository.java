package com.dgt.paribahanpool.employee.service;

import com.dgt.paribahanpool.employee.model.EmployeeTrainingInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeTrainingInfoRepository extends JpaRepository<EmployeeTrainingInfo, Long> {

}
