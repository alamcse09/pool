package com.dgt.paribahanpool.employee.service;

import com.dgt.paribahanpool.employee.model.EmployeeTrainingInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class EmployeeTrainingInfoService {

    private final EmployeeTrainingInfoRepository employeeTrainingInfoRepository;

    public Optional<EmployeeTrainingInfo> findById(Long id ) {

        return employeeTrainingInfoRepository.findById( id );
    }
}
