package com.dgt.paribahanpool.employee.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.designation.model.DesignationResponse;
import com.dgt.paribahanpool.designation.service.DesignationService;
import com.dgt.paribahanpool.employee.model.Employee;
import com.dgt.paribahanpool.employee.model.EmployeeAddRequest;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.GeolocationType;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.geolocation.model.GeoLocationResponse;
import com.dgt.paribahanpool.geolocation.service.GeoLocationService;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class EmployeeValidationService {

    private final MvcUtil mvcUtil;
    private final EmployeeService employeeService;
    private final UserService userService;
    private final DesignationService designationService;
    private final GeoLocationService geoLocationService;

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, EmployeeAddRequest employeeAddRequest ){

        List<GeoLocationResponse> districtList = geoLocationService.findByGeoLocationType( GeolocationType.DISTRICT );

        employeeAddRequest.setDistrictLists( districtList );

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "employeeAddRequest" )
                .object( employeeAddRequest )
                .title( "title.employee.add" )
                .content( "employee/employee-add" )
                .activeMenu( Menu.EMPLOYEE_ADD )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.EMPLOYEE_ADD_FORM,FrontEndLibrary.TAB_WIZARD } )
                .build();

        boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        if( !isValid ) {

            List<DesignationResponse> designationResponseList = designationService.findAll( DesignationService::getDesignationResponseFromDesignation );
            model.addAttribute( "designationList", designationResponseList );

            return false;
        }

        if( employeeAddRequest.getId() == null ) {

            Optional<User> userOptional = userService.findByUsername( employeeAddRequest.getEmail(), false );

            if( userOptional.isPresent() ) {

                return setModelWithError( model, params, "error.common.email.already.exist");
            }
        }

        return true;
    }

    public Boolean editEmployee( Long id, RedirectAttributes redirectAttributes ) {

        Optional<Employee> employee = employeeService.findById( id );

        if( !employee.isPresent() ) {

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound");
            return false;
        }

        return true;
    }

    public RestValidationResult delete(Long id)  throws NotFoundException{

        if( !employeeService.existById( id ) ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    private boolean setModelWithError( Model model, HandleBindingResultParams params , String errorMessage ) {

        mvcUtil.addErrorMessage( model, errorMessage );
        model.addAttribute( params.getKey(), params.getObject() );
        mvcUtil.addTitleAndContent( model, params.getTitle(), params.getContent(), params.getActiveMenu() );

        if( params.getFrontEndLibraries() != null ){

            mvcUtil.addCssAndJsByLibraryName( model, params.getFrontEndLibraries() );
        }

        return false;
    }
}
