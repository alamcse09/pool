package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum AccusedPersonType {

    SYSTEM_USER,
    MECHANIC;

    private static Map<AccusedPersonType, String> personTypeStringHashMap = new HashMap<>();

    static {

        personTypeStringHashMap.put( SYSTEM_USER, "dropdown.options.complain.accused.system-user" );
        personTypeStringHashMap.put( MECHANIC, "dropdown.options.complain.accused.status.mechanic" );
    }

    public static String getLabel( AccusedPersonType accusedPersonType ) {

        return personTypeStringHashMap.get( accusedPersonType );
    }
}
