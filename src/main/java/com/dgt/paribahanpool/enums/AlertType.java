package com.dgt.paribahanpool.enums;

public enum AlertType {

    FITNESS(0),
    TAX_TOKEN(1),
    WARRATANTY_EXPIRY(2);

    private Integer value;

    AlertType( Integer value ){

        this.value = value;
    }
}
