package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum BloodGroup {

    A_POSITIVE,
    A_NEGATIVE,
    B_POSITIVE,
    B_NEGATIVE,
    O_POSITIVE,
    O_NEGATIVE,
    AB_POSITIVE,
    AB_NEGATIVE;

    private static Map<BloodGroup, String> bloodGroupLabelMap = new HashMap<>();

    static {

        bloodGroupLabelMap.put( A_POSITIVE, "dropdown.options.blood.group.a_positive" );
        bloodGroupLabelMap.put( A_NEGATIVE, "dropdown.options.blood.group.a_negative" );
        bloodGroupLabelMap.put( B_POSITIVE, "dropdown.options.blood.group.b_positive" );
        bloodGroupLabelMap.put( B_NEGATIVE, "dropdown.options.blood.group.b_negative" );
        bloodGroupLabelMap.put( O_POSITIVE, "dropdown.options.blood.group.o_positive" );
        bloodGroupLabelMap.put( O_NEGATIVE, "dropdown.options.blood.group.o_negative" );
        bloodGroupLabelMap.put( AB_POSITIVE, "dropdown.options.blood.group.ab_positive" );
        bloodGroupLabelMap.put( AB_NEGATIVE, "dropdown.options.blood.group.ab_negative" );
    }

    public static String getLabel( BloodGroup bloodGroup ) {

        return bloodGroupLabelMap.get(bloodGroup);
    }
}
