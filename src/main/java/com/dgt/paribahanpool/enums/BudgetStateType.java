package com.dgt.paribahanpool.enums;

public enum BudgetStateType {

    INITIAL_BUDGET,
    REVISED_BUDGET,
    FINAL_BUDGET;
}
