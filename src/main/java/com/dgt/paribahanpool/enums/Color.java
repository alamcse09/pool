package com.dgt.paribahanpool.enums;

public enum Color {

    BLUE(1),
    BLACK(2),
    CYAN(3),
    GREEN(4),
    PEARL_BLACK(5),
    PEARL_WHITE(6),
    RED(4),
    RED_WINE(5),
    TEAL(6),
    WHITE(7);


    private Integer value;
    Color(Integer i) {
        this.value = i;
    }
}
