package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum CommitteePositionType {

    SOBHAPOTI,
    SODOSSO,
    SODOSSO_SOBHAPOTI;

    private static Map<CommitteePositionType, String> committeePositionTypeStringMap = new HashMap<>();

    static {

        committeePositionTypeStringMap.put( SOBHAPOTI, "dropdown.options.committee.position-type.sobhapoti" );
        committeePositionTypeStringMap.put( SODOSSO, "dropdown.options.committee.position-type.sodosso" );
        committeePositionTypeStringMap.put( SODOSSO_SOBHAPOTI, "dropdown.options.committee.position-type.sodosso-sobhapoti" );
    }

    public static String getLabel( CommitteePositionType committeePositionType ){

        return committeePositionTypeStringMap.get( committeePositionType );
    }
}
