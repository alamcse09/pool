package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum ConfirmType {

    YES (0),
    NO (1);

    private Integer value;

    ConfirmType( Integer value ){

        this.value = value;
    }

    private static Map<ConfirmType, String> confirmTypeMap = new HashMap<>();

    static {

        confirmTypeMap.put( YES, "radio.options.confirm.type.yes" );
        confirmTypeMap.put( NO, "radio.options.confirm.type.no" );
    }

    public static String getLabel( ConfirmType confirmType ) {

        return confirmTypeMap.get(confirmType);
    }
}
