package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum DriverType {

    Motor_Vehicle( 1 ),
    Marine_Vehicle( 2 );

    private static Map<DriverType, String> driverTypeLabelMap = new HashMap<>();

    static {

        driverTypeLabelMap.put( Motor_Vehicle, "dropdown.options.driver.add.driver_type.motor_vehicle" );
        driverTypeLabelMap.put( Marine_Vehicle, "dropdown.options.driver.add.driver_type.marine_vehicle" );

    }

    private Integer value;

    DriverType( Integer value ){

        this.value = value;
    }

    public static String getLabel( DriverType driverType ){

        return DriverType.driverTypeLabelMap.get( driverType );
    }
}
