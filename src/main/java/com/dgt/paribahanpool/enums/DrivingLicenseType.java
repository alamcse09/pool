package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum DrivingLicenseType {

    H,
    M,
    L;

    private static Map<DrivingLicenseType, String> drivingLicenseLabelMap = new HashMap<>();

    static {
        drivingLicenseLabelMap.put(H,"dropdown.options.driving.license.heavy");
        drivingLicenseLabelMap.put(M,"dropdown.options.driving.license.medium");
        drivingLicenseLabelMap.put(L,"dropdown.options.driving.license.light");
    }

    public static String getLabel( DrivingLicenseType drivingLicenseType) {

        return drivingLicenseLabelMap.get(drivingLicenseType);
    }
}
