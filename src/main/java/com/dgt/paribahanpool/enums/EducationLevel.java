package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum EducationLevel {

    PRIMARY,
    PSC,
    JSC,
    SECONDIARY,
    SSC,
    HIGHER_SECONDIARY,
    HSC,
    DIPLOMA,
    BA,
    HONORS,
    BSC,
    MASTERS,
    PHD;

    private static Map<EducationLevel, String> educationLevelLabelMap = new HashMap<>();

    static {

        educationLevelLabelMap.put( PRIMARY, "dropdown.options.education.level.primary" );
        educationLevelLabelMap.put( PSC, "dropdown.options.education.level.psc" );
        educationLevelLabelMap.put( JSC, "dropdown.options.education.level.jsc" );
        educationLevelLabelMap.put( SECONDIARY, "dropdown.options.education.level.secondiary" );
        educationLevelLabelMap.put( SSC, "dropdown.options.education.level.ssc" );
        educationLevelLabelMap.put( HIGHER_SECONDIARY, "dropdown.options.education.level.higher-secondiary" );
        educationLevelLabelMap.put( HSC, "dropdown.options.education.level.hsc" );
        educationLevelLabelMap.put( DIPLOMA, "dropdown.options.education.level.diploma" );
        educationLevelLabelMap.put( BA, "dropdown.options.education.level.ba" );
        educationLevelLabelMap.put( HONORS, "dropdown.options.education.level.honors" );
        educationLevelLabelMap.put( BSC, "dropdown.options.education.level.bsc" );
        educationLevelLabelMap.put( MASTERS, "dropdown.options.education.level.masters" );
        educationLevelLabelMap.put( PHD, "dropdown.options.education.level.phd" );
    }

    public static String getLabel( EducationLevel educationLevel ) {

        return educationLevelLabelMap.get( educationLevel );
    }
}
