package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum EmployeeType {

    DIVISION,
    DISTRICT,
    UPAZILA,
    PROBATIONARY,
    VVIP,
    ADMINSTRATIVE;

    private static Map<EmployeeType, String> employeeTypeStringMap = new HashMap<>();

    static {

        employeeTypeStringMap.put( PROBATIONARY, "dropdown.options.employee.type.probationary" );
        employeeTypeStringMap.put( VVIP, "dropdown.options.employee.type.vvip" );
        employeeTypeStringMap.put( DIVISION, "dropdown.options.employee.type.division" );
        employeeTypeStringMap.put( DISTRICT, "dropdown.options.employee.type.district" );
        employeeTypeStringMap.put(UPAZILA, "dropdown.options.employee.type.upozilla" );
        employeeTypeStringMap.put( ADMINSTRATIVE, "dropdown.options.employee.type.administrative" );
    }

    public static String getLabel( EmployeeType employeeType ){

        return employeeTypeStringMap.get( employeeType );
    }
}
