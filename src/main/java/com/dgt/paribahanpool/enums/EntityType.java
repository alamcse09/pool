package com.dgt.paribahanpool.enums;

public enum EntityType {

    VEHICLE(0),
    WARRANTY(1);

    private Integer value;

    EntityType( Integer value ){

        this.value = value;
    }
}
