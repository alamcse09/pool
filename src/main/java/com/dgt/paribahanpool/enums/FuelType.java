package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum FuelType {

    PETROL(1),
    OCTANE(2),
    DIESEL(3),
    CNG(4);

    private static Map<FuelType,String> fuelTypeLabelMap = new HashMap<>();

    static {

        fuelTypeLabelMap.put( DIESEL, "dropdown.options.vehicle.add.fuel_type.diesel" );
        fuelTypeLabelMap.put( PETROL, "dropdown.options.vehicle.add.fuel_type.petrol");
        fuelTypeLabelMap.put( OCTANE, "dropdown.options.vehicle.add.fuel_type.octane");
        fuelTypeLabelMap.put( CNG, "dropdown.options.vehicle.add.fuel_type.cng");
    }

    private Integer value;

    FuelType( Integer value ){

        this.value = value;
    }

    public static String getLabel( FuelType fuelType ){

        return FuelType.fuelTypeLabelMap.get( fuelType );
    }
}
