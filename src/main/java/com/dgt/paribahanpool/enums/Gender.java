package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum Gender {

    MALE,
    FEMALE;

    private static Map<Gender, String> genderLabelMap = new HashMap<>();

    static {

        genderLabelMap.put( MALE, "dropdown.options.gender.male" );
        genderLabelMap.put( FEMALE, "dropdown.options.gender.female" );
    }

    public static String getLabel( Gender gender ) {

        return genderLabelMap.get(gender);
    }
}
