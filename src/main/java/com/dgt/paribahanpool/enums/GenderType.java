package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum GenderType {

    Male( 1 ),
    Female( 2 );

    public static Map<GenderType, String> genderTypeLabelMap = new HashMap<>();

    static {

        genderTypeLabelMap.put( Male, "radio.options.gender_type.male" );
        genderTypeLabelMap.put( Female, "radio.options.gender_type.female" );

    }

    private Integer value;

    GenderType( Integer value ){

        this.value = value;
    }

    public static String getLabel( GenderType genderType ){

        return genderType.genderTypeLabelMap.get( genderType );
    }
}
