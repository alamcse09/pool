package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum GeolocationType {

    DIVISION(0),
    DISTRICT(1),
    UPAZILA(2);

    public static Map<GeolocationType, String> geolocationTypeLabelMap = new HashMap<>();

    static {

        geolocationTypeLabelMap.put( DIVISION, "radio.options.gender_type.male" );
        geolocationTypeLabelMap.put( DISTRICT, "radio.options.gender_type.female" );
        geolocationTypeLabelMap.put( UPAZILA, "radio.options.gender_type.female" );

    }

    private Integer value;

    GeolocationType( Integer value ){

        this.value = value;
    }

    public static String getLabel( GeolocationType geolocationType ){

        return geolocationType.geolocationTypeLabelMap.get( geolocationType );
    }
}
