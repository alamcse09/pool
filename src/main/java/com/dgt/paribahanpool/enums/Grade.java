package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum Grade {

    GRADE1,
    GRADE2,
    GRADE3,
    GRADE4,
    GRADE5,
    GRADE6,
    GRADE7,
    GRADE8,
    GRADE9,
    GRADE10,
    GRADE11,
    GRADE12,
    GRADE13,
    GRADE14,
    GRADE15,
    GRADE16,
    GRADE17,
    GRADE18,
    GRADE19,
    GRADE20;
//    ADMINISTRATIVE_USE,
//    OTHERS;

    private static Map<Grade, String> gradeLabelMap = new HashMap<>();

    static {

        gradeLabelMap.put( GRADE1, "dropdown.options.grade1" );
        gradeLabelMap.put( GRADE2, "dropdown.options.grade2" );
        gradeLabelMap.put( GRADE3, "dropdown.options.grade3" );
        gradeLabelMap.put( GRADE4, "dropdown.options.grade4" );
        gradeLabelMap.put( GRADE5, "dropdown.options.grade5" );
        gradeLabelMap.put( GRADE6, "dropdown.options.grade6" );
        gradeLabelMap.put( GRADE7, "dropdown.options.grade7" );
        gradeLabelMap.put( GRADE8, "dropdown.options.grade8" );
        gradeLabelMap.put( GRADE9, "dropdown.options.grade9" );
        gradeLabelMap.put( GRADE10, "dropdown.options.grade10" );
        gradeLabelMap.put( GRADE11, "dropdown.options.grade11" );
        gradeLabelMap.put( GRADE12, "dropdown.options.grade12" );
        gradeLabelMap.put( GRADE13, "dropdown.options.grade13" );
        gradeLabelMap.put( GRADE14, "dropdown.options.grade14" );
        gradeLabelMap.put( GRADE15, "dropdown.options.grade15" );
        gradeLabelMap.put( GRADE16, "dropdown.options.grade16" );
        gradeLabelMap.put( GRADE17, "dropdown.options.grade17" );
        gradeLabelMap.put( GRADE18, "dropdown.options.grade18" );
        gradeLabelMap.put( GRADE19, "dropdown.options.grade19" );
        gradeLabelMap.put( GRADE20, "dropdown.options.grade20" );
//        gradeLabelMap.put( ADMINISTRATIVE_USE, "dropdown.options.administrative_use" );
//        gradeLabelMap.put( OTHERS, "dropdown.options.others" );
    }

    public static String getLabel( Grade grade ) {

        return gradeLabelMap.get( grade );
    }
}
