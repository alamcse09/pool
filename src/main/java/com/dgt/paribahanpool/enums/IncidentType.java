package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum IncidentType {

    LOST(1),
    ACCIDENT(2),
    BURNT(3),
    DROWNED(4),
    STOLEN(5);

    public static Map<IncidentType, String> incidentTypeStringMap = new HashMap<>();

    static {

        incidentTypeStringMap.put(LOST, "dropdown.options.incident.add.lost" );
        incidentTypeStringMap.put(ACCIDENT, "dropdown.options.incident.add.accident" );
        incidentTypeStringMap.put( BURNT, "dropdown.options.incident.add.burnt" );
        incidentTypeStringMap.put( DROWNED, "dropdown.options.incident.add.drowned" );
        incidentTypeStringMap.put( STOLEN, "dropdown.options.incident.add.stolen" );
    }

    private Integer value;

    IncidentType( Integer value ) {

        this.value = value;
    }

    public static String getLabel( IncidentType incidentType ) {

        return incidentTypeStringMap.get( incidentType );
    }
}
