package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum InventoryType {

    ORIGINAL(0),
    RECONDITION(1);

    private static Map<InventoryType,String> inventoryTypeLabelMap = new HashMap<>();

    static {

        inventoryTypeLabelMap.put( ORIGINAL, "dropdown.options.inventory.original" );
        inventoryTypeLabelMap.put( RECONDITION, "dropdown.options.inventory.recondition");
    }

    private Integer value;

    InventoryType( Integer value ){

        this.value = value;
    }

    public static String getLabel( InventoryType inventoryType ){

        return InventoryType.inventoryTypeLabelMap.get( inventoryType );
    }
}
