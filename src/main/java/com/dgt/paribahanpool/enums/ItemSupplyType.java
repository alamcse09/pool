package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum ItemSupplyType {

    FRAMEWORK_CONTRACT;

    private static Map<ItemSupplyType, String> itemSupplyTypeLabelMap = new HashMap<>();

    static {

        itemSupplyTypeLabelMap.put( FRAMEWORK_CONTRACT, "dropdown.options.vendor.add.item_supply_type.framework-contract" );
    }

    public static String getLabel( ItemSupplyType itemSupplyType ) {

        return itemSupplyTypeLabelMap.get(itemSupplyType);
    }
}
