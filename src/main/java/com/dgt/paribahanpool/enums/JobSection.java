package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum JobSection {

    TYRE(0),
    BATTERY(1),
    BODY(2),
    COLOR(3),
    AC(4),
    TAYLOR(5),
    MACHINE_SHOP(6);

    private static Map<JobSection,String> jobSectionLabelMap = new HashMap<>();

    static {

        jobSectionLabelMap.put( TYRE, "dropdown.options.job.section.tyre" );
        jobSectionLabelMap.put( BATTERY, "dropdown.options.job.section.battery" );
        jobSectionLabelMap.put( BODY, "dropdown.options.job.section.body" );
        jobSectionLabelMap.put( COLOR, "dropdown.options.job.section.color" );
        jobSectionLabelMap.put( AC, "dropdown.options.job.section.ac" );
        jobSectionLabelMap.put( TAYLOR, "dropdown.options.job.section.taylor" );
        jobSectionLabelMap.put( MACHINE_SHOP, "dropdown.options.job.section.machine_shop" );
    }

    private Integer value;

    JobSection( Integer value ){

        this.value = value;
    }

    public static String getLabel( JobSection jobSection ){

        return JobSection.jobSectionLabelMap.get( jobSection );
    }
}
