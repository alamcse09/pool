package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum JobType {

    PERMANENT,
    TEMPORARY;

    private static Map<JobType, String> jobTypeLabelMap = new HashMap<>();

    static {

        jobTypeLabelMap.put( PERMANENT, "dropdown.options.job_type.permanent" );
        jobTypeLabelMap.put( TEMPORARY, "dropdown.options.job_type.temporary" );
    }

    public static String getLabel( JobType jobType ) {

        return jobTypeLabelMap.get(jobType);
    }
}
