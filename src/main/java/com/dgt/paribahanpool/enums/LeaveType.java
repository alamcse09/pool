package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum LeaveType {

    SICK_LEAVE(1),
    YEARLY_LEAVE(2),
    CASUAL_LEAVE(3),
    EARNED_LEAVE(4),
    MEDICAL_LEAVE(5),
    ABROAD_LEAVE(6),
    AMUSEMENT_LEAVE(7),
    UNPAID_LEAVE(8),
    MATERNITY_LEAVE(9),
    OTHER_LEAVE(10);



    public static Map<LeaveType, String> leaveTypeStringMap = new HashMap<>();

    static {

        leaveTypeStringMap.put( CASUAL_LEAVE, "dropdown.options.leave.add.casual" );
        leaveTypeStringMap.put( SICK_LEAVE, "dropdown.options.leave.add.sick" );
        leaveTypeStringMap.put( YEARLY_LEAVE, "dropdown.options.leave.add.yearly" );
        leaveTypeStringMap.put( EARNED_LEAVE, "dropdown.options.leave.add.earned" );
        leaveTypeStringMap.put( MEDICAL_LEAVE, "dropdown.options.leave.add.medical" );
        leaveTypeStringMap.put( ABROAD_LEAVE, "dropdown.options.leave.add.abroad" );
        leaveTypeStringMap.put( AMUSEMENT_LEAVE, "dropdown.options.leave.add.amusement" );
        leaveTypeStringMap.put( UNPAID_LEAVE, "dropdown.options.leave.add.unpaid" );
        leaveTypeStringMap.put( MATERNITY_LEAVE, "dropdown.options.leave.add.maternity" );
        leaveTypeStringMap.put( OTHER_LEAVE, "dropdown.options.leave.add.other" );
    }

    private Integer value;

    LeaveType( Integer value ) {

        this.value = value;
    }

    public static String getLabel( LeaveType leaveType ) {

        return leaveTypeStringMap.get( leaveType );
    }
}
