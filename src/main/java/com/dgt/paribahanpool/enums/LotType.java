package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum LotType {
    GENUINE(1),
    OTHER_COUNTRY(2),
    RECONDITION(3);

    private static Map<LotType,String> lotTypeLabelMap = new HashMap<>();

    static{
        lotTypeLabelMap.put(GENUINE,"dropdown.options.inventory.lot.type.genuine");
        lotTypeLabelMap.put(OTHER_COUNTRY,"dropdown.options.inventory.lot.type.other.country");
        lotTypeLabelMap.put(RECONDITION,"dropdown.options.inventory.lot.type.recondition");
    }

    private Integer value;

    LotType(Integer value){

        this.value = value;

    }

    public static String getLabel(LotType lotType){

        return LotType.lotTypeLabelMap.get( lotType );
    }
}


