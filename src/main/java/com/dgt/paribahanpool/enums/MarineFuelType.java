package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum MarineFuelType {
    PETROL(1),
    OCTANE(2),
    DIESEL(3);

    private static Map<MarineFuelType,String> fuelTypeLabelMap = new HashMap<>();

    static {

        fuelTypeLabelMap.put( DIESEL, "dropdown.options.marine.add.fuel_type.diesel" );
        fuelTypeLabelMap.put( PETROL, "dropdown.options.marine.add.fuel_type.petrol");
        fuelTypeLabelMap.put( OCTANE, "dropdown.options.marine.add.fuel_type.octane");

    }

    private Integer value;

    MarineFuelType( Integer value ){

        this.value = value;
    }

    public static String getLabel( MarineFuelType marineFuelType ){

        return MarineFuelType.fuelTypeLabelMap.get( marineFuelType );
    }


}
