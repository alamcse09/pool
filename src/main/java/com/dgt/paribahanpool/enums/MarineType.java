package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum MarineType {
    SPEED_BOAT(1),
    CABIN_CRUISER(2);

    private static Map<MarineType,String> marineTypeLabelMap = new HashMap<>();

    static {

        marineTypeLabelMap.put( SPEED_BOAT, "dropdown.options.marine.add.marine_type.speed_boat" );
        marineTypeLabelMap.put( CABIN_CRUISER, "dropdown.options.marine.add.marine_type.cabin_cruiser");

    }

    private Integer value;

    MarineType( Integer value ){

        this.value = value;
    }

    public static String getLabel( MarineType marineType ){

        return MarineType.marineTypeLabelMap.get( marineType );
    }


}
