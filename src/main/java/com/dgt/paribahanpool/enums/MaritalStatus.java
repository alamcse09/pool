package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum MaritalStatus {

    MARRIED,
    UNMARRIED;

    private static Map<MaritalStatus, String> maritalStatusLabelMap = new HashMap<>();

    static {

        maritalStatusLabelMap.put( MARRIED, "dropdown.options.marital.status.married" );
        maritalStatusLabelMap.put( UNMARRIED, "dropdown.options.marital.status.unmarried" );
    }

    public static String getLabel( MaritalStatus maritalStatus ) {

        return maritalStatusLabelMap.get(maritalStatus);
    }
}
