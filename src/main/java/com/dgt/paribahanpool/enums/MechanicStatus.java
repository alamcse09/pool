package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum MechanicStatus {
    BLACKLIST(1),
    ACTIVE(2);

    private static Map<MechanicStatus,String> statusTypeLabelMap = new HashMap<>();

    static {

        statusTypeLabelMap.put( BLACKLIST, "dropdown.options.mechanic.add.status_type.blacklist" );
        statusTypeLabelMap.put(ACTIVE, "dropdown.options.mechanic.add.status_type.processing");

    }

    private Integer value;

    MechanicStatus(Integer value ){

        this.value = value;
    }

    public static String getLabel( MechanicStatus mechanicStatus){

        return MechanicStatus.statusTypeLabelMap.get(mechanicStatus);
    }
}
