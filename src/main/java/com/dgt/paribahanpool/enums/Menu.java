package com.dgt.paribahanpool.enums;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum Menu {

    DASHBOARD,

    VEHICLE_ADD,
    VEHICLE_SEARCH,
    VEHICLE_AUCTION,
    VEHICLE_USER,
    MARINE_ADD,
    MARINE_SEARCH,
    MECHANIC_ADD,
    MECHANIC_SEARCH,
    NOC_ADD,
    NOC_SEARCH,
    PROJECT_VEHICLE_ADD,
    PROJECT_VEHICLE_SEARCH,
    VENDOR_ADD,
    VENDOR_SEARCH,
    DRIVER_ADD,
    DRIVER_REQUISITION_ADD,
    DRIVER_REQUISITION_SEARCH,
    DRIVER_REQUISITION_VIEW,
    DRIVER_SEARCH,
    INCIDENT_ADD,
    INCIDENT_SEARCH,
    INCIDENT_VIEW,
    BUDGET_REPORT,
    PURCHASE_DEMAND_ADD,
    EMPLOYEE_ADD,
    DRIVER_ASSIGN_ADD,
    DRIVE_ASSIGN_SEARCH,
    CHANGE_PASSWORD,
    MY_PROFILE,
    EMPLOYEE_SEARCH,
    AUDIT_LOG_SEARCH,
    USER_LEVEL_ADD,
    USER_LEVEL_SEARCH,
    USER_SEARCH,
    REPLACE_APPLICATION_VIEW,
    REPLACE_APPLICATION_SEARCH,
    REPLACE_APPLICATION_ADD,
    REPLACE_VEHICLE_ADD,
    DESIGNATION_ADD,
    DESIGNATION_SEARCH,
    MARINE_SERVICE_APPLICATION_ADD,
    MARINE_SERVICE_APPLICATION_SEARCH,
    MARINE_SERVICE_APPLICATION_VIEW,
    LEAVE_APPLICATION_ADD,
    LEAVE_APPLICATION_SEARCH,
    LEAVE_APPROVAL_SEARCH,
    NOC_MY_SEARCH,
    REPLACE_APPLICATION_MY_SEARCH,
    NOC_APPLICATION_VIEW,
    VEHICLE_SERVICE_ADD,
    INVENTORY_ADD,
    INVENTORY_SEARCH,
    INVENTORY_REPORT,
    VEHICLE_SERVICE_SEARCH,
    VEHICLE_SERVICE_MY_SEARCH,
    MEETING_ADD,
    MEETING_SEARCH,
    MEETING_MINUTES_UPDATE,
    MEETING_VIEW,
    VEHICLE_SERVICE_VIEW,
    BUDGET_CODE_SEARCH,
    BUDGET_CODE_ADD,
    BUDGET_ADD,
    BUDGET_SEARCH,
    BUDGET_VIEW,
    BUDGET_REVISED_AMOUNT,
    BUDGET_FINAL_AMOUNT,
    BILL_ADD,
    BILL_PREVIEW,
    BILL_SEARCH,
    CHEQUE_ISSUE,
    CHEQUE_DISTRIBUTION,
    COMPLAINS_ADD,
    COMPLAINS_SEARCH,
    COMPLAINS_MY_SEARCH,
    COMPLAINS_VIEW,
    TRAINING_ADD,
    TRAINING_SEARCH,
    TRAINING_ATTENDANCE_SEARCH,
    TRAINING_ATTENDANCE_ADD,
    TRAINING_ATTENDEE_ADD,
    TRAINING_ATTENDEE_SEARCH,
    TRAINING_EVALUATION_SEARCH,
    TRAINING_VIEW,
    TRAINING_EXPENSE_ADD,
    TRAINING_EXPENSE_SEARCH,
    WARRANTY_ADD,
    WARRANTY_SEARCH,
    WARRANTY_VIEW,
    TRAINING_PREVIEW,
    RERPORT_EMPLOYEE,
    MARINE_SERVICE_APPLICATION_DISPOSITION_VIEW,
    MARINE_SERVICE_DISPOSITION_APPLICATION_SEARCH,
    PURCHASE_DEMAND_SEARCH,
    OFFICE_UNIT_ORGANOGRAM_ADD,
    OFFICE_UNIT_ORGANOGRAM_SEARCH,
    OFFICE_UNIT_ORGANOGRAM,
    OFFICE_UNIT_ADD,
    SUPPLY_ORDER_ADD,
    SUPPLY_ORDER_SEARCH,
    PARTS_DISPOSITION_ADD,
    PARTS_DISPOSITION_SEARCH,
    PARTS_DISPOSITION_VIEW,
    VEHICLE_AUCTION_VIEW,
    SETTING_ADD,
    RERPORT_REQUISITION,
    RERPORT_DRIVER;

    private static Map<Menu, List<String>> breadcrumbText = new HashMap<>();

    static {

        breadcrumbText.put( VEHICLE_ADD, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.motor" ,"breadcrumb.vehicle.add" } ) );
        breadcrumbText.put( VEHICLE_SEARCH, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.motor" ,"breadcrumb.vehicle.search" } ) );
        breadcrumbText.put( VEHICLE_AUCTION, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.motor" ,"breadcrumb.vehicle.auction" } ) );

        breadcrumbText.put( VEHICLE_SERVICE_ADD, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.motor", "breadcrumb.vehicle.service" } ) );
        breadcrumbText.put( VEHICLE_SERVICE_SEARCH, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.motor", "breadcrumb.vehicle.service" } ) );
        breadcrumbText.put( VEHICLE_SERVICE_MY_SEARCH, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.motor", "breadcrumb.vehicle.service" } ) );

        breadcrumbText.put( VEHICLE_SERVICE_VIEW, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.motor", "breadcrumb.vehicle.service" } ) );

        breadcrumbText.put( MARINE_ADD, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.marine.management", "breadcrumb.vehicle.marine.add" } ) );
        breadcrumbText.put( MARINE_SEARCH, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.marine.management", "breadcrumb.vehicle.marine.search" } ) );
        breadcrumbText.put( NOC_ADD, Arrays.asList( new String[]{"breadcrumb.vehicle.management", "breadcrumb.vehicle.motor", "breadcrumb.vehicle.noc.add"}));
        breadcrumbText.put( NOC_SEARCH, Arrays.asList( new String[]{"breadcrumb.vehicle.management", "breadcrumb.vehicle.motor", "breadcrumb.vehicle.noc.search"}));
        breadcrumbText.put( MECHANIC_ADD, Arrays.asList( new String[]{ "breadcrumb.hr-management", "breadcrumb.hr-management.mechanic-management", "breadcrumb.hr-management.mechanic-management.add" } ) );
        breadcrumbText.put( MECHANIC_SEARCH, Arrays.asList( new String[]{ "breadcrumb.hr-management", "breadcrumb.hr-management.mechanic-management", "breadcrumb.hr-management.mechanic-management.search" } ) );
        breadcrumbText.put( DASHBOARD, Arrays.asList( new String[]{ "breadcrumb.dashboard" } ) );
        breadcrumbText.put( PROJECT_VEHICLE_ADD, Arrays.asList( new String[]{ "menu.vehicle-management.motor-vehicle.management.enlistment", "menu.vehicle-management.motor-vehicle.management.enlistment.project-vehicles" } ));
        breadcrumbText.put( PROJECT_VEHICLE_SEARCH, Arrays.asList( new String[]{ "menu.vehicle-management.motor-vehicle.management.enlistment", "breadcrumb.vehicle.project-vehicle.add" } ));
        breadcrumbText.put( VENDOR_ADD, Arrays.asList( new String[]{ "breadcrumb.vendor.management", "breadcrumb.vendor.management.add" } ));
        breadcrumbText.put( VENDOR_SEARCH, Arrays.asList( new String[]{ "breadcrumb.vendor.management", "breadcrumb.vendor.management.search" } ));
        breadcrumbText.put( PURCHASE_DEMAND_ADD, Arrays.asList( new String[]{ "menu.purchase-collection-management", "menu.purchase-collection-management.purchase-demand" } ));
        breadcrumbText.put( EMPLOYEE_ADD, Arrays.asList( new String[]{ "breadcrumb.hr-management", "title.employee.add" } ));
        breadcrumbText.put( EMPLOYEE_SEARCH, Arrays.asList( new String[]{ "breadcrumb.hr-management", "title.employee.search" } ));
        breadcrumbText.put( DRIVER_ADD, Arrays.asList( new String[]{ "breadcrumb.hr-management", "breadcrumb.hr-management.driver.management", "breadcrumb.hr-management.driver.add" } ));
        breadcrumbText.put( DRIVER_SEARCH, Arrays.asList( new String[]{ "breadcrumb.hr-management", "breadcrumb.hr-management.driver.management", "breadcrumb.hr-management.driver.search" } ));
        breadcrumbText.put( DRIVER_REQUISITION_ADD, Arrays.asList( new String[]{ "breadcrumb.hr-management", "breadcrumb.hr-management.driver.management", "breadcrumb.hr-management.driver.requisition" } ));
        breadcrumbText.put( DRIVER_REQUISITION_SEARCH, Arrays.asList( new String[]{ "breadcrumb.hr-management", "breadcrumb.hr-management.driver.management", "breadcrumb.hr-management.driver.requisition.search" } ));
        breadcrumbText.put( DRIVER_REQUISITION_VIEW, Arrays.asList( new String[]{ "breadcrumb.hr-management", "breadcrumb.hr-management.driver.management", "breadcrumb.hr-management.driver.requisition.view" } ));
        breadcrumbText.put( INCIDENT_ADD, Arrays.asList( new String[]{ "breadcrumb.incident.add" } ) );
        breadcrumbText.put( INCIDENT_SEARCH, Arrays.asList( new String[]{ "breadcrumb.incident.search" } ) );
        breadcrumbText.put( DRIVER_ASSIGN_ADD, Arrays.asList( new String[]{ "breadcrumb.hr-management", "breadcrumb.hr-management.driver.management", "breadcrumb.hr-management.driver.assign.add" } ) );
        breadcrumbText.put( DRIVE_ASSIGN_SEARCH, Arrays.asList( new String[]{ "breadcrumb.hr-management", "breadcrumb.hr-management.driver.management", "breadcrumb.hr-management.driver.assign.search" } ) );
        breadcrumbText.put( CHANGE_PASSWORD, Arrays.asList( new String[]{ "breadcrumb.user", "breadcrumb.user.password" } ) );
        breadcrumbText.put( VEHICLE_USER, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "title.vehicle.requisition" } ) );
        breadcrumbText.put( AUDIT_LOG_SEARCH, Arrays.asList( new String[]{ "breadcrumb.audit-log", "breadcrumb.audit-log.search" } ) );
        breadcrumbText.put( USER_LEVEL_ADD, Arrays.asList( new String[]{ "breadcrumb.user-level", "breadcrumb.user-level-add" } ) );
        breadcrumbText.put( USER_LEVEL_SEARCH, Arrays.asList( new String[]{ "breadcrumb.user-level", "breadcrumb.user-level-search" } ) );
        breadcrumbText.put( USER_SEARCH, Arrays.asList( new String[]{ "breadcrumb.user", "breadcrumb.user.search"} ) );
        breadcrumbText.put( REPLACE_APPLICATION_ADD,Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "title.replace.application.add"} ) );
        breadcrumbText.put( REPLACE_APPLICATION_SEARCH, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "title.replace.application.search"} ) );
        breadcrumbText.put( REPLACE_VEHICLE_ADD, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "title.replace.vehicle"} ) );
        breadcrumbText.put( DESIGNATION_ADD, Arrays.asList( new String[]{ "breadcrumb.user-management", "breadcrumb.user-management.designation", "breadcrumb.user-management.designation.add"} ) );
        breadcrumbText.put( DESIGNATION_SEARCH, Arrays.asList( new String[]{ "breadcrumb.user-management", "breadcrumb.user-management.designation", "breadcrumb.user-management.designation.search"} ) );
        breadcrumbText.put( MARINE_SERVICE_APPLICATION_ADD, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.marine.management", "breadcrumb.vehicle.marine-service-application.add"} ) );
        breadcrumbText.put( MARINE_SERVICE_APPLICATION_SEARCH, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.marine.management", "breadcrumb.vehicle.marine-service-application.search"} ) );
        breadcrumbText.put( MARINE_SERVICE_APPLICATION_VIEW, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.marine.management", "breadcrumb.vehicle.marine-service-application.view"} ) );
        breadcrumbText.put( LEAVE_APPLICATION_ADD, Arrays.asList( new String[]{ "breadcrumb.hr-management", "breadcrumb.attendance-leave.leave", "breadcrumb.attendance-leave.leave.add"} ) );
        breadcrumbText.put( LEAVE_APPLICATION_SEARCH, Arrays.asList( new String[]{ "breadcrumb.hr-management", "breadcrumb.attendance-leave.leave", "breadcrumb.attendance-leave.leave.search"} ) );
        breadcrumbText.put( LEAVE_APPROVAL_SEARCH, Arrays.asList( new String[]{ "breadcrumb.hr-management", "breadcrumb.attendance-leave.leave", "breadcrumb.attendance-leave.leave.leave-approval-search"} ) );
        breadcrumbText.put( NOC_MY_SEARCH, Arrays.asList( new String[]{"breadcrumb.vehicle.management", "breadcrumb.vehicle.motor", "breadcrumb.vehicle.noc.search"}));
        breadcrumbText.put( REPLACE_APPLICATION_MY_SEARCH, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "title.replace.application.search"} ) );
        breadcrumbText.put( INVENTORY_ADD, Arrays.asList( new String[]{ "menu.store-management", "menu.store-management.store-attachment"} ) );
        breadcrumbText.put( INVENTORY_SEARCH, Arrays.asList( new String[]{ "menu.store-management", "title.inventory.search"} ) );
        breadcrumbText.put( INVENTORY_REPORT, Arrays.asList( new String[]{ "menu.store-management", "title.inventory.report"} ) );
        breadcrumbText.put( MEETING_ADD, Arrays.asList( new String[]{ "breadcrumb.meeting", "breadcrumb.meeting.add"} ) );
        breadcrumbText.put( MEETING_SEARCH, Arrays.asList( new String[]{ "breadcrumb.meeting", "breadcrumb.meeting.search"} ) );
        breadcrumbText.put( MEETING_MINUTES_UPDATE, Arrays.asList( new String[]{ "breadcrumb.meeting", "breadcrumb.meeting.meeting-minutes-update"} ) );
        breadcrumbText.put( MEETING_VIEW, Arrays.asList( new String[]{ "breadcrumb.meeting", "breadcrumb.meeting.view"} ) );
        breadcrumbText.put( BUDGET_CODE_SEARCH, Arrays.asList( new String[]{ "breadcrumb.budget", "breadcrumb.budget.code-search"} ) );
        breadcrumbText.put( BUDGET_CODE_ADD, Arrays.asList( new String[]{ "breadcrumb.budget", "breadcrumb.budget.code-add"} ) );
        breadcrumbText.put( BUDGET_ADD, Arrays.asList( new String[]{ "breadcrumb.budget", "breadcrumb.budget.add"} ) );
        breadcrumbText.put( BUDGET_SEARCH, Arrays.asList( new String[]{ "breadcrumb.budget", "breadcrumb.budget.search"} ) );
        breadcrumbText.put( BUDGET_VIEW, Arrays.asList( new String[]{ "breadcrumb.budget", "breadcrumb.budget.view"} ) );
        breadcrumbText.put( BUDGET_REVISED_AMOUNT, Arrays.asList( new String[]{ "breadcrumb.budget", "breadcrumb.budget.revised-search"} ) );
        breadcrumbText.put( BUDGET_FINAL_AMOUNT, Arrays.asList( new String[]{ "breadcrumb.budget", "breadcrumb.budget.final-search"} ) );
        breadcrumbText.put( COMPLAINS_ADD, Arrays.asList( new String[]{ "breadcrumb.complains", "breadcrumb.complains.add"} ) );
        breadcrumbText.put( COMPLAINS_SEARCH, Arrays.asList( new String[]{ "breadcrumb.complains", "breadcrumb.complains.search"} ) );
        breadcrumbText.put( COMPLAINS_MY_SEARCH, Arrays.asList( new String[]{ "breadcrumb.complains", "breadcrumb.complains.search"} ) );
        breadcrumbText.put( COMPLAINS_VIEW, Arrays.asList( new String[]{ "breadcrumb.complains", "breadcrumb.complains.view"} ) );
        breadcrumbText.put( TRAINING_ADD, Arrays.asList( new String[]{ "breadcrumb.training", "breadcrumb.training.add"} ) );
        breadcrumbText.put( TRAINING_SEARCH, Arrays.asList( new String[]{ "breadcrumb.training", "breadcrumb.training.search"} ) );
        breadcrumbText.put( TRAINING_ATTENDANCE_SEARCH, Arrays.asList( new String[]{ "breadcrumb.training", "breadcrumb.training.attendance.search"} ) );
        breadcrumbText.put( TRAINING_ATTENDANCE_ADD, Arrays.asList( new String[]{ "breadcrumb.training", "breadcrumb.training.attendance.add"} ) );
        breadcrumbText.put( TRAINING_ATTENDEE_ADD, Arrays.asList( new String[]{ "breadcrumb.training", "breadcrumb.training.attendee.add"} ) );
        breadcrumbText.put( TRAINING_ATTENDEE_SEARCH, Arrays.asList( new String[]{ "breadcrumb.training", "breadcrumb.training.attendee.search"} ) );
        breadcrumbText.put( TRAINING_EVALUATION_SEARCH, Arrays.asList( new String[]{ "breadcrumb.training", "breadcrumb.training.evaluation.search"} ) );
        breadcrumbText.put( TRAINING_VIEW, Arrays.asList( new String[]{ "breadcrumb.training", "breadcrumb.training.view"} ) );
        breadcrumbText.put( TRAINING_EXPENSE_ADD, Arrays.asList( new String[]{ "breadcrumb.training", "breadcrumb.training.expense.add"} ) );
        breadcrumbText.put( TRAINING_PREVIEW, Arrays.asList( new String[]{ "breadcrumb.training", "breadcrumb.training.report"} ) );
        breadcrumbText.put( TRAINING_EXPENSE_SEARCH, Arrays.asList( new String[]{ "breadcrumb.training", "breadcrumb.training.expense.search"} ) );
        breadcrumbText.put( WARRANTY_ADD, Arrays.asList( new String[]{ "breadcrumb.store-management", "breadcrumb.store-management.warranty-add"} ) );
        breadcrumbText.put( WARRANTY_SEARCH, Arrays.asList( new String[]{ "breadcrumb.store-management", "breadcrumb.store-management.warranty-search"} ) );
        breadcrumbText.put( CHEQUE_ISSUE, Arrays.asList( new String[]{ "breadcrumb.bill-management", "breadcrumb.bill-management.cheque-issue-add"} ) );
        breadcrumbText.put( BILL_PREVIEW, Arrays.asList( new String[]{ "breadcrumb.bill-management", "breadcrumb.bill-management.bill-preview"} ) );
        breadcrumbText.put( RERPORT_EMPLOYEE, Arrays.asList( new String[]{ "breadcrumb.report-management", "breadcrumb.report-management.employee"} ) );
        breadcrumbText.put( MARINE_SERVICE_APPLICATION_DISPOSITION_VIEW, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.marine.management", "breadcrumb.vehicle.marine-service-application.disposition-view"} ) );
        breadcrumbText.put( MARINE_SERVICE_DISPOSITION_APPLICATION_SEARCH, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.marine.management", "breadcrumb.vehicle.marine-service-application.disposition-search"} ) );
        breadcrumbText.put( WARRANTY_VIEW, Arrays.asList( new String[]{ "breadcrumb.store-management", "breadcrumb.store-management.warranty-view"} ) );
        breadcrumbText.put( PARTS_DISPOSITION_ADD, Arrays.asList( new String[]{ "breadcrumb.store-management", "breadcrumb.store-management.parts-dispostion-add"} ) );
        breadcrumbText.put( VEHICLE_AUCTION_VIEW, Arrays.asList( new String[]{ "breadcrumb.vehicle.management", "breadcrumb.vehicle.motor" ,"breadcrumb.vehicle.auction.view" } ) );
        breadcrumbText.put( RERPORT_REQUISITION, Arrays.asList( new String[]{ "breadcrumb.report-management", "breadcrumb.report-management.requisition"} ) );
        breadcrumbText.put( RERPORT_DRIVER, Arrays.asList( new String[]{ "breadcrumb.report-management", "breadcrumb.report-management.driver"} ) );
    }
    public static List<String> getBreadcrumbText(Menu menu ){

        return breadcrumbText.get( menu );
    }
}
