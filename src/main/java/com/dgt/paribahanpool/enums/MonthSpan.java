package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum MonthSpan {

    APRIL_TO_JUNE( 1 ),
    JULY_TO_SEPTEMBER(2),
    OCTOBER_TO_DECEMBER(3),
    JANUARY_TO_MARCH(4);

    private static Map<MonthSpan,String> monthSpanLabelMap = new HashMap<>();

    static {

        monthSpanLabelMap.put( APRIL_TO_JUNE, "dropdown.options.training.add.month_span.april_to_june" );
        monthSpanLabelMap.put( JULY_TO_SEPTEMBER, "dropdown.options.training.add.month_span.july_to_september");
        monthSpanLabelMap.put( OCTOBER_TO_DECEMBER, "dropdown.options.training.add.month_span.october_to_december");
        monthSpanLabelMap.put( JANUARY_TO_MARCH, "dropdown.options.training.add.month_span.january_to_march");

    }

    private Integer value;

    MonthSpan( Integer value ){

        this.value = value;
    }

    public static String getLabel( MonthSpan monthSpan ){

        return MonthSpan.monthSpanLabelMap.get( monthSpan );
    }
}
