package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum NocApplicationType {

    PLR,
    RENEW_VEHICLE;

    private static Map<NocApplicationType, String> nocApplicationTypeLabelMap = new HashMap<>();

    static {

        nocApplicationTypeLabelMap.put( PLR, "dropdown.options.noc.add.noc_application.plr" );
        nocApplicationTypeLabelMap.put( RENEW_VEHICLE, "dropdown.options.noc.add.noc_application.renew_vehicle" );
    }

    public static String getLabel( NocApplicationType nocApplicationType ) {

        return nocApplicationTypeLabelMap.get(nocApplicationType);
    }
}
