package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum OwnerType {

    DIVISION(0),
    DISTRICT(1),
    UPAZILA(2),
    USER(3);

    private static Map<OwnerType,String> ownerTypeLabelMap = new HashMap<>();

    static {

        ownerTypeLabelMap.put( DIVISION, "dropdown.options.owner.type.division" );
        ownerTypeLabelMap.put( DISTRICT, "dropdown.options.owner.type.district");
        ownerTypeLabelMap.put( UPAZILA, "dropdown.options.owner.type.upazila");
        ownerTypeLabelMap.put( USER, "dropdown.options.owner.type.user");
    }

    private Integer value;

    OwnerType( Integer value ) {

        this.value = value;
    }

    public static String getLabel( OwnerType ownerType ){

        return OwnerType.ownerTypeLabelMap.get( ownerType );
    }
}
