package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum ProductType {

    STATIONARY(0),
    OIL(1),
    PARTS(2);

    private static Map<ProductType,String> productTypeLabelMap = new HashMap<>();

    static {

        productTypeLabelMap.put( STATIONARY, "dropdown.purchase.demand.add.product_type.stationary" );
        productTypeLabelMap.put( OIL, "dropdown.purchase.demand.add.product_type.oil");
        productTypeLabelMap.put( PARTS, "dropdown.purchase.demand.add.product_type.parts");
    }

    private Integer value;

    ProductType( Integer value ){

        this.value = value;
    }

    public static String getLabel( ProductType productType ){

        return ProductType.productTypeLabelMap.get( productType );
    }
}
