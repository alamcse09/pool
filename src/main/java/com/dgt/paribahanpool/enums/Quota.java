package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum Quota {

    FREEDOM_FIGHTER,
    DISABLED,
    ANSAR_VDP;

    private static Map<Quota, String> quotaLabelMap = new HashMap<>();

    static {

        quotaLabelMap.put( FREEDOM_FIGHTER, "dropdown.options.quota.freedom.fighter" );
        quotaLabelMap.put( DISABLED, "dropdown.options.quota.disabled" );
        quotaLabelMap.put( ANSAR_VDP, "dropdown.options.quota.ansar_vdp" );
    }

    public static String getLabel( Quota quota ) {

        return quotaLabelMap.get( quota );
    }
}
