package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum RegistrationType {

    ACTIVE(1),
    EXPIRED(2);

    private static Map<RegistrationType, String> registrationTypeLabelMap = new HashMap<>();

    static {

        registrationTypeLabelMap.put( ACTIVE, "dropdown.options.vehicle.add.registration_type.active" );
        registrationTypeLabelMap.put( EXPIRED, "dropdown.options.vehicle.add.registration_type.expired" );
    }

    private Integer value;

    RegistrationType( Integer value ) {

        this.value = value;
    }

    public static String getLabel( RegistrationType registrationType ) {

        return registrationTypeLabelMap.get(registrationType);
    }
}
