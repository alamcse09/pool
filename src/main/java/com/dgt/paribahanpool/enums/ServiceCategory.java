package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum ServiceCategory {

    REGULAR_CHECKUP(0),
    REPAIRING(1);

    private Integer val;

    ServiceCategory( Integer value ){

        this.val = value;
    }

    private static Map<ServiceCategory, String> serviceCategoryLabelMap = new HashMap<>();

    static {

        serviceCategoryLabelMap.put( REGULAR_CHECKUP, "dropdown.options.service-category.regular" );
        serviceCategoryLabelMap.put( REPAIRING, "dropdown.options.service-category.repairing" );
    }

    public static String getLabel( ServiceCategory serviceCategory ) {

        return serviceCategoryLabelMap.get( serviceCategory );
    }
}
