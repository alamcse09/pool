package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum ServiceType {

    MAJOR(0),
    MINOR(1);

    private Integer val;

    ServiceType( Integer value ){

        this.val = value;
    }

    private static Map<ServiceType, String> serviceTypeLabelMap = new HashMap<>();

    static {

        serviceTypeLabelMap.put( MAJOR, "dropdown.options.service-type.major" );
        serviceTypeLabelMap.put( MINOR, "dropdown.options.service-type.minor" );
    }

    public static String getLabel( ServiceType serviceType ) {

        return serviceTypeLabelMap.get( serviceType );
    }
}
