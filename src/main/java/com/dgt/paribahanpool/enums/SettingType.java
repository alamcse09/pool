package com.dgt.paribahanpool.enums;

public enum SettingType {

	NOC_DISTRIBUTION_FOOTER(0),
	DUMMY_CHECKBOX(1),
	DUMMY_RADIO(2);

	private Integer settingType;

	SettingType(Integer ordinal ){

		this.settingType = ordinal;
	}
}
