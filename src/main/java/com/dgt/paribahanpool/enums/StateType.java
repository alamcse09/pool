package com.dgt.paribahanpool.enums;

public enum StateType {

    START,
    POSITIVE_END,
    NEGATIVE_END,
    INTERMEDIATE;
}
