package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum Status {

    PENDING(0),
    PROCESSING(1),
    APPROVED(2),
    REJECTED(3),

    AUCTIONABLE(4),
    SOLD(5);

    private Integer value;

    Status( Integer value ){

        this.value = value;
    }

    private static Map<Status, String> statusLabelMap = new HashMap<>();

    static {

        statusLabelMap.put( PENDING, "dropdown.options.status.pending" );
        statusLabelMap.put( PROCESSING, "dropdown.options.status.processing" );
        statusLabelMap.put( APPROVED, "dropdown.options.status.approved" );
        statusLabelMap.put( REJECTED, "dropdown.options.status.rejected" );

        statusLabelMap.put( AUCTIONABLE, "dropdown.options.status.auctionable" );
        statusLabelMap.put( SOLD, "dropdown.options.status.sold" );
    }

    public static String getLabel( Status status ) {

        return statusLabelMap.get(status);
    }
}
