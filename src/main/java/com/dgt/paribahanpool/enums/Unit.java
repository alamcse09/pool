package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum Unit {

    SET(0),
    KG(1),
    LITER(2);

    private static Map<Unit,String> unitLabelMap = new HashMap<>();
    private static Map<Unit,Boolean> isSupportFraction = new HashMap<>();

    static {

        unitLabelMap.put( SET, "dropdown.options.unit.set" );
        unitLabelMap.put( KG, "dropdown.options.unit.kg");
        unitLabelMap.put( LITER, "dropdown.options.unit.liter");
        isSupportFraction.put( SET, false);
        isSupportFraction.put( KG, true);
        isSupportFraction.put( LITER, true);
    }

    private Integer value;

    Unit( Integer value ){

        this.value = value;
    }

    public static String getLabel( Unit unit ){

        return Unit.unitLabelMap.get( unit );
    }

    public static Boolean isSupportFraction( Unit unit ) {

        if(unit==null) return false;

        return Unit.isSupportFraction.get(unit);
    }
}
