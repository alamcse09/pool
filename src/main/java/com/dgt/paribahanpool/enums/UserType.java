package com.dgt.paribahanpool.enums;

public enum UserType {

    SYSTEM_USER,
    PUBLIC,
    MECHANIC;
}
