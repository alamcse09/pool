package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum VehicleAllotment {

    GIVEN,
    NOT_GIVEN;

    private static Map<VehicleAllotment, String> vehicleAllotmentLabelMap = new HashMap<>();

    static {

        vehicleAllotmentLabelMap.put( GIVEN, "dropdown.options.noc.add.vehicle_allotment.given" );
        vehicleAllotmentLabelMap.put( NOT_GIVEN, "dropdown.options.noc.add.vehicle_allotment.not_given" );
    }

    public static String getLabel( VehicleAllotment vehicleAllotment ) {

        return vehicleAllotmentLabelMap.get(vehicleAllotment);
    }

}
