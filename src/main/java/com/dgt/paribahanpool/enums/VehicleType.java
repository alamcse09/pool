package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum VehicleType {

    CAR(1),
    JEEP(2),
    MICRO_BUS(3),
    MINI_BUS(4),
    BUS(5),
    PICK_UP_VAN(6),
    AMBULANCE(7),
    FREEZER_VAN(8),
    MOTOR_CYCLE( 10 ),
    OTHERS(9);

    private static Map<VehicleType, String> vehicleTypeLabelMap = new HashMap<>();

    static {

        vehicleTypeLabelMap.put( CAR, "dropdown.options.vehicle.add.vehicle_type.car" );
        vehicleTypeLabelMap.put( JEEP, "dropdown.options.vehicle.add.vehicle_type.jeep" );
        vehicleTypeLabelMap.put( MICRO_BUS, "dropdown.options.vehicle.add.vehicle_type.micro_bus" );
        vehicleTypeLabelMap.put( MINI_BUS, "dropdown.options.vehicle.add.vehicle_type.mini_bus" );
        vehicleTypeLabelMap.put( BUS, "dropdown.options.vehicle.add.vehicle_type.bus" );
        vehicleTypeLabelMap.put( PICK_UP_VAN, "dropdown.options.vehicle.add.vehicle_type.pickup_van" );
        vehicleTypeLabelMap.put( AMBULANCE, "dropdown.options.vehicle.add.vehicle_type.ambulance" );
        vehicleTypeLabelMap.put( FREEZER_VAN, "dropdown.options.vehicle.add.vehicle_type.freezer_van" );
        vehicleTypeLabelMap.put( MOTOR_CYCLE, "dropdown.options.vehicle.add.vehicle_type.motor_cycle" );
        vehicleTypeLabelMap.put( OTHERS, "dropdown.options.vehicle.add.vehicle_type.others" );
    }

    private Integer value;

    VehicleType( Integer value ) {

        this.value = value;
    }

    public static String getLabel( VehicleType vehicleType ) {

        return vehicleTypeLabelMap.get(vehicleType);
    }
}
