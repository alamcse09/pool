package com.dgt.paribahanpool.enums;

import java.util.HashMap;
import java.util.Map;

public enum WarrantyType {

    FROM_BUYING_DATE(0),
    FROM_USING_DATE(1);

    private Integer value;

    WarrantyType( Integer value ){

        this.value = value;
    }

    private static Map<WarrantyType, String> warrantyTypeLabelMap = new HashMap<>();

    static {

        warrantyTypeLabelMap.put( FROM_BUYING_DATE, "dropdown.options.warranty.from-buy-date" );
        warrantyTypeLabelMap.put( FROM_USING_DATE, "dropdown.options.status.from-using-date" );
    }

    public static String getLabel( WarrantyType warrantyType ) {

        return warrantyTypeLabelMap.get(warrantyType);
    }
}
