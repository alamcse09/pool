package com.dgt.paribahanpool.excel;

import com.dgt.paribahanpool.excel.model.ExcelData;
import com.dgt.paribahanpool.excel.model.SheetCell;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public class ExcelBuilder {

    private XSSFWorkbook workbook;

    public static ExcelBuilder createExcel( ExcelData excelData ){

        ExcelBuilder excelBuilder = new ExcelBuilder();

        excelBuilder.workbook = new XSSFWorkbook();

        excelBuilder.createSheets( excelData.getSheets() );
        excelBuilder.populateData( excelData );

        return excelBuilder;
    }

    public byte[] exportToByteArray() throws IOException {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        workbook.write( byteArrayOutputStream );
        return byteArrayOutputStream.toByteArray();
    }

    private void populateData(ExcelData excelData) {

        excelData.getSheets().forEach(
                sheetName -> {

                    populateSheetData( sheetName, excelData.getSheetData( sheetName ) );
                }
        );
    }

    private void populateSheetData( String sheetName, List<SheetCell> cellList ) {

        Sheet sheet = workbook.getSheet( sheetName );

        for( SheetCell sheetCell: cellList ){

            Row row = sheet.getRow( sheetCell.getRow() -1 );
            if( row == null ){

                row = sheet.createRow( sheetCell.getRow() - 1 );
            }

            Cell cell = row.createCell( sheetCell.getCol() -1 );

            setMergedRegion( sheet, sheetCell );

            CellUtil.setCellStyleProperty( cell, CellUtil.VERTICAL_ALIGNMENT, sheetCell.getVerticalAlignment() );
            CellUtil.setCellStyleProperty( cell, CellUtil.ALIGNMENT, sheetCell.getHorizontalAlignment() );

            cell.setCellValue( sheetCell.getValue() );
        }
    }

    private void setMergedRegion( Sheet sheet, SheetCell sheetCell ) {

        if( sheetCell.getColSpan() > 1 || sheetCell.getRowSpan() > 1 ) {

            int firstRow = sheetCell.getRow() - 1;
            int lastRow = firstRow + sheetCell.getRowSpan() - 1;

            int firstCol = sheetCell.getCol() - 1;
            int lastCol = firstCol + sheetCell.getColSpan() - 1;

            CellRangeAddress mergedRegion = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
            sheet.addMergedRegion(mergedRegion);

        }
    }

    private void createSheets( List<String> sheets ) {

        sheets.forEach( sheetName -> workbook.createSheet( sheetName ) );
    }
}
