package com.dgt.paribahanpool.excel.model;

import java.util.List;

public interface ExcelData {

    List<String> getSheets();

    List<SheetCell> getSheetData(String sheet );
}
