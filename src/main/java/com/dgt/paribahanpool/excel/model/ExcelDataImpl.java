package com.dgt.paribahanpool.excel.model;

import lombok.Data;

import java.util.*;

@Data
public class ExcelDataImpl implements ExcelData {

    Map<String,List<SheetCell>> sheetData = new HashMap<>();

    public ExcelDataImpl( Map<String,List<SheetCell>> sheetListData ){

        sheetData = sheetListData;

//        SheetCell sheetCell11 = new SheetCell( "Month", 1,1,2,1 );
//        SheetCell sheetCell12 = new SheetCell( "Savings", 1, 2, 1, 2 );
//
//        SheetCell sheetCell21 = new SheetCell( "January", 2, 2, 1, 1 );
//        SheetCell sheetCell22 = new SheetCell( "January", 2, 3, 1, 1 );
//
//        List<SheetCell> data = Arrays.asList( sheetCell11, sheetCell12, sheetCell21, sheetCell22 );
//
//        sheetData.put( "Test", data );
    }

    @Override
    public List<String> getSheets() {

        return new ArrayList<>( sheetData.keySet() );
    }

    @Override
    public List<SheetCell> getSheetData(String sheet) {

        return sheetData.get( sheet );
    }

}
