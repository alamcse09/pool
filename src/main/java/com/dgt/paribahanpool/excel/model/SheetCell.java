package com.dgt.paribahanpool.excel.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;

@Data
@RequiredArgsConstructor
public final class SheetCell{

    @Setter( AccessLevel.NONE )
    private final String value;

    @Setter( AccessLevel.NONE )
    private final Integer row;

    @Setter( AccessLevel.NONE )
    private final Integer col;

    @Setter( AccessLevel.NONE )
    private final Integer rowSpan;

    @Setter( AccessLevel.NONE )
    private final Integer colSpan;

    private HorizontalAlignment horizontalAlignment = HorizontalAlignment.CENTER;

    private VerticalAlignment verticalAlignment = VerticalAlignment.CENTER;

    public static SheetCell valueOf( Object obj ){

        return new SheetCell( obj.toString(), 1, 1, 1, 1 );
    }
}
