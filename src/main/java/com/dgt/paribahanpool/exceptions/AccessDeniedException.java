package com.dgt.paribahanpool.exceptions;

public class AccessDeniedException extends Exception{

    AccessDeniedException( String msg ){

        super( msg );
    }
}
