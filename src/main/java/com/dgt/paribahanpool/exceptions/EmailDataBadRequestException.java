package com.dgt.paribahanpool.exceptions;

public class EmailDataBadRequestException extends Exception{

    public EmailDataBadRequestException(String msg){

        super( msg );
    }
}
