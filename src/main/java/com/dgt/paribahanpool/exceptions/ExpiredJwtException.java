package com.dgt.paribahanpool.exceptions;

public class ExpiredJwtException extends Exception {

    ExpiredJwtException( String msg ){

        super( msg );
    }
}
