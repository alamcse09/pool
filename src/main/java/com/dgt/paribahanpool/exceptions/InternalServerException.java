package com.dgt.paribahanpool.exceptions;

public class InternalServerException extends Exception{

    InternalServerException( String msg ){

        super( msg );
    }
}
