package com.dgt.paribahanpool.exceptions;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String msg){

        super( msg );
    }
}
