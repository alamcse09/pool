package com.dgt.paribahanpool.exceptions;

public class RequiredParamMissingException extends Exception{

    public RequiredParamMissingException( String msg ){

        super( msg );
    }
}
