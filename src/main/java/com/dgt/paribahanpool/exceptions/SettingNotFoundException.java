package com.dgt.paribahanpool.exceptions;

public class SettingNotFoundException extends RuntimeException {

    public SettingNotFoundException(String msg ){

        super( msg );
    }
}
