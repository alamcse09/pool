package com.dgt.paribahanpool.exceptions;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserDisabledException extends UsernameNotFoundException {
    public UserDisabledException(String s) {

        super(s);
    }
}
