package com.dgt.paribahanpool.geolocation.controller;

import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.geolocation.model.GeoLocationResponse;
import com.dgt.paribahanpool.geolocation.service.GeoLocationService;
import com.dgt.paribahanpool.geolocation.service.GeoLocationValidationService;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping( "/api/geolocation" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class GeoLocationRestController {

    private final GeoLocationService geoLocationService;
    private final GeoLocationValidationService geoLocationValidationService;

    @GetMapping(
            value = "/find-by-parent/{id}"
    )
    public RestResponse findByParentId(
            @PathVariable( "id" ) Long id
    ) throws NotFoundException {

        RestValidationResult restValidationResult = geoLocationValidationService.findByParentId( id );

        if( restValidationResult.getSuccess() ) {

            List<GeoLocationResponse> geoLocationResponseList = geoLocationService.findByParentId( id );
            return RestResponse.builder().success( true ).payload(geoLocationResponseList).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }
}
