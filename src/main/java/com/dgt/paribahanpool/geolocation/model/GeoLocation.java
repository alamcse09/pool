package com.dgt.paribahanpool.geolocation.model;

import com.dgt.paribahanpool.enums.GeolocationType;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@Entity
@Table( name = "geolocation" )
@SQLDelete( sql = "UPDATE geolocation SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class GeoLocation extends AuditableEntity {
    
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "geolocation_type" )
    private GeolocationType geoLocationType;

    @Column( name = "name_en" )
    private String nameEn;

    @Column( name = "name_bn" )
    private String nameBn;

    @Column( name = "path" )
    private String path;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "parent_id", foreignKey = @ForeignKey( name = "fk_geolocation_parent_id" ) )
    private GeoLocation parentGeoLocation;
}
