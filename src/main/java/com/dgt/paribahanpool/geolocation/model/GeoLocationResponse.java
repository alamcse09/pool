package com.dgt.paribahanpool.geolocation.model;

import com.dgt.paribahanpool.util.NameResponse;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GeoLocationResponse {

    private Long id;

    private NameResponse geoLocationName;

    private String nameEn;
    private String nameBn;
}
