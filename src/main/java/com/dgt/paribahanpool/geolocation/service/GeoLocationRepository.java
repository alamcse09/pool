package com.dgt.paribahanpool.geolocation.service;

import com.dgt.paribahanpool.enums.GeolocationType;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GeoLocationRepository extends DataTablesRepository<GeoLocation, Long>, JpaRepository<GeoLocation,Long> {

    List<GeoLocation> findByParentGeoLocation( GeoLocation parentGeoLocation );
    List<GeoLocation> findByGeoLocationType( GeolocationType geoLocationType );
}
