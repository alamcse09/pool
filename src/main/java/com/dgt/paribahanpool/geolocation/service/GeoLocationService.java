package com.dgt.paribahanpool.geolocation.service;

import com.dgt.paribahanpool.enums.GeolocationType;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.geolocation.model.GeoLocationResponse;
import com.dgt.paribahanpool.util.NameResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class GeoLocationService {

    private final GeoLocationRepository geoLocationRepository;

    public Optional<GeoLocation> findById( Long id ){

        return geoLocationRepository.findById( id );
    }

    public GeoLocation findReferenceById( Long id ){

        return geoLocationRepository.getById( id );
    }

    @Transactional
    @Cacheable( cacheNames = "geo_response_by_parent_id" )
    public List<GeoLocationResponse> findByParentId( Long id ){

        GeoLocation geoLocation = findReferenceById( id );

        List<GeoLocation> geoLocationList = geoLocationRepository.findByParentGeoLocation( geoLocation );

        return getGeoLocationResponseListFromGeoLocationList( geoLocationList );
    }

    private List<GeoLocationResponse> getGeoLocationResponseListFromGeoLocationList( List<GeoLocation> geoLocationList ) {

        return geoLocationList
                .stream()
                .map( this:: getGeoLocationResponseFromGeoLocation )
                .collect( Collectors.toList() );
    }

    private GeoLocationResponse getGeoLocationResponseFromGeoLocation( GeoLocation geoLocation ) {

        return GeoLocationResponse.builder()
                .id( geoLocation.getId() )
                .geoLocationName(
                        NameResponse.builder()
                        .nameBn( geoLocation.getNameBn() )
                        .nameEn( geoLocation.getNameEn() )
                        .build()
                )
                .nameEn( geoLocation.getNameEn() )
                .nameBn( geoLocation.getNameBn() )
                .build();
    }

    @Cacheable( cacheNames = "geo_response_by_geo_type" )
    public List<GeoLocationResponse> findByGeoLocationType( GeolocationType geoLocationType ){

        List<GeoLocation> geoLocationList = geoLocationRepository.findByGeoLocationType( geoLocationType );

        return getGeoLocationResponseListFromGeoLocationList( geoLocationList );
    }


}
