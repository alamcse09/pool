package com.dgt.paribahanpool.geolocation.service;

import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.geolocation.model.GeoLocationResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class GeoLocationValidationService {

    private final GeoLocationService geoLocationService;

    public RestValidationResult findByParentId( Long id ) throws NotFoundException {

        return RestValidationResult.valid();
    }

}
