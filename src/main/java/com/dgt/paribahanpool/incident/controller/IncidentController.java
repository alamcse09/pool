package com.dgt.paribahanpool.incident.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.incident.model.Incident;
import com.dgt.paribahanpool.incident.model.IncidentAddRequest;
import com.dgt.paribahanpool.incident.model.IncidentAdditionalData;
import com.dgt.paribahanpool.incident.service.IncidentModelService;
import com.dgt.paribahanpool.incident.service.IncidentValidationService;
import com.dgt.paribahanpool.log.model.LogEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/incident" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class IncidentController extends MVCController {

    private final IncidentModelService incidentModelService;
    private final IncidentValidationService incidentValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.incident.add", content = "incident/incident-add", activeMenu = Menu.INCIDENT_ADD)
    @AddFrontEndLibrary( libraries = {FrontEndLibrary.INCIDENT_ADD_FORM } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add incident form" );
        incidentModelService.addIncidentGet( model );
        return viewRoot;
    }

    @PostMapping( "/add" )
    public String addIncident(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid IncidentAddRequest incidentAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
            ){

        if ( !incidentValidationService.handleBindingResultForAddFormPost( model, bindingResult, incidentAddRequest ) ) {

            return viewRoot;
        }

        if ( incidentValidationService.addIncidentPost( redirectAttributes, incidentAddRequest ) ) {

            Incident incident = incidentModelService.addIncidentPost( incidentAddRequest, redirectAttributes );
            log( LogEvent.INCIDENT_ADDED, incident.getId(), "Incident Added", request );
        }

        return "redirect:/incident/search";
    }

    @GetMapping( "/edit/{incidentId}" )
    @TitleAndContent(title = "title.incident.edit", content = "incident/incident-add", activeMenu = Menu.INCIDENT_ADD)
    @AddFrontEndLibrary( libraries = {FrontEndLibrary.INCIDENT_ADD_FORM } )
    public String edit(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable( "incidentId" ) Long incidentId
    ){

        log.debug( "Rendering Edit Incident Form" );

        if( incidentValidationService.editIncident( redirectAttributes, incidentId ) ){

            incidentModelService.editIncident( model, incidentId );
            return viewRoot;
        }

        return "redirect:/incident/search";
    }

    @PostMapping( "/edit/{incidentId}" )
    public String editIncident(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid IncidentAddRequest incidentAddRequest,
            BindingResult bindingResult,
            @PathVariable( "incidentId" ) Long incidentId,
            HttpServletRequest request
    ){

        if ( !incidentValidationService.handleBindingResultForEditFormPost( model, bindingResult, incidentAddRequest ) ) {

            return viewRoot;
        }

        if ( incidentValidationService.addIncidentPost( redirectAttributes, incidentAddRequest ) ) {

            Incident incident = incidentModelService.addIncidentPost( incidentAddRequest, redirectAttributes );
            log( LogEvent.INCIDENT_EDITED, incident.getId(), "Incident Edited", request );
        }

        return "redirect:/incident/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.incident.search", content = "incident/incident-search", activeMenu = Menu.INCIDENT_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.INCIDENT_SEARCH } )
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Incident Search page called" );
        return viewRoot;
    }

    @GetMapping( "/{id}" )
    @TitleAndContent( title = "title.incident.view", content = "incident/incident-view", activeMenu = Menu.INCIDENT_VIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.JQUERY_PRINT, FrontEndLibrary.INCIDENT_VIEW } )
    public String view(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ) {

        if( incidentValidationService.view( redirectAttributes, id ) ){

            incidentModelService.view( model, id, getLoggedInUser() );
            return viewRoot;
        }

        return "redirect:/incident/search";
    }

    @PostMapping( "/take-action" )
    public String takeAction(
            @RequestParam( "id" ) Long id,
            @RequestParam( "action" ) Long actionId,
            @Valid IncidentAdditionalData incidentAdditionalData,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ) throws Exception {

        if( incidentValidationService.takeActionValidation( redirectAttributes, id, actionId, getLoggedInUser(), incidentAdditionalData ) ){

            incidentModelService.takeAction( redirectAttributes, id, actionId, incidentAdditionalData, getLoggedInUser() );
            log( LogEvent.INCIDENT_APPLICATION_TAKE_ACTION, actionId, "Incident Take action", request );
        }
        else{

            return "redirect:/incident/" + id;
        }

        return "redirect:/incident/search";
    }

}
