package com.dgt.paribahanpool.incident.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.incident.model.IncidentSearchResponse;
import com.dgt.paribahanpool.incident.service.IncidentService;
import com.dgt.paribahanpool.incident.service.IncidentValidationService;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.mechanic.service.MechanicService;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/incident" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class IncidentRestController extends BaseRestController {

    private final IncidentValidationService incidentValidationService;
    private final IncidentService incidentService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<IncidentSearchResponse> searchIncident(
            DataTablesInput dataTablesInput,
            HttpServletRequest request
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return incidentService.searchForDatatable( dataTablesInput, request );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = incidentValidationService.delete( id );
        if( restValidationResult.getSuccess() ){

            incidentService.delete( id );
            log( LogEvent.INCIDENT_DELETED, id, "", request );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }
}
