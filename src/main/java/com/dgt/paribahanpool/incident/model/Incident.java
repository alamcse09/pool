package com.dgt.paribahanpool.incident.model;

import com.dgt.paribahanpool.committee.model.Committee;
import com.dgt.paribahanpool.enums.IncidentType;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.WorkflowEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table( name = "incident" )
@SQLDelete( sql = "UPDATE incident SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class Incident extends AuditableEntity implements WorkflowEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "incident_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate incidentDate;

    @Column( name = "reg_no" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String regNo;

    @Column( name = "chassis_no" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String chassisNo;

    @Column( name = "incident_time" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.timeFormat_hour_min_ampm )
    private LocalTime incidentTime;

    @Column( name = "incident_location" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String incidentLocation;

    @Column( name = "incident_type" )
    private IncidentType incidentType;

    @Column( name = "incident_details", columnDefinition = "TEXT")
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String incidentDetails;

    @Column( name = "user_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String userName;

    @Column( name = "user_name_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String userNameEn;

    @Column( name = "driver_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String driverName;

    @Column( name = "driver_name_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String driverNameEn;

    @Column( name = "fine" )
    private Double fine;

    @Column( name = "collected_fine" )
    private Double collectedFine;

    @Column( name = "to_be_fixed_for_da", columnDefinition = "TEXT" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String toBeFixedForDa;

    @Column( name = "standing_committee_members", columnDefinition = "TEXT" )
    private String standingCommitteeMembers;

    @Column( name = "investigation_report_group_id" )
    private Long investigationReportGroupId;

    @Column( name = "chalan_document_group_id" )
    private Long chalanGroupId;

    @Column( name = "gd_document_group_id" )
    private Long gdDocGroupId;

    @Column( name = "inquiry_document_group_id" )
    private Long inquiryDocGroupId;

    @Column( name = "verdict_document_group_id" )
    private Long verdictDocGroupId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "user_id", foreignKey = @ForeignKey( name = "fk_incident_user_id" ) )
    private User actionTakingUser;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "state_id", foreignKey = @ForeignKey( name = "fk_incident_state_id" ) )
    private State state;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinTable(
            name = "incident_committee_member_map",
            joinColumns = @JoinColumn( name = "incident_id", foreignKey = @ForeignKey( name = "fk_committee_member_incident_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "committee_member_id", foreignKey = @ForeignKey( name = "fk_incident_id_committee_member_map_member_record_id" ) )
    )
    private Set<Committee> committeeMemberSet = new HashSet<>();

    @ManyToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "vehicle_id", foreignKey = @ForeignKey( name = "incident_vehicle_id" ) )
    private Vehicle vehicle;
}
