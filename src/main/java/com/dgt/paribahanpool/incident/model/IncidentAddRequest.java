package com.dgt.paribahanpool.incident.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.IncidentType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;
import java.util.List;

@Data
public class IncidentAddRequest {

    public Long id;
    private Long vehicleId;

    @NotBlank( message = "{validation.common.required}" )
//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    private String regNo;

//    @NotBlank( message = "{validation.common.required}" )
//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    private String chassisNo;

    @PastOrPresent( message = "{validation.common.date.past_or_present}" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    public LocalDate incidentDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.timeFormat_hour_min_ampm )
    public LocalTime incidentTime;

    @NotBlank( message = "{validation.common.required}" )
    public String incidentLocation;

    private IncidentType incidentType;

    public String incidentDetails;

//    @NotBlank( message = "{validation.common.required}" )
    public String userName;

//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphabet}" )
//    @NotBlank( message = "{validation.common.required}" )
    public String userNameEn;

//    @NotBlank( message = "{validation.common.required}" )
    public String driverName;

//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphabet}" )
//    @NotBlank( message = "{validation.common.required}" )
    public String driverNameEn;

    private MultipartFile[] gdFiles;

    List<DocumentMetadata> gdDocuments = Collections.EMPTY_LIST;
}
