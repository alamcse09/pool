package com.dgt.paribahanpool.incident.model;

import com.dgt.paribahanpool.committee.model.CommitteeAddRequest;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.workflow.model.WorkflowAdditionalData;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
public class IncidentAdditionalData implements WorkflowAdditionalData {

    private List<CommitteeAddRequest> committeeAddRequestList;

    private transient MultipartFile[] committeeInvestigationReportFiles;
    private List<DocumentMetadata> committeeInvestigationReportDocuments = Collections.EMPTY_LIST;

    private transient MultipartFile[] chalanReportFiles;
    private List<DocumentMetadata> chalanReportDocuments = Collections.EMPTY_LIST;

    private transient MultipartFile[] verdictReportFiles;
    private List<DocumentMetadata> verdictReportDocuments = Collections.EMPTY_LIST;

    private Long actionTakerMemberId;

    private String toBeFixedForDa;

    private Double collectedFine;
    private Double finedAmount;

    private Boolean isToBeFixed;
    private String incidentDetails;
    private String incidentLocation;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate incidentDate;

}
