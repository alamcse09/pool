package com.dgt.paribahanpool.incident.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.IncidentType;
import com.dgt.paribahanpool.workflow.model.StateResponse;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Data
public class IncidentSearchResponse {

    private Long id;
    private LocalDate incidentDate;
    private LocalTime incidentTime;
    private String incidentLocation;
    private IncidentType incidentType;
    private String incidentTypeStr;
    private String userName;
    private String driverName;
    private String fine;
    private StateResponse stateResponse;
    List<DocumentMetadata> investigationReportDocuments;
    List<DocumentMetadata> documents;
    List<DocumentMetadata> gdDocuments;
    List<DocumentMetadata> inquiryDocuments;
    List<DocumentMetadata> verdictDocuments;
}
