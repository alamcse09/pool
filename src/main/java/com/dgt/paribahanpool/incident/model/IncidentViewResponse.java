package com.dgt.paribahanpool.incident.model;

import com.dgt.paribahanpool.committee.model.CommitteeResponse;
import com.dgt.paribahanpool.enums.IncidentType;
import com.dgt.paribahanpool.user.model.UserResponse;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Data
public class IncidentViewResponse {

    private Long id;
    private Long stateId;
    private LocalDate incidentDate;
    public LocalTime incidentTime;
    public String incidentLocation;
    private IncidentType incidentType;
    public String incidentDetails;
    public String userName;
    public String driverName;
    private String registrationNo;
    private String chassisNo;
    private String toBeFixedForDa;
    private List<CommitteeResponse> committeeResponseList;

    private List<UserResponse> showEmployeeList;
    private Long actionTakerMemberId;
}
