package com.dgt.paribahanpool.incident.service;

import com.dgt.paribahanpool.incident.model.Incident;
import com.dgt.paribahanpool.incident.model.IncidentAddRequest;
import com.dgt.paribahanpool.incident.model.IncidentAdditionalData;
import com.dgt.paribahanpool.incident.model.IncidentViewResponse;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.VehicleViewResponse;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import com.dgt.paribahanpool.workflow.model.StateActionMapResponse;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class IncidentModelService {

    private final IncidentService incidentService;
    private final MvcUtil mvcUtil;
    private final StateActionMapService stateActionMapService;
    private final VehicleService vehicleService;

    public void addIncidentGet( Model model ){

        List<VehicleViewResponse> vehicleViewResponseList = vehicleService.getVehicleViewResponseList();

        model.addAttribute( "vehicleViewResponseList", vehicleViewResponseList );

        model.addAttribute( "incidentAddRequest", new IncidentAddRequest() );
    }

    public Incident addIncidentPost(IncidentAddRequest incidentAddRequest, RedirectAttributes redirectAttributes){

        Incident incident = incidentService.save( incidentAddRequest );

        if( incidentAddRequest.getId() != null ){

            mvcUtil.addSuccessMessage( redirectAttributes, "success.incident.edit" );
        }
        else{

            mvcUtil.addSuccessMessage( redirectAttributes, "success.incident.add" );
        }

        return incident;
    }

    public void editIncident( Model model, Long incidentId ){


        List<VehicleViewResponse> vehicleViewResponseList = vehicleService.getVehicleViewResponseList();

        model.addAttribute( "vehicleViewResponseList", vehicleViewResponseList );

        Optional<Incident> incident = incidentService.findById( incidentId );

        if( incident.isPresent() ){

            IncidentAddRequest incidentAddRequest = incidentService.getIncidentAddRequestFromIncident( incident.get() );

            model.addAttribute( "incidentAddRequest", incidentAddRequest );
        }
    }

    @Transactional
    public void view( Model model, Long id, UserPrincipal loggedInUser ) {

        Optional<Incident> incidentOptional = incidentService.findById( id );

        if( incidentOptional.isPresent() ){

            IncidentViewResponse incidentViewResponse = incidentService.getIncidentViewResponseFromIncident( incidentOptional.get() );
            model.addAttribute( "incidentViewResponse", incidentViewResponse );

            if( incidentViewResponse.getStateId() != null ){

                List<StateActionMapResponse> stateActionMapResponseList = stateActionMapService.findStateActionMapResponseListByCurrentStateAndRoleIdSet( incidentViewResponse.getStateId(), loggedInUser.getRoleIds() );
                model.addAttribute( "stateActionList", stateActionMapResponseList );
            }
        }
    }

    public void takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, IncidentAdditionalData incidentAdditionalData, UserPrincipal loggedInUser ) throws Exception {


        incidentService.takeAction( id, actionId, incidentAdditionalData, loggedInUser );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.common.action.success" );
    }

}
