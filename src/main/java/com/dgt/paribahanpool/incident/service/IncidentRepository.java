package com.dgt.paribahanpool.incident.service;

import com.dgt.paribahanpool.incident.model.Incident;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface IncidentRepository extends DataTablesRepository<Incident, Long> {
}
