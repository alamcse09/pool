package com.dgt.paribahanpool.incident.service;

import com.dgt.paribahanpool.committee.model.Committee;
import com.dgt.paribahanpool.committee.model.CommitteeResponse;
import com.dgt.paribahanpool.committee.model.CommitteeType;
import com.dgt.paribahanpool.committee.service.CommitteeService;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.document.model.Document;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.enums.IncidentType;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.incident.model.*;
import com.dgt.paribahanpool.locale.LocalizeUtil;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.Util;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import com.dgt.paribahanpool.vehicle.service.VehicleServiceApplicationService;
import com.dgt.paribahanpool.workflow.model.*;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class IncidentService implements WorkflowService<Incident> {

    private final IncidentRepository incidentRepository;
    private final DocumentService documentService;
    private final StateService stateService;
    private final StateActionMapService stateActionMapService;
    private final CommitteeService committeeService;
    private final UserService userService;
    private final VehicleService vehicleService;
    private final VehicleServiceApplicationService vehicleServiceApplicationService;
    private final LocalizeUtil localizeUtil;
    private final Gson gson;

    public Optional<Incident> findById(Long id){

        return incidentRepository.findById( id );
    }

    @Transactional()
    public Incident save( IncidentAddRequest incidentAddRequest ){

        Incident incident = new Incident();

        if( incidentAddRequest.getId() != null ){

            incident = findById( incidentAddRequest.getId() ).get();
        }

        incident = getIncidentFromIncidentAddRequest( incident, incidentAddRequest );

        return save( incident );
    }

    public Incident save( Incident incident ){

        return incidentRepository.save( incident );
    }

    public void save( Collection committeeMemberList ){

        incidentRepository.saveAll( committeeMemberList );
    }

    public Incident getIncidentFromIncidentAddRequest( Incident incident, IncidentAddRequest incidentAddRequest ){

        incident.setIncidentDate( incidentAddRequest.getIncidentDate() );
        incident.setIncidentTime( incidentAddRequest.getIncidentTime() );
        incident.setIncidentLocation( incidentAddRequest.getIncidentLocation() );
        incident.setIncidentType( incidentAddRequest.getIncidentType() );
        incident.setIncidentDetails( incidentAddRequest.getIncidentDetails() );
        incident.setUserName( incidentAddRequest.getUserName() );
        incident.setUserNameEn( incidentAddRequest.getUserNameEn() );
        incident.setDriverName( incidentAddRequest.getDriverName() );
        incident.setDriverNameEn( incidentAddRequest.getDriverNameEn() );
        incident.setRegNo( incidentAddRequest.getRegNo() );
        incident.setChassisNo( incidentAddRequest.getChassisNo() );

        if( incident.getGdDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( incidentAddRequest.getGdFiles(), Collections.EMPTY_MAP );
            incident.setGdDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( incidentAddRequest.getGdFiles(), incident.getGdDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        if( incident.getState() == null ){

            Long startStateId = AppConstants.INCIDENT_REPORT_INSTANTIATED;

            State state = stateService.findStateReferenceById( startStateId );
            incident.setState( state );
        }

        if( incidentAddRequest.getVehicleId() != null ){

            Optional<Vehicle> vehicleOptional = vehicleService.findById( incidentAddRequest.getVehicleId() );
            if( vehicleOptional.isPresent() ){

                Vehicle vehicle = vehicleOptional.get();
                incident.setVehicle( vehicle );
            }
        }

        return incident;
    }

    public IncidentAddRequest getIncidentAddRequestFromIncident( Incident incident){

        IncidentAddRequest incidentAddRequest = new IncidentAddRequest();

        incidentAddRequest.setId( incident.getId() );
        incidentAddRequest.setIncidentDate( incident.getIncidentDate() );
        incidentAddRequest.setIncidentTime( incident.getIncidentTime() );
        incidentAddRequest.setIncidentLocation( incident.getIncidentLocation() );
        incidentAddRequest.setIncidentType( incident.getIncidentType() );
        incidentAddRequest.setIncidentDetails( incident.getIncidentDetails() );
        incidentAddRequest.setUserName( incident.getUserName() );
        incidentAddRequest.setDriverName( incident.getDriverName() );

        if( incident.getGdDocGroupId() != null && incident.getGdDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( incident.getGdDocGroupId() );
            incidentAddRequest.setGdDocuments( documentMetadataList );
        }

        return incidentAddRequest;
    }

    public DataTablesOutput<IncidentSearchResponse> searchForDatatable(DataTablesInput dataTablesInput, HttpServletRequest request) {

        return incidentRepository.findAll( dataTablesInput, ( incident ) -> getIncidentSearchResponseFromIncident( incident, request ) );
    }

    private IncidentSearchResponse getIncidentSearchResponseFromIncident( Incident incident ) {

        return getIncidentSearchResponseFromIncident( incident, null );
    }

    private IncidentSearchResponse getIncidentSearchResponseFromIncident( Incident incident, HttpServletRequest request ) {

        IncidentSearchResponse incidentSearchResponse = new IncidentSearchResponse();

        incidentSearchResponse.setId( incident.getId() );
        incidentSearchResponse.setIncidentDate( incident.getIncidentDate() );
        incidentSearchResponse.setIncidentTime( incident.getIncidentTime() );
        incidentSearchResponse.setIncidentLocation( incident.getIncidentLocation() );
        incidentSearchResponse.setIncidentType( incident.getIncidentType() );

        if( request != null && incident.getIncidentType() != null )
            incidentSearchResponse.setIncidentTypeStr( localizeUtil.getMessageFromMessageSource( IncidentType.getLabel( incident.getIncidentType() ), request ) );

        incidentSearchResponse.setUserName( incident.getUserName() );
        incidentSearchResponse.setDriverName( incident.getDriverName() );
        incidentSearchResponse.setFine( (incident.getFine() == null) ? "N/A" : Util.makeDigitBangla(incident.getFine()) );

        List<DocumentMetadata> investigationDocumentdataList = documentService.findDocumentMetaDataListByGroupId( incident.getInvestigationReportGroupId() );
        incidentSearchResponse.setInvestigationReportDocuments( investigationDocumentdataList );

        List<DocumentMetadata> documentdataList = documentService.findDocumentMetaDataListByGroupId( incident.getChalanGroupId() );
        incidentSearchResponse.setDocuments( documentdataList );

        List<DocumentMetadata> gdDocumentdataList = documentService.findDocumentMetaDataListByGroupId( incident.getGdDocGroupId() );
        incidentSearchResponse.setGdDocuments( gdDocumentdataList );

        List<DocumentMetadata> inquiryDocumentdataList = documentService.findDocumentMetaDataListByGroupId( incident.getInquiryDocGroupId() );
        incidentSearchResponse.setInquiryDocuments( inquiryDocumentdataList );

        List<DocumentMetadata> verdictDocumentdataList = documentService.findDocumentMetaDataListByGroupId( incident.getVerdictDocGroupId() );
        incidentSearchResponse.setVerdictDocuments( verdictDocumentdataList );

        State state = incident.getState();
        if( state != null ){

            StateResponse stateResponse = StateResponse.builder()
                    .id( state.getId() )
                    .name( state.getName() )
                    .nameEn( state.getNameEn() )
                    .stateType( state.getStateType() )
                    .build();

            incidentSearchResponse.setStateResponse( stateResponse );
        }

        return incidentSearchResponse;
    }

    public Boolean existById( Long id ) {

        return incidentRepository.existsById( id );
    }

    public void delete( Long id ) {

        incidentRepository.deleteById( id );
    }

    public IncidentViewResponse getIncidentViewResponseFromIncident( Incident incident ) {

        IncidentViewResponse incidentViewResponse = new IncidentViewResponse();

        List<UserResponse> userResponseList = userService.getUserResponseList( UserType.SYSTEM_USER );

        incidentViewResponse.setId( incident.getId() );
        incidentViewResponse.setStateId( incident.getState() != null? incident.getState().getId(): null );
        incidentViewResponse.setIncidentDate( incident.getIncidentDate() );
        incidentViewResponse.setIncidentTime( incident.getIncidentTime() );
        incidentViewResponse.setIncidentType( incident.getIncidentType() );
        incidentViewResponse.setIncidentLocation( incident.getIncidentLocation() );
        incidentViewResponse.setIncidentDetails( incident.getIncidentDetails() );
        incidentViewResponse.setUserName( incident.getUserName() );
        incidentViewResponse.setDriverName( incident.getDriverName() );
        incidentViewResponse.setRegistrationNo( incident.getRegNo() );
        incidentViewResponse.setChassisNo( incident.getChassisNo() );
        incidentViewResponse.setToBeFixedForDa( (incident.getToBeFixedForDa() == null) ? "N/A" : incident.getToBeFixedForDa() );

        if( incident.getStandingCommitteeMembers() != null && incident.getStandingCommitteeMembers().length() > 0 ){

            Type type = new TypeToken<List<CommitteeResponse>>(){}.getType();
            List<CommitteeResponse> committeeResponseList = gson.fromJson( incident.getStandingCommitteeMembers(), type );
            incidentViewResponse.setCommitteeResponseList( committeeResponseList );
        }

        Optional<Vehicle> vehicleOptional = vehicleService.findById( incident.getId() );

        if( vehicleOptional.isPresent() ){

            Vehicle vehicle = vehicleOptional.get();

            incidentViewResponse.setDriverName( (vehicle.getDriver() != null) ? vehicle.getDriver().getName() : "N/A" );
            incidentViewResponse.setUserName( (vehicle.getVehicleOwner() != null) ? vehicle.getVehicleOwner().getUser().getPublicUserInfo().getName() : "N/A" );
        }

        incidentViewResponse.setShowEmployeeList( userResponseList );

        return incidentViewResponse;

    }

    @Transactional
    public void takeAction(Long id, Long actionId, IncidentAdditionalData incidentAdditionalData, UserPrincipal loggedInUser) throws Exception {

        stateActionMapService.takeAction( this, id, actionId, loggedInUser, (workflowEntity ) -> setAdditionalData( workflowEntity, actionId, incidentAdditionalData ) );
    }

    private WorkflowAdditionalData setAdditionalData( WorkflowEntity workflowEntity, Long actionId, WorkflowAdditionalData additionalActionData ) {

        Incident incident = (Incident) workflowEntity;

        IncidentAdditionalData incidentAdditionalData = (IncidentAdditionalData) additionalActionData;

        if( actionId.equals( AppConstants.INCIDENT_FORWARD_TO_UD_ACTION_ID ) ){

            if( incidentAdditionalData.getIsToBeFixed() ){

                if( incidentAdditionalData.getIncidentDate() != null ){

                    incident.setIncidentDate( incidentAdditionalData.getIncidentDate() );
                }
                if( incidentAdditionalData.getIncidentDetails() != null ){

                    incident.setIncidentDetails( incidentAdditionalData.getIncidentDetails() );
                }
                if( incidentAdditionalData.getIncidentLocation() != null ){

                    incident.setIncidentLocation( incidentAdditionalData.getIncidentLocation() );
                }
            }
        }

        if( actionId.equals( AppConstants.INCIDENT_FORWARD_TO_DA_TO_UPLOAD_INVES_REP_ACTION_ID) ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( incidentAdditionalData.getCommitteeInvestigationReportFiles(), Collections.EMPTY_MAP );
            incident.setInvestigationReportGroupId( documentUploadedResponse.getDocGroupId() );
        }

        if( actionId.equals( AppConstants.UPLOAD_CHALAN_REPORT) ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( incidentAdditionalData.getChalanReportFiles(), Collections.EMPTY_MAP );
            incident.setChalanGroupId( documentUploadedResponse.getDocGroupId() );
        }

        if( actionId.equals( AppConstants.INCIDENT_FORWARD_BACK_TO_DA_ACTION_ID ) ){

            incident.setToBeFixedForDa( incidentAdditionalData.getToBeFixedForDa() );
        }

        if( actionId.equals( AppConstants.INCIDENT_COLLECT_FINE_ACTION_ID) ){

            incident.setCollectedFine( incidentAdditionalData.getCollectedFine() );
            vehicleServiceApplicationService.saveFromIncident( incident );
        }

        if( actionId.equals( AppConstants.INCIDENT_DO_FINE_ACTION_ID) ){

            incident.setFine( incidentAdditionalData.getFinedAmount() );
        }

        if( actionId.equals( AppConstants.INCIDENT_SENT_FOR_SERVICE_ACTION_ID) ){

            vehicleServiceApplicationService.saveFromIncident( incident );
        }

        if( actionId.equals( AppConstants.INCIDENT_UPLOAD_VERDICT_REPORT ) ){

            String standingCommitteeMemeber = committeeService.getCommitteeResponseListAsJson( CommitteeType.INCIDENT_STANDING_COMMITTEE );
            incident.setStandingCommitteeMembers( standingCommitteeMemeber );

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( incidentAdditionalData.getVerdictReportFiles(), Collections.EMPTY_MAP );
            incident.setVerdictDocGroupId( documentUploadedResponse.getDocGroupId() );
        }

        if( actionId.equals( AppConstants.CREATE_COMMITTEE ) && incidentAdditionalData.getCommitteeAddRequestList() != null && incidentAdditionalData.getCommitteeAddRequestList().size() > 0 ){

            Set<Committee> committeeSet =
                    incidentAdditionalData.getCommitteeAddRequestList()
                    .stream()
                    .map( committeeAddRequest -> committeeService.getCommitteeFromCommitteeAddRequest( committeeAddRequest ) )
                            .filter( committee -> committee != null )
                            .collect( Collectors.toSet() );

            incident.getCommitteeMemberSet().clear();
            incident.getCommitteeMemberSet().addAll( committeeSet );

            User user = userService.findById( incidentAdditionalData.getActionTakerMemberId() ).get();

            incident.setActionTakingUser( user );
        }


        return additionalActionData;
    }
}
