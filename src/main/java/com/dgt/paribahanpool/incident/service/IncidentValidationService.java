package com.dgt.paribahanpool.incident.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.incident.model.Incident;
import com.dgt.paribahanpool.incident.model.IncidentAddRequest;
import com.dgt.paribahanpool.incident.model.IncidentAdditionalData;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.VehicleViewResponse;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class IncidentValidationService {

    private final MvcUtil mvcUtil;
    private final IncidentService incidentService;
    private final StateActionMapService stateActionMapService;
    private final UserService userService;
    private final VehicleService vehicleService;

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, IncidentAddRequest incidentAddRequest){

        Boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "incidentAddRequest" )
                        .object( incidentAddRequest )
                        .title( "title.incident.add" )
                        .content( "incident/incident-add" )
                        .activeMenu( Menu.INCIDENT_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.INCIDENT_ADD_FORM } )
                        .build()
        );

        if( !isValid ){

            List<VehicleViewResponse> vehicleViewResponseList = vehicleService.getVehicleViewResponseList();

            model.addAttribute( "vehicleViewResponseList", vehicleViewResponseList );
        }

        return isValid;
    }

    public Boolean handleBindingResultForEditFormPost(Model model, BindingResult bindingResult, IncidentAddRequest incidentAddRequest){

        Boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "incidentAddRequest" )
                        .object( incidentAddRequest )
                        .title( "title.incident.edit" )
                        .content( "incident/incident-add" )
                        .activeMenu( Menu.INCIDENT_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.INCIDENT_ADD_FORM } )
                        .build()
        );

        if( !isValid ){

            List<VehicleViewResponse> vehicleViewResponseList = vehicleService.getVehicleViewResponseList();

            model.addAttribute( "vehicleViewResponseList", vehicleViewResponseList );
        }

        return isValid;
    }

    public Boolean addIncidentPost( RedirectAttributes redirectAttributes, IncidentAddRequest incidentAddRequest ){

        if( incidentAddRequest.getId() != null ){

            Optional<Incident> incident = incidentService.findById( incidentAddRequest.getId() );

            if( !incident.isPresent() ){

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
                return false;
            }
        }
        return true;
    }

    public Boolean editIncident( RedirectAttributes redirectAttributes, Long incidentId ){

        Optional<Incident> incident = incidentService.findById( incidentId );

        if( !incident.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    public RestValidationResult delete( Long id ) throws NotFoundException {

        Boolean exist = incidentService.existById( id );
        if( !exist ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean view( RedirectAttributes redirectAttributes, Long id ) {

        Optional<Incident> incidentOptional = incidentService.findById( id );

        if( !incidentOptional.isPresent() ) {

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    @Transactional
    public Boolean takeActionValidation( RedirectAttributes redirectAttributes, Long id, Long actionId, UserPrincipal loggedInUser, IncidentAdditionalData incidentAdditionalData ) {

        Optional<Incident> incidentOptional = incidentService.findById( id );

        if( !incidentOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }
        else{

            Incident incident = incidentOptional.get();
            State state = incident.getState();

            if( !stateActionMapService.actionAllowed( state.getId(), loggedInUser.getRoleIds(), actionId ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.common.action.not-allowed" );
                return false;
            }

            if( actionId.equals( AppConstants.CREATE_COMMITTEE) && ( incidentAdditionalData.getCommitteeAddRequestList() == null || incidentAdditionalData.getCommitteeAddRequestList().size() < 0 || incidentAdditionalData.getActionTakerMemberId() == null ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.incident.add.committee" );
                return false;
            }

            if( actionId.equals( AppConstants.INCIDENT_UPLOAD_VERDICT_REPORT ) && ( incidentAdditionalData.getVerdictReportFiles() == null || incidentAdditionalData.getVerdictReportFiles().length < 0 ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.incident.add.no-report-committee-verdict" );
                return false;
            }

            if( actionId.equals( AppConstants.INCIDENT_FORWARD_TO_DA_TO_UPLOAD_INVES_REP_ACTION_ID) && !Objects.equals(incident.getActionTakingUser().getId(), loggedInUser.getUser().getId())){

                mvcUtil.addErrorMessage( redirectAttributes, "error.common.action.not-allowed" );
                return false;
            }

            if( actionId.equals( AppConstants.CREATE_COMMITTEE) && incidentAdditionalData.getActionTakerMemberId() != null ){

                Optional<User> userOptional = userService.findById( incidentAdditionalData.getActionTakerMemberId() );

                if( !userOptional.isPresent() ){

                    mvcUtil.addErrorMessage( redirectAttributes, "error.incident.add.no-action-taker" );
                    return false;
                }
            }

            if( actionId.equals( AppConstants.UPLOAD_INQUIRY_REPORT) && ( incidentAdditionalData.getCommitteeInvestigationReportFiles() == null || incidentAdditionalData.getCommitteeInvestigationReportFiles().length < 0 ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.incident.add.no-report-committee-inquiry" );
                return false;
            }

            if( actionId.equals( AppConstants.UPLOAD_CHALAN_REPORT) && ( incidentAdditionalData.getChalanReportFiles() == null || incidentAdditionalData.getChalanReportFiles().length < 0 ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.incident.add.no-report-chalan-inquiry" );
                return false;
            }

            if( actionId.equals( AppConstants.INCIDENT_COLLECT_FINE_ACTION_ID ) ){

                if( incidentAdditionalData.getCollectedFine() == null || incidentAdditionalData.getCollectedFine() < 0 ) {
                    mvcUtil.addErrorMessage(redirectAttributes, "error.incident.action.collected-fine.not-given");
                    return false;
                }

                if( !incidentAdditionalData.getCollectedFine().equals( incident.getFine() ) ){

                    mvcUtil.addErrorMessage( redirectAttributes, "error.incident.action.collected-fine.not-match" );
                    return false;
                }
            }

            if( actionId.equals( AppConstants.INCIDENT_DO_FINE_ACTION_ID ) && ( incidentAdditionalData.getFinedAmount() == null || incidentAdditionalData.getFinedAmount() < 0 ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.incident.action.fined-amount.not-given" );
                return false;
            }

            return true;
        }
    }
}
