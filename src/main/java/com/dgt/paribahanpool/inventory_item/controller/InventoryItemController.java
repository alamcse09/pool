package com.dgt.paribahanpool.inventory_item.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.bill.model.BillAddRequest;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.inventory_item.model.InventoryItemAddRequest;
import com.dgt.paribahanpool.inventory_item.model.InventoryReportFetchParam;
import com.dgt.paribahanpool.inventory_item.service.InventoryItemModelService;
import com.dgt.paribahanpool.inventory_item.service.InventoryItemValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/inventory" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class InventoryItemController extends MVCController {

    private final InventoryItemModelService inventoryItemModelService;
    private final InventoryItemValidationService inventoryItemValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.inventory.add", content = "inventory/inventory-add", activeMenu = Menu.INVENTORY_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.INVENTORY_ADD_FORM } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add inventory form" );
        inventoryItemModelService.addInventoryGet( model );
        return viewRoot;
    }

    @PostMapping( value = "/add" )
    public String addInventory(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid InventoryItemAddRequest inventoryItemAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !inventoryItemValidationService.handleBindingResultForAddFormPost( model, bindingResult, inventoryItemAddRequest ) ) {

            return viewRoot;
        }

        inventoryItemModelService.addInventoryPost( inventoryItemAddRequest, model );

        return "redirect:/inventory/search";
    }

    @GetMapping( "/edit/{inventoryItemId}" )
    @TitleAndContent( title = "title.inventory.item.edit", content = "inventory/inventory-add", activeMenu = Menu.INVENTORY_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.INVENTORY_ADD_FORM, FrontEndLibrary.BRAND_MODEL_ADD } )
    public String edit(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable( "inventoryItemId" ) Long inventoryItemId
    ) {

        if( inventoryItemValidationService.editInventory( redirectAttributes, inventoryItemId ) ) {

            inventoryItemModelService.editInventory( model, inventoryItemId );
            return viewRoot;
        }

        return "redirect:/inventory/search";
    }

    @PostMapping("/edit")
    public String edit(
            HttpServletRequest request,
            Model model,
            BindingResult bindingResult,
            RedirectAttributes redirectAttributes,
            @Valid InventoryItemAddRequest inventoryItemAddRequest
    ) {
        if( !inventoryItemValidationService.handleBindingResultForEditFormPost( model, bindingResult, inventoryItemAddRequest ) ) {

            return viewRoot;
        }

        if( inventoryItemValidationService.editInventoryPost( redirectAttributes, inventoryItemAddRequest ) ) {

            inventoryItemModelService.editInventoryPost( inventoryItemAddRequest, model );
        }
        return "redirect:/inventory/search";
    }

    @GetMapping("/search")
    @TitleAndContent( title = "title.inventory.search", content = "inventory/inventory-search", activeMenu = Menu.INVENTORY_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.INVENTORY_SEARCH_FORM })
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ) {
        return viewRoot;
    }

    @GetMapping( "/report-search" )
    @TitleAndContent( title = "title.report.search", content = "inventory/inventory-report-search", activeMenu = Menu.INVENTORY_REPORT )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.INVENTORY_REPORT_FORM } )
    public String searchReport(
            HttpServletRequest request,
            Model model
    ){

        inventoryItemModelService.getReportSearchParams( model );
        return viewRoot;
    }

    @PostMapping("/report-search")
    public String showReport(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid InventoryReportFetchParam inventoryReportFetchParam,
            BindingResult bindingResult,
            HttpServletRequest request
    ) {
        inventoryItemModelService.getInventoryItemsByPackage( model, inventoryReportFetchParam );
        return "inventory/inventory-report";
    }
}
