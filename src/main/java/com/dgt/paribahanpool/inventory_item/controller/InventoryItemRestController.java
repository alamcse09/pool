package com.dgt.paribahanpool.inventory_item.controller;

import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.inventory_item.model.InventoryItemResponse;
import com.dgt.paribahanpool.inventory_item.model.InventoryItemSearchResponse;
import com.dgt.paribahanpool.inventory_item.service.InventoryCategoryService;
import com.dgt.paribahanpool.inventory_item.service.InventoryItemService;
import com.dgt.paribahanpool.inventory_item.service.InventoryItemValidationService;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping( "/api/inventory" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class InventoryItemRestController {

    private final InventoryItemService inventoryItemService;
    private final InventoryItemValidationService inventoryItemValidationService;
    private final InventoryCategoryService inventoryCategoryService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<InventoryItemSearchResponse> searchInventoryItem(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return inventoryItemService.searchForDatatable( dataTablesInput );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id
    ) throws NotFoundException {

        RestValidationResult restValidationResult = inventoryItemValidationService.delete( id );

        if( restValidationResult.getSuccess() ) {

            inventoryItemService.delete( id );

            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        } else {

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @GetMapping(
            value = "/find-item/{modelId}/{inventoryCategoryId}"
    )
    public RestResponse findItem(
            @PathVariable( "modelId" ) Long modelId,
            @PathVariable( "inventoryCategoryId" ) Long inventoryCategoryId
    ) throws NotFoundException {

        RestValidationResult restValidationResult = inventoryCategoryService.isExist( inventoryCategoryId );

        if( restValidationResult.getSuccess() ) {

            List<InventoryItemResponse> inventoryItemResponseList = inventoryItemService.findBySpecificVehicleAndModelId(inventoryCategoryId, modelId);

            return RestResponse.builder().success(true).payload(inventoryItemResponseList).build();

        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

}
