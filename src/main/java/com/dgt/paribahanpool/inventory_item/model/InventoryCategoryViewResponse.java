package com.dgt.paribahanpool.inventory_item.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InventoryCategoryViewResponse {

    private Long id;
    private String nameEn;
    private String nameBn;
}
