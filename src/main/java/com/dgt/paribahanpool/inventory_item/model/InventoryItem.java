package com.dgt.paribahanpool.inventory_item.model;

import com.dgt.paribahanpool.enums.InventoryType;
import com.dgt.paribahanpool.enums.ProductType;
import com.dgt.paribahanpool.enums.Unit;
import com.dgt.paribahanpool.enums.WarrantyType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.VersionableAuditableEntity;
import com.dgt.paribahanpool.vehicle.model.Brand;
import com.dgt.paribahanpool.vehicle.model.Model;
import com.dgt.paribahanpool.vendor.model.Vendor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Entity
@Table( name = "inventory_item")
public class InventoryItem extends VersionableAuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "lot_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String lotNumber;

    @Column( name = "lot_type" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String lotType;

    @Column( name = "package_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String packageNumber;

    @Column( name = "product_name_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String productNameEn;

    @Column( name = "product_name_bn" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String productNameBn;

    @Column( name = "product_type" )
    private ProductType productType;

    @Column( name = "country" )
    private Long country;

    @Column( name = "fiscal_year" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String fiscalYear;

    @Column( name = "product_id" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String productId;

    @Column( name = "inventory_type" )
    private InventoryType inventoryType;

    @Column( name = "quantity" )
    private Double quantity;

    @Column( name = "unit" )
    private Unit unit;

    @Column( name = "unit_price" )
    private Double unitPrice;

    @Column( name = "purchase_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate purchaseDate;

    @Column( name = "source_name_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String sourceNameEn;

    @Column( name = "source_name_bn" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String sourceNameBn;

    @Column( name = "seller_name_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String sellerNameEn;

    @Column( name = "seller_name_bn" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String sellerNameBn;

    @Column( name = "brand_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String brandName;

    @Column( name = "model" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String model;

    @Column( name = "production_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate productionDate;

    @Column( name = "expire_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate expireDate;

    @Column( name = "location" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String location;

    @Column( name = "is_for_specific_vehicle" )
    private Boolean isForSpecificVehicle;

    @Column( name = "warranty_type" )
    private WarrantyType warrantyType;

    @NotNull
    @Min( 0 )
    @Column( name = "warranty_period" )
    private Integer warrantyPeriod;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
    @JoinColumn( name = "brand_id", foreignKey = @ForeignKey( name = "fk_inventory_item_brand_id" ) )
    private Brand brand;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH} )
    @JoinColumn( name = "vehicle_model_id", foreignKey = @ForeignKey( name = "fk_inventory_item_vehicle_model_id" ) )
    private Model vehicleModel;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "inventory_category_id", foreignKey = @ForeignKey( name = "fk_inventory_item_inventory_category_id" ) )
    private InventoryCategory inventoryCategory;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "vendor_id", foreignKey = @ForeignKey( name = "fk_inventory_item_vendor_id" ) )
    private Vendor vendor;
}
