package com.dgt.paribahanpool.inventory_item.model;

import com.dgt.paribahanpool.enums.InventoryType;
import com.dgt.paribahanpool.enums.ProductType;
import com.dgt.paribahanpool.enums.Unit;
import com.dgt.paribahanpool.enums.WarrantyType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.vehicle.model.BrandViewResponse;
import com.dgt.paribahanpool.vehicle.model.ModelViewResponse;
import com.dgt.paribahanpool.vendor.model.VendorSearchResponse;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.List;

@Data
public class InventoryItemAddRequest {

    private Long id;

//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    @NotBlank( message = "{validation.common.required}" )
    private String lotNumber;

    @NotBlank( message = "{validation.common.required}" )
    private String lotType;

    private Long country;

//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    @NotBlank( message = "{validation.common.required}" )
    private String packageNumber;

//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    @NotBlank( message = "{validation.common.required}" )
    private String productNameEn;

    @NotBlank( message = "{validation.common.required}" )
    private String productNameBn;

    @NotNull( message = "{validation.common.required}" )
    private ProductType productType;

    @NotNull( message = "{validation.common.required}" )
    private String fiscalYear;

    private String productId;

    @NotNull( message = "{validation.common.required}" )
    private InventoryType inventoryType;

    private Double quantity;

    @NotNull( message = "{validation.common.required}" )
    private Unit unit;

    private Double unitPrice;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate purchaseDate;

//    @Pattern( regexp = "[a-zA-Z ]+|", message = "{validation.common.alphanumeric}" )
    private String sourceNameEn;

    private String sourceNameBn;

//    @Pattern( regexp = "[a-zA-Z ]+|", message = "{validation.common.alphanumeric}" )
    private String sellerNameEn;

    private String sellerNameBn;

//    @Pattern( regexp = "[a-zA-Z ]+|", message = "{validation.common.alphanumeric}" )
    private String brandName;

//    @Pattern( regexp = "[a-zA-Z ]+|", message = "{validation.common.alphanumeric}" )
    private String model;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate productionDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate expireDate;

    private String location;

    @NotNull
    private Integer warrantyPeriod;

    @NotNull
    private WarrantyType warrantyType;

    private Boolean isForSpecificVehicle;

    private List<InventoryCategoryViewResponse> inventoryCategoryViewResponseList;

    private List<BrandViewResponse> brandViewResponseList;

    private List<ModelViewResponse> modelViewResponseList;

    private List<VendorSearchResponse> vendorSearchResponseList;

    @NotNull
    private Long vendorId;

//    @NotNull
    private Long brandId;

//    @NotNull
    private Long modelId;

    @NotNull
    private Long inventoryCategoryId;
}
