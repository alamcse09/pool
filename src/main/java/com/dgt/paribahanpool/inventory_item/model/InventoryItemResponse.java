package com.dgt.paribahanpool.inventory_item.model;

import com.dgt.paribahanpool.enums.Unit;
import lombok.Data;

@Data
public class InventoryItemResponse {

    private Long id;
    private String nameEn;
    private String nameBn;
    private Double metadata;
    private Unit unit;
}
