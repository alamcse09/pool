package com.dgt.paribahanpool.inventory_item.model;

import com.dgt.paribahanpool.enums.InventoryType;
import com.dgt.paribahanpool.enums.ProductType;
import com.dgt.paribahanpool.enums.Unit;
import lombok.Data;

import java.time.LocalDate;

@Data
public class InventoryItemSearchResponse {

    private Long id;

    private String lotNumber;

    private String packageNumber;

    private String productNameEn;

    private String productNameBn;

    private ProductType productType;

    private String productId;

    private InventoryType inventoryType;

    private Double quantity;

    private Unit unit;

    private Double unitPrice;

    private LocalDate purchaseDate;

    private String sourceNameEn;

    private String sourceNameBn;

    private String sellerNameEn;

    private String sellerNameBn;

    private String productDetails;

    private String brandName;

    private String origin;

    private String model;

    private LocalDate productionDate;

    private LocalDate expireDate;

    private String location;

}
