package com.dgt.paribahanpool.inventory_item.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class InventoryMetaData {

    private Long id;
    private String vendorNameBn;
    private LocalDate useDate;
    private String productNameBn;
    private Integer warrantyPeriod;
}
