package com.dgt.paribahanpool.inventory_item.model;

import lombok.Data;

@Data
public class InventoryPackageReportResponse {

    private String packageNumber;

    private String packageName;

    private String lotNumber;

    private String lotName;

    private String productName;

    private String unit;

    private String amount;

    private String rate;

    private String totalPrice;
}
