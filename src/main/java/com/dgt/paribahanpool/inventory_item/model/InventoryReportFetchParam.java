package com.dgt.paribahanpool.inventory_item.model;

import lombok.Data;

@Data
public class InventoryReportFetchParam {

    private String packageNumber;
    private String lotNumber;
}
