package com.dgt.paribahanpool.inventory_item.service;

import com.dgt.paribahanpool.inventory_item.model.InventoryCategory;
import com.dgt.paribahanpool.vehicle.model.Brand;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InventoryCategoryRepository extends DataTablesRepository<InventoryCategory, Long>, JpaRepository<InventoryCategory,Long> {
}
