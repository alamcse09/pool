package com.dgt.paribahanpool.inventory_item.service;

import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.inventory_item.model.InventoryCategory;
import com.dgt.paribahanpool.inventory_item.model.InventoryCategoryViewResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class InventoryCategoryService {

    private final InventoryCategoryRepository inventoryCategoryRepository;

    public List<InventoryCategoryViewResponse> getAllInventoryCategoryViewResponse() {

        List<InventoryCategory> inventoryCategoryList = findAll();

        return inventoryCategoryList.stream()
                .map( this::getAllInventoryCategoryViewResponseFromInventoryCategory )
                .collect( Collectors.toList() );
    }

    private InventoryCategoryViewResponse getAllInventoryCategoryViewResponseFromInventoryCategory(InventoryCategory inventoryCategory) {

        return InventoryCategoryViewResponse.builder()
                .id( inventoryCategory.getId() )
                .nameEn( inventoryCategory.getNameEn() )
                .nameBn( inventoryCategory.getNameBn() )
                .build();
    }

    private List<InventoryCategory> findAll() {

        return inventoryCategoryRepository.findAll();
    }

    public Optional<InventoryCategory> findById(Long inventoryCategoryId) {

        return inventoryCategoryRepository.findById( inventoryCategoryId );
    }

    public RestValidationResult isExist(Long id ) throws NotFoundException {

        if( !existById( id ) ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    private Boolean existById(Long id) {

        return inventoryCategoryRepository.existsById( id );
    }
}
