package com.dgt.paribahanpool.inventory_item.service;

import com.dgt.paribahanpool.country.model.Country;
import com.dgt.paribahanpool.country.model.CountryResponse;
import com.dgt.paribahanpool.country.service.CountryService;
import com.dgt.paribahanpool.inventory_item.model.*;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.BrandViewResponse;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.service.BrandService;
import com.dgt.paribahanpool.vendor.model.VendorSearchResponse;
import com.dgt.paribahanpool.vendor.service.VendorService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class InventoryItemModelService {

    private final MvcUtil mvcUtil;
    private final InventoryItemService inventoryItemService;
    private final BrandService brandService;
    private final InventoryCategoryService inventoryCategoryService;
    private final VendorService vendorService;
    private final CountryService countryService;

    public void addInventoryGet(Model model) {

        InventoryItemAddRequest inventoryItemAddRequest = new InventoryItemAddRequest();

        List<InventoryCategoryViewResponse> inventoryCategoryViewResponseList = inventoryCategoryService.getAllInventoryCategoryViewResponse();

        inventoryItemAddRequest.setInventoryCategoryViewResponseList( inventoryCategoryViewResponseList );

        List<BrandViewResponse> brandViewResponseList = brandService.getAllBrandViewResponse();

        inventoryItemAddRequest.setBrandViewResponseList( brandViewResponseList );

        List<VendorSearchResponse> vendorSearchResponseList = vendorService.getAllVendorSearchResponseList();

        inventoryItemAddRequest.setVendorSearchResponseList( vendorSearchResponseList );

        List<CountryResponse> allCountryList = countryService.getAllCountries();

        List<String> fiscalYearList = new ArrayList<>();

        int thisYear = LocalDate.now().getYear();

        for(int k=thisYear-2;k<thisYear+2;k++){
            fiscalYearList.add(k+"-"+(k+1));
        }

        model.addAttribute("inventoryItemAddRequest", inventoryItemAddRequest );
        model.addAttribute("allCountryList", allCountryList );
        model.addAttribute("fiscalYearList", fiscalYearList );

    }

    public InventoryItem addInventoryPost(InventoryItemAddRequest inventoryItemAddRequest, Model model) {

        InventoryItem inventoryItem = inventoryItemService.save( inventoryItemAddRequest );
        mvcUtil.addSuccessMessage( model, "inventory.add.success" );
        return inventoryItem;
    }

    public void editInventory(Model model, Long inventoryItemId) {

        Optional<InventoryItem> inventoryItemOptional = inventoryItemService.findById( inventoryItemId );

        if( inventoryItemOptional.isPresent() ) {

            InventoryItemAddRequest inventoryItemAddRequest = inventoryItemService.getInventoryItemGetRequestFromInventoryItem(inventoryItemOptional.get());

            List<InventoryCategoryViewResponse> inventoryCategoryViewResponseList = inventoryCategoryService.getAllInventoryCategoryViewResponse();

            inventoryItemAddRequest.setInventoryCategoryViewResponseList( inventoryCategoryViewResponseList );

            List<BrandViewResponse> brandViewResponseList = brandService.getAllBrandViewResponse();

            inventoryItemAddRequest.setBrandViewResponseList( brandViewResponseList );

            List<VendorSearchResponse> vendorSearchResponseList = vendorService.getAllVendorSearchResponseList();

            inventoryItemAddRequest.setVendorSearchResponseList( vendorSearchResponseList );

            List<CountryResponse> allCountryList = countryService.getAllCountries();

            List<String> fiscalYearList = new ArrayList<>();

            int thisYear = LocalDate.now().getYear();

            for(int k=thisYear-2;k<thisYear+2;k++){
                fiscalYearList.add(k+"-"+(k+1));
            }

            model.addAttribute("inventoryItemAddRequest", inventoryItemAddRequest );
            model.addAttribute("allCountryList", allCountryList );
            model.addAttribute("fiscalYearList", fiscalYearList );
        }
    }

    public InventoryItem editInventoryPost(InventoryItemAddRequest inventoryItemAddRequest, Model model) {

        InventoryItem inventoryItem = inventoryItemService.save( inventoryItemAddRequest );
        mvcUtil.addSuccessMessage( model, "inventory.edit.success" );
        return inventoryItem;
    }

    public void getInventoryItemsByPackage(Model model, InventoryReportFetchParam inventoryReportFetchParam) {

        List<InventoryPackageReportResponse> inventoryPackageReportResponses = inventoryItemService.getInventoryItemsByPackageId(inventoryReportFetchParam);
        model.addAttribute("totalPrice",inventoryPackageReportResponses.get(inventoryPackageReportResponses.size()-1).getTotalPrice());
        inventoryPackageReportResponses.remove(inventoryPackageReportResponses.size()-1);
        model.addAttribute("inventoryPackageReportResponses",inventoryPackageReportResponses);

    }

    public void getReportSearchParams(Model model) {
        InventoryReportFetchParam inventoryReportFetchParam = new InventoryReportFetchParam();
        model.addAttribute("inventoryReportFetchParam",inventoryReportFetchParam);
    }
}
