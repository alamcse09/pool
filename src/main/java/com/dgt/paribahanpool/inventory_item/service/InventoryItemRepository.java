package com.dgt.paribahanpool.inventory_item.service;

import com.dgt.paribahanpool.enums.WarrantyType;
import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import com.dgt.paribahanpool.inventory_item.model.InventoryItemResponse;
import com.dgt.paribahanpool.inventory_item.model.InventoryMetaData;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface InventoryItemRepository extends DataTablesRepository<InventoryItem, Long>, JpaRepository<InventoryItem,Long> {

    @Override
    List<InventoryItem> findAll();

    List<InventoryItem> findInventoryItemByPackageNumber(String packageNumber);

    List<InventoryItem> findInventoryItemByPackageNumberAndLotNumber(String packageNumber,String lotNumber);

    List<InventoryItem> findInventoryItemByLotNumber(String lotNumber);

    List<InventoryItem> findByWarrantyType(WarrantyType warrantyType);

    @Query( "SELECT new com.dgt.paribahanpool.inventory_item.model.InventoryMetaData( i.id, i.vendor.vendorName, vp.useDate, i.productNameBn, i.warrantyPeriod ) From InventoryItem i LEFT JOIN VehicleParts vp ON i.id = vp.inventoryItem.id WHERE i.warrantyType = ?1" )
    List<InventoryMetaData> findFromVehiclePartsFromTableForUsingDate(WarrantyType warrantyType );
}
