package com.dgt.paribahanpool.inventory_item.service;

import com.dgt.paribahanpool.enums.ProductType;
import com.dgt.paribahanpool.enums.WarrantyType;
import com.dgt.paribahanpool.inventory_item.model.*;
import com.dgt.paribahanpool.util.Util;
import com.dgt.paribahanpool.vehicle.model.Brand;
import com.dgt.paribahanpool.vehicle.model.Model;
import com.dgt.paribahanpool.vehicle.service.BrandService;
import com.dgt.paribahanpool.vehicle.service.ModelService;
import com.dgt.paribahanpool.vendor.model.Vendor;
import com.dgt.paribahanpool.vendor.service.VendorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.dgt.paribahanpool.inventory_item.service.InventorySpecification.*;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class InventoryItemService {

    private final InventoryItemRepository inventoryItemRepository;
    private final BrandService brandService;
    private final ModelService modelService;
    private final InventoryCategoryService inventoryCategoryService;
    private final VendorService vendorService;

    public Optional<InventoryItem> findById(Long id ){

        return inventoryItemRepository.findById( id );
    }

    public InventoryItem save( InventoryItem inventoryItem ) {

        return inventoryItemRepository.save( inventoryItem );
    }

    public List<InventoryItem> save(Collection inventoryItemList ) {

        return inventoryItemRepository.saveAll(inventoryItemList);
    }

    @Transactional
    public InventoryItem save(InventoryItemAddRequest inventoryItemAddRequest) {

        InventoryItem inventoryItem = new InventoryItem();

        if( inventoryItemAddRequest.getId() != null ) {

            inventoryItem = findById( inventoryItemAddRequest.getId() ).get();
        }

        getInventoryItemFromAddRequest( inventoryItem, inventoryItemAddRequest );

        return save( inventoryItem );
    }

    private InventoryItem getInventoryItemFromAddRequest( InventoryItem inventoryItem, InventoryItemAddRequest inventoryItemAddRequest ) {

        inventoryItem.setLotNumber(inventoryItemAddRequest.getLotNumber());
        inventoryItem.setPackageNumber(inventoryItemAddRequest.getPackageNumber());
        inventoryItem.setProductNameEn(inventoryItemAddRequest.getProductNameEn());
        inventoryItem.setProductNameBn(inventoryItemAddRequest.getProductNameBn());
        inventoryItem.setProductType(inventoryItemAddRequest.getProductType());
        inventoryItem.setProductId(inventoryItemAddRequest.getProductId());
        inventoryItem.setInventoryType(inventoryItemAddRequest.getInventoryType());
        inventoryItem.setQuantity(inventoryItemAddRequest.getQuantity());
        inventoryItem.setUnit(inventoryItemAddRequest.getUnit());
        inventoryItem.setUnitPrice(inventoryItemAddRequest.getUnitPrice());
        inventoryItem.setPurchaseDate(inventoryItemAddRequest.getPurchaseDate());
        inventoryItem.setSourceNameEn(inventoryItemAddRequest.getSourceNameEn());
        inventoryItem.setSourceNameBn(inventoryItemAddRequest.getSourceNameBn());
        inventoryItem.setSellerNameEn(inventoryItemAddRequest.getSellerNameEn());
        inventoryItem.setSellerNameBn(inventoryItemAddRequest.getSellerNameBn());
        inventoryItem.setBrandName(inventoryItemAddRequest.getBrandName());
        inventoryItem.setModel(inventoryItemAddRequest.getModel());
        inventoryItem.setProductionDate(inventoryItemAddRequest.getProductionDate());
        inventoryItem.setExpireDate(inventoryItemAddRequest.getExpireDate());
        inventoryItem.setLocation(inventoryItemAddRequest.getLocation());
        inventoryItem.setWarrantyType( inventoryItemAddRequest.getWarrantyType() );
        inventoryItem.setIsForSpecificVehicle( inventoryItemAddRequest.getIsForSpecificVehicle() );
        inventoryItem.setWarrantyPeriod( inventoryItemAddRequest.getWarrantyPeriod() );
        inventoryItem.setLotType(inventoryItemAddRequest.getLotType());
        inventoryItem.setFiscalYear(inventoryItemAddRequest.getFiscalYear());
        inventoryItem.setCountry(inventoryItemAddRequest.getCountry());

        if (inventoryItemAddRequest.getVendorId() != null) {

            Optional<Vendor> vendorOptional = vendorService.findById(inventoryItemAddRequest.getVendorId());

            if (vendorOptional.isPresent()) {

                inventoryItem.setVendor( vendorOptional.get() );
            }
        }

        if( inventoryItemAddRequest.getIsForSpecificVehicle() == true ) {

            if (inventoryItemAddRequest.getBrandId() != null) {

                Optional<Brand> brandOptional = brandService.findById(inventoryItemAddRequest.getBrandId());

                if (brandOptional.isPresent()) {

                    inventoryItem.setBrand(brandOptional.get());
                }
            }

            if (inventoryItemAddRequest.getModelId() != null) {

                Optional<Model> modelOptional = modelService.findById(inventoryItemAddRequest.getModelId());

                if (modelOptional.isPresent()) {

                    inventoryItem.setVehicleModel(modelOptional.get());
                }
            }
        }

        if (inventoryItemAddRequest.getInventoryCategoryId() != null) {

            Optional<InventoryCategory> inventoryCategoryOptional = inventoryCategoryService.findById(inventoryItemAddRequest.getInventoryCategoryId());

            if (inventoryCategoryOptional.isPresent()) {

                inventoryItem.setInventoryCategory( inventoryCategoryOptional.get() );
            }
        }

        return inventoryItem;
    }

    public InventoryItemAddRequest getInventoryItemGetRequestFromInventoryItem( InventoryItem inventoryItem ) {

        InventoryItemAddRequest inventoryItemAddRequest = new InventoryItemAddRequest();

        inventoryItemAddRequest.setId(inventoryItem.getId());
        inventoryItemAddRequest.setLotNumber(inventoryItem.getLotNumber());
        inventoryItemAddRequest.setPackageNumber(inventoryItem.getPackageNumber());
        inventoryItemAddRequest.setProductNameEn(inventoryItem.getProductNameEn());
        inventoryItemAddRequest.setProductNameBn(inventoryItem.getProductNameBn());
        inventoryItemAddRequest.setProductType(inventoryItem.getProductType());
        inventoryItemAddRequest.setProductId(inventoryItem.getProductId());
        inventoryItemAddRequest.setInventoryType(inventoryItem.getInventoryType());
        inventoryItemAddRequest.setQuantity(inventoryItem.getQuantity());
        inventoryItemAddRequest.setUnit(inventoryItem.getUnit());
        inventoryItemAddRequest.setUnitPrice(inventoryItem.getUnitPrice());
        inventoryItemAddRequest.setPurchaseDate(inventoryItem.getPurchaseDate());
        inventoryItemAddRequest.setSourceNameEn(inventoryItem.getSourceNameEn());
        inventoryItemAddRequest.setSourceNameBn(inventoryItem.getSourceNameBn());
        inventoryItemAddRequest.setSellerNameEn(inventoryItem.getSellerNameEn());
        inventoryItemAddRequest.setSellerNameBn(inventoryItem.getSellerNameBn());
        inventoryItemAddRequest.setBrandName(inventoryItem.getBrandName());
        inventoryItemAddRequest.setModel(inventoryItem.getModel());
        inventoryItemAddRequest.setProductionDate(inventoryItem.getProductionDate());
        inventoryItemAddRequest.setExpireDate(inventoryItem.getExpireDate());
        inventoryItemAddRequest.setLocation(inventoryItem.getLocation());
        inventoryItemAddRequest.setCountry(inventoryItem.getCountry());
        inventoryItemAddRequest.setLotType(inventoryItem.getLotType());
        inventoryItemAddRequest.setFiscalYear(inventoryItem.getFiscalYear());

        return inventoryItemAddRequest;
    }


    public DataTablesOutput<InventoryItemSearchResponse> searchForDatatable(DataTablesInput dataTablesInput) {

        Specification<InventoryItem> inventoryItemSpecification = filterByIsDeleted(false);

        return inventoryItemRepository.findAll( dataTablesInput, null, inventoryItemSpecification, this::getInventoryItemSearchResponseFromInventoryItem );
    }

    private InventoryItemSearchResponse getInventoryItemSearchResponseFromInventoryItem(InventoryItem inventoryItem) {

        InventoryItemSearchResponse inventoryItemSearchResponse = new InventoryItemSearchResponse();

        inventoryItemSearchResponse.setId( inventoryItem.getId() );
        inventoryItemSearchResponse.setLotNumber(inventoryItem.getLotNumber());
        inventoryItemSearchResponse.setPackageNumber(inventoryItem.getPackageNumber());
        inventoryItemSearchResponse.setProductNameEn(inventoryItem.getProductNameEn());
        inventoryItemSearchResponse.setProductNameBn(inventoryItem.getProductNameBn());
        inventoryItemSearchResponse.setProductType(inventoryItem.getProductType());
        inventoryItemSearchResponse.setProductId(inventoryItem.getProductId());
        inventoryItemSearchResponse.setInventoryType(inventoryItem.getInventoryType());
        inventoryItemSearchResponse.setQuantity(inventoryItem.getQuantity());
        inventoryItemSearchResponse.setUnit(inventoryItem.getUnit());
        inventoryItemSearchResponse.setUnitPrice(inventoryItem.getUnitPrice());
        inventoryItemSearchResponse.setPurchaseDate(inventoryItem.getPurchaseDate());
        inventoryItemSearchResponse.setSourceNameEn(inventoryItem.getSourceNameEn());
        inventoryItemSearchResponse.setSourceNameBn(inventoryItem.getSourceNameBn());
        inventoryItemSearchResponse.setSellerNameEn(inventoryItem.getSellerNameEn());
        inventoryItemSearchResponse.setSellerNameBn(inventoryItem.getSellerNameBn());
        inventoryItemSearchResponse.setBrandName(inventoryItem.getBrandName());
        inventoryItemSearchResponse.setModel(inventoryItem.getModel());
        inventoryItemSearchResponse.setProductionDate(inventoryItem.getProductionDate());
        inventoryItemSearchResponse.setExpireDate(inventoryItem.getExpireDate());
        inventoryItemSearchResponse.setLocation(inventoryItem.getLocation());

        return inventoryItemSearchResponse;
    }

    public boolean existById( Long id ) {

        return inventoryItemRepository.existsById(id);
    }

    public void delete(Long id) {

        Optional<InventoryItem> inventoryItemToDelete =  findById( id );

        if( inventoryItemToDelete.isPresent() ) {

            InventoryItem inventoryItem = inventoryItemToDelete.get();
            inventoryItem.setIsDeleted( true );
            save( inventoryItem );
        }
    }

    public List<InventoryItemResponse> getInventoryItems() {

        List<InventoryItem> inventoryItems =  inventoryItemRepository.findAll();

        List<InventoryItemResponse> inventoryItemListResponse = new ArrayList<>();

        inventoryItemListResponse = inventoryItems.stream()
                .filter( item -> item.getIsDeleted() == false && item.getQuantity() > 0 )
                .map(this::getInventoryItemResponseFromInventoryItem)
                .collect(Collectors.toList());

        return inventoryItemListResponse;

    }

    private InventoryItemResponse getInventoryItemResponseFromInventoryItem(InventoryItem inventoryItem) {
        InventoryItemResponse inventoryItemResponse = new InventoryItemResponse();

        inventoryItemResponse.setId( inventoryItem.getId() );
        inventoryItemResponse.setNameBn( String.format( "%s ( %s %s )", inventoryItem.getProductNameBn(), Util.makeDigitBangla( inventoryItem.getQuantity().toString() ), getUnitBn( inventoryItem.getProductType() ) ) );
        inventoryItemResponse.setNameEn( String.format( "%s ( %s %s )", inventoryItem.getProductNameEn(), inventoryItem.getQuantity(), getUnitEn( inventoryItem.getProductType() ) ) );
        inventoryItemResponse.setMetadata( inventoryItem.getUnitPrice() );
        inventoryItemResponse.setUnit( inventoryItem.getUnit() );

        return inventoryItemResponse;
    }

    private String getUnitEn(ProductType productType) {

        if( productType.equals(ProductType.STATIONARY) || productType.equals(ProductType.PARTS) ) {
            return "Set";
        } else if( productType.equals(ProductType.OIL) ) {
            return "Liter";
        } else {
            return "";
        }
    }

    private String getUnitBn(ProductType productType) {

        if( productType.equals(ProductType.STATIONARY) || productType.equals(ProductType.PARTS) ) {
            return "সেট";
        } else if( productType.equals(ProductType.OIL) ) {
            return "লিটার";
        } else {
            return "";
        }
    }

    public List<InventoryItemResponse> findBySpecificVehicleAndModelId( Long inventoryCategoryId, Long modelId ) {

        Specification<InventoryItem> inventoryItemSpecificationFilteredByModelIdAndInventoryCategoryId = filterByIsSpecificVehicle( false ).and( filterByModelId( modelId )  ).and( filterByCategoryId( inventoryCategoryId ) );

        Specification<InventoryItem> inventoryItemSpecificationFilteredByInventoryCategoryId = filterByCategoryId( inventoryCategoryId );

        List<InventoryItem> allInventoryItemList = findAll( inventoryItemSpecificationFilteredByInventoryCategoryId );

        List<InventoryItem> inventoryItemListFilteredByModelIdAndInventoryCategoryId = findAll( inventoryItemSpecificationFilteredByModelIdAndInventoryCategoryId );

        List<InventoryItem> inventoryItemList = (List<InventoryItem>) CollectionUtils.subtract( allInventoryItemList, inventoryItemListFilteredByModelIdAndInventoryCategoryId );

        return inventoryItemList.stream()
                .map( this::getInventoryItemResponseFromInventoryItem )
                .collect(Collectors.toList());
    }

    private List<InventoryItem> findAll() {

        return inventoryItemRepository.findAll();
    }

    private List<InventoryItem> findAll( Specification<InventoryItem> inventoryItemSpecification ){

        return inventoryItemRepository.findAll( inventoryItemSpecification );
    }

    public List<InventoryItem> getAllInventoryWithWarrantyFromBuyDate(){

        return inventoryItemRepository.findByWarrantyType( WarrantyType.FROM_BUYING_DATE );
    }

    public List<InventoryMetaData> getAllInventoryWithWarrantyFromUseDate() {

        return inventoryItemRepository.findFromVehiclePartsFromTableForUsingDate( WarrantyType.FROM_USING_DATE );
    }

    public List<InventoryPackageReportResponse> getInventoryItemsByPackageId(InventoryReportFetchParam inventoryReportFetchParam) {
        List<InventoryItem> inventoryItems = new ArrayList<>();
        List<InventoryPackageReportResponse> inventoryPackageReportResponses = new ArrayList<>();

        if (!inventoryReportFetchParam.getPackageNumber().isEmpty() && !inventoryReportFetchParam.getLotNumber().isEmpty() ){
            inventoryItems = inventoryItemRepository.findInventoryItemByPackageNumberAndLotNumber(inventoryReportFetchParam.getPackageNumber(),inventoryReportFetchParam.getLotNumber());
        }else if (!inventoryReportFetchParam.getPackageNumber().isEmpty() && inventoryReportFetchParam.getLotNumber().isEmpty()){
            inventoryItems = inventoryItemRepository.findInventoryItemByPackageNumber(inventoryReportFetchParam.getPackageNumber());
        }else if (inventoryReportFetchParam.getPackageNumber().isEmpty() && !inventoryReportFetchParam.getLotNumber().isEmpty()){
            inventoryItems = inventoryItemRepository.findInventoryItemByLotNumber(inventoryReportFetchParam.getLotNumber());
        }

        double total = 0;
        for (InventoryItem inventoryItem : inventoryItems){
            total += inventoryItem.getQuantity() * inventoryItem.getUnitPrice();
            inventoryPackageReportResponses.add(getInventoryPackageReportResponseFromInventoryItem(inventoryItem));
        }
        InventoryPackageReportResponse totalCarrier = new InventoryPackageReportResponse();
        totalCarrier.setTotalPrice(Util.makeDigitBangla(total));
        inventoryPackageReportResponses.add(totalCarrier);
        return inventoryPackageReportResponses;
    }

    public InventoryPackageReportResponse getInventoryPackageReportResponseFromInventoryItem(InventoryItem inventoryItem){
        InventoryPackageReportResponse inventoryPackageReportResponse = new InventoryPackageReportResponse();

        inventoryPackageReportResponse.setPackageName(inventoryItem.getProductNameBn());
        inventoryPackageReportResponse.setPackageNumber(Util.makeDigitBangla(inventoryItem.getPackageNumber().toString()));
        inventoryPackageReportResponse.setLotNumber(Util.makeDigitBangla(inventoryItem.getLotNumber().toString()));
        inventoryPackageReportResponse.setLotName(inventoryItem.getProductNameBn());
        inventoryPackageReportResponse.setProductName(inventoryItem.getProductNameBn());
        inventoryPackageReportResponse.setUnit(com.dgt.paribahanpool.enums.Unit.getLabel(inventoryItem.getUnit()));
        inventoryPackageReportResponse.setAmount(Util.makeDigitBangla(inventoryItem.getQuantity().toString()));
        inventoryPackageReportResponse.setRate(Util.makeDigitBangla(inventoryItem.getUnitPrice().toString()));
        inventoryPackageReportResponse.setTotalPrice(Util.makeDigitBangla(inventoryItem.getUnitPrice() * inventoryItem.getQuantity()));

        return inventoryPackageReportResponse;
    }
}
