package com.dgt.paribahanpool.inventory_item.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.inventory_item.model.InventoryCategoryViewResponse;
import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import com.dgt.paribahanpool.inventory_item.model.InventoryItemAddRequest;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.BrandViewResponse;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.service.BrandService;
import com.dgt.paribahanpool.vendor.model.VendorSearchResponse;
import com.dgt.paribahanpool.vendor.service.VendorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class InventoryItemValidationService {

    private final MvcUtil mvcUtil;
    private final InventoryItemService inventoryItemService;
    private final InventoryCategoryService inventoryCategoryService;
    private final BrandService brandService;
    private final VendorService vendorService;

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, InventoryItemAddRequest inventoryItemAddRequest) {

        List<InventoryCategoryViewResponse> inventoryCategoryViewResponseList = inventoryCategoryService.getAllInventoryCategoryViewResponse();

        inventoryItemAddRequest.setInventoryCategoryViewResponseList( inventoryCategoryViewResponseList );

        List<BrandViewResponse> brandViewResponseList = brandService.getAllBrandViewResponse();

        inventoryItemAddRequest.setBrandViewResponseList( brandViewResponseList );

        List<VendorSearchResponse> vendorSearchResponseList = vendorService.getAllVendorSearchResponseList();

        inventoryItemAddRequest.setVendorSearchResponseList( vendorSearchResponseList );

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "inventoryItemAddRequest" )
                        .object( inventoryItemAddRequest )
                        .title( "title.inventory.add" )
                        .content( "inventory/inventory-add" )
                        .activeMenu( Menu.INVENTORY_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.INVENTORY_ADD_FORM } )
                        .build()
        );

    }

    public boolean editInventoryPost(RedirectAttributes redirectAttributes, InventoryItemAddRequest inventoryItemAddRequest) {

        if( inventoryItemAddRequest.getId() != null ) {

            Optional<InventoryItem> inventoryItem = inventoryItemService.findById( inventoryItemAddRequest.getId() );

            if ( !inventoryItem.isPresent() ) {

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
                return false;
            }
        }

        return true;
    }

    public boolean editInventory( RedirectAttributes redirectAttributes, Long inventoryItemId ) {

        Optional<InventoryItem> inventoryItemOptional = inventoryItemService.findById( inventoryItemId );

        if( !inventoryItemOptional.isPresent() ) {
            mvcUtil.addErrorMessage( redirectAttributes , "validation.common.notfound");
            return false;
        }

        return true;
    }

    public Boolean handleBindingResultForEditFormPost(Model model, BindingResult bindingResult, InventoryItemAddRequest inventoryItemAddRequest) {

        List<InventoryCategoryViewResponse> inventoryCategoryViewResponseList = inventoryCategoryService.getAllInventoryCategoryViewResponse();

        inventoryItemAddRequest.setInventoryCategoryViewResponseList( inventoryCategoryViewResponseList );

        List<BrandViewResponse> brandViewResponseList = brandService.getAllBrandViewResponse();

        inventoryItemAddRequest.setBrandViewResponseList( brandViewResponseList );

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "inventoryItemAddRequest" )
                        .object( inventoryItemAddRequest )
                        .title( "title.inventory.item.edit" )
                        .content( "inventory/inventory-add" )
                        .activeMenu( Menu.INVENTORY_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.INVENTORY_ADD_FORM, FrontEndLibrary.BRAND_MODEL_ADD } )
                        .build()
        );
    }

    public RestValidationResult delete(Long id) throws NotFoundException {

        if( !inventoryItemService.existById( id ) ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }
}
