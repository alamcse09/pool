package com.dgt.paribahanpool.inventory_item.service;

import com.dgt.paribahanpool.driver.model.Driver_;
import com.dgt.paribahanpool.inventory_item.model.InventoryCategory_;
import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import com.dgt.paribahanpool.inventory_item.model.InventoryItem_;
import com.dgt.paribahanpool.vehicle.model.Model_;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.model.Vehicle_;
import org.springframework.data.jpa.domain.Specification;

public class InventorySpecification {

    public static Specification<InventoryItem> filterByIsDeleted(Boolean isDeleted) {

        if( isDeleted == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( InventoryItem_.IS_DELETED ), isDeleted );
    }

    public static Specification<InventoryItem> filterByIsSpecificVehicle(Boolean isForSpecificVehicle) {

        if( isForSpecificVehicle == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( InventoryItem_.IS_FOR_SPECIFIC_VEHICLE ), isForSpecificVehicle );
    }

    public static Specification<InventoryItem> filterByModelId( Long modelId ){

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( InventoryItem_.VEHICLE_MODEL ).get( Model_.ID ), modelId );
    }

    public static Specification<InventoryItem> filterByCategoryId( Long categoryId ){

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( InventoryItem_.INVENTORY_CATEGORY ).get(InventoryCategory_.ID ), categoryId );
    }

}
