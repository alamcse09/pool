package com.dgt.paribahanpool.leave.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.leave.model.LeaveApplication;
import com.dgt.paribahanpool.leave.model.LeaveApplicationAddRequest;
import com.dgt.paribahanpool.leave.service.LeaveApplicationModelService;
import com.dgt.paribahanpool.leave.service.LeaveApplicationValidationService;
import com.dgt.paribahanpool.log.model.LogEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/leave-application" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class LeaveApplicationController extends MVCController {

    private final LeaveApplicationModelService leaveApplicationModelService;
    private final LeaveApplicationValidationService leaveApplicationValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.leave.add", content = "leave-application/leave-application-add", activeMenu = Menu.LEAVE_APPLICATION_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.LEAVE_APPLICATION_ADD } )
    public String add(
            HttpServletRequest httpServletRequest,
            Model model
    ) {

        log.debug( "Rendering Add Leave Application Form" );
        leaveApplicationModelService.addLeaveApplication( model, getLoggedInUser().getUser() );
        return viewRoot;
    }

    @PostMapping( value = "/add" )
    public String addLeaveApplication(
            @Valid LeaveApplicationAddRequest leaveApplicationAddRequest,
            RedirectAttributes redirectAttributes,
            Model model,
            BindingResult bindingResult,
            HttpServletRequest request
    ){
        if( !leaveApplicationValidationService.handleBindingResultForAddFormPost( model, bindingResult, leaveApplicationAddRequest ) ){

            return viewRoot;
        }

        if( leaveApplicationValidationService.validLeaveApplicationPost( model, redirectAttributes, leaveApplicationAddRequest, getLoggedInUser() ) ){

            LeaveApplication leaveApplication = leaveApplicationModelService.addLeaveApplicationPost( leaveApplicationAddRequest, redirectAttributes, getLoggedInUser() );
            log( LogEvent.LEAVE_APPLICATION_ADD, leaveApplication.getId(), "Leave Application added", request );

            return "redirect:/leave-application/search";
        }

        return viewRoot;
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.leave.search", content = "leave-application/leave-application-search", activeMenu = Menu.LEAVE_APPLICATION_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.LEAVE_APPLICATION_SEARCH } )
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ) {

        log.debug( "Leave Application Search page called" );
        return viewRoot;
    }

    @GetMapping( "/leave-approval-search" )
    @TitleAndContent( title = "title.leave.leave-approval-search", content = "leave-application/leave-approval-search", activeMenu = Menu.LEAVE_APPROVAL_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.LEAVE_APPROVAL_SEARCH } )
    public String getLeaveApprovalSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Leave Approval Search page called" );
        return viewRoot;
    }

    @GetMapping( "/{id}" )
    @TitleAndContent( title = "title.leave.leave-approval-view", content = "leave-application/leave-approval-view", activeMenu = Menu.LEAVE_APPROVAL_SEARCH )
    public String view(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ){

        if( leaveApplicationValidationService.view( redirectAttributes, id ) ) {

            leaveApplicationModelService.view( model, id, getLoggedInUser(), request );
            return viewRoot;
        }

        return "redirect:/leave-application/leave-approval-search";
    }

    @PostMapping( "/take-action" )
    @TitleAndContent( title = "title.leave.leave-approval-view", content = "leave-application/leave-approval-view", activeMenu = Menu.LEAVE_APPROVAL_SEARCH )
    public String takeAction(
            @RequestParam( "id" ) Long id,
            @RequestParam( "action" ) Long actionId,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ) throws Exception {

        if( leaveApplicationValidationService.takeAction( redirectAttributes, id, actionId, getLoggedInUser() ) ){

            leaveApplicationModelService.takeAction( redirectAttributes, id, actionId, getLoggedInUser() );
            log( LogEvent.LEAVE_APPLICATION_APPROVAL, actionId, "LEAVE Approval Take action", request );
        }
        else{

            return "redirect:/leave-application/" + id;
        }

        return "redirect:/leave-application/leave-approval-search";
    }
}
