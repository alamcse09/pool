package com.dgt.paribahanpool.leave.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.leave.model.LeaveApplicationSearchResponse;
import com.dgt.paribahanpool.leave.model.LeaveApprovalSearchResponse;
import com.dgt.paribahanpool.leave.service.LeaveApplicationService;
import com.dgt.paribahanpool.leave.service.LeaveApplicationValidationService;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/leave-application" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class LeaveApplicationRestController extends BaseRestController {

    private final LeaveApplicationService leaveApplicationService;
    private final LeaveApplicationValidationService leaveApplicationValidationService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<LeaveApplicationSearchResponse> searchLeaveApplication(
            DataTablesInput dataTablesInput,
            HttpServletRequest request
    ) {

        log.debug( "Request params, {}", dataTablesInput );
        return leaveApplicationService.searchForDatatable( dataTablesInput, request, getLoggedInUser().getUser().getId() );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = leaveApplicationValidationService.delete( id );
        log( LogEvent.LEAVE_DELETE, id, "", request );
        if( restValidationResult.getSuccess() ) {

            leaveApplicationService.delete( id );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{
            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @GetMapping(
            value = "/leave-approval-search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<LeaveApprovalSearchResponse> searchLeaveApproval(
            DataTablesInput dataTablesInput,
            HttpServletRequest request
    ) {

        log.debug( "Request params, {}", dataTablesInput );
        return leaveApplicationService.searchForLeaveApprovalDatatable( dataTablesInput, request, getLoggedInUser() );
    }
}
