package com.dgt.paribahanpool.leave.model;

import com.dgt.paribahanpool.enums.LeaveType;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.WorkflowEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table( name = "leave_application" )
public class LeaveApplication extends AuditableEntity implements WorkflowEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "leave_type" )
    private LeaveType leaveType;

    @Column( name = "emergency_phone_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String emergencyPhoneNumber;

    @Column( name = "leave_reason" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String leaveReason;

    @Column( name = "leave_start_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate leaveStartDate;

    @Column( name = "leave_end_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate leaveEndDate;

    @Column( name = "total_leave_count" )
    private Long totalLeaveCount;

    @Column( name = "start_state" )
    private Long startState;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne
    @JoinColumn( name = "state_id", foreignKey = @ForeignKey( name = "fk_leave_app_state_id" ) )
    private State state;

    @Column( name = "document_group_id" )
    private Long docGroupId;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne
    @JoinColumn( name = "user_id", foreignKey = @ForeignKey( name = "fk_leave_app_user_id" ) )
    private User appliedBy;
}
