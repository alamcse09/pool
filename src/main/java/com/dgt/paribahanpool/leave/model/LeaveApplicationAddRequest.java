package com.dgt.paribahanpool.leave.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.LeaveType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class LeaveApplicationAddRequest {

    private Long id;

    @NotNull( message = "{validation.common.required}" )
    private LeaveType leaveType;

    @Digits( integer = 11, fraction = 0, message = "{validation.common.numeric}" )
    @NotNull( message = "{validation.common.required}" )
    private String emergencyPhoneNumber;

    private String leaveReason;

    @NotNull( message = "{validation.common.required}" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate leaveStartDate;

    @NotNull( message = "{validation.common.required}" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate leaveEndDate;

    private Long totalLeaveCount;

    private Long remainLeave;

    @NotNull( message = "{validation.common.required}" )
    private Long appliedTo;

    private Map<Long, String> appliedToMap = new HashMap<>();

    private MultipartFile[] files;

    List<DocumentMetadata> documents = Collections.EMPTY_LIST;
}
