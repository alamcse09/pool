package com.dgt.paribahanpool.leave.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.LeaveType;
import com.dgt.paribahanpool.workflow.model.StateResponse;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class LeaveApplicationSearchResponse {

    private Long id;
    private String leaveType;
    private String emergencyPhoneNumber;
    private String leaveStartDate;
    private String leaveEndDate;
    private String appliedTo;
    private Long totalLeaveCount;
    private String leaveReason;

    private StateResponse stateResponse;
    private String statusEn;

    List<DocumentMetadata> documentMetadataList;
}
