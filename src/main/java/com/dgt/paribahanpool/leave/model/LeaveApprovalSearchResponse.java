package com.dgt.paribahanpool.leave.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.LeaveType;
import com.dgt.paribahanpool.enums.StateType;
import com.dgt.paribahanpool.workflow.model.StateResponse;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class LeaveApprovalSearchResponse {

    private Long id;
    private String leaveType;
    private String emergencyPhoneNumber;
    private String leaveStartDate;
    private String leaveEndDate;

    private StateResponse stateResponse;
    private String statusEn;
    public StateType stateType;

    List<DocumentMetadata> documentMetadataList;

    private LeaveApplicationLeaveApprovalSearchResponse appliedBy;
}
