package com.dgt.paribahanpool.leave.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LeaveApprovalSearchResponseEmployeeInfo {

    private String name;
    private String nameEn;

    private LeaveApprovalSearchResponseDesignationInfo designation;
}
