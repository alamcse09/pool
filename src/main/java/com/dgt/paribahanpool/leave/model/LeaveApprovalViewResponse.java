package com.dgt.paribahanpool.leave.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.LeaveType;
import com.dgt.paribahanpool.enums.StateType;
import lombok.Data;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
public class LeaveApprovalViewResponse {

    private Long id;
    private Long stateId;
    private String leaveType;
    private String emergencyPhoneNumber;
    private String leaveStartDate;
    private String leaveEndDate;
    private String appliedTo;

    public StateType stateType;
    List<DocumentMetadata> documents = Collections.EMPTY_LIST;

    private String employeeNameEn;
    private String employeeNameBn;

    private String designationNameEn;
    private String designationNameBn;

    private Long totalLeaveCount;
    private String leaveReason;
}
