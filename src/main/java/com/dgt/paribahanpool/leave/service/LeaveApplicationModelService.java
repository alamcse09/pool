package com.dgt.paribahanpool.leave.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.enums.LeaveType;
import com.dgt.paribahanpool.leave.model.LeaveApplication;
import com.dgt.paribahanpool.leave.model.LeaveApplicationAddRequest;
import com.dgt.paribahanpool.leave.model.LeaveApprovalViewResponse;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.workflow.model.StateActionMapResponse;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class LeaveApplicationModelService {

    private final MvcUtil mvcUtil;
    private final LeaveApplicationService leaveApplicationService;
    private final StateActionMapService stateActionMapService;
    private final Gson gson;

    @Transactional
    public void addLeaveApplication( Model model, User loggedInUser ){

        LeaveApplicationAddRequest leaveApplicationAddRequest = new LeaveApplicationAddRequest();

        Map<Long, String> appliedToMap = leaveApplicationService.getAppliedToMap();

        Map<String, Integer> leaveTypeIntegerMap = new HashMap<>();

        leaveTypeIntegerMap = leaveApplicationService.getLeaveAmountPerLeaveType( loggedInUser.getEmployee().getId() );

        leaveApplicationAddRequest.setAppliedToMap( appliedToMap );

        model.addAttribute("leaveTypeIntegerMapObject", gson.toJson( leaveTypeIntegerMap ) );

        model.addAttribute("leaveApplicationAddRequest", leaveApplicationAddRequest );
    }

    public LeaveApplication addLeaveApplicationPost(LeaveApplicationAddRequest leaveApplicationAddRequest, RedirectAttributes redirectAttributes, UserPrincipal loggedInUser ) {

        LeaveApplication leaveApplication = leaveApplicationService.save( leaveApplicationAddRequest, loggedInUser );
        mvcUtil.addSuccessMessage(redirectAttributes, "success.leave.add");

        return leaveApplication;
    }

    @Transactional
    public void view( Model model, Long id, UserPrincipal loggedInUser, HttpServletRequest request ) {

        Optional<LeaveApplication> leaveApplicationOptional = leaveApplicationService.findById( id );

        if( leaveApplicationOptional.isPresent() ) {

            LeaveApplication leaveApplication = leaveApplicationOptional.get();

            LeaveApprovalViewResponse leaveApprovalViewResponse = leaveApplicationService.getLeaveApprovalViewResponseFromLeaveApplication( leaveApplication, request );
            model.addAttribute( "leaveApprovalViewResponse", leaveApprovalViewResponse );

            if( leaveApprovalViewResponse.getStateId() != null ){

                List<StateActionMapResponse> stateActionMapList = stateActionMapService.findStateActionMapResponseListByCurrentStateAndRoleIdSet( leaveApprovalViewResponse.getStateId(), loggedInUser.getRoleIds() );
                model.addAttribute( "stateActionList", stateActionMapList );
            }
        }
    }

    public void takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, UserPrincipal loggedInUser ) throws Exception {

        leaveApplicationService.takeAction( id, actionId, loggedInUser );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.common.action.success" );
    }
}
