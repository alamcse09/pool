package com.dgt.paribahanpool.leave.service;

import com.dgt.paribahanpool.leave.model.LeaveApplication;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface LeaveApplicationRepository extends DataTablesRepository<LeaveApplication, Long> {
}
