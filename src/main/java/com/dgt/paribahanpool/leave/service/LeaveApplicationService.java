package com.dgt.paribahanpool.leave.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.designation.model.Designation;
import com.dgt.paribahanpool.document.model.Document;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.employee.model.Employee;
import com.dgt.paribahanpool.employee.model.EmployeeLeaveCount;
import com.dgt.paribahanpool.employee.service.EmployeeLeaveCountService;
import com.dgt.paribahanpool.enums.LeaveType;
import com.dgt.paribahanpool.leave.model.*;
import com.dgt.paribahanpool.locale.LocalizeUtil;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.workflow.model.*;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.dgt.paribahanpool.leave.service.LeaveApplicationSpecification.distinctLeave;
import static com.dgt.paribahanpool.leave.service.LeaveApplicationSpecification.filterByAvailableActionInLeave;
import static com.dgt.paribahanpool.leave.service.LeaveApplicationSpecification.filterByLoggedInUser;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class LeaveApplicationService implements WorkflowService<LeaveApplication> {

    private final LeaveApplicationRepository leaveApplicationRepository;
    private final DocumentService documentService;
    private final StateService stateService;
    private final LocalizeUtil localizeUtil;
    private final StateActionMapService stateActionMapService;
    private final EmployeeLeaveCountService employeeLeaveCountService;

    public Optional<LeaveApplication> findById( Long id ) {

        return leaveApplicationRepository.findById( id );
    }

    public LeaveApplication save( LeaveApplication leaveApplication ){

        return leaveApplicationRepository.save( leaveApplication );
    }

    public LeaveApplication save( LeaveApplicationAddRequest leaveApplicationAddRequest, UserPrincipal loggedInUser ) {

        LeaveApplication leaveApplication = new LeaveApplication();

        if( leaveApplicationAddRequest.getId() != null ) {

            leaveApplication = findById( leaveApplication.getId() ).get();
        }

        leaveApplication = getLeaveApplicationFromLeaveApplicationAddRequest( leaveApplication, leaveApplicationAddRequest, loggedInUser );

        return save( leaveApplication );
    }

    private LeaveApplication getLeaveApplicationFromLeaveApplicationAddRequest( LeaveApplication leaveApplication, LeaveApplicationAddRequest leaveApplicationAddRequest, UserPrincipal loggedInUser ) {

        leaveApplication.setLeaveType( leaveApplicationAddRequest.getLeaveType() );
        leaveApplication.setEmergencyPhoneNumber( leaveApplicationAddRequest.getEmergencyPhoneNumber() );
        leaveApplication.setLeaveReason( leaveApplicationAddRequest.getLeaveReason() );
        leaveApplication.setLeaveStartDate( leaveApplicationAddRequest.getLeaveStartDate() );
        leaveApplication.setLeaveEndDate( leaveApplicationAddRequest.getLeaveEndDate() );
        leaveApplication.setTotalLeaveCount( leaveApplicationAddRequest.getTotalLeaveCount() );
        leaveApplication.setStartState( leaveApplicationAddRequest.getAppliedTo() );

        User user = loggedInUser.getUser();

        leaveApplication.setAppliedBy( user );

        if( leaveApplication.getDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( leaveApplicationAddRequest.getFiles(), Collections.EMPTY_MAP );
            leaveApplication.setDocGroupId( documentUploadedResponse.getDocGroupId()  );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( leaveApplicationAddRequest.getFiles(), leaveApplication.getDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        if( leaveApplication.getState() == null ){

            Long startStateId = leaveApplicationAddRequest.getAppliedTo();
            State state =  stateService.findStateReferenceById( startStateId );
            leaveApplication.setState( state );
        }

        return leaveApplication;
    }

    public DataTablesOutput<LeaveApplicationSearchResponse> searchForDatatable(DataTablesInput dataTablesInput, HttpServletRequest request, Long loggedInUserId ) {

        return leaveApplicationRepository.findAll( dataTablesInput, null,  filterByLoggedInUser( loggedInUserId ), leaveApplication -> getLeaveApplicationSearchResponseFromLeaveApplication( leaveApplication, request ) );
    }

    private LeaveApplicationSearchResponse getLeaveApplicationSearchResponseFromLeaveApplication( LeaveApplication leaveApplication, HttpServletRequest request ) {

        LeaveApplicationSearchResponse leaveApplicationSearchResponse = new LeaveApplicationSearchResponse();

        Map<Long, String> appliedToMap = getAppliedToMap();
        String appliedTo = appliedToMap.get( leaveApplication.getStartState() );

        leaveApplicationSearchResponse.setId( leaveApplication.getId() );
        leaveApplicationSearchResponse.setLeaveType( localizeUtil.getMessageFromMessageSource( LeaveType.getLabel( leaveApplication.getLeaveType() ), request ) );
        leaveApplicationSearchResponse.setEmergencyPhoneNumber( leaveApplication.getEmergencyPhoneNumber() );
        leaveApplicationSearchResponse.setLeaveStartDate( leaveApplication.getLeaveStartDate().format( DateTimeFormatter.ofPattern("dd/MM/yyyy") ) );
        leaveApplicationSearchResponse.setLeaveEndDate( leaveApplication.getLeaveEndDate().format( DateTimeFormatter.ofPattern("dd/MM/yyyy") ) );
        leaveApplicationSearchResponse.setTotalLeaveCount( leaveApplication.getTotalLeaveCount() );
        leaveApplicationSearchResponse.setLeaveReason( leaveApplication.getLeaveReason() );
        leaveApplicationSearchResponse.setAppliedTo( localizeUtil.getMessageFromMessageSource( appliedTo, request ) );

        State state = leaveApplication.getState();
        if( state != null ){

            StateResponse stateResponse = StateResponse.builder()
                    .id( state.getId() )
                    .name( state.getName() )
                    .nameEn( state.getNameEn() )
                    .stateType( state.getStateType() )
                    .build();

            leaveApplicationSearchResponse.setStateResponse( stateResponse );
        }

        List<DocumentMetadata> documentDataList = documentService.findDocumentMetaDataListByGroupId( leaveApplication.getDocGroupId() );
        leaveApplicationSearchResponse.setDocumentMetadataList( documentDataList );

        return leaveApplicationSearchResponse;
    }

    public Map<Long, String> getAppliedToMap() {

        Map<Long, String> appliedToMap = new HashMap<>();

        appliedToMap.put( AppConstants.LEAVE_APPLICATION_MARINE_INIT_STATE_ID, "dropdown.options.leave.add.appliedTo.marine" );
        appliedToMap.put( AppConstants.LEAVE_APPLICATION_HeadOffice_INIT_STATE_ID, "dropdown.options.leave.add.appliedTo.headOffice" );
        appliedToMap.put( AppConstants.LEAVE_APPLICATION_RAndH_INIT_STATE_ID, "dropdown.options.leave.add.appliedTo.roadHighway" );

        return appliedToMap;
    }

    public Boolean existById( Long id ) {

        return leaveApplicationRepository.existsById( id );
    }

    public void delete( Long id ) {

        leaveApplicationRepository.deleteById( id );
    }

    public DataTablesOutput<LeaveApprovalSearchResponse> searchForLeaveApprovalDatatable(DataTablesInput dataTablesInput, HttpServletRequest request, UserPrincipal loggedInUser ) {

        return leaveApplicationRepository.findAll( dataTablesInput, distinctLeave(), filterByAvailableActionInLeave( loggedInUser.getRoleIds() ), leaveApplication -> getLeaveApprovalSearchResponseFromLeaveApplication( leaveApplication, request ) );
    }

    private LeaveApprovalSearchResponse getLeaveApprovalSearchResponseFromLeaveApplication( LeaveApplication leaveApplication, HttpServletRequest request ){

        if( leaveApplication.getAppliedBy() != null ){

            List<DocumentMetadata> documentDataList = documentService.findDocumentMetaDataListByGroupId( leaveApplication.getDocGroupId() );

            State state = leaveApplication.getState();

            StateResponse stateResponse = StateResponse.builder()
                    .id( state.getId() )
                    .name( state.getName() )
                    .nameEn( state.getNameEn() )
                    .stateType( state.getStateType() )
                    .build();

            Designation designation = leaveApplication.getAppliedBy().getEmployee().getDesignation();
            LeaveApprovalSearchResponseDesignationInfo leaveApprovalSearchResponseDesignationInfo = null;

            if( designation != null ) {
                leaveApprovalSearchResponseDesignationInfo = LeaveApprovalSearchResponseDesignationInfo.builder()
                        .nameBn(leaveApplication.getAppliedBy().getEmployee().getDesignation().getNameBn())
                        .nameEn(leaveApplication.getAppliedBy().getEmployee().getDesignation().getNameEn())
                        .build();
            }

            LeaveApprovalSearchResponseEmployeeInfo leaveApprovalSearchResponseEmployeeInfo = LeaveApprovalSearchResponseEmployeeInfo.builder()
                    .name( leaveApplication.getAppliedBy().getEmployee().getName() )
                    .nameEn( leaveApplication.getAppliedBy().getEmployee().getNameEn() )
                    .designation( leaveApprovalSearchResponseDesignationInfo )
                    .build();

            LeaveApplicationLeaveApprovalSearchResponse leaveApprovalSearchResponse = LeaveApplicationLeaveApprovalSearchResponse.builder()
                    .employee( leaveApprovalSearchResponseEmployeeInfo )
                    .build();

            return LeaveApprovalSearchResponse.builder()
                    .id( leaveApplication.getId() )
                    .leaveType( localizeUtil.getMessageFromMessageSource( LeaveType.getLabel( leaveApplication.getLeaveType() ), request ) )
                    .emergencyPhoneNumber( leaveApplication.getEmergencyPhoneNumber() )
                    .leaveStartDate( leaveApplication.getLeaveStartDate().format( DateTimeFormatter.ofPattern("dd/MM/yyyy") ) )
                    .leaveEndDate( leaveApplication.getLeaveEndDate().format( DateTimeFormatter.ofPattern("dd/MM/yyyy") ) )
                    .stateResponse( stateResponse )
                    .stateType( leaveApplication.getState().getStateType() )
                    .documentMetadataList( documentDataList )
                    .appliedBy( leaveApprovalSearchResponse )
                    .build();

        }

        return null;

    }

    public LeaveApprovalViewResponse getLeaveApprovalViewResponseFromLeaveApplication( LeaveApplication leaveApplication, HttpServletRequest request ) {

        LeaveApprovalViewResponse leaveApprovalViewResponse = new LeaveApprovalViewResponse();

        Map<Long, String> appliedToMap = getAppliedToMap();
        String appliedTo = appliedToMap.get( leaveApplication.getStartState() );

        List<DocumentMetadata> documentDataList = documentService.findDocumentMetaDataListByGroupId( leaveApplication.getDocGroupId() );


        leaveApprovalViewResponse.setId( leaveApplication.getId() );
        leaveApprovalViewResponse.setStateId( leaveApplication.getState() != null? leaveApplication.getState().getId(): null );
        leaveApprovalViewResponse.setLeaveType( localizeUtil.getMessageFromMessageSource( LeaveType.getLabel( leaveApplication.getLeaveType() ), request ) );
        leaveApprovalViewResponse.setEmergencyPhoneNumber( leaveApplication.getEmergencyPhoneNumber() );
        leaveApprovalViewResponse.setLeaveStartDate( leaveApplication.getLeaveStartDate().format( DateTimeFormatter.ofPattern("dd/MM/yyyy") ) );
        leaveApprovalViewResponse.setLeaveEndDate( leaveApplication.getLeaveEndDate().format( DateTimeFormatter.ofPattern("dd/MM/yyyy") ) );
        leaveApprovalViewResponse.setAppliedTo( localizeUtil.getMessageFromMessageSource( appliedTo, request ) );
        leaveApprovalViewResponse.setStateType( leaveApplication.getState().getStateType() );
        leaveApprovalViewResponse.setDocuments( documentDataList );
        leaveApprovalViewResponse.setLeaveReason( leaveApplication.getLeaveReason() );
        leaveApprovalViewResponse.setTotalLeaveCount( leaveApplication.getTotalLeaveCount() );

        Employee employee = leaveApplication.getAppliedBy().getEmployee();

        leaveApprovalViewResponse.setEmployeeNameEn( employee.getNameEn() );
        leaveApprovalViewResponse.setEmployeeNameBn( employee.getName() );
        leaveApprovalViewResponse.setDesignationNameEn( employee.getNameEn() );
        leaveApprovalViewResponse.setDesignationNameBn( employee.getName() );

        return leaveApprovalViewResponse;
    }

    @Transactional
    public void takeAction(Long id, Long actionId, UserPrincipal loggedInUser ) throws Exception {

        if( actionId.equals( AppConstants.LEAVE_APPLICATION_MARINE_APPROVE_ACTION_ID ) ||  actionId.equals( AppConstants.LEAVE_APPLICATION_HeadOffice_APPROVE_ACTION_ID ) || actionId.equals( AppConstants.LEAVE_APPLICATION_RAndH_APPROVE_ACTION_ID ) ) {

            updateEmployeeLeaveCount(  id, actionId, loggedInUser );
        }

        stateActionMapService.takeAction( this, id, actionId, loggedInUser );
    }

    private void updateEmployeeLeaveCount(Long id, Long actionId, UserPrincipal loggedInUser) {

       Optional<LeaveApplication> leaveApplicationOptional = findById( id );

       if( leaveApplicationOptional.isPresent() ){

           LeaveApplication leaveApplication = leaveApplicationOptional.get();

           Optional<EmployeeLeaveCount> employeeLeaveCountOptional = employeeLeaveCountService.getLeaveCountByEmployeeId(loggedInUser.getEmployee().getId());

           if (employeeLeaveCountOptional.isPresent()) {

               EmployeeLeaveCount employeeLeaveCount = employeeLeaveCountOptional.get();

               if (leaveApplication.getLeaveType() == LeaveType.SICK_LEAVE) {

                   employeeLeaveCount.setMedicalLeaveCount(employeeLeaveCount.getMedicalLeaveCount() - leaveApplication.getTotalLeaveCount().intValue());
               } else if (leaveApplication.getLeaveType() == LeaveType.YEARLY_LEAVE) {

                   employeeLeaveCount.setYearlyLeaveCount(employeeLeaveCount.getYearlyLeaveCount() - leaveApplication.getTotalLeaveCount().intValue());
               } else if (leaveApplication.getLeaveType() == LeaveType.CASUAL_LEAVE) {

                   employeeLeaveCount.setCasualLeaveCount(employeeLeaveCount.getCasualLeaveCount() - leaveApplication.getTotalLeaveCount().intValue());
               } else if (leaveApplication.getLeaveType() == LeaveType.EARNED_LEAVE) {

                   employeeLeaveCount.setEarnedLeaveCount(employeeLeaveCount.getEarnedLeaveCount() - leaveApplication.getTotalLeaveCount().intValue());
               } else if (leaveApplication.getLeaveType() == LeaveType.MEDICAL_LEAVE) {

                   employeeLeaveCount.setMedicalLeaveCount(employeeLeaveCount.getMedicalLeaveCount() - leaveApplication.getTotalLeaveCount().intValue());
               } else if (leaveApplication.getLeaveType() == LeaveType.ABROAD_LEAVE) {

                   employeeLeaveCount.setAbroadLeaveCount(employeeLeaveCount.getAbroadLeaveCount() - leaveApplication.getTotalLeaveCount().intValue());
               } else if (leaveApplication.getLeaveType() == LeaveType.AMUSEMENT_LEAVE) {

                   employeeLeaveCount.setAmusementLeaveCount(employeeLeaveCount.getAmusementLeaveCount() - leaveApplication.getTotalLeaveCount().intValue());
               } else if (leaveApplication.getLeaveType() == LeaveType.UNPAID_LEAVE) {

                   employeeLeaveCount.setUnpaidLeaveCount(employeeLeaveCount.getUnpaidLeaveCount() - leaveApplication.getTotalLeaveCount().intValue());
               } else if (leaveApplication.getLeaveType() == LeaveType.MATERNITY_LEAVE) {

                   employeeLeaveCount.setMaternityLeaveCount(employeeLeaveCount.getMaternityLeaveCount() - leaveApplication.getTotalLeaveCount().intValue());
               } else if (leaveApplication.getLeaveType() == LeaveType.OTHER_LEAVE) {

                   employeeLeaveCount.setOtherLeaveCount(employeeLeaveCount.getOtherLeaveCount() - leaveApplication.getTotalLeaveCount().intValue());
               }

               employeeLeaveCountService.save( employeeLeaveCount );
           }

       }

    }

    public Map<String, Integer> getLeaveAmountPerLeaveType(Long employeeId) {

        Optional<EmployeeLeaveCount> employeeLeaveCountOptional = employeeLeaveCountService.getLeaveCountByEmployeeId( employeeId );

        Map<String, Integer> leaveTypeIntegerMap = new HashMap<>();

        if(  employeeLeaveCountOptional.isPresent() ) {

            EmployeeLeaveCount employeeLeaveCount = employeeLeaveCountOptional.get();

                leaveTypeIntegerMap.put(LeaveType.SICK_LEAVE.toString(), (employeeLeaveCount.getMedicalLeaveCount() == null) ? 0 : employeeLeaveCount.getMedicalLeaveCount());
                leaveTypeIntegerMap.put(LeaveType.YEARLY_LEAVE.toString(), (employeeLeaveCount.getYearlyLeaveCount() == null) ? 0 : employeeLeaveCount.getYearlyLeaveCount());
                leaveTypeIntegerMap.put(LeaveType.CASUAL_LEAVE.toString(), (employeeLeaveCount.getCasualLeaveCount() == null) ? 0 : employeeLeaveCount.getCasualLeaveCount());
                leaveTypeIntegerMap.put(LeaveType.EARNED_LEAVE.toString(), (employeeLeaveCount.getEarnedLeaveCount() == null) ? 0 : employeeLeaveCount.getEarnedLeaveCount());
                leaveTypeIntegerMap.put(LeaveType.MEDICAL_LEAVE.toString(), (employeeLeaveCount.getMedicalLeaveCount() == null) ? 0 : employeeLeaveCount.getMedicalLeaveCount());
                leaveTypeIntegerMap.put(LeaveType.ABROAD_LEAVE.toString(), (employeeLeaveCount.getAbroadLeaveCount() == null) ? 0 : employeeLeaveCount.getAbroadLeaveCount());
                leaveTypeIntegerMap.put(LeaveType.AMUSEMENT_LEAVE.toString(), (employeeLeaveCount.getAmusementLeaveCount() == null) ? 0 : employeeLeaveCount.getAmusementLeaveCount());
                leaveTypeIntegerMap.put(LeaveType.UNPAID_LEAVE.toString(), (employeeLeaveCount.getUnpaidLeaveCount() == null) ? 0 : employeeLeaveCount.getUnpaidLeaveCount());
                leaveTypeIntegerMap.put(LeaveType.MATERNITY_LEAVE.toString(), (employeeLeaveCount.getMaternityLeaveCount() == null) ? 0 : employeeLeaveCount.getMaternityLeaveCount());
                leaveTypeIntegerMap.put(LeaveType.OTHER_LEAVE.toString(), (employeeLeaveCount.getOtherLeaveCount() == null) ? 0 : employeeLeaveCount.getOtherLeaveCount());
        }

        return leaveTypeIntegerMap;
    }
}
