package com.dgt.paribahanpool.leave.service;

import com.dgt.paribahanpool.leave.model.LeaveApplication;
import com.dgt.paribahanpool.leave.model.LeaveApplication_;
import com.dgt.paribahanpool.noc_application.model.NocApplication;
import com.dgt.paribahanpool.noc_application.model.NocApplication_;
import com.dgt.paribahanpool.workflow.model.StateActionMap;
import com.dgt.paribahanpool.workflow.model.StateActionMap_;
import com.dgt.paribahanpool.workflow.model.State_;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Set;

public class LeaveApplicationSpecification {

    public static Specification<LeaveApplication> distinctLeave(){

        return (root, query, criteriaBuilder) -> {

            query.distinct( true );
            return null;
        };
    }

    public static Specification<LeaveApplication> filterByAvailableActionInLeave( Set<Long> roleIdSet ){

        return ( root, query, criteriaBuilder ) -> {

            Root<StateActionMap> stateActionMapRoot = query.from( StateActionMap.class );

            Predicate joinTable = criteriaBuilder.equal( root.get( LeaveApplication_.STATE ).get( State_.ID ) , stateActionMapRoot.get( StateActionMap_.STATE ).get( State_.ID ) );
            Predicate roleIdIn = stateActionMapRoot.get( StateActionMap_.ROLE_ID ).in( roleIdSet );

            return criteriaBuilder.and( joinTable, roleIdIn );
        };
    }

    public static Specification<LeaveApplication> filterByLoggedInUser( Long id ){

        if( id == null ){
            return Specification.where( null );
        }

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( LeaveApplication_.CREATED_BY ), id );
    }

}
