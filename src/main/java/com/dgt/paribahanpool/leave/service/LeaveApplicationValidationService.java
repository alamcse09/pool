package com.dgt.paribahanpool.leave.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.employee.model.EmployeeLeaveCount;
import com.dgt.paribahanpool.employee.service.EmployeeLeaveCountService;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.LeaveType;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.enums.StateType;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.leave.model.LeaveApplication;
import com.dgt.paribahanpool.leave.model.LeaveApplicationAddRequest;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class LeaveApplicationValidationService {

    private final MvcUtil mvcUtil;
    private final LeaveApplicationService leaveApplicationService;
    private final StateActionMapService stateActionMapService;
    private final EmployeeLeaveCountService employeeLeaveCountService;

    public Boolean handleBindingResultForAddFormPost( Model model, BindingResult bindingResult, LeaveApplicationAddRequest leaveApplicationAddRequest ){

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "leaveApplicationAddRequest" )
                        .object( leaveApplicationAddRequest )
                        .title( "title.leave.add" )
                        .content( "leave-application/leave-application-add" )
                        .activeMenu( Menu.LEAVE_APPLICATION_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.LEAVE_APPLICATION_ADD } )
                        .build()
        );
    }

    @Transactional
    public RestValidationResult delete( Long id ) throws NotFoundException {

        Boolean exist = leaveApplicationService.existById( id );
        Optional<LeaveApplication> optionalLeaveApplication = leaveApplicationService.findById(id);
        if( !exist ){

            throw new NotFoundException( "validation.common.notfound" );
        }
        else if( optionalLeaveApplication.isPresent() ){

            LeaveApplication leaveApplication = optionalLeaveApplication.get();

            if( leaveApplication.getState() != null && leaveApplication.getState().getStateType() == StateType.POSITIVE_END ){

                throw new NotFoundException( "validation.common.already-approved" );
            }

        }

        return RestValidationResult.valid();
    }

    public Boolean view( RedirectAttributes redirectAttributes, Long id ) {

        Optional<LeaveApplication> leaveApplicationOptional = leaveApplicationService.findById( id );

        if( !leaveApplicationOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    @Transactional
    public Boolean takeAction( RedirectAttributes redirectAttributes, Long id, Long actionId, UserPrincipal loggedInUser ) {

        Optional<LeaveApplication> leaveApplicationOptional = leaveApplicationService.findById( id );

        if( !leaveApplicationOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }
        else{

            LeaveApplication leaveApplication = leaveApplicationOptional.get();
            State state = leaveApplication.getState();

            if( !stateActionMapService.actionAllowed( state.getId(), loggedInUser.getRoleIds(), actionId ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.common.action.not-allowed" );
                return false;
            }
        }

        return true;
    }

    public Boolean validLeaveApplicationPost(Model model, RedirectAttributes redirectAttributes, LeaveApplicationAddRequest leaveApplicationAddRequest, UserPrincipal loggedInUser) {

        Optional<EmployeeLeaveCount> employeeLeaveCountOptional = employeeLeaveCountService.getLeaveCountByEmployeeId(loggedInUser.getEmployee().getId());

        if( employeeLeaveCountOptional.isPresent() ){

            EmployeeLeaveCount employeeLeaveCount = employeeLeaveCountOptional.get();

            if (leaveApplicationAddRequest.getLeaveType() == LeaveType.SICK_LEAVE) {

                return validLeave( employeeLeaveCount.getMedicalLeaveCount(), leaveApplicationAddRequest.getTotalLeaveCount().intValue(), redirectAttributes );

            } else if (leaveApplicationAddRequest.getLeaveType() == LeaveType.YEARLY_LEAVE) {

                return validLeave( employeeLeaveCount.getYearlyLeaveCount(), leaveApplicationAddRequest.getTotalLeaveCount().intValue(), redirectAttributes  );

            } else if (leaveApplicationAddRequest.getLeaveType() == LeaveType.CASUAL_LEAVE) {

                return validLeave( employeeLeaveCount.getCasualLeaveCount(), leaveApplicationAddRequest.getTotalLeaveCount().intValue(), redirectAttributes  );
            } else if (leaveApplicationAddRequest.getLeaveType() == LeaveType.EARNED_LEAVE) {

                return validLeave( employeeLeaveCount.getEarnedLeaveCount(), leaveApplicationAddRequest.getTotalLeaveCount().intValue(), redirectAttributes  );
            } else if (leaveApplicationAddRequest.getLeaveType() == LeaveType.MEDICAL_LEAVE) {

                return validLeave( employeeLeaveCount.getMedicalLeaveCount(), leaveApplicationAddRequest.getTotalLeaveCount().intValue(), redirectAttributes  );
            } else if (leaveApplicationAddRequest.getLeaveType() == LeaveType.ABROAD_LEAVE) {

                return validLeave( employeeLeaveCount.getAbroadLeaveCount(), leaveApplicationAddRequest.getTotalLeaveCount().intValue(), redirectAttributes  );
            } else if (leaveApplicationAddRequest.getLeaveType() == LeaveType.AMUSEMENT_LEAVE) {

                return validLeave( employeeLeaveCount.getAmusementLeaveCount(), leaveApplicationAddRequest.getTotalLeaveCount().intValue(), redirectAttributes );
            } else if (leaveApplicationAddRequest.getLeaveType() == LeaveType.UNPAID_LEAVE) {

                return validLeave( employeeLeaveCount.getUnpaidLeaveCount(), leaveApplicationAddRequest.getTotalLeaveCount().intValue(), redirectAttributes );
            } else if (leaveApplicationAddRequest.getLeaveType() == LeaveType.MATERNITY_LEAVE) {

                return validLeave( employeeLeaveCount.getMaternityLeaveCount(), leaveApplicationAddRequest.getTotalLeaveCount().intValue(), redirectAttributes );
            } else if (leaveApplicationAddRequest.getLeaveType() == LeaveType.OTHER_LEAVE) {

                return validLeave( employeeLeaveCount.getOtherLeaveCount(), leaveApplicationAddRequest.getTotalLeaveCount().intValue(), redirectAttributes  );
            }
        }

        mvcUtil.addErrorMessage( redirectAttributes, "validation.leave.missing-leave-info" );
        return false;
    }

    private Boolean validLeave(Integer remainingLeavedays, Integer appliedDays, RedirectAttributes redirectAttributes) {

        if( remainingLeavedays - appliedDays <= 0 ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.leave.no-remaining-leave" );
            return false;
        }

        return true;
    }
}
