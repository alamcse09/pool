package com.dgt.paribahanpool.locale;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Component
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class LocalizeUtil {

    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;

    public String getMessageFromMessageSource( String key, String defaultText, HttpServletRequest request ){

        try{

            if( defaultText == null || defaultText.trim().length() == 0 )
                return getMessageFromMessageSource( key, request );
            return messageSource.getMessage( key, null, defaultText, localeResolver.resolveLocale( request ) );
        }
        catch ( NoSuchMessageException e ){

            return null;
        }
    }

    public String getMessageFromMessageSource( String key, HttpServletRequest request ){

        try{

            return messageSource.getMessage( key, null, localeResolver.resolveLocale( request ) );
        }
        catch ( NoSuchMessageException e ){

            return null;
        }
    }
}
