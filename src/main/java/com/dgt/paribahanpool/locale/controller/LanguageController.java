package com.dgt.paribahanpool.locale.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

@Slf4j
@Controller
@RequestMapping( "/language" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class LanguageController {

    private final LocaleResolver localeResolver;

    @GetMapping( "/change/{lang}" )
    public String changeLang(
            @PathVariable( "lang" ) String lang,
            HttpServletRequest request,
            HttpServletResponse response
    ){

        localeResolver.setLocale(request, response, Locale.forLanguageTag( lang ) );
        return "redirect:/";
    }
}
