package com.dgt.paribahanpool.locale.controller;

import com.dgt.paribahanpool.rest.RestResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

@Slf4j
@RestController
@RequestMapping( "/language" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired } )
public class LanguageRestController {

    private final LocaleResolver localeResolver;

    @PutMapping( value = "/{lang}" )
    public ResponseEntity<RestResponse<Object>> changeLanguage(

            @PathVariable( "lang" ) String lang,
            HttpServletRequest request,
            HttpServletResponse response
    ) {

        log.debug( "Language change rest controller called" );
        localeResolver.setLocale(request, response, Locale.forLanguageTag( lang ) );

        return ResponseEntity.ok().body( RestResponse.builder().success( true ).build() );
    }
}
