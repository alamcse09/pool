package com.dgt.paribahanpool.log.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.service.AuditLogModelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Controller
@RequestMapping( "/log" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class AuditLogController extends MVCController {

    private final AuditLogModelService auditLogModelService;

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.audit-log.search", content = "audit-log/audit-log-search", activeMenu = Menu.AUDIT_LOG_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.AUDIT_LOG_SEARCH } )
    public String search(

            HttpServletRequest request,
            Model model
    ){

        return viewRoot;
    }

    @GetMapping( "/search/{id}" )
    @TitleAndContent( title = "title.audit-log.search", content = "audit-log/audit-log-search", activeMenu = Menu.AUDIT_LOG_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.AUDIT_LOG_SEARCH } )
    public String search(

            HttpServletRequest request,
            Model model,
            @PathVariable( "id" ) Long userId
    ){

        auditLogModelService.searchByUser( model, userId );
        return viewRoot;
    }
}
