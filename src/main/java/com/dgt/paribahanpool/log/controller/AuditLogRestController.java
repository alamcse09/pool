package com.dgt.paribahanpool.log.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.log.model.AuditLogSearchResponse;
import com.dgt.paribahanpool.log.service.AuditLogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping( "/api/log" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class AuditLogRestController extends BaseRestController {

    private final AuditLogService auditLogService;

    @GetMapping( "/search" )
    public DataTablesOutput<AuditLogSearchResponse> search(
            DataTablesInput dataTablesInput
    ){

        return auditLogService.search( dataTablesInput, getLoggedInUser().getUser().getId() );
    }

    @GetMapping( "/search/{id}" )
    public DataTablesOutput<AuditLogSearchResponse> search(
            DataTablesInput dataTablesInput,
            @PathVariable( "id" ) Long userId
    ){

        return auditLogService.search( dataTablesInput, userId );
    }
}
