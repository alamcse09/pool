package com.dgt.paribahanpool.log.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(
        name = "audit_log",
        indexes = {
                @Index( name = "idx_aduit_log_user_id", columnList = "user_id" ),
                @Index( name = "idx_aduit_log_log_event", columnList = "log_event" )
        }
)
public class AuditLog {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "user_id" )
    private Long userId;

    @Column( name = "log_event" )
    private LogEvent logEvent;

    @Column( name = "timestamp" )
    private LocalDateTime timestamp;

    @Column( name = "ip_address" )
    private String ipAddress;

    @Column( name = "related_entity_id" )
    private Long relatedEntityId;

    @Column( name = "description" )
    private String description;
}
