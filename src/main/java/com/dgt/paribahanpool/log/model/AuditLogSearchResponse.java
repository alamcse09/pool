package com.dgt.paribahanpool.log.model;

import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
@Builder
public class AuditLogSearchResponse {

    private Long id;
    private Long userId;
    private LogEvent logEvent;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy_hhmma )
    private LocalDateTime timestamp;

    private String ipAddress;
    private Long relatedEntityId;
    private String description;
}
