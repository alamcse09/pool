package com.dgt.paribahanpool.log.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class AuditLogModelService {

    public void searchByUser(Model model, Long userId ){

        model.addAttribute( "userId", userId );
    }
}
