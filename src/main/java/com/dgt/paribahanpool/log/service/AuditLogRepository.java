package com.dgt.paribahanpool.log.service;

import com.dgt.paribahanpool.log.model.AuditLog;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface AuditLogRepository extends DataTablesRepository<AuditLog, Long> {
}
