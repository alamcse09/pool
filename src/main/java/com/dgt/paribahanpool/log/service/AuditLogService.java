package com.dgt.paribahanpool.log.service;

import com.dgt.paribahanpool.log.model.AuditLog;
import com.dgt.paribahanpool.log.model.AuditLogSearchResponse;
import com.dgt.paribahanpool.log.model.LogEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import static com.dgt.paribahanpool.log.service.AuditLogSpecification.filterByLoggedInUser;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class AuditLogService {

    private final AuditLogRepository auditLogRepository;

    private AuditLog save( AuditLog aduitLog ){

        return auditLogRepository.save( aduitLog );
    }

    public AuditLog save( Long loggedInUserId, LogEvent logEvent, String ip, Long relatedEntityId, String description ){

        AuditLog auditLog = new AuditLog();

        auditLog.setUserId( loggedInUserId );
        auditLog.setLogEvent( logEvent );
        auditLog.setIpAddress( ip );
        auditLog.setTimestamp(LocalDateTime.now() );
        auditLog.setDescription( description );
        auditLog.setRelatedEntityId( relatedEntityId );

        return save( auditLog );
    }

    public DataTablesOutput<AuditLogSearchResponse> search( DataTablesInput dataTablesInput, Long userId ) {

        return auditLogRepository.findAll( dataTablesInput, null, filterByLoggedInUser( userId ), this::getAuditLogSearchResponseFromAuditLog );
    }

    private AuditLogSearchResponse getAuditLogSearchResponseFromAuditLog( AuditLog auditLog ) {

        return AuditLogSearchResponse.builder()
                .id( auditLog.getId() )
                .logEvent( auditLog.getLogEvent() )
                .description( auditLog.getDescription() )
                .ipAddress( auditLog.getIpAddress() )
                .relatedEntityId( auditLog.getRelatedEntityId() )
                .timestamp( auditLog.getTimestamp() )
                .userId( auditLog.getUserId() )
                .build();

    }
}
