package com.dgt.paribahanpool.log.service;

import com.dgt.paribahanpool.log.model.AuditLog;
import com.dgt.paribahanpool.log.model.AuditLog_;
import org.springframework.data.jpa.domain.Specification;

public class AuditLogSpecification {

    public static Specification<AuditLog> filterByLoggedInUser( Long id ){

        if( id == null ){
            return Specification.where( null );
        }

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( AuditLog_.USER_ID ), id );
    }
}
