package com.dgt.paribahanpool.marine.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.marine.model.Marine;
import com.dgt.paribahanpool.marine.model.MarineAddRequest;
import com.dgt.paribahanpool.marine.service.MarineModelService;
import com.dgt.paribahanpool.marine.service.MarineValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/marine" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )

public class MarineController extends MVCController {

    private final MarineModelService marineModelService;
    private final MarineValidationService marineValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.marine.add", content = "marine/marine-add", activeMenu = Menu.MARINE_ADD)
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MARINE_ADD_FORM } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add marine form" );
        marineModelService.addMarine( model );
        return viewRoot;
    }

    @PostMapping( value = "/add" )
    public String addMarine(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid MarineAddRequest marineAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !marineValidationService.handleBindingResultForAddFormPost( model, bindingResult, marineAddRequest ) ) {

            return viewRoot;
        }

        if( marineValidationService.addMarinePost( redirectAttributes, marineAddRequest ) ) {

            Marine marine = marineModelService.addMarinePost( marineAddRequest, redirectAttributes );
            log( LogEvent.MARINE_ADDED, marine.getId(), "Marine Added", request );
        }

        return "redirect:/marine/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title="title.marine.search", content="marine/marine-search", activeMenu = Menu.MARINE_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MARINE_SEARCH })
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Marine Search page called" );
        return viewRoot;
    }

    @GetMapping( "/edit/{marineId}" )
    @TitleAndContent( title = "title.marine.edit", content = "marine/marine-add", activeMenu = Menu.MARINE_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MARINE_ADD_FORM } )
    public String edit(
        HttpServletRequest request,
        Model model,
        RedirectAttributes redirectAttributes,
        @PathVariable("marineId") Long marineId
    ){

        log.debug( "Rendering edit marine form" );

        if( marineValidationService.editMarine( redirectAttributes, marineId ) ){

            marineModelService.editMarine( model, marineId );
            return viewRoot;
        }

        return "redirect:/marine/search";
    }

}
