package com.dgt.paribahanpool.marine.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.marine.service.MarineServiceApplicationDispositionService;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.marine.model.MarineSearchResponse;
import com.dgt.paribahanpool.marine.service.MarineService;
import com.dgt.paribahanpool.marine.service.MarineValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@Slf4j
@RestController
@RequestMapping( "/api/marine" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class MarineRestController extends BaseRestController {

    private final MarineService marineService;
    private final MarineValidationService marineValidationService;
    private final MarineServiceApplicationDispositionService marineServiceApplicationDispositionService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<MarineSearchResponse> searchMarine(
            DataTablesInput dataTablesInput,
            HttpServletRequest request
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return marineService.searchForDatatable( dataTablesInput, request );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = marineValidationService.delete( id );
        if( restValidationResult.getSuccess() ){

            marineService.delete( id );
            log( LogEvent.MARINE_DELETE, id, "", request );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @PostMapping("/disposition/{id}")
    public RestResponse getMarineDisposition(
            @PathVariable( "id" ) Long id
    ) throws NotFoundException {

        RestValidationResult validationResult = marineValidationService.delete( id );

        if( validationResult.getSuccess() ) {

            marineServiceApplicationDispositionService.saveDispositionFromService( id );
            return RestResponse.builder().success( true ).message( "success.common.disposition.success" ).build();
        } else {

            return RestResponse.builder().success( false ).message( validationResult.getMessage() ).build();
        }

    }
}
