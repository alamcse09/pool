package com.dgt.paribahanpool.marine.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.marine.model.MarineServiceApplication;
import com.dgt.paribahanpool.marine.model.MarineServiceApplicationAddRequest;
import com.dgt.paribahanpool.marine.model.MarineServiceApplicationAdditionalData;
import com.dgt.paribahanpool.marine.service.MarineServiceApplicationDispositionService;
import com.dgt.paribahanpool.marine.service.MarineServiceApplicationModelService;
import com.dgt.paribahanpool.marine.service.MarineServiceApplicationValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/marine-service-application")
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class MarineServiceApplicationController extends MVCController {

    private final MarineServiceApplicationModelService marineServiceApplicationModelService;
    private final MarineServiceApplicationValidationService marineServiceApplicationValidationService;
    private final MarineServiceApplicationDispositionService marineServiceApplicationDispositionService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.marine-service-application.add", content = "marine/marine-service-application-add", activeMenu = Menu.MARINE_SERVICE_APPLICATION_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MARINE_SERVICE_APPLICATION_ADD } )
    public String addPage(
            Model model,
            HttpServletRequest request
    ){

        marineServiceApplicationModelService.addPage( model );
        return viewRoot;
    }

    @GetMapping( "/add/{marineId}" )
    @TitleAndContent( title = "title.marine-service-application.add", content = "marine/marine-service-application-add", activeMenu = Menu.MARINE_SERVICE_APPLICATION_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MARINE_SERVICE_APPLICATION_ADD } )
    public String addById(
            @PathVariable( "marineId" ) Long id,
            Model model
    ){

        marineServiceApplicationModelService.addById( model, id );
        return viewRoot;
    }

    @PostMapping( "/add" )
    public String addPost(
            Model model,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            @Valid MarineServiceApplicationAddRequest marineServiceApplicationAddRequest,
            BindingResult bindingResult

    ){

        if( !marineServiceApplicationValidationService.handleBindingResultFromAddPost( model, bindingResult, marineServiceApplicationAddRequest ) ){

            return viewRoot;
        }

        if( marineServiceApplicationValidationService.addPost( redirectAttributes, marineServiceApplicationAddRequest ) ){

            MarineServiceApplication marineServiceApplication = marineServiceApplicationModelService.addPost( marineServiceApplicationAddRequest, model );
            log( LogEvent.MARINE_SERVICE_APPLICATION_APPLIED, marineServiceApplication.getId(), "", request );
        }

        return "redirect:/marine-service-application/search";
    }

    @GetMapping( "/{id}")
    @TitleAndContent( title = "title.marine-service-application.view", content = "marine/marine-service-application-view", activeMenu = Menu.MARINE_SERVICE_APPLICATION_VIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MARINE_SERVICE_APPLICATION_VIEW } )
    public String view(
            Model model,
            HttpServletRequest request,
            @PathVariable( "id" ) Long id
    ){

        if( marineServiceApplicationValidationService.view( id ) ) {

            marineServiceApplicationModelService.view( model, id, getLoggedInUser() );
            return viewRoot;
        }
        else{

            return "redirect:/marine-service-application/search";
        }
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.marine-service-application.search", content = "marine/marine-service-application-search", activeMenu = Menu.MARINE_SERVICE_APPLICATION_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MARINE_SERVICE_APPLICATION_SEARCH } )
    public String search(
            Model model,
            HttpServletRequest request
    ){

        return viewRoot;
    }

    @PostMapping( "/take-action" )
    public String takeAction(
            @RequestParam( "id" ) Long id,
            @RequestParam( "action" ) Long actionId,
            @Valid MarineServiceApplicationAdditionalData marineServiceApplicationAdditionalData,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ) {

        if( marineServiceApplicationValidationService.takeActionValidation( redirectAttributes, id, actionId, getLoggedInUser(), marineServiceApplicationAdditionalData ) ){

            marineServiceApplicationModelService.takeAction( redirectAttributes, id, actionId, marineServiceApplicationAdditionalData, getLoggedInUser() );
            log( LogEvent.MARINE_SERVICE_APPLICATION_TAKE_ACTION, actionId, "Marine Service application Take action", request );
        }
        else{

            return "redirect:/marine-service-application/" + id;
        }

        return "redirect:/marine-service-application/search";
    }
}
