package com.dgt.paribahanpool.marine.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.marine.model.MarineServiceApplicationRestResponse;
import com.dgt.paribahanpool.marine.model.MarineServiceApplicationSearchResponse;
import com.dgt.paribahanpool.marine.service.*;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/marine-service-application" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class MarineServiceApplicationRestController extends BaseRestController {

    private final MarineServiceApplicationService marineServiceApplicationService;
    private final MarineServiceApplicationValidationService marineServiceApplicationValidationService;
    private final MarineValidationService marineValidationService;
    private final MarineService marineService;

    @GetMapping("/search")
    public DataTablesOutput<MarineServiceApplicationSearchResponse> search(
            DataTablesInput dataTablesInput
    ) {

        return marineServiceApplicationService.search(dataTablesInput);
    }

    @DeleteMapping("/{id}")
    public RestResponse delete(
            @PathVariable("id") Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = marineServiceApplicationValidationService.delete(id);

        if (restValidationResult.getSuccess()) {

            marineServiceApplicationService.delete( id );
            log( LogEvent.MARINE_SERVICE_APPLICATION_DELETED, id, "", request );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }else {
            return RestResponse.builder().success(false).message(restValidationResult.getMessage()).build();
        }
    }

    @GetMapping( "/engineNo/{number}" )
    public RestResponse getFromEngineNo(
            @PathVariable( "number" ) String number
    ) throws NotFoundException {

        RestValidationResult validationResult = marineValidationService.findByEngineNumber( number );

        if( validationResult.getSuccess() ) {

            MarineServiceApplicationRestResponse marineServiceApplicationRestResponse = marineService.marineServiceApplicationRestResponseByEngineNumber( number );
            return RestResponse.builder().success(true).payload( marineServiceApplicationRestResponse ).build();
        } else {

            return RestResponse.builder().success( false ).message( validationResult.getMessage() ).build();
        }

    }
}
