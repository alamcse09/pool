package com.dgt.paribahanpool.marine.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.marine.model.MarineServiceDispositionApplicationAdditionalData;
import com.dgt.paribahanpool.marine.service.MarineServiceApplicationDispositionModelService;
import com.dgt.paribahanpool.marine.service.MarineServiceApplicationDispositionValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/marine-service-disposition-application")
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class MarineServiceDispositionApplicationController extends MVCController {

    private final MarineServiceApplicationDispositionValidationService marineServiceApplicationDispositionValidationService;
    private final MarineServiceApplicationDispositionModelService marineServiceApplicationDispositionModelService;

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.marine-service-disposition-application.search", content = "marine/marine-service-disposition-application-search", activeMenu = Menu.MARINE_SERVICE_DISPOSITION_APPLICATION_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MARINE_SERVICE_DISPOSITION_APPLICATION_SEARCH } )
    public String search(
            Model model,
            HttpServletRequest request
    ){

        return viewRoot;
    }

    @GetMapping( "/{id}" )
    @TitleAndContent( title = "title.marine-service-application.disposition.view", content = "marine/marine-service-application-disposition-view", activeMenu = Menu.MARINE_SERVICE_APPLICATION_DISPOSITION_VIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MARINE_SERVICE_APPLICATION_DISPOSITION_VIEW } )
    public String viewDispotion(
            Model model,
            HttpServletRequest request,
            @PathVariable( "id" ) Long id
    ){

        if( marineServiceApplicationDispositionValidationService.view( id ) ) {

            marineServiceApplicationDispositionModelService.viewDispostion( model, id, getLoggedInUser(), request );
            return viewRoot;
        }
        else{

            return "redirect:/marine-service-disposition-application/search";
        }
    }


    @PostMapping( "/take-action" )
    public String takeAction(
            @RequestParam( "id" ) Long id,
            @RequestParam( "action" ) Long actionId,
            @Valid MarineServiceDispositionApplicationAdditionalData marineServiceDispositionApplicationAdditionalData,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ) {

        if( marineServiceApplicationDispositionValidationService.takeActionValidation( redirectAttributes, id, actionId, getLoggedInUser(), marineServiceDispositionApplicationAdditionalData ) ){

            marineServiceApplicationDispositionModelService.takeAction( redirectAttributes, id, actionId, marineServiceDispositionApplicationAdditionalData, getLoggedInUser() );
            log( LogEvent.MARINE_SERVICE_DISPOSITION_APPLICATION_TAKE_ACTION, actionId, "Marine Service Disposition Application Take action", request );
        }
        else{

            return "redirect:/marine-service-disposition-application/" + id;
        }

        return "redirect:/marine-service-disposition-application/search";
    }
}
