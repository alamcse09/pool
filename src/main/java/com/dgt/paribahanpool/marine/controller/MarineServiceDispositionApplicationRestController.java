package com.dgt.paribahanpool.marine.controller;

import com.dgt.paribahanpool.marine.model.MarineServiceDispositionSearchResponse;
import com.dgt.paribahanpool.marine.service.MarineServiceApplicationDispositionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/marine-service-dispostion-application" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class MarineServiceDispositionApplicationRestController {

    private final MarineServiceApplicationDispositionService marineServiceApplicationDispositionService;

    @GetMapping("/search")
    public DataTablesOutput<MarineServiceDispositionSearchResponse> search(
            DataTablesInput dataTablesInput,
            HttpServletRequest request
    ) {

        return marineServiceApplicationDispositionService.search(dataTablesInput, request);
    }
}
