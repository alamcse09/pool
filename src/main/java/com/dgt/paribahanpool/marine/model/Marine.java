package com.dgt.paribahanpool.marine.model;

import com.dgt.paribahanpool.driver.model.Driver;
import com.dgt.paribahanpool.enums.MarineFuelType;
import com.dgt.paribahanpool.enums.MarineType;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(
        name = "marine",
        uniqueConstraints = {
                @UniqueConstraint( columnNames = { "engine_number" }, name ="uk_marine_engine_number" )
        }
)
@ToString( exclude = { "marineServiceRecordSet" } )
@EqualsAndHashCode( exclude = { "marineServiceRecordSet" } )
public class Marine extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "model_year" )
    private Integer modelYear;

    @Column( name = "brand" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String brand;

    @Column( name = "color" )
    @Convert( converter = StringTrimConverter.class )
    private String color;


    @Column( name = "marine_type" )
    private MarineType marineType;

    @Column( name = "engine_number" )
    @Convert( converter = StringTrimConverter.class )
    private String engineNo;

    @Column( name = "engine_quantity" )
    private Integer engineQuantity;

    @Column( name = "horsepower" )
    private Double horsepower;

    @Column( name = "fuel_type" )
    private MarineFuelType fuelType;

    @Column( name = "seat_capacity" )
    private Integer seatCapacity;

    @Column( name = "sales_org_name" )
    @Convert( converter = StringTrimConverter.class )
    private String salesOrganizationName;

    @Column( name = "requisition_details", columnDefinition = "TEXT" )
    @Convert( converter = StringTrimConverter.class )
    private String requisitionDetails;

    @Column( name = "boat_details" )
    @Convert( converter = StringTrimConverter.class )
    private String boatDetails;

    @Column( name = "allocation_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate allocationDate;

    @Column( name = "buy_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate buyDate;

    @Column( name = "cost" )
    private Double cost;

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinTable(
            name = "marine_marine_service_record_map",
            joinColumns = @JoinColumn( name = "marine_id", foreignKey = @ForeignKey( name = "fk_marine_service_record_marine_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "marine_service_record_id", foreignKey = @ForeignKey( name = "fk_marine_id_marine_service_record_map_service_record_id" ) )
    )
    private Set<MarineServiceRecord> marineServiceRecordSet = new HashSet<>();

    @Where( clause = "is_deleted = false" )
    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "driver_id", foreignKey = @ForeignKey( name = "fk_marine_driver_id" ) )
    private Driver driver;

    @Where( clause = "is_deleted = false" )
    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "user_id", foreignKey = @ForeignKey( name = "fk_marine_user_id" ) )
    private User user;
}
