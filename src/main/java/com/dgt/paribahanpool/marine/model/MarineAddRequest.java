package com.dgt.paribahanpool.marine.model;

import com.dgt.paribahanpool.enums.MarineFuelType;
import com.dgt.paribahanpool.enums.MarineType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.util.List;

@Data
public class MarineAddRequest {

    public Long id;

    @NotNull( message = "{validation.common.required}")
    @Min( value = 1900, message = "{validation.common.min}" )
    @Max( value = 2100, message = "{validation.common.max}" )
    public Integer modelYear;

//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    @NotNull( message = "{validation.common.required}")
    private String brand;

    private String color;

    private MarineType marineType;

//    @Pattern(regexp = "^[a-zA-Z0-9]+", message = "{validation.common.alphanumeric}")
    @NotNull( message = "{validation.common.required}")
    private String engineNo;


    private Integer engineQuantity;

    private Double horsepower;

    private MarineFuelType fuelType;

    private Integer seatCapacity;

    private String salesOrganizationName;

    private String requisitionDetails;

    @PastOrPresent( message = "{validation.common.date.past_or_present}" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    @NotNull( message = "{validation.common.required}")
    private LocalDate allocationDate;

    private String boatDetails;

    private Double cost;

    @PastOrPresent( message = "{validation.common.date.past_or_present}" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate buyDate;

    @Valid
    List<MarineServiceInfoAddRequest> marineServiceInfoAddRequests;
}
