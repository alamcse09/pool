package com.dgt.paribahanpool.marine.model;

import com.dgt.paribahanpool.enums.MarineFuelType;
import com.dgt.paribahanpool.enums.MarineType;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class MarineSearchResponse {

    private Long id;
    private Integer modelYear;
    private String brand;
    private String color;
    private String marineType;
    private String engineNo;
    private Integer engineQuantity;
    private Double horsepower;
    private String fuelType;
    private Integer seatCapacity;
    private String salesOrganizationName;
    private String marineOwner;
    private String allocationDate;

    private List<MarineServiceRecordSearchResponse> marineServiceRecords;
}
