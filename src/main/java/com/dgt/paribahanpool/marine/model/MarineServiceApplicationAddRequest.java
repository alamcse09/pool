package com.dgt.paribahanpool.marine.model;

import com.dgt.paribahanpool.enums.ServiceCategory;
import com.dgt.paribahanpool.enums.ServiceType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class MarineServiceApplicationAddRequest {

    private Long id;

    private Long marineId;

    private Double cost;

    private String userName;

    private String userDesignation;

    private String userWorkingPlace;

    private String userMobileNumber;

    private String driverName;

    private String driverMobileNumber;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate serviceDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate applicationDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.timeFormat_hour_min_ampm )
    private LocalTime serviceTime;

    private String engineNo;

    private ServiceCategory serviceCategory;

    private ServiceType serviceType;
}
