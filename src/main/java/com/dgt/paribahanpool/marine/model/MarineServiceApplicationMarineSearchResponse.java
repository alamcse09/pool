package com.dgt.paribahanpool.marine.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MarineServiceApplicationMarineSearchResponse {

    private String engineNo;
    private Double horsepower;
    private String brand;
}
