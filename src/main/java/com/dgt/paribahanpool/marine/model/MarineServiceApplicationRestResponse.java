package com.dgt.paribahanpool.marine.model;

import lombok.Data;

@Data
public class MarineServiceApplicationRestResponse {

    private Long marineId;
    private String engineNo = "";
    private String userName = "";
    private String userDesignation = "";
    private String userWorkingPlace = "";
    private String userMobileNumber = "";
    private String driverName = "";
    private String driverMobileNumber = "";
}
