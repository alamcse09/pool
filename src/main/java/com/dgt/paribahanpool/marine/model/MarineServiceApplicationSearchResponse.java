package com.dgt.paribahanpool.marine.model;

import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.workflow.model.StateResponse;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Builder
public class MarineServiceApplicationSearchResponse {

    private Long id;
    private Double cost;
    private String userName;
    private String userDesignation;
    private String userWorkingPlace;
    private String driverName;
    private String driverMobileNumber;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate serviceDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.timeFormat_hour_min_ampm )
    private LocalTime serviceTime;

    private StateResponse stateResponse;

    private MarineServiceApplicationMarineSearchResponse marine;
}
