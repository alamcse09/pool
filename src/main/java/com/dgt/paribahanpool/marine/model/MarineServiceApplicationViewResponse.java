package com.dgt.paribahanpool.marine.model;

import com.dgt.paribahanpool.committee.model.CommitteeResponse;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.util.NameResponse;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Data
@Builder
public class MarineServiceApplicationViewResponse {

    private Long id;
    private String engineNo;
    private String marineUserName;
    private String marineUserDesignation;
    private String marineUserWorkingPlace;
    private String marineUserMobileNumber;
    private String driverName;
    private String driverMobileNumber;
    private String serviceDate;
    private LocalTime serviceTime;
    private Double cost;
    private String appliedDate;

    private List<UserResponse> showEmployeeList;
    private Long actionTakerMemberId;

    private List<CommitteeResponse> committeeResponseList;
    private NameResponse actionTakerResponse;
    private List<DocumentMetadata> committeeReportDocuments;

    private DocumentMetadata signMetaData;
}
