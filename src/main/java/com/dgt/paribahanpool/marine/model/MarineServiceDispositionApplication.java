package com.dgt.paribahanpool.marine.model;

import com.dgt.paribahanpool.committee.model.Committee;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.WorkflowEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table( name = "marine_service_disposition_application" )
@SQLDelete( sql = "UPDATE marine_service_disposition_application SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class MarineServiceDispositionApplication extends AuditableEntity implements WorkflowEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "service_date" )
    private LocalDate serviceDate;

    @Column( name = "service_time" )
    private LocalTime serviceTime;

    @Column( name = "cost" )
    private Double cost;

    @Column( name = "user_name" )
    private String userName;

    @Column( name = "user_designation" )
    private String userDesignation;

    @Column( name = "user_working_place" )
    private String userWorkingPlace;

    @Column( name = "user_mobile_number" )
    private String userMobileNumber;

    @Column( name = "driver_name" )
    private String driverName;

    @Column( name = "driver_mobile_number" )
    private String driverMobileNumber;

    @Column( name = "investigation_report_group_id" )
    private Long investigationReportGroupId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne
    @JoinColumn( name = "marine_id", foreignKey = @ForeignKey( name = "fk_marine_service_disposition_application_marine_id" ) )
    private Marine marine;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne
    @JoinColumn( name = "state_id", foreignKey = @ForeignKey( name = "fk_marine_service_disposition_application_state_id" ) )
    private State state;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "committee_action_taker_id", foreignKey = @ForeignKey( name = "fk_marine_service_disposition_app_user_id" ) )
    private User actionTakingUser;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinTable(
            name = "marine_service_disposition_app_committee_member_map",
            joinColumns = @JoinColumn( name = "marine_service_disposition_id", foreignKey = @ForeignKey( name = "fk_committee_member_marine_service_disposition_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "committee_member_id", foreignKey = @ForeignKey( name = "fk_marine_service_disposition_id_committee_member_map_member_id" ) )
    )
    private Set<Committee> committeeMemberSet = new HashSet<>();
}
