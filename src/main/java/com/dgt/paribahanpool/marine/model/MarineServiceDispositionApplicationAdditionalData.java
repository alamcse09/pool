package com.dgt.paribahanpool.marine.model;

import com.dgt.paribahanpool.committee.model.CommitteeAddRequest;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.workflow.model.WorkflowAdditionalData;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;

@Data
public class MarineServiceDispositionApplicationAdditionalData implements WorkflowAdditionalData {

    private List<CommitteeAddRequest> committeeAddRequestList;
    private Long actionTakerMemberId;

    private transient MultipartFile[] committeeInvestigationReportFiles;
    private List<DocumentMetadata> committeeInvestigationReportDocuments = Collections.EMPTY_LIST;
}
