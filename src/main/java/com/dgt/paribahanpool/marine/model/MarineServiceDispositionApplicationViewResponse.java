package com.dgt.paribahanpool.marine.model;

import com.dgt.paribahanpool.committee.model.CommitteeResponse;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.MarineFuelType;
import com.dgt.paribahanpool.enums.MarineType;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.util.NameResponse;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class MarineServiceDispositionApplicationViewResponse {

    private Long id;
    private Integer modelYear;
    private String brand;
    private String color;
    private String marineType;
    private String engineNo;
    private Integer engineQuantity;
    private Double horsepower;
    private String fuelType;
    private Integer seatCapacity;
    private String salesOrganizationName;
    private String requisitionDetails;
    private String allocationDate;

    private List<UserResponse> showEmployeeList;
    private Long actionTakerMemberId;

    private List<CommitteeResponse> committeeResponseList;
    private NameResponse actionTakerResponse;
    private List<DocumentMetadata> committeeReportDocuments;
}
