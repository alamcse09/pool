package com.dgt.paribahanpool.marine.model;

import com.dgt.paribahanpool.workflow.model.StateResponse;
import lombok.Data;

@Data
public class MarineServiceDispositionSearchResponse {

    private Long id;

    private MarineSearchResponse marine;

    private StateResponse stateResponse;
}
