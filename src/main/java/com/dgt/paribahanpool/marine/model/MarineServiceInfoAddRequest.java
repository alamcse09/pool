package com.dgt.paribahanpool.marine.model;

import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;

@Data
public class MarineServiceInfoAddRequest {

    private Long id;

    @FutureOrPresent( message = "{validation.common.date.future_or_present}" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    @NotNull( message = "{validation.common.required}" )
    private LocalDate servicingDate;
    
    @NotNull( message = "{validation.common.required}" )
    private Double servicingCost;

    @NotNull( message = "{validation.common.required}" )
    private String servicingVendorName;
}
