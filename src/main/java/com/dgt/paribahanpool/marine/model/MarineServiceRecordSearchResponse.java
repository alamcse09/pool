package com.dgt.paribahanpool.marine.model;

import lombok.Data;

@Data
public class MarineServiceRecordSearchResponse {

    private String servicingDate;
    private Double servicingCost;
    private String servicingVendorName;
}
