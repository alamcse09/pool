package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.marine.model.Marine;
import com.dgt.paribahanpool.marine.model.MarineAddRequest;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class MarineModelService {

    private final MarineService marineService;
    private final MvcUtil mvcUtil;

    public void addMarine( Model model ){

        model.addAttribute("marineAddRequest", new MarineAddRequest());
    }

    public Marine addMarinePost( MarineAddRequest marineAddRequest, RedirectAttributes redirectAttributes ) {

        Marine marine = marineService.save( marineAddRequest );

        if( marineAddRequest.getId() != null ){

            mvcUtil.addSuccessMessage( redirectAttributes, "success.marine.edit" );
        }
        else {

            mvcUtil.addSuccessMessage( redirectAttributes, "success.marine.add" );
        }

        return marine;
    }

    @Transactional
    public void editMarine( Model model, Long marineId ){

        Optional<Marine> marine = marineService.findById( marineId );
        if(marine.isPresent()){

            MarineAddRequest marineAddRequest = marineService.getMarineAddRequestFromMarine( marine.get() );
            model.addAttribute( "marineAddRequest", marineAddRequest );
        }
    }
}
