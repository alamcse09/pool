package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.marine.model.Marine;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarineRepository extends DataTablesRepository<Marine,Long>, JpaRepository<Marine,Long> {
}
