package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.driver.model.Driver;
import com.dgt.paribahanpool.enums.FuelType;
import com.dgt.paribahanpool.enums.MarineFuelType;
import com.dgt.paribahanpool.enums.MarineType;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.locale.LocalizeUtil;
import com.dgt.paribahanpool.marine.model.*;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class MarineService {

    private final MarineRepository marineRepository;
    private final MarineServiceRecordService marineServiceRecordService;
    private final LocalizeUtil localizeUtil;

    public Marine save( Marine marine ){

        return marineRepository.save( marine );
    }

    @Transactional
    public Marine save( MarineAddRequest marineAddRequest ){

        Marine marine = new Marine();

        if( marineAddRequest.getId() != null ){

            marine = findById( marineAddRequest.getId() ).get();
        }

        marine = getMarineFromMarineAddRequest( marine, marineAddRequest );

        return save( marine );
    }

    public void delete( Long id ) {

        Optional<Marine> marineOptional = findById( id );

        if( marineOptional.isPresent() ) {

            Marine marine = marineOptional.get();
            marine.setIsDeleted( true );
            save( marine );
        }
    }

    public Optional<Marine> findById(Long id ){

        return marineRepository.findById( id );
    }

    public Boolean existById( Long id ){

        return marineRepository.existsById( id );
    }

    public Marine getMarineFromMarineAddRequest( Marine marine, MarineAddRequest marineAddRequest ){

        marine.setModelYear( marineAddRequest.getModelYear() );
        marine.setBrand( marineAddRequest.getBrand() );
        marine.setColor( marineAddRequest.getColor() );
        marine.setMarineType( marineAddRequest.getMarineType() );
        marine.setEngineNo( marineAddRequest.getEngineNo() );
        marine.setEngineQuantity( marineAddRequest.getEngineQuantity() );
        marine.setHorsepower( marineAddRequest.getHorsepower() );
        marine.setFuelType( marineAddRequest.getFuelType() );
        marine.setSeatCapacity( marineAddRequest.getSeatCapacity() );
        marine.setSalesOrganizationName( marineAddRequest.getSalesOrganizationName() );
        marine.setRequisitionDetails( marineAddRequest.getRequisitionDetails() );
        marine.setAllocationDate( marineAddRequest.getAllocationDate() );
        marine.setBoatDetails( marineAddRequest.getBoatDetails() );
        marine.setBuyDate( marineAddRequest.getBuyDate() );
        marine.setCost( marineAddRequest.getCost() );

        if( marineAddRequest.getMarineServiceInfoAddRequests() != null ) {

            Set<MarineServiceRecord> marineServiceRecordSet =
                    marineAddRequest.getMarineServiceInfoAddRequests()
                            .stream()
                            .map( marineServiceInfoAddRequest -> getMarineServiceRecordFromMarineServiceInfoRequest( marineServiceInfoAddRequest, marine ) )
                            .filter( marineServiceRecord -> marineServiceRecord != null )
                            .collect( Collectors.toSet() );

            marine.getMarineServiceRecordSet().clear();
            marine.getMarineServiceRecordSet().addAll( marineServiceRecordSet );
        }

        return marine;
    }

    public MarineServiceRecord getMarineServiceRecordFromMarineServiceInfoRequest( MarineServiceInfoAddRequest marineServiceInfoAddRequest, Marine marine ){

        MarineServiceRecord marineServiceRecord = new MarineServiceRecord();

        if( marineServiceInfoAddRequest.getId() != null && marineServiceInfoAddRequest.getId() > 0 ){

            Optional<MarineServiceRecord> marineServiceRecordOptional = marineServiceRecordService.findById( marineServiceInfoAddRequest.getId() );
            if( marineServiceRecordOptional.isPresent() ){

                marineServiceRecord = marineServiceRecordOptional.get();
            }
            else{
                return null;
            }
        }

        marineServiceRecord.setServicingDate( marineServiceInfoAddRequest.getServicingDate() );
        marineServiceRecord.setServicingCost( marineServiceInfoAddRequest.getServicingCost() );
        marineServiceRecord.setServicingVendorName( marineServiceInfoAddRequest.getServicingVendorName() );

        return marineServiceRecord;
    }

    public MarineAddRequest getMarineAddRequestFromMarine( Marine marine ){

        MarineAddRequest marineAddRequest = new MarineAddRequest();

        marineAddRequest.setId( marine.getId() );
        marineAddRequest.setModelYear( marine.getModelYear() );
        marineAddRequest.setBrand( marine.getBrand() );
        marineAddRequest.setColor( marine.getColor() );
        marineAddRequest.setMarineType( marine.getMarineType() );
        marineAddRequest.setEngineNo( marine.getEngineNo() );
        marineAddRequest.setEngineQuantity( marine.getEngineQuantity() );
        marineAddRequest.setHorsepower( marine.getHorsepower() );
        marineAddRequest.setFuelType( marine.getFuelType() );
        marineAddRequest.setSeatCapacity( marine.getSeatCapacity() );
        marineAddRequest.setSalesOrganizationName( marine.getSalesOrganizationName() );
        marineAddRequest.setRequisitionDetails( marine.getRequisitionDetails() );
        marineAddRequest.setAllocationDate( marine.getAllocationDate() );

        if( marine.getMarineServiceRecordSet() != null ) {

            List<MarineServiceInfoAddRequest> marineServiceInfoAddRequests =
                    marine.getMarineServiceRecordSet()
                            .stream()
                            .map( marineServiceRecord -> getMarineServiceInfoRequestFromMarineServiceRecord( marineServiceRecord, marineAddRequest ) )
                            .collect( Collectors.toList() );

            marineAddRequest.setMarineServiceInfoAddRequests( marineServiceInfoAddRequests );
        }

        return marineAddRequest;

    }

    public MarineServiceInfoAddRequest getMarineServiceInfoRequestFromMarineServiceRecord( MarineServiceRecord marineServiceRecord, MarineAddRequest marineAddRequest ){

        MarineServiceInfoAddRequest marineServiceInfoAddRequest = new MarineServiceInfoAddRequest();

        marineServiceInfoAddRequest.setId( marineServiceRecord.getId() );
        marineServiceInfoAddRequest.setServicingDate( marineServiceRecord.getServicingDate() );
        marineServiceInfoAddRequest.setServicingCost( marineServiceRecord.getServicingCost() );
        marineServiceInfoAddRequest.setServicingVendorName( marineServiceRecord.getServicingVendorName() );

        return marineServiceInfoAddRequest;
    }

    public MarineServiceRecordSearchResponse getMarineServiceRecordSearchResponseFromMarine( MarineServiceRecord marineServiceRecord ){

        MarineServiceRecordSearchResponse marineServiceRecordSearchResponse = new MarineServiceRecordSearchResponse();

        marineServiceRecordSearchResponse.setServicingCost( marineServiceRecord.getServicingCost() );
        marineServiceRecordSearchResponse.setServicingDate( marineServiceRecord.getServicingDate().format( DateTimeFormatter.ofPattern( DateTimeFormatPattern.dateFormat_slash_ddMMyyyy ) ) );
        marineServiceRecordSearchResponse.setServicingVendorName( marineServiceRecord.getServicingVendorName() );

        return marineServiceRecordSearchResponse;
    }

    public MarineSearchResponse getMarineSearchResponseFromMarine( Marine marine, HttpServletRequest request  ){

        MarineSearchResponse marineSearchResponse = new MarineSearchResponse();

        marineSearchResponse.setId( marine.getId() );
        marineSearchResponse.setModelYear( marine.getModelYear() );
        marineSearchResponse.setBrand( marine.getBrand() );
        marineSearchResponse.setColor( marine.getColor() );
        marineSearchResponse.setMarineType( localizeUtil.getMessageFromMessageSource( MarineType.getLabel( marine.getMarineType() ), request ) );
        marineSearchResponse.setEngineNo( marine.getEngineNo() );
        marineSearchResponse.setEngineQuantity( marine.getEngineQuantity() );
        marineSearchResponse.setHorsepower( marine.getHorsepower() );
        marineSearchResponse.setFuelType(  localizeUtil.getMessageFromMessageSource( MarineFuelType.getLabel( marine.getFuelType() ), request  ) );
        marineSearchResponse.setSeatCapacity( marine.getSeatCapacity() );
        marineSearchResponse.setSalesOrganizationName( marine.getSalesOrganizationName() );
        marineSearchResponse.setMarineOwner( marine.getRequisitionDetails() );
        marineSearchResponse.setAllocationDate( marine.getAllocationDate().format( DateTimeFormatter.ofPattern("dd/MM/yyyy") ) );

        if( marine.getMarineServiceRecordSet() != null ){

            List<MarineServiceRecordSearchResponse> marineServiceRecordSearchResponseList =
                    marine.getMarineServiceRecordSet()
                            .stream()
                            .map( this::getMarineServiceRecordSearchResponseFromMarine )
                            .collect( Collectors.toList() );

            marineSearchResponse.setMarineServiceRecords( marineServiceRecordSearchResponseList );
        }

        return marineSearchResponse;
    }

    public DataTablesOutput<MarineSearchResponse> searchForDatatable(DataTablesInput dataTablesInput, HttpServletRequest request) {

        Specification<Marine> filterByIsDeleted = MarineSpecification.filterByIsDeleted( false );
        return marineRepository.findAll( dataTablesInput, null, filterByIsDeleted, marine -> getMarineSearchResponseFromMarine( marine, request ) );
    }

    public List<Marine> getMarineByEngineNo(String engineNo ){

        Specification<Marine> marineSpecification = MarineSpecification.filterByEngineNo( engineNo );

        return marineRepository.findAll( marineSpecification );
    }

    public Marine getReference(Long marineId) {

        return marineRepository.getById( marineId );
    }

    @Transactional
    public MarineServiceApplicationRestResponse marineServiceApplicationRestResponseByEngineNumber(String engineNumber ) {

        MarineServiceApplicationRestResponse marineServiceApplicationRestResponse = new MarineServiceApplicationRestResponse();

        List<Marine> marineList = getMarineByEngineNo(engineNumber);

        if( marineList.size()>0 ) {

            marineServiceApplicationRestResponse.setMarineId( marineList.get(0).getId() );
            marineServiceApplicationRestResponse.setEngineNo( engineNumber );

            User user = marineList.get(0).getUser();

            if( user !=null ) {

                if(user.getUserType().equals( UserType.PUBLIC)) {

                    marineServiceApplicationRestResponse.setUserName( user.getPublicUserInfo().getName() );
                    marineServiceApplicationRestResponse.setUserDesignation( user.getPublicUserInfo().getDesignation() );
                    marineServiceApplicationRestResponse.setUserWorkingPlace( user.getPublicUserInfo().getWorkplace() );
                    marineServiceApplicationRestResponse.setUserMobileNumber( user.getPublicUserInfo().getPhoneNo() );
                } else {

                    marineServiceApplicationRestResponse.setUserName( user.getEmployee().getName() );
                    marineServiceApplicationRestResponse.setUserDesignation( user.getEmployee().getDesignation().getNameEn() );
                    marineServiceApplicationRestResponse.setUserWorkingPlace( user.getEmployee().getPresentAddress() );
                    marineServiceApplicationRestResponse.setUserMobileNumber( user.getEmployee().getMobileNumber() );
                }
            }

            Driver driver = marineList.get(0).getDriver();

            if( driver != null ) {

                marineServiceApplicationRestResponse.setDriverName( driver.getName() );
                marineServiceApplicationRestResponse.setDriverMobileNumber( driver.getPhoneNumber() );
            }

        }

        return marineServiceApplicationRestResponse;
    }

    public Long getNumberOfMarine() {

        return marineRepository.count();
    }
}
