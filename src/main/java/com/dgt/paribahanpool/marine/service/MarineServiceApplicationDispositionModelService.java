package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.committee.model.CommitteeResponse;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.enums.MarineFuelType;
import com.dgt.paribahanpool.enums.MarineType;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.locale.LocalizeUtil;
import com.dgt.paribahanpool.marine.model.MarineServiceDispositionApplication;
import com.dgt.paribahanpool.marine.model.MarineServiceDispositionApplicationAdditionalData;
import com.dgt.paribahanpool.marine.model.MarineServiceDispositionApplicationViewResponse;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.util.NameResponse;
import com.dgt.paribahanpool.workflow.model.StateActionMapResponse;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class MarineServiceApplicationDispositionModelService {

    private final MvcUtil mvcUtil;
    private final DocumentService documentService;
    private final StateActionMapService stateActionMapService;
    private final UserService userService;
    private final MarineServiceApplicationDispositionService marineServiceApplicationDispositionService;
    private final LocalizeUtil localizeUtil;

    @Transactional
    public void viewDispostion(Model model, Long id, UserPrincipal loggedInUser, HttpServletRequest request ) {

        Optional<MarineServiceDispositionApplication> marineServiceDispositionApplicationOptional = marineServiceApplicationDispositionService.findById( id );

        if( marineServiceDispositionApplicationOptional.isPresent() ){

            MarineServiceDispositionApplication marineServiceDispositionApplication = marineServiceDispositionApplicationOptional.get();

            if( marineServiceDispositionApplication.getState() != null ){

                List<StateActionMapResponse> stateActionMapList = stateActionMapService.findStateActionMapResponseListByCurrentStateAndRoleIdSet( marineServiceDispositionApplication.getState().getId(), loggedInUser.getRoleIds() );
                model.addAttribute( "stateActionList", stateActionMapList );
            }

            List<UserResponse> userResponseList = userService.getUserResponseList( UserType.SYSTEM_USER );
            User user = marineServiceDispositionApplication.getActionTakingUser();

            List<CommitteeResponse> committeeResponseList = marineServiceDispositionApplication.getCommitteeMemberSet()
                    .stream()
                    .map(
                            committee -> CommitteeResponse.builder()
                                    .nameEn( committee.getNameEn() )
                                    .nameBn( committee.getName() )
                                    .build()
                    )
                    .collect(Collectors.toList() );

            NameResponse actionTakerResponse = NameResponse.builder()
                    .nameBn( user != null? user.getEmployee().getName(): "" )
                    .nameEn( user != null? user.getEmployee().getNameEn(): "" )
                    .build();

            List<DocumentMetadata> committeeReportMetadataList = documentService.findDocumentMetaDataListByGroupId( marineServiceDispositionApplication.getInvestigationReportGroupId() );

            MarineServiceDispositionApplicationViewResponse marineServiceDispositionApplicationViewResponse = MarineServiceDispositionApplicationViewResponse.builder()
                    .id( marineServiceDispositionApplication.getId() )
                    .modelYear( marineServiceDispositionApplication.getMarine().getModelYear() )
                    .brand( marineServiceDispositionApplication.getMarine().getBrand() )
                    .color( marineServiceDispositionApplication.getMarine().getColor() )
                    .marineType( localizeUtil.getMessageFromMessageSource( MarineType.getLabel(marineServiceDispositionApplication.getMarine().getMarineType() ), request ) )
                    .engineNo( marineServiceDispositionApplication.getMarine().getEngineNo() )
                    .engineQuantity( marineServiceDispositionApplication.getMarine().getEngineQuantity() )
                    .horsepower( marineServiceDispositionApplication.getMarine().getHorsepower() )
                    .fuelType( localizeUtil.getMessageFromMessageSource(MarineFuelType.getLabel(marineServiceDispositionApplication.getMarine().getFuelType() ), request ) )
                    .seatCapacity( marineServiceDispositionApplication.getMarine().getSeatCapacity() )
                    .salesOrganizationName( marineServiceDispositionApplication.getMarine().getSalesOrganizationName() )
                    .requisitionDetails( marineServiceDispositionApplication.getMarine().getRequisitionDetails() )
                    .allocationDate( marineServiceDispositionApplication.getMarine().getAllocationDate().format( DateTimeFormatter.ofPattern("dd/MM/yyyy") ) )
                    .showEmployeeList( userResponseList )
                    .committeeResponseList(committeeResponseList)
                    .actionTakerResponse(actionTakerResponse)
                    .committeeReportDocuments(committeeReportMetadataList)
                    .actionTakerMemberId( marineServiceDispositionApplication.getActionTakingUser() != null? marineServiceDispositionApplication.getActionTakingUser().getId(): -1L )
                    .build();

            model.addAttribute( "marineServiceDispositionApplicationViewResponse", marineServiceDispositionApplicationViewResponse );
        }
    }

    public void takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, MarineServiceDispositionApplicationAdditionalData marineServiceDispositionApplicationAdditionalData, UserPrincipal loggedInUser) {

        marineServiceApplicationDispositionService.takeAction( id, actionId, marineServiceDispositionApplicationAdditionalData, loggedInUser );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.common.action.success" );
    }
}
