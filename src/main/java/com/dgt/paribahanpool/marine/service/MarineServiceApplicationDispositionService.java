package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.committee.model.Committee;
import com.dgt.paribahanpool.committee.service.CommitteeService;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.marine.model.*;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.workflow.model.*;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class MarineServiceApplicationDispositionService implements WorkflowService<MarineServiceDispositionApplication> {

    private final MarineServiceApplicationService marineServiceApplicationService;
    private final MarineServiceDispositionApplicationRepository marineServiceDispositionApplicationRepository;
    private final MarineService marineService;
    private final StateService stateService;
    private final StateActionMapService stateActionMapService;
    private final CommitteeService committeeService;
    private final UserService userService;
    private final DocumentService documentService;

    @Override
    public Optional<MarineServiceDispositionApplication> findById(Long id) {

        return marineServiceDispositionApplicationRepository.findById( id );
    }

    @Override
    public MarineServiceDispositionApplication save(MarineServiceDispositionApplication marineServiceDispositionApplication) {

        return marineServiceDispositionApplicationRepository.save( marineServiceDispositionApplication );
    }

    @Transactional
    public void saveDispositionFromService(Long id) {

        Optional<MarineServiceApplication> marineServiceApplicationOptional = marineServiceApplicationService.findById( id );

        MarineServiceDispositionApplication marineServiceDispositionApplication = new MarineServiceDispositionApplication();

        marineServiceDispositionApplication.setMarine( marineServiceApplicationOptional.get().getMarine() );

        State state = stateService.findStateReferenceById(AppConstants.MARINE_SERVICE_DISPOSITION_APP_INIT_STATE_ID );
        marineServiceDispositionApplication.setState( state );

        save( marineServiceDispositionApplication );
    }

    public DataTablesOutput<MarineServiceDispositionSearchResponse> search(DataTablesInput dataTablesInput, HttpServletRequest request) {

        return marineServiceDispositionApplicationRepository.findAll( dataTablesInput, marine -> getMarineDispostionSearchResponseFromMarineDisposition(marine, request) );
    }

    private MarineServiceDispositionSearchResponse getMarineDispostionSearchResponseFromMarineDisposition(MarineServiceDispositionApplication marineServiceDispositionApplication, HttpServletRequest request ) {

        if( marineServiceDispositionApplication.getMarine() != null ){

            MarineServiceDispositionSearchResponse marineServiceDispositionSearchResponse = new MarineServiceDispositionSearchResponse();

            marineServiceDispositionSearchResponse.setId( marineServiceDispositionApplication.getId() );

            MarineSearchResponse marineSearchResponse = marineService.getMarineSearchResponseFromMarine( marineServiceDispositionApplication.getMarine(), request );

            marineServiceDispositionSearchResponse.setMarine( marineSearchResponse );

            State state = marineServiceDispositionApplication.getState();
            StateResponse stateResponse = null;
            if( state != null ){

                stateResponse = StateResponse.builder()
                        .id( state.getId() )
                        .name( state.getName() )
                        .nameEn( state.getNameEn() )
                        .stateType( state.getStateType() )
                        .build();
            }

            marineServiceDispositionSearchResponse.setStateResponse( stateResponse );

            return marineServiceDispositionSearchResponse;
        }

        return null;
    }

    public Boolean existById(Long id) {

        return marineServiceDispositionApplicationRepository.existsById( id );
    }

    @Transactional
    public void takeAction(Long id, Long actionId, MarineServiceDispositionApplicationAdditionalData marineServiceDispositionApplicationAdditionalData, UserPrincipal loggedInUser) {

        stateActionMapService.takeAction( this, id, actionId, loggedInUser, (workflowEntity ) -> setAdditionalData( workflowEntity, actionId, marineServiceDispositionApplicationAdditionalData ) );

    }

    private WorkflowAdditionalData setAdditionalData(WorkflowEntity workflowEntity, Long actionId, WorkflowAdditionalData additionalActionData ) {

        MarineServiceDispositionApplication marineServiceDispositionApplication = (MarineServiceDispositionApplication) workflowEntity;

        MarineServiceDispositionApplicationAdditionalData marineServiceDispositionApplicationAdditionalData = (MarineServiceDispositionApplicationAdditionalData) additionalActionData;

        if( actionId.equals( AppConstants.MARINE_SERVICE_DISPOSITION_APP_CREATE_COMMITTEE ) && marineServiceDispositionApplicationAdditionalData.getCommitteeAddRequestList() != null && marineServiceDispositionApplicationAdditionalData.getCommitteeAddRequestList().size() > 0 ){

            Set<Committee> committeeSet =
                    marineServiceDispositionApplicationAdditionalData.getCommitteeAddRequestList()
                            .stream()
                            .map( committeeAddRequest -> committeeService.getCommitteeFromCommitteeAddRequest( committeeAddRequest ) )
                            .filter( committee -> committee != null )
                            .collect( Collectors.toSet() );

            marineServiceDispositionApplication.getCommitteeMemberSet().clear();
            marineServiceDispositionApplication.getCommitteeMemberSet().addAll( committeeSet );

            User user = userService.findById( marineServiceDispositionApplicationAdditionalData.getActionTakerMemberId() ).get();

            marineServiceDispositionApplication.setActionTakingUser( user );
        }

        if( actionId.equals( AppConstants.MARINE_SERVICE_DISPOSITION_APP_COMMITTEE_SUBMIT_REPORT) ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( marineServiceDispositionApplicationAdditionalData.getCommitteeInvestigationReportFiles(), Collections.EMPTY_MAP );
            marineServiceDispositionApplication.setInvestigationReportGroupId( documentUploadedResponse.getDocGroupId() );
        }

        return marineServiceDispositionApplicationAdditionalData;
    }
}
