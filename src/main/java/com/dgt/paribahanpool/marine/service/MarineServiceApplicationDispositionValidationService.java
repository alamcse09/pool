package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.marine.model.MarineServiceApplication;
import com.dgt.paribahanpool.marine.model.MarineServiceDispositionApplication;
import com.dgt.paribahanpool.marine.model.MarineServiceDispositionApplicationAdditionalData;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class MarineServiceApplicationDispositionValidationService {

    private final MarineServiceApplicationDispositionService marineServiceApplicationDispositionService;
    private final MvcUtil mvcUtil;
    private final StateActionMapService stateActionMapService;
    private final UserService userService;

    public Boolean view(Long id) {

        Boolean exists = marineServiceApplicationDispositionService.existById( id );
        return exists;
    }

    public Boolean takeActionValidation(RedirectAttributes redirectAttributes, Long id, Long actionId, UserPrincipal loggedInUser, MarineServiceDispositionApplicationAdditionalData marineServiceDispositionApplicationAdditionalData) {

        Optional<MarineServiceDispositionApplication> marineServiceDispositionApplicationOptional = marineServiceApplicationDispositionService.findById( id );

        if( !marineServiceDispositionApplicationOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }
        else {

            MarineServiceDispositionApplication marineServiceDispositionApplication = marineServiceDispositionApplicationOptional.get();
            State state = marineServiceDispositionApplication.getState();

            if (!stateActionMapService.actionAllowed(state.getId(), loggedInUser.getRoleIds(), actionId)) {

                mvcUtil.addErrorMessage(redirectAttributes, "error.common.action.not-allowed");
                return false;
            }

            if ( actionId.equals( AppConstants.MARINE_SERVICE_APP_CREATE_COMMITTEE ) && ( marineServiceDispositionApplicationAdditionalData.getCommitteeAddRequestList() == null || marineServiceDispositionApplicationAdditionalData.getCommitteeAddRequestList().size() <= 0 || marineServiceDispositionApplicationAdditionalData.getActionTakerMemberId() == null ) ) {

                mvcUtil.addErrorMessage(redirectAttributes, "error.incident.add.committee");
                return false;
            }

            if ( actionId.equals(AppConstants.MARINE_SERVICE_APP_CREATE_COMMITTEE) && marineServiceDispositionApplicationAdditionalData.getActionTakerMemberId() != null ) {

                Optional<User> userOptional = userService.findById( marineServiceDispositionApplicationAdditionalData.getActionTakerMemberId() );

                if ( !userOptional.isPresent() ) {

                    mvcUtil.addErrorMessage(redirectAttributes, "error.incident.add.no-action-taker");
                    return false;
                }
            }
        }

        return true;
    }
}
