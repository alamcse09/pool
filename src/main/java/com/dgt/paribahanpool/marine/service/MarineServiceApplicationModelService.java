package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.committee.model.CommitteeResponse;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.marine.model.*;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.util.NameResponse;
import com.dgt.paribahanpool.workflow.model.StateActionMapResponse;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class MarineServiceApplicationModelService {

    private final MarineServiceApplicationService marineServiceApplicationService;
    private final MarineService marineService;
    private final MvcUtil mvcUtil;
    private final StateActionMapService stateActionMapService;
    private final UserService userService;
    private final DocumentService documentService;

    public void addPage(Model model) {

        model.addAttribute( "marineServiceApplicationAddRequest", new MarineServiceApplicationAddRequest() );
    }

    public void addById( Model model, Long marineId ) {

        MarineServiceApplicationAddRequest marineServiceApplicationAddRequest = new MarineServiceApplicationAddRequest();

        Optional<Marine> marineOptional = marineService.findById( marineId );

        if( marineOptional.isPresent() ){

            Marine marine = marineOptional.get();

            marineServiceApplicationAddRequest.setMarineId( marine.getId() );
            marineServiceApplicationAddRequest.setEngineNo( marine.getEngineNo() );
            marineServiceApplicationAddRequest.setDriverName( marine.getDriver() != null? marine.getDriver().getPhoneNumber(): "" );
            marineServiceApplicationAddRequest.setUserName( marine.getUser() != null && marine.getUser().getPublicUserInfo() != null? marine.getUser().getPublicUserInfo().getName() : "" );
            marineServiceApplicationAddRequest.setUserDesignation( marine.getUser() != null && marine.getUser().getPublicUserInfo() != null? marine.getUser().getPublicUserInfo().getDesignation(): "" );
            marineServiceApplicationAddRequest.setUserWorkingPlace( marine.getUser() != null && marine.getUser().getPublicUserInfo() != null? marine.getUser().getPublicUserInfo().getWorkplace(): "" );
        }

        model.addAttribute( "marineServiceApplicationAddRequest", marineServiceApplicationAddRequest );
    }

    public MarineServiceApplication addPost( MarineServiceApplicationAddRequest marineServiceApplicationAddRequest, Model model ) {

        MarineServiceApplication marineServiceApplication = marineServiceApplicationService.save( marineServiceApplicationAddRequest );
        mvcUtil.addSuccessMessage( model, "vehicle.add.success" );
        return marineServiceApplication;
    }

    @Transactional
    public void view( Model model, Long id, UserPrincipal loggedInUser ) {

        Optional<MarineServiceApplication> marineServiceApplicationOptional = marineServiceApplicationService.findById( id );
        
        if( marineServiceApplicationOptional.isPresent() ){
            
            MarineServiceApplication marineServiceApplication = marineServiceApplicationOptional.get();
            
            if( marineServiceApplication.getState() != null ){

                List<StateActionMapResponse> stateActionMapList = stateActionMapService.findStateActionMapResponseListByCurrentStateAndRoleIdSet( marineServiceApplication.getState().getId(), loggedInUser.getRoleIds() );
                model.addAttribute( "stateActionList", stateActionMapList );
            }

            List<UserResponse> userResponseList = userService.getUserResponseList( UserType.SYSTEM_USER );
            User user = marineServiceApplication.getActionTakingUser();

            List<CommitteeResponse> committeeResponseList = marineServiceApplication.getCommitteeMemberSet()
                    .stream()
                    .map(
                            committee -> CommitteeResponse.builder()
                                    .nameEn( committee.getNameEn() )
                                    .nameBn( committee.getName() )
                                    .build()
                    )
                    .collect(Collectors.toList() );

            NameResponse actionTakerResponse = NameResponse.builder()
                    .nameBn( user != null? user.getEmployee().getName(): "" )
                    .nameEn( user != null? user.getEmployee().getNameEn(): "" )
                    .build();

            List<DocumentMetadata> committeeReportMetadataList = documentService.findDocumentMetaDataListByGroupId( marineServiceApplication.getInvestigationReportGroupId() );

            MarineServiceApplicationViewResponse marineServiceApplicationViewResponse = MarineServiceApplicationViewResponse.builder()
                    .id( marineServiceApplication.getId() )
                    .engineNo( marineServiceApplication.getMarine().getEngineNo() )
                    .marineUserName( marineServiceApplication.getUserName() )
                    .marineUserDesignation( marineServiceApplication.getUserDesignation() )
                    .marineUserWorkingPlace( marineServiceApplication.getUserWorkingPlace() )
                    .marineUserMobileNumber( marineServiceApplication.getUserMobileNumber() )
                    .driverName( marineServiceApplication.getDriverName() )
                    .driverMobileNumber( marineServiceApplication.getDriverMobileNumber() )
                    .serviceDate( marineServiceApplication.getServiceDate().format( DateTimeFormatter.ofPattern( DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )  ) )
                    .serviceTime( marineServiceApplication.getServiceTime() )
                    .cost( marineServiceApplication.getCost() )
                    .showEmployeeList( userResponseList )
                    .actionTakerMemberId( marineServiceApplication.getActionTakingUser() != null? marineServiceApplication.getActionTakingUser().getId(): -1L )
                    .committeeResponseList( committeeResponseList )
                    .actionTakerResponse( actionTakerResponse )
                    .committeeReportDocuments( committeeReportMetadataList )
                    .appliedDate( marineServiceApplication.getCreatedAt().toLocalDate().format( DateTimeFormatter.ofPattern( DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )  ) )
                    .build();

            model.addAttribute( "marineServiceApplicationViewResponse", marineServiceApplicationViewResponse );
        }
    }

    public void takeAction(

            RedirectAttributes redirectAttributes,
            Long id,
            Long actionId,
            MarineServiceApplicationAdditionalData marineServiceApplicationAdditionalData,
            UserPrincipal loggedInUser
    ) {

        marineServiceApplicationService.takeAction( id, actionId, marineServiceApplicationAdditionalData, loggedInUser );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.common.action.success" );
    }
}
