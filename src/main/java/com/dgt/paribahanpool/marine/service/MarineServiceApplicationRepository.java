package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.marine.model.MarineServiceApplication;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface MarineServiceApplicationRepository extends DataTablesRepository<MarineServiceApplication, Long> {
}
