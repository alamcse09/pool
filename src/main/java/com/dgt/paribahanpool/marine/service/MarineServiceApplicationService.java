package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.committee.model.Committee;
import com.dgt.paribahanpool.committee.service.CommitteeService;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.incident.model.Incident;
import com.dgt.paribahanpool.incident.model.IncidentAdditionalData;
import com.dgt.paribahanpool.marine.model.*;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.workflow.model.*;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class MarineServiceApplicationService implements WorkflowService<MarineServiceApplication> {

    private final MarineServiceApplicationRepository marineServiceApplicationRepository;
    private final StateService stateService;
    private final MarineService marineService;
    private final StateActionMapService stateActionMapService;
    private final CommitteeService committeeService;
    private final UserService userService;
    private final DocumentService documentService;

    public MarineServiceApplication save( MarineServiceApplication marineServiceApplication ){

        return marineServiceApplicationRepository.save( marineServiceApplication );
    }

    public Optional<MarineServiceApplication> findById(Long id){

        return marineServiceApplicationRepository.findById( id );
    }

    public Boolean existById(Long id) {

        return marineServiceApplicationRepository.existsById( id );
    }

    public void delete(Long id){

        marineServiceApplicationRepository.deleteById( id );
    }

    public MarineServiceApplication save( MarineServiceApplicationAddRequest marineServiceApplicationAddRequest ) {

        MarineServiceApplication marineServiceApplication = getMarineServiceApplicationFromAddRequest( marineServiceApplicationAddRequest );

        return save( marineServiceApplication );
    }

    private MarineServiceApplication getMarineServiceApplicationFromAddRequest(MarineServiceApplicationAddRequest marineServiceApplicationAddRequest ) {

        MarineServiceApplication marineServiceApplication = new MarineServiceApplication();

        marineServiceApplication.setUserName( marineServiceApplicationAddRequest.getUserName() );
        marineServiceApplication.setUserDesignation( marineServiceApplicationAddRequest.getUserDesignation() );
        marineServiceApplication.setUserWorkingPlace( marineServiceApplicationAddRequest.getUserWorkingPlace() );
        marineServiceApplication.setUserMobileNumber( marineServiceApplicationAddRequest.getUserMobileNumber() );
        marineServiceApplication.setDriverName( marineServiceApplicationAddRequest.getDriverName() );
        marineServiceApplication.setDriverMobileNumber( marineServiceApplicationAddRequest.getDriverMobileNumber() );
        marineServiceApplication.setCost( marineServiceApplicationAddRequest.getCost() );
        marineServiceApplication.setServiceDate( marineServiceApplicationAddRequest.getServiceDate() );
        marineServiceApplication.setServiceTime( marineServiceApplicationAddRequest.getServiceTime() );
        marineServiceApplication.setServiceCategory( marineServiceApplicationAddRequest.getServiceCategory() );
        marineServiceApplication.setServiceType( marineServiceApplicationAddRequest.getServiceType() );
        marineServiceApplication.setApplicationDate(marineServiceApplicationAddRequest.getApplicationDate());

        Marine marine = marineService.getReference( marineServiceApplicationAddRequest.getMarineId() );
        marineServiceApplication.setMarine( marine );
        
        State state = stateService.findStateReferenceById(AppConstants.MARINE_SERVICE_APPLICATION_INIT_STATE_ID );
        marineServiceApplication.setState( state );

        return marineServiceApplication;
    }

    public DataTablesOutput<MarineServiceApplicationSearchResponse> search(DataTablesInput dataTablesInput) {

        return marineServiceApplicationRepository.findAll( dataTablesInput, this::getMarineServiceApplicationSearchResponseFromMarineServcieApplication );
    }

    private MarineServiceApplicationSearchResponse getMarineServiceApplicationSearchResponseFromMarineServcieApplication(MarineServiceApplication marineServiceApplication) {

        if( marineServiceApplication.getMarine() != null ) {

            MarineServiceApplicationMarineSearchResponse marineServiceApplicationMarineSearchResponse = MarineServiceApplicationMarineSearchResponse.builder()
                    .engineNo(marineServiceApplication.getMarine().getEngineNo())
                    .brand( marineServiceApplication.getMarine().getBrand() )
                    .horsepower( marineServiceApplication.getMarine().getHorsepower() )
                    .build();

            State state = marineServiceApplication.getState();
            StateResponse stateResponse = null;
            if( state != null ){

                stateResponse = StateResponse.builder()
                        .id( state.getId() )
                        .name( state.getName() )
                        .nameEn( state.getNameEn() )
                        .stateType( state.getStateType() )
                        .build();
            }

            return MarineServiceApplicationSearchResponse.builder()
                    .id( marineServiceApplication.getId() )
                    .cost( marineServiceApplication.getCost() )
                    .serviceTime( marineServiceApplication.getServiceTime() )
                    .serviceDate( marineServiceApplication.getServiceDate() )
                    .marine( marineServiceApplicationMarineSearchResponse )
                    .userName( marineServiceApplication.getUserName() )
                    .userDesignation( marineServiceApplication.getUserDesignation() )
                    .userWorkingPlace( marineServiceApplication.getUserWorkingPlace() )
                    .driverName( marineServiceApplication.getDriverName() )
                    .driverMobileNumber( marineServiceApplication.getDriverMobileNumber() )
                    .stateResponse( stateResponse )
                    .build();
        }

        return null;
    }

    @Transactional
    public void takeAction(Long id, Long actionId, MarineServiceApplicationAdditionalData marineServiceApplicationAdditionalData, UserPrincipal loggedInUser) {

        stateActionMapService.takeAction( this, id, actionId, loggedInUser, (workflowEntity ) -> setAdditionalData( workflowEntity, actionId, marineServiceApplicationAdditionalData ) );
    }

    private WorkflowAdditionalData setAdditionalData( WorkflowEntity workflowEntity, Long actionId, WorkflowAdditionalData additionalActionData ) {

        MarineServiceApplication marineServiceApplication = (MarineServiceApplication) workflowEntity;

        MarineServiceApplicationAdditionalData marineServiceApplicationAdditionalData = (MarineServiceApplicationAdditionalData) additionalActionData;

        if( actionId.equals( AppConstants.MARINE_SERVICE_APP_CREATE_COMMITTEE ) && marineServiceApplicationAdditionalData.getCommitteeAddRequestList() != null && marineServiceApplicationAdditionalData.getCommitteeAddRequestList().size() > 0 ){

            Set<Committee> committeeSet =
                    marineServiceApplicationAdditionalData.getCommitteeAddRequestList()
                            .stream()
                            .map( committeeAddRequest -> committeeService.getCommitteeFromCommitteeAddRequest( committeeAddRequest ) )
                            .filter( committee -> committee != null )
                            .collect( Collectors.toSet() );

            marineServiceApplication.getCommitteeMemberSet().clear();
            marineServiceApplication.getCommitteeMemberSet().addAll( committeeSet );

            User user = userService.findById( marineServiceApplicationAdditionalData.getActionTakerMemberId() ).get();

            marineServiceApplication.setActionTakingUser( user );
        }

        if( actionId.equals( AppConstants.MARINE_SERVICE_APP_COMMITTEE_SUBMIT_REPORT) ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( marineServiceApplicationAdditionalData.getCommitteeInvestigationReportFiles(), Collections.EMPTY_MAP );
            marineServiceApplication.setInvestigationReportGroupId( documentUploadedResponse.getDocGroupId() );
        }

        return marineServiceApplicationAdditionalData;
    }
}
