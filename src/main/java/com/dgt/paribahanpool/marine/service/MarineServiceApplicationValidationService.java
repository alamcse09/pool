package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.incident.model.Incident;
import com.dgt.paribahanpool.marine.model.MarineServiceApplication;
import com.dgt.paribahanpool.marine.model.MarineServiceApplicationAddRequest;
import com.dgt.paribahanpool.marine.model.MarineServiceApplicationAdditionalData;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.swing.text.html.Option;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class MarineServiceApplicationValidationService {

    private final MarineServiceApplicationService marineServiceApplicationService;
    private final MvcUtil mvcUtil;
    private final StateActionMapService stateActionMapService;
    private final UserService userService;

    public Boolean handleBindingResultFromAddPost( Model model, BindingResult bindingResult, MarineServiceApplicationAddRequest marineServiceApplicationAddRequest ) {

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "marineServiceApplicationAddRequest" )
                .object( marineServiceApplicationAddRequest )
                .title( "title.marine-service-application.add" )
                .content( "marine/marine-service-application-add" )
                .activeMenu( Menu.MARINE_SERVICE_APPLICATION_ADD )
                .build();

        boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        return isValid;
    }

    public Boolean addPost(RedirectAttributes redirectAttributes, MarineServiceApplicationAddRequest marineServiceApplicationAddRequest) {

        return true;
    }

    public RestValidationResult delete(Long id) throws NotFoundException {

        Boolean exist = marineServiceApplicationService.existById( id );
        if( !exist ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean view(Long id) {

        Boolean exists = marineServiceApplicationService.existById( id );
        return exists;
    }

    public Boolean takeActionValidation(
            RedirectAttributes redirectAttributes,
            Long id,
            Long actionId,
            UserPrincipal loggedInUser,
            MarineServiceApplicationAdditionalData marineServiceApplicationAdditionalData
    ) {

        Optional<MarineServiceApplication> marineServiceApplicationOptional = marineServiceApplicationService.findById( id );

        if( !marineServiceApplicationOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }
        else {

            MarineServiceApplication marineServiceApplication = marineServiceApplicationOptional.get();
            State state = marineServiceApplication.getState();

            if (!stateActionMapService.actionAllowed(state.getId(), loggedInUser.getRoleIds(), actionId)) {

                mvcUtil.addErrorMessage(redirectAttributes, "error.common.action.not-allowed");
                return false;
            }

            if ( actionId.equals( AppConstants.MARINE_SERVICE_APP_CREATE_COMMITTEE ) && ( marineServiceApplicationAdditionalData.getCommitteeAddRequestList() == null || marineServiceApplicationAdditionalData.getCommitteeAddRequestList().size() <= 0 || marineServiceApplicationAdditionalData.getActionTakerMemberId() == null ) ) {

                mvcUtil.addErrorMessage(redirectAttributes, "error.incident.add.committee");
                return false;
            }

            if ( actionId.equals(AppConstants.MARINE_SERVICE_APP_CREATE_COMMITTEE) && marineServiceApplicationAdditionalData.getActionTakerMemberId() != null ) {

                Optional<User> userOptional = userService.findById( marineServiceApplicationAdditionalData.getActionTakerMemberId() );

                if ( !userOptional.isPresent() ) {

                    mvcUtil.addErrorMessage(redirectAttributes, "error.incident.add.no-action-taker");
                    return false;
                }
            }
        }

        return true;
    }
}
