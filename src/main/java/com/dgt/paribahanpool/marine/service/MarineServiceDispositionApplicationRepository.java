package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.marine.model.MarineServiceDispositionApplication;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface MarineServiceDispositionApplicationRepository extends DataTablesRepository<MarineServiceDispositionApplication, Long> {
}
