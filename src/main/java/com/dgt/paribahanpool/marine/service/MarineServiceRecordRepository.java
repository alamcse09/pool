package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.marine.model.MarineServiceRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarineServiceRecordRepository extends JpaRepository<MarineServiceRecord, Long> {
}
