package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.marine.model.MarineServiceRecord;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class MarineServiceRecordService {

    private final MarineServiceRecordRepository marineServiceRecordRepository;

    public Optional<MarineServiceRecord> findById(Long id ){

        return marineServiceRecordRepository.findById( id );
    }
}
