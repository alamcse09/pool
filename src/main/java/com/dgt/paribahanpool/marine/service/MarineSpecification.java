package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.marine.model.Marine;
import com.dgt.paribahanpool.marine.model.Marine_;
import org.springframework.data.jpa.domain.Specification;

public class MarineSpecification {

    public static Specification<Marine> filterByIsDeleted( Boolean isDeleted ){

        if( isDeleted == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Marine_.IS_DELETED ), isDeleted );
    }

    public static Specification<Marine> filterByEngineNo( String engineNo ){

        if( engineNo == null || engineNo.isEmpty() )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Marine_.ENGINE_NO ), engineNo.trim() );
    }
}
