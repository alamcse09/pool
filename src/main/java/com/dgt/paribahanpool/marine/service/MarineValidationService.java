package com.dgt.paribahanpool.marine.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.marine.model.Marine;
import com.dgt.paribahanpool.marine.model.MarineAddRequest;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class MarineValidationService {

    private final MvcUtil mvcUtil;
    private final MarineService marineService;

    public Boolean handleBindingResultForAddFormPost( Model model, BindingResult bindingResult, MarineAddRequest marineAddRequest ){

        HandleBindingResultParams params = HandleBindingResultParams
                        .builder()
                        .key( "marineAddRequest" )
                        .object( marineAddRequest )
                        .title( "title.marine.add" )
                        .content( "marine/marine-add" )
                        .activeMenu( Menu.MARINE_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.MARINE_ADD_FORM } )
                        .build();

        boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        if( !isValid ) return false;

        return validateMarineUniqueConstraints( model, marineAddRequest.getId(), marineAddRequest.getEngineNo(), params );
    }

    public RestValidationResult findByEngineNumber( String engineNumber ) throws NotFoundException {

        if(marineService.getMarineByEngineNo( engineNumber ).size() > 0) {

            return RestValidationResult.valid();
        }

        throw new NotFoundException("validation.common.notfound");
    }

    public boolean validateMarineUniqueConstraints( Model model, Long id, String engineNo, HandleBindingResultParams params ){

        if( id == null ){

            if( marineService.getMarineByEngineNo( engineNo ).size() > 0 ){

                return setModelWithError( model, params, "error.marine.engine_number.already.exist" );
            }
        }
        else{

            List<Marine> marineList = marineService.getMarineByEngineNo( engineNo );

            if( marineList.size() > 0 && !marineList.get(0).getId().equals( id ) ){

                return setModelWithError( model, params, "error.marine.engine_number.already.exist" );
            }
        }

        return true;
    }

    public boolean setModelWithError( Model model, HandleBindingResultParams params, String errorMessage ){

        mvcUtil.addErrorMessage( model, errorMessage );
        model.addAttribute( params.getKey(), params.getObject() );
        mvcUtil.addTitleAndContent( model, params.getTitle(), params.getContent(), params.getActiveMenu() );

        if( params.getFrontEndLibraries() != null ){

            mvcUtil.addCssAndJsByLibraryName( model, params.getFrontEndLibraries() );
        }

        return false;
    }

    public Boolean addMarinePost( RedirectAttributes redirectAttributes, MarineAddRequest marineAddRequest ) {

        if( marineAddRequest.getId() != null ) {

            Optional<Marine> marine = marineService.findById( marineAddRequest.getId() );

            if ( !marine.isPresent() ) {

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
                return false;
            }
        }

        return true;
    }

    public Boolean editMarine(RedirectAttributes redirectAttributes, Long marineId){

        Optional<Marine> marine = marineService.findById(marineId);
        if(!marine.isPresent()){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    public RestValidationResult delete( Long id ) throws NotFoundException {

        Boolean exist = marineService.existById( id );
        if( !exist ){

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }
}
