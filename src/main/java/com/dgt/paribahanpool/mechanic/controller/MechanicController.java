package com.dgt.paribahanpool.mechanic.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.mechanic.model.Mechanic;
import com.dgt.paribahanpool.mechanic.model.MechanicAddRequest;
import com.dgt.paribahanpool.mechanic.service.MechanicModelService;
import com.dgt.paribahanpool.mechanic.service.MechanicValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/mechanic" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class MechanicController extends MVCController {

    private final MechanicModelService mechanicModelService;
    private final MechanicValidationService mechanicValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.mechanic.add", content = "mechanic/mechanic-add", activeMenu = Menu.MECHANIC_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MECHANIC_ADD_FORM} )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add Mechanic form" );
        mechanicModelService.addMechanicGet( model );
        return viewRoot;
    }

    @PostMapping( value = "/add" )
    public String addMechanic(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid MechanicAddRequest mechanicAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !mechanicValidationService.handleBindingResultForAddFormPost( model, bindingResult, mechanicAddRequest ) ){

            return viewRoot;
        }

        if( mechanicValidationService.addMechanicPost( redirectAttributes, mechanicAddRequest) ){

            Mechanic mechanic = mechanicModelService.addMechanicPost( mechanicAddRequest, redirectAttributes );
            log( LogEvent.MECHANIC_ADDED, mechanic.getId(), "Mechanic Added", request );
        }

        return "redirect:/mechanic/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.mechanic.search", content = "mechanic/mechanic-search", activeMenu = Menu.MECHANIC_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MECHANIC_SEARCH } )
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Mechanic Search page called" );
        return viewRoot;
    }

    @GetMapping( "/edit/{mechanicId}" )
    @TitleAndContent( title = "title.mechanic.edit", content = "mechanic/mechanic-add", activeMenu = Menu.MECHANIC_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MECHANIC_ADD_FORM })
    public String edit(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable( "mechanicId" ) Long mechanicId
    ){

        log.debug( "Rendering edit Mechanic Form" );

        if( mechanicValidationService.editMechanic( redirectAttributes, mechanicId ) ) {

            mechanicModelService.editMechanic( model, mechanicId );
            return viewRoot;
        }

        return "redirect:/mechanic/search";
    }

    @PostMapping( value = "/edit/{mechanicId}")
    public String editMechanic(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid MechanicAddRequest mechanicAddRequest,
            BindingResult bindingResult,
            @PathVariable( "mechanicId" ) Long mechanicId,
            HttpServletRequest request
    ){

        if( !mechanicValidationService.handleBindingResultForEditFormPost( model, bindingResult, mechanicAddRequest ) ){

            return viewRoot;
        }

        if( mechanicValidationService.editMechanic( redirectAttributes, mechanicAddRequest.getId() ) ){

            Mechanic mechanic = mechanicModelService.addMechanicPost( mechanicAddRequest, redirectAttributes );
            log( LogEvent.MECHANIC_EDITED, mechanic.getId(), "Mechanic Edited", request );
        }

        return "redirect:/mechanic/search";
    }


}
