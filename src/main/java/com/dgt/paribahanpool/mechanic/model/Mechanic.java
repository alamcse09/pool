package com.dgt.paribahanpool.mechanic.model;

import com.dgt.paribahanpool.enums.MechanicStatus;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.util.model.VersionableAuditableEntity;
import lombok.Data;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table( name = "mechanic",
        uniqueConstraints = {
                @UniqueConstraint( columnNames = { "mechanic_id" }, name = "uk_mechanic_mechanic_id" )
        }
)
@SQLDelete( sql = "UPDATE mechanic set is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class Mechanic extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "mechanic_id" )
    private Long mechanicId;

    @Column( name = "name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String name;

    @Column( name = "father_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String fatherName;

    @Column( name = "mother_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String motherName;

    @Column( name = "phone_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String phoneNumber;

    @Column( name = "address" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String address;

    @Column( name = "expertise" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String expertise;

    @Column( name = "qualification" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String qualification;

    @Column( name = "experience" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String experience;

    @Column( name = "enrolment_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate enrolmentDate;

    @Column( name = "date_of_birth" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate dateOfBirth;

    @Column( name = "nid_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String nidNumber;

    @Column( name = "document_group_id" )
    private Long docGroupId;

    @Column( name = "mechanic_status" )
    private MechanicStatus mechanicStatus;

    @Column( name = "average_ratings" )
    private Double averageRatings;

    @Column( name = "total_ratings" )
    private Double totalRatings;

    @Column( name = "count_of_people_rated" )
    private Double countOfPeopleRated;

    @Column( name = "email" )
    private String email;

    @Column( name = "password" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String password;
}
