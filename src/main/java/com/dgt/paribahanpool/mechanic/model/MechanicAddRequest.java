package com.dgt.paribahanpool.mechanic.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.MechanicStatus;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
public class MechanicAddRequest {

    public Long id;

    @NotNull( message = "{validation.common.required}")
    public Long mechanicId;

//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphabet}" )
    @NotBlank( message = "{validation.common.required}" )
    public String name;

    public String nameEn;

    public String nameBn;

    private double metadata = 0.0;

//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphabet}" )
    @NotBlank( message = "{validation.common.required}" )
    public String fatherName;

//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphabet}" )
    @NotBlank( message = "{validation.common.required}" )
    public String motherName;

    @Digits( integer = 11, fraction = 0, message = "{validation.common.numeric}" )
    @NotNull( message = "{validation.common.required}")
    public String phoneNumber;

    @NotBlank( message = "{validation.common.required}" )
    public String address;

    public String expertise;

    public String qualification;

    public String experience;

    @PastOrPresent( message = "{validation.common.date.past_or_present}" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    @NotNull( message = "{validation.common.required}")
    public LocalDate enrolmentDate;

    @PastOrPresent( message = "{validation.common.date.past}" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    @NotNull( message = "{validation.common.required}")
    public LocalDate dateOfBirth;

    @Digits( integer = 17, fraction = 0, message = "{validation.common.numeric}" )
    @NotNull( message = "{validation.common.required}")
    public String nidNumber;

    private MultipartFile[] files;

    List<DocumentMetadata> documents = Collections.EMPTY_LIST;

    public MechanicStatus mechanicStatus;

    private Double averageRatings;

    private Double totalRatings;

    private Double countOfPeopleRated;

    private String email;

    private String password = "1234";
}
