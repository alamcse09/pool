package com.dgt.paribahanpool.mechanic.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.MechanicStatus;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class MechanicSearchResponse {

    public Long id;
    public Long mechanicId;
    public String name;
    public String fatherName;
    public String motherName;
    public String phoneNumber;
    public String address;
    public String expertise;
    public String qualification;
    public String experience;
    public LocalDate enrolmentDate;
    public LocalDate dateOfBirth;
    public String nidNumber;
    public MechanicStatus mechanicStatus;

    private List<DocumentMetadata> documentMetadataList;
}
