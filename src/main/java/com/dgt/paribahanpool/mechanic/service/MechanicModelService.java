package com.dgt.paribahanpool.mechanic.service;

import com.dgt.paribahanpool.mechanic.model.Mechanic;
import com.dgt.paribahanpool.mechanic.model.MechanicAddRequest;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class MechanicModelService {

    private final MechanicService mechanicService;
    private final MvcUtil mvcUtil;

    public void addMechanicGet(Model model ){

        model.addAttribute( "mechanicAddRequest", new MechanicAddRequest() );
    }

    public Mechanic addMechanicPost( MechanicAddRequest mechanicAddRequest, RedirectAttributes redirectAttributes ){

        Mechanic mechanic = mechanicService.save( mechanicAddRequest );

        if( mechanicAddRequest.getId() != null ){

            mvcUtil.addSuccessMessage( redirectAttributes, "success.mechanic.edit" );
        }
        else {

            mvcUtil.addSuccessMessage(redirectAttributes, "success.mechanic.add");
        }

        return mechanic;
    }

    public void editMechanic( Model model, Long mechanicId ){

        Optional<Mechanic> mechanic = mechanicService.findById( mechanicId );

        if( mechanic.isPresent() ){

            MechanicAddRequest mechanicAddRequest = mechanicService.getMechanicAddRequestFromMechanic( mechanic.get() );

            model.addAttribute( "mechanicAddRequest", mechanicAddRequest );
        }
    }
}
