package com.dgt.paribahanpool.mechanic.service;

import com.dgt.paribahanpool.mechanic.model.Mechanic;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

import java.util.List;
import java.util.Optional;

public interface MechanicRepository extends DataTablesRepository<Mechanic, Long> {

    Optional<Mechanic> findByIdAndIsDeleted(Long id, Boolean isDeleted);

    List<Mechanic> findAllByIsDeleted(boolean b);
}
