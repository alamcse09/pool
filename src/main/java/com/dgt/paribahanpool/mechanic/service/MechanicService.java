package com.dgt.paribahanpool.mechanic.service;

import com.dgt.paribahanpool.document.model.Document;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.mechanic.model.Mechanic;
import com.dgt.paribahanpool.mechanic.model.MechanicAddRequest;
import com.dgt.paribahanpool.mechanic.model.MechanicSearchResponse;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.vehicle.model.MechanicViewResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class MechanicService {

    private final MechanicRepository mechanicRepository;
    private final DocumentService documentService;
    private final UserService userService;

    public Optional<Mechanic> findById( Long id ){

        return mechanicRepository.findById( id );
    }

    public List<Mechanic> findAll() {

        return mechanicRepository.findAllByIsDeleted(false);
    }

    public final Optional<Mechanic> findById( Long  id, Boolean isDeleted ){

        return mechanicRepository.findByIdAndIsDeleted( id, isDeleted );
    }

    @Transactional
    public Mechanic save( MechanicAddRequest mechanicAddRequest ){

        Mechanic mechanic = new Mechanic();
        User user = null;

        if( mechanicAddRequest.getId() != null ){

            mechanic = findById( mechanicAddRequest.getId() ).get();
            Optional<User> userOptional = userService.findByMechanic( mechanic );
            if( userOptional.isPresent() ){
                user = userOptional.get();
            }
        }
        else{

            user = userService.createUserFromMechanic( mechanicAddRequest );
        }
        mechanic = getMechanicFromMechanicAddRequest( mechanic, mechanicAddRequest );
        user.setMechanic( mechanic );
        userService.save( user );

        return save( mechanic );
    }

    public Mechanic save( Mechanic mechanic ){

        return mechanicRepository.save( mechanic );
    }

    public void delete( Long id ) {

        mechanicRepository.deleteById( id );
    }

    public Boolean existById( Long id ){

        return mechanicRepository.existsById( id );
    }

    public Mechanic getMechanicFromMechanicAddRequest( Mechanic mechanic, MechanicAddRequest mechanicAddRequest ){

        mechanic.setMechanicId( mechanicAddRequest.getMechanicId() );
        mechanic.setName( mechanicAddRequest.getName() );
        mechanic.setFatherName( mechanicAddRequest.getFatherName() );
        mechanic.setMotherName( mechanicAddRequest.getMotherName() );
        mechanic.setPhoneNumber( mechanicAddRequest.getPhoneNumber() );
        mechanic.setAddress( mechanicAddRequest.getAddress() );
        mechanic.setExpertise( mechanicAddRequest.getExpertise() );
        mechanic.setQualification( mechanicAddRequest.getQualification() );
        mechanic.setExperience( mechanicAddRequest.getExperience() );
        mechanic.setEnrolmentDate( mechanicAddRequest.getEnrolmentDate() );
        mechanic.setDateOfBirth( mechanicAddRequest.getDateOfBirth() );
        mechanic.setNidNumber( mechanicAddRequest.getNidNumber() );
        mechanic.setMechanicStatus( mechanicAddRequest.getMechanicStatus() );
        mechanic.setEmail( mechanicAddRequest.getEmail() );

        if( mechanic.getDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( mechanicAddRequest.getFiles(), Collections.EMPTY_MAP );
            mechanic.setDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( mechanicAddRequest.getFiles(), mechanic.getDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        return mechanic;
    }

    public MechanicAddRequest getMechanicAddRequestFromMechanic( Mechanic mechanic ){

        MechanicAddRequest mechanicAddRequest = new MechanicAddRequest();

        mechanicAddRequest.setId( mechanic.getId() );
        mechanicAddRequest.setMechanicId( mechanic.getMechanicId() );
        mechanicAddRequest.setName( mechanic.getName() );
        mechanicAddRequest.setFatherName( mechanic.getFatherName() );
        mechanicAddRequest.setMotherName( mechanic.getMotherName() );
        mechanicAddRequest.setPhoneNumber( mechanic.getPhoneNumber() );
        mechanicAddRequest.setAddress( mechanic.getAddress() );
        mechanicAddRequest.setExpertise( mechanic.getExpertise() );
        mechanicAddRequest.setQualification( mechanic.getQualification() );
        mechanicAddRequest.setExperience( mechanic.getExperience() );
        mechanicAddRequest.setEnrolmentDate( mechanic.getEnrolmentDate() );
        mechanicAddRequest.setDateOfBirth( mechanic.getDateOfBirth() );
        mechanicAddRequest.setNidNumber( mechanic.getNidNumber() );
        mechanicAddRequest.setMechanicStatus( mechanic.getMechanicStatus() );
        mechanicAddRequest.setEmail( mechanic.getEmail() );

        if( mechanic.getDocGroupId() != null && mechanic.getDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( mechanic.getDocGroupId() );
            mechanicAddRequest.setDocuments( documentMetadataList );
        }

        return mechanicAddRequest;
    }

    public MechanicSearchResponse getMechanicSearchResponseFromMechanic( Mechanic mechanic ){

        MechanicSearchResponse mechanicSearchResponse = new MechanicSearchResponse();

        mechanicSearchResponse.setId( mechanic.getId() );
        mechanicSearchResponse.setMechanicId( mechanic.getMechanicId() );
        mechanicSearchResponse.setName( mechanic.getName() );
        mechanicSearchResponse.setFatherName( mechanic.getFatherName() );
        mechanicSearchResponse.setMotherName( mechanic.getMotherName() );
        mechanicSearchResponse.setPhoneNumber( mechanic.getPhoneNumber() );
        mechanicSearchResponse.setAddress( mechanic.getAddress() );
        mechanicSearchResponse.setExpertise( mechanic.getExpertise() );
        mechanicSearchResponse.setQualification( mechanic.getQualification() );
        mechanicSearchResponse.setExperience( mechanic.getExperience() );
        mechanicSearchResponse.setEnrolmentDate( mechanic.getEnrolmentDate() );
        mechanicSearchResponse.setDateOfBirth( mechanic.getDateOfBirth() );
        mechanicSearchResponse.setNidNumber( mechanic.getNidNumber() );
        mechanicSearchResponse.setMechanicStatus( mechanic.getMechanicStatus() );

        List<DocumentMetadata> documentdataList = documentService.findDocumentMetaDataListByGroupId( mechanic.getDocGroupId() );
        mechanicSearchResponse.setDocumentMetadataList( documentdataList );

        return mechanicSearchResponse;
    }

    public DataTablesOutput<MechanicSearchResponse> searchForDatatable( DataTablesInput dataTablesInput ){

        return mechanicRepository.findAll( dataTablesInput, this::getMechanicSearchResponseFromMechanic );
    }

    public List<Mechanic> getMechanicByMechanicId( Long mechanicId ){

        Specification<Mechanic> mechanicSpecification = MechanicSpecification.filterByMechanicId( mechanicId );

        return mechanicRepository.findAll( mechanicSpecification );
    }

    public List<MechanicViewResponse> getAllMechanicViewResponseList() {

        List<Mechanic> mechanicList = findAll();

        return mechanicList
                .stream()
                .map( this::getMechanicViewResponseFromMechanic )
                .collect( Collectors.toList() );
    }

    private MechanicViewResponse getMechanicViewResponseFromMechanic(Mechanic mechanic) {

        MechanicViewResponse mechanicViewResponse = new MechanicViewResponse();

        mechanicViewResponse.setName( mechanic.getName() );
        mechanicViewResponse.setMechanicId( mechanic.getId() );

        return mechanicViewResponse;
    }
}
