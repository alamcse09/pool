package com.dgt.paribahanpool.mechanic.service;

import com.dgt.paribahanpool.mechanic.model.Mechanic;
import com.dgt.paribahanpool.mechanic.model.Mechanic_;
import org.springframework.data.jpa.domain.Specification;

public class MechanicSpecification {

    public static Specification<Mechanic> filterByIsDeleted( Boolean isDeleted ){

        if( isDeleted == null ){

            return Specification.where( null );
        }

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(Mechanic_.IS_DELETED ), isDeleted );
    }

    public static Specification<Mechanic> filterByMechanicId( Long mechanicId ){

        if( mechanicId == null )
            return Specification.where( null );

        return ( (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(Mechanic_.MECHANIC_ID ), mechanicId.longValue() ) );
    }
}
