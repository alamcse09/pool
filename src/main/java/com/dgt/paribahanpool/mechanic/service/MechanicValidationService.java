package com.dgt.paribahanpool.mechanic.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.mechanic.model.Mechanic;
import com.dgt.paribahanpool.mechanic.model.MechanicAddRequest;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class MechanicValidationService {

    private final MvcUtil mvcUtil;
    private final MechanicService mechanicService;

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, MechanicAddRequest mechanicAddRequest ){

        HandleBindingResultParams params = HandleBindingResultParams
                        .builder()
                        .key( "mechanicAddRequest" )
                        .object( mechanicAddRequest )
                        .title( "title.mechanic.add" )
                        .content( "mechanic/mechanic-add" )
                        .activeMenu( Menu.MECHANIC_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.MECHANIC_ADD_FORM } )
                        .build();

        boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        if( !isValid )
            return false;

        return validateAddMechanicUniqueConstraints( model, mechanicAddRequest.getId(), mechanicAddRequest.getMechanicId(), params );
    }

    public boolean validateAddMechanicUniqueConstraints( Model model, Long id, Long mechanicId, HandleBindingResultParams params ){

        if( id == null ){

            if( mechanicService.getMechanicByMechanicId( mechanicId ).size() > 0){

                return setModelWithError( model, params, "error.mechanic.mechanic_id.already.exist" );
            }
        }

        return true;
    }

    public Boolean handleBindingResultForEditFormPost(Model model, BindingResult bindingResult, MechanicAddRequest mechanicAddRequest ){

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "mechanicAddRequest" )
                .object( mechanicAddRequest )
                .title( "title.mechanic.edit" )
                .content( "mechanic/mechanic-add" )
                .activeMenu( Menu.MECHANIC_ADD )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.MECHANIC_ADD_FORM } )
                .build();

        boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        if( !isValid )
            return false;

        return validateEditMechanicUniqueConstraints( model, mechanicAddRequest.getId(), mechanicAddRequest.getMechanicId(), params );
    }

    public boolean validateEditMechanicUniqueConstraints( Model model, Long id, Long mechanicId, HandleBindingResultParams params ){

        Optional<Mechanic> mechanicOptional = mechanicService.findById( mechanicId );

        if( mechanicOptional.isPresent() && !mechanicOptional.get().getId().equals( id ) ){

            return setModelWithError( model, params, "error.mechanic.mechanic_id.already.exist" );
        }

        return true;
    }

    public Boolean addMechanicPost( RedirectAttributes redirectAttributes, MechanicAddRequest mechanicAddRequest ){

        return true;
    }

    public Boolean editMechanic( RedirectAttributes redirectAttributes, Long mechanicId ){

        Optional<Mechanic> mechanic = mechanicService.findById( mechanicId );

        if( !mechanic.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    private boolean setModelWithError( Model model, HandleBindingResultParams params , String errorMessage ) {

        mvcUtil.addErrorMessage( model, errorMessage );
        model.addAttribute( params.getKey(), params.getObject() );
        mvcUtil.addTitleAndContent( model, params.getTitle(), params.getContent(), params.getActiveMenu() );

        if( params.getFrontEndLibraries() != null ){

            mvcUtil.addCssAndJsByLibraryName( model, params.getFrontEndLibraries() );
        }

        return false;
    }

    public RestValidationResult delete( Long id ) throws NotFoundException {

        Boolean exist = mechanicService.existById( id );
        if( !exist ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }
}
