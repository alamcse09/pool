package com.dgt.paribahanpool.meeting.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.EmailDataBadRequestException;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.meeting.model.MeetingAddRequest;
import com.dgt.paribahanpool.meeting.service.MeetingModelService;
import com.dgt.paribahanpool.meeting.service.MeetingValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/meeting" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired})
public class MeetingController extends MVCController {

    private final MeetingModelService meetingModelService;
    private final MeetingValidationService meetingValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.meeting.add", content = "meeting/meeting-add", activeMenu = Menu.MEETING_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MEETING_ADD })
    public String addMeeting(
            Model model,
            HttpServletRequest httpServletRequest
    ){

        meetingModelService.addMeeting( model );
        return viewRoot;
    }

    @PostMapping( "/add" )
    public String addMeetingPost(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid MeetingAddRequest meetingAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ) throws EmailDataBadRequestException {

        if( !meetingValidationService.handleBindingResultForAddFormPost( model, bindingResult, meetingAddRequest ) ){

            return viewRoot;
        }

        if( meetingValidationService.addMeetingPost( redirectAttributes, meetingAddRequest ) ){

            meetingModelService.addMeetingPost( meetingAddRequest, redirectAttributes );
            log( LogEvent.MEETING_ADD, meetingAddRequest.getId(), "Meeting Added", request );
        }

        return "redirect:/meeting/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title="title.meeting.search", content="meeting/meeting-search", activeMenu = Menu.MEETING_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MEETING_SEARCH })
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ) {

        log.debug( "Meeting Search page called" );
        return viewRoot;
    }

    @GetMapping( "/meeting-minutes" )
    @TitleAndContent( title = "title.meeting.update-meeting-minutes", content = "meeting/meeting-minutes-update", activeMenu = Menu.MEETING_MINUTES_UPDATE )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.MEETING_MINUTES_UPDATE } )
    public String minutesUpdator(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering meeting minutes update page" );
        return viewRoot;
    }

    @PostMapping( "/meeting-minutes/update" )
    public String addMeetingMinutes(
            Model model,
            RedirectAttributes redirectAttributes,
            MeetingAddRequest meetingAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ) {

        if( !meetingValidationService.handleBindingResultForMeetingMinutes( model, bindingResult ) ){

            return viewRoot;
        }

        if( meetingValidationService.addMeetingMinutes( redirectAttributes, meetingAddRequest, getLoggedInUser().getUser().getId() ) ){

            meetingModelService.addMeetingMinutes( meetingAddRequest, redirectAttributes );
            log( LogEvent.MEETING_MINUTES_UPDATE, meetingAddRequest.getId(), "Meeting Minutes Added", request );
        }

        return "redirect:/meeting/meeting-minutes";
    }

    @GetMapping( "/{id}" )
    @TitleAndContent( title = "title.meeting.view", content = "meeting/meeting-view", activeMenu = Menu.MEETING_VIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.JQUERY_PRINT, FrontEndLibrary.MEETING_VIEW } )
    public String view(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ) {

        if( meetingValidationService.view( redirectAttributes, id ) ){

            meetingModelService.view( model, id );
            return viewRoot;
        }

        return "redirect:/meeting/search";
    }
}
