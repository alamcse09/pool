package com.dgt.paribahanpool.meeting.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.meeting.model.MeetingSearchResponse;
import com.dgt.paribahanpool.meeting.service.MeetingService;
import com.dgt.paribahanpool.meeting.service.MeetingValidationService;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/meeting" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class MeetingRestController extends BaseRestController {

    private final MeetingValidationService meetingValidationService;
    private final MeetingService meetingService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<MeetingSearchResponse> searchMeeting(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return meetingService.searchForDatatable( dataTablesInput );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = meetingValidationService.delete( id );
        if( restValidationResult.getSuccess() ){

            meetingService.delete( id );
            log( LogEvent.MEETING_DELETE, id, "", request );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @GetMapping(
            value = "/meeting-minutes",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<MeetingSearchResponse> searchMeetingMinutes(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return meetingService.searchForMeetingMinutesDatatable( dataTablesInput, getLoggedInUser().getUser().getId() );
    }
}
