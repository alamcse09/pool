package com.dgt.paribahanpool.meeting.model;

import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.workflow.model.State;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table( name = "meeting" )
@SQLDelete( sql = "UPDATE meeting SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class Meeting extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate date;

    @Column( name = "starting_time" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.timeFormat_hour_min_ampm )
    private LocalTime startingTime;

    @Column( name = "ending_time" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.timeFormat_hour_min_ampm )
    private LocalTime endingTime;

    @Column( name = "location", columnDefinition = "TEXT" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String location;

    @Column( name = "topic", columnDefinition = "TEXT" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String topic;

    @Lob
    @Column( name = "meetingAgenda" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String meetingAgenda;

    @Column( name = "document_group_id" )
    private Long docGroupId;

    @Column( name = "meeting_minutes_document_group_id" )
    private Long meetingMinutesDocGroupId;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "user_id", foreignKey = @ForeignKey( name = "fk_meeting_user_id" ) )
    private User meetingMinuteTaker;

    @OneToOne
    @JoinColumn( name = "state_id", foreignKey = @ForeignKey( name = "fk_meeting_state_id" ) )
    private State state;

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinTable(
            name = "meeting_meeting_participant_info_map",
            joinColumns = @JoinColumn( name = "meeting_id", foreignKey = @ForeignKey( name = "fk_meeting_participant_info_meeting_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "meeting_participant_info_id", foreignKey = @ForeignKey( name = "fk_meeting_id_meeting_participant_info_id" ) )
    )
    private Set<MeetingParticipantInfo> meetingParticipantInfoSet = new HashSet<>();
}