package com.dgt.paribahanpool.meeting.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Data
public class MeetingAddRequest {

    private Long id;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate date;

    @DateTimeFormat( pattern = DateTimeFormatPattern.timeFormat_hour_min_ampm )
    private LocalTime startingTime;

    @DateTimeFormat( pattern = DateTimeFormatPattern.timeFormat_hour_min_ampm )
    private LocalTime endingTime;

    @NotBlank( message = "{validation.common.required}" )
    private String location;

    @NotBlank( message = "{validation.common.required}" )
    private String topic;

    @NotBlank( message = "{validation.common.required}" )
    private String meetingAgenda;

    List<UserResponse> showEmployeeList;

    Long meetingMinuteTakeUserId;

    Long[] participantUserIdList;

    List<MeetingParticipantInfoAddRequest> meetingParticipantInfoAddRequestSet;

    private MultipartFile[] files;
    List<DocumentMetadata> documents = Collections.EMPTY_LIST;

    private MultipartFile[] meetingMinutesFiles;
    List<DocumentMetadata> meetingMinutesDocuments = Collections.EMPTY_LIST;
}
