package com.dgt.paribahanpool.meeting.model;

import lombok.Data;

import javax.validation.constraints.Digits;

@Data
public class MeetingParticipantInfoAddRequest {

    private Long id;

    private String nameEn;

    private String nameBn;

    @Digits( integer = 11, fraction = 0, message = "{validation.common.numeric}" )
    private String phoneNumber;

    private String designation;

    private String email;

    private String department;
}
