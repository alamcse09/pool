package com.dgt.paribahanpool.meeting.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MeetingParticipantInfoViewResponse {

    private Long id;
    private String nameEn;
    private String nameBn;
    private String phoneNumber;
    private String designation;
    private String email;
    private String department;
}
