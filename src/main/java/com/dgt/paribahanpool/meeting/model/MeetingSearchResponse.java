package com.dgt.paribahanpool.meeting.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Data
public class MeetingSearchResponse {

    private Long id;

    @JsonFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate date;

    @JsonFormat( pattern = DateTimeFormatPattern.timeFormat_hourhour_minmin_ampm )
    private LocalTime startingTime;

    @JsonFormat( pattern = DateTimeFormatPattern.timeFormat_hourhour_minmin_ampm )
    private LocalTime endingTime;

    private String location;
    private String topic;
    private String meetingAgenda;
    List<DocumentMetadata> documents;
    List<DocumentMetadata> meetingMinutes;
}
