package com.dgt.paribahanpool.meeting.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Data
@Builder
public class MeetingViewResponse {

    private Long id;
    private LocalDate date;
    private LocalTime startingTime;
    private LocalTime endingTime;
    private String location;
    private String topic;
    private String meetingAgenda;
    private String meetingMinuteTakerNameBn;
    private String meetingMinuteTakerNameEn;
    List<MeetingParticipantInfoViewResponse> meetingParticipantInfoViewList;
}
