package com.dgt.paribahanpool.meeting.service;

import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.exceptions.EmailDataBadRequestException;
import com.dgt.paribahanpool.meeting.model.Meeting;
import com.dgt.paribahanpool.meeting.model.MeetingAddRequest;
import com.dgt.paribahanpool.meeting.model.MeetingViewResponse;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class MeetingModelService {

    private final UserService userService;
    private final MeetingService meetingService;
    private final MvcUtil mvcUtil;

    public void addMeeting( Model model ){

        List<UserResponse> userResponseList = userService.getUserResponseList( UserType.SYSTEM_USER );

        MeetingAddRequest meetingAddRequest = new MeetingAddRequest();

        meetingAddRequest.setShowEmployeeList( userResponseList );

        model.addAttribute( "meetingAddRequest", meetingAddRequest );
    }

    @Transactional
    public void addMeetingPost( MeetingAddRequest meetingAddRequest, RedirectAttributes redirectAttributes ) throws EmailDataBadRequestException {

        Meeting meeting = meetingService.save( meetingAddRequest );

        meetingService.sendEmailToParticipant( meeting );

        meetingService.sendEmailToMinuteTaker( meeting );

        meetingService.sendNotificationForNewMeeting( meeting );

        mvcUtil.addSuccessMessage( redirectAttributes, "success.meeting.add" );
    }

    public void addMeetingMinutes( MeetingAddRequest meetingAddRequest, RedirectAttributes redirectAttributes ) {

        meetingService.saveMeetingMinutes( meetingAddRequest );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.meeting.meeting-minutes" );
    }

    public void view( Model model, Long id ) {

        MeetingViewResponse meetingViewResponse = meetingService.getMeetingViewResponseFromMeeting( id );
        model.addAttribute("meetingViewResponse", meetingViewResponse);
    }
}
