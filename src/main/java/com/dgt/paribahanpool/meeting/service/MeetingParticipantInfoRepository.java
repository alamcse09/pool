package com.dgt.paribahanpool.meeting.service;

import com.dgt.paribahanpool.meeting.model.MeetingParticipantInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MeetingParticipantInfoRepository extends JpaRepository<MeetingParticipantInfo, Long> {
}
