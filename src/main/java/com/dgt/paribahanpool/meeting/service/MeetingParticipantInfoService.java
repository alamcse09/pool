package com.dgt.paribahanpool.meeting.service;

import com.dgt.paribahanpool.meeting.model.MeetingParticipantInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class MeetingParticipantInfoService {

    private final MeetingParticipantInfoRepository meetingParticipantInfoRepository;

    public Optional<MeetingParticipantInfo> findById(Long id ){

        return meetingParticipantInfoRepository.findById( id );
    }
}
