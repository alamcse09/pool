package com.dgt.paribahanpool.meeting.service;

import com.dgt.paribahanpool.meeting.model.Meeting;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface MeetingRepository extends DataTablesRepository<Meeting, Long> {
}
