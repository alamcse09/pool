package com.dgt.paribahanpool.meeting.service;

import com.dgt.paribahanpool.document.model.Document;
import com.dgt.paribahanpool.document.model.DocumentData;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.exceptions.EmailDataBadRequestException;
import com.dgt.paribahanpool.meeting.model.*;
import com.dgt.paribahanpool.notification.model.*;
import com.dgt.paribahanpool.notification.service.NotificationService;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.ast.Not;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.dgt.paribahanpool.meeting.service.MeetingSpecification.findByUserId;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class MeetingService {

    private final MeetingRepository meetingRepository;
    private final MeetingParticipantInfoService meetingParticipantInfoService;
    private final UserService userService;
    private final DocumentService documentService;
    private final NotificationSender notificationSender;
    private final NotificationService notificationService;

    public Optional<Meeting> findById(Long id ) {

        return meetingRepository.findById( id );
    }

    public Meeting save( Meeting meeting ){

        return meetingRepository.save( meeting );
    }

    @Transactional
    public Meeting save( MeetingAddRequest meetingAddRequest ) {

        Meeting meeting = new Meeting();

        if( meetingAddRequest.getId() != null ){

            meeting = findById( meetingAddRequest.getId() ).get();
        }

        meeting = getMeetingFromMeetingAddRequest( meeting, meetingAddRequest );

        return save( meeting );
    }

    private Meeting getMeetingFromMeetingAddRequest( Meeting meeting, MeetingAddRequest meetingAddRequest ) {

        Optional<User> meetingMinuteTaker = userService.findById( meetingAddRequest.getMeetingMinuteTakeUserId() );

        meeting.setDate( meetingAddRequest.getDate() );
        meeting.setStartingTime( meetingAddRequest.getStartingTime() );
        meeting.setEndingTime( meetingAddRequest.getEndingTime() );
        meeting.setLocation( meetingAddRequest.getLocation() );
        meeting.setTopic( meetingAddRequest.getTopic() );
        meeting.setMeetingAgenda( meetingAddRequest.getMeetingAgenda() );

        if( meetingMinuteTaker.isPresent() ){

            meeting.setMeetingMinuteTaker( meetingMinuteTaker.get() );
        }

        if( meetingAddRequest.getMeetingParticipantInfoAddRequestSet() != null  ){

            Set<MeetingParticipantInfo> meetingParticipantPublicInfoSet =
                    meetingAddRequest.getMeetingParticipantInfoAddRequestSet()
                    .stream()
                    .map( meetingParticipantInfoAddRequest -> getMeetingParticipantInfoFromMeetingParticipantInfoRequest( meetingParticipantInfoAddRequest, meeting ) )
                    .filter( meetingParticipantInfo -> meetingParticipantInfo != null )
                    .collect( Collectors.toSet() );

            meeting.getMeetingParticipantInfoSet().clear();
            meeting.getMeetingParticipantInfoSet().addAll( meetingParticipantPublicInfoSet );
        }

        if( meetingAddRequest.getParticipantUserIdList() != null && meetingAddRequest.getParticipantUserIdList().length > 0 ){

            List<User> userList = userService.getUserListFromParticipantIdList( meetingAddRequest.getParticipantUserIdList() );

            List<MeetingParticipantInfoAddRequest> meetingParticipantInfoAddRequestSet = getMeetingParticipantInfoAddRequestSetFromUserList( userList );

            meetingAddRequest.setMeetingParticipantInfoAddRequestSet( meetingParticipantInfoAddRequestSet );

            Set<MeetingParticipantInfo> meetingParticipantEmployeeInfoSet =
                    meetingAddRequest.getMeetingParticipantInfoAddRequestSet()
                            .stream()
                            .map( meetingParticipantInfoAddRequest -> getMeetingParticipantInfoFromMeetingParticipantInfoRequest( meetingParticipantInfoAddRequest, meeting ) )
                            .filter( meetingParticipantInfo -> meetingParticipantInfo != null )
                            .collect( Collectors.toSet() );

            meeting.getMeetingParticipantInfoSet().addAll( meetingParticipantEmployeeInfoSet );
        }

        if( meeting.getDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( meetingAddRequest.getFiles(), Collections.EMPTY_MAP );
            meeting.setDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( meetingAddRequest.getFiles(), meeting.getDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        return meeting;

    }

    private List<MeetingParticipantInfoAddRequest> getMeetingParticipantInfoAddRequestSetFromUserList( List<User> userList ) {

        List<MeetingParticipantInfoAddRequest> meetingParticipantInfoAddRequestSet = new ArrayList<>();

        for( User user: userList ){

            MeetingParticipantInfoAddRequest meetingParticipantInfoAddRequest = new MeetingParticipantInfoAddRequest();
            meetingParticipantInfoAddRequest.setNameBn( user.getEmployee().getName() );
            meetingParticipantInfoAddRequest.setNameEn( user.getEmployee().getNameEn() );
            meetingParticipantInfoAddRequest.setPhoneNumber( user.getEmployee().getMobileNumber() );
            meetingParticipantInfoAddRequest.setDesignation( user.getEmployee().getDesignation().getNameBn() );
            meetingParticipantInfoAddRequest.setEmail( user.getEmployee().getEmail() );
            meetingParticipantInfoAddRequest.setDepartment( user.getEmployee().getOfficeMinistry() );

            meetingParticipantInfoAddRequestSet.add( meetingParticipantInfoAddRequest );
        }

        return meetingParticipantInfoAddRequestSet;
    }

    private MeetingParticipantInfo getMeetingParticipantInfoFromMeetingParticipantInfoRequest( MeetingParticipantInfoAddRequest meetingParticipantInfoAddRequest, Meeting meeting ) {

        MeetingParticipantInfo meetingParticipantInfo = new MeetingParticipantInfo();

        if( meetingParticipantInfoAddRequest.getId() != null && meetingParticipantInfoAddRequest.getId() > 0 ){

            Optional<MeetingParticipantInfo> meetingParticipantInfoOptional = meetingParticipantInfoService.findById( meetingParticipantInfoAddRequest.getId() );
            if( meetingParticipantInfoOptional.isPresent() ){

                meetingParticipantInfo = meetingParticipantInfoOptional.get();
            }
            else{

                return null;
            }
        }

        meetingParticipantInfo.setNameBn( meetingParticipantInfoAddRequest.getNameBn() );
        meetingParticipantInfo.setNameEn( meetingParticipantInfoAddRequest.getNameEn() );
        meetingParticipantInfo.setPhoneNumber( meetingParticipantInfoAddRequest.getPhoneNumber() );
        meetingParticipantInfo.setDesignation( meetingParticipantInfoAddRequest.getDesignation() );
        meetingParticipantInfo.setEmail( meetingParticipantInfoAddRequest.getEmail() );
        meetingParticipantInfo.setDepartment( meetingParticipantInfoAddRequest.getDepartment() );

        return meetingParticipantInfo;
    }

    public DataTablesOutput<MeetingSearchResponse> searchForDatatable( DataTablesInput dataTablesInput ) {

        return meetingRepository.findAll( dataTablesInput, this::getMeetingSearchResponseFromMeeting );
    }

    private MeetingSearchResponse getMeetingSearchResponseFromMeeting( Meeting meeting ) {

        MeetingSearchResponse meetingSearchResponse = new MeetingSearchResponse();

        meetingSearchResponse.setId( meeting.getId() );
        meetingSearchResponse.setDate( meeting.getDate() );
        meetingSearchResponse.setStartingTime( meeting.getStartingTime() );
        meetingSearchResponse.setEndingTime( meeting.getEndingTime() );
        meetingSearchResponse.setLocation( meeting.getLocation() );
        meetingSearchResponse.setTopic( meeting.getTopic() );
        meetingSearchResponse.setMeetingAgenda( meeting.getMeetingAgenda() );

        List<DocumentMetadata> documentdataList = documentService.findDocumentMetaDataListByGroupId( meeting.getDocGroupId() );
        meetingSearchResponse.setDocuments( documentdataList );

        List<DocumentMetadata> documentdataListMeeting = documentService.findDocumentMetaDataListByGroupId( meeting.getMeetingMinutesDocGroupId() );
        meetingSearchResponse.setMeetingMinutes( documentdataListMeeting );

        return meetingSearchResponse;
    }

    public Boolean existById( Long id ) {

        return meetingRepository.existsById( id );
    }

    public void delete( Long id ) {

        meetingRepository.deleteById( id );
    }

    public DataTablesOutput<MeetingSearchResponse> searchForMeetingMinutesDatatable( DataTablesInput dataTablesInput, Long userId ) {

        return meetingRepository.findAll( dataTablesInput, null, findByUserId( userId ), this::getMeetingSearchResponseFromMeeting );
    }

    public void saveMeetingMinutes( MeetingAddRequest meetingAddRequest ) {

        Meeting meeting = findById( meetingAddRequest.getId() ).get();

        if( meeting.getMeetingMinutesDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( meetingAddRequest.getMeetingMinutesFiles(), Collections.EMPTY_MAP );
            meeting.setMeetingMinutesDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( meetingAddRequest.getMeetingMinutesFiles(), meeting.getMeetingMinutesDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        save( meeting );
    }

    @Transactional
    public MeetingViewResponse getMeetingViewResponseFromMeeting( Long id ) {

        Meeting meeting = findById( id ).get();

        String minuteTakerNameBn = meeting.getMeetingMinuteTaker().getEmployee().getName();
        String minuteTakerNameEn = meeting.getMeetingMinuteTaker().getEmployee().getNameEn();

        MeetingViewResponse meetingViewResponse = MeetingViewResponse.builder()
                .id( meeting.getId() )
                .date( meeting.getDate() )
                .startingTime( meeting.getStartingTime() )
                .endingTime( meeting.getEndingTime() )
                .location( meeting.getLocation() )
                .topic( meeting.getTopic() )
                .meetingAgenda( meeting.getMeetingAgenda() )
                .meetingMinuteTakerNameBn( minuteTakerNameBn )
                .meetingMinuteTakerNameEn( minuteTakerNameEn )
                .meetingParticipantInfoViewList(
                        meeting.getMeetingParticipantInfoSet().stream()
                        .map(
                                meetingParticipantInfo ->
                                        MeetingParticipantInfoViewResponse.builder()
                                .id( meetingParticipantInfo.getId() )
                                .nameEn( meetingParticipantInfo.getNameEn() )
                                .nameBn( meetingParticipantInfo.getNameBn() )
                                .phoneNumber( meetingParticipantInfo.getPhoneNumber() )
                                .designation( meetingParticipantInfo.getDesignation() )
                                .email( meetingParticipantInfo.getEmail() )
                                .department( meetingParticipantInfo.getDepartment() )
                                .build()
                        )
                        .collect( Collectors.toList() )
                )
                .build();

        return meetingViewResponse;
    }

    public void sendEmailToParticipant( Meeting meeting ) throws EmailDataBadRequestException {

        Set<MeetingParticipantInfo> meetingParticipantInfoSet = meeting.getMeetingParticipantInfoSet();


        for( MeetingParticipantInfo meetingParticipantInfo: meetingParticipantInfoSet ) {

            List<DocumentData> documents = Collections.EMPTY_LIST;

            Long id = meetingParticipantInfo.getId();
            String email = meetingParticipantInfo.getEmail();
            LocalDate date = meeting.getDate();
            LocalTime startingTime = meeting.getStartingTime();
            LocalTime endingTime = meeting.getEndingTime();
            String location = meeting.getLocation();
            String topic = meeting.getTopic();
            String meetingAgenda = meeting.getMeetingAgenda();

            if( meeting.getDocGroupId() != null && meeting.getDocGroupId() > 0 ){

                List<DocumentData> documentMetadataList = documentService.findDocumentDataListByGroupId( meeting.getDocGroupId() );
                documents = documentMetadataList;
            }

            Map<String, Object> params = new HashMap<>();

            params.put("participantId", id);
            params.put("date", date);
            params.put("startingTime", startingTime);
            params.put("endingTime", endingTime);
            params.put("location", location);
            params.put("topic", topic);
            params.put("meetingAgenda", meetingAgenda);

            EmailData emailData = EmailData.builder()
                    .to(new String[]{email})
                    .isHtml(true)
                    .templatePath("email/meeting/call-to-participant")
                    .params(params)
                    .attachmentList(
                            documents.stream()
                                    .map(
                                            this::getEmailAttachmentFromDocumentMetaData
                                    )
                                    .collect( Collectors.toList() )
                    )
                    .subject("Invitation For Meeting - Paribahan Pool")
                    .build();

            notificationSender.sendEmail(emailData);

        }

    }

    private EmailAttachment getEmailAttachmentFromDocumentMetaData( DocumentData documentData ) {

        return EmailAttachment.builder()
                .name( documentData.getName() )
                .contentType( documentData.getContentType() )
                .data( documentData.getData() )
                .build();
    }

    public void sendEmailToMinuteTaker( Meeting meeting ) throws EmailDataBadRequestException {

        User minuteTaker = meeting.getMeetingMinuteTaker();

        Long id = minuteTaker.getId();
        String email = minuteTaker.getUsername();
        LocalDate date = meeting.getDate();
        LocalTime startingTime = meeting.getStartingTime();
        LocalTime endingTime = meeting.getEndingTime();
        String location = meeting.getLocation();
        String topic = meeting.getTopic();
        String meetingAgenda = meeting.getMeetingAgenda();

        Map<String, Object> params = new HashMap<>();

        params.put("participantId", id);
        params.put("date", date);
        params.put("startingTime", startingTime);
        params.put("endingTime", endingTime);
        params.put("location", location);
        params.put("topic", topic);
        params.put("meetingAgenda", meetingAgenda);

        EmailData emailData = EmailData.builder()
                .to(new String[]{email})
                .isHtml(true)
                .templatePath("email/meeting/minute-taker")
                .params(params)
                .subject("Invitation To Attend Meeting As An Minute Taker - Paribahan Pool")
                .build();

        notificationSender.sendEmail(emailData);
    }

    public void sendNotificationForNewMeeting(Meeting meeting) {

        Notification notification = Notification.of(
                NotificationType.MEETING_MINUTES_TAKER_ASSIGNED,
                "Meeting",
                "মিটিং",
                "You have assigned as meeting minutes taker of a meeting",
                "আপনি একটি মিটিং এর মিনিট লেখক হিসেবে দায়িত্ব পেয়েছেন",
                "meeting/meeting-minutes",
                "meeting",
                meeting.getId(),
                meeting.getMeetingMinuteTaker()
        );

        notificationService.save( notification );
    }

    public static NotificationMetadata getNotificationMetadata() {

        return NotificationMetadata.builder()
                .titleEn( "Meeting workflow" )
                .titleBn( "মিটিং আবেদন ওয়ার্ক ফ্লো " )
                .textEn( "A Meeting request is waiting for your action" )
                .textBn( "একটি মিটিং আবেদন আপনার একশন এর জন্য অপেক্ষমান অবস্থায় আছে" )
                .redirectUrl( "meeting/{id}" )
                .icon( "flaticon2-group text-success" )
                .build();
    }
}
