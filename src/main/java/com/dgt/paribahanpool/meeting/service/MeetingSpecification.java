package com.dgt.paribahanpool.meeting.service;

import com.dgt.paribahanpool.meeting.model.Meeting;
import com.dgt.paribahanpool.meeting.model.Meeting_;
import com.dgt.paribahanpool.user.model.User_;
import org.springframework.data.jpa.domain.Specification;

public class MeetingSpecification {

    public static Specification<Meeting> findByUserId( Long userId ){

        if( userId == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) ->  criteriaBuilder.equal( root.get( Meeting_.MEETING_MINUTE_TAKER ).get( User_.ID ), userId );
    }
}
