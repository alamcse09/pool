package com.dgt.paribahanpool.meeting.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.meeting.model.Meeting;
import com.dgt.paribahanpool.meeting.model.MeetingAddRequest;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

import java.time.LocalDate;
import java.time.LocalTime;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class MeetingValidationService {

    private final MvcUtil mvcUtil;
    private final MeetingService meetingService;

    public Boolean handleBindingResultForAddFormPost( Model model, BindingResult bindingResult, MeetingAddRequest meetingAddRequest ) {

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "meetingAddRequest" )
                        .object( meetingAddRequest )
                        .title( "title.meeting.add" )
                        .content( "meeting/meeting-add" )
                        .activeMenu( Menu.MEETING_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.MEETING_ADD } )
                        .build()
        );
    }

    public Boolean addMeetingPost(RedirectAttributes redirectAttributes, MeetingAddRequest meetingAddRequest ) {

        Integer systemUserParticipantLength = new Integer(0);
        Integer publicUserParticipantLength = new Integer(0);

        LocalTime startTime = meetingAddRequest.getStartingTime();
        LocalTime endTime = meetingAddRequest.getEndingTime();


        if( meetingAddRequest.getParticipantUserIdList() != null && meetingAddRequest.getParticipantUserIdList().length > 0 ) {

            systemUserParticipantLength = meetingAddRequest.getParticipantUserIdList().length;
        }

        if( meetingAddRequest.getMeetingParticipantInfoAddRequestSet() != null  ) {

            publicUserParticipantLength = meetingAddRequest.getMeetingParticipantInfoAddRequestSet().size();
        }

        if( ( systemUserParticipantLength + publicUserParticipantLength ) <= 1 ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.meeting.not-enough-participant" );
            return false;
        }

        if( startTime.compareTo( endTime ) >= 1 ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.meeting.time" );
            return false;
        }

        return true;
    }

    public RestValidationResult delete( Long id ) throws NotFoundException {

        Boolean exist = meetingService.existById( id );
        if( !exist ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean handleBindingResultForMeetingMinutes( Model model, BindingResult bindingResult ) {

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .title( "title.meeting.update-meeting-minutes" )
                        .content( "meeting/meeting-minutes-update" )
                        .activeMenu( Menu.MEETING_MINUTES_UPDATE )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.MEETING_MINUTES_UPDATE } )
                        .build()
        );
    }

    @Transactional
    public Boolean addMeetingMinutes( RedirectAttributes redirectAttributes, MeetingAddRequest meetingAddRequest, Long userId ) {

        if( meetingAddRequest.getId() != null ){

            Optional<Meeting> meetingOptional = meetingService.findById( meetingAddRequest.getId() );

            if( !meetingOptional.isPresent() ){

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
                return false;
            }
            if( meetingOptional.get().getMeetingMinuteTaker() != null && meetingOptional.get().getMeetingMinuteTaker().getId() > 0 && !meetingOptional.get().getMeetingMinuteTaker().getId().equals( userId ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "validation.meeting.not-valid-minute-taker" );
                return false;
            }
        }

        return true;
    }

    public Boolean view( RedirectAttributes redirectAttributes, Long id ) {

        Optional<Meeting> meetingOptional = meetingService.findById( id );

        if( !meetingOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }
}
