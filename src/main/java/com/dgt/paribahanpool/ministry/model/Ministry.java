package com.dgt.paribahanpool.ministry.model;

import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "ministry" )
public class Ministry extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    @Column( name = "name_en" )
    private String nameEn;

    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    @Column( name = "name_bn" )
    private String nameBn;

    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    @Column( name = "name_short" )
    private String shortName;

    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    @Column( name = "reference_code" )
    private String referenceCode;
}
