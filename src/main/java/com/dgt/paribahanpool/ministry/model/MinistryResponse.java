package com.dgt.paribahanpool.ministry.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MinistryResponse {

    private Long id;
    private String nameEn;
    private String nameBn;
}
