package com.dgt.paribahanpool.ministry.service;

import com.dgt.paribahanpool.ministry.model.Ministry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MinistryRepository extends JpaRepository<Ministry,Long> {
}
