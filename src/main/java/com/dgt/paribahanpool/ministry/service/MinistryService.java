package com.dgt.paribahanpool.ministry.service;

import com.dgt.paribahanpool.ministry.model.Ministry;
import com.dgt.paribahanpool.ministry.model.MinistryResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class MinistryService {

    private final MinistryRepository ministryRepository;

    public List<Ministry> findAll(){

        return ministryRepository.findAll();
    }

    @Transactional( propagation = Propagation.REQUIRED )
    public List<MinistryResponse> findAllMinistryResponse(){

        return findAll().stream().map( this::getMinistryResponseFromMinistry ).collect( Collectors.toList() );
    }

    private MinistryResponse getMinistryResponseFromMinistry( Ministry ministry ) {

        return MinistryResponse.builder()
                .id( ministry.getId() )
                .nameBn( ministry.getNameBn() )
                .nameEn( ministry.getNameEn() )
                .build();
    }

    public Ministry getReference( Long id ){

        return ministryRepository.getById( id );
    }

    public Ministry findById(Long id) {
        return ministryRepository.findById(id).get();
    }
}
