package com.dgt.paribahanpool.noc_application.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.noc_application.model.NocAdditionalActionData;
import com.dgt.paribahanpool.noc_application.model.NocApplication;
import com.dgt.paribahanpool.noc_application.model.NocApplicationAddRequest;
import com.dgt.paribahanpool.noc_application.service.NocApplicationModelService;
import com.dgt.paribahanpool.noc_application.service.NocApplicationValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/noc" )
@RequiredArgsConstructor ( onConstructor_ = {@Autowired } )
public class NocApplicationController extends MVCController {

    private final NocApplicationModelService nocApplicationModelService;
    private final NocApplicationValidationService nocApplicationValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.noc.add", content = "noc/noc-add",activeMenu = Menu.NOC_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.NOC_ADD_FORM } )
    public String add(
            HttpServletRequest httpServletRequest,
            Model model
    ) {

        nocApplicationModelService.addNocGet( model, getLoggedInUser() );
        return viewRoot;
    }

    @GetMapping( "/{id}" )
    @TitleAndContent( title = "title.noc.view", content = "noc/noc-view", activeMenu = Menu.NOC_APPLICATION_VIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.JQUERY_PRINT, FrontEndLibrary.NOC_VIEW } )
    public String view(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ){

        if( nocApplicationValidationService.view( redirectAttributes, id ) ) {

            nocApplicationModelService.view( model, id, getLoggedInUser(), httpServletRequest );
            return viewRoot;
        }

        return "redirect:/noc/search";
    }

    @GetMapping( "/create/{id}" )
    @TitleAndContent( title = "title.noc.view", content = "noc/noc-create", activeMenu = Menu.NOC_APPLICATION_VIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.JQUERY_PRINT, FrontEndLibrary.NOC_VIEW, FrontEndLibrary.LETTER_PRINT } )
    public String create(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ){

        if( nocApplicationValidationService.create( redirectAttributes, id ) ){

            nocApplicationModelService.create( model, id, getLoggedInUser(), httpServletRequest );
            return "noc/noc-create";
        }

        return "redirect:/noc/search";
    }

    @PostMapping( "/add" )
    public String addNocApplication(
            RedirectAttributes redirectAttributes,
            Model model,
            @Valid NocApplicationAddRequest nocApplicationAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){
        if( !nocApplicationValidationService.handleBindingResultForAddFormPost(model,bindingResult,nocApplicationAddRequest) ) {

            return viewRoot;
        }

        if( nocApplicationValidationService.validatePostRequest(redirectAttributes, nocApplicationAddRequest) ) {

            NocApplication nocApplication = nocApplicationModelService.addNocPost(nocApplicationAddRequest, model);
            log( LogEvent.NOC_APPLICATION_ADDED, nocApplication.getId(), "NOC added", request );
        }

        return "redirect:/noc/my-search";
    }

    @GetMapping( "/edit/{nocId}")
    @TitleAndContent( title = "title.noc.add", content = "noc/noc-add",activeMenu = Menu.NOC_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.NOC_ADD_FORM } )
    public String edit(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable("nocId") Long nocId
    ){
        if( nocApplicationValidationService.validateEdit(redirectAttributes, nocId) ) {

            nocApplicationModelService.editNoc( model, nocId );
            return viewRoot;
        }
        return "redirect:/noc/my-search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.noc.search", content = "noc/noc-search", activeMenu = Menu.NOC_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.NOC_SEARCH })
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ){

        return viewRoot;
    }

    @GetMapping( value="/my-search" )
    @TitleAndContent( title = "title.noc.search", content = "noc/noc-my-search", activeMenu = Menu.NOC_MY_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.NOC_MY_SEARCH })
    public String getMySearchPage(
            HttpServletRequest request,
            Model model
    ){

        return viewRoot;
    }


    @PostMapping( "/take-action" )
    @TitleAndContent( title = "title.noc.search", content = "noc/noc-search", activeMenu = Menu.NOC_SEARCH )
    public String takeAction(
            @RequestParam( "id" ) Long id,
            @RequestParam( "action" ) Long actionId,
            NocAdditionalActionData nocAdditionalActionData,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ) throws Exception {

        if( nocApplicationValidationService.takeAction( redirectAttributes, id, actionId, getLoggedInUser(), nocAdditionalActionData ) ){

            nocApplicationModelService.takeAction( redirectAttributes, id, actionId, nocAdditionalActionData, getLoggedInUser() );
            log( LogEvent.NOC_APPLICATION_TAKE_ACTION, actionId, "NOC Take action", request );
        }
        else{

            return "redirect:/noc/" + id;
        }

        return "redirect:/noc/search";
    }
}
