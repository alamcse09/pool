package com.dgt.paribahanpool.noc_application.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.noc_application.model.NocSearchResponse;
import com.dgt.paribahanpool.noc_application.service.NocApplicationService;
import com.dgt.paribahanpool.noc_application.service.NocApplicationValidationService;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/noc" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class NocApplicationRestController extends BaseRestController {

    private final NocApplicationService nocApplicationService;
    private final NocApplicationValidationService nocApplicationValidationService;

    @GetMapping(
            value="/my-search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<NocSearchResponse> searchMyVehicleNoc(
            DataTablesInput dataTablesInput, HttpServletRequest request
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return nocApplicationService.searchForMyDatatable( dataTablesInput, getLoggedInUser().getUser().getId(), request );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = nocApplicationValidationService.delete( id );
        if( restValidationResult.getSuccess() ) {

            nocApplicationService.delete( id );
            log( LogEvent.NOC_DELETE, id, "", request );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{
            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<NocSearchResponse> searchVehicle(
            DataTablesInput dataTablesInput,
            HttpServletRequest request
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return nocApplicationService.searchForDatatable( dataTablesInput, getLoggedInUser(), request );
    }
}
