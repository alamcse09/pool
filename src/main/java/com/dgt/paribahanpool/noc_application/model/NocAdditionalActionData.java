package com.dgt.paribahanpool.noc_application.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.workflow.model.WorkflowAdditionalData;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;
import java.util.Set;

@Data
public class NocAdditionalActionData implements WorkflowAdditionalData {

    private Long mechanicInchargeId;
    private Double collectedFine;
    private Double finedAmount;
    private Boolean hasVehicleUdoOa;
    private Boolean isApprovedMechanic;
    private Boolean isApprovedAtoUd;
    private Set<Long> vehicleIds;

    private transient MultipartFile[] nocIssueFiles;
    private List<DocumentMetadata> nocIssueDocuments = Collections.EMPTY_LIST;
}
