package com.dgt.paribahanpool.noc_application.model;

import com.dgt.paribahanpool.enums.NocApplicationType;
import com.dgt.paribahanpool.enums.VehicleAllotment;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.WorkflowEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table (name ="noc_application")
public class NocApplication extends AuditableEntity implements WorkflowEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name="name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String name;

    @Column( name="identity_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String identityNumber;

    @Column( name="designation" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String designation;

    @Column( name="working_place" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String workingPlace;

    @Column( name="email" )
    @Email
    private String eMail;

    @Column( name="mobile_number" )
    @Convert( converter = EscapeHtmlConverter.class )
    @Convert( converter = StringTrimConverter.class )
    private String mobileNumber;

    @Column( name="home" )
    @Convert( converter = EscapeHtmlConverter.class )
    @Convert( converter = StringTrimConverter.class )
    private String home;

    @Column( name="date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate date;

    @Column( name="prl_accept_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate prlAcceptDate;

    @Column( name = "prl_accept_identity_number" )
    @Convert( converter = EscapeHtmlConverter.class )
    @Convert( converter = StringTrimConverter.class )
    private String prlAcceptIdentityNumber;

    @Column( name="debt_accept_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate debtAcceptDate;

    @Column( name = "debt_accept_identity_number" )
    @Convert( converter = EscapeHtmlConverter.class )
    @Convert( converter = StringTrimConverter.class )
    private String debtAcceptIdentityNumber;

    @Column( name = "noc_application_type")
    private NocApplicationType nocApplicationType;

    @Column( name = "vehicle_allotment")
    private VehicleAllotment vehicleAllotment;

    @Column( name="allotted_car_registration_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String allottedCarRegistrationNumber;

    @Column( name = "document_group_id" )
    private Long docGroupId;

    @Column( name = "mechanic_incharge_id" )
    private Long mechanicInchargeId;

    @Column( name = "fined_amount" )
    private Double finedAmount;

    @Column( name = "collected_fine" )
    private Double collectedFine;

    @Column( name = "approved_date" )
    private LocalDate approvedDate;

    @Column( name = "distribution", columnDefinition = "TEXT" )
    private String distribution;

    @Column( name = "has_vehicle_udo_oa" )
    private Boolean hasVehicleUdoOa;

    @Column( name = "is_approved_mechanic" )
    private Boolean isApprovedMechanic;

    @Column( name = "is_found_vehicle_ato_ud" )
    private Boolean isApprovedAtoUd;

    @Column( name = "noc_isssue_group_id" )
    private Long nocIssueGroupId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "approved_by_id", foreignKey = @ForeignKey( name = "fk_noc_application_approved_by_id" ) )
    private User approvedBy;

    @OneToOne
    @JoinColumn( name = "state_id", foreignKey = @ForeignKey( name = "fk_noc_app_state_id" ) )
    private State state;

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = false )
    @JoinTable(
            name = "noc_vehicle_map",
            joinColumns = @JoinColumn( name = "noc_application_id", foreignKey = @ForeignKey( name = "fk_noc_vehicle_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "vehicle_id", foreignKey = @ForeignKey( name = "fk_vehicle_id_noc_map_noc_application_id" ) )
    )
    private Set<Vehicle> additionalVehicleSet = new HashSet<>();

    @Transient
    private String moduleName = "noc";
}
