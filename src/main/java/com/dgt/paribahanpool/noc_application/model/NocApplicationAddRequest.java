package com.dgt.paribahanpool.noc_application.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.NocApplicationType;
import com.dgt.paribahanpool.enums.VehicleAllotment;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
public class NocApplicationAddRequest {

    public Long id;

    @NotNull( message = "{validation.common.required}")
    public String name;

    @NotNull( message = "{validation.common.required}")
    public String identityNumber;

    public String designation;

    private String home;

    public String workingPlace;

    @Email ( message = "{validation.common.email}")
    public String eMail;

    public String mobileNumber;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    public LocalDate date;

    private String prlAcceptIdentityNumber;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate prlAcceptDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate debtAcceptDate;

    private String debtAcceptIdentityNumber;

    public NocApplicationType nocApplicationType;

    public VehicleAllotment vehicleAllotment;

    private String allottedCarRegistrationNumber;


    public MultipartFile[] files;

    List<DocumentMetadata> documents = Collections.EMPTY_LIST;
}
