package com.dgt.paribahanpool.noc_application.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.NocApplicationType;
import com.dgt.paribahanpool.enums.StateType;
import com.dgt.paribahanpool.enums.VehicleAllotment;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.vehicle.model.VehicleViewResponse;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class NocApplicationViewResponse {

    private Long id;
    private Long stateId;
    public String name;
    public String serviceId;
    public String designation;
    public String workplace;
    public String email;
    public String phoneNo;
    public LocalDate date;
    public VehicleAllotment vehicleAllotment;
    private String allottedCarRegistrationNumber;
    public NocApplicationType nocApplicationType;
    public StateType stateType;
    public Double finedAmount;
    private DocumentMetadata signMetaData;
    private LocalDate prlAcceptDate;
    private String prlAcceptIdentityNumber;
    private LocalDate debtAcceptDate;
    private String debtAcceptIdentityNumber;
    private LocalDate creationDate;
    private LocalDate approvedDate;
    private UserResponse approvedBy;
    private String distribution;
    private List<VehicleViewResponse> vehicleViewResponseList;
    private List<VehicleViewResponse> newlyAddedvehicleViewResponseList;
    private Boolean hasVehicleUdoOa;
    private Boolean isApprovedMechanic;
    private Boolean isApprovedAtoUd;
}
