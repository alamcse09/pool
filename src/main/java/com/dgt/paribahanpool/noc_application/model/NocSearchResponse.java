package com.dgt.paribahanpool.noc_application.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.NocApplicationType;
import com.dgt.paribahanpool.enums.VehicleAllotment;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.workflow.model.StateResponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class NocSearchResponse {

    private Long id;
    public String name;
    public String identityNumber;
    public String designation;
    public String workingPlace;
    public String eMail;
    public String mobileNumber;

    public String date;

    public String nocApplicationType;
    public String vehicleAllotment;
    private String allottedCarRegistrationNumber;

    private StateResponse stateResponse;
    private String statusEn;

    private List<DocumentMetadata> documentMetadataList;
}
