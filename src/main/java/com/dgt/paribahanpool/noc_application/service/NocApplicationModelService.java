package com.dgt.paribahanpool.noc_application.service;

import com.dgt.paribahanpool.employee.model.Employee;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.mechanic.service.MechanicService;
import com.dgt.paribahanpool.noc_application.model.NocAdditionalActionData;
import com.dgt.paribahanpool.noc_application.model.NocApplication;
import com.dgt.paribahanpool.noc_application.model.NocApplicationAddRequest;
import com.dgt.paribahanpool.noc_application.model.NocApplicationViewResponse;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.PublicUserInfo;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.*;
import com.dgt.paribahanpool.vehicle.service.VehicleOwnerHistoryService;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import com.dgt.paribahanpool.workflow.model.StateActionMapResponse;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.dgt.paribahanpool.vehicle.service.VehicleSpecification.filterByVehicleOwnerId;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class NocApplicationModelService {

    private final MvcUtil mvcUtil;
    private final NocApplicationService nocApplicationService;
    private final StateActionMapService stateActionMapService;
    private final MechanicService mechanicService;
    private final VehicleService vehicleService;
    private final VehicleOwnerHistoryService vehicleOwnerHistoryService;
    private final UserService userService;

    public void addNocGet( Model model, UserPrincipal loggedInUser ) {

        User user = loggedInUser.getUser();

        if (user.getUserType().equals(UserType.PUBLIC)) {

            PublicUserInfo publicUserInfo = user.getPublicUserInfo();

            if( publicUserInfo!= null ) {

                NocApplicationAddRequest nocApplicationAddRequest = new NocApplicationAddRequest();

                nocApplicationAddRequest.setName( publicUserInfo.getNameEn() );
                nocApplicationAddRequest.setIdentityNumber( publicUserInfo.getServiceId() );
                nocApplicationAddRequest.setDesignation( publicUserInfo.getDesignationEn() );
                nocApplicationAddRequest.setWorkingPlace( publicUserInfo.getWorkplace() );
                nocApplicationAddRequest.setEMail( publicUserInfo.getEmail() );
                nocApplicationAddRequest.setMobileNumber( publicUserInfo.getPhoneNo() );
                nocApplicationAddRequest.setDate( LocalDate.now() );
                model.addAttribute("nocApplicationAddRequest", nocApplicationAddRequest);
            }

        } else {

            Employee employeeInfo = user.getEmployee();

            if( employeeInfo != null) {

                NocApplicationAddRequest nocApplicationAddRequest = new NocApplicationAddRequest();

                nocApplicationAddRequest.setName( employeeInfo.getNameEn() );
                nocApplicationAddRequest.setIdentityNumber( employeeInfo.getEmployeeID() );
                nocApplicationAddRequest.setDesignation(employeeInfo.getDesignation() == null ? "N/A" : employeeInfo.getDesignation().getNameEn() );
                nocApplicationAddRequest.setWorkingPlace( employeeInfo.getPresentAddress() );
                nocApplicationAddRequest.setEMail( employeeInfo.getEmail() );
                nocApplicationAddRequest.setMobileNumber( employeeInfo.getMobileNumber() );
                nocApplicationAddRequest.setDate( LocalDate.now() );
                model.addAttribute("nocApplicationAddRequest", nocApplicationAddRequest);
            }
        }
    }

    public NocApplication addNocPost( NocApplicationAddRequest nocApplicationAddRequest, Model model ) {

        NocApplication nocApplication = nocApplicationService.save(nocApplicationAddRequest);

        log.debug( "NOC application saved. Id {}", nocApplication.getId() );
        mvcUtil.addSuccessMessage( model, "noc.add.success" );

        return nocApplication;
    }

    public void editNoc( Model model, Long nocId) {

        Optional<NocApplication> nocApplication = nocApplicationService.findById( nocId );

        if(nocApplication.isPresent()) {

            NocApplicationAddRequest nocApplicationAddRequest = nocApplicationService.getNocApplicationAddRequestFromNocApplication( nocApplication.get() );

            model.addAttribute( "nocApplicationAddRequest", nocApplicationAddRequest);
        }
    }

    @Transactional
    public void view(Model model, Long id, UserPrincipal loggedInUser, HttpServletRequest request) {

        Optional<NocApplication> nocApplication = nocApplicationService.findById( id );

        if( nocApplication.isPresent() ) {

            NocApplicationViewResponse nocApplicationViewResponse = nocApplicationService.getNocApplicationViewResponseFromNocApplication( nocApplication.get(), request );

            List<VehicleViewResponse> vehicleViewResponseList = new ArrayList<>();

            Optional<User> optionalUser = userService.findById( nocApplication.get().getCreatedBy() );

            if( optionalUser.isPresent() ) {

                List<VehicleOwnerHistory> vehicleOwnerHistoryList = vehicleOwnerHistoryService.findOwnerHistoryForUser( optionalUser.get() );

                vehicleViewResponseList = vehicleOwnerHistoryList.stream().map(
                                vehicleOwner -> {
                                    return VehicleViewResponse.builder()
                                            .engineNo(vehicleOwner.getVehicle().getEngineNo())
                                            .assignedAt( vehicleOwner.getAssignedAt() )
                                            .vehicleType(vehicleOwner.getVehicle().getVehicleType())
                                            .registrationNo(vehicleOwner.getVehicle().getRegistrationNo())
                                            .build();
                                }
                        )
                        .collect(Collectors.toList());


                nocApplicationViewResponse.setVehicleViewResponseList(vehicleViewResponseList);
            }

            List<VehicleViewResponse> newlyAddedvehicleViewResponseList;

            List<Vehicle> newlyAddedVehicle = nocApplication.get().getAdditionalVehicleSet().stream().collect(Collectors.toList());

            newlyAddedvehicleViewResponseList = newlyAddedVehicle.stream().map(
                    vehicle -> {
                        return VehicleViewResponse.builder()
                                .registrationNo( vehicle.getRegistrationNo() )
                                .build();
                    }
                )
                    .collect(Collectors.toList());

            nocApplicationViewResponse.setNewlyAddedvehicleViewResponseList( newlyAddedvehicleViewResponseList );

            model.addAttribute( "nocApplicationViewResponse", nocApplicationViewResponse );

            model.addAttribute("viewAction","true");

            User user = loggedInUser.getUser();

            List<MechanicViewResponse> mechanicViewResponseList = mechanicService.getAllMechanicViewResponseList();

            model.addAttribute("mechanicViewResponseList", mechanicViewResponseList);

            if(user.getUserType().equals(UserType.PUBLIC)) {

                PublicUserInfo publicUserInfo = user.getPublicUserInfo();

                if(nocApplication.get().getIdentityNumber().equals(publicUserInfo.getServiceId())) {

                    model.addAttribute("viewAction","false");
                }
            }

            if( nocApplicationViewResponse.getStateId() != null ){

                List<StateActionMapResponse> stateActionMapList = stateActionMapService.findStateActionMapResponseListByCurrentStateAndRoleIdSet( nocApplicationViewResponse.getStateId(), loggedInUser.getRoleIds() );
                model.addAttribute( "stateActionList", stateActionMapList );
            }
        }

        List<VehicleResponseForSelection> vehicleResponseForSelectionList = vehicleService.findByVehicleAuctionDataIsNullAndVehicleDispositionIsNull();

        model.addAttribute( "allAvailableFreeVehiclesForMI", vehicleResponseForSelectionList );
    }

    @Transactional
    public void create(Model model, Long id, UserPrincipal loggedInUser, HttpServletRequest request) {

        Optional<NocApplication> nocApplication = nocApplicationService.findById( id );

        if( nocApplication.isPresent() ) {

            NocApplicationViewResponse nocApplicationViewResponse = nocApplicationService.getNocApplicationViewResponseFromNocApplication( nocApplication.get(), request );
            model.addAttribute( "nocApplicationViewResponse", nocApplicationViewResponse );
        }
    }

    public void takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, NocAdditionalActionData nocAdditionalActionData, UserPrincipal loggedInUser) throws Exception {

        nocApplicationService.takeAction( id, actionId, nocAdditionalActionData, loggedInUser );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.common.action.success" );
    }
}
