package com.dgt.paribahanpool.noc_application.service;

import com.dgt.paribahanpool.noc_application.model.NocApplication;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface NocApplicationRepository extends DataTablesRepository<NocApplication, Long> {
}
