package com.dgt.paribahanpool.noc_application.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.enums.NocApplicationType;
import com.dgt.paribahanpool.enums.SettingType;
import com.dgt.paribahanpool.enums.VehicleAllotment;
import com.dgt.paribahanpool.locale.LocalizeUtil;
import com.dgt.paribahanpool.noc_application.model.*;
import com.dgt.paribahanpool.notification.model.NotificationMetadata;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.setting.model.Setting;
import com.dgt.paribahanpool.setting.service.SettingService;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import com.dgt.paribahanpool.workflow.model.*;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.dgt.paribahanpool.noc_application.service.NocApplicationSpecification.filterByCreatedBy;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class NocApplicationService implements WorkflowService<NocApplication> {

    private final DocumentService documentService;
    private final NocApplicationRepository nocApplicationRepository;
    private final StateService stateService;
    private final StateActionMapService stateActionMapService;
    private final UserService userService;
    private final SettingService settingService;
    private final VehicleService vehicleService;
    private final LocalizeUtil localizeUtil;

    public NocApplication save( NocApplication nocApplication) {

        log.debug( "Noc application save method. Before calling repository" );
        return nocApplicationRepository.save(nocApplication);
    }

    public Optional<NocApplication> findById( Long id ) {

        return nocApplicationRepository.findById(id);
    }

    public NocApplication save( NocApplicationAddRequest nocApplicationAddRequest ) {

        NocApplication nocApplication = new NocApplication();

        if(nocApplicationAddRequest.getId() != null) {

            nocApplication = findById( nocApplicationAddRequest.getId() ).get();
        }

        getNocApplicationFromANocApplicationAddRequest( nocApplication, nocApplicationAddRequest );

        return save(nocApplication);
    }

    public NocApplicationAddRequest getNocApplicationAddRequestFromNocApplication( NocApplication nocApplication ) {

        NocApplicationAddRequest nocApplicationAddRequest = new NocApplicationAddRequest();

        nocApplicationAddRequest.setId( nocApplication.getId() );
        nocApplicationAddRequest.setNocApplicationType( nocApplication.getNocApplicationType() );
        nocApplicationAddRequest.setDate( nocApplication.getDate() );
        nocApplicationAddRequest.setDesignation( nocApplication.getDesignation() );
        nocApplicationAddRequest.setEMail( nocApplication.getEMail() );
        nocApplicationAddRequest.setName( nocApplication.getName() );
        nocApplicationAddRequest.setIdentityNumber( nocApplication.getIdentityNumber() );
        nocApplicationAddRequest.setVehicleAllotment( nocApplication.getVehicleAllotment() );
        nocApplicationAddRequest.setMobileNumber( nocApplication.getMobileNumber() );
        nocApplicationAddRequest.setWorkingPlace( nocApplication.getWorkingPlace() );

        if( nocApplication.getDocGroupId() != null && nocApplication.getDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( nocApplication.getDocGroupId() );
            nocApplicationAddRequest.setDocuments( documentMetadataList );
        }

        return nocApplicationAddRequest;
    }

    public NocApplication getNocApplicationFromANocApplicationAddRequest( NocApplication nocApplication, NocApplicationAddRequest nocApplicationAddRequest ) {

        nocApplication.setNocApplicationType( nocApplicationAddRequest.getNocApplicationType() );
        nocApplication.setDate( nocApplicationAddRequest.getDate() );
        nocApplication.setDesignation( nocApplicationAddRequest.getDesignation() );
        nocApplication.setEMail( nocApplicationAddRequest.getEMail() );
        nocApplication.setIdentityNumber( nocApplicationAddRequest.getIdentityNumber() );
        nocApplication.setMobileNumber( nocApplicationAddRequest.getMobileNumber() );
        nocApplication.setName( nocApplicationAddRequest.getName() );
        nocApplication.setVehicleAllotment( nocApplicationAddRequest.getVehicleAllotment() );
        nocApplication.setWorkingPlace( nocApplicationAddRequest.getWorkingPlace() );
        nocApplication.setAllottedCarRegistrationNumber(nocApplicationAddRequest.getAllottedCarRegistrationNumber());
        nocApplication.setPrlAcceptIdentityNumber(nocApplicationAddRequest.getPrlAcceptIdentityNumber());
        nocApplication.setPrlAcceptDate(nocApplicationAddRequest.getPrlAcceptDate());
        nocApplication.setDebtAcceptIdentityNumber(nocApplicationAddRequest.getDebtAcceptIdentityNumber());
        nocApplication.setDebtAcceptDate(nocApplicationAddRequest.getDebtAcceptDate());
        nocApplication.setHome(nocApplicationAddRequest.getHome());

        if( nocApplication.getState() == null ){

//            Long startStateId = AppConstants.NOC_APP_PLR_INIT_STATE_ID;
//            if( nocApplication.getNocApplicationType() == NocApplicationType.RENEW_VEHICLE ){
//                startStateId = AppConstants.NOC_APP_ENCASHMENT_INIT_STATE_ID;
//            }

            Long startStateId = AppConstants.NOC_START_STATE_ID;

            State state = stateService.findStateReferenceById( startStateId );
            nocApplication.setState( state );
        }

        DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( nocApplicationAddRequest.getFiles(), Collections.EMPTY_MAP );
        nocApplication.setDocGroupId( documentUploadedResponse.getDocGroupId() );

        return nocApplication;
    }

    public NocSearchResponse getNocSearchResponseFromNocApplication(  NocApplication nocApplication, HttpServletRequest request ){

        NocSearchResponse nocSearchResponse = new NocSearchResponse();

        nocSearchResponse.setId( nocApplication.getId() );
        nocSearchResponse.setName( nocApplication.getName() );
        nocSearchResponse.setDate( nocApplication.getCreatedAt().format( DateTimeFormatter.ofPattern("dd/MM/yyyy") ) );
        nocSearchResponse.setNocApplicationType( localizeUtil.getMessageFromMessageSource( NocApplicationType.getLabel( nocApplication.getNocApplicationType() ), request ) );
        nocSearchResponse.setDesignation( nocApplication.getDesignation() );
        nocSearchResponse.setVehicleAllotment( localizeUtil.getMessageFromMessageSource( VehicleAllotment.getLabel( nocApplication.getVehicleAllotment() ), request ) );
        nocSearchResponse.setAllottedCarRegistrationNumber( (nocApplication.getAllottedCarRegistrationNumber() == null) ? "N/A" : nocApplication.getAllottedCarRegistrationNumber() );
        nocSearchResponse.setEMail( nocApplication.getEMail() );
        nocSearchResponse.setIdentityNumber( nocApplication.getIdentityNumber() );
        nocSearchResponse.setWorkingPlace( nocApplication.getWorkingPlace() );
        nocSearchResponse.setMobileNumber( nocApplication.getMobileNumber() );

        List<DocumentMetadata> documentDataList = documentService.findDocumentMetaDataListByGroupId( nocApplication.getDocGroupId() );
        nocSearchResponse.setDocumentMetadataList( documentDataList );

        State state = nocApplication.getState();
        if( state != null ){

            StateResponse stateResponse = StateResponse.builder()
                    .id( state.getId() )
                    .name( state.getName() )
                    .nameEn( state.getNameEn() )
                    .stateType( state.getStateType() )
                    .build();
            nocSearchResponse.setStateResponse( stateResponse );
        }

        return nocSearchResponse;
    }

    public DataTablesOutput<NocSearchResponse> searchForDatatable( DataTablesInput dataTablesInput, UserPrincipal loggedInUser,
                                                                   HttpServletRequest request ) {

        return nocApplicationRepository.findAll( dataTablesInput, nocApplication -> getNocSearchResponseFromNocApplication(nocApplication, request) );
    }

    public NocApplicationViewResponse getNocApplicationViewResponseFromNocApplication( NocApplication nocApplication, HttpServletRequest request ) {

        NocApplicationViewResponse nocApplicationViewResponse = new NocApplicationViewResponse();

        User user = userService.findById( nocApplication.getCreatedBy() ).get();
        List<DocumentMetadata> signMetaData = documentService.findDocumentMetaDataListByGroupId( user.getSignDocGroupId() );

        User approvedBy = null;

        if( nocApplication.getApprovedBy() != null ) {

            approvedBy = userService.findById(nocApplication.getApprovedBy().getId()).get();
        }

        nocApplicationViewResponse.setId( nocApplication.getId() );
        nocApplicationViewResponse.setStateId( nocApplication.getState() != null? nocApplication.getState().getId(): null );
        nocApplicationViewResponse.setName( nocApplication.getName() );
        nocApplicationViewResponse.setServiceId( nocApplication.getIdentityNumber() );
        nocApplicationViewResponse.setDesignation( nocApplication.getDesignation() );
        nocApplicationViewResponse.setWorkplace( nocApplication.getWorkingPlace() );
        nocApplicationViewResponse.setEmail( nocApplication.getEMail() );
        nocApplicationViewResponse.setPhoneNo( nocApplication.getMobileNumber() );
        nocApplicationViewResponse.setDate( nocApplication.getDate() );
        nocApplicationViewResponse.setVehicleAllotment( nocApplication.getVehicleAllotment() );
        nocApplicationViewResponse.setAllottedCarRegistrationNumber( nocApplication.getAllottedCarRegistrationNumber() );
        nocApplicationViewResponse.setNocApplicationType( nocApplication.getNocApplicationType() );
        nocApplicationViewResponse.setStateType( nocApplication.getState().getStateType() );
        nocApplicationViewResponse.setFinedAmount( nocApplication.getFinedAmount() );
        nocApplicationViewResponse.setPrlAcceptDate( nocApplication.getPrlAcceptDate() );
        nocApplicationViewResponse.setPrlAcceptIdentityNumber( nocApplication.getPrlAcceptIdentityNumber() );
        nocApplicationViewResponse.setDebtAcceptDate( nocApplication.getDebtAcceptDate() );
        nocApplicationViewResponse.setDebtAcceptIdentityNumber( nocApplication.getDebtAcceptIdentityNumber() );
        nocApplicationViewResponse.setCreationDate( nocApplication.getDate() );
        nocApplicationViewResponse.setApprovedDate( nocApplication.getApprovedDate() );
        nocApplicationViewResponse.setDistribution( nocApplication.getDistribution() );
        nocApplicationViewResponse.setHasVehicleUdoOa( nocApplication.getHasVehicleUdoOa() );
        nocApplicationViewResponse.setIsApprovedMechanic( nocApplication.getIsApprovedMechanic() );
        nocApplicationViewResponse.setIsApprovedAtoUd( nocApplication.getIsApprovedAtoUd() );
        nocApplicationViewResponse.setApprovedBy( (approvedBy == null) ? null :
                UserResponse.builder()
                        .id( approvedBy.getId() )
                        .nameBn( approvedBy.getEmployee().getName() )
                        .nameEn( approvedBy.getEmployee().getNameEn() )
                        .phoneNumber( approvedBy.getEmployee().getMobileNumber() )
                        .designation( approvedBy.getEmployee().getDesignation().getNameBn() )
                        .email( approvedBy.getEmployee().getEmail() )
                        .build()
        );

        if( signMetaData.size() > 0 ){
            nocApplicationViewResponse.setSignMetaData( signMetaData.get(0) );
        }
        else{

            nocApplicationViewResponse.setSignMetaData( null );
        }


        return nocApplicationViewResponse;
    }

    @Transactional
    public void takeAction(Long id, Long actionId, NocAdditionalActionData nocAdditionalActionData, UserPrincipal loggedInUser) throws Exception {

        stateActionMapService.takeAction( this, id, actionId, loggedInUser, ( workflowEntity ) -> setAdditionalData( workflowEntity, actionId, nocAdditionalActionData, loggedInUser ) );
    }

    private WorkflowAdditionalData setAdditionalData( WorkflowEntity workflowEntity, Long actionId, WorkflowAdditionalData additionalActionData, UserPrincipal loggedInUser ) {

        NocApplication nocApplication = (NocApplication) workflowEntity;
        NocAdditionalActionData nocAdditionalActionData = (NocAdditionalActionData) additionalActionData;

        if( actionId.equals( AppConstants.NOC_APPROVE ) ){

            nocApplication.setApprovedBy( loggedInUser.getUser() );
            nocApplication.setApprovedDate( LocalDate.now() );

            Optional<Setting> nocDistributionSettingOptional = settingService.findBySettingType( SettingType.NOC_DISTRIBUTION_FOOTER );

            if( nocDistributionSettingOptional.isPresent() ){

                Setting nocDistributionSetting = nocDistributionSettingOptional.get();
                nocApplication.setDistribution( nocDistributionSetting.getValue() );
            }
        }

        if( actionId.equals( AppConstants.NOC_ADD_MISSING_VEHICLE_ACTION_ID ) ){

            Set<Vehicle> vehicleSet = new HashSet<>();

            nocAdditionalActionData.getVehicleIds().forEach( (vehicleId) -> {

                Vehicle vehicle = vehicleService.findReferenceById( vehicleId );

                vehicleSet.add( vehicle );
            });

            nocApplication.getAdditionalVehicleSet().addAll( vehicleSet );
        }

        if( actionId.equals( AppConstants.NOC_SENT_TO_MECHANIC_INCHARGE_ACTION_ID ) ){

            nocApplication.setHasVehicleUdoOa( nocAdditionalActionData.getHasVehicleUdoOa() );
            nocApplication.setMechanicInchargeId( nocAdditionalActionData.getMechanicInchargeId() );
        }

        if( actionId.equals( AppConstants.NOC_SENT_TO_ATO_ACTION_ID ) || actionId.equals( AppConstants.NOC_SENT_TO_UD_ACTION_ID ) ){

            nocApplication.setIsApprovedMechanic( nocAdditionalActionData.getIsApprovedMechanic() );
        }

        if( actionId.equals( AppConstants.NOC_SENT_TO_AD_ACTION_ID ) ){

            nocApplication.setIsApprovedAtoUd( nocAdditionalActionData.getIsApprovedAtoUd() );
        }

        if( actionId.equals( AppConstants.NOC_APP_ENCASHMENT_COLLECT_FINE_ACTION_ID ) ){

            nocApplication.setCollectedFine(nocAdditionalActionData.getCollectedFine() );
        }

        if( actionId.equals( AppConstants.NOC_APP_ENCASHMENT_DO_FINE_ACTION_ID ) ){

            nocApplication.setFinedAmount( nocAdditionalActionData.getFinedAmount() );
        }

        if( actionId.equals( AppConstants.NOC_UPLOAD_NOC_ISSUE_ACTION_ID ) ){

            nocApplication.setApprovedDate( LocalDate.now() );
            nocApplication.setApprovedBy( loggedInUser.getUser() );

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( nocAdditionalActionData.getNocIssueFiles(), Collections.EMPTY_MAP );
            nocApplication.setNocIssueGroupId( documentUploadedResponse.getDocGroupId() );
        }

        return additionalActionData;
    }

    public DataTablesOutput<NocSearchResponse> searchForMyDatatable( DataTablesInput dataTablesInput, Long loggedInUserId, HttpServletRequest request ) {

        return nocApplicationRepository.findAll( dataTablesInput,  filterByCreatedBy( loggedInUserId ), null, nocApplication -> getNocSearchResponseFromNocApplication( nocApplication, request ) );
    }

    public Boolean existsById( Long id ) {

        return nocApplicationRepository.existsById( id );
    }

    public void delete( Long id ) {

        nocApplicationRepository.deleteById( id );
    }

    public static NotificationMetadata getNotificationMetadata() {

        return NotificationMetadata.builder()
                .titleEn( "NOC Application workflow" )
                .titleBn( "অনাপত্তি পত্র আবেদন ওয়ার্ক ফ্লো " )
                .textEn( "A NOC Application is waiting for your action" )
                .textBn( "একটি অনাপত্তিপত্র আবেদন আপনার একশন এর জন্য অপেক্ষমান অবস্থায় আছে" )
                .redirectUrl( "noc/{id}" )
                .icon( "flaticon2-paper text-warning" )
                .build();
    }
}
