package com.dgt.paribahanpool.noc_application.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.enums.StateType;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.noc_application.model.NocAdditionalActionData;
import com.dgt.paribahanpool.noc_application.model.NocApplication;
import com.dgt.paribahanpool.noc_application.model.NocApplicationAddRequest;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired } )
public class NocApplicationValidationService {

    private final MvcUtil mvcUtil;
    private final NocApplicationService nocApplicationService;
    private final StateActionMapService stateActionMapService;

    public Boolean handleBindingResultForAddFormPost( Model model, BindingResult bindingResult, NocApplicationAddRequest nocApplicationAddRequest ){

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "nocApplicationAddRequest" )
                        .object( nocApplicationAddRequest )
                        .title( "title.noc.add" )
                        .content( "noc/noc-add" )
                        .activeMenu( Menu.NOC_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.NOC_ADD_FORM } )
                        .build()
        );
    }

    public Boolean validatePostRequest( RedirectAttributes redirectAttributes, NocApplicationAddRequest nocApplicationAddRequest ) {

        if(nocApplicationAddRequest.getId() != null) {

            Optional<NocApplication> nocApplication = nocApplicationService.findById( nocApplicationAddRequest.getId() );

            if( !nocApplication.isPresent() ) {

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound");
                return false;
            }
        }
        return true;
    }

    public Boolean validateEdit( RedirectAttributes redirectAttributes, Long nocId ) {

        Optional<NocApplication> nocApplication = nocApplicationService.findById( nocId );

        if( !nocApplication.isPresent() ) {

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound");
            return false;
        }

        return true;
    }

    public Boolean view( RedirectAttributes redirectAttributes, Long id ) {

        Optional<NocApplication> nocApplication = nocApplicationService.findById( id );

        if( !nocApplication.isPresent() ) {

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    public Boolean create( RedirectAttributes redirectAttributes, Long id ) {

        Optional<NocApplication> nocApplicationOptional = nocApplicationService.findById( id );

        if( !nocApplicationOptional.isPresent() ) {

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }
        else{

            NocApplication nocApplication = nocApplicationOptional.get();

            if( nocApplication.getState() == null || ( nocApplication.getState().getStateType() != StateType.POSITIVE_END ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.not-positive-end-of-state" );
                return false;
            }
        }

        return true;
    }

    @Transactional
    public Boolean takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, UserPrincipal loggedInUser, NocAdditionalActionData nocAdditionalActionData) {

        Optional<NocApplication> nocApplicationOptional = nocApplicationService.findById( id );

        if( !nocApplicationOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }
        else{

            NocApplication nocApplication = nocApplicationOptional.get();
            State state = nocApplication.getState();

            if( !stateActionMapService.actionAllowed( state.getId(), loggedInUser.getRoleIds(), actionId ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.common.action.not-allowed" );
                return false;
            }

            if( actionId.equals( AppConstants.NOC_APP_ENCASHMENT_CHOOSE_MECHANIC_INCHARGE_ACTION_ID ) && ( nocAdditionalActionData.getMechanicInchargeId() == null || nocAdditionalActionData.getMechanicInchargeId() < 0 ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.noc.action.mechanic-incharge-id.not-given" );
                return false;
            }

            if( actionId.equals( AppConstants.NOC_APP_ENCASHMENT_COLLECT_FINE_ACTION_ID) ){

                if( nocAdditionalActionData.getCollectedFine() == null || nocAdditionalActionData.getCollectedFine() < 0 ) {
                    mvcUtil.addErrorMessage(redirectAttributes, "error.noc.action.collected-fine.not-given");
                    return false;
                }

                if( !nocAdditionalActionData.getCollectedFine().equals( nocApplication.getFinedAmount() ) ){

                    mvcUtil.addErrorMessage( redirectAttributes, "error.noc.action.collected-fine.not-match" );
                    return false;
                }
            }

            if( actionId.equals( AppConstants.NOC_APP_ENCASHMENT_DO_FINE_ACTION_ID) && ( nocAdditionalActionData.getFinedAmount() == null || nocAdditionalActionData.getFinedAmount() < 0 ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.noc.action.fined-amount.not-given" );
                return false;
            }

            return true;
        }
    }

    public RestValidationResult delete( Long id ) throws NotFoundException {

        Boolean exist = nocApplicationService.existsById( id );

        if( !exist ){

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }
}
