package com.dgt.paribahanpool.notification.controller;

import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.notification.service.NotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Slf4j
@Controller
@RequestMapping( "/notification" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class NotificationController extends MVCController {

    private final NotificationService notificationService;

    @GetMapping( "/redirect" )
    public String redirect(
            @RequestParam( "redirectUrl" ) String redirectUrl,
            @RequestParam( "id" ) Long id
    ){
        notificationService.markAsSeen( id );
        return "forward:/" + redirectUrl;
    }
}
