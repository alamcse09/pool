package com.dgt.paribahanpool.notification.model;

import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "email" )
public class Email extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( name = "email_to", length = 3000 )
    private String to;

    @Column( name = "cc", length = 3000 )
    private String cc;

    @Column( name = "bcc", length = 3000 )
    private String bcc;

    @Column( name = "subject", columnDefinition = "TEXT" )
    private String subject;

    @Column( name = "text", columnDefinition = "TEXT" )
    private String text;

    @Column( name = "is_html" )
    private Boolean isHtml;

    @Column( name = "params", columnDefinition = "TEXT" )
    private String params;

    @Column( name = "template_path", length = 2000 )
    private String templatePath;

    @Column( name = "doc_group_id" )
    private Long docGroupId;
}
