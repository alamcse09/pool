package com.dgt.paribahanpool.notification.model;

import com.dgt.paribahanpool.notification.validation.annotation.ValidEmailHtmlTemplate;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Data
@Builder
@ValidEmailHtmlTemplate( primaryField = "isHtml", secondaryField = "templatePath" )
public class EmailData {

    @NotNull( message = "{validation.common.required}" )
    @Size( min = 1, message = "{validation.common.size}" )
    private String[] to;
    private String[] cc;
    private String[] bcc;

    @NotBlank( message = "{validation.common.required}" )
    private String subject;
    private String text;

    @Builder.Default
    @NotNull( message = "{validation.common.required}" )
    private Boolean isHtml = true;

    private String templatePath;
    private Map<String,Object> params;
    private List<List<String>> paramList;

    @Builder.Default
    @NotNull( message = "{validation.common.required}" )
    private Locale locale = Locale.forLanguageTag( "en" );

    private List<EmailAttachment> attachmentList;
}
