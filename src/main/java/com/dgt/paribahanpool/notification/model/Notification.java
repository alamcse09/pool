package com.dgt.paribahanpool.notification.model;

import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table( name = "notification" )
public class Notification extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "notification_type" )
    private NotificationType notificationType;

    @Column( name = "notification_time" )
    private LocalDateTime notificationTime;

    @Column( name = "title_en" )
    private String titleEn;

    @Column( name = "title_bn" )
    private String titleBn;

    @Column( name = "text_en" )
    private String textEn;

    @Column( name = "text_bn" )
    private String textBn;

    @Column( name = "is_seen" )
    private Boolean isSeen = false;

    @Column( name = "redirect_url" )
    private String redirectUrl;

    @Column( name = "module_type" )
    private String moduleType;

    @Column( name = "entity_id" )
    private Long entityId;

    @ManyToOne
    @JoinColumn( name = "to_user", foreignKey = @ForeignKey( name = "fk_notification_user_id" ) )
    private User toUser;

    public static Notification of(
            NotificationType notificationType,
            String titleEn,
            String titleBn,
            String textEn,
            String textBn,
            String redirectUrl,
            String moduleType,
            Long entityId,
            User user
    ){

        Notification notification = new Notification();

        notification.setNotificationType( notificationType );
        notification.setNotificationTime( LocalDateTime.now() );
        notification.setTitleEn( titleEn );
        notification.setTitleBn( titleBn );
        notification.setTextEn( textEn );
        notification.setTextBn( textBn );
        notification.setIsSeen ( false );
        notification.setRedirectUrl( redirectUrl );
        notification.setModuleType( moduleType );
        notification.setEntityId( entityId );
        notification.setToUser( user );

        return notification;
    }
}
