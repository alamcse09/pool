package com.dgt.paribahanpool.notification.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NotificationMetadata {

    private String titleEn = "";
    private String titleBn = "";
    private String textEn = "";
    private String textBn = "";
    private String redirectUrl = "";
    private String icon = "flaticon2-information text-info";
}
