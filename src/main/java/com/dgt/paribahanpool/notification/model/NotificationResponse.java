package com.dgt.paribahanpool.notification.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class NotificationResponse {

    private Long id;
    private NotificationType notificationType;
    private LocalDateTime notificationTime;
    private String titleEn;
    private String titleBn;
    private String textEn;
    private String textBn;
    private Boolean isSeen = false;
    private String redirectUrl = "#";
    private Integer hoursAgo;
    private String moduleType;
}
