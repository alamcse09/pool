package com.dgt.paribahanpool.notification.model;

import com.dgt.paribahanpool.exceptions.EmailDataBadRequestException;

import java.util.concurrent.CompletableFuture;

public interface NotificationSender {

    CompletableFuture<Boolean> sendEmail( EmailData emailData ) throws EmailDataBadRequestException;
    CompletableFuture<SmsResponse> sendSms( SmsData smsData );
}
