package com.dgt.paribahanpool.notification.model;

public enum NotificationType {

    EVENT,
    MEETING_MINUTES_TAKER_ASSIGNED, WORKFLOW_ACTION;
}
