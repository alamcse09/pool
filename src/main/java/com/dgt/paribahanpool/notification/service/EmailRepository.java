package com.dgt.paribahanpool.notification.service;

import com.dgt.paribahanpool.notification.model.Email;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailRepository extends JpaRepository<Email,Long> {
}
