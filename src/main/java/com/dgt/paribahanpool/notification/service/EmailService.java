package com.dgt.paribahanpool.notification.service;

import com.dgt.paribahanpool.exceptions.EmailDataBadRequestException;
import com.dgt.paribahanpool.notification.model.Email;
import com.dgt.paribahanpool.notification.model.EmailData;
import com.dgt.paribahanpool.notification.model.NotificationSender;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class EmailService {

    private final EmailRepository emailRepository;
    private final NotificationSender notificationSender;
    private final Gson gson;

    public Email save( Email email ){

        return emailRepository.save( email );
    }

    public Email sendEmail( EmailData emailData ) throws EmailDataBadRequestException {

        notificationSender.sendEmail( emailData );
        Email email = getEmailFromEmailData( emailData );
        return save( email );
    }

    private Email getEmailFromEmailData(EmailData emailData) {

        Email email = new Email();

        email.setTo( Arrays.asList( emailData.getTo() ).stream().collect(Collectors.joining( ";" ) ) );

        if( emailData.getCc() != null ){
            email.setCc( Arrays.asList( emailData.getCc() ).stream().collect(Collectors.joining( ";" ) ) );
        }

        if( emailData.getBcc() != null ){
            email.setBcc( Arrays.asList( emailData.getBcc() ).stream().collect(Collectors.joining( ";" ) ) );
        }

        email.setSubject( emailData.getSubject() );
        email.setText( emailData.getText() );

        email.setIsHtml( emailData.getIsHtml() );

        if( emailData.getParams() != null ) {
            email.setParams(gson.toJson(emailData.getParams()));
        }

        email.setTemplatePath( emailData.getTemplatePath() );

        return email;
    }
}
