package com.dgt.paribahanpool.notification.service;

import com.dgt.paribahanpool.notification.model.Notification;
import com.dgt.paribahanpool.user.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NotificationRepository extends JpaRepository<Notification, Long> {

    List<Notification> findByToUser(User user, Pageable pageable);

    Integer countByToUserAndIsSeen(User user, Boolean isSeen );
}
