package com.dgt.paribahanpool.notification.service;

import com.dgt.paribahanpool.config.AppProperties;
import com.dgt.paribahanpool.exceptions.EmailDataBadRequestException;
import com.dgt.paribahanpool.notification.model.EmailData;
import com.dgt.paribahanpool.notification.model.NotificationSender;
import com.dgt.paribahanpool.notification.model.SmsData;
import com.dgt.paribahanpool.notification.model.SmsResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class NotificationSenderImpl implements NotificationSender {

    private final SpringTemplateEngine templateEngine;
    private final JavaMailSender javaMailSender;
    private final Validator validator;
    private final AppProperties appProperties;

    @Override
    @Async( "notificationTaskExecutor" )
    public CompletableFuture<Boolean> sendEmail( EmailData emailData ) throws EmailDataBadRequestException {

        validateEmailData( emailData );

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        try {

            MimeMessageHelper message = new MimeMessageHelper( mimeMessage, true, StandardCharsets.UTF_8.name() );

            message.setTo( emailData.getTo() );
            message.setFrom( appProperties.getEmail().getFrom() );
            message.setSubject( emailData.getSubject() );

            if( emailData.getIsHtml() ){

                String text = getHtmlTextFromTemplate( emailData );
                message.setText( text, true );
            }
            else {

                message.setText( emailData.getText() );
            }

            if( emailData.getAttachmentList() != null && emailData.getAttachmentList().size() > 0 ){

                emailData.getAttachmentList().forEach( emailAttachment -> {

                    DataSource source = new ByteArrayDataSource( emailAttachment.getData(), emailAttachment.getContentType() );

                    try {
                        message.addAttachment( emailAttachment.getName(), source );
                    } catch (MessagingException e) {
                        log.warn( "Email attachment couldn't be attached. {}, {}", emailAttachment.getName(), emailAttachment.getContentType() );
                    }
                });
            }

            javaMailSender.send( mimeMessage );

        }  catch ( MailException | MessagingException e ) {

            log.warn( "Email could not be sent to user '{}'", emailData.getTo(), e );
            return CompletableFuture.completedFuture( false );
        }

        return CompletableFuture.completedFuture( true );
    }

    private String getHtmlTextFromTemplate( EmailData emailData ) {

        Context context = new Context( emailData.getLocale() );

        context.setVariable( "host", appProperties.getServer().getHost() );
        context.setVariable( "port", appProperties.getServer().getPort() );

        if( emailData.getParams() != null ) {

            for ( String key : emailData.getParams().keySet() ) {

                context.setVariable( key, emailData.getParams().get( key ) );
            }
        }

        String text = templateEngine.process( emailData.getTemplatePath(), context );

        return text;
    }

    private void validateEmailData(EmailData emailData) throws EmailDataBadRequestException {

        Set<ConstraintViolation<EmailData>> constraintViolationSet = validator.validate( emailData );

        String errorMessage = constraintViolationSet.stream()
                .map( emailDataConstraintViolation -> String.format( "%s -> %s", emailDataConstraintViolation.getPropertyPath().toString(), emailDataConstraintViolation.getMessage() ) )
                .collect( Collectors.joining(";" ) );

        if( constraintViolationSet != null && constraintViolationSet.size() > 0 ){

            throw new EmailDataBadRequestException( errorMessage );
        }
    }

    @Override
    @Async( "notificationTaskExecutor" )
    public CompletableFuture<SmsResponse> sendSms( SmsData smsData ) {

        return null;
    }
}
