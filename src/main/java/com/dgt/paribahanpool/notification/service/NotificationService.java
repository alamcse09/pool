package com.dgt.paribahanpool.notification.service;

import com.dgt.paribahanpool.notification.model.Notification;
import com.dgt.paribahanpool.notification.model.NotificationResponse;
import com.dgt.paribahanpool.user.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class NotificationService {

    private final NotificationRepository notificationRepository;

    public Notification save( Notification notification ){

        return notificationRepository.save( notification );
    }

    public void save(List<Notification> notifications) {

        notificationRepository.saveAll( notifications );
    }

    @Transactional
    public void markAsSeen(Long id) {

        Notification notification = notificationRepository.getById( id );
        notification.setIsSeen( true );
        notificationRepository.save( notification );
    }

    public List<NotificationResponse> getNotificationResponseForUser( User user, Pageable pageable ){

        List<Notification> notificationList = notificationRepository.findByToUser( user , pageable );
        return getNotificationResponseListFromNotificationList( notificationList );
    }

    public Integer countUnseenNotification( User user ){

        return notificationRepository.countByToUserAndIsSeen( user, false );
    }

    private List<NotificationResponse> getNotificationResponseListFromNotificationList(List<Notification> notificationList) {

        return notificationList.stream().map( this::getNotificationResponseFromNotification ).collect( Collectors.toList() );
    }

    private NotificationResponse getNotificationResponseFromNotification( Notification notification ) {

        return NotificationResponse.builder()
                .notificationTime( notification.getNotificationTime() )
                .notificationType( notification.getNotificationType() )
                .isSeen( notification.getIsSeen() )
                .textBn( notification.getTextBn() )
                .textEn( notification.getTextEn() )
                .titleBn( notification.getTitleBn() )
                .titleEn( notification.getTitleEn() )
                .id( notification.getId() )
                .redirectUrl( notification.getRedirectUrl() )
                .moduleType( notification.getModuleType() )
                .build();
    }
}
