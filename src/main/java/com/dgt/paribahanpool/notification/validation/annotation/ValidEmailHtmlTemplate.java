package com.dgt.paribahanpool.notification.validation.annotation;

import com.dgt.paribahanpool.notification.validation.validator.EmailHtmlTemplateValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target({ElementType.TYPE})
@Retention( RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
@Constraint(validatedBy = EmailHtmlTemplateValidator.class )
public @interface ValidEmailHtmlTemplate {

    String primaryField();
    String secondaryField();

    String message() default "{validation.common.email.ishtml.nulltemplate}";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
