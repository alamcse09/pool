package com.dgt.paribahanpool.notification.validation.validator;

import com.dgt.paribahanpool.notification.validation.annotation.ValidEmailHtmlTemplate;
import com.dgt.paribahanpool.validation.ValidationBeanUtil;
import com.dgt.paribahanpool.validation.ValidationUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
public class EmailHtmlTemplateValidator implements ConstraintValidator<ValidEmailHtmlTemplate,Object> {

    private String primaryField;
    private String secondaryField;


    @Override
    public void initialize( ValidEmailHtmlTemplate constraintAnnotation ) {

        this.primaryField = constraintAnnotation.primaryField();
        this.secondaryField = constraintAnnotation.secondaryField();
    }

    @Override
    public boolean isValid( Object value, ConstraintValidatorContext context ) {

        try {
            Object field1Value = ValidationBeanUtil.getProperty( value, primaryField );
            Object field2Value = ValidationBeanUtil.getProperty( value, secondaryField );

            if ( field1Value != null && (Boolean) field1Value == true && StringUtils.isBlank( (CharSequence) field2Value ) ) {

                return false;
            }

            return true;
        }
        catch ( IllegalAccessException e ) {

            log.error( "Can't access property during validation checking" );
            return ValidationUtil.handleExceptionWithMessage( context, "{field1} or {field2} is not accessible" );

        } catch ( NoSuchFieldException e ) {

            log.error( "Annotation has wrong field name property during validation checking" );
            return ValidationUtil.handleExceptionWithMessage( context, "{field1} or {field12} is not a valid property for this request" );
        }
    }
}
