package com.dgt.paribahanpool.office_unit.model;

import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@SqlResultSetMappings(
        @SqlResultSetMapping(
                name = "OFFICE_UNIT_SEARCH_RESPONSE",
                classes = {
                        @ConstructorResult(
                                targetClass = com.dgt.paribahanpool.office_unit.model.OfficeUnitSearchResponse.class,
                                columns = {
                                        @ColumnResult(name = "id",type = Long.class),
                                        @ColumnResult(name = "nameEn",type = String.class),
                                        @ColumnResult(name = "nameBn", type = String.class),
                                        @ColumnResult(name = "parentName", type = String.class)
                                }
                        )
                }
        )
)
@Data
@Entity
@Table(name = "office_unit")
@SQLDelete(sql = "UPDATE office_unit SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "is_deleted = false")
public class OfficeUnit extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column(name = "id")
    private Long id;

    @Column(name = "name_en")
    @Convert(converter = StringTrimConverter.class)
    @Convert(converter = EscapeHtmlConverter.class)
    @NotBlank(message = "Enter the name in english")
    private String nameEn;

    @Column(name = "name_bn")
    @Convert(converter = StringTrimConverter.class)
    @Convert(converter = EscapeHtmlConverter.class)
    @NotBlank(message = "Enter the name in bangla")
    private String nameBn;

    @Column(name = "brief_description")
    private String briefDescription;
}

