package com.dgt.paribahanpool.office_unit.model;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class OfficeUnitAddRequest {
    private Long id;
    @NotBlank(message = "{validation.common.required}")
    private String nameEn;
    @NotBlank(message = "{validation.common.required}")
    private String nameBn;
    private String briefDescription;
    private Long parentId;
}
