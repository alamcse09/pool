package com.dgt.paribahanpool.office_unit.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OfficeUnitSearchRequest {
    private Long id;
    private String nameEn;
    private String nameBn;
    private Long parentId;
    private Integer pageNo;
    private Integer pageSize;
}
