package com.dgt.paribahanpool.office_unit.model;

import lombok.*;

@Setter
@Getter
@Builder
@ToString
@AllArgsConstructor
public class OfficeUnitSearchResponse {
    private Long id;
    private String nameEn;
    private String nameBn;
    private String parentName;
}
