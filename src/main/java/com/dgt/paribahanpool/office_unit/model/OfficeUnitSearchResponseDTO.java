package com.dgt.paribahanpool.office_unit.model;

public interface OfficeUnitSearchResponseDTO {
    Long getId();
    String getNameEn();
    String getNameBn();
    String getParentName();
}
