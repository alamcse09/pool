package com.dgt.paribahanpool.office_unit.service;

import com.dgt.paribahanpool.office_unit.model.OfficeUnit;
import com.dgt.paribahanpool.office_unit.model.OfficeUnitSearchResponseDTO;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface OfficeUnitRepository extends JpaRepository<OfficeUnit, Long>, DataTablesRepository<OfficeUnit,Long> {

    List<OfficeUnit> findByIdIn(List<Long>ids);

    @Modifying
    @Transactional
    @Query(value = "UPDATE OfficeUnit ou SET ou.isDeleted = 1 WHERE ou.id =?1")
    void setIsDeletedTrueById(Long id);

    @Query(value = "SELECT a.id as id,a.name_en as nameEn,a.name_bn as nameBn,IFNULL(b.name_en,'') as parentName FROM office_unit2 a left join office_unit2 b on a.parent_id = b.id",nativeQuery = true)
    List<OfficeUnitSearchResponseDTO> getAllOfficeUnitSearchResponseDTO();
}
