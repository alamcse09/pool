package com.dgt.paribahanpool.office_unit.service;

import com.dgt.paribahanpool.office_unit.model.OfficeUnit;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class OfficeUnitService {
    private final OfficeUnitRepository officeUnitRepository;

    public Optional<OfficeUnit> findById(Long id) {
        if (id == null) return Optional.empty();
        return officeUnitRepository.findById(id);
    }

    private List<OfficeUnit> getOfficeUnitList(Predicate<OfficeUnit> filterFunction) {
        return officeUnitRepository
                .findAll()
                .stream()
                .filter(filterFunction)
                .collect(Collectors.toList());
    }

    public Map<Long, OfficeUnit> getByIds(List<Long> ids) {
        return officeUnitRepository
                .findByIdIn(ids)
                .stream()
                .collect(Collectors.toMap(OfficeUnit::getId, e -> e, (e1, e2) -> e1));
    }

    public OfficeUnit save(OfficeUnit officeUnit) {

        return officeUnitRepository.save(officeUnit);
    }


    public Boolean existById(Long id) {
        return officeUnitRepository.existsById(id);
    }

    public void delete(Long id) {
        officeUnitRepository.setIsDeletedTrueById(id);
    }

}
