package com.dgt.paribahanpool.office_unit.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.office_unit.model.OfficeUnit;
import com.dgt.paribahanpool.office_unit.model.OfficeUnitAddRequest;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class OfficeUnitValidationService {

    private final MvcUtil mvcUtil;
    private final OfficeUnitService officeUnitService;

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, OfficeUnitAddRequest officeUnitAddRequest ){

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "officeUnitAddRequest" )
                .object( officeUnitAddRequest )
                .title( "title.office-unit.add" )
                .content( "office-unit/office-unit-add" )
                .activeMenu( Menu.OFFICE_UNIT_ADD )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.OFFICE_UNIT_ADD,FrontEndLibrary.TAB_WIZARD } )
                .build();

        return mvcUtil.handleBindingResult(
                bindingResult,
                model,
                params
        );
    }


    public Boolean editOfficeUnit(RedirectAttributes redirectAttributes, Long officeUnitId) {

        Optional<OfficeUnit> officeUnitOptional = officeUnitService.findById(officeUnitId);

        if (!officeUnitOptional.isPresent()) {

            mvcUtil.addErrorMessage(redirectAttributes, "validation.common.notfound");
            return false;
        }

        return true;
    }
    public RestValidationResult delete(Long id) throws NotFoundException {

        Boolean exist = officeUnitService.existById(id);
        if (!exist) {

            throw new NotFoundException("validation.common.notfound");
        }

        return RestValidationResult.valid();
    }
}
