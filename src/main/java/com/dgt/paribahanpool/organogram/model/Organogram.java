package com.dgt.paribahanpool.organogram.model;

import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.*;

import javax.persistence.*;
@SqlResultSetMappings(
        @SqlResultSetMapping(
                name = "OFFICE_UNIT_ORG_SEARCH_RESPONSE",
                classes = {
                        @ConstructorResult(
                                targetClass = OrganogramSearchResponse.class,
                                columns = {
                                        @ColumnResult(name = "id",type = Long.class),
                                        @ColumnResult(name = "nameEn",type = String.class),
                                        @ColumnResult(name = "nameBn", type = String.class),
                                        @ColumnResult(name = "officeUnitName", type = String.class)
                                }
                        )
                }
        )
)
@Data
@Entity
@Table( name = "organogram")
public class Organogram extends AuditableEntity {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "office_unit_id")
    private Long officeUnitId;

    @Column( name = "name_en" )
    private String nameEn;

    @Column( name = "name_bn" )
    private String nameBn;

    @Column( name = "org_tree" )
    private String orgTree;

    @Column( name = "sequence_num" )
    private Integer sequenceNum;
}
