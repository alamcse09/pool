package com.dgt.paribahanpool.organogram.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrganogramAddRequest {
    public Long id;
    public Long officeUnitId;
    public String nameEn;
    public String nameBn;
    public Integer sequenceNum;
}
