package com.dgt.paribahanpool.organogram.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrganogramSearchRequest {
    private Long id;
    private String nameEn;
    private String nameBn;
    private Long officeUnitId;
    private Integer pageNo;
    private Integer pageSize;
}
