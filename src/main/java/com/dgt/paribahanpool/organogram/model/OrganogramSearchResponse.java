package com.dgt.paribahanpool.organogram.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrganogramSearchResponse {
    private Long id;
    private String nameEn;
    private String nameBn;
    private String officeUnitName;
}
