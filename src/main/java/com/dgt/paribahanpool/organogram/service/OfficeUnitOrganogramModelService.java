package com.dgt.paribahanpool.organogram.service;

import com.dgt.paribahanpool.office_unit.service.OfficeUnitService;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class OfficeUnitOrganogramModelService {

    private final OfficeUnitOrganogramService officeUnitOrganogramService;
    private final OfficeUnitService officeUnitService;
    private final MvcUtil mvcUtil;

}
