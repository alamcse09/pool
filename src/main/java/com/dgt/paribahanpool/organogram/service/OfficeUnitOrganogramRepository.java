package com.dgt.paribahanpool.organogram.service;

import com.dgt.paribahanpool.organogram.model.Organogram;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface OfficeUnitOrganogramRepository extends DataTablesRepository<Organogram, Long> {
    List<Organogram> findByOfficeUnitId(Long officeUnitId);
    List<Organogram> findByIdIn(List<Long>ids);

    @Modifying
    @Transactional
    @Query(value = "UPDATE Organogram ou SET ou.isDeleted = 1 WHERE ou.id =?1")
    void setIsDeletedTrueById(Long id);
}
