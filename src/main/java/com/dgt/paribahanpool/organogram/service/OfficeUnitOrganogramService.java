package com.dgt.paribahanpool.organogram.service;

import com.dgt.paribahanpool.organogram.model.Organogram;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class OfficeUnitOrganogramService {

    private final OfficeUnitOrganogramRepository officeUnitOrganogramRepository;

    public Optional<Organogram> findById(Long id) {
        if (id == null) return Optional.empty();
        return officeUnitOrganogramRepository.findById(id);
    }

    public boolean existById(Long id) {
        if (id == null) return false;
        return officeUnitOrganogramRepository.existsById(id);
    }

    public List<Organogram> findByOfficeUnitId(Long officeUnitId) {
        return officeUnitOrganogramRepository.findByOfficeUnitId(officeUnitId);
    }
}
