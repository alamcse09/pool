package com.dgt.paribahanpool.organogram.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.organogram.model.Organogram;
import com.dgt.paribahanpool.organogram.model.OrganogramAddRequest;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class OfficeUnitOrganogramValidationService {
    private final OfficeUnitOrganogramService officeUnitOrganogramService;
    private final MvcUtil mvcUtil;

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, OrganogramAddRequest organogramAddRequest){

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "officeUnitOrganogramAddRequest" )
                .object(organogramAddRequest)
                .title( "title.office-unit-org.add" )
                .content( "office-unit-organogram/office-unit-organogram-add" )
                .activeMenu( Menu.OFFICE_UNIT_ORGANOGRAM_ADD )
                //.frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.OFFICE_UNIT_ADD,FrontEndLibrary.TAB_WIZARD } )
                .build();

        return mvcUtil.handleBindingResult(
                bindingResult,
                model,
                params
        );
    }


    public Boolean editOfficeUnitOrganogram(RedirectAttributes redirectAttributes, Long officeUnitOrgId) {

        Optional<Organogram> officeUnitOrganogramOptional = officeUnitOrganogramService.findById(officeUnitOrgId);

        if (!officeUnitOrganogramOptional.isPresent()) {

            mvcUtil.addErrorMessage(redirectAttributes, "validation.common.notfound");
            return false;
        }

        return true;
    }
    public RestValidationResult delete(Long id) throws NotFoundException {
        Boolean exist = officeUnitOrganogramService.existById(id);
        if (!exist) {

            throw new NotFoundException("validation.common.notfound");
        }

        return RestValidationResult.valid();
    }
}
