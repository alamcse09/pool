package com.dgt.paribahanpool.parts_disposition.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.parts_disposition.model.PartsDispositionAddRequest;
import com.dgt.paribahanpool.parts_disposition.model.PartsDispositionAdditionalData;
import com.dgt.paribahanpool.parts_disposition.service.PartsDispositionModelService;
import com.dgt.paribahanpool.parts_disposition.service.PartsDispositionValidationService;
import com.dgt.paribahanpool.warranty.model.WarrantyAdditionalData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/parts-disposition" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class PartsDispositionController extends MVCController {

    private final PartsDispositionModelService partsDispositionModelService;
    private final PartsDispositionValidationService partsDispositionValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.parts-disposition.add", content = "parts-disposition/parts-disposition-add", activeMenu = Menu.PARTS_DISPOSITION_ADD)
    @AddFrontEndLibrary( libraries = {FrontEndLibrary.PARTS_DISPOSITION_ADD } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add incident form" );
        partsDispositionModelService.addPartsDisposition( model );
        return viewRoot;
    }

    @PostMapping( "/add" )
    public String addPartsItemPost(
            @Valid PartsDispositionAddRequest partsDispositionAddRequest,
            BindingResult bindingResult,
            RedirectAttributes redirectAttributes,
            Model model,
            HttpServletRequest request
    ){
        if( !partsDispositionValidationService.handleBindingResultForAddFormPost( model, bindingResult, partsDispositionAddRequest ) ) {

            return viewRoot;
        }

        if( partsDispositionValidationService.addPartsDispositionPost( redirectAttributes, partsDispositionAddRequest ) ) {

            partsDispositionModelService.addPartsDispositionPost( partsDispositionAddRequest, model );
            log( LogEvent.PARTS_DISPOSTION_ADD, partsDispositionAddRequest.getId(), "Parts Disposition Added", request );
        }

        return "redirect:/parts-disposition/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( content = "parts-disposition/parts-disposition-search" )
    @AddFrontEndLibrary( libraries = {FrontEndLibrary.PARTS_DISPOSITION_SEARCH } )
    public String search(
            Model model,
            HttpServletRequest request,
            @RequestParam( "active" ) Menu activeMenu
    ){

        model.addAttribute("activeMenu", activeMenu);

        String key = new String();

        if( activeMenu == Menu.PARTS_DISPOSITION_SEARCH  ){

            key = "title.parts-disposition.search";
            model.addAttribute("isForWarranty", false);
        }
        else{

            key = "title.warranty.search";
            model.addAttribute("isForWarranty", true);
        }

        model.addAttribute( "pageTitle", key );
        return viewRoot;
    }

    @PostMapping( "/send-to-warranty/{vehicleId}/{inventoryItemId}/{vehiclePartsId}" )
    public String sendPartsForWarranty(
            Model model,
            HttpServletRequest request,
            RedirectAttributes redirectAttributes,
            @PathVariable( "vehicleId" ) Long vehicleId,
            @PathVariable( "inventoryItemId" ) Long inventoryItemId,
            @PathVariable( "vehiclePartsId" ) Long vehiclePartsId
    ){

        if( !partsDispositionValidationService.addPartsDispositionValidFromVehicleAndItem( redirectAttributes, vehicleId, inventoryItemId, vehiclePartsId ) ){

            return "redirect:/vehicle-service/search";
        }

        partsDispositionModelService.saveWithVehicleAndInventory( vehicleId, inventoryItemId, vehiclePartsId, Boolean.FALSE );


        return "redirect:/parts-disposition/search?active=WARRANTY_SEARCH";
    }

    @PostMapping( "/send-to-disposition/{vehicleId}/{inventoryItemId}/{vehiclePartsId}" )
    public String sendPartsForDisposition(
            Model model,
            HttpServletRequest request,
            RedirectAttributes redirectAttributes,
            @PathVariable( "vehicleId" ) Long vehicleId,
            @PathVariable( "inventoryItemId" ) Long inventoryItemId,
            @PathVariable( "vehiclePartsId" ) Long vehiclePartsId
    ){

        if( !partsDispositionValidationService.addPartsDispositionValidFromVehicleAndItem( redirectAttributes, vehicleId, inventoryItemId, vehiclePartsId ) ){

            return "redirect:/vehicle-service/search";
        }

        partsDispositionModelService.saveWithVehicleAndInventory( vehicleId, inventoryItemId, vehiclePartsId, Boolean.TRUE );

        return "redirect:/parts-disposition/search?active=PARTS_DISPOSITION_SEARCH";
    }

    @GetMapping( "/{id}" )
    @TitleAndContent( title = "title.parts-disposition.view", content = "parts-disposition/parts-disposition-view", activeMenu = Menu.PARTS_DISPOSITION_VIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.JQUERY_PRINT, FrontEndLibrary.PARTS_DISPOSITION_VIEW } )
    public String view(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ){

        if( partsDispositionValidationService.view( redirectAttributes, id ) ){

            partsDispositionModelService.view( model, id, getLoggedInUser() );
            return viewRoot;
        }

        return "redirect:/parts-disposition/search?active=PARTS_DISPOSITION_SEARCH";
    }

    @PostMapping( "/take-action" )
    public String takeAction(
            @RequestParam( "id" ) Long id,
            @RequestParam( "action" ) Long actionId,
            @Valid PartsDispositionAdditionalData partsDispositionAdditionalData,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ) throws Exception {

        if( partsDispositionValidationService.takeActionValidation( actionId, redirectAttributes ) ) {

            partsDispositionModelService.takeAction( redirectAttributes, id, actionId, partsDispositionAdditionalData, getLoggedInUser() );
            log( LogEvent.PARTS_DECISION_TAKEN, actionId, "Parts Disposition Take action", request );
        }

        if( actionId.equals(AppConstants.PARTS_DISPOSITION_APPROVED_ACTION_ID ) || actionId.equals( AppConstants.PARTS_DISPOSITION_REJECT_ACTION_ID ) ) {

            return "redirect:/parts-disposition/search?active=PARTS_DISPOSITION_SEARCH";
        }
        else{

            return "redirect:/parts-disposition/search?active=WARRANTY_SEARCH";
        }

    }
}
