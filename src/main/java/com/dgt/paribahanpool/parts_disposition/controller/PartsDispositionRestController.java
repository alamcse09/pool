package com.dgt.paribahanpool.parts_disposition.controller;

import com.dgt.paribahanpool.enums.BudgetStateType;
import com.dgt.paribahanpool.parts_disposition.model.PartsDispositionSearchResponse;
import com.dgt.paribahanpool.parts_disposition.service.PartsDispositionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping( "/api/parts-disposition" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class PartsDispositionRestController {

    private final PartsDispositionService partsDispositionService;

    @GetMapping(
            value = "/search/{isForWarranty}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<PartsDispositionSearchResponse> search(
            @PathVariable( "isForWarranty" ) Boolean isForWarranty,
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return partsDispositionService.searchForDatatable( dataTablesInput, isForWarranty );
    }
}
