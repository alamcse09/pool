package com.dgt.paribahanpool.parts_disposition.model;

import com.dgt.paribahanpool.enums.WarrantyType;
import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.vehicle.model.VehicleParts;
import com.dgt.paribahanpool.vendor.model.Vendor;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.WorkflowEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table( name = "parts_disposition" )
@SQLDelete( sql = "UPDATE parts_disposition SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class PartsDisposition extends AuditableEntity implements WorkflowEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "parts_id" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String partsId;

    @Column( name = "name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String name;

    @Column( name = "name_bn" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String nameBn;

    @Column( name = "date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate useDate;

    @Column( name = "warranty_beginning_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyBeginningDate;

    @Column( name = "warranty_ending_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyEndingDate;

    @Column( name = "warranty_type" )
    private WarrantyType warrantyType;

    @Column( name = "reg_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String registrationNo;

    @Column( name = "chassis_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String chassisNo;

    @Column( name = "brandName" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String brandName;

    @Column( name = "model" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String model;

    @Column( name = "committee_decision" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String committeeDecision;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "inventory_id", foreignKey = @ForeignKey( name = "fk_parts_disposition_inventory_id" ) )
    private InventoryItem inventoryItem;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "vendor_id", foreignKey = @ForeignKey( name = "fk_parts_disposition_vendor_id" ) )
    private Vendor vendor;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "vehicle_parts_id", foreignKey = @ForeignKey( name = "fk_parts_disposition_vehicle_parts_id" ) )
    private VehicleParts vehicleParts;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "state_id", foreignKey = @ForeignKey( name = "fk_parts_disposition_state_id" ) )
    private State state;


}
