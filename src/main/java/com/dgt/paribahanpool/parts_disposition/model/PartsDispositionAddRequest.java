package com.dgt.paribahanpool.parts_disposition.model;

import com.dgt.paribahanpool.inventory_item.model.InventoryItemResponse;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.vendor.model.VendorSearchResponse;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Data
public class PartsDispositionAddRequest {

    private Long id;

    private String partsId;

    private String name;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate date;;

    private String registrationNo;
    private String chassisNo;
    private String brandName;
    private String model;

    private List<InventoryItemResponse> inventoryItemResponseList;

    @NotNull
    private Long inventoryItemId;

    private List<VendorSearchResponse> vendorViewResponseList;

    private Long vendorId;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyBeginningDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyEndingDate;
}
