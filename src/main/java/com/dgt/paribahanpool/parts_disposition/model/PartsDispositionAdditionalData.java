package com.dgt.paribahanpool.parts_disposition.model;

import com.dgt.paribahanpool.workflow.model.WorkflowAdditionalData;
import lombok.Data;

@Data
public class PartsDispositionAdditionalData implements WorkflowAdditionalData {

    private String commentDecision;
}
