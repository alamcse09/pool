package com.dgt.paribahanpool.parts_disposition.model;

import com.dgt.paribahanpool.workflow.model.StateResponse;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class PartsDispositionSearchResponse {

    private Long id;
    private String partsId;
    private String name;
    private LocalDate useDate;
    private LocalDate warrantyBeginningDate;
    private LocalDate warrantyEndingDate;
    private Long warrantyRemaining;
    private String registrationNo;
    private String chassisNo;
    private String brandName;
    private String model;

    private StateResponse stateResponse;
}
