package com.dgt.paribahanpool.parts_disposition.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PartsDispositionViewResponse {

    private Long id;
    private String partsId;
    private String name;
    private LocalDate date;
    private LocalDate warrantyBeginningDate;
    private LocalDate warrantyEndingDate;
    private String registrationNo;
    private String chassisNo;
    private String brandName;
    private String model;
}
