package com.dgt.paribahanpool.parts_disposition.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.inventory_item.model.InventoryItemResponse;
import com.dgt.paribahanpool.inventory_item.service.InventoryItemService;
import com.dgt.paribahanpool.parts_disposition.model.PartsDisposition;
import com.dgt.paribahanpool.parts_disposition.model.PartsDispositionAddRequest;
import com.dgt.paribahanpool.parts_disposition.model.PartsDispositionAdditionalData;
import com.dgt.paribahanpool.parts_disposition.model.PartsDispositionViewResponse;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.InventoryItemViewResponse;
import com.dgt.paribahanpool.vehicle.model.VehicleServiceApplication;
import com.dgt.paribahanpool.vendor.model.VendorSearchResponse;
import com.dgt.paribahanpool.vendor.service.VendorService;
import com.dgt.paribahanpool.warranty.model.WarrantyAdditionalData;
import com.dgt.paribahanpool.warranty.service.WarrantyService;
import com.dgt.paribahanpool.workflow.model.StateActionMapResponse;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class PartsDispositionModelService {

    private final VendorService vendorService;
    private final InventoryItemService inventoryItemService;
    private final PartsDispositionService partsDispositionService;
    private final StateActionMapService stateActionMapService;
    private final WarrantyService warrantyService;
    private final MvcUtil mvcUtil;

    public void addPartsDisposition(Model model) {

        List<InventoryItemResponse> inventoryItemViewResponseList = inventoryItemService.getInventoryItems();

        PartsDispositionAddRequest partsDispositionAddRequest = new PartsDispositionAddRequest();

        List<VendorSearchResponse> vendorSearchResponseList = vendorService.getAllVendorSearchResponseList();

        partsDispositionAddRequest.setVendorViewResponseList( vendorSearchResponseList );

        partsDispositionAddRequest.setInventoryItemResponseList( inventoryItemViewResponseList );

        model.addAttribute("partsDispositionAddRequest",partsDispositionAddRequest);
    }

    public void addPartsDispositionPost(PartsDispositionAddRequest partsDispositionAddRequest, Model model) {

        partsDispositionService.save( partsDispositionAddRequest );
        mvcUtil.addSuccessMessage( model, "parts-disposition.add.success" );
    }

    @Transactional
    public void saveWithVehicleAndInventory(Long vehicleId, Long inventoryItemId, Long vehiclePartsId, Boolean isForDisposition) {

        partsDispositionService.saveWithVehicleAndInventory( vehicleId, inventoryItemId, vehiclePartsId, isForDisposition );
//        mvcUtil.addSuccessMessage( model, "parts-disposition.add.success" );
    }

    public void view(Model model, Long id, UserPrincipal loggedInUser) {

        Optional<PartsDisposition> partsDispositionOptional = partsDispositionService.findById( id );

        if( partsDispositionOptional.isPresent() ){

            PartsDispositionViewResponse partsDispositionViewResponse = partsDispositionService.getPartsDispositionViewResponseFromPartsDisposition( partsDispositionOptional.get() );
            model.addAttribute( "partsDispositionViewResponse", partsDispositionViewResponse );

            if( partsDispositionOptional.get().getState().getId() != null ){

                List<StateActionMapResponse> stateActionMapResponseList = stateActionMapService.findStateActionMapResponseListByCurrentStateAndRoleIdSet( partsDispositionOptional.get().getState().getId(), loggedInUser.getRoleIds() );
                model.addAttribute( "stateActionList", stateActionMapResponseList );
            }
        }

    }

    public void takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, PartsDispositionAdditionalData partsDispositionAdditionalData, UserPrincipal loggedInUser) throws Exception {

        partsDispositionService.takeAction(id, actionId, partsDispositionAdditionalData, loggedInUser);

        mvcUtil.addSuccessMessage( redirectAttributes, "success.common.action.success" );
    }
}
