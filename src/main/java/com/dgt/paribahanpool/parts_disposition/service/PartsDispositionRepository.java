package com.dgt.paribahanpool.parts_disposition.service;

import com.dgt.paribahanpool.parts_disposition.model.PartsDisposition;
import com.dgt.paribahanpool.vehicle.model.VehicleServiceApplication;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface PartsDispositionRepository extends DataTablesRepository<PartsDisposition,Long> {


}
