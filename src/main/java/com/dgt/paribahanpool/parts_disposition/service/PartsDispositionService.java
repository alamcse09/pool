package com.dgt.paribahanpool.parts_disposition.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import com.dgt.paribahanpool.inventory_item.service.InventoryItemService;
import com.dgt.paribahanpool.parts_disposition.model.*;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.model.VehicleParts;
import com.dgt.paribahanpool.vehicle.service.VehiclePartsService;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import com.dgt.paribahanpool.vendor.model.Vendor;
import com.dgt.paribahanpool.vendor.service.VendorService;
import com.dgt.paribahanpool.warranty.model.WarrantyAdditionalData;
import com.dgt.paribahanpool.workflow.model.*;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

import static com.dgt.paribahanpool.parts_disposition.service.PartsDispositionSpecification.filterByStateId;
import static java.time.temporal.ChronoUnit.DAYS;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class PartsDispositionService implements WorkflowService<PartsDisposition> {

    private final PartsDispositionRepository partsDispositionRepository;
    private final StateService stateService;
    private final InventoryItemService inventoryItemService;
    private final VendorService vendorService;
    private final VehicleService vehicleService;
    private final VehiclePartsService vehiclePartsService;
    private final StateActionMapService stateActionMapService;

    @Override
    public Optional<PartsDisposition> findById(Long id) {
        return partsDispositionRepository.findById( id );
    }

    public PartsDisposition save(PartsDisposition partsDisposition ){

        return partsDispositionRepository.save( partsDisposition );
    }

    public void save( PartsDispositionAddRequest partsDispositionAddRequest ) {

        PartsDisposition partsDisposition = new PartsDisposition();

        if( partsDispositionAddRequest.getId() != null ) {

            Optional<PartsDisposition> partsDispositionOptional = partsDispositionRepository.findById( partsDispositionAddRequest.getId() );

            if( partsDispositionOptional.isPresent() ){

                partsDisposition = partsDispositionOptional.get();
            }
        }

        partsDisposition = getPartsDispositionFromPartsDispositionAddRequest( partsDisposition, partsDispositionAddRequest );

        save( partsDisposition );

    }

    private PartsDisposition getPartsDispositionFromPartsDispositionAddRequest(PartsDisposition partsDisposition, PartsDispositionAddRequest partsDispositionAddRequest) {

        partsDisposition.setPartsId( partsDispositionAddRequest.getPartsId() );
        partsDisposition.setName( partsDispositionAddRequest.getName() );
        partsDisposition.setUseDate( partsDispositionAddRequest.getDate() );
        partsDisposition.setWarrantyEndingDate( partsDispositionAddRequest.getWarrantyEndingDate() );
        partsDisposition.setWarrantyBeginningDate( partsDispositionAddRequest.getWarrantyBeginningDate() );
        partsDisposition.setRegistrationNo( partsDispositionAddRequest.getRegistrationNo() );
        partsDisposition.setChassisNo( partsDispositionAddRequest.getChassisNo() );
        partsDisposition.setBrandName( partsDispositionAddRequest.getBrandName() );
        partsDisposition.setModel( partsDispositionAddRequest.getModel() );

        if( partsDispositionAddRequest.getInventoryItemId() != null ){

            Optional<InventoryItem> inventoryItemOptional = inventoryItemService.findById( partsDispositionAddRequest.getInventoryItemId() );
            if( inventoryItemOptional.isPresent() ){

                InventoryItem inventoryItem = inventoryItemOptional.get();
                partsDisposition.setInventoryItem( inventoryItem );
            }
        }

        if( partsDispositionAddRequest.getVendorId() != null ){

            Optional<Vendor> vendorOptional = vendorService.findById( partsDispositionAddRequest.getVendorId() );
            if( vendorOptional.isPresent() ){

                Vendor vendor = vendorOptional.get();
                partsDisposition.setVendor( vendor );
            }
        }

        if( partsDisposition.getState() == null ){

            State state = stateService.findStateReferenceById( AppConstants.PARTS_DISPOSITION_SENT_TO_VENDOR_STATE_ID );
            partsDisposition.setState( state );
        }

        return partsDisposition;
    }

    public DataTablesOutput<PartsDispositionSearchResponse> searchForDatatable(DataTablesInput dataTablesInput, Boolean isForWarranty) {

        if( isForWarranty ){

            Specification<PartsDisposition> partsDispositionSpecification = filterByStateId(AppConstants.WARRANTY_SENT_TO_VENDOR_STATE_ID).or( filterByStateId(AppConstants.WARRANTY_APPROVE_STATE_ID) ).or( filterByStateId(AppConstants.WARRANTY_REJECT_STATE_ID) );

            return partsDispositionRepository.findAll( dataTablesInput, null, partsDispositionSpecification, this::getPartsDispositionSearchResponseFromPartsDisposition );
        }
        else {

            Specification<PartsDisposition> partsDispositionSpecification = filterByStateId(AppConstants.PARTS_DISPOSITION_SENT_TO_VENDOR_STATE_ID).or( filterByStateId(AppConstants.PARTS_DISPOSITION_APPROVE_STATE_ID) ).or( filterByStateId(AppConstants.PARTS_DISPOSITION_REJECT_STATE_ID) );

            return partsDispositionRepository.findAll( dataTablesInput, null, partsDispositionSpecification, this::getPartsDispositionSearchResponseFromPartsDisposition );
        }

    }

    private PartsDispositionSearchResponse getPartsDispositionSearchResponseFromPartsDisposition(PartsDisposition partsDisposition) {

        State state = partsDisposition.getState();

        StateResponse stateResponse = null;

        if( state != null ){

            stateResponse = state.getResponse();
        }

        return PartsDispositionSearchResponse.builder()
                .id( partsDisposition.getId() )
                .partsId( partsDisposition.getPartsId() )
                .name( partsDisposition.getName() )
                .useDate( partsDisposition.getUseDate() )
                .warrantyBeginningDate( partsDisposition.getWarrantyBeginningDate() )
                .warrantyEndingDate( partsDisposition.getWarrantyEndingDate() )
                .warrantyRemaining( DAYS.between(LocalDate.now(), partsDisposition.getWarrantyEndingDate() ) )
                .registrationNo( partsDisposition.getRegistrationNo() )
                .chassisNo( partsDisposition.getChassisNo() )
                .brandName( partsDisposition.getBrandName() )
                .model( partsDisposition.getModel() )
                .stateResponse( stateResponse )
                .build();
    }

    public void saveWithVehicleAndInventory(Long vehicleId, Long inventoryItemId, Long vehiclePartsId, Boolean isForDisposition) {

        Optional<Vehicle> vehicleOptional = vehicleService.findById( vehicleId );

        Optional<InventoryItem> inventoryItemOptional = inventoryItemService.findById( inventoryItemId );

        Optional<VehicleParts> vehiclePartsOptional = vehiclePartsService.findById( vehiclePartsId );

        PartsDisposition partsDisposition = new PartsDisposition();

        if( vehiclePartsOptional.isPresent() ){

            VehicleParts vehicleParts = vehiclePartsOptional.get();

            partsDisposition.setName( vehicleParts.getInventoryItem().getProductNameEn() );
            partsDisposition.setNameBn( vehicleParts.getInventoryItem().getProductNameBn() );
            partsDisposition.setUseDate( vehicleParts.getUseDate() );
            partsDisposition.setWarrantyBeginningDate( vehicleParts.getWarrantyStartDate() );
            partsDisposition.setWarrantyEndingDate( vehicleParts.getWarrantyEndDate() );
            partsDisposition.setWarrantyType( vehicleParts.getWarrantyType() );
            partsDisposition.setRegistrationNo( vehicleParts.getVehicle().getRegistrationNo() );
            partsDisposition.setChassisNo( vehicleParts.getVehicle().getChassisNo() );
            partsDisposition.setBrandName( vehicleParts.getVehicle().getBrand().getNameBn() );
            partsDisposition.setModel( vehicleParts.getVehicle().getModel().getNameBn() );
            partsDisposition.setVehicleParts( vehicleParts );
        }

        if( inventoryItemOptional.isPresent() ){

            partsDisposition.setInventoryItem( inventoryItemOptional.get() );
        }

        if( isForDisposition ){

            Long startStateId = AppConstants.PARTS_DISPOSITION_SENT_TO_VENDOR_STATE_ID;

            State state = stateService.findStateReferenceById( startStateId );
            partsDisposition.setState( state );
        }
        else if( !isForDisposition ){

            Long startStateId = AppConstants.WARRANTY_SENT_TO_VENDOR_STATE_ID;

            State state = stateService.findStateReferenceById( startStateId );
            partsDisposition.setState( state );
        }

        save( partsDisposition );
    }

    public PartsDispositionViewResponse getPartsDispositionViewResponseFromPartsDisposition(PartsDisposition partsDisposition) {

        PartsDispositionViewResponse partsDispositionViewResponse = new PartsDispositionViewResponse();

        partsDispositionViewResponse.setId( partsDisposition.getId() );
        partsDispositionViewResponse.setPartsId( partsDisposition.getPartsId() );
        partsDispositionViewResponse.setName( partsDisposition.getName() );
        partsDispositionViewResponse.setDate( partsDisposition.getUseDate() );
        partsDispositionViewResponse.setWarrantyBeginningDate( partsDisposition.getWarrantyBeginningDate() );
        partsDispositionViewResponse.setWarrantyEndingDate( partsDisposition.getWarrantyEndingDate() );
        partsDispositionViewResponse.setRegistrationNo( partsDisposition.getRegistrationNo() );
        partsDispositionViewResponse.setChassisNo( partsDisposition.getChassisNo() );
        partsDispositionViewResponse.setBrandName( partsDisposition.getBrandName() );
        partsDispositionViewResponse.setModel( partsDisposition.getModel() );

        return partsDispositionViewResponse;
    }

    public void takeAction(Long id, Long actionId, PartsDispositionAdditionalData partsDispositionAdditionalData, UserPrincipal loggedInUser) throws Exception {

        stateActionMapService.takeAction( this, id, actionId, loggedInUser, (workflowEntity ) -> setAdditionalData( workflowEntity, actionId, partsDispositionAdditionalData ));
    }

    private WorkflowAdditionalData setAdditionalData(WorkflowEntity workflowEntity, Long actionId, WorkflowAdditionalData additionalActionData) {

        PartsDisposition partsDisposition = (PartsDisposition) workflowEntity;

        PartsDispositionAdditionalData partsDispositionAdditionalData = (PartsDispositionAdditionalData) additionalActionData;

        if( actionId.equals( AppConstants.PARTS_DISPOSITION_APPROVED_ACTION_ID) ){

            VehicleParts vehicleParts = partsDisposition.getVehicleParts();

            vehicleParts.setIsActive( false );
            partsDisposition.setVehicleParts( vehicleParts );
        }

        if( actionId.equals( AppConstants.WARRANTY_REJECT_ACTION_ID ) ){

            partsDisposition.setCommitteeDecision( partsDispositionAdditionalData.getCommentDecision() );
        }

        return additionalActionData;
    }
}
