package com.dgt.paribahanpool.parts_disposition.service;

import com.dgt.paribahanpool.parts_disposition.model.PartsDisposition;
import com.dgt.paribahanpool.parts_disposition.model.PartsDisposition_;
import com.dgt.paribahanpool.workflow.model.State_;
import org.springframework.data.jpa.domain.Specification;

public class PartsDispositionSpecification {

    public static Specification<PartsDisposition> filterByStateId( Long id ){

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( PartsDisposition_.STATE ).get(State_.ID ), id );
    }
}
