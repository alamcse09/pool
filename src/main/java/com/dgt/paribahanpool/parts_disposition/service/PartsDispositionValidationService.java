package com.dgt.paribahanpool.parts_disposition.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import com.dgt.paribahanpool.inventory_item.service.InventoryItemService;
import com.dgt.paribahanpool.parts_disposition.model.PartsDisposition;
import com.dgt.paribahanpool.parts_disposition.model.PartsDispositionAddRequest;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.model.VehicleParts;
import com.dgt.paribahanpool.vehicle.model.VehicleViewResponse;
import com.dgt.paribahanpool.vehicle.service.VehiclePartsService;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class PartsDispositionValidationService {

    private final MvcUtil mvcUtil;
    private final InventoryItemService inventoryItemService;
    private final VehicleService vehicleService;
    private final VehiclePartsService vehiclePartsService;
    private final PartsDispositionService partsDispositionService;

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, PartsDispositionAddRequest partsDispositionAddRequest) {

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "partsDispositionAddRequest" )
                .object( partsDispositionAddRequest )
                .title( "title.parts-disposition.add" )
                .content( "parts-disposition/parts-disposition-add" )
                .activeMenu( Menu.PARTS_DISPOSITION_ADD )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.PARTS_DISPOSITION_ADD } )
                .build();

        Boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        return isValid;
    }

    public Boolean addPartsDispositionPost(RedirectAttributes redirectAttributes, PartsDispositionAddRequest partsDispositionAddRequest) {

        return true;
    }

    @Transactional
    public Boolean addPartsDispositionValidFromVehicleAndItem(RedirectAttributes redirectAttributes, Long vehicleId, Long inventoryItemId, Long vehiclePartsId ) {

        Optional<Vehicle> vehicleOptional = vehicleService.findById( vehicleId );

        Optional<InventoryItem> inventoryItemOptional = inventoryItemService.findById( inventoryItemId );

        Optional<VehicleParts> vehiclePartsOptional = vehiclePartsService.findById( vehiclePartsId );

        if( !vehicleOptional.isPresent() || !inventoryItemOptional.isPresent() || !vehiclePartsOptional.isPresent() ){

            mvcUtil.addSuccessMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }
        else{

            return true;
        }
    }

    public Boolean view(RedirectAttributes redirectAttributes, Long id) {

        Optional<PartsDisposition> partsDispositionOptional = partsDispositionService.findById( id );

        if( !partsDispositionOptional.isPresent() ){

            mvcUtil.addSuccessMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    public Boolean takeActionValidation(Long actionId, RedirectAttributes redirectAttributes) {

        return true;
    }
}
