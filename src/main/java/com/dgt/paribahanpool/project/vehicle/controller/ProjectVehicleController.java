package com.dgt.paribahanpool.project.vehicle.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.project.vehicle.model.ProjectVehicleAddRequest;
import com.dgt.paribahanpool.project.vehicle.service.ProjectVehicleModelService;
import com.dgt.paribahanpool.project.vehicle.service.ProjectVehicleValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/project-vehicle" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired })
public class ProjectVehicleController extends MVCController {

    private final ProjectVehicleModelService projectVehicleModelService;
    private final ProjectVehicleValidationService projectVehicleValidationService;

    @GetMapping( "/add")
    @TitleAndContent( title = "title.project.vehicle.add", content = "project-vehicle/project-vehicle-add", activeMenu = Menu.PROJECT_VEHICLE_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.PROJECT_VEHICLE_ADD_FORM, FrontEndLibrary.BRAND_MODEL_ADD } )
    public String add(
            HttpServletRequest httpServletRequest,
            Model model
    ){

        projectVehicleModelService.addProjectVehicleGet(model);
        return viewRoot;
    }

    @PostMapping( value = "/add" )
    public String addProjectVehicle(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid ProjectVehicleAddRequest projectVehicleAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !projectVehicleValidationService.handleBindingResultForAddFormPost( model, bindingResult, projectVehicleAddRequest ) ) {

            return viewRoot;
        }

        projectVehicleModelService.addProjectVehiclePost( model, projectVehicleAddRequest );
        log( LogEvent.PROJECT_VEHICLE_ADDED, projectVehicleAddRequest.getId(), "Project Vehicle Added", request );

        return "redirect:/project-vehicle/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.project.vehicle.search", content = "project-vehicle/project-vehicle-search", activeMenu = Menu.PROJECT_VEHICLE_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.PROJECT_VEHICLE_SEARCH } )
    public String search(
            HttpServletRequest request,
            Model model
    ){

        return viewRoot;
    }

    @GetMapping( "/edit/{projectVehicleid}" )
    @TitleAndContent( title = "title.project.vehicle.edit", content = "project-vehicle/project-vehicle-add", activeMenu = Menu.PROJECT_VEHICLE_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.PROJECT_VEHICLE_ADD_FORM, FrontEndLibrary.BRAND_MODEL_ADD } )
    public String edit(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable( "projectVehicleid" ) Long projectVehicleId
    ){

        log.debug( "Rendering edit Mechanic Form" );

        if( projectVehicleValidationService.editProjectVehicle( redirectAttributes, projectVehicleId ) ) {

            projectVehicleModelService.editProjectVehicle( model, projectVehicleId );
            return viewRoot;
        }

        return "redirect:/project-vehicle/search";
    }

    @PostMapping( "/edit/{projectVehicleid}" )
    public String editProjectVehicle(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid ProjectVehicleAddRequest projectVehicleAddRequest,
            BindingResult bindingResult,
            @PathVariable( "projectVehicleid" ) Long projectVehicleId,
            HttpServletRequest request
    ){

        if( !projectVehicleValidationService.handleBindingResultForEditFormPost( model, bindingResult, projectVehicleAddRequest) ){

            return viewRoot;
        }

        if( projectVehicleValidationService.editProjectVehicle( redirectAttributes, projectVehicleId ) ) {

            projectVehicleModelService.editProjectVehiclePost( projectVehicleAddRequest, redirectAttributes );
            log( LogEvent.PROJECT_VEHICLE_EDITED, projectVehicleAddRequest.getId(), "Project Vehicle Edited", request );
        }

        return "redirect:/project-vehicle/search";
    }

}
