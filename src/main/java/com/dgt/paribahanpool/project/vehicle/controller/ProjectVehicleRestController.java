package com.dgt.paribahanpool.project.vehicle.controller;

import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.project.vehicle.model.ProjectVehicleSearchResponse;
import com.dgt.paribahanpool.project.vehicle.service.ProjectVehicleService;
import com.dgt.paribahanpool.project.vehicle.service.ProjectVehicleValidationService;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping( "/api/project-vehicle" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired } )
public class ProjectVehicleRestController {

    private final ProjectVehicleService projectVehicleService;
    private final ProjectVehicleValidationService projectVehicleValidationService;

    @GetMapping( "/search" )
    public DataTablesOutput<ProjectVehicleSearchResponse> search(
            DataTablesInput dataTablesInput
    ){

        return projectVehicleService.search( dataTablesInput );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id
    ) throws NotFoundException {

        RestValidationResult restValidationResult = projectVehicleValidationService.delete( id );
        if( restValidationResult.getSuccess() ){

            projectVehicleService.delete( id );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{
            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }
}
