package com.dgt.paribahanpool.project.vehicle.model;

import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table( name = "project_vehicle")
@ToString( exclude = { "vehicle" } )
@EqualsAndHashCode( exclude = { "vehicle" } )
@SQLDelete( sql = "UPDATE project_vehicle SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class ProjectVehicle extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "project_organization_name")
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String projectOrganizationName;

    @Column( name = "received_date")
    private LocalDate receivedDate;

    @Column( name = "received_documents_name")
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String receivedDocumentsName;

    @Column( name = "comments")
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String comments;

    @OneToOne( cascade = {CascadeType.ALL} )
    @JoinColumn( name = "vehicle_id", foreignKey = @ForeignKey( name = "fk_project_vehicle_vehicle_id" ) )
    private Vehicle vehicle;

}
