package com.dgt.paribahanpool.project.vehicle.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.VehicleType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.vehicle.model.BrandViewResponse;
import com.dgt.paribahanpool.vehicle.model.ModelViewResponse;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
public class ProjectVehicleAddRequest {

    public Long id;

    public Long vehicleId;

    @Min( value = 1900, message = "{validation.common.min}" )
    @Max( value = 2100, message = "{validation.common.max}" )
    @NotNull( message = "{validation.common.required}")
    public Integer modelYear;

//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
//    @NotBlank( message = "{validation.common.required}" )
//    public String brand;

    @NotBlank( message = "{validation.common.required}" )
    public String registrationNo;

    @NotBlank( message = "{validation.common.required}" )
    public String chassisNo;

    @NotBlank( message = "{validation.common.required}" )
    public String engineNo;

    public String engineCC;

    public VehicleType vehicleType;

    @NotBlank( message = "{validation.common.required}" )
    public String projectOrganizationName;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    @NotNull( message = "{validation.common.required}" )
    public LocalDate receivedDate;

    private List<BrandViewResponse> brandViewResponseList;

    private List<ModelViewResponse> modelViewResponseList;

    @NotNull
    private Long brandId;

    @NotNull
    private Long modelId;

    public String receivedDocumentsName;

    public String comments;

    private MultipartFile[] files;

    List<DocumentMetadata> documents = Collections.EMPTY_LIST;
}
