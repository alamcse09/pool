package com.dgt.paribahanpool.project.vehicle.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@Builder
public class ProjectVehicleSearchResponse {

    private Long id;

    private String projectOrganizationName;

    @DateTimeFormat( pattern = "dd/MM/yyyy" )
    private LocalDate receivedDate;

    VehicleSearchResponseForProject vehicle;

}
