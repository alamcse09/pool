package com.dgt.paribahanpool.project.vehicle.model;

import com.dgt.paribahanpool.enums.VehicleType;
import com.dgt.paribahanpool.vehicle.model.BrandViewResponse;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VehicleSearchResponseForProject {

    private String registrationNo;
    private String chassisNo;
    private String engineNo;
    private Integer modelYear;
    private BrandViewResponse brand;
    private String engineCC;
    private VehicleType vehicleType;
}
