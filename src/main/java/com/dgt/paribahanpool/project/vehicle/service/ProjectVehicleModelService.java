package com.dgt.paribahanpool.project.vehicle.service;

import com.dgt.paribahanpool.project.vehicle.model.ProjectVehicle;
import com.dgt.paribahanpool.project.vehicle.model.ProjectVehicleAddRequest;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.BrandViewResponse;
import com.dgt.paribahanpool.vehicle.service.BrandService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class ProjectVehicleModelService {

    private final ProjectVehicleService projectVehicleService;
    private final MvcUtil mvcUtil;
    private final BrandService brandService;

    public void addProjectVehicleGet(Model model) {

        ProjectVehicleAddRequest projectVehicleAddRequest = new ProjectVehicleAddRequest();

        List<BrandViewResponse> brandViewResponseList = brandService.getAllBrandViewResponse();

        projectVehicleAddRequest.setBrandViewResponseList( brandViewResponseList );

        model.addAttribute("projectVehicleAddRequest", projectVehicleAddRequest );
    }

    public void addProjectVehiclePost( Model model, ProjectVehicleAddRequest projectVehicleAddRequest ) {

        projectVehicleService.save(projectVehicleAddRequest);
        mvcUtil.addSuccessMessage( model, "vehicle.add.success" );
    }

    public void editProjectVehicle( Model model, Long id ) {

        Optional<ProjectVehicle> projectVehicleOptional = projectVehicleService.findById( id );

        if( projectVehicleOptional.isPresent() ){

            ProjectVehicleAddRequest projectVehicleAddRequest = projectVehicleService.getProjectVehicleAddRequestFromProjectVehicle( projectVehicleOptional.get() );

            List<BrandViewResponse> brandViewResponseList = brandService.getAllBrandViewResponse();

            projectVehicleAddRequest.setBrandViewResponseList( brandViewResponseList );

            model.addAttribute( "projectVehicleAddRequest", projectVehicleAddRequest );
        }
    }

    public void editProjectVehiclePost( ProjectVehicleAddRequest projectVehicleAddRequest, RedirectAttributes redirectAttributes ) {

        projectVehicleService.save( projectVehicleAddRequest );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.project-vehicle.edit" );
    }
}
