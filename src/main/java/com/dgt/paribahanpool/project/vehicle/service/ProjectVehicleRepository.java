package com.dgt.paribahanpool.project.vehicle.service;

import com.dgt.paribahanpool.project.vehicle.model.ProjectVehicle;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface ProjectVehicleRepository extends DataTablesRepository<ProjectVehicle,Long> {
}
