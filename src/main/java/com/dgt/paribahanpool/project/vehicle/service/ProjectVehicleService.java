package com.dgt.paribahanpool.project.vehicle.service;

import com.dgt.paribahanpool.document.model.Document;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.project.vehicle.model.ProjectVehicle;
import com.dgt.paribahanpool.project.vehicle.model.ProjectVehicleAddRequest;
import com.dgt.paribahanpool.project.vehicle.model.ProjectVehicleSearchResponse;
import com.dgt.paribahanpool.project.vehicle.model.VehicleSearchResponseForProject;
import com.dgt.paribahanpool.vehicle.model.Brand;
import com.dgt.paribahanpool.vehicle.model.BrandViewResponse;
import com.dgt.paribahanpool.vehicle.model.Model;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.service.BrandService;
import com.dgt.paribahanpool.vehicle.service.ModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class ProjectVehicleService {

    private final DocumentService documentService;
    private final ProjectVehicleRepository projectVehicleRepository;
    private final BrandService brandService;
    private final ModelService modelService;

    public ProjectVehicle save( ProjectVehicle projectVehicle ) {

        return projectVehicleRepository.save(projectVehicle);
    }

    public Optional<ProjectVehicle> findById( Long id ){

        return projectVehicleRepository.findById( id );
    }

    @Transactional( propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE )
    public ProjectVehicle save( ProjectVehicleAddRequest projectVehicleAddRequest ) {

        ProjectVehicle projectVehicle = new ProjectVehicle();
        Vehicle vehicle = new Vehicle();

        if( projectVehicleAddRequest.getId() != null ){

            projectVehicle = findById( projectVehicleAddRequest.getId() ).get();
            vehicle = projectVehicle.getVehicle();
        }

        projectVehicle = getProjectVehicleFromProjectVehicleAddRequest( vehicle, projectVehicle, projectVehicleAddRequest );

        return save(projectVehicle);
    }

    private ProjectVehicle getProjectVehicleFromProjectVehicleAddRequest( Vehicle vehicle, ProjectVehicle projectVehicle, ProjectVehicleAddRequest projectVehicleAddRequest ) {

        projectVehicle.setProjectOrganizationName( projectVehicleAddRequest.getProjectOrganizationName() );
        projectVehicle.setReceivedDate( projectVehicleAddRequest.getReceivedDate() );
        projectVehicle.setReceivedDocumentsName( projectVehicleAddRequest.getReceivedDocumentsName() );
        projectVehicle.setComments( projectVehicleAddRequest.getComments() );

        vehicle.setModelYear( projectVehicleAddRequest.getModelYear() );

        vehicle.setRegistrationNo( projectVehicleAddRequest.getRegistrationNo() );
        vehicle.setChassisNo( projectVehicleAddRequest.getChassisNo() );
        vehicle.setEngineNo( projectVehicleAddRequest.getEngineNo() );
        vehicle.setEngineCC( projectVehicleAddRequest.getEngineCC() );
        vehicle.setVehicleType( projectVehicleAddRequest.getVehicleType() );
        vehicle.setFromProject( true );

        if( projectVehicleAddRequest.getBrandId() != null ){

            Optional<Brand> brandOptional = brandService.findById( projectVehicleAddRequest.getBrandId() );

            if( brandOptional.isPresent() ){

                vehicle.setBrand( brandOptional.get() );
            }
        }

        if( projectVehicleAddRequest.getModelId() != null ){

            Optional<Model> modelOptional = modelService.findById( projectVehicleAddRequest.getModelId() );

            if( modelOptional.isPresent() ){

                vehicle.setModel( modelOptional.get() );
            }
        }

        if( vehicle.getDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( projectVehicleAddRequest.getFiles(), Collections.EMPTY_MAP );
            vehicle.setDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( projectVehicleAddRequest.getFiles(), vehicle.getDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        projectVehicle.setVehicle( vehicle );


        return projectVehicle;
    }

    public DataTablesOutput<ProjectVehicleSearchResponse> search( DataTablesInput dataTablesInput ) {

        return projectVehicleRepository.findAll( dataTablesInput, this::getProjectVehicleSearchResponseFromProjectVehicle );
    }

    private ProjectVehicleSearchResponse getProjectVehicleSearchResponseFromProjectVehicle( ProjectVehicle projectVehicle ) {

        Vehicle vehicle = projectVehicle.getVehicle();

        BrandViewResponse brandViewResponse = BrandViewResponse.builder()
                .nameEn((vehicle.getBrand()==null || vehicle.getBrand().getNameEn()==null) ? "N/A" : vehicle.getBrand().getNameEn())
                .nameBn((vehicle.getBrand()==null || vehicle.getBrand().getNameBn()==null) ? "N/A" : vehicle.getBrand().getNameBn())
                .build();

        VehicleSearchResponseForProject vehicleSearchResponseForProject = VehicleSearchResponseForProject.builder()
                .vehicleType( vehicle.getVehicleType() )
                .brand( brandViewResponse )
                .chassisNo( vehicle.getChassisNo() )
                .engineCC( vehicle.getEngineCC() )
                .engineNo( vehicle.getEngineNo() )
                .modelYear( vehicle.getModelYear() )
                .registrationNo( vehicle.getRegistrationNo() )
                .build();

        return ProjectVehicleSearchResponse.builder()
                .id( projectVehicle.getId() )
                .projectOrganizationName( projectVehicle.getProjectOrganizationName() )
                .receivedDate( projectVehicle.getReceivedDate() )
                .vehicle( vehicleSearchResponseForProject )
                .build();
    }

    public void delete(Long id) {

        projectVehicleRepository.deleteById( id );
    }

    public Boolean existById(Long id) {

        return projectVehicleRepository.existsById( id );
    }

    public ProjectVehicleAddRequest getProjectVehicleAddRequestFromProjectVehicle( ProjectVehicle projectVehicle ) {

        ProjectVehicleAddRequest projectVehicleAddRequest = new ProjectVehicleAddRequest();

        projectVehicleAddRequest.setId( projectVehicle.getId() );
        projectVehicleAddRequest.setProjectOrganizationName( projectVehicle.getProjectOrganizationName() );
        projectVehicleAddRequest.setReceivedDate( projectVehicle.getReceivedDate() );
        projectVehicleAddRequest.setReceivedDocumentsName( projectVehicle.getReceivedDocumentsName() );
        projectVehicleAddRequest.setComments( projectVehicle.getComments() );

        Vehicle vehicle = projectVehicle.getVehicle();

        projectVehicleAddRequest.setVehicleId( vehicle.getId() );
        projectVehicleAddRequest.setModelYear( vehicle.getModelYear() );
        projectVehicleAddRequest.setBrandId( vehicle.getBrand().getId() );
        projectVehicleAddRequest.setModelId( vehicle.getModel().getId() );
        projectVehicleAddRequest.setRegistrationNo( vehicle.getRegistrationNo() );
        projectVehicleAddRequest.setChassisNo( vehicle.getChassisNo() );
        projectVehicleAddRequest.setEngineNo( vehicle.getEngineNo() );
        projectVehicleAddRequest.setEngineCC( vehicle.getEngineCC() );
        projectVehicleAddRequest.setVehicleType( vehicle.getVehicleType() );

        if( vehicle.getDocGroupId() != null && vehicle.getDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( vehicle.getDocGroupId() );
            projectVehicleAddRequest.setDocuments( documentMetadataList );
        }

        return projectVehicleAddRequest;
    }
}
