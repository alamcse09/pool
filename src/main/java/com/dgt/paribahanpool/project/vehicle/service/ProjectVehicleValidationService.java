package com.dgt.paribahanpool.project.vehicle.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.project.vehicle.model.ProjectVehicle;
import com.dgt.paribahanpool.project.vehicle.model.ProjectVehicleAddRequest;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.BrandViewResponse;
import com.dgt.paribahanpool.vehicle.service.BrandService;
import com.dgt.paribahanpool.vehicle.service.VehicleValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class ProjectVehicleValidationService {

    private final MvcUtil mvcUtil;
    private final VehicleValidationService vehicleValidationService;
    private final ProjectVehicleService projectVehicleService;
    private final BrandService brandService;

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, ProjectVehicleAddRequest projectVehicleAddRequest ){

        List<BrandViewResponse> brandViewResponseList = brandService.getAllBrandViewResponse();

        projectVehicleAddRequest.setBrandViewResponseList( brandViewResponseList );

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "projectVehicleAddRequest" )
                .object( projectVehicleAddRequest )
                .title( "title.project.vehicle.add" )
                .content( "project-vehicle/project-vehicle-add" )
                .activeMenu( Menu.PROJECT_VEHICLE_ADD )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.PROJECT_VEHICLE_ADD_FORM, FrontEndLibrary.BRAND_MODEL_ADD } )
                .build();

        boolean isValid =  mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        if(!isValid) return false;

        return vehicleValidationService.validateVehicleUniqueConstraints(model, projectVehicleAddRequest.getId(), projectVehicleAddRequest.getRegistrationNo(), projectVehicleAddRequest.getEngineNo(), projectVehicleAddRequest.getChassisNo(), params);
    }

    public RestValidationResult delete(Long id) throws NotFoundException {

        Boolean exist = projectVehicleService.existById( id );
        if( !exist ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean editProjectVehicle( RedirectAttributes redirectAttributes, Long id ) {

        Optional<ProjectVehicle> projectVehicleOptional = projectVehicleService.findById( id );

        if( !projectVehicleOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    public Boolean handleBindingResultForEditFormPost( Model model, BindingResult bindingResult, ProjectVehicleAddRequest projectVehicleAddRequest ) {

        List<BrandViewResponse> brandViewResponseList = brandService.getAllBrandViewResponse();

        projectVehicleAddRequest.setBrandViewResponseList( brandViewResponseList );

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "projectVehicleAddRequest" )
                .object( projectVehicleAddRequest )
                .title( "title.project.vehicle.edit" )
                .content( "project-vehicle/project-vehicle-add" )
                .activeMenu( Menu.PROJECT_VEHICLE_ADD )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.PROJECT_VEHICLE_ADD_FORM, FrontEndLibrary.BRAND_MODEL_ADD } )
                .build();

        Boolean isValid =  mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        if(!isValid)
            return false;

        return vehicleValidationService.validateVehicleUniqueConstraints( model, projectVehicleAddRequest.getVehicleId(), projectVehicleAddRequest.getRegistrationNo(), projectVehicleAddRequest.getEngineNo(), projectVehicleAddRequest.getChassisNo(), params );
    }
}
