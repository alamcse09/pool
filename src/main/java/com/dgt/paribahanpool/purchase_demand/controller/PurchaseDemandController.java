package com.dgt.paribahanpool.purchase_demand.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.marine.model.MarineAddRequest;
import com.dgt.paribahanpool.purchase_demand.model.PurchaseDemandAddRequest;
import com.dgt.paribahanpool.purchase_demand.service.PurchaseDemandModelService;
import com.dgt.paribahanpool.purchase_demand.service.PurchaseDemandValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/purchase-demand")
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class PurchaseDemandController extends MVCController {

    private final PurchaseDemandModelService purchaseDemandModelService;
    private final PurchaseDemandValidationService purchaseDemandValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.purchase.demand.add", content = "purchase-demand/purchase-demand-add", activeMenu = Menu.PURCHASE_DEMAND_ADD)
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.PURCHASE_DEMAND_ADD_FORM } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add purchase demand form" );
        purchaseDemandModelService.addPurchaseDemand( model );
        return viewRoot;
    }

    @PostMapping( value = "/add" )
    public String addPurchaseDemand(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid PurchaseDemandAddRequest purchaseDemandAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !purchaseDemandValidationService.handleBindingResultForAddFormPost( model, bindingResult, purchaseDemandAddRequest ) ) {

            return viewRoot;
        }

        purchaseDemandModelService.addPurchaseDemandPost( purchaseDemandAddRequest, redirectAttributes );
        log( LogEvent.PURCHASE_DEMAND_ADDED, purchaseDemandAddRequest.getId(), "Purchase demand added", request );

        return "redirect:/purchase-demand/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.purchase-demand.search", content = "purchase-demand/purchase-demand-search", activeMenu = Menu.PURCHASE_DEMAND_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.PURCHASE_DEMAND_SEARCH } )
    public String getPurchaseDemandSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Purchase Demand Search page has been called" );
        return viewRoot;
    }

    @PostMapping( value = "/edit" )
    public String editPurchaseDemand(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid PurchaseDemandAddRequest purchaseDemandAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !purchaseDemandValidationService.handleBindingResultForAddFormPost( model, bindingResult, purchaseDemandAddRequest ) ) {

            return viewRoot;
        }

        if( purchaseDemandValidationService.editPurchaseDemand( purchaseDemandAddRequest.getId(), redirectAttributes )) {

            purchaseDemandModelService.editPurchaseDemandPost( purchaseDemandAddRequest, redirectAttributes );
            log( LogEvent.PURCHASE_DEMAND_EDITED, purchaseDemandAddRequest.getId(), "Purchase demand edited", request );
        }


        return "redirect:/purchase-demand/add";
    }

    @GetMapping( "/edit/{id}" )
    @TitleAndContent( title = "title.purchase.demand.edit", content = "purchase-demand/purchase-demand-add", activeMenu = Menu.PURCHASE_DEMAND_ADD)
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.PURCHASE_DEMAND_ADD_FORM } )
    public String edit(
            HttpServletRequest request,
            RedirectAttributes redirectAttributes,
            @PathVariable("id") Long id,
            Model model
    ){

        if( purchaseDemandValidationService.editPurchaseDemand( id, redirectAttributes )) {

            purchaseDemandModelService.editPurchaseDemandGet( model, id );
            return viewRoot;
        }

        return "redirect:/purchase-deman/search";
    }
}
