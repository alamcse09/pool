package com.dgt.paribahanpool.purchase_demand.controller;

import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.purchase_demand.model.PurchaseDemandSearchResponse;
import com.dgt.paribahanpool.purchase_demand.service.PurchaseDemandService;
import com.dgt.paribahanpool.purchase_demand.service.PurchaseDemandValidationService;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping( "/api/purchase-demand" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class PurchaseDemandRestController {

    private final PurchaseDemandService purchaseDemandService;
    private final PurchaseDemandValidationService purchaseDemandValidationService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<PurchaseDemandSearchResponse> searchPurchaseDemand(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return purchaseDemandService.searchForDatatable( dataTablesInput );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id
    ) throws NotFoundException {

        RestValidationResult restValidationResult = purchaseDemandValidationService.delete( id );

        if( restValidationResult.getSuccess() ) {

            purchaseDemandService.delete( id );

            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        } else {

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }
}
