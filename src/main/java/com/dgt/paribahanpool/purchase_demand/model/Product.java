package com.dgt.paribahanpool.purchase_demand.model;

import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
@Table( name = "product" )
public class Product {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column( name = "id")
    private Long id;

    @Column( name = "product_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String productName;

    @Column( name = "product_identity" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String productIdentity;

    @Column( name = "tender_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String tenderNumber;

    @Column( name = "quantity" )
    private Integer quantity;

    @Column( name = "rate_per_unit" )
    private Integer ratePerUnit;

    @Column( name = "total_rate" )
    private Integer totalRate;

    @Column( name = "comments" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String comments;
}
