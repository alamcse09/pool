package com.dgt.paribahanpool.purchase_demand.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class ProductAddRequest {

    private Long id;

    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    @NotBlank( message = "{validation.common.required}" )
    private String productName;

    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    @NotBlank( message = "{validation.common.required}" )
    private String productIdentity;

    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    @NotBlank( message = "{validation.common.required}" )
    private String tenderNumber;

    @NotNull( message = "{validation.common.required}" )
    private Integer quantity;

    @NotNull( message = "{validation.common.required}" )
    private Integer ratePerUnit;

    private Integer totalRate;

    private String comments;
}
