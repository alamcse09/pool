package com.dgt.paribahanpool.purchase_demand.model;

import com.dgt.paribahanpool.enums.ProductType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table( name = "puchase_demand")
@SQLDelete( sql = "UPDATE supply_order set is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class PurchaseDemand extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column( name = "id")
    private Long id;

    @Column( name = "product_type")
    private ProductType productType;

    @Column( name = "date")
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate date;

    @Column( name = "total_price")
    private Integer totalPrice;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinTable(
            name = "purchase_demand_product_map",
            joinColumns = @JoinColumn( name = "purchase_demand_id", foreignKey = @ForeignKey( name = "fk_product_purchase_demand_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "product_id", foreignKey = @ForeignKey( name = "fk_purchase_demand_id_purchase_demand_product_map_id" ) )
    )
    private Set<Product> productSet = new HashSet<>();
}
