package com.dgt.paribahanpool.purchase_demand.model;

import com.dgt.paribahanpool.enums.ProductType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Data
public class PurchaseDemandAddRequest {

    private Long id;

    @NotNull( message = "{validation.common.required}")
    private ProductType productType;

    @NotNull( message = "{validation.common.required}")
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate date;

    private Integer totalPrice;

    @Valid
    List<ProductAddRequest> productAddRequests;
}
