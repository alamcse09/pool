package com.dgt.paribahanpool.purchase_demand.model;

import com.dgt.paribahanpool.enums.ProductType;
import lombok.Data;

import java.time.LocalDate;

@Data
public class PurchaseDemandSearchResponse {

    private Long id;

    private ProductType productType;

    private LocalDate date;

    private Integer totalPrice;
}
