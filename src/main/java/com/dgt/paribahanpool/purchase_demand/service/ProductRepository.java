package com.dgt.paribahanpool.purchase_demand.service;

import com.dgt.paribahanpool.purchase_demand.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
