package com.dgt.paribahanpool.purchase_demand.service;

import com.dgt.paribahanpool.purchase_demand.model.Product;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class ProductService {

    private final ProductRepository productRepository;

    public Optional<Product> findById( Long id ) {

        return productRepository.findById( id );
    }
}
