package com.dgt.paribahanpool.purchase_demand.service;

import com.dgt.paribahanpool.purchase_demand.model.PurchaseDemand;
import com.dgt.paribahanpool.purchase_demand.model.PurchaseDemandAddRequest;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class PurchaseDemandModelService {

    private final PurchaseDemandService purchaseDemandService;
    private final MvcUtil mvcUtil;

    public void addPurchaseDemand( Model model ){

        model.addAttribute("purchaseDemandAddRequest", new PurchaseDemandAddRequest());
    }

    public void addPurchaseDemandPost( PurchaseDemandAddRequest purchaseDemandAddRequest, RedirectAttributes redirectAttributes ) {

        purchaseDemandService.save( purchaseDemandAddRequest );

        mvcUtil.addSuccessMessage( redirectAttributes, "success.purchase.demand.add" );
    }

    public void editPurchaseDemandPost( PurchaseDemandAddRequest purchaseDemandAddRequest, RedirectAttributes redirectAttributes ) {

        purchaseDemandService.save( purchaseDemandAddRequest );

        mvcUtil.addSuccessMessage( redirectAttributes, "success.purchase.demand.edit" );
    }

    @Transactional
    public void editPurchaseDemandGet( Model model, Long id ) {

        Optional<PurchaseDemand> purchaseDemand = purchaseDemandService.findById( id );

        if( purchaseDemand.isPresent() ) {

            model.addAttribute( "purchaseDemandAddRequest", purchaseDemandService.getPurchaseDemandAddRequestFromPurchaseDemand( purchaseDemand.get() ) );
        }
    }
}
