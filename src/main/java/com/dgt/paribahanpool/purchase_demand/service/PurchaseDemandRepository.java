package com.dgt.paribahanpool.purchase_demand.service;

import com.dgt.paribahanpool.purchase_demand.model.PurchaseDemand;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface PurchaseDemandRepository extends DataTablesRepository< PurchaseDemand, Long > {
}
