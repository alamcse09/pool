package com.dgt.paribahanpool.purchase_demand.service;

import com.dgt.paribahanpool.bill.model.Bill;
import com.dgt.paribahanpool.bill.model.BillSearchResponse;
import com.dgt.paribahanpool.purchase_demand.model.*;
import com.dgt.paribahanpool.supply_order.model.SupplyOrder;
import com.dgt.paribahanpool.workflow.model.State;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class PurchaseDemandService {

    private final PurchaseDemandRepository purchaseDemandRepository;
    private final ProductService productService;

    public PurchaseDemand save( PurchaseDemand purchaseDemand ) {

        return purchaseDemandRepository.save( purchaseDemand );
    }

    @Transactional
    public PurchaseDemand save( PurchaseDemandAddRequest purchaseDemandAddRequest ) {

        PurchaseDemand purchaseDemand = new PurchaseDemand();

        if( purchaseDemandAddRequest.getId() != null ) {

            purchaseDemand = findById( purchaseDemandAddRequest.getId() ).get();
        }

        purchaseDemand = getPurchaseDemandFromPurchaseDemandAddRequest( purchaseDemand, purchaseDemandAddRequest );

        return save(purchaseDemand);
    }

    public Optional<PurchaseDemand> findById( Long id ) {

        return purchaseDemandRepository.findById( id );
    }

    private PurchaseDemand getPurchaseDemandFromPurchaseDemandAddRequest(PurchaseDemand purchaseDemand, PurchaseDemandAddRequest purchaseDemandAddRequest ) {

        purchaseDemand.setProductType( purchaseDemandAddRequest.getProductType() );
        purchaseDemand.setDate( purchaseDemandAddRequest.getDate() );
        purchaseDemand.setTotalPrice( purchaseDemandAddRequest.getTotalPrice() );

        if( purchaseDemandAddRequest.getProductAddRequests() != null) {

            Set<Product> productSet = purchaseDemandAddRequest.getProductAddRequests()
                    .stream()
                    .map( productAddRequest -> getProductFromProductAddRequest(productAddRequest) )
                    .filter( product -> product != null )
                    .collect( Collectors.toSet() );

            purchaseDemand.getProductSet().clear();
            purchaseDemand.getProductSet().addAll( productSet );
        }

        return purchaseDemand;
    }

    private Product getProductFromProductAddRequest( ProductAddRequest productAddRequest ) {

        Product product = new Product();

        if( productAddRequest.getId() != null ) {

            Optional<Product> productOptional = productService.findById( productAddRequest.getId() );

            if( productOptional.isPresent() ) {

                product = productOptional.get();
            } else {

                return null;
            }
        }

        product.setProductName( productAddRequest.getProductName() );
        product.setProductIdentity( productAddRequest.getProductIdentity() );
        product.setTenderNumber( productAddRequest.getTenderNumber() );
        product.setQuantity( productAddRequest.getQuantity() );
        product.setRatePerUnit( productAddRequest.getRatePerUnit() );
        product.setTotalRate( productAddRequest.getTotalRate() );
        product.setComments( productAddRequest.getComments() );

        return product;
    }

    private PurchaseDemandSearchResponse getPurchaseDemandSearchResponseFromPurchaseDemand( PurchaseDemand purchaseDemand ) {

        PurchaseDemandSearchResponse purchaseDemandSearchResponse = new PurchaseDemandSearchResponse();
        purchaseDemandSearchResponse.setId( purchaseDemand.getId() );
        purchaseDemandSearchResponse.setProductType( purchaseDemand.getProductType() );
        purchaseDemandSearchResponse.setDate( purchaseDemand.getDate() );
        purchaseDemandSearchResponse.setTotalPrice( purchaseDemand.getTotalPrice() );

        return  purchaseDemandSearchResponse;
    }

    public DataTablesOutput<PurchaseDemandSearchResponse> searchForDatatable(DataTablesInput dataTablesInput) {

        return purchaseDemandRepository.findAll( dataTablesInput, this::getPurchaseDemandSearchResponseFromPurchaseDemand );
    }

    public PurchaseDemandAddRequest getPurchaseDemandAddRequestFromPurchaseDemand( PurchaseDemand purchaseDemand ) {

        PurchaseDemandAddRequest purchaseDemandAddRequest = new PurchaseDemandAddRequest();

        purchaseDemandAddRequest.setId( purchaseDemand.getId() );
        purchaseDemandAddRequest.setProductType( purchaseDemand.getProductType() );
        purchaseDemandAddRequest.setDate( purchaseDemand.getDate() );
        purchaseDemandAddRequest.setTotalPrice( purchaseDemand.getTotalPrice() );

        if(purchaseDemand.getProductSet().size() > 0 ) {

            List<ProductAddRequest> productAddRequestList = purchaseDemand.getProductSet()
                    .stream()
                    .map(product -> getProductAddRequestFromProduct( product ))
                    .collect(Collectors.toList());

            purchaseDemandAddRequest.setProductAddRequests( productAddRequestList );
        }

        return purchaseDemandAddRequest;
    }

    private ProductAddRequest getProductAddRequestFromProduct( Product product ) {

        ProductAddRequest productAddRequest = new ProductAddRequest();

        productAddRequest.setId( product.getId() );
        productAddRequest.setProductName( product.getProductName() );
        productAddRequest.setProductIdentity( product.getProductIdentity() );
        productAddRequest.setTenderNumber( product.getTenderNumber() );
        productAddRequest.setQuantity( product.getQuantity() );
        productAddRequest.setRatePerUnit( product.getRatePerUnit() );
        productAddRequest.setTotalRate( product.getTotalRate() );
        productAddRequest.setComments( product.getComments() );

        return productAddRequest;
    }

    public void delete(Long id) {
        Optional<PurchaseDemand> purchaseDemandToDelete =  findById( id );

        if( purchaseDemandToDelete.isPresent() ) {

            PurchaseDemand purchaseDemand = purchaseDemandToDelete.get();
            purchaseDemand.setIsDeleted( true );
            save( purchaseDemand );
        }
    }

    public boolean existById(Long id) {
        return purchaseDemandRepository.existsById(id);
    }
}
