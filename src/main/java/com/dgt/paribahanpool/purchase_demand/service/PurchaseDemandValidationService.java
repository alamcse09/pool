package com.dgt.paribahanpool.purchase_demand.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.marine.model.MarineAddRequest;
import com.dgt.paribahanpool.purchase_demand.model.PurchaseDemand;
import com.dgt.paribahanpool.purchase_demand.model.PurchaseDemandAddRequest;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class PurchaseDemandValidationService {

    private final MvcUtil mvcUtil;
    private final PurchaseDemandService purchaseDemandService;

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, PurchaseDemandAddRequest purchaseDemandAddRequest ){

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "purchaseDemandAddRequest" )
                        .object( purchaseDemandAddRequest )
                        .title( "title.purchase.demand.add" )
                        .content( "purchase-demand/purchase-demand-add" )
                        .activeMenu( Menu.PURCHASE_DEMAND_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.PURCHASE_DEMAND_ADD_FORM } )
                        .build()
        );
    }

    public boolean editPurchaseDemand( Long id, RedirectAttributes redirectAttributes ) {

        Optional<PurchaseDemand> purchaseDemand = purchaseDemandService.findById( id );

        if( !purchaseDemand.isPresent() ) {

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound");
            return false;
        }

        return true;
    }

    public RestValidationResult delete(Long id) throws NotFoundException {
        if( !purchaseDemandService.existById( id ) ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }
}
