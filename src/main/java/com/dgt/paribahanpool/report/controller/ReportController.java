package com.dgt.paribahanpool.report.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.report.service.ReportService;
import com.dgt.paribahanpool.vehicle.model.VehicleRequisitionReportData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Controller
@RequestMapping( "/report" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class ReportController extends MVCController {

    private final ReportService reportService;

    @GetMapping( "/employee" )
    @TitleAndContent( title = "title.report.employee", content = "report/report-employee", activeMenu = Menu.RERPORT_EMPLOYEE )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Employee Report form" );
        reportService.getEmployeeGroupByGrade( model );
        return viewRoot;
    }

    @GetMapping( "/requisition" )
    @TitleAndContent( title = "title.report.requisition", content = "report/report-requisition", activeMenu = Menu.RERPORT_REQUISITION )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.RERPORT_REQUISITION, FrontEndLibrary.GEOLOCATION_ADD} )
    public String getRequisition(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Employee Report form" );
        reportService.getOwnerType( model );
        return viewRoot;
    }

    @PostMapping( "/requisition" )
    public String getRequisitedVehicles(
            VehicleRequisitionReportData requisitionReportData,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ) throws Exception {

        reportService.getReservedVehicles(model, requisitionReportData );

        return "report/print/report-requisition-print";
    }

    @GetMapping( "/driver" )
    @TitleAndContent( title = "title.report.driver", content = "report/driver-district", activeMenu = Menu.RERPORT_DRIVER )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.RERPORT_DRIVER, FrontEndLibrary.GEOLOCATION_ADD} )
    public String getDistricts(
            HttpServletRequest request,
            Model model
    ){

        reportService.getDistrictList( model );
        return viewRoot;
    }

    @GetMapping( "/driver-report/{districtId}" )
    public String getSupplyOrderPrinted(
            HttpServletRequest request,
            @PathVariable( "districtId" ) Long districtId,
            Model model
    ){

        reportService.getDriverReport( model, districtId );

        return "report/driver-report";
    }
}
