package com.dgt.paribahanpool.report.service;

import com.dgt.paribahanpool.driver.model.DriverSearchResponse;
import com.dgt.paribahanpool.driver.service.DriverService;
import com.dgt.paribahanpool.employee.service.EmployeeService;
import com.dgt.paribahanpool.enums.GeolocationType;
import com.dgt.paribahanpool.enums.Grade;
import com.dgt.paribahanpool.geolocation.model.GeoLocationResponse;
import com.dgt.paribahanpool.geolocation.service.GeoLocationService;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.model.VehicleOwner;
import com.dgt.paribahanpool.vehicle.model.VehicleRequisitionReportData;
import com.dgt.paribahanpool.vehicle.model.VehicleViewResponse;
import com.dgt.paribahanpool.vehicle.service.VehicleOwnerService;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static com.dgt.paribahanpool.vehicle.service.VehicleSpecification.filterByVehicleOwnerId;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class ReportService {

    private final EmployeeService employeeService;
    private final GeoLocationService geoLocationService;
    private final VehicleOwnerService vehicleOwnerService;
    private final VehicleService vehicleService;
    private final DriverService driverService;

    public void getEmployeeGroupByGrade( Model model ) {

        List<Object[]> employeeCountGroupByGrade =  employeeService.getEmployeeCountGroupByGrade();

        List<Grade> gradeListForEmployee = new ArrayList<>();

        List<Long> employeeCountGroupedByGradeList = new ArrayList<>();

        Long totalEmployee = employeeService.getNumberOfEmployeeCount();

//        for( int i = 0 ; i< employeeCountGroupByGrade.size(); i++ ){
//
//            gradeListForEmployee.add( (Grade) employeeCountGroupByGrade.get(i)[1] );
//        }

        Integer index = 0;

        for( Grade grade : Grade.values() ){

            if( index < employeeCountGroupByGrade.size() && grade == employeeCountGroupByGrade.get(index)[1] ) {

                gradeListForEmployee.add( (Grade) employeeCountGroupByGrade.get(index)[1] );

                employeeCountGroupedByGradeList.add((Long) employeeCountGroupByGrade.get(index)[0]);
            }
            else{

                employeeCountGroupedByGradeList.add( 0L );
                gradeListForEmployee.add( grade );
            }
            index += 1;

            if( index == 20 ){

                break;
            }
        }

        model.addAttribute( "gradeListForEmployee", gradeListForEmployee );
        model.addAttribute( "employeeCountGroupedByGradeList", employeeCountGroupedByGradeList );
        model.addAttribute( "totalEmployee", totalEmployee );
    }

    public void getOwnerType(Model model) {

        List<GeoLocationResponse> geoLocationList = geoLocationService.findByGeoLocationType( GeolocationType.DIVISION );
        model.addAttribute( "divisionList", geoLocationList );

        Map<Long, String> ownerTypeMap = new HashMap<>();

        ownerTypeMap.put( 0L, "dropdown.options.owner.type.division" );
        ownerTypeMap.put( 1L, "dropdown.options.owner.type.district" );
        ownerTypeMap.put( 2L, "dropdown.options.owner.type.upazila" );
        ownerTypeMap.put( 3L, "dropdown.options.owner.type.department" );

        model.addAttribute( "ownerTypeMap", ownerTypeMap );
    }

    public void getReservedVehicles(Model model, VehicleRequisitionReportData requisitionReportData) {

        Optional<VehicleOwner> vehicleOwnerOptional = null;

        List<VehicleViewResponse> vehicleViewResponseList = new ArrayList<>();

        if( requisitionReportData.getOwnerTypeValue() < 3L ) {

            vehicleOwnerOptional = vehicleOwnerService.findGeolocationByOwnerTypeValueAndRequisitionReport( requisitionReportData.getOwnerTypeValue(), requisitionReportData);
        }

        if( vehicleOwnerOptional.isPresent() ) {

            Specification<Vehicle> vehicleSpecification = filterByVehicleOwnerId( vehicleOwnerOptional.get().getId() );

            List<Vehicle> vehicleList = vehicleService.findAll( vehicleSpecification );

            vehicleViewResponseList = vehicleList.stream().map(
                    vehicle -> {
                        return VehicleViewResponse.builder()
                                .engineNo( vehicle.getEngineNo() )
                                .vehicleType( vehicle.getVehicleType() )
                                .registrationNo(vehicle.getRegistrationNo() )
                                .build();
                    }
            )
                    .collect(Collectors.toList());
        }

        String ownerTypeString = "";

        if( requisitionReportData.getOwnerTypeValue() == 0L ){

            ownerTypeString = "বিভাগ";
        }
        if( requisitionReportData.getOwnerTypeValue() == 1L ){

            ownerTypeString = "জেলা";
        }
        if( requisitionReportData.getOwnerTypeValue() == 2L ){

            ownerTypeString = "উপজেলা";
        }

        model.addAttribute( "vehicleViewResponseList", vehicleViewResponseList );
        model.addAttribute( "ownerTypeString", ownerTypeString );
    }

    public void getDistrictList(Model model) {
        List<GeoLocationResponse> districtList = geoLocationService.findByGeoLocationType( GeolocationType.DISTRICT );
        Long districtId = null;
        model.addAttribute( "districtList", districtList );
        model.addAttribute( "districtId", districtId );
    }

    public void getDriverReport(Model model, Long districtId){
        List<DriverSearchResponse> driverSearchResponses = driverService.getDriverByDistrictId(districtId);
        LocalDate date = LocalDate.now();
        String districtName = geoLocationService.findById(districtId).get().getNameBn();
        model.addAttribute("driverSearchResponses",driverSearchResponses);
        model.addAttribute("date",date);
        model.addAttribute("districtName",districtName);
    }
}
