package com.dgt.paribahanpool.rest;

public enum ErrorCode {

    WRONG_CREDENTIAL(0);

    private Integer value;

    ErrorCode( Integer value ){

        this.value = value;
    }
}
