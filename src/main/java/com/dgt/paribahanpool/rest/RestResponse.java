package com.dgt.paribahanpool.rest;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class RestResponse<T> {

    private Boolean success;
    private ErrorCode code;
    private String message;
    private LocalDateTime time;
    private T payload;
}
