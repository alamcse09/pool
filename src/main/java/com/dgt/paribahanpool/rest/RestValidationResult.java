package com.dgt.paribahanpool.rest;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RestValidationResult {

    private Boolean success;
    private ErrorCode code;
    private String message;

    public static RestValidationResult valid() {

        return RestValidationResult.builder().success( true ).build();
    }
}
