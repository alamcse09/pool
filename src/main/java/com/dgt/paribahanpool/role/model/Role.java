package com.dgt.paribahanpool.role.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "role" )
public class Role {

    @Id
    @Column( name = "id" )
    private Long id;

    @Column( name = "name" )
    private String name;

    @Column( name = "role_group_en" )
    private String roleGroupEn;

    @Column( name = "role_group_bn" )
    private String roleGroupBn;

    @Column( name = "role_text_en" )
    private String roleTextEn;

    @Column( name = "role_text_bn" )
    private String roleTextBn;
}
