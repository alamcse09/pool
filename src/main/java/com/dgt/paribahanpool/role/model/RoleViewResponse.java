package com.dgt.paribahanpool.role.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RoleViewResponse {

    private Long id;
    private String name;
    private String roleGroupEn;
    private String roleGroupBn;
    private String roleTextEn;
    private String roleTextBn;
}
