package com.dgt.paribahanpool.role.service;

import com.dgt.paribahanpool.role.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Set<Role> findByIdIn( Set<Long> ids );
}
