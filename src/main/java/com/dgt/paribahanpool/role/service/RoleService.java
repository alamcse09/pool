package com.dgt.paribahanpool.role.service;

import com.dgt.paribahanpool.role.model.Role;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class RoleService {

    private final RoleRepository roleRepository;

    public Optional<Role> findById(Long id ){

        return roleRepository.findById( id );
    }

    public Role findReferenceById( Long id ){

        return roleRepository.getById( id );
    }

    public Set<Role> findByIdIn( Set<Long> ids ){

        return roleRepository.findByIdIn( ids );
    }

    public Set<Role> getRoleSetFromRoleIdArr( Long[] roleIds ){

        return Arrays.asList( roleIds ).stream()
                .map( this::findReferenceById )
                .collect( Collectors.toSet() );
    }

    public Set<Role> getRoles( Set<Long> ids ){

        return findByIdIn( ids );
    }

    public Long[] getRoleIdArrFromRoleSet( Set<Role> roleSet ){

        return roleSet
                .stream()
                .map( Role::getId )
                .toArray( Long[]::new );
    }
}
