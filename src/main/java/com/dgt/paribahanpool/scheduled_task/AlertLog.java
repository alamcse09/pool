package com.dgt.paribahanpool.scheduled_task;

import com.dgt.paribahanpool.enums.AlertType;
import com.dgt.paribahanpool.enums.EntityType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table( name = "alert_log" )
public class AlertLog {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "alert_type" )
    private AlertType alertType;

    @Column( name = "entity_type" )
    private EntityType entityType;

    @Column( name = "entity_id" )
    private Long entityId;

    @Column( name = "date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate date;

    @Column( name = "is_alerted" )
    private Boolean isAlerted;
}
