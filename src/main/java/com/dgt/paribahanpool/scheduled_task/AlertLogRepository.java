package com.dgt.paribahanpool.scheduled_task;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlertLogRepository extends DataTablesRepository<AlertLog, Long>, JpaRepository<AlertLog, Long> {
}
