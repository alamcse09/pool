package com.dgt.paribahanpool.scheduled_task;

import com.dgt.paribahanpool.enums.AlertType;
import com.dgt.paribahanpool.enums.EntityType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class AlertLogService {

    private final AlertLogRepository alertLogRepository;

    public void save( AlertLog alertLog ){

        alertLogRepository.save( alertLog );
    }

    @Transactional
    public void saveAlertLog( AlertType alertType, LocalDate date, Long entityId, EntityType entityType, Boolean isAlerted ) {

        AlertLog alertLog = new AlertLog();

        alertLog.setAlertType( alertType );
        alertLog.setDate( date );
        alertLog.setEntityId( entityId );
        alertLog.setEntityType( entityType );
        alertLog.setIsAlerted( isAlerted );

        save( alertLog );
    }
}
