package com.dgt.paribahanpool.scheduled_task;

import com.dgt.paribahanpool.enums.AlertType;
import com.dgt.paribahanpool.enums.EntityType;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;

public class AlertLogSpecification {

    public static Specification<AlertLog> filterByAlertType( AlertType alertType ){

        return ((root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( AlertLog_.ALERT_TYPE ), alertType ) );
    }

    public static Specification<AlertLog> filterByDate( LocalDate date ){

        return ((root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( AlertLog_.DATE ), date ) );
    }

    public static Specification<AlertLog> filterIsAlerted( Boolean isAlerted ){

        return ((root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( AlertLog_.IS_ALERTED ), isAlerted ) );
    }

    public static Specification<AlertLog> filterByEntityType( EntityType entityType ){

        return ((root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( AlertLog_.ENTITY_TYPE ), entityType ) );
    }
}
