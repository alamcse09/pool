package com.dgt.paribahanpool.scheduled_task;

import com.dgt.paribahanpool.config.AppProperties;
import com.dgt.paribahanpool.enums.AlertType;
import com.dgt.paribahanpool.enums.EntityType;
import com.dgt.paribahanpool.exceptions.EmailDataBadRequestException;
import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import com.dgt.paribahanpool.inventory_item.model.InventoryMetaData;
import com.dgt.paribahanpool.inventory_item.service.InventoryItemService;
import com.dgt.paribahanpool.notification.model.EmailData;
import com.dgt.paribahanpool.notification.model.NotificationSender;
import com.dgt.paribahanpool.notification.model.SmsData;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class AlertSender {

    private final AlertLogService alertLogService;
    private final NotificationSender notificationSender;
    private final VehicleService vehicleService;
    private final AppProperties appProperties;
    private final InventoryItemService inventoryItemService;

    @Transactional
    @Scheduled( fixedDelay = 14400000L )
    public void vehicleFitnessDateExpiryAlert() throws EmailDataBadRequestException {

        if( appProperties.getAlertReciever() == null || appProperties.getAlertReciever().getEmail() == null ){

            log.warn( "Application alert receiver email and phone no not set in properties file" );
        }
        else {
            LocalDate currentDate = LocalDate.now();
            LocalDate fitnessTokenExpiryDate = currentDate.plusMonths(2);

            List<Vehicle> vehicleAlertList = vehicleService.findVehicleForFitnessAlert(fitnessTokenExpiryDate);

            for (Vehicle vehicle : vehicleAlertList) {

                Long vehicleId = vehicle.getId();
                String registrationNo = vehicle.getRegistrationNo();
                String chassisNo = vehicle.getChassisNo();
                String engineNo = vehicle.getEngineNo();
                LocalDate fitnessDate = vehicle.getFitnessDateEnd();

                Map<String, Object> params = new HashMap<>();
                params.put("registrationNo", registrationNo);
                params.put("chassisNo", chassisNo);
                params.put("engineNo", engineNo);
                params.put("fitnessDate", fitnessDate);

                EmailData emailData = EmailData.builder()
                        .to(new String[]{appProperties.getAlertReciever().getEmail()})
                        .isHtml(true)
                        .templatePath("email/alert/fitness-expiry-alert")
                        .params(params)
                        .subject("Fitness Date Expiry Alert - Paribahan Pool")
                        .build();

                SmsData smsData = SmsData.builder()
                        .phone(appProperties.getAlertReciever().getPhoneNumber())
                        .text("Fitness Expiry Date - Paribahan Pool")
                        .build();

                notificationSender.sendEmail(emailData);
                notificationSender.sendSms(smsData);

                alertLogService.saveAlertLog(AlertType.FITNESS, fitnessTokenExpiryDate, vehicleId, EntityType.VEHICLE, true);
            }

            log.debug("Fitness Alert Sent");
        }
    }

    @Transactional
    @Scheduled( fixedDelay = 14400000L )
    public void vehicleTaxTokenDateExpiryAlert() throws EmailDataBadRequestException {

        if( appProperties.getAlertReciever() == null || appProperties.getAlertReciever().getEmail() == null ){

            log.warn( "Application alert receiver email and phone no not set in properties file" );
        }
        else {
            LocalDate currentDate = LocalDate.now();
            LocalDate taxTokenExpiryDate = currentDate.plusMonths(2);

            List<Vehicle> vehicleAlertList = vehicleService.findVehicleForTaxTokenAlert(taxTokenExpiryDate);

            for (Vehicle vehicle : vehicleAlertList) {

                Long vehicleId = vehicle.getId();
                String registrationNo = vehicle.getRegistrationNo();
                String chassisNo = vehicle.getChassisNo();
                String engineNo = vehicle.getEngineNo();
                LocalDate taxTokenDate = vehicle.getTaxTokenDate();

                Map<String, Object> params = new HashMap<>();
                params.put("registrationNo", registrationNo);
                params.put("chassisNo", chassisNo);
                params.put("engineNo", engineNo);
                params.put("taxTokenDate", taxTokenDate);

                EmailData emailData = EmailData.builder()
                        .to(new String[]{appProperties.getAlertReciever().getEmail()})
                        .isHtml(true)
                        .templatePath("email/alert/tax-token-expiry-alert")
                        .params(params)
                        .subject("Tax Token Date Expiry Alert - Paribahan Pool")
                        .build();

                SmsData smsData = SmsData.builder()
                        .phone(appProperties.getAlertReciever().getPhoneNumber())
                        .text("Tax Token Date Expiry Alert - Paribahan Pool")
                        .build();

                notificationSender.sendEmail(emailData);
                notificationSender.sendSms(smsData);

                alertLogService.saveAlertLog(AlertType.TAX_TOKEN, taxTokenExpiryDate, vehicleId, EntityType.VEHICLE, true);
            }

            log.debug("Tax Token Alert Sent");
        }
    }

    @Transactional
    @Scheduled( fixedDelay = 14400000L )
    public void warrantyExpiryAlert() throws EmailDataBadRequestException {

        if( appProperties.getAlertReciever() == null || appProperties.getAlertReciever().getEmail() == null ){

            log.warn( "Application alert receiver email and phone no not set in properties file" );
        }
        else{

            LocalDate currentDate = LocalDate.now();

            List<InventoryItem> inventoryItemFromBuyDateList = inventoryItemService.getAllInventoryWithWarrantyFromBuyDate();

            List<InventoryMetaData> inventoryItemFromUseDateList = inventoryItemService.getAllInventoryWithWarrantyFromUseDate();


            List<List<String>> paramList = new ArrayList<>();


            for( InventoryItem inventoryItem : inventoryItemFromBuyDateList ){

                LocalDate expiryDate = inventoryItem.getPurchaseDate().plusDays( inventoryItem.getWarrantyPeriod() );

                if( currentDate.isAfter( expiryDate.minusDays( 16 ) )  ){

                    Long itemId = inventoryItem.getId();
                    String itemName = inventoryItem.getProductNameBn();
                    String vendorName = inventoryItem.getVendor().getVendorName();
                    Integer warrantyPeriod = inventoryItem.getWarrantyPeriod();

                    List<String> params = new ArrayList<>();

                    params.add( itemId.toString() );
                    params.add( itemName );
                    params.add( vendorName );
                    params.add( warrantyPeriod.toString() );
                    params.add( inventoryItem.getPurchaseDate().toString() );

                    paramList.add( params );

                }
            }

            for( InventoryMetaData inventoryMetaData : inventoryItemFromUseDateList ){

                LocalDate expiryDate = inventoryMetaData.getUseDate().plusDays( inventoryMetaData.getWarrantyPeriod() );

                if( currentDate.isAfter( expiryDate.minusDays( 16 ) )  ){

                    Long itemId = inventoryMetaData.getId();
                    String itemName = inventoryMetaData.getProductNameBn();
                    String vendorName = inventoryMetaData.getVendorNameBn();
                    Integer warrantyPeriod = inventoryMetaData.getWarrantyPeriod();

                    List<String> params = new ArrayList<>();

                    params.add( itemId.toString() );
                    params.add( itemName );
                    params.add( vendorName );
                    params.add( warrantyPeriod.toString() );
                    params.add( inventoryMetaData.getUseDate().toString() );

                    paramList.add( params );

                }
            }

            for( List<String> params : paramList ){

                LocalDate startDate = LocalDate.parse( params.get(4) );
                Integer warrantyPeriod = Integer.parseInt( params.get( 3 ) );
                LocalDate expiryDate = startDate.plusDays( warrantyPeriod );

                alertLogService.saveAlertLog(AlertType.WARRATANTY_EXPIRY, expiryDate.minusDays(16), Long.parseLong(params.get(0)), EntityType.WARRANTY, true);
            }

            if( paramList.size() > 0 ) {

                EmailData emailData = EmailData.builder()
                        .to(new String[]{appProperties.getAlertReciever().getEmail()})
                        .isHtml(true)
                        .templatePath("email/alert/warranty-expiry-alert")
                        .paramList(paramList)
                        .subject("Warranty Date Expiry Alert - Paribahan Pool")
                        .build();

                SmsData smsData = SmsData.builder()
                        .phone(appProperties.getAlertReciever().getPhoneNumber())
                        .text("Warranty Date Expiry Alert - Paribahan Pool")
                        .build();

                notificationSender.sendEmail(emailData);
                notificationSender.sendSms(smsData);

            }

        }
    }
}
