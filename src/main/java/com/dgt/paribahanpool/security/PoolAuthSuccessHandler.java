package com.dgt.paribahanpool.security;

import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.log.service.AuditLogService;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class PoolAuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private final AuditLogService auditLogService;
    private final MvcUtil mvcUtil;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {

        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        User user = userPrincipal.getUser();
        auditLogService.save( user.getId(), LogEvent.USER_LOGGED_ID, mvcUtil.getClientIP( request ), user.getId(), "" );

        super.onAuthenticationSuccess( request, response, authentication );
    }
}
