package com.dgt.paribahanpool.security;

import com.dgt.paribahanpool.config.AppProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import static org.springframework.http.HttpMethod.*;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final AuthenticationFailureHandler authenticationFailureHandler;
    private final AppProperties appProperties;
    private final PoolAuthSuccessHandler poolAuthSuccessHandler;
    private final UserDetailsService userDetailsService;

    @Bean
    public PasswordEncoder passwordEncoder(){

        return new BCryptPasswordEncoder();
    }

    AntPathRequestMatcher[] disableCsrf = {
            new AntPathRequestMatcher( "/login"),
            new AntPathRequestMatcher( "/forgot-password"),
            new AntPathRequestMatcher( "/reset-password")
    };

    @Override
    protected void configure( HttpSecurity http ) throws Exception {

        http
                .cors()
                .and()

                .csrf()
                .ignoringRequestMatchers( disableCsrf )
                .and()

                .authorizeRequests()

                .antMatchers( "/login**", "/forgot-password", "/reset-password/**", "/signup", "/language/*" ).permitAll()

                .antMatchers( "/" ).authenticated()
                .antMatchers( "/user/my-profile" ).authenticated()
                .antMatchers( "/user/password" ).authenticated()
                .antMatchers( "/log/search", "/api/log/search" ).authenticated()
                .antMatchers( "/actuator/**" ).hasAnyAuthority( "ROLE_ADMIN" )
                .antMatchers( "/swagger-ui/**" ).hasAnyAuthority( "ROLE_ADMIN" )
                .antMatchers( "/user/search", "/api/user/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_USER_SEARCH" )
                .antMatchers( "/log/search/*", "/api/log/search/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_SEE_OTHER_USER_LOG" )
                .antMatchers( "/vehicle/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_ADD" )
                .antMatchers( "/vehicle/edit/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_EDIT" )
                .antMatchers( "/vehicle/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_SEARCH" )
                .antMatchers( "/vehicle/auction" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_AUCTION" )
                .antMatchers( "/vehicle/requisition" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_REQUISITION" )
                .antMatchers( "/vehicle/requisition/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_REQUISITION_SEARCH" )
                .antMatchers( "/vehicle/my-requisition/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_MY_REQUISITION_SEARCH" )
                .antMatchers( "/vehicle/requisition/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_REQUISITION_VIEW" )

                .antMatchers( "/vehicle-service/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_SERVICE_ADD" )
                .antMatchers( "/vehicle-service/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_SERVICE_SEARCH" )
                .antMatchers( "/vehicle-service/my-search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_SERVICE_MY_SEARCH" )
                .antMatchers( "/vehicle-service/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_SERVICE_VIEW" )
                .antMatchers( "/vehicle-service/take-action" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_SERVICE_TAKE_ACTION" )
                .antMatchers( "/vehicle-service/add-more-product" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_SERVICE_ADD_MORE_PRODUCT" )

                .antMatchers( "/marine/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MARINE_ADD" )
                .antMatchers( "/marine/edit/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MARINE_EDIT" )
                .antMatchers( "/marine/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MARINE_SEARCH" )
                .antMatchers( "/marine-service-application/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MARINE_SERVICE_APPLICATION_ADD" )
                .antMatchers( "/marine-service-application/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MARINE_SERVICE_APPLICATION_SEARCH" )
                .antMatchers( "/marine-service-application/edit/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MARINE_SERVICE_APPLICATION_EDIT" )
                .antMatchers( DELETE, "/api/marine-service-application/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MARINE_SERVICE_APPLICATION_DELETE" )
                .antMatchers( GET, "/marine-service-application/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MARINE_SERVICE_APPLICATION_VIEW" )
                .antMatchers( "/noc/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_NOC_ADD" )
                .antMatchers( "/noc/edit/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_NOC_EDIT" )
                .antMatchers( "/noc/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_NOC_SEARCH" )
                .antMatchers( "/noc/my-search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_NOC_MY_SEARCH" )
                .antMatchers( "/noc/*" ).hasAnyAuthority( "ROLE_ADMIN", "NOC_PLR_VIEW", "NOC_ENCASHMENT_VIEW" )
                .antMatchers( "/noc/create/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_NOC_CREATE" )
                .antMatchers( "/mechanic/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MECHANIC_ADD" )
                .antMatchers( "/mechanic/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MECHANIC_SEARCH" )
                .antMatchers( "/mechanic/edit/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MECHANIC_EDIT" )
                .antMatchers( "/project-vehicle/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_PROJECT_VEHICLE_ADD" )
                .antMatchers( "/project-vehicle/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_PROJECT_VEHICLE_SEARCH" )
                .antMatchers( DELETE, "/api/project-vehicle/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_PROJECT_VEHICLE_DELETE" )
                .antMatchers( "/project-vehicle/edit/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_PROJECT_VEHICLE_EDIT" )
                .antMatchers( "/vendor/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VENDOR_ADD" )
                .antMatchers( "/vendor/edit/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VENDOR_EDIT" )
                .antMatchers( "/vendor/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VENDOR_SEARCH" )
                .antMatchers("/purchase-demand/search").hasAnyAuthority("ROLE_ADMIN","ROLE_PURCHASE_DEMAND_SEARCH")
                .antMatchers( "/purchase-demand/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_PURCHASE_DEMAND_ADD" )
                .antMatchers( "/purchase-demand/edit/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_PURCHASE_DEMAND_EDIT" )
                .antMatchers( "/driver/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_DRIVER_ADD" )
                .antMatchers( "/driver/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_DRIVER_SEARCH" )
                .antMatchers( "/driver/edit/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_DRIVER_EDIT" )
                .antMatchers( "/incident/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_INCIDENT_ADD" )
                .antMatchers( "/incident/edit/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_INCIDENT_EDIT" )
                .antMatchers( "/incident/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_INCIDENT_SEARCH" )
                .antMatchers( DELETE, "/api/incident/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_INCIDENT_DELETE" )
                .antMatchers( "/incident/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_INCIDENT_VIEW" )
                .antMatchers( "/employee/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_EMPLOYEE_ADD" )
                .antMatchers( "/employee/edit/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_EMPLOYEE_EDIT" )
                .antMatchers( "/employee/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_EMPLOYEE_SEARCH" )
                .antMatchers( "/driver-assign/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_DRIVER_ASSIGN_ADD" )
                .antMatchers( "/driver-assign/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_DRIVER_ASSIGN_SEARCH" )
                .antMatchers( "/driver/add-driver-requisition" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_DRIVER_REQUISITION" )
                .antMatchers( "/driver/driver-requisition/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_DRIVER_REQUISITION_SEARCH" )
                .antMatchers( "/driver/driver-requisition/view/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_DRIVER_REQUISITION_VIEW" )
                .antMatchers( "/user-level/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_USER_LEVEL_ADD" )
                .antMatchers( "/user-level/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_USER_LEVEL_SEARCH" )
                .antMatchers( "/user-level/edit/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_USER_LEVEL_EDIT" )
                .antMatchers( "/replace-application/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_REPLACE_APPLICATION_VIEW" )
                .antMatchers( "/replace-application/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_REPLACE_APPLICATION_ADD" )
                .antMatchers( "/replace-application/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_REPLACE_APPLICATION_SEARCH" )
                .antMatchers( "/replace-application/replace/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_REPLACE_ADD" )
                .antMatchers("/new/requisition/*").hasAnyAuthority("ROLE_ADMIN","ROLE_NEW_VEHICLE_REQUISITION")
                .antMatchers( "/designation/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_DESIGNATION_ADD" )
                .antMatchers( "/designation/edit/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_DESIGNATION_EDIT" )
                .antMatchers( "/designation/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_DESIGNATION_SEARCH" )
                .antMatchers( "/leave-application/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_LEAVE_APPLICATION_ADD" )
                .antMatchers( "/inventory/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_INVENTORY_ADD" )
                .antMatchers( "/inventory/edit/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_INVENTORY_EDIT" )
                .antMatchers( "/inventory/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_INVENTORY_SEARCH" )
                .antMatchers( "/inventory/report-search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_INVENTORY_REPORT" )
                .antMatchers( DELETE, "/api/inventory/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_INVENTORY_DELETE" )
                .antMatchers( "/leave-application/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_LEAVE_APPLICATION_SEARCH" )
                .antMatchers( "/leave-application/leave-approval-search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_LEAVE_APPLICATION_APPROVAL_SEARCH" )
                .antMatchers( DELETE, "/api/marine/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MARINE_DELETE" )
                .antMatchers( DELETE, "/api/driver/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_DRIVER_DELETE" )
                .antMatchers( DELETE, "/api/noc/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_NOC_DELETE" )
                .antMatchers( DELETE, "/api/leave-application/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_LEAVE_DELETE" )

                .antMatchers( "/replace-application/my-search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_MY_REPLACE_SEARCH" )
                .antMatchers( DELETE, "/api/replace-application/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_VEHICLE_MY_REPLACE_DELETE" )
                .antMatchers( "/meeting/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MEETING_ADD" )
                .antMatchers( "/meeting/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MEETING_SEARCH" )
                .antMatchers( DELETE, "/api/meeting/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MEETING_DELETE" )
                .antMatchers( "/meeting/meeting-minutes" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MEETING_MINUTES_UPDATE" )
                .antMatchers( "/meeting/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MEETING_VIEW" )
                .antMatchers( "/budget/code-search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_BUDGET_CODE_SEARCH" )
                .antMatchers( "/budget/code-add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_BUDGET_CODE_ADD" )
                .antMatchers( "/budget/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_BUDGET_ADD" )
                .antMatchers( "/budget/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_BUDGET_SEARCH" )
                .antMatchers( DELETE, "/api/budget/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_BUDGET_DELETE" )
                .antMatchers( "/budget/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_BUDGET_VIEW" )
                .antMatchers( "/bill/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_BILL_ADD" )
                .antMatchers( "/bill/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_BILL_SEARCH" )
                .antMatchers( "/complains/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_COMPLAIN_ADD" )
                .antMatchers( "/complains/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_COMPLAIN_SEARCH" )
                .antMatchers( DELETE, "/api/complains/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_COMPLAIN_DELETE" )
                .antMatchers( "/complains/my-search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_COMPLAIN_MY_SEARCH" )
                .antMatchers( "/training/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_TRAINING_ADD" )
                .antMatchers( "/training/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_TRAINING_SEARCH" )
                .antMatchers( DELETE, "/api/training/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_TRAINING_DELETE" )
                .antMatchers( "/training/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_TRAINING_ATTENDANCE_SEARCH" )
                .antMatchers( "/training-attendance/*/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_TRAINING_ATTENDANCE_ADD" )
                .antMatchers( "/training/attendee/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_TRAINING_ATTENDEE_ADD" )
                .antMatchers( "/training/attendee/search/*/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_TRAINING_ATTENDEE_SEARCH" )
                .antMatchers( DELETE, "/api/training/attendee/trainer/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_TRAINING_TRAINER_DELETE" )
                .antMatchers( DELETE, "/api/training/attendee/trainee/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_TRAINING_TRAINEE_DELETE" )
                .antMatchers( DELETE, "/api/training/attendee/course-director/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_TRAINING_COURSE_DIRECTOR_DELETE" )
                .antMatchers( "/training/evaluation/search/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_TRAINING_EVALUATION" )
                .antMatchers("/training/*").hasAnyAuthority("ROLE_ADMIN","ROLE_TRAINING_EVALUATION")
                .antMatchers("/training/report/*").hasAnyAuthority("ROLE_ADMIN","ROLE_TRAINING_REPORT")
                .antMatchers("/training-expense/add").hasAnyAuthority("ROLE_ADMIN","ROLE_TRAINING_EXPENSE_MANAGEMENT")
                .antMatchers("/warranty/add").hasAnyAuthority("ROLE_ADMIN","ROLE_WARRANTY_CLAIM_MANAGEMENT")
                .antMatchers( "/warranty/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_WARRANTY_CLAIM_SEARCH" )
                .antMatchers( DELETE, "/api/warranty/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_WARRANTY_CLAIM_DELETE" )
                .antMatchers( "/warranty/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_WARRANTY_CLAIM_VIEW" )
                .antMatchers( "/training-expense/search/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_TRAINING_EXPENSE_SEARCH" )
                .antMatchers( DELETE, "/training-expense/delete/*/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_TRAINING_EXPENSE_DELETE" )
                .antMatchers("/report/employee").hasAnyAuthority("ROLE_ADMIN","ROLE_REPORT_EMPLOYEE")
                .antMatchers( "/marine-service-disposition-application/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MARINE_DISPOSITION_SEARCH" )
                .antMatchers( "/marine-service-disposition-application/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_MARINE_DISPOSITION_VIEW" )
                .antMatchers( "/parts-disposition/search" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_PARTS_DISPOSITION_SEARCH" )
                .antMatchers( "/parts-disposition/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_PARTS_DISPOSITION_VIEW" )
                .antMatchers("/supply-order/search").hasAnyAuthority("ROLE_ADMIN","ROLE_SUPPLY_ORDER_SEARCH")
                .antMatchers("/supply-order/add").hasAnyAuthority("ROLE_ADMIN","ROLE_SUPPLY_ORDER_ADD")
                .antMatchers("/vehicle/auction/*").hasAnyAuthority("ROLE_ADMIN","ROLE_VEHICLE_AUCTION_VIEW")
                .antMatchers("/report/requisition").hasAnyAuthority("ROLE_ADMIN","ROLE_REPORT_REQUISITION")
                .antMatchers("/report/driver").hasAnyAuthority("ROLE_ADMIN","ROLE_REPORT_DRIVER")

                .antMatchers( "/setting/add" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_SETTING_ADD" )
                .antMatchers( "/api/setting/get/*" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_SETTING_ADD" )
                .antMatchers( POST, "/api/setting/set" ).hasAnyAuthority( "ROLE_ADMIN", "ROLE_SETTING_ADD" )

                .and()

                .formLogin()
                .loginPage( "/login" )
                .defaultSuccessUrl( "/" )
                .successHandler( poolAuthSuccessHandler )
                .failureHandler( authenticationFailureHandler )
                .and()

                .logout()
                .logoutRequestMatcher( new AntPathRequestMatcher( "/logout" ) )
                .logoutSuccessUrl( "/login" );

        /*http
            .sessionManagement()
            .maximumSessions( appProperties.getSession().getMaxSession() );*/

        http
                .headers()
                .xssProtection();

        http
                .rememberMe()
                .key( appProperties.getRememberMe().getToken() )
                .tokenValiditySeconds( appProperties.getRememberMe().getValiditySeconds() )
                .userDetailsService( userDetailsService );
    }
}
