package com.dgt.paribahanpool.security.controller;

import com.dgt.paribahanpool.security.service.LoginModelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Controller
@RequiredArgsConstructor( onConstructor_ = { @Autowired } )
public class LoginController {

    private final LoginModelService loginModelService;

    @GetMapping( "/login" )
    public String login(
            @RequestParam( value = "error", required = false ) Boolean error,
            @RequestParam( value = "msg", required = false ) String message,
            Model model
    ){

        loginModelService.setFailedLoginData( model, error, message );
        return "security/login";
    }
}
