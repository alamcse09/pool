package com.dgt.paribahanpool.security.model;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.employee.model.Employee;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.mechanic.model.Mechanic;
import com.dgt.paribahanpool.user.model.PublicUserInfo;
import com.dgt.paribahanpool.user.model.User;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public class UserPrincipal extends org.springframework.security.core.userdetails.User {

    private Mechanic mechanic;
    private User user;
    private PublicUserInfo publicUserInfo;
    private Employee employee;
    private Set<Long> roleIds;
    private String name = "";
    private String nameEn = "";
    private Long ppDocId;

    public UserPrincipal( User user, Collection<? extends GrantedAuthority> authorities ) {

        super( user.getUsername(), user.getPassword(), user.getIsEnabled(), true, true, true, authorities );

        this.user = user;
        this.publicUserInfo = user.getPublicUserInfo();
        this.employee = user.getEmployee();
        this.mechanic = user.getMechanic();
        this.ppDocId = user.getProfilePicDocGroupId();

        if( user.getUserLevel() != null && user.getUserLevel().getRoleSet() != null ) {
            this.roleIds = user.getUserLevel().getRoleSet().stream()
                    .map( role -> role.getId() )
                    .collect(Collectors.toSet());
        }

        if( user.getUserType() == null || user.getUserType().equals( UserType.SYSTEM_USER ) ){

            if( user.getEmployee() != null ){
                this.name = user.getEmployee().getName();
                this.nameEn = user.getEmployee().getNameEn();
            }
        }
        else if( user.getUserType() == null || user.getUserType().equals( UserType.PUBLIC ) ){

            if( user.getPublicUserInfo() != null ){

                this.name = user.getPublicUserInfo().getName();
                this.nameEn = user.getPublicUserInfo().getNameEn();
            }
        }
        else if( user.getUserType() == null || user.getUserType().equals( UserType.MECHANIC ) ){

            if( user.getMechanic() != null ){

                this.name = user.getMechanic().getName();
                this.nameEn = user.getMechanic().getName();
            }
        }
    }

    public String getName(){

        if( user.getUserType() == UserType.SYSTEM_USER ){
            return employee.getName();
        }
        else if( user.getUserType() == UserType.PUBLIC ){
            return publicUserInfo.getName();
        }
        else if( user.getUserType() == UserType.MECHANIC ){
            return mechanic.getName();
        }

        return "";
    }

    public String getNameEn(){

        if( user.getUserType() == UserType.SYSTEM_USER ){
            return employee.getNameEn();
        }
        else if( user.getUserType() == UserType.PUBLIC ){
            return publicUserInfo.getNameEn();
        }
        else if( user.getUserType() == UserType.MECHANIC ){
            return mechanic.getName();
        }

        return "";
    }

    public String getDesignation(){

        if( user.getUserType() != null && user.getUserType() == UserType.SYSTEM_USER ){
            return employee.getDesignation() == null? "": employee.getDesignation().getNameBn();
        }
        else if( user.getUserType() != null && user.getUserType() == UserType.PUBLIC ){
            return publicUserInfo.getDesignation();
        }
        else if( user.getUserType() != null && user.getUserType() == UserType.MECHANIC ){

            return AppConstants.MECHANIC_DESIGNATION_BN;
        }

        return "";
    }

    public String getDesignationEn(){

        if( user.getUserType() != null && user.getUserType() == UserType.SYSTEM_USER ){
            return employee.getDesignation() == null? "": employee.getDesignation().getNameEn();
        }
        else if( user.getUserType() != null && user.getUserType() == UserType.PUBLIC ){
            return publicUserInfo.getDesignationEn();
        }
        else if( user.getUserType() != null && user.getUserType() == UserType.MECHANIC ) {

            return AppConstants.MECHANIC_DESIGNATION_EN;
        }

        return "";
    }

    public String getEmail(){

        if( user.getUserType() == UserType.SYSTEM_USER ){
            return employee.getEmail();
        }
        else if( user.getUserType() == UserType.SYSTEM_USER ){
            return publicUserInfo.getEmail();
        }
        else if( user.getUserType() == UserType.MECHANIC ){

            return user.getUsername();
        }

        return "";
    }

    public Long getPpDocId() {
        return ppDocId;
    }

    public void setPpDocId(Long ppDocId) {
        this.ppDocId = ppDocId;
    }

    public Mechanic getMechanic() {
        return mechanic;
    }

    public void setMechanic(Mechanic mechanic) {
        this.mechanic = mechanic;
    }

    public User getUser() {
        return user;
    }

    public PublicUserInfo getPublicUserInfo() {
        return publicUserInfo;
    }

    public void setPublicUserInfo(PublicUserInfo publicUserInfo) {
        this.publicUserInfo = publicUserInfo;
    }

    public Employee getEmployee(){

        return this.employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Long> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(Set<Long> roleIds) {
        this.roleIds = roleIds;
    }
}
