package com.dgt.paribahanpool.security.service;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
public class LoginModelService {

    public void setFailedLoginData(Model model, Boolean error, String message ){

        if( error != null && error == true ){

            model.addAttribute( "msg", message );
        }
    }
}
