package com.dgt.paribahanpool.setting.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.setting.service.SettingModelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;


@Slf4j
@Controller
@RequestMapping("/setting" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class SettingController extends MVCController {

	private final SettingModelService settingModelService;

	@GetMapping( "/add" )
	@TitleAndContent( title = "title.setting.add", content = "setting/setting-add", activeMenu = Menu.SETTING_ADD )
	@AddFrontEndLibrary( libraries = { FrontEndLibrary.SETTING_ADD })
	public String add(
			HttpServletRequest request,
			Model model
	){

		settingModelService.getAddPage( model );
		return viewRoot;
	}
	
}
