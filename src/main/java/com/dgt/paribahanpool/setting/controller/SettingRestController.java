package com.dgt.paribahanpool.setting.controller;


import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.enums.SettingType;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.setting.model.SettingResponse;
import com.dgt.paribahanpool.setting.service.SettingModelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/setting")
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class SettingRestController extends BaseRestController {

    private final SettingModelService settingModelService;

    @PostMapping( "/set" )
    public Object setAValue(

            @RequestParam( "key" ) SettingType key,
            @RequestParam( "value" ) String value
    )  {

        settingModelService.save( key, value );
        return RestResponse.builder().success( true ).message( "setting saved" ).build();
    }

    @GetMapping( "/{key}" )
    public RestResponse getValueByKey(
            @PathVariable( "key" ) SettingType key
    ) {

        SettingResponse settingResponse = settingModelService.findSettingResponseBySettingType( key );
        return RestResponse.builder().success( true ).payload( settingResponse ).build();
    }
}
