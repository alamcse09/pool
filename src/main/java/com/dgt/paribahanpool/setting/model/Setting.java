package com.dgt.paribahanpool.setting.model;

import com.dgt.paribahanpool.enums.SettingType;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table( name = "setting" )
@SQLDelete( sql = "UPDATE setting SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class Setting extends AuditableEntity implements Serializable {

	@Id
	@Column( name = "setting_key" )
	private SettingType key;

	@Column( name = "value", columnDefinition = "TEXT")
	private String value;
}
