package com.dgt.paribahanpool.setting.model;

import com.dgt.paribahanpool.enums.SettingType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SettingResponse {

    private SettingType settingType;
    private String value;
}
