package com.dgt.paribahanpool.setting.service;

import com.dgt.paribahanpool.enums.SettingType;
import com.dgt.paribahanpool.setting.model.Setting;
import com.dgt.paribahanpool.setting.model.SettingResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class SettingModelService {

    private final SettingService settingService;

    public Setting save( SettingType settingType, String value ){

        return settingService.save( settingType, value );
    }

    public SettingResponse findSettingResponseBySettingType( SettingType settingType ){

        return settingService.findSettingResponseBySettingType( settingType );
    }

    public void getAddPage(Model model) {

        List<Setting> settingList = settingService.findAll();
        model.addAttribute( "settings", settingList );

        Map<String,String> dummyCheckbox = new HashMap<>();
        Map<String,String> dummyRadio = new HashMap<>();

        dummyCheckbox.put( "1", "checkbox 1" );
        dummyCheckbox.put( "2", "checkbox 2" );
        dummyCheckbox.put( "3", "checkbox 3" );
        dummyCheckbox.put( "4", "checkbox 4" );

        dummyRadio.put( "yes", "Yes" );
        dummyRadio.put( "no", "No" );

        model.addAttribute( "dummyCheckbox", dummyCheckbox );
        model.addAttribute( "dummyRadio", dummyRadio );
    }
}
