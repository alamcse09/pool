package com.dgt.paribahanpool.setting.service;

import com.dgt.paribahanpool.enums.SettingType;
import com.dgt.paribahanpool.setting.model.Setting;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SettingRepository extends JpaRepository<Setting, SettingType>{

	Optional<Setting> findByKey( SettingType type );
}
