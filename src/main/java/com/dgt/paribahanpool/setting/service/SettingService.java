package com.dgt.paribahanpool.setting.service;

import com.dgt.paribahanpool.enums.SettingType;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.setting.model.Setting;
import com.dgt.paribahanpool.setting.model.SettingResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class SettingService {

	private final SettingRepository settingRepository;

	public Optional<Setting> findBySettingType(SettingType settingType ) {

		return settingRepository.findById( settingType );
	}

	public Setting save( SettingType key, String value ) {
		
		Setting setting = new Setting();

		setting.setKey( key );
		setting.setValue( value );

		return settingRepository.save( setting );
	}

	@Transactional( propagation = Propagation.REQUIRED, readOnly = true )
	public List<Setting> findAll() {

		return settingRepository.findAll();
	}

	public SettingResponse findSettingResponseBySettingType(SettingType settingType) throws NotFoundException {

		Optional<Setting> settingOptional = findBySettingType( settingType );

		if( settingOptional.isPresent() ){

			Setting setting = settingOptional.get();
			return SettingResponse.builder().settingType( setting.getKey() ).value( setting.getValue() ).build();
		}
		else
			throw new NotFoundException( "Setting not found with given key " + settingType );
	}
}
