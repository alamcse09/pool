package com.dgt.paribahanpool.supply_order.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.purchase_demand.model.PurchaseDemandAddRequest;
import com.dgt.paribahanpool.supply_order.model.SupplyOrderAddRequest;
import com.dgt.paribahanpool.supply_order.service.SupplyOrderModelService;
import com.dgt.paribahanpool.supply_order.service.SupplyOrderValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/supply-order")
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class SupplyOrderController extends MVCController {
    private final SupplyOrderModelService supplyOrderModelService;
    private final SupplyOrderValidationService supplyOrderValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.supply.order.add", content = "supply-order/supply-order-add", activeMenu = Menu.SUPPLY_ORDER_ADD)
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.SUPPLY_ORDER_ADD_FORM } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add Supply Order form" );
        supplyOrderModelService.addSupplyOrder( model );
        return viewRoot;
    }

    @PostMapping( value = "/add" )
    public String addPurchaseDemand(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid SupplyOrderAddRequest supplyOrderAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !supplyOrderValidationService.handleBindingResultForAddFormPost( model, bindingResult, supplyOrderAddRequest ) ) {

            return viewRoot;
        }

        supplyOrderModelService.addSupplyOrderPost(supplyOrderAddRequest , redirectAttributes );
        log( LogEvent.SUPPLY_ORDER_ADDED, supplyOrderAddRequest.getId(), "Supply Order added", request );

        return "redirect:/supply-order/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.supply-order.search", content = "supply-order/supply-order-search", activeMenu = Menu.SUPPLY_ORDER_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.SUPPLY_ORDER_SEARCH } )
    public String getSupplyOrderSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Supply Order Search page has been called" );
        return viewRoot;
    }

    @GetMapping( "/print/{id}" )
    public String getSupplyOrderPrinted(
            HttpServletRequest request,
            @PathVariable( "id" ) Long id,
            Model model
    ){

        log.debug( "Supply Order Printing..." );

        supplyOrderModelService.getExpenseList( model, id );

        return "supply-order/supply-order-print";
    }
}
