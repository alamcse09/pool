package com.dgt.paribahanpool.supply_order.controller;

import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.purchase_demand.model.PurchaseDemandSearchResponse;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.supply_order.model.SupplyOrderSearchResonse;
import com.dgt.paribahanpool.supply_order.service.SupplyOrderService;
import com.dgt.paribahanpool.supply_order.service.SupplyOrderValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping( "/api/supply-order" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class SupplyOrderRestController {

    private final SupplyOrderService supplyOrderService;
    private final SupplyOrderValidationService supplyOrderValidationService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<SupplyOrderSearchResonse> searchSupplyOrder(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return supplyOrderService.searchForDatatable( dataTablesInput );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id
    ) throws NotFoundException {

        RestValidationResult restValidationResult = supplyOrderValidationService.delete( id );

        if( restValidationResult.getSuccess() ) {

            supplyOrderService.delete( id );

            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        } else {

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }
}
