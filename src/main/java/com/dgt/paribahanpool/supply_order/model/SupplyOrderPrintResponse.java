package com.dgt.paribahanpool.supply_order.model;

import com.dgt.paribahanpool.enums.ProductType;
import com.dgt.paribahanpool.purchase_demand.model.Product;
import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class SupplyOrderPrintResponse {

    private Long id;

    private ProductType productType;

    private LocalDate date;

    private Integer totalPrice;

    private Double vat;

    private Double grandTotal;

    private List<Product> productList;
}
