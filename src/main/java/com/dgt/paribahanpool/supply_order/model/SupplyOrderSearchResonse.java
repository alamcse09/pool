package com.dgt.paribahanpool.supply_order.model;

import com.dgt.paribahanpool.enums.ProductType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class SupplyOrderSearchResonse {

    private Long id;

    private ProductType productType;

    private String vendorName;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate date;

    private String address;

    private Integer totalPrice;

    private Double vat;

    private Double grandTotal;

    private String comment;

}
