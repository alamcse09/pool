package com.dgt.paribahanpool.supply_order.service;

import com.dgt.paribahanpool.purchase_demand.model.Product;
import com.dgt.paribahanpool.purchase_demand.model.PurchaseDemandAddRequest;
import com.dgt.paribahanpool.supply_order.model.SupplyOrder;
import com.dgt.paribahanpool.supply_order.model.SupplyOrderAddRequest;
import com.dgt.paribahanpool.supply_order.model.SupplyOrderPrintResponse;
import com.dgt.paribahanpool.training.model.TrainingExpenseItemViewResponse;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class SupplyOrderModelService {

    private final SupplyOrderService supplyOrderService;
    private final MvcUtil mvcUtil;

    public void addSupplyOrder(Model model) {
        model.addAttribute("supplyOrderAddRequest",new SupplyOrderAddRequest());
    }

    public void addSupplyOrderPost(SupplyOrderAddRequest supplyOrderAddRequest, RedirectAttributes redirectAttributes) {
        supplyOrderService.save(supplyOrderAddRequest);
        mvcUtil.addSuccessMessage( redirectAttributes, "success.supply.order.add" );
    }

    public void getExpenseList( Model model, Long id ) {

        SupplyOrderPrintResponse supplyOrderPrintResponse = supplyOrderService.getSupplyOrderPrintResponseById(id);
        model.addAttribute( "productList", supplyOrderPrintResponse.getProductList() );
        model.addAttribute( "supplyOrderPrintResponse", supplyOrderPrintResponse );
        model.addAttribute("productType",supplyOrderPrintResponse.getProductType().name());
    }
}
