package com.dgt.paribahanpool.supply_order.service;

import com.dgt.paribahanpool.supply_order.model.SupplyOrder;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface SupplyOrderRepository extends DataTablesRepository<SupplyOrder,Long> {
}
