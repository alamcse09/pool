package com.dgt.paribahanpool.supply_order.service;

import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import com.dgt.paribahanpool.purchase_demand.model.Product;
import com.dgt.paribahanpool.purchase_demand.model.ProductAddRequest;
import com.dgt.paribahanpool.purchase_demand.model.PurchaseDemand;
import com.dgt.paribahanpool.purchase_demand.model.PurchaseDemandSearchResponse;
import com.dgt.paribahanpool.purchase_demand.service.ProductService;
import com.dgt.paribahanpool.supply_order.model.SupplyOrder;
import com.dgt.paribahanpool.supply_order.model.SupplyOrderAddRequest;
import com.dgt.paribahanpool.supply_order.model.SupplyOrderPrintResponse;
import com.dgt.paribahanpool.supply_order.model.SupplyOrderSearchResonse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class SupplyOrderService {

    private final SupplyOrderRepository supplyOrderRepository;
    private final ProductService productService;

    public SupplyOrder save(SupplyOrder supplyOrder){
        return supplyOrderRepository.save(supplyOrder);
    }

    @Transactional
    public SupplyOrder save(SupplyOrderAddRequest supplyOrderAddRequest) {
        SupplyOrder supplyOrder= new SupplyOrder();

        if (supplyOrderAddRequest.getId() != null){
            supplyOrder = findById( supplyOrderAddRequest.getId() ).get();
        }

        supplyOrder = getSupplyOrderFromSupplyOrderAddRequest(supplyOrder, supplyOrderAddRequest);

        return save(supplyOrder);
    }

    private SupplyOrder getSupplyOrderFromSupplyOrderAddRequest(SupplyOrder supplyOrder, SupplyOrderAddRequest supplyOrderAddRequest) {
        supplyOrder.setProductType(supplyOrderAddRequest.getProductType());
        supplyOrder.setDate(supplyOrderAddRequest.getDate());
        supplyOrder.setAddress(supplyOrderAddRequest.getAddress());
        supplyOrder.setVendorName(supplyOrderAddRequest.getVendorName());
        supplyOrder.setTotalPrice(supplyOrderAddRequest.getTotalPrice());
        supplyOrder.setVat(supplyOrderAddRequest.getVat());
        supplyOrder.setGrandTotal(supplyOrderAddRequest.getGrandTotal());
        supplyOrder.setComment(supplyOrderAddRequest.getComment());
        if (supplyOrderAddRequest.getProductAddRequests() != null){
            Set<Product> productSet = supplyOrderAddRequest.getProductAddRequests()
                    .stream()
                    .map( productAddRequest -> getProductFromProductAddRequest(productAddRequest) )
                    .filter( product -> product != null )
                    .collect( Collectors.toSet() );

            supplyOrder.getProductSet().clear();
            supplyOrder.getProductSet().addAll( productSet );
        }
        return supplyOrder;
    }

    public Optional<SupplyOrder> findById(Long id ) {

        return supplyOrderRepository.findById( id );
    }

    private Product getProductFromProductAddRequest( ProductAddRequest productAddRequest ) {

        Product product = new Product();

        if( productAddRequest.getId() != null ) {

            Optional<Product> productOptional = productService.findById( productAddRequest.getId() );

            if( productOptional.isPresent() ) {

                product = productOptional.get();
            } else {

                return null;
            }
        }

        product.setProductName( productAddRequest.getProductName() );
        product.setProductIdentity( productAddRequest.getProductIdentity() );
        product.setTenderNumber( productAddRequest.getTenderNumber() );
        product.setQuantity( productAddRequest.getQuantity() );
        product.setRatePerUnit( productAddRequest.getRatePerUnit() );
        product.setTotalRate( productAddRequest.getTotalRate() );
        product.setComments( productAddRequest.getComments() );

        return product;
    }


    public DataTablesOutput<SupplyOrderSearchResonse> searchForDatatable(DataTablesInput dataTablesInput) {
        return supplyOrderRepository.findAll( dataTablesInput, this::getSupplyOrderSearchResponseFromSupplyOrder );
    }

    private SupplyOrderSearchResonse getSupplyOrderSearchResponseFromSupplyOrder(SupplyOrder supplyOrder) {
        SupplyOrderSearchResonse supplyOrderSearchResonse = new SupplyOrderSearchResonse();

        supplyOrderSearchResonse.setId(supplyOrder.getId());
        supplyOrderSearchResonse.setProductType(supplyOrder.getProductType());
        supplyOrderSearchResonse.setVendorName(supplyOrder.getVendorName());
        supplyOrderSearchResonse.setDate(supplyOrder.getDate());
        supplyOrderSearchResonse.setAddress(supplyOrder.getAddress());
        supplyOrderSearchResonse.setTotalPrice(supplyOrder.getTotalPrice());
        supplyOrderSearchResonse.setVat(supplyOrder.getVat());
        supplyOrderSearchResonse.setGrandTotal(supplyOrder.getGrandTotal());
        supplyOrderSearchResonse.setComment(supplyOrder.getComment());

        return supplyOrderSearchResonse;
    }

    public SupplyOrderPrintResponse getSupplyOrderPrintResponseById(Long id){
        Optional<SupplyOrder> supplyOrder = supplyOrderRepository.findById(id);

        return getSupplyOrderPrintResponseFromSupplyOrder(supplyOrder.get());
    }

    public SupplyOrderPrintResponse getSupplyOrderPrintResponseFromSupplyOrder(SupplyOrder supplyOrder){
        SupplyOrderPrintResponse supplyOrderPrintResponse = new SupplyOrderPrintResponse();

        supplyOrderPrintResponse.setDate(supplyOrder.getDate());
        supplyOrderPrintResponse.setProductType(supplyOrder.getProductType());
        supplyOrderPrintResponse.setTotalPrice(supplyOrder.getTotalPrice());
        supplyOrderPrintResponse.setVat(supplyOrder.getVat());
        supplyOrderPrintResponse.setGrandTotal(supplyOrder.getGrandTotal());
        supplyOrderPrintResponse.setId(supplyOrder.getId());
        List<Product> productList = supplyOrder.getProductSet().stream().collect(Collectors.toList());
        supplyOrderPrintResponse.setProductList(productList);

        return supplyOrderPrintResponse;
    }

    public void delete(Long id) {
        Optional<SupplyOrder> supplyOrderToDelete =  findById( id );

        if( supplyOrderToDelete.isPresent() ) {

            SupplyOrder supplyOrder = supplyOrderToDelete.get();
            supplyOrder.setIsDeleted( true );
            save( supplyOrder );
        }
    }

    public boolean existById(Long id) {
        return supplyOrderRepository.existsById(id);
    }
}
