package com.dgt.paribahanpool.supply_order.service;

import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.purchase_demand.model.PurchaseDemandAddRequest;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.supply_order.model.SupplyOrderAddRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class SupplyOrderValidationService {
    private final SupplyOrderService supplyOrderService;
    public boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, SupplyOrderAddRequest supplyOrderAddRequest) {
        return true;
    }

    public RestValidationResult delete(Long id) throws NotFoundException {
        if( !supplyOrderService.existById( id ) ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }
}
