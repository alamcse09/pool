package com.dgt.paribahanpool.training.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.exceptions.BadRequestException;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.training.model.AttendanceSearchFilter;
import com.dgt.paribahanpool.training.model.TrainingAttendanceAddRequest;
import com.dgt.paribahanpool.training.model.TrainingAttendanceResponse;
import com.dgt.paribahanpool.training.service.TrainingAttendanceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping( "/api/training-attendance" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class TrainerAttendanceRestController extends BaseRestController {

    private final TrainingAttendanceService trainingAttendanceService;

    @PostMapping( "/update" )
    public RestResponse updateAttendance(

            @Valid TrainingAttendanceAddRequest trainingAttendanceAddRequest,
            BindingResult bindingResult

    ) throws BadRequestException {

        handleBindingResult( bindingResult );
        trainingAttendanceService.save( trainingAttendanceAddRequest );
        return RestResponse.builder().success( true ).message( "success.common.action.success" ).build();
    }

    @GetMapping( "/search" )
    public DataTablesOutput<TrainingAttendanceResponse> searchTrainingAttendance(

            DataTablesInput dataTablesInput,
            AttendanceSearchFilter attendanceSearchFilter

    ){

        log.debug( "Training attendance search, params, {}", dataTablesInput );
        return trainingAttendanceService.search( dataTablesInput, attendanceSearchFilter );
    }
}