package com.dgt.paribahanpool.training.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.training.service.TrainingAttendanceModelService;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

@Slf4j
@Controller
@RequestMapping( "/training-attendance" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class TrainingAttendanceController extends MVCController {

    private final TrainingAttendanceModelService trainingAttendanceModelService;

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.training.attendance.search", content = "training/training-attendance-search", activeMenu = Menu.TRAINING_ATTENDANCE_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.TRAINING_ATTENDANCE_SEARCH })
    public String getAttendanceSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Training Attendance Search page called" );
        trainingAttendanceModelService.getTrainingList( model );
        return viewRoot;
    }

    @GetMapping( "/all-trainee/{id}/{date}" )
    @TitleAndContent( title = "title.training.attendance.add", content = "training/training-attendance-add", activeMenu = Menu.TRAINING_ATTENDANCE_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.TRAINING_ATTENDANCE_ADD })
    public String getTraineeAddPage(
            HttpServletRequest request,
            Model model,
            @PathVariable("id") Long trainingId,
            @PathVariable("date") @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_hyphen_ddMMyyyy ) LocalDate date
    ){

        log.debug( "Training Attendance Search page called" );
        trainingAttendanceModelService.getTrainingList( model );
        trainingAttendanceModelService.getTraineeListFromTrainingIdAndDate( model, trainingId, date );
        return viewRoot;
    }
}
