package com.dgt.paribahanpool.training.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.training.model.AttendeeAddRequest;
import com.dgt.paribahanpool.training.model.TrainingAddRequest;
import com.dgt.paribahanpool.training.service.TrainingEvaluationModelService;
import com.dgt.paribahanpool.training.service.TrainingModelService;
import com.dgt.paribahanpool.training.service.TrainingValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Slf4j
@Controller
@RequestMapping( "/training" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )

public class TrainingController extends MVCController {

    private final TrainingModelService trainingModelService;
    private final TrainingValidationService trainingValidationService;
    private final TrainingEvaluationModelService trainingEvaluationModelService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.training.add", content = "training/training-add", activeMenu = Menu.TRAINING_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.TRAINING_ADD } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add training form" );
        trainingModelService.addTraining( model );
        return viewRoot;
    }

    @PostMapping( "/add" )
    public String addTraining(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid TrainingAddRequest trainingAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !trainingValidationService.handleBindingResultForAddFormPost( model, bindingResult, trainingAddRequest ) ){

            return viewRoot;
        }

        if( trainingValidationService.addTrainingPost( redirectAttributes, trainingAddRequest ) ){

            trainingModelService.addTrainingPost( trainingAddRequest, redirectAttributes );
            log( LogEvent.TRAINING_ADDED, trainingAddRequest.getId(), "Training Added", request );
        }

        return "redirect:/training/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.training.search", content = "training/training-search", activeMenu = Menu.TRAINING_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.TRAINING_SEARCH } )
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Training Search page called" );
        return viewRoot;
    }

    @GetMapping( "/attendee/add" )
    @TitleAndContent( title = "title.training.attendee.add", content = "training/training-attendee-add", activeMenu = Menu.TRAINING_ATTENDEE_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.TRAINING_ATTENDEE_ADD } )
    public String getAttendee(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add training attendee form" );
        trainingModelService.addAttendee( model );
        return viewRoot;
    }

    @PostMapping( "/attendee/add" )
    public String postAttendees(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid AttendeeAddRequest attendeeAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !trainingValidationService.handleBindingResultForAddAttendeeFormPost( model, bindingResult, attendeeAddRequest ) ){

            return viewRoot;
        }

        if( trainingValidationService.addAttendeePost( redirectAttributes, attendeeAddRequest ) ){

            trainingModelService.addAttendeePost( attendeeAddRequest, redirectAttributes );
            log( LogEvent.TRAINING_ATTENDEE_ADDED, attendeeAddRequest.getId(), "Attendee Added", request );
        }

        return "redirect:/training/attendee/search/-1/1";
    }

    @GetMapping( "/attendee/search/{trainingId}/{batchNo}" )
    @TitleAndContent( title = "title.training.attendee.search", content = "training/training-attendee-search", activeMenu = Menu.TRAINING_ATTENDEE_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.TRAINING_ATTENDEE_SEARCH } )
    public String getAttendeeSearchPage(
            HttpServletRequest request,
            Model model,
            @NotNull @PathVariable( "trainingId" ) Long trainingId,
            @NotNull @PathVariable( "batchNo" ) Long batchNo
    ){

        log.debug( "Training Attendee Search page called" );
        trainingModelService.addAttendee( model );
        trainingModelService.searchAttendee( model, trainingId, batchNo );
        return viewRoot;
    }

    @GetMapping( "/evaluation/search/{id}" )
    @TitleAndContent( title = "title.training.evaluation.search", content = "training/training-evaluation-search", activeMenu = Menu.TRAINING_EVALUATION_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.TRAINING_EVALUATION_SEARCH })
    public String getEvaluationSearchPage(
            HttpServletRequest request,
            Model model,
            @PathVariable("id") Long trainingId
    ){

        log.debug( "Training Evaluation Search page called" );
        trainingModelService.getTrainingList( model );
        trainingEvaluationModelService.getTraineeFromTrainingId( model, trainingId );
        return viewRoot;
    }

    @GetMapping( "/{id}" )
    @TitleAndContent( title = "title.training.view", content = "training/training-view", activeMenu = Menu.TRAINING_VIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.JQUERY_PRINT, FrontEndLibrary.TRAINING_VIEW })
    public String getTrainingSearchView(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ){

        if( trainingValidationService.view( redirectAttributes, id ) ){

            trainingModelService.view( model, id );
            return viewRoot;
        }

        return "redirect:/training/search";
    }

    @GetMapping( "/report/{id}" )
    @TitleAndContent( title = "title.training.preview", content = "training/training-report", activeMenu = Menu.TRAINING_PREVIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.JQUERY_PRINT } )
    public String getReportSearch(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ){

        if( trainingValidationService.view( redirectAttributes, id ) ){

            trainingModelService.getTrainingViewResponse( model, id );
            return viewRoot;
        }

        return "redirect:/training/search";
    }

}
