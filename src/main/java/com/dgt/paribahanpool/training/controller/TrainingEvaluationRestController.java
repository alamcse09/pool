package com.dgt.paribahanpool.training.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.exceptions.BadRequestException;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.training.model.TrainingEvaluationAddRequest;
import com.dgt.paribahanpool.training.model.TrainingEvaluationResponse;
import com.dgt.paribahanpool.training.service.TrainingEvaluationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping( "/api/training/evaluation/")
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class TrainingEvaluationRestController extends BaseRestController {

    private final TrainingEvaluationService trainingEvaluationService;

    @PostMapping( "/update" )
    public RestResponse updateAttendance(

            @Valid TrainingEvaluationAddRequest trainingEvaluationAddRequest,
            BindingResult bindingResult

    ) throws BadRequestException {

        handleBindingResult( bindingResult );
        trainingEvaluationService.save( trainingEvaluationAddRequest );
        return RestResponse.builder().success( true ).message( "success.common.action.success" ).build();
    }

    @GetMapping( "/search" )
    public DataTablesOutput<TrainingEvaluationResponse> searchTrainingAttendance(

            DataTablesInput dataTablesInput,
            @RequestParam( "trainingId" ) Long trainingId

    ){

        log.debug( "Training evaluation search, params, {}", dataTablesInput );
        return trainingEvaluationService.search( dataTablesInput, trainingId );
    }
}
