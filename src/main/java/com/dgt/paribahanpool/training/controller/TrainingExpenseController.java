package com.dgt.paribahanpool.training.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.training.model.TrainingExpenseAddRequest;
import com.dgt.paribahanpool.training.service.TrainingExpenseModelService;
import com.dgt.paribahanpool.training.service.TrainingExpenseService;
import com.dgt.paribahanpool.training.service.TrainingExpenseValidationService;
import com.dgt.paribahanpool.training.service.TrainingModelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/training-expense" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class TrainingExpenseController extends MVCController {

    private final TrainingExpenseValidationService trainingExpenseValidationService;
    private final TrainingExpenseModelService trainingExpenseModelService;
    private final TrainingModelService trainingModelService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.training.expense.add", content = "training/training-expense-add", activeMenu = Menu.TRAINING_EXPENSE_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.TRAINING_EXPENSE_ADD } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add Training Expense form" );
        trainingExpenseModelService.addExpenseGet( model );
        return viewRoot;
    }

    @PostMapping( "/add" )
    public String addExpense(
            @Valid TrainingExpenseAddRequest trainingExpenseAddRequest,
            BindingResult bindingResult,
            Model model,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){

        if( !trainingExpenseValidationService.handleBindingResultForAddFormPost( model, bindingResult, trainingExpenseAddRequest ) ) {

            return viewRoot;
        }

        if( trainingExpenseValidationService.addTrainingExpensePost( redirectAttributes, trainingExpenseAddRequest ) ) {

            trainingExpenseModelService.addTrainingExpensePost( trainingExpenseAddRequest, model );
            log( LogEvent.TRAINING_EXPENSE_ADDED, -1L, "Training Expense Added", request );
        }

        return "redirect:/training/search";
    }

    @GetMapping("/search/{id}")
    @TitleAndContent( title = "title.training.expense.search", content = "training/training-expense-search", activeMenu = Menu.TRAINING_EXPENSE_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.TRAINING_EXPENSE_SEARCH } )
    public String getExpenseSearch(
            HttpServletRequest request,
            Model model,
            @PathVariable ("id") Long trainingId
    ){

        log.debug( "Training Expense Search page called" );
        trainingModelService.getTrainingViewResponse( model, trainingId );
        trainingExpenseModelService.getExpenseList( model, trainingId );
        return viewRoot;
    }

    @GetMapping( "/print/{trainingId}" )
    public String getReportPrinted(
            HttpServletRequest request,
            @PathVariable( "trainingId" ) Long trainingId,
            Model model
    ){

        log.debug( "Training Expense Printing..." );

        trainingExpenseModelService.getExpenseList( model, trainingId );

        return "training/training-expense-report-print";
    }
}
