package com.dgt.paribahanpool.training.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.training.model.TrainingExpense;
import com.dgt.paribahanpool.training.service.TrainingExpenseModelService;
import com.dgt.paribahanpool.training.service.TrainingExpenseService;
import com.dgt.paribahanpool.training.service.TrainingModelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/training-expense" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class TrainingExpenseRestController extends BaseRestController {

    private final TrainingExpenseModelService trainingExpenseModelService;
    private final TrainingExpenseService trainingExpenseService;

    @DeleteMapping( "/delete/{trainingId}/{expenseId}" )
    public RestResponse delete(
            @PathVariable( "trainingId" ) Long trainingId,
            @PathVariable( "expenseId" ) Long expenseId,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = trainingExpenseModelService.delete( expenseId );
        if( restValidationResult.getSuccess() ){

            log( LogEvent.TRAINING_EXPENSE_DELETE, expenseId, "", request );
            trainingExpenseModelService.updateExpense( trainingId, expenseId );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }
}
