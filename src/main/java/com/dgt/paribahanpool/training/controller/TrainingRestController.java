package com.dgt.paribahanpool.training.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.training.model.TrainingSearchResponse;
import com.dgt.paribahanpool.training.service.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/training" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class TrainingRestController extends BaseRestController {

    private final TrainingService trainingService;
    private final TrainingValidationService trainingValidationService;
    private final TraineeValidationService traineeValidationService;
    private final TrainerValidationService trainerValidationService;
    private final CourseDirectorValidationService courseDirectorValidationService;
    private final TraineeService traineeService;
    private final TrainerService trainerService;
    private final TrainingCourseDirectorService trainingCourseDirectorService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<TrainingSearchResponse> searchTraining(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return trainingService.searchForDatatable( dataTablesInput );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = trainingValidationService.delete( id );
        if( restValidationResult.getSuccess() ){

            log( LogEvent.TRAINING_DELETED, id, "", request );
            trainingService.delete( id );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @DeleteMapping( "/attendee/trainee/{id}" )
    public RestResponse deleteTrainee(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = traineeValidationService.delete( id );
        if( restValidationResult.getSuccess() ){

            log( LogEvent.TRAINING_TRAINEE_DELETED, id, "", request );
            traineeService.delete( id );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @DeleteMapping( "/attendee/trainer/{id}" )
    public RestResponse deleteTrainer(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = trainerValidationService.delete( id );
        if( restValidationResult.getSuccess() ){

            log( LogEvent.TRAINING_TRAINER_DELETED, id, "", request );
            trainerService.delete( id );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @DeleteMapping( "/attendee/course-director/{id}" )
    public RestResponse deleteCourseDirector(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = courseDirectorValidationService.delete( id );
        if( restValidationResult.getSuccess() ){

            log( LogEvent.TRAINING_COURSE_DIRECTOR_DELETED, id, "", request );
            trainingCourseDirectorService.delete( id );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }
}
