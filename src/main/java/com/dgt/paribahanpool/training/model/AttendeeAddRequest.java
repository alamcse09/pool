package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.user.model.UserResponse;
import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class AttendeeAddRequest {

    private Long id;

    private Long trainingId;

    private Long batchNo;

    List<UserResponse> showEmployeeList;

    Long[] participantUserIdList;

    @Valid
    List<TraineeAddRequest> traineeAddRequestList;

    @Valid
    List<TrainerAddRequest> trainerAddRequestList;

    @Valid
    List<TrainingCourseDirectorAddRequest> trainingCourseDirectorAddRequestList;

}
