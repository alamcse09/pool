package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.designation.model.Designation;
import com.dgt.paribahanpool.enums.Grade;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "training_course_director" )
public class CourseDirector {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String name;

    @Column( name = "grade" )
    private Grade grade;

    @Column( name = "batch_no" )
    private Long batchNo;

    @OneToOne
    @JoinColumn( name = "designation_id", foreignKey = @ForeignKey( name = "fk_training_course_director_designation_id" ) )
    private Designation designation;

    @ManyToOne
    @JoinColumn( name = "training_id", foreignKey = @ForeignKey( name = "fk_course_director_training_id" ) )
    private Training training;
}
