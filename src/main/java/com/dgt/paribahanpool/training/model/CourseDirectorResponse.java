package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.designation.model.DesignationSearchResponse;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CourseDirectorResponse {

    private Long id;
    private String name;
    private DesignationSearchResponse designation;
}
