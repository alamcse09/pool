package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.designation.model.Designation;
import com.dgt.paribahanpool.designation.model.DesignationSearchResponse;
import com.dgt.paribahanpool.enums.Grade;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "trainee" )
public class Trainee {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "trainee_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String traineeName;

    @Column( name = "grade" )
    private Grade grade;

    @Column( name = "batch_no" )
    private Long batchNo;

    @OneToOne
    @JoinColumn( name = "designation_id", foreignKey = @ForeignKey( name = "fk_trainee_designation_id" ) )
    private Designation designation;

    @ManyToOne
    @JoinColumn( name = "training_id", foreignKey = @ForeignKey( name = "fk_trainee_training_id" ) )
    private Training training;

    public TraineeResponse getTraineeResponse(){

        Designation traineeDesignation = this.getDesignation();

        return TraineeResponse
                .builder()
                .id( this.getId() )
                .traineeName( this.getTraineeName() )
                .designation(
                        DesignationSearchResponse
                                .builder()
                                .id( traineeDesignation.getId() )
                                .nameEn( traineeDesignation.getNameEn() )
                                .nameBn( traineeDesignation.getNameBn() )
                                .build()
                )
                .build();
    }
}
