package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.enums.Grade;
import lombok.Data;

import javax.persistence.Column;

@Data
public class TraineeAddRequest {

    private Long id;

    private String traineeName;

    private Grade grade;

    private Long traineeDesignationId;
}
