package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.designation.model.DesignationSearchResponse;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TraineeResponse {

    private Long id;
    private String traineeName;
    private DesignationSearchResponse designation;
    private Boolean isPresent;
    private Double mark;
    private Long batchNo;
}
