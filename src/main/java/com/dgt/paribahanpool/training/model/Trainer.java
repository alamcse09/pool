package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.designation.model.Designation;
import com.dgt.paribahanpool.enums.Grade;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "trainer" )
public class Trainer {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "trainer_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String trainerName;

    @Column( name = "grade" )
    private Grade grade;

    @Column( name = "batch_no" )
    private Long batchNo;

    @OneToOne
    @JoinColumn( name = "designation_id", foreignKey = @ForeignKey( name = "fk_trainer_designation_id" ) )
    private Designation designation;

    @ManyToOne
    @JoinColumn( name = "training_id", foreignKey = @ForeignKey( name = "fk_trainer_training_id" ) )
    private Training training;
}
