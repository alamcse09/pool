package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.enums.Grade;
import lombok.Data;

@Data
public class TrainerAddRequest {

    private Long id;

    private String trainerName;

    private Grade grade;

    private Long trainerDesingationId;
}
