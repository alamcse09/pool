package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.enums.MonthSpan;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table( name = "training" )
@SQLDelete( sql = "UPDATE training SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class Training extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "training_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String trainingName;

    @Column( name = "location", columnDefinition = "TEXT" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String location;

    @Column( name = "topic", columnDefinition = "TEXT" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String topic;

    @Column( name = "month_span" )
    private MonthSpan monthSpan;

    @Column( name = "starting_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate startingDate;

    @Column( name = "ending_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate endingDate;

    @Column( name = "batch_count" )
    private Long batchCount;

    @Column( name = "total_expense_cost" )
    private Double totalExpenseCost;

    public TrainingResponse getTrainingResponse(){

        return TrainingResponse
                .builder()
                .id( this.getId() )
                .trainingName( this.getTrainingName() )
                .build();
    }
}
