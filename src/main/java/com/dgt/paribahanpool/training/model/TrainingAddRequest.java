package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.enums.MonthSpan;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class TrainingAddRequest {

    public Long id;

    @NotNull( message = "{validation.common.required}")
    private String trainingName;

    @NotBlank( message = "{validation.common.required}" )
    private String location;

    @NotBlank( message = "{validation.common.required}" )
    private String topic;

    private MonthSpan monthSpan;

    private Long batchCount;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate startingDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate endingDate;
}
