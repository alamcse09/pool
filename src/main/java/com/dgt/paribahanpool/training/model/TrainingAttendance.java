package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.util.model.VersionableAuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table( name = "training_attendance" )
public class TrainingAttendance extends VersionableAuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "is_present" )
    private Boolean isPresent;

    @Column( name = "date" )
    private LocalDate attendanceDate;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn( name = "training_id", foreignKey = @ForeignKey( name = "fk_training_training_attendance_id" ) )
    private Training training;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn( name = "trainee_id", foreignKey = @ForeignKey( name = "fk_training_attendance_trainee_id" ) )
    private Trainee trainee;
}
