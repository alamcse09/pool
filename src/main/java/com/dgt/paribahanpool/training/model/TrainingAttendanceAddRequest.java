package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class TrainingAttendanceAddRequest {

    private Long trainingId;
    private Long traineeId;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_hyphen_ddMMyyyy )
    private LocalDate attendanceDate;
    private Boolean isPresent;
}
