package com.dgt.paribahanpool.training.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TrainingAttendanceResponse {

    private Long id;
    private Boolean isPresent;
    private TrainingResponse training;
    private TraineeResponse trainee;
}
