package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.enums.Grade;
import lombok.Data;

@Data
public class TrainingCourseDirectorAddRequest {

    private Long id;

    private String trainingCourseDirectorName;

    private Grade grade;

    private Long trainingCourseDirectorDesingationId;
}
