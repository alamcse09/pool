package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
@Table( name = "training_evaluation" )
public class TrainingEvaluation extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "mark" )
    private Double mark;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn( name = "training_id", foreignKey = @ForeignKey( name = "fk_training_training_evaluation_id" ) )
    private Training training;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn( name = "trainee_id", foreignKey = @ForeignKey( name = "fk_training_attendance_evaluation_id" ) )
    private Trainee trainee;
}
