package com.dgt.paribahanpool.training.model;

import lombok.Data;

@Data
public class TrainingEvaluationAddRequest {

    private Long trainingId;
    private Long traineeId;
    private Double mark;
}
