package com.dgt.paribahanpool.training.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TrainingEvaluationResponse {

    private Long id;
    private Double mark;
    private TrainingResponse training;
    private TraineeResponse trainee;
}
