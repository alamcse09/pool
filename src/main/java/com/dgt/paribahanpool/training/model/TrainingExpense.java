package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
@Table( name = "training_expense" )
public class TrainingExpense extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "amount" )
    private Integer amount;

    @Column( name = "rate" )
    private Double rate;

    @Column( name = "total_cost" )
    private Double totalCost;

    @Column( name = "comment", length = 4096 )
    private String comment;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn( name = "training_id", foreignKey = @ForeignKey( name = "fk_training_expense_training_id" ) )
    private Training training;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn( name = "training_expense_type_id", foreignKey = @ForeignKey( name = "fk_training_expense_training_expense_type_id" ) )
    private TrainingExpenseType trainingExpenseType;
}
