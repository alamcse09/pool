package com.dgt.paribahanpool.training.model;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class TrainingExpenseAddRequest {

    private Long id;

    @NotNull( message = "validation.common.required" )
    @Min( value = 1, message = "validation.common.min" )
    private Long trainingId;

    @Valid
    private List<TrainingExpenseItemAddRequest> items;
}
