package com.dgt.paribahanpool.training.model;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class TrainingExpenseItemAddRequest {

    private Long id;

    @NotNull( message = "validation.common.required" )
    @Min( value = 1, message = "validation.common.min" )
    private Long trainingExpenseTypeId;

    @NotNull( message = "validation.common.required" )
    @Min( value = 1, message = "validation.common.min" )
    private Integer amount;

    @NotNull( message = "validation.common.required" )
    @Min( value = 1, message = "validation.common.min" )
    private Double rate;

    @NotNull( message = "validation.common.required" )
    @Min( value = 1, message = "validation.common.min" )
    private Double totalCost;

    private String comment;
}
