package com.dgt.paribahanpool.training.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TrainingExpenseItemViewResponse {

    private Long id;
    private TrainingExpenseTypeViewResponse trainingExpenseTypeId;
    private Integer amount;
    private Double rate;
    private Double totalCost;
    private String comment;
}
