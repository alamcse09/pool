package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "training_expense_type" )
public class TrainingExpenseType extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "name_en", length = 512 )
    private String nameEn;

    @Column( name = "name_bn", length = 512 )
    private String nameBn;
}
