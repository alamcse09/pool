package com.dgt.paribahanpool.training.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TrainingExpenseTypeViewResponse {

    private Long id;
    private String nameEn;
    private String nameBn;
}
