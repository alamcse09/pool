package com.dgt.paribahanpool.training.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TrainingResponse {

    private Long id;
    private String trainingName;
}
