package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.enums.MonthSpan;
import lombok.Data;

@Data
public class TrainingSearchResponse {

    private Long id;
    private String trainingName;
    private String location;
    private String topic;
    private MonthSpan monthSpan;
    private Long batchCount;
    private Double totalExpenseCost;
}
