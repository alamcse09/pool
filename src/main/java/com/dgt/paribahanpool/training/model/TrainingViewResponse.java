package com.dgt.paribahanpool.training.model;

import com.dgt.paribahanpool.enums.MonthSpan;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Data
@Builder
public class TrainingViewResponse {

    private Long id;
    private String trainingName;

    private List<TraineeResponse> topTraineeResponseList;
    private List<TraineeResponse> allTraineeResponseList;
    private Map<Long,List<TraineeResponse>> traineeResponseMap;

    private String location;
    private String topic;
    private MonthSpan monthSpan;
    private Long batchCount;
    private LocalDate startingDate;
    private LocalDate endingDate;
    private Double totalExpenseCost;
    private int totalTrainee;
    private int totalTrainer;
}
