package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.*;
import org.springframework.data.jpa.domain.Specification;

public class CourseDirectorSpecification {

    public static Specification<CourseDirector> filterByTraining( Long trainingId ){

        if( trainingId == null ){

            return Specification.where( null );
        }

        return ( (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( CourseDirector_.TRAINING ).get( Training_.ID ), trainingId ) );
    }

    public static Specification<CourseDirector> filterByBatchNo( Long batchNo ){

        if( batchNo == null ){

            return Specification.where( null );
        }

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( CourseDirector_.BATCH_NO ), batchNo );
    }
}
