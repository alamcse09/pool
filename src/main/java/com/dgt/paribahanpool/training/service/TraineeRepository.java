package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.Trainee;
import com.dgt.paribahanpool.training.model.Training;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TraineeRepository extends JpaRepository<Trainee,Long>, DataTablesRepository<Trainee, Long> {
    List<Trainee> findByTraining(Training training);
}
