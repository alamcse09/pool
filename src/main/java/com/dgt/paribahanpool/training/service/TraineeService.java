package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.designation.model.Designation;
import com.dgt.paribahanpool.designation.model.DesignationSearchResponse;
import com.dgt.paribahanpool.designation.service.DesignationService;
import com.dgt.paribahanpool.employee.model.Employee;
import com.dgt.paribahanpool.employee.service.EmployeeService;
import com.dgt.paribahanpool.training.model.*;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class TraineeService {

    private final TraineeRepository traineeRepository;
    private final DesignationService designationService;
    private final UserService userService;

    public Trainee getReference( Long id ){

        return traineeRepository.getById( id );
    }

    private void save( Trainee trainee ) {

        traineeRepository.save( trainee );
    }

    public void saveTraineeFromAttendee(AttendeeAddRequest attendeeAddRequest, Training training ) {

        for( TraineeAddRequest traineeAddRequest : attendeeAddRequest.getTraineeAddRequestList() ){

            Designation designation = designationService.findById( traineeAddRequest.getTraineeDesignationId() ).get();

            Trainee trainee = new Trainee();

            trainee.setTraineeName( traineeAddRequest.getTraineeName() );
            trainee.setGrade( traineeAddRequest.getGrade() );
            trainee.setDesignation( designation );
            trainee.setTraining( training );
            trainee.setBatchNo( attendeeAddRequest.getBatchNo() );
            save( trainee );
        }

    }

    public List<Trainee> findByTraining(Training training) {

        return traineeRepository.findByTraining( training );
    }

    public List<TraineeResponse> getTraineeResposeListFromSpecification( Specification<Trainee> traineeSpecification ) {

        List<Trainee> traineeList = traineeRepository.findAll( traineeSpecification );

        List<TraineeResponse> traineeResponseList = traineeList.stream().map( this::getTraineeResponseFromTrainee ).collect( Collectors.toList() );

        return traineeResponseList;
    }

    private TraineeResponse getTraineeResponseFromTrainee( Trainee trainee ) {

        Designation traineeDesignation = trainee.getDesignation();

        return TraineeResponse
                .builder()
                .id( trainee.getId() )
                .traineeName( trainee.getTraineeName() )
                .designation(
                        DesignationSearchResponse
                                .builder()
                                .id( traineeDesignation.getId() )
                                .nameEn( traineeDesignation.getNameEn() )
                                .nameBn( traineeDesignation.getNameBn() )
                                .build()
                )
                .build();
    }

    public Boolean existById( Long id ) {

        return traineeRepository.existsById( id );
    }

    public void delete( Long id ){

        traineeRepository.deleteById( id );
    }

    public List<Trainee> getTraineeListByTrainingId( Long id ) {

        Specification<Trainee> traineeSpecification = TraineeSpecification.filterByTraining( id ) ;
        List<Trainee> traineeList = traineeRepository.findAll( traineeSpecification );
        return traineeList;
    }

    @Transactional
    public void saveTraineeFromParticipantUser(AttendeeAddRequest attendeeAddRequest, Training training) {
        for(Long userId : attendeeAddRequest.getParticipantUserIdList()){
            if (userId != null){
                User user = userService.findById(userId).get();

                Designation designation = user.getEmployee().getDesignation();

                Trainee trainee = new Trainee();

                trainee.setTraineeName( user.getEmployee().getName() );
                trainee.setGrade( user.getEmployee().getCurrentGrade() );
                trainee.setDesignation( designation );
                trainee.setTraining( training );
                trainee.setBatchNo( attendeeAddRequest.getBatchNo() );
                save( trainee );
            }
        }
    }
}
