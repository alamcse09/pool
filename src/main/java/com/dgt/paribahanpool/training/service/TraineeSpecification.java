package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.*;
import org.springframework.data.jpa.domain.Specification;

public class TraineeSpecification {

    public static Specification<Trainee> filterByTraining( Long trainingId ){

        if( trainingId == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Trainee_.TRAINING ).get( Training_.ID ), trainingId );
    }

    public static Specification<Trainee> filterByBatchNo(Long batchNo ){

        if( batchNo == null ){

            return Specification.where( null );
        }

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Trainee_.BATCH_NO ), batchNo );
    }
}
