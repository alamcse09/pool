package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.Trainee;
import com.dgt.paribahanpool.training.model.Trainer;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainerRepository extends JpaRepository<Trainer,Long>, DataTablesRepository<Trainer, Long> {
}
