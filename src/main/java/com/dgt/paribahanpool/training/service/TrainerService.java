package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.designation.model.Designation;
import com.dgt.paribahanpool.designation.model.DesignationSearchResponse;
import com.dgt.paribahanpool.designation.service.DesignationService;
import com.dgt.paribahanpool.training.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class TrainerService {

        private final TrainerRepository trainerRepository;
        private final DesignationService designationService;

        public void saveTrainerFromAttendee( AttendeeAddRequest attendeeAddRequest, Training training ) {

                for( TrainerAddRequest trainerAddRequest : attendeeAddRequest.getTrainerAddRequestList() ){

                        Designation designation = designationService.findById( trainerAddRequest.getTrainerDesingationId() ).get();

                        Trainer trainer = new Trainer();

                        trainer.setTrainerName( trainerAddRequest.getTrainerName() );
                        trainer.setGrade( trainerAddRequest.getGrade() );
                        trainer.setDesignation( designation );
                        trainer.setTraining( training );
                        trainer.setBatchNo( attendeeAddRequest.getBatchNo() );
                        save( trainer );
                }
        }

        private void save( Trainer trainer ) {

                trainerRepository.save( trainer );
        }

        public List<TrainerResponse> getTrainerResponseFromSpecifiation( Specification<Trainer> trainerSpecification ) {

                List<Trainer> trainerList = trainerRepository.findAll( trainerSpecification );

                List<TrainerResponse> trainerResponseList = trainerList.stream().map( this::getTrainerResponseFromTrainer ).collect(Collectors.toList());

                return trainerResponseList;
        }

        private TrainerResponse getTrainerResponseFromTrainer( Trainer trainer ) {

                Designation traineeDesignation = trainer.getDesignation();

                return TrainerResponse
                        .builder()
                        .id( trainer.getId() )
                        .trainerName( trainer.getTrainerName() )
                        .designation(
                                DesignationSearchResponse
                                        .builder()
                                        .id( traineeDesignation.getId() )
                                        .nameEn( traineeDesignation.getNameEn() )
                                        .nameBn( traineeDesignation.getNameBn() )
                                        .build()
                        )
                        .build();
        }

        public Boolean existById( Long id ) {

                return trainerRepository.existsById( id );
        }

        public void delete( Long id ) {

                trainerRepository.deleteById( id );
        }
}
