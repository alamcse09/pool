package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.Trainer;
import com.dgt.paribahanpool.training.model.Trainer_;
import com.dgt.paribahanpool.training.model.Training_;
import org.springframework.data.jpa.domain.Specification;

public class TrainerSpecification {

    public static Specification<Trainer> filterByTraining( Long trainingId ){

        if( trainingId == null ){

            return Specification.where( null );
        }

        return ( (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Trainer_.TRAINING ).get( Training_.ID ), trainingId ) );
    }

    public static Specification<Trainer> filterByBatchNo( Long batchNo ){

        if( batchNo == null ){

            return Specification.where( null );
        }

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Trainer_.BATCH_NO ), batchNo );
    }
}
