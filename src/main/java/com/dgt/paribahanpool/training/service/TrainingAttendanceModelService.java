package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.TraineeResponse;
import com.dgt.paribahanpool.training.model.Training;
import com.dgt.paribahanpool.training.model.TrainingAttendance;
import com.dgt.paribahanpool.training.model.TrainingSearchResponse;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.dgt.paribahanpool.training.service.TrainingAttendanceSpecification.filterByPresentDay;
import static com.dgt.paribahanpool.training.service.TrainingAttendanceSpecification.filterByTraining;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class TrainingAttendanceModelService {

    private final TrainingService trainingService;
    private final TrainingAttendanceService trainingAttendanceService;

    public void getTrainingList( Model model ) {

        List<Training> trainingList = trainingService.findAll();

        List<TrainingSearchResponse> trainingSearchResponseList = trainingService.getTrainingSearchResponseListFromTrainingList( trainingList );

        model.addAttribute( "trainingSearchResponseList", trainingSearchResponseList );
    }

    public void getTraineeListFromTrainingIdAndDate( Model model, Long trainingId, LocalDate date ) {

        Specification<TrainingAttendance> trainingAttendanceSpecification = filterByTraining( trainingId )
                .and( filterByPresentDay( date ) );

        List<TraineeResponse> traineeListResponseListFromSpecification = trainingAttendanceService.getTraineeListResponseListFromSpecification( trainingAttendanceSpecification );

        List<TraineeResponse> traineeResponseListFromTrainingId = trainingAttendanceService.getAllTrainee( trainingId );

        List<TraineeResponse> traineeResponseList = trainingAttendanceService.getTraineeResponseListFromSpecificationTraineeListAndTrainingTraineeList( traineeListResponseListFromSpecification, traineeResponseListFromTrainingId );

        model.addAttribute( "traineeResponseList", traineeResponseList );

        model.addAttribute( "trainingId", trainingId );

        String dateString = date.format(DateTimeFormatter.ofPattern(DateTimeFormatPattern.dateFormat_slash_ddMMyyyy));

        model.addAttribute( "date", dateString );
    }
}
