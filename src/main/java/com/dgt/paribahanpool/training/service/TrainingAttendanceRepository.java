package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.Trainee;
import com.dgt.paribahanpool.training.model.Training;
import com.dgt.paribahanpool.training.model.TrainingAttendance;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface TrainingAttendanceRepository extends JpaRepository<TrainingAttendance, Long>, DataTablesRepository<TrainingAttendance,Long> {

    Optional<TrainingAttendance> findByTrainingAndTraineeAndAttendanceDate( Training training, Trainee trainee, LocalDate attendanceDate );
}
