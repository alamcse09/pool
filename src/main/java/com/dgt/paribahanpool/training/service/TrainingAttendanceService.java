package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.designation.model.Designation;
import com.dgt.paribahanpool.designation.model.DesignationSearchResponse;
import com.dgt.paribahanpool.training.model.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.dgt.paribahanpool.training.service.TrainingAttendanceSpecification.filterByPresentDay;
import static com.dgt.paribahanpool.training.service.TrainingAttendanceSpecification.filterByTraining;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class TrainingAttendanceService {

    private final TrainingAttendanceRepository trainingAttendanceRepository;
    private final TrainingService trainingService;
    private final TraineeService traineeService;

    public TrainingAttendance save( TrainingAttendance trainingAttendance ){

        return trainingAttendanceRepository.save( trainingAttendance );
    }

    public TrainingAttendance save( TrainingAttendanceAddRequest trainingAttendanceAddRequest ){

        return save( getTrainingAttendanceFromAddRequest( trainingAttendanceAddRequest ) );
    }

    private TrainingAttendance getTrainingAttendanceFromAddRequest( TrainingAttendanceAddRequest trainingAttendanceAddRequest ) {

        Training training = trainingService.getReference( trainingAttendanceAddRequest.getTrainingId() );
        Trainee trainee = traineeService.getReference( trainingAttendanceAddRequest.getTraineeId() );

        Optional<TrainingAttendance> trainingAttendanceOptional = trainingAttendanceRepository.findByTrainingAndTraineeAndAttendanceDate( training, trainee, trainingAttendanceAddRequest.getAttendanceDate() );

        TrainingAttendance trainingAttendance = new TrainingAttendance();

        if( trainingAttendanceOptional.isPresent() ){

            trainingAttendance = trainingAttendanceOptional.get();
        }

        trainingAttendance.setTraining( training );
        trainingAttendance.setTrainee( trainee );
        trainingAttendance.setAttendanceDate( trainingAttendanceAddRequest.getAttendanceDate() );
        trainingAttendance.setIsPresent( trainingAttendanceAddRequest.getIsPresent() );

        return trainingAttendance;
    }

    @Transactional
    public DataTablesOutput<TrainingAttendanceResponse> search( DataTablesInput dataTablesInput, AttendanceSearchFilter attendanceSearchFilter) {

        Specification<TrainingAttendance> trainingAttendanceSpecification = filterByTraining( attendanceSearchFilter.getTrainingId() )
                                                                            .and( filterByPresentDay( attendanceSearchFilter.getAttendanceDate() ) );

        return trainingAttendanceRepository.findAll( dataTablesInput, null, trainingAttendanceSpecification, this::getTrainingAttendanceSearchResponseFromTrainingAttendance );
    }

    private TrainingAttendanceResponse getTrainingAttendanceSearchResponseFromTrainingAttendance(TrainingAttendance trainingAttendance) {

        Training training = trainingAttendance.getTraining();
        Trainee trainee = trainingAttendance.getTrainee();
        Designation traineeDesignation = trainee.getDesignation();

        return TrainingAttendanceResponse
                .builder()
                .id( trainingAttendance.getId() )
                .training( training.getTrainingResponse() )
                .trainee( trainee.getTraineeResponse() )
                .isPresent( trainingAttendance.getIsPresent() )
                .build();
    }

    @Transactional
    public List<TraineeResponse> getAllTrainee( Long trainingId ) {

        Training training = trainingService.getReference( trainingId );
        List<Trainee> traineeList = traineeService.findByTraining( training );

        return traineeList.stream().map( this::getTrainingAttendacneTraineeResponseFromTrainee ).collect( Collectors.toList() );
    }

    private TraineeResponse getTrainingAttendacneTraineeResponseFromTrainee(Trainee trainee ) {

        Designation traineeDesignation = trainee.getDesignation();

        return TraineeResponse
                .builder()
                .id( trainee.getId() )
                .traineeName( trainee.getTraineeName() )
                .isPresent( false )
                .designation(
                        DesignationSearchResponse
                                .builder()
                                .id( traineeDesignation.getId() )
                                .nameEn( traineeDesignation.getNameEn() )
                                .nameBn( traineeDesignation.getNameBn() )
                                .build()
                )
                .build();
    }

    public List<TraineeResponse> getTraineeListResponseListFromSpecification( Specification<TrainingAttendance> trainingAttendanceSpecification ) {

        List<TrainingAttendance> trainingAttendanceList = trainingAttendanceRepository.findAll( trainingAttendanceSpecification );

        List<TraineeResponse> traineeResponseListFromTrainingAttendance = trainingAttendanceList.stream().map( this::getTraineeResponseFromTrainingAttendance ).collect(Collectors.toList());

        return traineeResponseListFromTrainingAttendance;
    }

    private TraineeResponse getTraineeResponseFromTrainingAttendance( TrainingAttendance trainingAttendance ) {

        Trainee trainee = trainingAttendance.getTrainee();

        Designation traineeDesignation = trainee.getDesignation();

        return TraineeResponse
                .builder()
                .id( trainee.getId() )
                .traineeName( trainee.getTraineeName() )
                .isPresent( trainingAttendance.getIsPresent() )
                .designation(
                        DesignationSearchResponse
                                .builder()
                                .id( traineeDesignation.getId() )
                                .nameEn( traineeDesignation.getNameEn() )
                                .nameBn( traineeDesignation.getNameBn() )
                                .build()
                )
                .build();
    }

    public List<TraineeResponse> getTraineeResponseListFromSpecificationTraineeListAndTrainingTraineeList( List<TraineeResponse> traineeListResponseListFromSpecification, List<TraineeResponse> traineeResponseListFromTrainingId ) {

        List<TraineeResponse> finalTraineeResponseList = new ArrayList<>();

        for( TraineeResponse traineeResponseFromTraining: traineeResponseListFromTrainingId ){

            for( TraineeResponse traineeResponseFromSpecification : traineeListResponseListFromSpecification ){

                if( traineeResponseFromSpecification.getId() == traineeResponseFromTraining.getId() ){

                    traineeResponseFromTraining.setIsPresent( traineeResponseFromSpecification.getIsPresent() );
                    break;
                }
            }

            finalTraineeResponseList.add( traineeResponseFromTraining );
        }

        return finalTraineeResponseList;
    }
}
