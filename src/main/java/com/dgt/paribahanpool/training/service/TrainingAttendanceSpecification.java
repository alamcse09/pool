package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.TrainingAttendance;
import com.dgt.paribahanpool.training.model.TrainingAttendance_;
import com.dgt.paribahanpool.training.model.Training_;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;

public class TrainingAttendanceSpecification {

    public static Specification<TrainingAttendance> filterByTraining( Long trainingId ) {

        if( trainingId == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( TrainingAttendance_.TRAINING ).get( Training_.ID ), trainingId );
    }

    public static Specification<TrainingAttendance> filterByPresentDay( LocalDate attendanceDate ) {

        if( attendanceDate == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( TrainingAttendance_.ATTENDANCE_DATE ), attendanceDate );
    }


}
