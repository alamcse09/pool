package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.enums.Grade;
import com.dgt.paribahanpool.training.model.CourseDirector;
import com.dgt.paribahanpool.training.model.Trainer;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainingCourseDirectorRepository extends JpaRepository<CourseDirector,Long>, DataTablesRepository<CourseDirector, Long> {

    CourseDirector findByNameAndGrade( String name, Grade grade );

}
