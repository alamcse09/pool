package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.designation.model.Designation;
import com.dgt.paribahanpool.designation.model.DesignationSearchResponse;
import com.dgt.paribahanpool.designation.service.DesignationService;
import com.dgt.paribahanpool.enums.Grade;
import com.dgt.paribahanpool.training.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class TrainingCourseDirectorService {

    private final TrainingCourseDirectorRepository trainingCourseDirectorRepository;
    private final DesignationService designationService;

    public void saveCourseDirectorFromAttendee(AttendeeAddRequest attendeeAddRequest, Training training ) {

        for( TrainingCourseDirectorAddRequest trainingCourseDirectorAddRequest : attendeeAddRequest.getTrainingCourseDirectorAddRequestList() ){

            Designation designation = designationService.findById( trainingCourseDirectorAddRequest.getTrainingCourseDirectorDesingationId() ).get();

            CourseDirector courseDirector = new CourseDirector();

            courseDirector.setName( trainingCourseDirectorAddRequest.getTrainingCourseDirectorName() );
            courseDirector.setGrade( trainingCourseDirectorAddRequest.getGrade() );
            courseDirector.setDesignation( designation );
            courseDirector.setTraining( training );
            courseDirector.setBatchNo( attendeeAddRequest.getBatchNo() );
            save( courseDirector );
        }
    }

    private void save( CourseDirector courseDirector ) {

        trainingCourseDirectorRepository.save( courseDirector );
    }

    public List<CourseDirectorResponse> getCourseDirectorResponseListFromSpecification( Specification<CourseDirector> courseDirectorSpecification ) {

        List<CourseDirector> trainerList = trainingCourseDirectorRepository.findAll( courseDirectorSpecification );

        List<CourseDirectorResponse> courseDirectorResponse = trainerList.stream().map( this::getCourseDirectorResponseFromCourseDirector ).collect(Collectors.toList());

        return courseDirectorResponse;
    }

    private CourseDirectorResponse getCourseDirectorResponseFromCourseDirector( CourseDirector courseDirector ) {

        Designation designation = courseDirector.getDesignation();

        return CourseDirectorResponse
                .builder()
                .id( courseDirector.getId() )
                .name( courseDirector.getName() )
                .designation(
                        DesignationSearchResponse
                                .builder()
                                .id( designation.getId() )
                                .nameEn( designation.getNameEn() )
                                .nameBn( designation.getNameBn() )
                                .build()
                )
                .build();
    }

    public Boolean existById( Long id ) {

        return trainingCourseDirectorRepository.existsById( id );
    }

    public CourseDirector getNameAndGrade( String name, Grade grade ){

        return trainingCourseDirectorRepository.findByNameAndGrade( name, grade );
    }

    public void delete( Long id ) {

        trainingCourseDirectorRepository.deleteById( id );
    }
}
