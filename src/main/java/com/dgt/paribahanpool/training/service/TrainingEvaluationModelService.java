package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.TraineeResponse;
import com.dgt.paribahanpool.training.model.TrainingEvaluation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class TrainingEvaluationModelService {

    private final TrainingEvaluationService trainingEvaluationService;
    private final TraineeService traineeService;


    public void getTraineeFromTrainingId( Model model, Long trainingId ) {

        Specification<TrainingEvaluation> trainingEvaluationSpecification = TrainingEvaluationSpecification.findByTrainingId( trainingId );

        List<TraineeResponse> traineeResponseListFromSpecification = trainingEvaluationService.getTraineeEvaluationListFromSpecification( trainingEvaluationSpecification );

        List<TraineeResponse> traineeResponseListFromTraining = trainingEvaluationService.getAllTrainee( trainingId );

        List<TraineeResponse> traineeResponseList = trainingEvaluationService.getTraineeResponseFromSpecificationAndTraining( traineeResponseListFromSpecification, traineeResponseListFromTraining );

        model.addAttribute( "traineeResponseList", traineeResponseList );

        model.addAttribute( "trainingId", trainingId );
    }
}
