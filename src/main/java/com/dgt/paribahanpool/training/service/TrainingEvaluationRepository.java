package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.Trainee;
import com.dgt.paribahanpool.training.model.Training;
import com.dgt.paribahanpool.training.model.TrainingAttendance;
import com.dgt.paribahanpool.training.model.TrainingEvaluation;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TrainingEvaluationRepository extends JpaRepository<TrainingEvaluation,Long> , DataTablesRepository<TrainingEvaluation,Long> {
    Optional<TrainingEvaluation> findByTrainingAndTrainee(Training training, Trainee trainee);

    List<TrainingEvaluation> findByTraining(Training training);
}
