package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.designation.model.Designation;
import com.dgt.paribahanpool.designation.model.DesignationSearchResponse;
import com.dgt.paribahanpool.training.model.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class TrainingEvaluationService {

    private final TrainingEvaluationRepository trainingEvaluationRepository;
    private final TrainingService trainingService;
    private final TraineeService traineeService;

    public TrainingEvaluation save( TrainingEvaluation trainingEvaluation ){

        return trainingEvaluationRepository.save( trainingEvaluation );
    }

    @Transactional
    public TrainingEvaluation save( TrainingEvaluationAddRequest trainingEvaluationAddRequest ){

        return save( getTrainingEvaluationFromAddRequest( trainingEvaluationAddRequest ) );
    }

    private TrainingEvaluation getTrainingEvaluationFromAddRequest( TrainingEvaluationAddRequest trainingAttendanceAddRequest ) {

        Training training = trainingService.getReference( trainingAttendanceAddRequest.getTrainingId() );
        Trainee trainee = traineeService.getReference( trainingAttendanceAddRequest.getTraineeId() );

        Optional<TrainingEvaluation> trainingEvaluationOptional = trainingEvaluationRepository.findByTrainingAndTrainee( training, trainee );

        TrainingEvaluation trainingEvaluation = new TrainingEvaluation();

        if( trainingEvaluationOptional.isPresent() ){

            trainingEvaluation = trainingEvaluationOptional.get();
        }

        trainingEvaluation.setTraining( training );
        trainingEvaluation.setTrainee( trainee );
        trainingEvaluation.setMark( trainingAttendanceAddRequest.getMark() );

        return trainingEvaluation;
    }

    @Transactional
    public DataTablesOutput<TrainingEvaluationResponse>  search( DataTablesInput dataTablesInput, Long trainingId ) {

        Specification<TrainingEvaluation> trainingEvaluationSpecification = TrainingEvaluationSpecification.findByTrainingId( trainingId );
        return trainingEvaluationRepository.findAll( dataTablesInput, null, trainingEvaluationSpecification, this::getTrainingEvaluationResponseFromTrainingEvaluation );
    }

    private TrainingEvaluationResponse getTrainingEvaluationResponseFromTrainingEvaluation(TrainingEvaluation trainingEvaluation) {

        return TrainingEvaluationResponse.builder()
                .id( trainingEvaluation.getId() )
                .mark( trainingEvaluation.getMark() )
                .training( trainingEvaluation.getTraining().getTrainingResponse() )
                .trainee( trainingEvaluation.getTrainee().getTraineeResponse() )
                .build();
    }

    public List<TraineeResponse> getTraineeEvaluationListFromSpecification( Specification<TrainingEvaluation> trainingEvaluationSpecification ) {

        List<TrainingEvaluation> trainingEvaluationList = trainingEvaluationRepository.findAll( trainingEvaluationSpecification );

        return trainingEvaluationList.stream().map( this::getTrainingResponseFromTrainingEvaluation ).collect( Collectors.toList() );
    }

    private TraineeResponse getTrainingResponseFromTrainingEvaluation( TrainingEvaluation trainingEvaluation ) {

        Trainee trainee = trainingEvaluation.getTrainee();

        Designation designation = trainee.getDesignation();

        return TraineeResponse
                .builder()
                .id( trainee.getId() )
                .traineeName( trainee.getTraineeName() )
                .mark( trainingEvaluation.getMark() )
                .designation(
                        DesignationSearchResponse
                                .builder()
                                .id( designation.getId() )
                                .nameEn( designation.getNameEn() )
                                .nameBn( designation.getNameBn() )
                                .build()
                )
                .build();
    }

    public List<TraineeResponse> getTraineeResponseFromSpecificationAndTraining( List<TraineeResponse> traineeListResponseListFromSpecification, List<TraineeResponse> traineeResponseListFromTrainingId ) {

        List<TraineeResponse> finalTraineeResponseList = new ArrayList<>();

        for( TraineeResponse traineeResponseFromTraining: traineeResponseListFromTrainingId ){

            for( TraineeResponse traineeResponseFromSpecification : traineeListResponseListFromSpecification ){

                if( traineeResponseFromSpecification.getId() == traineeResponseFromTraining.getId() ){

                    traineeResponseFromTraining.setMark( traineeResponseFromSpecification.getMark() );
                    break;
                }
            }

            finalTraineeResponseList.add( traineeResponseFromTraining );
        }

        return finalTraineeResponseList;
    }

    private TraineeResponse getTraineeResponseFromTrainee( Trainee trainee ) {

        Designation traineeDesignation = trainee.getDesignation();

        return TraineeResponse
                .builder()
                .id( trainee.getId() )
                .traineeName( trainee.getTraineeName() )
                .isPresent( false )
                .mark( null )
                .designation(
                        DesignationSearchResponse
                                .builder()
                                .id( traineeDesignation.getId() )
                                .nameEn( traineeDesignation.getNameEn() )
                                .nameBn( traineeDesignation.getNameBn() )
                                .build()
                )
                .build();
    }

    @Transactional
    public List<TraineeResponse> getAllTrainee( Long trainingId ) {

        Training training = trainingService.getReference( trainingId );
        List<Trainee> traineeList = traineeService.findByTraining( training );

        return traineeList.stream().map( this::getTraineeResponseFromTrainee ).collect( Collectors.toList() );
    }

    @Transactional
    public TrainingViewResponse getTrainingViewResponseFromTrainingId( Long id ) {

            Training training = trainingService.findById( id ).get();

            List<TrainingEvaluation> trainingEvaluationList = trainingEvaluationRepository.findByTraining( training );

            List<TraineeResponse> traineeResponseList = getTraineeResponseListFromTrainingEvaluationList( trainingEvaluationList );
            List<TraineeResponse> topTraineeResponseList = getTopTraineeResponseListFromTraneeResponseList( trainingEvaluationList );

            Map<Long, List<TraineeResponse>> traineeResponseMap = traineeResponseList
                    .stream()
                    .collect(Collectors.groupingBy(TraineeResponse::getBatchNo));

        TrainingViewResponse trainingViewResponse = TrainingViewResponse.builder()
                    .id( training.getId() )
                    .trainingName( training.getTrainingName() )
                    .allTraineeResponseList( traineeResponseList )
                    .topTraineeResponseList( topTraineeResponseList )
                    .traineeResponseMap( traineeResponseMap )
                    .build();

            return trainingViewResponse;
    }

    private Long getBatchNoFromTraineeResponse(TraineeResponse traineeResponse){ return traineeResponse.getBatchNo();}

    private List<TraineeResponse> getTopTraineeResponseListFromTraneeResponseList(List<TrainingEvaluation> trainingEvaluationList) {

        return trainingEvaluationList.stream()
                .sorted( Comparator.comparing( TrainingEvaluation::getMark ).reversed() )
                .limit( 3 )
                .map( this::getTraineeResponseFromTrainingEvaluation )
                .collect(Collectors.toList());
    }

    private List<TraineeResponse> getTraineeResponseListFromTrainingEvaluationList(List<TrainingEvaluation> trainingEvaluationList) {

        return trainingEvaluationList.stream()
                .map( this::getTraineeResponseFromTrainingEvaluation )
                .collect(Collectors.toList());
    }

    private TraineeResponse getTraineeResponseFromTrainingEvaluation( TrainingEvaluation trainingEvaluation ) {

        return TraineeResponse.builder()
                .id( trainingEvaluation.getTrainee().getId() )
                .traineeName( trainingEvaluation.getTrainee().getTraineeName() )
                .designation(
                        DesignationSearchResponse.builder()
                                .id( trainingEvaluation.getTrainee().getDesignation().getId() )
                                .nameEn( trainingEvaluation.getTrainee().getDesignation().getNameEn() )
                                .nameBn( trainingEvaluation.getTrainee().getDesignation().getNameBn() )
                                .build()
                )
                .mark( trainingEvaluation.getMark() )
                .batchNo( trainingEvaluation.getTrainee().getBatchNo() )
                .build();
    }
}
