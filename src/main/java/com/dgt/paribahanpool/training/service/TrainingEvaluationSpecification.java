package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.Trainee_;
import com.dgt.paribahanpool.training.model.TrainingEvaluation;
import com.dgt.paribahanpool.training.model.TrainingEvaluation_;
import com.dgt.paribahanpool.training.model.Training_;
import org.springframework.data.jpa.domain.Specification;

public class TrainingEvaluationSpecification {

    public static Specification<TrainingEvaluation> findByTrainingId(Long trainingId ){

        if( trainingId == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(TrainingEvaluation_.TRAINING ).get(Training_.ID ), trainingId );
    }

    public static Specification<TrainingEvaluation> findByTraineeId( Long traineeId ){

        if( traineeId == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(TrainingEvaluation_.TRAINEE ).get(Trainee_.ID ), traineeId );
    }
}
