package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.training.model.*;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class TrainingExpenseModelService {

    private final TrainingExpenseService trainingExpenseService;
    private final TrainingService trainingService;
    private final TrainingExpenseTypeService trainingExpenseTypeService;
    private final MvcUtil mvcUtil;

    public void addTrainingExpensePost( TrainingExpenseAddRequest trainingExpenseAddRequest, Model model ) {

        trainingExpenseService.save( trainingExpenseAddRequest );
        mvcUtil.addSuccessMessage( model, "training-expense.add.success" );
    }

    public void addExpenseGet( Model model ) {

        model.addAttribute( "trainingExpenseAddRequest", new TrainingExpenseAddRequest() );

        List<Training> trainingList = trainingService.findAll();

        List<TrainingSearchResponse> trainingSearchResponseList = trainingService.getTrainingSearchResponseListFromTrainingList( trainingList );

        model.addAttribute( "trainingSearchResponseList", trainingSearchResponseList );

        List<TrainingExpenseType> trainingExpenseTypeList = trainingExpenseTypeService.findAll();

        List<TrainingExpenseTypeViewResponse> trainingExpenseTypeViewResponseList = trainingExpenseTypeService.getExpenseTypeViewResponseListFromExpenseList( trainingExpenseTypeList );

        model.addAttribute( "trainingExpenseTypeViewResponseList", trainingExpenseTypeViewResponseList );
    }

    public void getExpenseList( Model model, Long trainingId ) {

        List<TrainingExpenseItemViewResponse> itemViewResponseList = trainingExpenseService.getExpenseViewResponseListFromTrainingId( trainingId );
        model.addAttribute( "itemViewResponseList", itemViewResponseList );
        model.addAttribute( "trainingName", trainingService.findById(trainingId).get().getTrainingName() );
    }

    public RestValidationResult delete( Long expenseId ) throws NotFoundException {

        Boolean exist = trainingExpenseService.existById( expenseId );
        if( !exist ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    @Transactional
    public void updateExpense( Long trainingId, Long expenseId ) {

        Double amountToBeDeleted = trainingExpenseService.getReference( expenseId ).getTotalCost();

        trainingExpenseService.delete( expenseId );

        trainingService.updateTotalCost( trainingId, amountToBeDeleted );
    }
}
