package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.TrainingExpense;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TrainingExpenseRepository extends JpaRepository<TrainingExpense,Long> {

    List<TrainingExpense> findAllByTrainingId(Long trainingId);
}
