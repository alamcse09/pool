package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class TrainingExpenseService {

    private final TrainingExpenseTypeService trainingExpenseTypeService;
    private final TrainingExpenseRepository trainingExpenseRepository;
    private final TrainingService trainingService;

    public TrainingExpense getReference( Long id ){

        return trainingExpenseRepository.getById( id );
    }

    public List<TrainingExpense> save( List<TrainingExpense> trainingExpenseList ){

        return trainingExpenseRepository.saveAll( trainingExpenseList );
    }

    private List<TrainingExpense> findAllByTrainingId( Long trainingId ) {

        return trainingExpenseRepository.findAllByTrainingId( trainingId );
    }

    @Transactional
    public List<TrainingExpense> save(TrainingExpenseAddRequest trainingExpenseAddRequest ) {

        Optional<Training> trainingOptional = trainingService.findById( trainingExpenseAddRequest.getTrainingId() );

        if( trainingOptional.isPresent() ) {

            Training training = trainingOptional.get();

            List<TrainingExpense> trainingExpenseList = trainingExpenseAddRequest.getItems()
                    .stream()
                    .map(trainingExpenseItemAddRequest -> getTrainingExpenseFromTrainingExpenseItemAddRequest(trainingExpenseItemAddRequest, training))
                    .collect(Collectors.toList());

            Double totalCost = 0d;

            for (TrainingExpense trainingExpense : trainingExpenseList) {

                totalCost += trainingExpense.getTotalCost();
            }

            training.setTotalExpenseCost(totalCost);

            trainingService.save(training);

            return save(trainingExpenseList);
        }

        return null;
    }

    private TrainingExpense getTrainingExpenseFromTrainingExpenseItemAddRequest( TrainingExpenseItemAddRequest trainingExpenseItemAddRequest, Training training ) {

        TrainingExpenseType trainingExpenseType = trainingExpenseTypeService.getReference( trainingExpenseItemAddRequest.getTrainingExpenseTypeId() );

        TrainingExpense trainingExpense = new TrainingExpense();

        trainingExpense.setTraining( training );
        trainingExpense.setTrainingExpenseType( trainingExpenseType );

        trainingExpense.setAmount( trainingExpenseItemAddRequest.getAmount() );
        trainingExpense.setRate( trainingExpenseItemAddRequest.getRate() );
        trainingExpense.setTotalCost( trainingExpenseItemAddRequest.getTotalCost() );
        trainingExpense.setComment( trainingExpenseItemAddRequest.getComment() );

        return trainingExpense;
    }

    public List<TrainingExpenseItemViewResponse> getExpenseViewResponseListFromTrainingId( Long trainingId ) {

        List<TrainingExpense> trainingExpenseList = findAllByTrainingId( trainingId );

        return trainingExpenseList.stream().map( this::getExpenseViewResponseListFromTrainingExpense ).collect( Collectors.toList() );
    }

    private TrainingExpenseItemViewResponse getExpenseViewResponseListFromTrainingExpense( TrainingExpense trainingExpense ){

        return TrainingExpenseItemViewResponse.builder()
                .id( trainingExpense.getId() )
                .trainingExpenseTypeId(
                        TrainingExpenseTypeViewResponse
                                .builder()
                                .id( trainingExpense.getTrainingExpenseType().getId() )
                                .nameBn( trainingExpense.getTrainingExpenseType().getNameBn() )
                                .nameEn( trainingExpense.getTrainingExpenseType().getNameEn() )
                                .build()
                )
                .amount( trainingExpense.getAmount() )
                .totalCost( trainingExpense.getTotalCost() )
                .rate( trainingExpense.getRate() )
                .comment( trainingExpense.getComment() )
                .build();
    }

    public Boolean existById( Long expenseId ) {

        return trainingExpenseRepository.existsById( expenseId );
    }

    public void delete( Long expenseId ) {

        trainingExpenseRepository.deleteById( expenseId );
    }
}
