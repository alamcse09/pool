package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.TrainingExpenseType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainingExpenseTypeRepository extends JpaRepository<TrainingExpenseType,Long> {
}
