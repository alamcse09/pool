package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.Training;
import com.dgt.paribahanpool.training.model.TrainingExpenseType;
import com.dgt.paribahanpool.training.model.TrainingExpenseTypeViewResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class TrainingExpenseTypeService {

    private final TrainingExpenseTypeRepository trainingExpenseTypeRepository;

    public TrainingExpenseType getReference(Long trainingExpenseTypeId) {

        return trainingExpenseTypeRepository.getById( trainingExpenseTypeId );
    }

    public List<TrainingExpenseType> findAll() {

        return trainingExpenseTypeRepository.findAll();
    }

    public List<TrainingExpenseTypeViewResponse> getExpenseTypeViewResponseListFromExpenseList( List<TrainingExpenseType> trainingExpenseTypeList ) {

        return trainingExpenseTypeList.stream()
                .map(
                        expenseType -> TrainingExpenseTypeViewResponse.builder()
                                .id( expenseType.getId() )
                                .nameBn( expenseType.getNameBn() )
                                .nameEn(expenseType.getNameEn() )
                                .build()
                )
                .collect(Collectors.toList());
    }
}
