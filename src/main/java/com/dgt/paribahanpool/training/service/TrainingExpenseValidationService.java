package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.training.model.TrainingExpenseAddRequest;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class TrainingExpenseValidationService {

    private final TrainingExpenseService trainingExpenseService;
    private final MvcUtil mvcUtil;

    public boolean handleBindingResultForAddFormPost( Model model, BindingResult bindingResult, TrainingExpenseAddRequest trainingExpenseAddRequest ) {

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "trainingExpenseAddRequest" )
                .object( trainingExpenseAddRequest )
                .title( "title.vehicle.add" )
                .content( "training/training-expense-add" )
                .activeMenu( Menu.TRAINING_ADD )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.TRAINING_ADD } )
                .build();

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );
    }

    public boolean addTrainingExpensePost( RedirectAttributes redirectAttributes, TrainingExpenseAddRequest trainingExpenseAddRequest ) {

        return true;
    }
}
