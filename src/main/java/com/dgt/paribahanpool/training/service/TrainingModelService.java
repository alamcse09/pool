package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.designation.model.DesignationViewResponse;
import com.dgt.paribahanpool.designation.service.DesignationService;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.training.model.*;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class TrainingModelService {

    private final MvcUtil mvcUtil;
    private final DesignationService designationService;
    private final TrainingService trainingService;
    private final TraineeService traineeService;
    private final TrainerService trainerService;
    private final TrainingCourseDirectorService trainingCourseDirectorService;
    private final TrainingEvaluationService trainingEvaluationService;
    private final UserService userService;

    @Transactional
    public void addTraining( Model model ){

        TrainingAddRequest trainingAddRequest = new TrainingAddRequest();

        model.addAttribute("trainingAddRequest", trainingAddRequest);

        List<DesignationViewResponse> designationViewResponseList =  designationService.getDesignationViewResponseList();

        model.addAttribute("designationViewResponseList", designationViewResponseList);
    }

    public void addTrainingPost( TrainingAddRequest trainingAddRequest, RedirectAttributes redirectAttributes ) {

        trainingService.save( trainingAddRequest );

        if( trainingAddRequest.getId() != null ){

            mvcUtil.addSuccessMessage( redirectAttributes, "success.training.edit" );
        }
        else{

            mvcUtil.addSuccessMessage( redirectAttributes, "success.training.add" );
        }
    }

    public void addAttendee( Model model ) {

        AttendeeAddRequest attendeeAddRequest = new AttendeeAddRequest();

        List<UserResponse> userResponseList = userService.getUserResponseList( UserType.SYSTEM_USER );
        attendeeAddRequest.setShowEmployeeList( userResponseList );

        model.addAttribute( "attendeeAddRequest", attendeeAddRequest );

        List<Training> trainingList = trainingService.findAll();

        List<Long> batchNoList = trainingService.getBatchList( AppConstants.TRAINING_BATCH_NO );

        model.addAttribute("batchNoList", batchNoList );

        List<TrainingSearchResponse> trainingSearchResponseList = trainingService.getTrainingSearchResponseListFromTrainingList( trainingList );

        model.addAttribute( "trainingSearchResponseList", trainingSearchResponseList );

        List<DesignationViewResponse> designationViewResponseList =  designationService.getDesignationViewResponseList();

        model.addAttribute("designationViewResponseList", designationViewResponseList);
    }

    public void addAttendeePost( AttendeeAddRequest attendeeAddRequest, RedirectAttributes redirectAttributes ) {

        trainingService.saveAttendees( attendeeAddRequest );

        if( attendeeAddRequest.getId() != null ){

            mvcUtil.addSuccessMessage( redirectAttributes, "success.training.attendee.edit" );
        }
        else{

            mvcUtil.addSuccessMessage( redirectAttributes, "success.training.attendee.add" );
        }
    }

    public void searchAttendee( Model model, Long trainingId, Long batchNo ){

        Specification<Trainee> traineeSpecification = TraineeSpecification.filterByTraining( trainingId ).and( TraineeSpecification.filterByBatchNo( batchNo ) );

        List<TraineeResponse> traineeResponseList = traineeService.getTraineeResposeListFromSpecification( traineeSpecification );

        model.addAttribute( "traineeResponseList", traineeResponseList );

        Specification<Trainer> trainerSpecification = TrainerSpecification.filterByTraining( trainingId ).and( TrainerSpecification.filterByBatchNo( batchNo ) );

        List<TrainerResponse> trainerResponseList = trainerService.getTrainerResponseFromSpecifiation( trainerSpecification );

        model.addAttribute( "trainerResponseList", trainerResponseList );

        Specification<CourseDirector> courseDirectorSpecification = CourseDirectorSpecification.filterByTraining( trainingId ).and( CourseDirectorSpecification.filterByBatchNo( batchNo ) );

        List<CourseDirectorResponse> courseDirectorResponseList = trainingCourseDirectorService.getCourseDirectorResponseListFromSpecification( courseDirectorSpecification );

        model.addAttribute( "courseDirectorResponseList", courseDirectorResponseList );

        model.addAttribute( "trainingId", trainingId );
        model.addAttribute( "batchNo", batchNo );

    }

    public void getTrainingList( Model model ) {

        List<Training> trainingList = trainingService.findAll();

        List<TrainingSearchResponse> trainingSearchResponseList = trainingService.getTrainingSearchResponseListFromTrainingList( trainingList );

        model.addAttribute( "trainingSearchResponseList", trainingSearchResponseList );
    }

    public void view( Model model, Long id ) {

        TrainingViewResponse trainingViewResponse = trainingEvaluationService.getTrainingViewResponseFromTrainingId( id );
        model.addAttribute("trainingViewResponse", trainingViewResponse );
    }

    public void getTrainingViewResponse( Model model, Long trainingId ) {

        TrainingViewResponse trainingViewResponse = trainingService.getTrainingViewResponse( trainingId );
        model.addAttribute( "trainingViewResponse", trainingViewResponse );
    }
}