package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.training.model.Training;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TrainingRepository extends DataTablesRepository<Training, Long>, JpaSpecificationExecutor<Training>, JpaRepository<Training,Long> {
}
