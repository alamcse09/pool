package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.designation.model.DesignationSearchResponse;
import com.dgt.paribahanpool.training.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class TrainingService {

    private final TrainingRepository trainingRepository;
    private final TraineeService traineeService;
    private final TrainerService trainerService;
    private final TrainingCourseDirectorService trainingCourseDirectorService;

    public Optional<Training> findById( Long id ) {

        return trainingRepository.findById( id );
    }

    public Training getReference( Long id ){

        return trainingRepository.getById( id );
    }

    @Transactional
    public void save( TrainingAddRequest trainingAddRequest ) {

        Training training = new Training();

        if( trainingAddRequest.getId() != null ){

            training = findById( trainingAddRequest.getId() ).get();
        }

        training = getTrainingFromTrainingAddRequest( training, trainingAddRequest );

        save( training );
    }

    public Training save( Training training ){

        return trainingRepository.save( training );
    }

    private Training getTrainingFromTrainingAddRequest( Training training, TrainingAddRequest trainingAddRequest ) {

        training.setTrainingName( trainingAddRequest.getTrainingName() );
        training.setLocation( trainingAddRequest.getLocation() );
        training.setTopic( trainingAddRequest.getTopic() );
        training.setMonthSpan( trainingAddRequest.getMonthSpan() );
        training.setBatchCount( trainingAddRequest.getBatchCount() );
        training.setStartingDate( trainingAddRequest.getStartingDate() );
        training.setEndingDate( trainingAddRequest.getEndingDate() );

        return training;
    }

    public DataTablesOutput<TrainingSearchResponse> searchForDatatable( DataTablesInput dataTablesInput ) {

        return trainingRepository.findAll( dataTablesInput, this::getTrainingSearchResponseFromTraining );
    }

    private TrainingSearchResponse getTrainingSearchResponseFromTraining( Training training ) {

        TrainingSearchResponse trainingSearchResponse = new TrainingSearchResponse();

        trainingSearchResponse.setId( training.getId() );
        trainingSearchResponse.setTrainingName( training.getTrainingName() );
        trainingSearchResponse.setLocation( training.getLocation() );
        trainingSearchResponse.setTopic( training.getTopic() );
        trainingSearchResponse.setMonthSpan( training.getMonthSpan() );
        trainingSearchResponse.setBatchCount( training.getBatchCount() );
        trainingSearchResponse.setTotalExpenseCost( training.getTotalExpenseCost() );

        return trainingSearchResponse;
    }

    public Boolean existById( Long id ) {

        return trainingRepository.existsById( id );
    }

    public void delete( Long id ) {

        trainingRepository.deleteById( id );
    }

    public List<Training> findAll() {

        return trainingRepository.findAll();
    }

    public List<TrainingSearchResponse> getTrainingSearchResponseListFromTrainingList( List<Training> trainingList ) {

        List<TrainingSearchResponse> trainingSearchResponseList = new ArrayList<>();

        for( Training training : trainingList ){

            TrainingSearchResponse trainingSearchResponse = new TrainingSearchResponse();

            trainingSearchResponse.setId( training.getId() );
            trainingSearchResponse.setTrainingName( training.getTrainingName() + " (" + training.getMonthSpan() + ") " );

            trainingSearchResponseList.add( trainingSearchResponse );
        }

        return trainingSearchResponseList;
    }

    public List<Long> getBatchList( Long trainingBatchNo ) {

        List<Long> batchList = new ArrayList<>();

        for( Long batchNo = 1L; batchNo<= trainingBatchNo; batchNo++ ){

            batchList.add( batchNo );
        }

        return batchList;
    }

    public void saveAttendees( AttendeeAddRequest attendeeAddRequest ) {

        Training training = findById( attendeeAddRequest.getTrainingId() ).get();

        if( attendeeAddRequest.getTraineeAddRequestList() != null ){

            traineeService.saveTraineeFromAttendee( attendeeAddRequest, training );
        }

        if( attendeeAddRequest.getTrainerAddRequestList() != null ){

            trainerService.saveTrainerFromAttendee( attendeeAddRequest, training );
        }

        if( attendeeAddRequest.getTrainingCourseDirectorAddRequestList() != null ){

            trainingCourseDirectorService.saveCourseDirectorFromAttendee( attendeeAddRequest, training );
        }

        if( attendeeAddRequest.getParticipantUserIdList() != null){

            traineeService.saveTraineeFromParticipantUser( attendeeAddRequest, training );
        }
    }

    @Transactional
    public TrainingViewResponse getTrainingViewResponse( Long trainingId ) {

        Optional<Training> trainingOptional = findById( trainingId );
        Specification<Trainer> trainerSpecification = TrainerSpecification.filterByTraining( trainingId );

        if( trainingOptional.isPresent() ) {

            Training training = trainingOptional.get();

            return TrainingViewResponse.builder()
                    .id( training.getId())
                    .trainingName( training.getTrainingName() )
                    .location( training.getLocation() )
                    .topic( training.getTopic() )
                    .monthSpan( training.getMonthSpan() )
                    .batchCount( training.getBatchCount() )
                    .startingDate( training.getStartingDate() )
                    .endingDate( training.getEndingDate() )
                    .totalExpenseCost( training.getTotalExpenseCost() )
                    .totalTrainee( traineeService.getTraineeListByTrainingId( trainingId ).size() )
                    .totalTrainer( trainerService.getTrainerResponseFromSpecifiation( trainerSpecification).size() )
                    .build();
        }

        return null;
    }

    public void updateTotalCost( Long trainingId, Double amountToBeDeleted ) {

        Optional<Training> trainingOptional = findById( trainingId );

        if( trainingOptional.isPresent() ) {

            Training training = trainingOptional.get();

            training.setTotalExpenseCost(training.getTotalExpenseCost() - amountToBeDeleted);

            save(training);
        }
    }
}
