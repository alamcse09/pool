package com.dgt.paribahanpool.training.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.designation.model.DesignationViewResponse;
import com.dgt.paribahanpool.designation.service.DesignationService;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.incident.model.Incident;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.training.model.AttendeeAddRequest;
import com.dgt.paribahanpool.training.model.Training;
import com.dgt.paribahanpool.training.model.TrainingAddRequest;
import com.dgt.paribahanpool.training.model.TrainingSearchResponse;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class TrainingValidationService {

    private final MvcUtil mvcUtil;
    private final TrainingService trainingService;
    private final DesignationService designationService;

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, TrainingAddRequest trainingAddRequest ) {

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "trainingAddRequest" )
                        .object( trainingAddRequest )
                        .title( "title.training.add" )
                        .content( "training/training-add" )
                        .activeMenu( Menu.TRAINING_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.TRAINING_ADD } )
                        .build()
        );
    }

    public Boolean addTrainingPost( RedirectAttributes redirectAttributes, TrainingAddRequest trainingAddRequest ) {

        if( trainingAddRequest.getId() != null ){

            Optional<Training> trainingOptional = trainingService.findById( trainingAddRequest.getId() );

            if( !trainingOptional.isPresent() ){

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
                return false;
            }
        }

        return true;
    }

    public RestValidationResult delete(Long id) throws NotFoundException {

        Boolean exist = trainingService.existById( id );
        if( !exist ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean handleBindingResultForAddAttendeeFormPost( Model model, BindingResult bindingResult, AttendeeAddRequest attendeeAddRequest ) {

        List<DesignationViewResponse> designationViewResponseList =  designationService.getDesignationViewResponseList();
        model.addAttribute( "designationViewResponseList", designationViewResponseList );

        List<Training> trainingList = trainingService.findAll();
        List<Long> batchNoList = trainingService.getBatchList( AppConstants.TRAINING_BATCH_NO );
        model.addAttribute("batchNoList", batchNoList );

        List<TrainingSearchResponse> trainingSearchResponseList = trainingService.getTrainingSearchResponseListFromTrainingList( trainingList );
        model.addAttribute( "trainingSearchResponseList", trainingSearchResponseList );

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "attendeeAddRequest" )
                        .object( attendeeAddRequest )
                        .title( "title.training.attendee.add" )
                        .content( "training/training-attendee-add" )
                        .activeMenu( Menu.TRAINING_ATTENDEE_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.TRAINING_ATTENDEE_ADD } )
                        .build()
        );
    }

    public Boolean addAttendeePost( RedirectAttributes redirectAttributes, AttendeeAddRequest attendeeAddRequest ) {

        if( attendeeAddRequest.getId() != null ){

            Optional<Training> trainingOptional = trainingService.findById( attendeeAddRequest.getTrainingId() );

            if( !trainingOptional.isPresent() ){

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
                return false;
            }
        }

        return true;
    }

    public Boolean view( RedirectAttributes redirectAttributes, Long id ) {

        Optional<Training> trainingOptional = trainingService.findById( id );

        if( !trainingOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }
}
