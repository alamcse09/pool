package com.dgt.paribahanpool.user.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.exceptions.EmailDataBadRequestException;
import com.dgt.paribahanpool.user.model.ResetPasswordRequest;
import com.dgt.paribahanpool.user.model.SignupAddRequest;
import com.dgt.paribahanpool.user.service.UserModelService;
import com.dgt.paribahanpool.user.service.UserValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class PublicUserController {

    private final UserModelService userModelService;
    private final UserValidationService userValidationService;

    @GetMapping( "/forgot-password" )
    public String forgetPassword(){

        return "security/forgot-password";
    }

    @PostMapping( "/forgot-password" )
    public String forgetPasswordPost(

            RedirectAttributes redirectAttributes,
            @RequestParam( "email" ) String email

    ) throws EmailDataBadRequestException {

        if( userValidationService.forgetPassword( redirectAttributes, email ) ){

            userModelService.forgetPassword( redirectAttributes, email );
        }

        return "redirect:/login";
    }

    @GetMapping( "/reset-password/{userId}/{token}" )
    public String resetPassword(

            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable( "userId" ) Long userId,
            @PathVariable( "token" ) String token
    ){

        if( userValidationService.resetPassword( redirectAttributes, userId, token ) ){

            userModelService.resetPassword( model, userId, token );
            return "security/reset-password";
        }

        return "redirect:/login";
    }

    @PostMapping( "/reset-password" )
    public String resetPasswordPost(

            Model model,
            RedirectAttributes redirectAttributes,
            @Valid ResetPasswordRequest resetPasswordRequest,
            BindingResult bindingResult
    ){

        if( !userValidationService.handleBindingResultResetPassword( model, bindingResult, resetPasswordRequest ) ){

            return "security/reset-password";
        }

        if( userValidationService.resetPassword( redirectAttributes, resetPasswordRequest.getUserId(), resetPasswordRequest.getToken() ) ){

            userModelService.resetPassword( redirectAttributes, resetPasswordRequest );
        }

        return "redirect:/login";
    }

    @GetMapping( "/signup" )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.SIGNUP } )
    public String signUp(

            HttpServletRequest request,
            Model model
    ){

        userModelService.addSignupGet( model );
        return "security/signup";
    }

    @PostMapping( "/signup" )
    public String signUpPost(

            Model model,
            RedirectAttributes redirectAttributes,
            @Valid SignupAddRequest signupAddRequest,
            BindingResult bindingResult
    ) {

        if( !userValidationService.handleBindingResultSignupRequest( model, bindingResult, signupAddRequest) ){

            return "security/signup";
        }

        if( userValidationService.signup( model, signupAddRequest ) ){

            userModelService.addSignupPost( redirectAttributes, signupAddRequest );
            return "redirect:/login";
        }
        else{

            return "security/signup";
        }
    }
}
