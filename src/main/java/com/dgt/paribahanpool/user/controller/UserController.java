package com.dgt.paribahanpool.user.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.user.model.MyProfileRequest;
import com.dgt.paribahanpool.user.model.PasswordChangeRequest;
import com.dgt.paribahanpool.user.service.UserModelService;
import com.dgt.paribahanpool.user.service.UserValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Slf4j
@Controller
@RequestMapping( "/user" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class UserController extends MVCController {

    private final UserValidationService userValidationService;
    private final UserModelService userModelService;

    @GetMapping( "/password" )
    @TitleAndContent( title = "title.user.change.password", content = "user/change-password", activeMenu = Menu.CHANGE_PASSWORD )
    @AddFrontEndLibrary( libraries = FrontEndLibrary.CHANGE_PASSWORD )
    public String changePassword(

            HttpServletRequest request,
            Model model
    ){

        userModelService.changePasswordGet( model );
        return viewRoot;
    }

    @PostMapping( "/password" )
    public String changePasswordPost(

            RedirectAttributes redirectAttributes,
            Model model,
            PasswordChangeRequest passwordChangeRequest,
            BindingResult bindingResult
    ){

        if( !userValidationService.handleBindingResultChangePassword( model, bindingResult, passwordChangeRequest ) ){

            return "user/password";
        }

        if( userValidationService.changePassword( redirectAttributes, passwordChangeRequest, getLoggedInUser() ) ){

            userModelService.changePassword( redirectAttributes, passwordChangeRequest, getLoggedInUser() );
        }

        return "redirect:/user/password";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.user.search", content = "user/user-search", activeMenu = Menu.USER_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.USER_SEARCH } )
    public String search(
            HttpServletRequest request,
            Model model
    ){

        return viewRoot;
    }

    @GetMapping( "/my-profile" )
    @TitleAndContent( title = "title.my.profile", content = "user/my-profile", activeMenu = Menu.DASHBOARD )
    @AddFrontEndLibrary( libraries = FrontEndLibrary.MY_PROFILE )
    public String myProfile(

            HttpServletRequest request,
            Model model
    ){

        userModelService.myProfileGet( model, getLoggedInUser().getUser().getId() );
        return viewRoot;
    }

    @PostMapping( "/my-profile" )
    public String getProfile(
            RedirectAttributes redirectAttributes,
            Model model,
            MyProfileRequest myProfileRequest,
            BindingResult bindingResult
    ) {

        userModelService.addProfile( myProfileRequest, getLoggedInUser().getUser().getId(), redirectAttributes );

        return "redirect:/";
    }
}
