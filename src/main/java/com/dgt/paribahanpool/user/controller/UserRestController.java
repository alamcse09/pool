package com.dgt.paribahanpool.user.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.user.model.UserSearchResponse;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.user.service.UserValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping( "/api/user" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class UserRestController extends BaseRestController {

    private final UserService userService;
    private final UserValidationService userValidationService;

    @GetMapping( "/search" )
    public DataTablesOutput<UserSearchResponse> search(
            DataTablesInput dataTablesInput
    ){

        return userService.search( dataTablesInput );
    }

    @PostMapping( "/enable/{id}" )
    public RestResponse enableUser(
            @PathVariable( "id" ) Long id
    ){

        RestValidationResult restValidationResult = userValidationService.enable( id, getLoggedInUser() );

        if( restValidationResult.getSuccess() ) {

            userService.enable(id, true);
            return RestResponse.builder().success(true).message( "success.user.enable.success" ).build();
        }
        else{
            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @PostMapping( "/disable/{id}" )
    public RestResponse disableUser(
            @PathVariable( "id" ) Long id
    ){

        RestValidationResult restValidationResult = userValidationService.disable( id, getLoggedInUser() );

        if( restValidationResult.getSuccess() ) {

            userService.enable(id, false);
            return RestResponse.builder().success(true).message( "success.user.disable.success" ).build();
        }
        else{
            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }
}
