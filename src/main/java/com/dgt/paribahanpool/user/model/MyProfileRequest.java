package com.dgt.paribahanpool.user.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;

@Data
public class MyProfileRequest {

    private String nameEn;

    private String nameBn;

    private MultipartFile[] profilePicFiles;

    List<DocumentMetadata> profilePicDocuments = Collections.EMPTY_LIST;

    private MultipartFile[] signFiles;

    List<DocumentMetadata> signDocuments = Collections.EMPTY_LIST;
}
