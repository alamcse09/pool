package com.dgt.paribahanpool.user.model;

import com.dgt.paribahanpool.validation.annotation.EqualPasswords;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@EqualPasswords( field1 = "newPassword", field2 = "retypePassword" )
public class PasswordChangeRequest {

    @NotBlank( message = "{validation.common.required}" )
    private String currentPassword;

    @NotBlank( message = "{validation.common.required}" )
    private String newPassword;

    @NotBlank( message = "{validation.common.required}" )
    private String retypePassword;
}
