package com.dgt.paribahanpool.user.model;

import com.dgt.paribahanpool.enums.EmployeeType;
import com.dgt.paribahanpool.ministry.model.Ministry;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;

@Data
@Entity
@Table(
        name = "public_user_info",
        uniqueConstraints = {
                @UniqueConstraint( columnNames = { "service_id" }, name = "uk_public_user_info_service_id" )
        }
)
public class PublicUserInfo {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( name = "service_id", nullable = false )
    private String serviceId;

    @Column( name = "name", nullable = false )
    private String name;

    @Column( name = "name_en" )
    private String nameEn;

    @Column( name = "designation", nullable = false )
    private String designation;

    @Column( name = "designation_en", nullable = false )
    private String designationEn;

    @Column( name = "workplace" )
    private String workplace;

    @Column( name = "workplace_en" )
    private String workplaceEn;

    @Column( name = "address" )
    private String address;

    @Column( name = "address_en" )
    private String addressEn;

    @Email
    @Column( name = "email" )
    private String email;

    public EmployeeType employeeType;

    @Column( name = "phone_no" )
    private String phoneNo;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "ministry", foreignKey = @ForeignKey( name = "fk_public_user_ministry" ) )
    private Ministry ministry;
}
