package com.dgt.paribahanpool.user.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PublicUserInfoResponse {

    private String name;
    private String nameEn;
    private String designation;
    private String designationEn;
    private String workplace;
    private String workplaceEn;
    private String email;
    private String phoneNo;
}
