package com.dgt.paribahanpool.user.model;

import com.dgt.paribahanpool.validation.annotation.EqualPasswords;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualPasswords
public class ResetPasswordRequest {

    @Min( value = 1, message = "{validation.common.min}" )
    @NotNull( message = "{validation.common.required}" )
    private Long userId;

    @NotBlank( message = "{validation.common.required}" )
    private String token;

    @NotBlank( message = "{validation.common.required}" )
    private String password;

    @NotBlank( message = "{validation.common.required}" )
    private String retypePassword;
}
