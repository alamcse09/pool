package com.dgt.paribahanpool.user.model;

import com.dgt.paribahanpool.enums.EmployeeType;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.ministry.model.MinistryResponse;
import com.dgt.paribahanpool.validation.annotation.EqualPasswords;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@EqualPasswords( field1 = "password", field2 = "retypePassword" )
public class SignupAddRequest {

    public EmployeeType employeeType;

    @NotNull( message = "{validation.common.required}" )
    public String serviceId;

    public String nameEn;

    @NotNull( message = "{validation.common.required}" )
    public String nameBn;

    @NotNull( message = "{validation.common.required}" )
    public String designation;

    @NotNull( message = "{validation.common.required}" )
    public String designationEn;

    public String workplace;

    public String workplaceEn;

    public String address;

    public String addressEn;

    @Email( message = "{validation.common.email}" )
    @NotBlank( message = "{validation.common.required}" )
    public String email;

    public String phoneNo;

    @Size( min = 4, message = "{validation.common.size}" )
    @NotBlank( message = "{validation.common.required}" )
    public String password;

    @Size( min = 4, message = "{validation.common.size}" )
    @NotBlank( message = "{validation.common.required}" )
    public String retypePassword;

    private List<MinistryResponse> ministryResponseList;

    private Long ministryId;

    public UserType userType = UserType.PUBLIC;
}
