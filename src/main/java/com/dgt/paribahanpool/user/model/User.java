package com.dgt.paribahanpool.user.model;

import com.dgt.paribahanpool.employee.model.Employee;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.mechanic.model.Mechanic;
import com.dgt.paribahanpool.organogram.model.Organogram;
import com.dgt.paribahanpool.userlevel.model.UserLevel;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Entity
@Table( name = "user" )
@SQLDelete( sql = "UPDATE user set is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class User extends AuditableEntity implements Serializable{

    @Id
    @Column( name = "id" )
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @NotNull
    @Column( name = "username", columnDefinition = "varchar(100)")
    private String username;

    @NotNull
    @Column( name = "password" )
    private String password;

    @NotNull
    @Column( name = "user_type", nullable = false, columnDefinition = "int default 0")
    private UserType userType;

    @Column( name = "is_enabled" )
    private Boolean isEnabled = true;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn( name = "public_user_info_id", foreignKey = @ForeignKey( name = "fk_user_public_user_info_id" ) )
    private PublicUserInfo publicUserInfo;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn( name = "employee_id", foreignKey = @ForeignKey( name = "fk_user_employee_id" ) )
    private Employee employee;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne
    @JoinColumn( name = "user_level_id", foreignKey = @ForeignKey( name = "fk_user_user_level_id" ) )
    private UserLevel userLevel;

    @Column( name = "profile_pic_document_group_id" )
    private Long profilePicDocGroupId;

    @Column( name = "sign_document_group_id" )
    private Long signDocGroupId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn( name = "mechanic_id", foreignKey = @ForeignKey( name = "fk_user_mechanic_id" ) )
    private Mechanic mechanic;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "organogram_id", foreignKey = @ForeignKey( name = "fk_user_organogram_id" ) )
    private Organogram organogram;
}
