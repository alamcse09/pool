package com.dgt.paribahanpool.user.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserResponse {

    private Long id;
    private String nameBn;
    private String nameEn;
    private String phoneNumber;
    private String designation;
    private String email;
    private String workplace;
    private Object metadata;
}
