package com.dgt.paribahanpool.user.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserSearchResponse {

    private Long id;
    private PublicUserInfoResponse publicUserInfo;
}
