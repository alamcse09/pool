package com.dgt.paribahanpool.user.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table( name = "user_token" )
public class UserToken {

    @Id
    @Column( name = "user_id" )
    private Long userId;

    @Column( name = "token" )
    private String token;

    @CreatedDate
    @Column( name = "created_at" )
    private LocalDateTime createdAt;

    @Column( name = "expired_at" )
    private LocalDateTime expiredAt;
}
