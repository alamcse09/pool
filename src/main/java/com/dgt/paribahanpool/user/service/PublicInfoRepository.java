package com.dgt.paribahanpool.user.service;

import com.dgt.paribahanpool.user.model.PublicUserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PublicInfoRepository extends JpaRepository<PublicUserInfo, Long> {

    Optional<PublicUserInfo> findByServiceId( String serviceId );
}
