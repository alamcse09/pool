package com.dgt.paribahanpool.user.service;

import com.dgt.paribahanpool.ministry.model.Ministry;
import com.dgt.paribahanpool.ministry.service.MinistryService;
import com.dgt.paribahanpool.user.model.PublicUserInfo;
import com.dgt.paribahanpool.user.model.SignupAddRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class PublicInfoService {

    private final PublicInfoRepository publicInfoRepository;
    private final MinistryService ministryService;

    public PublicUserInfo save( PublicUserInfo publicUserInfo ){

        return publicInfoRepository.save( publicUserInfo );
    }

    @Transactional
    public PublicUserInfo getPublicInfoFromSignupAddRequest( SignupAddRequest signupAddRequest ){

        PublicUserInfo publicUserInfo = new PublicUserInfo();

        publicUserInfo.setServiceId( signupAddRequest.getServiceId() );
        publicUserInfo.setName( signupAddRequest.getNameBn() );
        publicUserInfo.setNameEn( signupAddRequest.getNameEn() );
        publicUserInfo.setDesignation( signupAddRequest.getDesignation() );
        publicUserInfo.setDesignationEn( signupAddRequest.getDesignationEn() );
        publicUserInfo.setAddress( signupAddRequest.getAddress() );
        publicUserInfo.setAddressEn( signupAddRequest.getAddressEn() );
        publicUserInfo.setEmail( signupAddRequest.getEmail() );
        publicUserInfo.setPhoneNo( signupAddRequest.getPhoneNo() );
        publicUserInfo.setEmployeeType( signupAddRequest.getEmployeeType() );

        if( signupAddRequest.getMinistryId() != null && signupAddRequest.getMinistryId() > 0 ) {

            Ministry ministry = ministryService.getReference(signupAddRequest.getMinistryId());
            publicUserInfo.setMinistry(ministry);
            publicUserInfo.setWorkplace( ministry.getNameBn() );
            publicUserInfo.setWorkplaceEn( ministry.getNameEn() );
        }

        return save( publicUserInfo );
    }

    public Optional<PublicUserInfo> findByServiceId( String serviceId ) {

        return publicInfoRepository.findByServiceId( serviceId );
    }

    public Long getNumberOfPublicUser() {

        return publicInfoRepository.count();
    }
}
