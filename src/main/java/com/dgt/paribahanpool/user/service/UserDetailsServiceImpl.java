package com.dgt.paribahanpool.user.service;

import com.dgt.paribahanpool.exceptions.UserDisabledException;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException {

        Optional<User> userOptional = userRepository.findByUsername( username );

        Set<GrantedAuthority> grantedAuthoritySet = null;

        if( userOptional.isPresent() ){

            User user = userOptional.get();

            if( !user.getIsEnabled() ){

                throw new UserDisabledException( "error.login.disabled" );
            }

            grantedAuthoritySet =
                    user.getUserLevel().getRoleSet()
                            .stream()
                            .map( role -> new SimpleGrantedAuthority( role.getName() ) )
                            .collect( Collectors.toSet() );

            return new UserPrincipal( user, grantedAuthoritySet );
        }
        else{

            throw new UsernameNotFoundException( "error.login.bad-credential" );
        }
    }
}
