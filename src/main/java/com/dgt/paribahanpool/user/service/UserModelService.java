package com.dgt.paribahanpool.user.service;

import com.dgt.paribahanpool.exceptions.EmailDataBadRequestException;
import com.dgt.paribahanpool.ministry.model.MinistryResponse;
import com.dgt.paribahanpool.ministry.service.MinistryService;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.*;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class UserModelService {

    private final UserService userService;
    private final MvcUtil mvcUtil;
    private final UserTokenService userTokenService;
    private final PublicInfoService publicInfoService;
    private final MinistryService ministryService;

    public void resetPassword( Model model, Long userId, String token ) {

        model.addAttribute( "resetPasswordRequest", new ResetPasswordRequest() );
        model.addAttribute( "userId", userId );
        model.addAttribute( "token", token );
    }

    @Transactional
    public void resetPassword( RedirectAttributes redirectAttributes, ResetPasswordRequest resetPasswordRequest) {

        userService.updatePassword( resetPasswordRequest.getUserId(), resetPasswordRequest.getPassword() );
        userTokenService.invalidateToken( resetPasswordRequest.getUserId(), resetPasswordRequest.getToken() );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.user.reset-password");
    }

    public void forgetPassword( RedirectAttributes redirectAttributes, String email ) throws EmailDataBadRequestException {

        userService.addForgetPasswordToken( email );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.user.forget-password.token-generated" );
    }

    public void changePassword( RedirectAttributes redirectAttributes, PasswordChangeRequest passwordChangeRequest, UserPrincipal loggedInUser) {

        userService.updatePassword( loggedInUser.getUser().getId(), passwordChangeRequest.getNewPassword() );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.user.change-password" );
    }

    public void changePasswordGet(Model model) {

        model.addAttribute( "passwordChangeRequest", new PasswordChangeRequest() );
    }

    public void addSignupGet( Model model ){

        List<MinistryResponse> ministryResponseList = ministryService.findAllMinistryResponse();
        SignupAddRequest signupAddRequest = new SignupAddRequest();
        signupAddRequest.setMinistryResponseList( ministryResponseList );

        model.addAttribute( "signupAddRequest", signupAddRequest );
    }

    public void addSignupPost( RedirectAttributes redirectAttributes, SignupAddRequest signupAddRequest ){

        PublicUserInfo publicUserInfo = publicInfoService.getPublicInfoFromSignupAddRequest( signupAddRequest );
        userService.getUserFromSignupAddRequest( signupAddRequest, publicUserInfo );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.user.signup" );
    }

    public void myProfileGet( Model model, Long userId ) {

        MyProfileRequest myProfileRequest = userService.getMyProfileRequestFromPublicUser( userId );
        model.addAttribute("myProfileRequest", myProfileRequest );
    }

    public void addProfile( MyProfileRequest myProfileRequest, Long id, RedirectAttributes redirectAttributes ) {

        userService.saveProfileInfo( id, myProfileRequest );

        mvcUtil.addSuccessMessage( redirectAttributes, "success.user.profile-update" );
    }
}
