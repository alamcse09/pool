package com.dgt.paribahanpool.user.service;

import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.mechanic.model.Mechanic;
import com.dgt.paribahanpool.user.model.User;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends DataTablesRepository<User, Long>,JpaRepository<User,Long> {

    Optional<User> findByUsername( String username );

    Optional<User> findByUsernameAndIsDeleted(String username, Boolean isDeleted);

    List<User> findByUserType( UserType userType );

    Optional<User> findByMechanicAndIsDeleted(Mechanic mechanic, Boolean isDeleted);

    @Query( "select distinct user from User user inner join user.userLevel userLevel inner join userLevel.roleSet roles where roles.id = ?1" )
    List<User> findByRoleId( Long roleId );
}
