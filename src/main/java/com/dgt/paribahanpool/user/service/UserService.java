package com.dgt.paribahanpool.user.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.config.AppProperties;
import com.dgt.paribahanpool.document.model.Document;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.employee.model.Employee;
import com.dgt.paribahanpool.employee.model.EmployeeAddRequest;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.exceptions.EmailDataBadRequestException;
import com.dgt.paribahanpool.mechanic.model.Mechanic;
import com.dgt.paribahanpool.mechanic.model.MechanicAddRequest;
import com.dgt.paribahanpool.notification.model.EmailData;
import com.dgt.paribahanpool.notification.model.NotificationSender;
import com.dgt.paribahanpool.role.model.Role;
import com.dgt.paribahanpool.user.model.*;
import com.dgt.paribahanpool.userlevel.service.UserLevelService;
import com.dgt.paribahanpool.util.NameResponse;
import com.dgt.paribahanpool.vehicle.model.JobAssistantDetails;
import com.dgt.paribahanpool.vehicle.model.MechanicInChargeDetails;
import com.dgt.paribahanpool.vehicle.model.WorkshopManagerDetails;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.dgt.paribahanpool.user.service.UserSpecification.filterByUserType;
import static com.dgt.paribahanpool.user.service.UserSpecification.findByEmployeeId;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired } )
public class UserService {

    private final UserLevelService userLevelService;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserTokenService userTokenService;
    private final AppProperties appProperties;
    private final NotificationSender notificationSender;
    private final DocumentService documentService;

    public User save( User user ){

        return userRepository.save( user );
    }

    private List<User> findByUserType( UserType userType ) {

        return userRepository.findByUserType( userType );
    }

    public Optional<User> findByMechanic(Mechanic mechanic) {

        return userRepository.findByMechanicAndIsDeleted( mechanic , false );
    }

    @Transactional( propagation = Propagation.REQUIRED )
    public Optional<User> findById(Long id ){

        return userRepository.findById( id );
    }

    @Transactional( propagation = Propagation.REQUIRED )
    public Optional<User> findByUsername( String username ){

        return userRepository.findByUsername( username );
    }

    @Transactional( propagation = Propagation.REQUIRED )
    public Optional<User> findByUsername( String username, Boolean isDeleted ){

        return userRepository.findByUsernameAndIsDeleted( username, isDeleted );
    }

    @Transactional
    public void updatePassword( Long userId, String newPassword ) {

        Optional<User> userOptional = findById( userId );
        if( userOptional.isPresent() ){

            User user = userOptional.get();
            user.setPassword( passwordEncoder.encode( newPassword ) );
            save( user );
        }
    }

    @Transactional
    public void addForgetPasswordToken(String email) throws EmailDataBadRequestException {

        Optional<User> userOptional = findByUsername( email, false );

        if( userOptional.isPresent() ){

            User user = userOptional.get();
            UserToken userToken = userTokenService.save( user.getId(), LocalDateTime.now().plusMinutes( appProperties.getTokentimeout() ) );

            sendResetPasswordTokenMail( email, user.getId(), userToken.getToken(), appProperties.getTokentimeout() );
        }
    }

    private void sendResetPasswordTokenMail( String email, Long id, String token, Integer passwordTokenTimeout ) throws EmailDataBadRequestException {

        Map<String,Object> params = new HashMap<>();

        params.put( "userId", id );
        params.put( "token", token );
        params.put( "timeout", passwordTokenTimeout );

        EmailData emailData = EmailData.builder()
                .to( new String[]{ email } )
                .isHtml( true )
                .templatePath( "email/user/forgot-password" )
                .params( params )
                .subject( "Reset Password - Paribahan Pool" )
                .build();

        notificationSender.sendEmail( emailData );
    }

    @Transactional
    public void getUserFromSignupAddRequest(SignupAddRequest signupAddRequest, PublicUserInfo publicUserInfo){

        User user = new User();

        user.setUsername( signupAddRequest.getEmail() ) ;
        user.setPassword( passwordEncoder.encode( signupAddRequest.getPassword() ) );
        user.setUserType( signupAddRequest.getUserType() );
        user.setPublicUserInfo( publicUserInfo );

        //TODO remove hardcoded user level
        user.setUserLevel( userLevelService.findById( AppConstants.PUBLIC_USER_ID ).get() );

        save( user );
    }

    @Transactional
    public User createUserFromEmployeeService( EmployeeAddRequest employeeAddRequest ) {

        User user = new User();

        user.setUsername( employeeAddRequest.getEmail() );

        if( !StringUtils.isBlank( employeeAddRequest.getPassword() ) && employeeAddRequest.getPassword().length() >= 4 ) {

            String encodedPassword = passwordEncoder.encode(employeeAddRequest.getPassword() );
            user.setPassword( encodedPassword );
        }

        user.setUserType( UserType.SYSTEM_USER );
        user.setUserLevel( userLevelService.findById( 2L ).get() );

        return save( user );
    }

    @Transactional
    public User editUserFromEmployeeService( Employee employee, EmployeeAddRequest employeeAddRequest) {

        List<User> userList = userRepository.findAll( findByEmployeeId( employee.getId() ) );

        if( userList.size() == 1 ){

            User user = userList.get( 0 );

            user.setUsername( employeeAddRequest.getEmail() );

            if( !StringUtils.isBlank( employeeAddRequest.getPassword() ) && employeeAddRequest.getPassword().length() >= 4 ) {

                String encodedPassword = passwordEncoder.encode(employeeAddRequest.getPassword() );
                user.setPassword( encodedPassword );
            }

            return save( user );
        }

        return new User();
    }

    @Transactional
    public DataTablesOutput<UserSearchResponse> search(DataTablesInput dataTablesInput) {

        Specification<User> filterByPublicUserSpecification = filterByUserType( UserType.PUBLIC );
        return userRepository.findAll( dataTablesInput, null, filterByPublicUserSpecification, this::getUserSearchResponseFromUser );
    }

    private UserSearchResponse getUserSearchResponseFromUser( User user ) {

        PublicUserInfo publicUserInfo = user.getPublicUserInfo();
        if( publicUserInfo != null ) {

            PublicUserInfoResponse publicUserInfoResponse = PublicUserInfoResponse.builder()
                    .designation( publicUserInfo.getDesignation() )
                    .designationEn( publicUserInfo.getDesignationEn() )
                    .name( publicUserInfo.getName() )
                    .nameEn( publicUserInfo.getNameEn() )
                    .phoneNo( publicUserInfo.getPhoneNo() )
                    .email( publicUserInfo.getEmail() )
                    .workplace( publicUserInfo.getWorkplace() )
                    .workplaceEn( publicUserInfo.getWorkplaceEn() )
                    .build();

            return UserSearchResponse.builder()
                    .id( user.getId() )
                    .publicUserInfo( publicUserInfoResponse )
                    .build();
        }

        return null;
    }

    public void enable( Long id, Boolean isEnabled ) {

        Optional<User> userOptional = userRepository.findById( id );
        if( userOptional.isPresent() ){

            User user = userOptional.get();

            user.setIsEnabled( isEnabled );
            save( user );
        }
    }

    public Set<Role> getRoleSetFromUser(String username ){

        Optional<User> userOptional = findByUsername( username );

        User user = userOptional.get();

        return user.getUserLevel().getRoleSet();
    }

    @Transactional
    public List<UserResponse> getUserResponseList( UserType userType ) {

        List<User> userList = findByUserType( userType );

        List<UserResponse> userResponseList = new ArrayList<>();

        if( userType == UserType.SYSTEM_USER ) {

            userResponseList = getEmployeeUserResponseFromUser(userList);
        }

        else if( userType == UserType.MECHANIC ) {

            userResponseList = getMechanicUserResponseFromUser(userList);
        }

        return userResponseList;
    }

    private List<UserResponse> getEmployeeUserResponseFromUser(List<User> userList ) {

        return userList
                .stream()
                .map( this::getUserResponseFromEmployee )
                .collect( Collectors.toList() );
    }

    private List<UserResponse> getMechanicUserResponseFromUser(List<User> userList ) {

        return userList
                .stream()
                .map( this::getUserResponseFromMechanic )
                .collect( Collectors.toList() );
    }

    private UserResponse getUserResponseFromMechanic( User user ) {

        return UserResponse.builder()
                .id( user.getId() )
                .nameBn( user.getMechanic().getName() + " (মেকানিক)" )
                .nameEn( user.getMechanic().getName() + " (Mechanic)" )
                .phoneNumber( user.getMechanic().getPhoneNumber() )
                .email( user.getMechanic().getEmail() )
                .build();
    }

    private UserResponse getUserResponseFromEmployee( User user ) {

        return UserResponse.builder()
                .id( user.getId() )
                .nameBn( user.getEmployee().getName() + " (" + user.getEmployee().getDesignation().getNameBn() + ')' )
                .nameEn( user.getEmployee().getNameEn() + " (" + user.getEmployee().getDesignation().getNameEn() + ')' )
                .phoneNumber( user.getEmployee().getMobileNumber() )
                .designation( user.getEmployee().getDesignation().getNameBn() )
                .email( user.getEmployee().getEmail() )
                ///TODO change office ministry
                .workplace( user.getEmployee().getOfficeMinistry() )
                .build();
    }


    public List<User> getUserListFromParticipantIdList( Long[] participantUserIdList ) {

        Iterable<Long> participantUserIdIterable = Arrays.asList(participantUserIdList);
        return userRepository.findAllById( participantUserIdIterable );
    }

    @Transactional
    public MyProfileRequest getMyProfileRequestFromPublicUser( Long userId ) {

        MyProfileRequest myProfileRequest = new MyProfileRequest();

        User user = findById( userId ).get();

        if( user.getUserType() == UserType.PUBLIC ){

            PublicUserInfo publicUserInfo = user.getPublicUserInfo();

            myProfileRequest.setNameBn( publicUserInfo.getName() );
            myProfileRequest.setNameEn( publicUserInfo.getNameEn() );
        }
        else{

            Employee employee = user.getEmployee();

            myProfileRequest.setNameBn( employee.getName() );
            myProfileRequest.setNameEn( employee.getNameEn() );
        }

        if ( user.getProfilePicDocGroupId() != null && user.getProfilePicDocGroupId() > 0 ) {

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( user.getProfilePicDocGroupId() );
            myProfileRequest.setProfilePicDocuments( documentMetadataList );
        }

        if ( user.getSignDocGroupId() != null && user.getSignDocGroupId() > 0 ) {

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( user.getSignDocGroupId() );
            myProfileRequest.setSignDocuments( documentMetadataList );
        }

        return myProfileRequest;
    }

    @Transactional
    public void saveProfileInfo( Long id, MyProfileRequest myProfileRequest ) {

        User user = findById( id ).get();

        if( user.getUserType() == UserType.PUBLIC ){

            PublicUserInfo publicUserInfo = user.getPublicUserInfo();

            publicUserInfo.setName( myProfileRequest.getNameBn() );
            publicUserInfo.setNameEn( myProfileRequest.getNameEn() );
        }
        else{

            Employee employee = user.getEmployee();

            employee.setName( myProfileRequest.getNameBn() );
            employee.setNameEn( myProfileRequest.getNameEn() );
        }

        if( user.getProfilePicDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( myProfileRequest.getProfilePicFiles(), Collections.EMPTY_MAP );
            user.setProfilePicDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            Long documentGroupId = user.getProfilePicDocGroupId();
            documentService.sofDeleteById( documentGroupId );
            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( myProfileRequest.getProfilePicFiles(), Collections.EMPTY_MAP );
            user.setProfilePicDocGroupId( documentUploadedResponse.getDocGroupId() );
        }

        if( user.getSignDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( myProfileRequest.getSignFiles(), Collections.EMPTY_MAP );
            user.setSignDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( myProfileRequest.getSignFiles(), user.getSignDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        save( user );
    }

    public List<MechanicInChargeDetails> getMechanicInChargeUser() {

        List<User> userList = findByUserType( UserType.SYSTEM_USER );

        return userList.stream().filter( user -> user.getEmployee()!= null)
                .filter( user -> user.getEmployee().getDesignation() != null)
                .filter( user ->  user.getEmployee().getDesignation().getId() == AppConstants.MECHANIC_IN_CHARGE_DESIGNATION_ID)
                .map( this::getMechanicInChargeDetailsFromUser )
                .collect(Collectors.toList());

    }

    public List<WorkshopManagerDetails> getWorkshopManagerUser() {

        List<User> userList = findByUserType( UserType.SYSTEM_USER );

        return userList.stream().filter( user -> user.getEmployee()!= null)
                .filter( user -> user.getEmployee().getDesignation() != null)
                .filter( user ->  user.getEmployee().getDesignation().getId() == AppConstants.WORKSHOP_MANAGER_DESIGNATION_ID)
                .map( this::getWorkshopManagerDetailsFromUser )
                .collect(Collectors.toList());

    }

    private WorkshopManagerDetails getWorkshopManagerDetailsFromUser(User user) {

        WorkshopManagerDetails workshopManagerDetails = new WorkshopManagerDetails();

        workshopManagerDetails.setUserId( user.getId() );
        workshopManagerDetails.setName(
                NameResponse.builder()
                        .nameBn( user.getEmployee().getName() )
                        .nameEn( user.getEmployee().getNameEn() )
                        .build()
        );

        return workshopManagerDetails;
    }

    private MechanicInChargeDetails getMechanicInChargeDetailsFromUser(User user) {

        MechanicInChargeDetails mechanicInChargeDetails = new MechanicInChargeDetails();

        mechanicInChargeDetails.setUserId( user.getId() );
        mechanicInChargeDetails.setName( NameResponse.builder()
                        .nameEn( user.getEmployee().getNameEn() )
                        .nameBn( user.getEmployee().getName() )
                .build());

        return mechanicInChargeDetails;
    }

    @Transactional
    public User createUserFromMechanic( MechanicAddRequest mechanicAddRequest ) {

        User user = new User();

        user.setUsername( mechanicAddRequest.getEmail() );

        String encodedPassword = passwordEncoder.encode( mechanicAddRequest.getPassword() );
        user.setPassword( encodedPassword );

        user.setUserType( UserType.MECHANIC );
        user.setUserLevel( userLevelService.findById( 4L ).get() );

        return save( user );
    }

    public List<User> findUserByRoleId(Long roleId) {

        return userRepository.findByRoleId( roleId );
    }

    public List<JobAssistantDetails> getJobAssistantUser() {

        List<User> userList = findByUserType( UserType.SYSTEM_USER );

        return userList.stream().filter( user -> user.getEmployee()!= null)
                .filter( user -> user.getEmployee().getDesignation() != null)
                .filter( user ->  user.getEmployee().getDesignation().getId() == AppConstants.JOB_ASSISTANT_DESIGNATION_ID)
                .map( this::getJobAssistantDetailsFromUser )
                .collect(Collectors.toList());
    }

    private JobAssistantDetails getJobAssistantDetailsFromUser(User user) {

        JobAssistantDetails jobAssistantDetails = new JobAssistantDetails();

        jobAssistantDetails.setUserId( user.getId() );
        jobAssistantDetails.setName(
                NameResponse.builder()
                        .nameBn( user.getEmployee().getName() )
                        .nameEn( user.getEmployee().getNameEn() )
                        .build()
        );

        return jobAssistantDetails;
    }
}
