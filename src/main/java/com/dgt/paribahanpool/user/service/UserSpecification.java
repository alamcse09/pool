package com.dgt.paribahanpool.user.service;

import com.dgt.paribahanpool.employee.model.Employee_;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.model.User_;
import org.springframework.data.jpa.domain.Specification;

public class UserSpecification {

    public static Specification<User> filterByUserType(UserType userType ){

        if( userType == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(User_.USER_TYPE ), userType );
    }

    public static Specification<User> findByEmployeeId( Long employeeId ){

        if( employeeId == null )
            return Specification.where( null );

        return ( root, query, criteriaBuilder ) -> criteriaBuilder.equal( root.get( User_.EMPLOYEE ).get(Employee_.ID ), employeeId );
    }
}
