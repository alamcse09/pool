package com.dgt.paribahanpool.user.service;

import com.dgt.paribahanpool.user.model.UserToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserTokenRepository extends JpaRepository<UserToken, Long> {

    Optional<UserToken> findByUserIdAndToken(Long userId, String token);
}
