package com.dgt.paribahanpool.user.service;

import com.dgt.paribahanpool.user.model.UserToken;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class UserTokenService {

    private final UserTokenRepository userTokenRepository;

    private UserToken save(UserToken userToken) {

        return userTokenRepository.save( userToken );
    }

    public Optional<UserToken> findByUserIdAndToken(Long userId, String token ){

        return userTokenRepository.findByUserIdAndToken( userId, token );
    }

    @Transactional
    public UserToken save( Long userId, String token, LocalDateTime expiredAt ){

        Optional<UserToken> userTokenOptional = userTokenRepository.findById( userId );
        UserToken userToken = null;

        if( userTokenOptional.isPresent() ){
            userToken = userTokenOptional.get();
        }
        else{
            userToken = new UserToken();
        }

        userToken.setToken( token );
        userToken.setUserId( userId );
        userToken.setExpiredAt( expiredAt );

        return userTokenRepository.save( userToken );
    }

    @Transactional
    public UserToken save( Long userId, LocalDateTime expiredAt ){

        UUID uuid = UUID.randomUUID();
        return save( userId, uuid.toString(), expiredAt );
    }

    public void invalidateToken(Long userId, String token) {

        Optional<UserToken> userTokenOptional = findByUserIdAndToken( userId, token );

        if( userTokenOptional.isPresent() ){

            UserToken userToken = userTokenOptional.get();

            userToken.setExpiredAt( LocalDateTime.now() );

            save( userToken );
        }
    }
}
