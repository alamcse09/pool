package com.dgt.paribahanpool.user.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.*;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class UserValidationService {

    private final UserService userService;
    private final PublicInfoService publicInfoService;
    private final UserTokenService userTokenService;
    private final MvcUtil mvcUtil;
    private final PasswordEncoder passwordEncoder;

    public Boolean resetPassword( RedirectAttributes redirectAttributes, Long userId, String token ) {

        Optional<UserToken> userTokenOptional = userTokenService.findByUserIdAndToken( userId, token );

        if( !userTokenOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "error.user.reset-password.invalid-token" );
            return false;
        }
        else{

            UserToken userToken = userTokenOptional.get();

            if( userToken.getExpiredAt().isBefore( LocalDateTime.now() ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.user.reset-password.token-expired" );
                return false;
            }
        }

        return true;
    }

    public Boolean handleBindingResultResetPassword( Model model, BindingResult bindingResult, ResetPasswordRequest resetPasswordRequest ) {

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "resetPasswordRequest" )
                .object( resetPasswordRequest )
                .build();

        Boolean isValid = mvcUtil.handleBindingResult(
                bindingResult,
                model,
                params
        );

        return isValid;
    }

    public Boolean handleBindingResultSignupRequest( Model model, BindingResult bindingResult, SignupAddRequest signupAddRequest ) {

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "signupAddRequest" )
                .object( signupAddRequest )
                .build();

        Boolean isValid = mvcUtil.handleBindingResult(
                bindingResult,
                model,
                params
        );

        return isValid;
    }

    public Boolean signup( Model model, SignupAddRequest signupAddRequest ){

        model.addAttribute( "signupAddRequest", signupAddRequest );

        Optional<User> optionalUser = userService.findByUsername( signupAddRequest.getEmail() );
        if( optionalUser.isPresent() ){

            mvcUtil.addErrorMessage( model, "validation.signup.sameUserName" );
            return false;
        }

        Optional<PublicUserInfo> publicUserInfoOptional = publicInfoService.findByServiceId( signupAddRequest.getServiceId() );
        if( publicUserInfoOptional.isPresent() ){

            mvcUtil.addErrorMessage( model, "validation.signup.sameServiceId" );
            return false;
        }

        return true;
    }

    public Boolean forgetPassword(RedirectAttributes redirectAttributes, String email) {

        Optional<User> userOptional = userService.findByUsername( email, false );

        if( !userOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    public Boolean handleBindingResultChangePassword( Model model, BindingResult bindingResult, PasswordChangeRequest passwordChangeRequest) {

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "passwordChangeRequest" )
                .object( passwordChangeRequest )
                .title( "title.user.change.password" )
                .content( "user/change-password" )
                .activeMenu( Menu.CHANGE_PASSWORD )
                .build();

        Boolean isValid = mvcUtil.handleBindingResult(
                bindingResult,
                model,
                params
        );

        return isValid;
    }

    public Boolean changePassword( RedirectAttributes redirectAttributes, PasswordChangeRequest passwordChangeRequest, UserPrincipal loggedInUser ) {

        if( !passwordEncoder.matches( passwordChangeRequest.getCurrentPassword(), loggedInUser.getUser().getPassword() ) ){

            mvcUtil.addErrorMessage( redirectAttributes, "error.user.change-password.current-pass-mismatch" );
            return false;
        }

        return true;
    }

    public RestValidationResult enable( Long id, UserPrincipal loggedInUser ) {

        if( id.equals( loggedInUser.getUser().getId() ) ){
            return RestValidationResult.builder().success( false ).message( "error.user.enable.himself" ).build();
        }
        return RestValidationResult.valid();
    }

    public RestValidationResult disable( Long id, UserPrincipal loggedInUser ) {

        if( id.equals( loggedInUser.getUser().getId() ) ){
            return RestValidationResult.builder().success( false ).message( "error.user.disable.himself" ).build();
        }
        return RestValidationResult.valid();
    }
}
