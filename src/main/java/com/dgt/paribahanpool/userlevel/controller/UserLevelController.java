package com.dgt.paribahanpool.userlevel.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.userlevel.model.UserLevel;
import com.dgt.paribahanpool.userlevel.model.UserLevelAddRequest;
import com.dgt.paribahanpool.userlevel.service.UserLevelModelService;
import com.dgt.paribahanpool.userlevel.service.UserLevelValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/user-level" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class UserLevelController extends MVCController {

    private final UserLevelModelService userLevelModelService;
    private final UserLevelValidationService userLevelValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.user-level.add", content = "user-level/user-level-add", activeMenu = Menu.USER_LEVEL_ADD)
    @AddFrontEndLibrary( libraries = {FrontEndLibrary.USER_LEVEL_ADD } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add User Level form" );
        userLevelModelService.addUserLevel( model, getLoggedInUser().getRoleIds(), getLoggedInUser().getUser().getUserLevel().getLevel() );
        return viewRoot;
    }

    @PostMapping( "/add" )
    public String addUser(
            Model model,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            @Valid UserLevelAddRequest userLevelAddRequest,
            BindingResult bindingResult
    ){

        if( !userLevelValidationService.handleBindingResultForAddFormPost( model, bindingResult, userLevelAddRequest, getLoggedInUser().getRoleIds() ) ){

            return viewRoot;
        }

        if( userLevelValidationService.addUserLevelPost( userLevelAddRequest, redirectAttributes, getLoggedInUser().getRoleIds(), getLoggedInUser().getUser().getUserLevel().getLevel() ) ) {

            UserLevel userLevel = userLevelModelService.addUserLevelPost( userLevelAddRequest, redirectAttributes );
            log( LogEvent.USER_LEVEL_ADD, userLevel.getId(), "User Level added", request );
            return "redirect:/user-level/search";
        }

        return "redirect:/user-level/add";
    }

    @GetMapping( "/edit/{id}" )
    @TitleAndContent( title = "title.user-level.add", content = "user-level/user-level-add", activeMenu = Menu.USER_LEVEL_ADD)
    @AddFrontEndLibrary( libraries = {FrontEndLibrary.USER_LEVEL_ADD } )
    public String editUserLevel(

            HttpServletRequest request,
            RedirectAttributes redirectAttributes,
            Model model,
            @PathVariable( "id" ) Long id
    ) {

        if( userLevelValidationService.editUserLevel( redirectAttributes, id, getLoggedInUser().getUser().getUserLevel().getLevel() ) ){

            userLevelModelService.editUserLevel( model, id, getLoggedInUser().getRoleIds(), getLoggedInUser().getUser().getUserLevel().getLevel() );
            return viewRoot;
        }

        return "redirect:/user-level/search";
    }

    @PostMapping( "/edit/{userLevelId}" )
    public String editUserLevel(
            Model model,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            @Valid UserLevelAddRequest userLevelAddRequest,
            BindingResult bindingResult,
            @PathVariable( "userLevelId" ) Long userLevelId

    ) {

        if( !userLevelValidationService.handleBindingResultForAddFormPost( model, bindingResult, userLevelAddRequest, getLoggedInUser().getRoleIds() ) ){

            return viewRoot;
        }

        if( !userLevelValidationService.addUserLevelPost( userLevelAddRequest, redirectAttributes, getLoggedInUser().getRoleIds(), getLoggedInUser().getUser().getUserLevel().getLevel() ) ){

            return "redirect:/user-level/edit/{userLevelId}";
        }

        if( userLevelValidationService.editUserLevel( redirectAttributes, userLevelAddRequest.getId(), getLoggedInUser().getUser().getUserLevel().getLevel() ) ){

            UserLevel userLevel = userLevelModelService.editUserLevelPost( userLevelAddRequest, redirectAttributes );
            log( LogEvent.USER_LEVEL_EDIT, userLevel.getId(), "User Level Edited", request );
        }

        return "redirect:/user-level/search";

    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.user-level.search", content = "user-level/user-level-search", activeMenu = Menu.USER_LEVEL_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.USER_LEVEL_SEARCH } )
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "User Level Search page called" );
        return viewRoot;
    }
}
