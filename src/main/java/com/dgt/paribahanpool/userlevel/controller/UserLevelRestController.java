package com.dgt.paribahanpool.userlevel.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.userlevel.model.UserLevelSearchResponse;
import com.dgt.paribahanpool.userlevel.service.UserLevelService;
import com.dgt.paribahanpool.userlevel.service.UserLevelValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping( "/api/user-level" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class UserLevelRestController extends BaseRestController {

    private final UserLevelService userLevelService;
    private final UserLevelValidationService userLevelValidationService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<UserLevelSearchResponse> searchUserLevel(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return userLevelService.searchForDataTable( dataTablesInput, getLoggedInUser().getUser().getUserLevel().getLevel() );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id
    ) throws NotFoundException {

        RestValidationResult restValidationResult = userLevelValidationService.delete( id );
        if( restValidationResult.getSuccess() ){

            userLevelService.delete( id );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{
            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

}
