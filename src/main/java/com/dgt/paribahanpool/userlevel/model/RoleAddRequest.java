package com.dgt.paribahanpool.userlevel.model;

import lombok.Builder;
import lombok.Data;

import javax.persistence.Column;

@Data
@Builder
public class RoleAddRequest {

    private Long id;
    private String name;
    private String roleGroupEn;
    private String roleGroupBn;
    private String roleTextEn;
    private String roleTextBn;
}
