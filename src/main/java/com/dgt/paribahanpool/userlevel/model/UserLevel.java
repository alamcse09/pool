package com.dgt.paribahanpool.userlevel.model;

import com.dgt.paribahanpool.role.model.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table( name = "user_level" )
@ToString( exclude = { "roleSet" } )
@EqualsAndHashCode( exclude = { "roleSet" } )
public class UserLevel {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "name_en" )
    private String nameEn;

    @Column( name = "name_bn" )
    private String nameBn;

    @ManyToMany
    @JoinTable(
            name = "user_level_role",
            joinColumns = @JoinColumn( name = "user_level_id" ),
            inverseJoinColumns = @JoinColumn( name = "role_id" ),
            foreignKey = @ForeignKey( name = "fk_user_level_id_user_level_role" ),
            inverseForeignKey = @ForeignKey( name = "fk_role_id_user_level_role" )
    )
    private Set<Role> roleSet;

    @Column( name = "level" )
    private Long level;
}
