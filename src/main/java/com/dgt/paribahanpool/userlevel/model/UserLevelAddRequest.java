package com.dgt.paribahanpool.userlevel.model;

import com.dgt.paribahanpool.role.model.Role;
import com.dgt.paribahanpool.role.model.RoleViewResponse;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
public class UserLevelAddRequest {

    private Long id;

    @NotBlank( message = "{validation.common.required}" )
    private String nameEn;

    @NotBlank( message = "{validation.common.required}" )
    private String nameBn;

    private Map<String, List<RoleViewResponse>> roleGroupsOfCurrentUser;

    @NotNull( message = "{validation.common.required}" )
    @Size( min = 1, message = "{validation.user-level.role-required}" )
    private Long[] roleId;

    private Set<RoleAddRequest> roleSet;

    @NotNull( message = "{validation.common.required}" )
    private Long levelOfUserLevel;
}
