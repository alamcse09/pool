package com.dgt.paribahanpool.userlevel.model;

import lombok.Data;

@Data
public class UserLevelResponse extends UserLevelSearchResponse{
}
