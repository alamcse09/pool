package com.dgt.paribahanpool.userlevel.model;

import lombok.Data;

@Data
public class UserLevelSearchResponse {

    public Long id;
    public String nameEn;
    public String nameBn;
    public Long level;
}
