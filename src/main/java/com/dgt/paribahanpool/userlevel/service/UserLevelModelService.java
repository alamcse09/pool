package com.dgt.paribahanpool.userlevel.service;

import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.userlevel.model.UserLevel;
import com.dgt.paribahanpool.userlevel.model.UserLevelAddRequest;
import com.dgt.paribahanpool.util.MvcUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Set;

import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class UserLevelModelService {

    private final MvcUtil mvcUtil;
    private final UserLevelService userLevelService;
    private final UserService userService;

    public void addUserLevel( Model model, Set<Long> roleIds, Long currentUserLevelLevel ){

        UserLevelAddRequest userLevelAddRequest = userLevelService.getUserLevelAddRequestForLoggedInUser( roleIds, currentUserLevelLevel );

        model.addAttribute( "userLevelAddRequest", userLevelAddRequest );
    }

    public UserLevel addUserLevelPost( UserLevelAddRequest userLevelAddRequest, RedirectAttributes redirectAttributes ){

        UserLevel userLevel = userLevelService.save( userLevelAddRequest );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.user-level.add" );

        return userLevel;
    }

    @Transactional
    public void editUserLevel( Model model, Long userLevelId, Set<Long> roleIds, Long currentUserLevel ){

        UserLevelAddRequest userLevelAddRequest = userLevelService.getUserLevelAddRequestFromUserLevel( userLevelId, roleIds, currentUserLevel );
        model.addAttribute( "userLevelAddRequest", userLevelAddRequest );
    }

    public UserLevel editUserLevelPost( UserLevelAddRequest userLevelAddRequest, RedirectAttributes redirectAttributes ){

        UserLevel userLevel = userLevelService.save( userLevelAddRequest );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.user-level.edit" );

        return userLevel;
    }
}
