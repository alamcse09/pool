package com.dgt.paribahanpool.userlevel.service;

import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.userlevel.model.UserLevel;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserLevelRepository extends DataTablesRepository<UserLevel,Long>, JpaRepository<UserLevel,Long> {
}
