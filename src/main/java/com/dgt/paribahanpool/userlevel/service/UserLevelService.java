package com.dgt.paribahanpool.userlevel.service;

import com.dgt.paribahanpool.designation.model.DesignationAddRequest;
import com.dgt.paribahanpool.role.model.Role;
import com.dgt.paribahanpool.role.model.RoleViewResponse;
import com.dgt.paribahanpool.role.service.RoleService;
import com.dgt.paribahanpool.userlevel.model.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class UserLevelService {

    private final UserLevelRepository userLevelRepository;
    private final RoleService roleService;

    public Optional<UserLevel> findById(Long id ){

        return userLevelRepository.findById( id );
    }

    public UserLevel findReference(Long userLevelId) {

        return userLevelRepository.getById( userLevelId );
    }

    public Boolean existById(Long id ) {

        return userLevelRepository.existsById( id );
    }

    public void delete( Long id ){

        userLevelRepository.deleteById( id );
    }

    @Transactional
    public UserLevel save( UserLevelAddRequest userLevelAddRequest ){

        UserLevel userLevel = new UserLevel();

        if( userLevelAddRequest.getId() != null ){

            userLevel = findById( userLevelAddRequest.getId() ).get();
        }

        userLevel = getUserLevelFromUserLevelAddReqeust( userLevel, userLevelAddRequest );

        return save( userLevel );
    }

    public UserLevel save( UserLevel userLevel ){

        return userLevelRepository.save( userLevel );
    }

    public UserLevel getUserLevelFromUserLevelAddReqeust( UserLevel userLevel, UserLevelAddRequest userLevelAddRequest ){

        userLevel.setNameBn( userLevelAddRequest.getNameBn() );
        userLevel.setNameEn( userLevelAddRequest.getNameEn() );

        Set<Role> roleSet = roleService.getRoleSetFromRoleIdArr( userLevelAddRequest.getRoleId() );

        userLevel.setRoleSet( roleSet );

        userLevel.setLevel( userLevelAddRequest.getLevelOfUserLevel() );

        return userLevel;
    }

    public UserLevelSearchResponse getUserLevelSearchResponseFromUserLevel( UserLevel userLevel ){

        UserLevelSearchResponse userLevelSearchResponse = new UserLevelSearchResponse();

        userLevelSearchResponse.setId( userLevel.getId() );
        userLevelSearchResponse.setNameBn( userLevel.getNameBn() );
        userLevelSearchResponse.setNameEn( userLevel.getNameEn() );
        userLevelSearchResponse.setLevel( userLevel.getLevel() );

        return userLevelSearchResponse;
    }

    public DataTablesOutput<UserLevelSearchResponse> searchForDataTable( DataTablesInput dataTablesInput, Long currentUserLevel ){

        Specification<UserLevel> filterByLevelOfUserLevel = UserLevelSpecification.filterByLevelOfUserLevel( currentUserLevel );
        return userLevelRepository.findAll( dataTablesInput, null, filterByLevelOfUserLevel, this::getUserLevelSearchResponseFromUserLevel );
    }

    public List<UserLevel> getUserLevelListByLevel( Long level ){

        Specification<UserLevel> filterByLevelOfUserLevel = UserLevelSpecification.filterByLevelOfUserLevel( level );
        return userLevelRepository.findAll( filterByLevelOfUserLevel );
    }

    public List<UserLevelResponse> getUserLevelResponseListByLevel(Long level ){

        return getUserLevelListByLevel( level ).stream()
                .map( this::getUserLevelSearchResponseFromUserLevel )
                .map( userLevelSearchResponse -> {

                    UserLevelResponse userLevelResponse = new UserLevelResponse();
                    BeanUtils.copyProperties( userLevelSearchResponse, userLevelResponse );
                    return userLevelResponse;
                })
                .collect( Collectors.toList() );
    }

    public UserLevelAddRequest getUserLevelAddRequestFromUserLevel( Long userLevelId, Set<Long> roleIds, Long currentUserLevel ){

        Optional<UserLevel> userLevelOptional = findById( userLevelId );

        if( userLevelOptional.isPresent() ) {

            UserLevel userLevel = userLevelOptional.get();

            Map<String, List<RoleViewResponse>> roleGroups = getRoleGroupsOfCurrentUser( roleIds );
            Set<RoleAddRequest> roleAddRequests = getRoleAddRequestFromRoleSet( userLevel.getRoleSet() );
            Long[] roleIdArr = roleService.getRoleIdArrFromRoleSet( userLevel.getRoleSet() );

            UserLevelAddRequest userLevelAddRequest = new UserLevelAddRequest();

            userLevelAddRequest.setId( userLevel.getId() );
            userLevelAddRequest.setNameBn( userLevel.getNameBn() );
            userLevelAddRequest.setNameEn( userLevel.getNameEn() );
            userLevelAddRequest.setRoleGroupsOfCurrentUser( roleGroups );
            userLevelAddRequest.setRoleSet( roleAddRequests );
            userLevelAddRequest.setRoleId( roleIdArr );
            userLevelAddRequest.setLevelOfUserLevel( userLevel.getLevel() );

            return userLevelAddRequest;
        }

        return null;
    }

    private Set<RoleAddRequest> getRoleAddRequestFromRoleSet( Set<Role> roleSet ) {

        return roleSet.stream()
                .map( this::getRoleAddRequestFromRole )
                .collect( Collectors.toSet() );
    }

    private RoleAddRequest getRoleAddRequestFromRole( Role role ) {

        return RoleAddRequest.builder()
                .id( role.getId() )
                .name( role.getName() )
                .roleGroupBn( role.getRoleGroupBn() )
                .roleGroupEn( role.getRoleGroupEn() )
                .roleTextBn( role.getRoleTextBn() )
                .roleTextEn( role.getRoleTextEn() )
                .build();
    }


    public UserLevelAddRequest getUserLevelAddRequestForLoggedInUser( Set<Long> roleIds, Long currentUserLevel ){

        UserLevelAddRequest userLevelAddRequest = new UserLevelAddRequest();

        Map<String, List<RoleViewResponse>> roleGroupsOfCurrentUser = getRoleGroupsOfCurrentUser( roleIds );

        userLevelAddRequest.setRoleGroupsOfCurrentUser( roleGroupsOfCurrentUser );
        userLevelAddRequest.setLevelOfUserLevel( currentUserLevel + 1L );

        return userLevelAddRequest;
    }

    public Map<String, List<RoleViewResponse>> getRoleGroupsOfCurrentUser( Set<Long> ids ){

        Set<Role> roles = roleService.getRoles( ids );

        Map<String, List<RoleViewResponse>> roleGroupsOfCurrentUser = getRoleGroupsFromRoleSet( roles );

        return roleGroupsOfCurrentUser;
    }

    public Map<String, List<RoleViewResponse>> getRoleGroupsFromRoleSet( Set<Role> roles ){

        Map<String, List<RoleViewResponse>> roleGroupsOfCurrentUser = roles
                .stream()
                .map( this::getRoleViewResponseFromRole )
                .collect( Collectors.groupingBy(roleViewResponse -> roleViewResponse.getRoleGroupEn() ) );

        return roleGroupsOfCurrentUser;
    }

    private RoleViewResponse getRoleViewResponseFromRole( Role role ){

        return RoleViewResponse.builder()
                .id( role.getId() )
                .roleGroupBn( role.getRoleGroupBn() )
                .roleGroupEn( role.getRoleGroupEn() )
                .roleTextBn(role.getRoleTextBn() )
                .roleTextEn(role.getRoleTextEn() )
                .name( role.getName() )
                .build();
    }
}
