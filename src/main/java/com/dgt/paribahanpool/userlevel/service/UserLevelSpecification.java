package com.dgt.paribahanpool.userlevel.service;

import com.dgt.paribahanpool.userlevel.model.UserLevel;
import com.dgt.paribahanpool.userlevel.model.UserLevel_;
import org.springframework.data.jpa.domain.Specification;

public class UserLevelSpecification {

    public static Specification<UserLevel> filterByLevelOfUserLevel( Long currentUserLevel ){

        if( currentUserLevel == null ){

            return Specification.where( null );
        }

        return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThan( root.get(UserLevel_.LEVEL ), currentUserLevel );
    }
}
