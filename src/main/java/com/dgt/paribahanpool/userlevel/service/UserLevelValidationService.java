package com.dgt.paribahanpool.userlevel.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.role.model.RoleViewResponse;
import com.dgt.paribahanpool.role.service.RoleService;
import com.dgt.paribahanpool.userlevel.model.UserLevel;
import com.dgt.paribahanpool.userlevel.model.UserLevelAddRequest;
import com.dgt.paribahanpool.util.MvcUtil;
import com.google.common.collect.Sets;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class UserLevelValidationService {

    private final MvcUtil mvcUtil;
    private final UserLevelService userLevelService;
    private final RoleService roleService;

    public Boolean handleBindingResultForAddFormPost( Model model, BindingResult bindingResult, UserLevelAddRequest userLevelAddRequest, Set<Long> roleIds ){


        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "userLevelAddReqeust" )
                .object( userLevelAddRequest )
                .title( "title.user-level.add" )
                .content( "user-level/user-level-add" )
                .activeMenu( Menu.USER_LEVEL_ADD )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.USER_LEVEL_ADD } )
                .build();

        boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        if( !isValid ) {

            Map<String, List<RoleViewResponse>> roleGroupsOfCurrentUser = userLevelService.getRoleGroupsOfCurrentUser( roleIds );

            userLevelAddRequest.setRoleGroupsOfCurrentUser( roleGroupsOfCurrentUser );

            mvcUtil.addErrorMessage( model, "validation.user-level.role-required" );

            return false;
        }

        return true;
    }

    public Boolean higherAuthorizationValidation( UserLevelAddRequest userLevelAddRequest, RedirectAttributes redirectAttributes, Set<Long> currentUserRoleIds ){

        Set<Long> roleIdsFromRequest = new HashSet<Long>();
        Collections.addAll( roleIdsFromRequest, userLevelAddRequest.getRoleId() );

        Set<Long> roleIdsFromRequestButNotInCurrentUserRoleIds = Sets.difference( roleIdsFromRequest, currentUserRoleIds );

        if( !roleIdsFromRequestButNotInCurrentUserRoleIds.isEmpty() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.user-level.authorize" );
            return false;
        }

        return true;
    }

    public Boolean higherLevelValidation( UserLevelAddRequest userLevelAddRequest, RedirectAttributes redirectAttributes, Long currentUserLevel ){

        Long levelOfUserLevel = userLevelAddRequest.getLevelOfUserLevel();

        if( levelOfUserLevel <= currentUserLevel ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.user-level.higher-level" );
            return false;
        }

        return true;
    }

    public Boolean addUserLevelPost( UserLevelAddRequest userLevelAddRequest, RedirectAttributes redirectAttributes, Set<Long> currentUserRoleIds, Long currentUserLevel ){

        Boolean isValidLevel = higherLevelValidation( userLevelAddRequest, redirectAttributes, currentUserLevel );

        Boolean isValidRoleIds = higherAuthorizationValidation( userLevelAddRequest, redirectAttributes, currentUserRoleIds );

        if( isValidLevel && isValidRoleIds ) {

            return true;
        }

        return false;
    }

    public RestValidationResult delete( Long id ) throws NotFoundException {

        Boolean exist = userLevelService.existById( id );

        if( !exist ){

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean editUserLevel( RedirectAttributes redirectAttributes, Long userLevelId, Long loggedInLevel ) {

        Optional<UserLevel> userLevelOptional = userLevelService.findById( userLevelId );

        if( !userLevelOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;

        } else{

            UserLevel userLevel = userLevelOptional.get();

            if( userLevel.getLevel() <= loggedInLevel ){

                mvcUtil.addErrorMessage( redirectAttributes, "validation.user-level.higher-level.edit" );
                return false;
            }

            return true;
        }
    }
}
