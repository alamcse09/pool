package com.dgt.paribahanpool.util;

public class DateTimeFormatPattern {

    public static final String dateFormat_slash_ddMMyyyy = "dd/MM/yyyy";
    public static final String dateFormat_hyphen_ddMMyyyy = "dd-MM-yyyy";
    public static final String dateFormat_hyphen_yyyyMMdd = "yyyy-MM-dd";
    public static final String dateFormat_slash_yyyyMMdd = "yyyy/MM/dd";
    public static final String dateFormat_month_name_date_year = "MMM dd, yyyy";
    public static final String timeFormat_hour_min_ampm = "h:m a";
    public static final String timeFormat_hourhour_minmin_ampm = "hh:mm a";

    public static final String dateFormat_slash_ddMMyyyy_hhmma = "dd/MM/yyyy h:m a";
}
