package com.dgt.paribahanpool.util;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class MvcUtil {

    private final Environment environment;
    private final LocaleResolver localeResolver;

    public Boolean handleBindingResult(

            BindingResult bindingResult,
            Model model,
            HandleBindingResultParams params
    ){

        if( bindingResult.hasErrors() ){

            log.warn( "Request has errors, {}", bindingResult.getAllErrors() );

            addErrorMessage( model, "error.common.invalid.data" );

            if( params.getKey() != null ) {

                model.addAttribute( params.getKey(), params.getObject() );
            }

            addTitleAndContent( model, params.getTitle(), params.getContent(), params.getActiveMenu() );

            if( params.getFrontEndLibraries() != null ){

                addCssAndJsByLibraryName( model, params.getFrontEndLibraries() );
            }
            return false;
        }

        return true;
    }

    public String getCurrentLang( HttpServletRequest request ){

        return localeResolver.resolveLocale( request ).getLanguage();
    }

    public void addTitleAndContent(Model model, String title, String content, Menu activeMenu ){

        model.addAttribute( "pageTitle", title );
        model.addAttribute( "content", content );
        model.addAttribute( "activeMenu", activeMenu );
        model.addAttribute( "contextPath", environment.getProperty( "server.servlet.context-path" ) );
    }

    public void addCssAndJsByLibraryName( Model model, FrontEndLibrary frontEndLibrary ){

        String customJs = environment.getProperty( String.format( "system.lib.%s.%s", frontEndLibrary.getLibName() , "customJs" ) );
        String customCss = environment.getProperty( String.format( "system.lib.%s.%s", frontEndLibrary.getLibName(), "customCss" ) );
        String thirdPartyJs = environment.getProperty( String.format( "system.lib.%s.%s", frontEndLibrary.getLibName(), "thirdPartyJs" ) );
        String thirdPartyCss = environment.getProperty( String.format( "system.lib.%s.%s", frontEndLibrary.getLibName(), "thirdPartyCss" ) );

        model.addAttribute( "customJs", mergeWithExisting( model,"customJs", customJs ) );
        model.addAttribute( "customCss", mergeWithExisting( model,"customCss", customCss ) );
        model.addAttribute( "thirdPartyJs", mergeWithExisting( model,"thirdPartyJs", thirdPartyJs ) );
        model.addAttribute( "thirdPartyCss", mergeWithExisting( model,"thirdPartyCss", thirdPartyCss ) );
    }

    public void addCssAndJsByLibraryName( Model model, Object... libraryNames ){

        for( Object object: libraryNames ){

            FrontEndLibrary library = ( FrontEndLibrary ) object;
            addCssAndJsByLibraryName( model, library );
        }
    }

    private List<String> mergeWithExisting(Model model, String attrName, String libStr ){

        List<String> existingLibs = new ArrayList<>();

        Map<String, Object > modelMap = model.asMap();
        Object obj = modelMap.get( attrName );

        if( obj != null ){
            existingLibs = (List<String>) obj;
        }

        if( !StringUtils.isBlank( libStr ) ) {

            String[] newLibs = splitString(libStr);
            Collections.addAll(existingLibs, newLibs);
        }

        return existingLibs;
    }

    private String[] splitString(String src ) {

        if( src == null )
            return null;

        return src.split( "," );
    }

    public String getClientIP(
            HttpServletRequest request
    ) {

        //TODO need to refine this method
        String xfHeader = request.getHeader("X-Forwarded-For");

        if ( xfHeader == null ){

            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }

    public void addSuccessMessage(RedirectAttributes redirectAttributes, String message ){

        redirectAttributes.addFlashAttribute( "successMessage", message );
    }

    public void addErrorMessage( RedirectAttributes redirectAttributes, String message ){

        redirectAttributes.addFlashAttribute( "errorMessage", message );
    }

    public void addSuccessMessage( Model model, String message ) {

        model.addAttribute( "successMessage", message );
    }

    public void addErrorMessage( Model model, String message ) {

        model.addAttribute( "errorMessage", message );
    }
}
