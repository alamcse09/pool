package com.dgt.paribahanpool.util;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NameResponse {

    private String nameEn;
    private String nameBn;
}
