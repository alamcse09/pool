package com.dgt.paribahanpool.util;

import com.dgt.paribahanpool.meeting.service.MeetingService;
import com.dgt.paribahanpool.noc_application.model.NocApplication;
import com.dgt.paribahanpool.noc_application.service.NocApplicationService;
import com.dgt.paribahanpool.notification.model.NotificationMetadata;

import java.util.HashMap;
import java.util.Map;

public class Util {

    private static Character[] digits = {'০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯'};
    private static Map<String,String> enBnMonthMap = new HashMap<>();
    private static Map<String,NotificationMetadata> moduleNotificationMetadataMap = new HashMap<>();

    static {

        enBnMonthMap.put( "January", "জানুয়ারি" );
        enBnMonthMap.put( "February", "ফেব্রুয়ারি" );
        enBnMonthMap.put( "March", "মার্চ" );
        enBnMonthMap.put( "April", "এপ্রিল" );
        enBnMonthMap.put( "May", "মে" );
        enBnMonthMap.put( "June", "জুন" );
        enBnMonthMap.put( "July", "জুলাই" );
        enBnMonthMap.put( "August", "আগস্ট" );
        enBnMonthMap.put( "September", "সেপ্টেম্বর" );
        enBnMonthMap.put( "October", "অক্টোবর" );
        enBnMonthMap.put( "November", "নভেম্বর" );
        enBnMonthMap.put( "December", "ডিসেম্বর" );

        moduleNotificationMetadataMap.put( "meeting", MeetingService.getNotificationMetadata() );
        moduleNotificationMetadataMap.put( "noc", NocApplicationService.getNotificationMetadata() );
    }

    public static String getNotificationIcon( String module ){

        return moduleNotificationMetadataMap.getOrDefault( module, NotificationMetadata.builder().build() ).getIcon();
    }

    public static NotificationMetadata getNotificationMetadataForModule(String moduleName) {

        return moduleNotificationMetadataMap.get( moduleName );
    }

    public static String replaceEnMonthName( String input ){

        for( String key: enBnMonthMap.keySet() ){

            if( input.contains( key ) ){

                input = input.replace( key, enBnMonthMap.get( key ) );
            }
        }

        return input;
    }

    public static String makeDigitBangla( String input ) {

        if( input == null ) return "";

        StringBuilder str = new StringBuilder();

        for( int i=0; i<input.length(); i++ ) {

            if( Character.isDigit( input.charAt( i ) ) ) {

                str.append( digits [ Character.getNumericValue( input.charAt(i) ) ] );
            }
            else
                str.append( input.charAt(i) );
        }

        return str.toString();
    }

    public static String makeDigitBangla( Double inputDouble ) {

        String input = inputDouble.toString();

        if( input == null ) return "";

        StringBuilder str = new StringBuilder();

        for( int i=0; i<input.length(); i++ ) {

            if( Character.isDigit( input.charAt( i ) ) ) {

                str.append( digits [ Character.getNumericValue( input.charAt(i) ) ] );
            }
            else
                str.append( input.charAt(i) );
        }

        return str.toString();
    }
}
