package com.dgt.paribahanpool.util.converter;

import org.apache.commons.text.StringEscapeUtils;

import javax.persistence.AttributeConverter;

public class EscapeHtmlConverter implements AttributeConverter<String, String> {

    @Override
    public String convertToDatabaseColumn(String attribute) {
        return attribute;
    }

    @Override
    public String convertToEntityAttribute(String dbData) {

        return StringEscapeUtils.escapeHtml4( dbData );
    }
}
