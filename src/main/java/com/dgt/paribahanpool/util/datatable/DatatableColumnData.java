package com.dgt.paribahanpool.util.datatable;

import lombok.Data;

@Data
public class DatatableColumnData {

    private String data;
    private String name;
    private String searchable;
    private String orderable;
    private DatatableSearch search;
}
