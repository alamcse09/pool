package com.dgt.paribahanpool.util.datatable;

import lombok.Data;

@Data
public class DatatableOrder {

    private Integer column;
    private DatatableSortDirection dir;
}
