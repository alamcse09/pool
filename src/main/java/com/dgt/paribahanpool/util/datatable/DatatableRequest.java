package com.dgt.paribahanpool.util.datatable;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DatatableRequest {

    private Integer draw;
    private List<DatatableColumnData> columns = new ArrayList<>();
    private List<DatatableOrder> order = new ArrayList<>();
    private Long start;
    private Long length;
    private DatatableSearch search;
    private Long _;
}
