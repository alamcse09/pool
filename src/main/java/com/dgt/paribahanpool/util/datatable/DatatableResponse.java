package com.dgt.paribahanpool.util.datatable;

import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
public class DatatableResponse<T> {

    private Integer draw;

    private long recordsTotal = 0L;

    private long recordsFiltered = 0L;

    private List<T> data = Collections.emptyList();

    private String error;

    public static <T> DatatableResponse<T> build( Integer draw, List<T> data, Long recordsTotal, Long recordsFiltered ){

        DatatableResponse<T> datatableResponse = new DatatableResponse();

        datatableResponse.setData( data );
        datatableResponse.setRecordsFiltered( recordsFiltered );
        datatableResponse.setRecordsTotal( recordsTotal );
        datatableResponse.setDraw( draw );

        return datatableResponse;
    }
}
