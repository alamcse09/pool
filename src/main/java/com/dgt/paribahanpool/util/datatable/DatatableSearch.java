package com.dgt.paribahanpool.util.datatable;

import lombok.Data;

@Data
public class DatatableSearch {

    private String value;
    private String regex;
}
