package com.dgt.paribahanpool.util.model;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

@MappedSuperclass
public class VersionableEntity {

    @Version
    private Long version;
}
