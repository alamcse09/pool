package com.dgt.paribahanpool.validation;

import lombok.Data;

@Data
public class FieldErrorResponse {

    private String field;
    private String defaultMessage;
    private Object rejectedValue;
}
