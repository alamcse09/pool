package com.dgt.paribahanpool.validation;

import java.lang.reflect.Field;

public class ValidationBeanUtil {

    public static Object getProperty( Object obj, String fieldName ) throws NoSuchFieldException, IllegalAccessException {

        Field field = obj.getClass().getDeclaredField( fieldName );
        field.setAccessible( true );

        Object val = field.get( obj );

        return val;
    }
}
