package com.dgt.paribahanpool.validation;

import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class ValidationUtil {

    public static Boolean handleExceptionWithBothPropertyAndMessage(ConstraintValidatorContext context, String messageTemplate, String propertyName ){

        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate( messageTemplate )
                .addPropertyNode( propertyName )
                .addConstraintViolation();

        return false;
    }

    public static Boolean handleExceptionWithBothPropertyListAndMessage(ConstraintValidatorContext context, String messageTemplate, List<String> propertyNameList ){

        context.disableDefaultConstraintViolation();
        for( String propertyName: propertyNameList ){

            context.buildConstraintViolationWithTemplate( messageTemplate )
                    .addPropertyNode( propertyName )
                    .addConstraintViolation();
        }
        return false;
    }

    public static Boolean handleExceptionWithMessage( ConstraintValidatorContext context, String message ){

        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate( message )
                .addConstraintViolation();

        return false;
    }

    public static Boolean handleExceptionWithPropertyName( ConstraintValidatorContext context, String propertyName ){

        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate( context.getDefaultConstraintMessageTemplate() )
                .addPropertyNode( propertyName )
                .addConstraintViolation();

        return false;
    }

    public static Boolean handleException( ConstraintValidatorContext context ){

        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate( context.getDefaultConstraintMessageTemplate() )
                .addConstraintViolation();

        return false;
    }
}
