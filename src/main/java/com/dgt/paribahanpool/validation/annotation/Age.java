package com.dgt.paribahanpool.validation.annotation;

import com.dgt.paribahanpool.validation.validator.AgeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention( RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
@Constraint(validatedBy = AgeValidator.class )
public @interface Age {

    int min();
    int max();

    String message() default "{validation.error.age}";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
