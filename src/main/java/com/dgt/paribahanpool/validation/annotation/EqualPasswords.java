package com.dgt.paribahanpool.validation.annotation;

import com.dgt.paribahanpool.validation.validator.EqualPasswordsValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EqualPasswordsValidator.class )
public @interface EqualPasswords {

    String field1() default "password";
    String field2() default "retypePassword";
    String message() default "{error.user.reset-password.retype-password.not-equal}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
