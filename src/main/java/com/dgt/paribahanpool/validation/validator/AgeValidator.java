package com.dgt.paribahanpool.validation.validator;

import com.dgt.paribahanpool.exceptions.BadRequestException;
import com.dgt.paribahanpool.validation.ValidationUtil;
import com.dgt.paribahanpool.validation.annotation.Age;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;

public class AgeValidator implements ConstraintValidator<Age,Object> {

    private Integer min;
    private Integer max;

    public void initialize( Age age ){

        this.min = age.min();
        this.max = age.max();
    }

    @Override
    public boolean isValid( Object value, ConstraintValidatorContext context ) {

        try {

            if( !( value instanceof LocalDate ) && !( value instanceof LocalDateTime ) && !( value instanceof Integer ) ){

                throw new BadRequestException( "{invalid.data.type}" );
            }

            if( value instanceof LocalDate ){

                LocalDate sourceDate = (LocalDate) value;
                LocalDate now = LocalDate.now();

                Period period = Period.between( sourceDate, now );

                if( min != null && period.getYears() < min ){
                    return false;
                }
                else if( max != null && period.getYears() > max ){
                    return false;
                }
            }
            else if( value instanceof LocalDateTime ){

                LocalDateTime sourceDateTime = (LocalDateTime) value;
                LocalDateTime now = LocalDateTime.now();

                Long year = sourceDateTime.until( now, ChronoUnit.YEARS );

                if( min != null && year < min ){
                    return false;
                }
                else if( max != null && year > max ){
                    return false;
                }
            }
            else if( value instanceof Integer ){

                Integer year = (Integer) value;

                if( min != null && year < min ){
                    return false;
                }
                else if( max != null && year > max ){
                    return false;
                }
            }
        }
        catch (BadRequestException e ) {

            ValidationUtil.handleExceptionWithMessage( context, e.getMessage() );
        }

        return true;
    }
}
