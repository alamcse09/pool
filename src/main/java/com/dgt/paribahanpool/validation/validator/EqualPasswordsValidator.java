package com.dgt.paribahanpool.validation.validator;

import com.dgt.paribahanpool.validation.ValidationBeanUtil;
import com.dgt.paribahanpool.validation.ValidationUtil;
import com.dgt.paribahanpool.validation.annotation.EqualPasswords;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
public class EqualPasswordsValidator implements ConstraintValidator<EqualPasswords, Object> {

    private String field1;
    private String field2;

    public void initialize( EqualPasswords annotation ){

        this.field1 = annotation.field1();
        this.field2 = annotation.field2();
    }

    @Override
    public boolean isValid( Object value, ConstraintValidatorContext context ) {

        try {
            Object field1Value = ValidationBeanUtil.getProperty( value, field1 );
            Object field2Value = ValidationBeanUtil.getProperty( value, field2 );

            if ( field1Value == null || field2Value == null )
                return ValidationUtil.handleExceptionWithMessage(context, "One of the password field is null");

            if ( !field1Value.equals( field2Value ) )
                return false;

            return true;
        }
        catch ( IllegalAccessException e ) {

            log.error( "Can't access property during validation checking" );
            return ValidationUtil.handleExceptionWithMessage( context, "{field1} or {field2} is not accessible" );

        } catch ( NoSuchFieldException e ) {

            log.error( "Annotation has wrong field name property during validation checking" );
            return ValidationUtil.handleExceptionWithMessage( context, "{field1} or {field12} is not a valid property for this request" );
        }
    }
}
