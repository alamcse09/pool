package com.dgt.paribahanpool.vehicle.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.EmailDataBadRequestException;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.vehicle.model.*;
import com.dgt.paribahanpool.vehicle.service.VehicleModelService;
import com.dgt.paribahanpool.vehicle.service.VehicleRequisitionApplicaitonValidaitonService;
import com.dgt.paribahanpool.vehicle.service.VehicleRequisitionApplicationService;
import com.dgt.paribahanpool.vehicle.service.VehicleValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/vehicle" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired } )
public class VehicleController extends MVCController {

    private final VehicleModelService vehicleModelService;
    private final VehicleValidationService vehicleValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.vehicle.add", content = "vehicle/add", activeMenu = Menu.VEHICLE_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.VEHICLE_ADD_FORM, FrontEndLibrary.BRAND_MODEL_ADD } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering Add vehicle form" );
        vehicleModelService.addVehicleGet( model );
        return viewRoot;
    }

    @PostMapping( value = "/add" )
    public String addVehicle(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid VehicleAddRequest vehicleAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ) throws EmailDataBadRequestException {

        if( !vehicleValidationService.handleBindingResultForAddFormPost( model, bindingResult, vehicleAddRequest ) ) {

            return viewRoot;
        }

        if( vehicleValidationService.addVehiclePost( redirectAttributes, vehicleAddRequest ) ) {

            Vehicle vehicle = vehicleModelService.addVehiclePost(vehicleAddRequest, model);
            log( LogEvent.VEHICLE_ADDED, vehicle.getId(), "Vehicle Added", request );
        }

        return "redirect:/vehicle/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.vehicle.search", content = "vehicle/vehicle-search", activeMenu = Menu.VEHICLE_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.VEHICLE_SEARCH })
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Vehicle Search page called" );
        return viewRoot;
    }

    @GetMapping( "/{id}" )
    @TitleAndContent( title = "title.vehicle.add", content = "vehicle/details" )
    public String viewVehicle(

            @PathVariable( "id" ) Long id,
            HttpServletRequest request,
            Model model
    ){

        model.addAttribute( "id", id );
        return viewRoot;
    }

    @GetMapping( "/edit/{vehicleId}" )
    @TitleAndContent( title = "title.vehicle.edit", content = "vehicle/add", activeMenu = Menu.VEHICLE_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.VEHICLE_ADD_FORM, FrontEndLibrary.BRAND_MODEL_ADD })
    public String edit(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable("vehicleId")  Long vehicleId
    ){
        log.debug( "Rendering edit vehicle form" );

        if( vehicleValidationService.editVehicle( redirectAttributes, vehicleId ) ) {

            vehicleModelService.editVehicle( model, vehicleId );
            return viewRoot;
        }

        return "redirect:/vehicle/search";
    }

    @GetMapping( "/auction" )
    @TitleAndContent( title = "title.vehicle.auction", content = "vehicle/vehicle-auction", activeMenu = Menu.VEHICLE_AUCTION )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.VEHICLE_AUCTION } )
    public String auction(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering vehicle auction page" );
        return viewRoot;
    }

    @GetMapping( "/auction/{id}" )
    @TitleAndContent( title = "title.vehicle.auction.view", content = "vehicle/vehicle-auction-view", activeMenu = Menu.VEHICLE_AUCTION_VIEW )
//    @AddFrontEndLibrary( libraries = { FrontEndLibrary.VEHICLE_AUCTION_VIEW } )
    public String auctionView(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request,
            RedirectAttributes redirectAttributes,
            Model model
    ){

        if( vehicleValidationService.isValidVehicle( redirectAttributes, id ) ){

            vehicleModelService.getAuctionViewResponseFromVehicleDisposition( model, id );
        }

        log.debug( "Rendering vehicle auction view page" );
        return viewRoot;
    }

    @GetMapping( "/requisition" )
    @TitleAndContent( title = "title.vehicle.requisition", content = "vehicle/requisition-add", activeMenu = Menu.VEHICLE_USER )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.REQUISITION_ADD, FrontEndLibrary.GEOLOCATION_ADD } )
    public String vehicleRequisition(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Rendering vehicle requisition page" );
        vehicleModelService.requisition( model, getLoggedInUser() );
        return viewRoot;
    }

    @PostMapping( "/requisition" )
    public String requisitionPost(

            Model model,
            RedirectAttributes redirectAttributes,
            @Valid VehicleRequisitionRequest vehicleRequisitionRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        log.debug( "vehicle requisition post called" );

        if( vehicleValidationService.requisition( model, bindingResult, vehicleRequisitionRequest ) ){

            vehicleModelService.addRequisition( redirectAttributes, vehicleRequisitionRequest, getLoggedInUser() );
            log( LogEvent.VEHICLE_REQUISITION_ADDED, vehicleRequisitionRequest.getId(), "Vehicle Requisition Added", request );
            return "redirect:/vehicle/my-requisition/search";

        }

        return viewRoot;
    }

    @GetMapping( "/requisition/search" )
    @TitleAndContent( title = "title.vehicle.requisition.search", content = "vehicle/requisition-search", activeMenu = Menu.VEHICLE_USER )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.VEHICLE_REQUISITION_SEARCH } )
    public String requisitionSearch(
            HttpServletRequest request,
            Model model
    ){

        return viewRoot;
    }

    @GetMapping( "/my-requisition/search" )
    @TitleAndContent( title = "title.vehicle.requisition.search", content = "vehicle/my-requisition-search", activeMenu = Menu.VEHICLE_USER )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.VEHICLE_MY_REQUISITION_SEARCH } )
    public String myRequisitionSearch(
            HttpServletRequest request,
            Model model
    ){

        return viewRoot;
    }

    @GetMapping( "/requisition/{id}" )
    @TitleAndContent( title = "title.vehicle.requisition.view", content = "vehicle/requisition-view", activeMenu = Menu.VEHICLE_USER )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.JQUERY_PRINT, FrontEndLibrary.REQUISITION_VIEW } )
    public String viewRequisition(

            @PathVariable( "id" ) Long id,
            HttpServletRequest request,
            Model model
    ){

        vehicleModelService.viewRequisition( model, id, getLoggedInUser() );
        return viewRoot;
    }

    @GetMapping( "/assign-user/{id}")
    @TitleAndContent( title = "title.vehicle.requisition", content = "replace-application/replace-vehicle",activeMenu = Menu.REPLACE_VEHICLE_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.REPLACE_VEHICLE_ADD } )
    public String replace(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable("id") Long id
    ){
        if( vehicleValidationService.validNewVehicleRequisitionRequest(redirectAttributes, id) ) {

            vehicleModelService.vehicleRequisitionGet( model, id );
            return viewRoot;
        }
        return "redirect:/vehicle/search";
    }

    @PostMapping( "/take-action" )
    @TitleAndContent( title = "title.vehicle.search", content = "vehicle/vehicle-search", activeMenu = Menu.VEHICLE_SEARCH )
    public String takeAction(
            @RequestParam( "id" ) Long id,
            @RequestParam( "action" ) Long actionId,
            RequisitionAdditionalActionData requisitionAdditionalActionData,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ) throws Exception {

        if( vehicleValidationService.takeAction( redirectAttributes, id, actionId, getLoggedInUser() ) ){

            vehicleModelService.takeAction( redirectAttributes, id, actionId, requisitionAdditionalActionData, getLoggedInUser() );
        }
        else{

            return "redirect:/vehicle/requisition/" + id;
        }

        return "redirect:/vehicle/requisition/search";
    }

    @PostMapping( "/auction/committee-decision" )
    public String addCommitteeDecision(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid VehicleAuctionCommitteeDecisionAddRequest vehicleAuctionCommitteeDecisionAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
            ){

        if( !vehicleValidationService.handleBindingResultForAuctionInfo( model, bindingResult ) ) {

            return viewRoot;
        }

        if( vehicleValidationService.addCommitteeDecision( redirectAttributes, vehicleAuctionCommitteeDecisionAddRequest) ) {

            vehicleModelService.addCommitteeDecision(vehicleAuctionCommitteeDecisionAddRequest, redirectAttributes );
            log( LogEvent.VEHICLE_AUCTION_COMMITTEE_DECISION, vehicleAuctionCommitteeDecisionAddRequest.getCommitteeDecisionId(), "Vehicle Auction Committee Decision Added", request );
        }

        return "redirect:/vehicle/auction";
    }

    @PostMapping( "/auction/buyer-info" )
    public String addBuyerInfo(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid VehicleAuctionBuyerInfoAddRequest vehicleAuctionBuyerInfoAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !vehicleValidationService.handleBindingResultForAuctionInfo( model, bindingResult ) ) {

            return viewRoot;
        }

        if( vehicleValidationService.addBuyerInfo( redirectAttributes, vehicleAuctionBuyerInfoAddRequest) ) {

            vehicleModelService.addBuyerInfo( vehicleAuctionBuyerInfoAddRequest, redirectAttributes );
            log( LogEvent.VEHICLE_AUCTION_BUYER_INFO, vehicleAuctionBuyerInfoAddRequest.getBuyerFromVehicleId(), "Vehicle Auction Buyer Info Added", request );
        }

        return "redirect:/vehicle/auction";
    }

    @GetMapping( "/requisition/print/{id}" )
    public String printRequisition(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ){

        vehicleModelService.viewRequisition( model, id, getLoggedInUser() );
        return "vehicle/component/vehicle-requisition-print";
    }
}
