package com.dgt.paribahanpool.vehicle.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.driver.model.DriverAddRequest;
import com.dgt.paribahanpool.enums.OwnerType;
import com.dgt.paribahanpool.exceptions.BadRequestException;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.training.model.TrainingAttendanceAddRequest;
import com.dgt.paribahanpool.vehicle.model.*;
import com.dgt.paribahanpool.vehicle.service.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping( "/api/vehicle" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired } )
public class VehicleRestController extends BaseRestController {

    private final VehicleService vehicleService;
    private final VehicleValidationService vehicleValidationService;
    private final VehicleRequisitionApplicationService vehicleRequisitionApplicationService;
    private final VehicleServiceApplicationService vehicleServiceApplicationService;
    private final VehicleRequisitionApplicaitonValidaitonService vehicleRequisitionApplicaitonValidaitonService;
    private final VehicleServiceApplicationValidationService vehicleServiceApplicationValidationService;
    private final BrandValidationService brandValidationService;
    private final ModelService modelService;
    private final VehicleOwnerService vehicleOwnerService;
    private final JobService jobService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<VehicleSearchResponse> searchVehicle(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return vehicleService.searchForDatatable( dataTablesInput );
    }

    @GetMapping(
            value = "/service/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<VehicleServiceSearchResponse> serviceSearchVehicle(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return vehicleServiceApplicationService.searchForDatatable( dataTablesInput );
    }

    @GetMapping(
            value = "/service/my-search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<VehicleServiceSearchResponse> serviceMySearchVehicle(
            DataTablesInput dataTablesInput
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return vehicleServiceApplicationService.searchForMyDatatable( dataTablesInput, getLoggedInUser().getUser().getId() );
    }

    @DeleteMapping( "/service/{id}" )
    public RestResponse deleteService(
            @PathVariable( "id" ) Long id
    ) throws NotFoundException {

        RestValidationResult restValidationResult = vehicleServiceApplicationValidationService.delete( id );
        if( restValidationResult.getSuccess() ){

            vehicleServiceApplicationService.delete( id );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{
            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @GetMapping(
            value = "/auction",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<VehicleAuctionResponse> auction(
            DataTablesInput dataTablesInput,
            HttpServletRequest request
    ){

        log.debug( "Request params for vehicle auction, {}", dataTablesInput );
        return vehicleService.searchForAuction( dataTablesInput, request );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id
    ) throws NotFoundException {

        RestValidationResult restValidationResult = vehicleValidationService.delete( id );
        if( restValidationResult.getSuccess() ){

            vehicleService.delete( id );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{
            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @GetMapping( "/requisition/search" )
    public DataTablesOutput<VehicleRequisitionSearchResponse> requisitionSearch(
            DataTablesInput dataTablesInput,
            HttpServletRequest request
    ){
        log.debug( "Request params for vehicle requisition, {}", dataTablesInput );
        return vehicleRequisitionApplicationService.search( dataTablesInput, request );
    }

    @GetMapping( "/my-requisition/search" )
    public DataTablesOutput<VehicleRequisitionSearchResponse> myRequisitionSearch(
            DataTablesInput dataTablesInput,
            HttpServletRequest request
    ){
        log.debug( "Request params for vehicle requisition, {}", dataTablesInput );
        return vehicleRequisitionApplicationService.search( dataTablesInput, request, getLoggedInUser().getUser().getId() );
    }

    @DeleteMapping( "/requisition/{id}")
    public RestResponse deleteRequisition(
            @PathVariable( "id" ) Long id
    ) throws NotFoundException {
        RestValidationResult restValidationResult = vehicleRequisitionApplicaitonValidaitonService.delete( id );
        if( restValidationResult.getSuccess() ){

            vehicleRequisitionApplicationService.delete( id );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{
            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @GetMapping( "/auction/id/{committeeDecisionId}" )
    public RestResponse getFromCommitteeDecisionId(
            @PathVariable( "committeeDecisionId" ) Long committeeDecisionId
    ) throws NotFoundException {

        RestValidationResult restValidationResult = vehicleValidationService.findByCommitteeDecisionId( committeeDecisionId );
        if( restValidationResult.getSuccess() ){

            VehicleAuctionCommitteeDecisionAddRequest vehicleAuctionCommitteeDecisionAddRequest = vehicleService.getCommitteeDecisionAddRequestFromCommitteeDecisionId( committeeDecisionId );
            return RestResponse.builder().success( true ).payload( vehicleAuctionCommitteeDecisionAddRequest ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @GetMapping(
            value = "/find-by-brand/{id}"
    )
    public RestResponse findByParentId(
            @PathVariable( "id" ) Long id
    ) throws NotFoundException {

        RestValidationResult restValidationResult = brandValidationService.isExist( id );

        if( restValidationResult.getSuccess() ) {

            List<ModelViewResponse> modelViewResponseList = modelService.getAllModelViewResponseByBrandId( id );
            return RestResponse.builder().success( true ).payload(modelViewResponseList).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @GetMapping( "/vehicle-owner-count/{vehicleOwner}/{geolocationId}" )
    public RestResponse getVehicleCountFromOwnerType(
            @PathVariable( "vehicleOwner" ) Integer ownerTypeVal,
            @PathVariable( "geolocationId" ) Long geolocationId
            ) throws NotFoundException {

        RestValidationResult restValidationResult = vehicleOwnerService.findByOwnerType( ownerTypeVal, getLoggedInUser().getUser(), geolocationId );

        if( restValidationResult.getSuccess() ){

            Integer vehicleCount = vehicleOwnerService.findVehicleCountOfOwnerType( ownerTypeVal, getLoggedInUser().getUser(), geolocationId );
            return RestResponse.builder().success( true ).payload( vehicleCount ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @GetMapping( "/assigned-vehicle-list/{vehicleOwner}/{geolocationId}" )
    public RestResponse getAssignedVehicle(
            @PathVariable( "vehicleOwner" ) Integer ownerTypeVal,
            @PathVariable( "geolocationId" ) Long geolocationId
    ) throws NotFoundException {

        RestValidationResult restValidationResult = vehicleOwnerService.findByOwnerType( ownerTypeVal, getLoggedInUser().getUser(), geolocationId );

        if( restValidationResult.getSuccess() ){

            List<VehicleViewResponse> vehicleViewResponseList = vehicleOwnerService.findAllAssignedVehicle( ownerTypeVal, getLoggedInUser().getUser(), geolocationId  );
            return RestResponse.builder().success( true ).payload( vehicleViewResponseList ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }

    @PostMapping( "/job-done" )
    public RestResponse updateDoneStatus(
            @Valid JobDoneRequest jobDoneRequest,
            BindingResult bindingResult
    ) throws BadRequestException {

        handleBindingResult( bindingResult );

        if( vehicleServiceApplicationValidationService.isValidActionTaker( jobDoneRequest.getJobId(), getLoggedInUser(), jobDoneRequest.getStateId() ) ){

            jobService.markJobDone( jobDoneRequest.getJobId(), jobDoneRequest.getIsPresent() );

            return RestResponse.builder().success( true ).message( "success.common.action.success" ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( "error.common.action.not-allowed" ).build();
        }
    }
}
