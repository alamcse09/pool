package com.dgt.paribahanpool.vehicle.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.vehicle.model.VehicleServiceAddRequest;
import com.dgt.paribahanpool.vehicle.model.VehicleServiceApplication;
import com.dgt.paribahanpool.vehicle.model.VehicleServiceAdditionalActionData;
import com.dgt.paribahanpool.vehicle.service.VehicleServiceApplicationModelService;
import com.dgt.paribahanpool.vehicle.service.VehicleServiceApplicationValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/vehicle-service" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class VehicleServiceApplicationController extends MVCController {

    private final VehicleServiceApplicationModelService vehicleServiceApplicationModelService;
    private final VehicleServiceApplicationValidationService vehicleServiceApplicationValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.vehicle.service.add", content = "vehicle/service/vehicle-service-add", activeMenu = Menu.VEHICLE_SERVICE_ADD )
    @AddFrontEndLibrary( libraries = {FrontEndLibrary.VEHICLE_SERVICE_ADD} )
    public String addPage(
            Model model,
            HttpServletRequest request
    ){

        vehicleServiceApplicationModelService.addPage( model );
        return viewRoot;
    }

    @PostMapping( "/add" )
    public String addServiceAppPost(
            @Valid VehicleServiceAddRequest vehicleServiceAddRequest,
            BindingResult bindingResult,
            RedirectAttributes redirectAttributes,
            Model model,
            HttpServletRequest request
    ){
        if( !vehicleServiceApplicationValidationService.handleBindingResultForAddFormPost( model, bindingResult, vehicleServiceAddRequest ) ) {

            return viewRoot;
        }

        if( vehicleServiceApplicationValidationService.addVehiclePost( redirectAttributes, vehicleServiceAddRequest ) ) {

            VehicleServiceApplication vehicleServiceApplication = vehicleServiceApplicationModelService.addVehiclePost( vehicleServiceAddRequest, model );
            log( LogEvent.VEHICLE_SERVICE_APPLICATION_ADDED, vehicleServiceApplication.getId(), "Vehicle service application Added", request );
        }

        return "redirect:/vehicle-service/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.vehicle.service.search", content = "vehicle/service/vehicle-service-search", activeMenu = Menu.VEHICLE_SERVICE_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.VEHICLE_SERVICE_SEARCH } )
    public String search(
            Model model,
            HttpServletRequest request
    ){

        return viewRoot;
    }

    @GetMapping( "/my-search" )
    @TitleAndContent( title = "title.vehicle.service.search", content = "vehicle/service/vehicle-service-my-search", activeMenu = Menu.VEHICLE_SERVICE_MY_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.VEHICLE_SERVICE_SEARCH } )
    public String mySearch(
            Model model,
            HttpServletRequest request
    ){

        return viewRoot;
    }

    @GetMapping( "/{id}" )
    @TitleAndContent( title = "title.vehicle.service.view", content = "vehicle/service/vehicle-service-view", activeMenu = Menu.VEHICLE_SERVICE_VIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.VEHICLE_SERVICE_VIEW } )
    public String view(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ){

        if( vehicleServiceApplicationValidationService.view( redirectAttributes, id ) ) {

            vehicleServiceApplicationModelService.view( model, id, getLoggedInUser() );
            return viewRoot;
        }

        return "redirect:/vehicle-service/search";
    }

    @PostMapping( "/take-action" )
    @TitleAndContent( title = "title.vehicle.service.search", content = "vehicle/service/vehicle-service-search", activeMenu = Menu.VEHICLE_SERVICE_SEARCH )
    public String takeAction(
            @RequestParam( "id" ) Long id,
            @RequestParam( "action" ) Long actionId,
            VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ) throws Exception {

        if( vehicleServiceApplicationValidationService.takeActionValidation( vehicleServiceAdditionalActionData, actionId, redirectAttributes ) ) {

            vehicleServiceApplicationModelService.takeAction( redirectAttributes, id, actionId, vehicleServiceAdditionalActionData, getLoggedInUser() );
        }

        return "redirect:/vehicle-service/search";
    }

    @PostMapping( "/add-more-product" )
    @TitleAndContent( title = "title.vehicle.service.search", content = "vehicle/service/vehicle-service-search", activeMenu = Menu.VEHICLE_SERVICE_SEARCH )
    public String addMoreProduct(
            @RequestParam( "id" ) Long id,
            @RequestParam( "job_id" ) Long jobId,
            VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ) throws Exception {

        if( vehicleServiceApplicationValidationService.addMoreProductValidation( vehicleServiceAdditionalActionData, redirectAttributes ) ) {

            vehicleServiceApplicationModelService.addMoreProduct( redirectAttributes, id, jobId, vehicleServiceAdditionalActionData, getLoggedInUser() );
        }
        return "redirect:/vehicle-service/search";
    }

    @GetMapping( "/gate-pass/{id}" )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.JQUERY_PRINT, FrontEndLibrary.LETTER_PRINT } )
    public String createGatePass(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ){

        if( vehicleServiceApplicationValidationService.getGatePass( redirectAttributes, id ) ){

            vehicleServiceApplicationModelService.getGatePass( model, id, getLoggedInUser() );
            return "vehicle/service/vehicle-service-gate-pass";
        }

        return "redirect:/vehicle-service/search";
    }
}
