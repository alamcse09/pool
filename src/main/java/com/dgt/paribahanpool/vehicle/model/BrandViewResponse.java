package com.dgt.paribahanpool.vehicle.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BrandViewResponse {

    private Long id;
    private String nameEn;
    private String nameBn;
}
