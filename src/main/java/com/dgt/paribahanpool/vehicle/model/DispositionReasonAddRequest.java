package com.dgt.paribahanpool.vehicle.model;

import lombok.Data;

@Data
public class DispositionReasonAddRequest {

    private Long id;
    private String reasonBn;
}
