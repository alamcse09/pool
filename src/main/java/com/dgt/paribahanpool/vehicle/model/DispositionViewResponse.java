package com.dgt.paribahanpool.vehicle.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DispositionViewResponse {

    private Long id;
    private String reasonBn;
}
