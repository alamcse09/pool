package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.Unit;
import lombok.Data;

@Data
public class InventoryItemViewResponse {

    private Long id;

    private String productNameEn;

    private String productNameBn;

    private Double quantity;

    private Unit unit;

    private double unitPrice;
}
