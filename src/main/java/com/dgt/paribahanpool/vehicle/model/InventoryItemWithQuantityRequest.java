package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.Unit;
import lombok.Data;

import javax.validation.constraints.Min;

@Data
public class InventoryItemWithQuantityRequest {

    @Min( value = 1, message = "{common.validation.min}")
    private Long id;

    private Long inventoryItemId;

    private Long inventoryCategoryId;

    private Unit unit;

    @Min( value = 0, message = "{common.validation.min}")
    private Double quantity;
}
