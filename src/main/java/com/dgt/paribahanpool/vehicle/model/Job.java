package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.JobSection;
import com.dgt.paribahanpool.enums.ServiceCategory;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table( name = "job" )
@SQLDelete( sql = "UPDATE job SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class Job extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    @Column( name = "job_id" )
    private String jobId;

    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    @Column( name = "sharok_no" )
    private String sharokNo;

    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    @Column( name = "name_bn" )
    private String nameBn;

    @Column( name = "service_category" )
    private ServiceCategory serviceCategory;

    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    @Column( name = "work_name", columnDefinition = "TEXT" )
    private String workName;

    @Column( name = "servicing_cost" )
    private Double servicingCost;

    @Column( name = "job_section" )
    private JobSection jobSection;

    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    @Column( name = "comments" )
    private String comments;

    @Column( name = "is_done" )
    private Boolean isDone = false;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "job_type_id", foreignKey = @ForeignKey( name = "fk_vehicle_service_app_job_type_id" ) )
    private JobType jobType;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "section_leader_id", foreignKey = @ForeignKey( name = "fk_job_section_leader_id" ) )
    private User sectionLeader;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany( cascade = CascadeType.ALL )
    @JoinTable(
            name = "job_inventory_item_map",
            joinColumns = @JoinColumn( name = "job_id", foreignKey = @ForeignKey( name = "fk_inventory_item_to_job_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "inventory_item_id", foreignKey = @ForeignKey( name = "fk_job_to_inventory_map_id" ) )
    )
    private List<JobInventoryItem> jobInventoryItems = new ArrayList<>();


    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany
    @JoinTable(
            name = "job_mechanics",
            joinColumns = @JoinColumn( name = "job_id", foreignKey = @ForeignKey( name = "fk_job_inventory_map_job_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "mechanic_id", foreignKey = @ForeignKey( name = "fk_job_inventory_map_mechanic_id" ) )
    )
    private Set<User> mechanics = new HashSet<>();
}
