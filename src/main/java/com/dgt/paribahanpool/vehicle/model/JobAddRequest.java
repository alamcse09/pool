package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.ServiceCategory;
import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class JobAddRequest {

    private Long id;

    private String jobId;

    private String sharokNo;

    private Long jobType;

    private ServiceCategory serviceCategory;

    private String workName;

    private String registrationNumber;

    private Double servicingCost;

    private Double totalCost;

    private String comments;

    private Long sectionLeaderId;

    @Valid
    private Long[] mechanicList;

    @Valid
    private List<InventoryItemWithQuantityRequest> inventoryItemWithQuantityRequestList;
}
