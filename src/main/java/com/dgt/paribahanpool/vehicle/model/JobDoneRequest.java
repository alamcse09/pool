package com.dgt.paribahanpool.vehicle.model;

import lombok.Data;

@Data
public class JobDoneRequest {

    private Long jobId;
    private Boolean isPresent;
    private Long stateId;
}
