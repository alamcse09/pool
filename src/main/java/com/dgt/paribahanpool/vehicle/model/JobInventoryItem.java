package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.Unit;
import com.dgt.paribahanpool.inventory_item.model.InventoryCategory;
import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@Entity
@Table( name = "job_inventory_item" )
@SQLDelete( sql = "UPDATE job_inventory_item SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class JobInventoryItem extends AuditableEntity {

    @Id
    @Column( name = "id" )
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( name = "quantity" )
    private Double quantity;

    @Column( name = "unit" )
    private Unit unit;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn( name = "inventory_category", foreignKey = @ForeignKey( name = "job_with_inventory_category_id" ) )
    private InventoryCategory inventoryCategory;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn( name = "inventory_item", foreignKey = @ForeignKey( name = "job_with_inventory_item_id" ) )
    private InventoryItem inventoryItem;
}
