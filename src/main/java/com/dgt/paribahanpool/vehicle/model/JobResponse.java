package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.JobSection;
import com.dgt.paribahanpool.inventory_item.model.InventoryCategoryViewResponse;
import lombok.Builder;
import lombok.Data;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class JobResponse {

    private Long id;
    private String jobId;
    private String sharokNo;
    private String jobNameBn;
    private String workName;
    private Boolean isJobViewed;
    private Double cost;
    private String sectionLeaderName;
    private JobSection jobSection;
    private Boolean isDone;

    @Valid
    private List<InventoryItemViewResponse> inventoryItemViewResponseList = new ArrayList<>();

    @Valid
    private List<InventoryCategoryViewResponse> inventoryCategoryViewResponseList = new ArrayList<>();

    private List<MechanicViewResponse> mechanicViewResponseList = new ArrayList<>();
}
