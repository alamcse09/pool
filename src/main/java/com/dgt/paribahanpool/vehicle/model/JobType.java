package com.dgt.paribahanpool.vehicle.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "job_type" )
public class JobType {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "name_en" )
    private String nameEn;

    @Column( name = "name_bn" )
    private String nameBn;
}
