package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.util.NameResponse;
import lombok.Data;

@Data
public class MechanicInChargeDetails {

    private Long userId;

    private NameResponse name;
}
