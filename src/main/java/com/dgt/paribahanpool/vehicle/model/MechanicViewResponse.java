package com.dgt.paribahanpool.vehicle.model;

import lombok.Data;

@Data
public class MechanicViewResponse {

    private String name;
    private String phoneNumber;
    private Long mechanicId;
}
