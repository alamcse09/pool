package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.workflow.model.WorkflowAdditionalData;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;
import java.util.Set;

@Data
public class RequisitionAdditionalActionData implements WorkflowAdditionalData {

    Set<Long> vehicleId;
    Long[] vehiclesToDettach;

    private transient MultipartFile[] rejectReasonsReportFiles;
    private List<DocumentMetadata> rejectReasonsReportDocuments = Collections.EMPTY_LIST;
}
