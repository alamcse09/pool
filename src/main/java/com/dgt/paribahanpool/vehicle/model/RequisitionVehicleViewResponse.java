package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.VehicleType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RequisitionVehicleViewResponse {

    private Long id;
    private String engineNumber;
    private String chassisNumber;
    private VehicleType vehicleType;
    private Integer modelYear;
    private String registrationNumber;
}
