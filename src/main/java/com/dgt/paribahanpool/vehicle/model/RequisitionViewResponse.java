package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.OwnerType;
import com.dgt.paribahanpool.enums.StateType;
import com.dgt.paribahanpool.user.model.UserResponse;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class RequisitionViewResponse {

    public Long id;
    public String name;
    public String designation;
    public String eMail;
    public String mobileNumber;
    public String identityNumber;
    public String workingPlace;
    public StateType stateType;
    private OwnerType ownerType;
    private String ownerNameEn;
    private String comments;
    private UserResponse currentStateUserInfo;
//    private LocalDate creationDate;

    public List<RequisitionVehicleViewResponse> vehicles;

    private DocumentMetadata signMetaData;
}
