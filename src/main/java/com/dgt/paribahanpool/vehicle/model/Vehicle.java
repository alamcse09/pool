package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.driver.model.Driver;
import com.dgt.paribahanpool.enums.FuelType;
import com.dgt.paribahanpool.enums.RegistrationType;
import com.dgt.paribahanpool.enums.Status;
import com.dgt.paribahanpool.enums.VehicleType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@Entity
@Table(
        name = "vehicle",
        uniqueConstraints = {
                @UniqueConstraint( columnNames = { "engine_number", "chassis_number" }, name = "uk_vehicle_engline_no_chassis_no" ),
                @UniqueConstraint( columnNames = { "reg_number" }, name = "uk_vehicle_reg_number" ),
                @UniqueConstraint( columnNames = { "driver_id" }, name = "uk_vehicle_driver" )
        }
)
@SQLDelete( sql = "UPDATE vehicle SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class Vehicle extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "model_year" )
    private Integer modelYear;

    @Column( name = "reg_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String registrationNo;

    @Column( name = "chassis_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String chassisNo;

    @Column( name = "color" )
    private Long color;

    @Column( name = "engine_number" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String engineNo;

    @Column( name = "reg_type" )
    private RegistrationType registrationType;

    @Column( name = "reg_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate regDate;

    @Column( name = "tax_token_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate taxTokenDate;

    @Column( name = "tax_token_date_end" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate taxTokenDateEnd;

    @Column( name = "fitness_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate fitnessDate;

    @Column( name = "fitness_date_end" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate fitnessDateEnd;

    @Column( name = "engine_cc" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String engineCC;

    @Column( name = "seat_capacity" )
    private Integer seatCapacity;

    @Column( name = "sales_org_name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String salesOrganizationName;

    @Column( name = "sales_org_name_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String salesOrganizationNameEn;

    @Column( name = "vehicle_type" )
    private VehicleType vehicleType;

    @Column( name = "fuel_type" )
    @ElementCollection( targetClass = FuelType.class ,fetch = FetchType.EAGER )
    @Enumerated(EnumType.ORDINAL)
    private Set<FuelType> fuelType;

    @Column( name = "document_group_id" )
    private Long docGroupId;

    @Column( name = "from_project" )
    private Boolean fromProject = false;

    @Column( name = "status" )
    private Status status;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne( fetch = FetchType.LAZY, cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH} )
    @JoinColumn( name = "brand_id", foreignKey = @ForeignKey( name = "fk_vehicle_brand_id" ) )
    private Brand brand;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne( fetch = FetchType.LAZY, cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH} )
    @JoinColumn( name = "model_id", foreignKey = @ForeignKey( name = "fk_vehicle_model_id" ) )
    private Model model;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "driver_id", foreignKey = @ForeignKey( name = "fk_vehicle_driver_id" ) )
    private Driver driver;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "vehicle_owner_id", foreignKey = @ForeignKey( name = "fk_vehicle_owner_id" ) )
    private VehicleOwner vehicleOwner;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "vehicle_auction_data_id", foreignKey = @ForeignKey( name = "fk_vehicle_vehicle_auction_data_id" ) )
    private VehicleAuctionData vehicleAuctionData;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinColumn( name = "vehicle_disposition_id", foreignKey = @ForeignKey( name = "fk_vehicle_vehicle_disposition_id" ) )
    private VehicleDisposition vehicleDisposition;
}
