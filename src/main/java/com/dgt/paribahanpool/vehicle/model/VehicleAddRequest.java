package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.FuelType;
import com.dgt.paribahanpool.enums.RegistrationType;
import com.dgt.paribahanpool.enums.VehicleType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Data
public class VehicleAddRequest {

    public Long id;

    @Min( value = 1900, message = "{validation.common.min}" )
    @Max( value = 2100, message = "{validation.common.max}" )
    @NotNull( message = "{validation.common.required}")
    public Integer modelYear;

//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
//    @NotBlank( message = "{validation.common.required}" )
//    public String brand;

//    @Pattern( regexp = "[a-zA-Z ]+|", message = "{validation.common.alphanumeric}" )
    public Long color;

    @NotBlank( message = "{validation.common.required}" )
//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    public String registrationNo;

    @NotBlank( message = "{validation.common.required}" )
//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    public String chassisNo;

    @NotBlank( message = "{validation.common.required}" )
//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    public String engineNo;

    public RegistrationType registrationType;

    @PastOrPresent( message = "{validation.common.date.past_or_present}" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    public LocalDate regDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    public LocalDate taxTokenDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate taxTokenDateEnd;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    public LocalDate fitnessDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    public LocalDate fitnessDateEnd;

//    @Pattern( regexp = "[a-zA-Z0-9 ]+|", message = "{validation.common.alphanumeric}" )
    public String engineCC;

    public Integer seatCapacity;

//    @Pattern( regexp = "[a-zA-Z0-9 ]+|", message = "{validation.common.alphanumeric}" )
    public String salesOrganizationName;

    private List<BrandViewResponse> brandViewResponseList;

    private List<ModelViewResponse> modelViewResponseList;

    @NotNull
    private Long brandId;

    @NotNull
    private Long modelId;

    public VehicleType vehicleType;

    public Set<FuelType> fuelType;

    private MultipartFile[] files;

    List<DocumentMetadata> documents = Collections.EMPTY_LIST;
}
