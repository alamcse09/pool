package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class VehicleAuctionBuyerInfoAddRequest {

    private Long buyerFromVehicleId;

    private String soldToNameEn;
    private String soldToNameBn;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate soldAt;
    private Double cost;
    private String soldAddressEn;
    private String soldAddressBn;
    private Integer isForPerOrOrg;

    private String nidNumber;

    private String tinNumber;
}
