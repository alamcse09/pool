package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;

@Data
public class VehicleAuctionCommitteeDecisionAddRequest {

    public Long committeeDecisionId;

    private Double committeePrice;

    private String committeeDecision;

    private MultipartFile[] committeeDecisionFiles;

    List<DocumentMetadata> documents = Collections.EMPTY_LIST;
}
