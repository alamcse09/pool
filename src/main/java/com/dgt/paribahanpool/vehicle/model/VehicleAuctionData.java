package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table( name = "vehicle_auction_data" )
@SQLDelete( sql = "UPDATE vehicle_auction_data SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class VehicleAuctionData extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "committee_price" )
    private Double committeePrice;

    @Column( name = "committee_decision", columnDefinition = "TEXT" )
    private String committeeDecision;

    @Column( name = "sold_price" )
    private Double soldPrice;

    @Column( name = "sold_name_bn" )
    private String soldToNameBn;

    @Column( name = "sold_name_en" )
    private String soldToNameEn;

    @Column( name = "sold_at" )
    private LocalDate soldAt;

    @Column( name = "sold_address_en", columnDefinition = "TEXT" )
    private String soldAddressEn;

    @Column( name = "sold_address_bn", columnDefinition = "TEXT" )
    private String soldAddressBn;

    @Column( name= "nid_number" )
    private String nidNumber;

    @Column( name= "tin_number" )
    private String tinNumber;

    @Column( name = "doc_group_id" )
    private Long docGroupId;
}
