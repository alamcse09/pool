package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.Status;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.NameResponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class VehicleAuctionResponse {

    private Long id;
    private String registrationNo;
    private String chassisNo;
    private BrandViewResponse brand;
    private Integer modelYear;
    private NameResponse vendor;

    @JsonFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate buyDate;

    private String status;
    private Status statusType;
    private Double price;
    private NameResponse buyer;
    private String committeeDecision;
}
