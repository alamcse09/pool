package com.dgt.paribahanpool.vehicle.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class VehicleAuctionViewResponse {

    private Long id;

    private Double litrePerKilo;

    private String vehicleLocation;

    private String maintainer;

    private String details;

    private String overhaulDetails;

    private Double kiloDriverAfterOverhaul;

    private Double kiloDrivenAfterFirstEngineOverhaul;

    private Double costFirstEngineOverhaul;

    private Double kiloDrivenAfterSecondEngineOverhaul;

    private Double costSecondEngineOverhaul;

    private Double kiloDrivenAfterThirdEngineOverhaul;

    private Double costThirdEngineOverhaul;

    private Double costOtherOverhaul;

    private Double costAllTillNow;

    private Double marketPrice;

    private String buyableNeededPart;

    private Double estimatedRepairCost;

    private String reasonForDisposition;

    private List<DispositionViewResponse> dispositionViewResponseList;
}
