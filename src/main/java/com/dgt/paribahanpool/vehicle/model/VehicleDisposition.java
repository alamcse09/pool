package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table( name = "vehicle_disposition" )
@SQLDelete( sql = "UPDATE vehicle_disposition SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class VehicleDisposition extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "litre_per_kilo" )
    private Double litrePerKilo;

    @Column( name = "vehicle_location" )
    private String vehicleLocation;

    @Column( name = "maintainer" )
    private String maintainer;

    @Column( name = "details" )
    private String details;

    @Column( name = "overhaul_details" )
    private String overhaulDetails;

    @Column( name = "kilo_driver_after_overhaul" )
    private Double kiloDriverAfterOverhaul;

    @Column( name = "kilo_driven_after_first_engine_overhaul" )
    private Double kiloDrivenAfterFirstEngineOverhaul;

    @Column( name = "cost_first_engine_overhaul" )
    private Double costFirstEngineOverhaul;

    @Column( name = "kilo_driven_after_second_engine_overhaul" )
    private Double kiloDrivenAfterSecondEngineOverhaul;

    @Column( name = "cost_second_engine_overhaul" )
    private Double costSecondEngineOverhaul;

    @Column( name = "kilo_driven_after_third_engine_overhaul" )
    private Double kiloDrivenAfterThirdEngineOverhaul;

    @Column( name = "cost_third_engine_overhaul" )
    private Double costThirdEngineOverhaul;

    @Column( name = "cost_other_overhaul" )
    private Double costOtherOverhaul;

    @Column( name = "cost_all_till_now" )
    private Double costAllTillNow;

    @Column( name = "market_price" )
    private Double marketPrice;

    @Column( name = "buyable_needed_part" )
    private String buyableNeededPart;

    @Column( name = "estimated_repair_cost" )
    private Double estimatedRepairCost;

    @Column( name = "reason_for_disposition" )
    private String reasonForDisposition;

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(
            name = "vehicle_disposition_disposition_reason_map",
            joinColumns = @JoinColumn( name = "vehicle_disposition_id", foreignKey = @ForeignKey( name = "fk_vehicle_disposition_disposition_reason_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "disposition_reason_id", foreignKey = @ForeignKey( name = "fk_vehicle_disposition_id_disposition_reason_map_disposition_id" ) )
    )
    private List<DispositionReason> dispositionReasonSet = new ArrayList<>();
}
