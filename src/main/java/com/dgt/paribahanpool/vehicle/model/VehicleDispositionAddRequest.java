package com.dgt.paribahanpool.vehicle.model;

import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class VehicleDispositionAddRequest {

    private Long id;

    private Double litrePerKilo;

    private String vehicleLocation;

    private String maintainer;

    private String details;

    private String overhaulDetails;

    private Double kiloDriverAfterOverhaul;

    private Double kiloDrivenAfterFirstEngineOverhaul;

    private Double costFirstEngineOverhaul;

    private Double kiloDrivenAfterSecondEngineOverhaul;

    private Double costSecondEngineOverhaul;

    private Double kiloDrivenAfterThirdEngineOverhaul;

    private Double costThirdEngineOverhaul;

    private Double costOtherOverhaul;

    private Double costAllTillNow;

    private Double marketPrice;

    private String buyableNeededPart;

    private Double estimatedRepairCost;

    private String reasonForDisposition;

    @Valid
    private List<DispositionReasonAddRequest> dispositionReasonAddRequestList;
}
