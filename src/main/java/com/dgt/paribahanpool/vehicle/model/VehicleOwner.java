package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.OwnerType;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
@Table( name = "vehicle_owner" )
public class VehicleOwner {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "owner_type" )
    private OwnerType ownerType;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "geo_location", foreignKey = @ForeignKey( name = "fk_vehicle_owner_geo_location_id" ) )
    private GeoLocation geoLocation;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "user", foreignKey = @ForeignKey( name = "fk_vehicle_owner_user_id" ) )
    private User user;

    @Column( name = "owner_name_en")
    private String ownerNameEn;

    @Column( name = "owner_name_bn")
    private String ownerNameBn;
}
