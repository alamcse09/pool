package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.OwnerType;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table( name = "vehicle_owner_history" )
public class VehicleOwnerHistory {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "assigned_at" )
    private LocalDateTime assignedAt;

    @Column( name = "returned_at" )
    private LocalDateTime returnedAt;

    @Column( name = "owner_type" )
    private OwnerType ownerType;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "geo_location_id", foreignKey = @ForeignKey( name = "fk_vehicle_owner_history_geo_location_id" ) )
    private GeoLocation geoLocation;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "user_id", foreignKey = @ForeignKey( name = "fk_vehicle_owner_history_user_id" ) )
    private User user;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "vehicle_id", foreignKey = @ForeignKey( name = "fk_vehicle_owner_history_vehicle_id" ) )
    private Vehicle vehicle;
}
