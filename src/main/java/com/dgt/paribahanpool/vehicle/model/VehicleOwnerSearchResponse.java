package com.dgt.paribahanpool.vehicle.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VehicleOwnerSearchResponse {

    //private VehicleOwnerInfoSearchResponse publicUserInfo;
    private String ownerNameEn;
    private String ownerNameBn;
}
