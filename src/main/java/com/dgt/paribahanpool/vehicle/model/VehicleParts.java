package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.WarrantyType;
import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table( name = "vehicle_parts" )
@SQLDelete( sql = "UPDATE vehicle_parts SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class VehicleParts extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "use_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate useDate;

    @Column( name = "warranty_start_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyStartDate;

    @Column( name = "warranty_end_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyEndDate;

    @Column( name = "warranty_type" )
    private WarrantyType warrantyType;

    @Column( name = "is_active" )
    private Boolean isActive;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "vehicle_id", foreignKey = @ForeignKey( name = "fk_vehicle_parts_vehicle_id" ) )
    private Vehicle vehicle;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "vehicle_service_application_id", foreignKey = @ForeignKey( name = "fk_vehicle_parts_vehicle_service_application_id" ) )
    private VehicleServiceApplication vehicleServiceApplication;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST } )
    @JoinColumn( name = "job_id", foreignKey = @ForeignKey( name = "fk_vehicle_parts_job_id" ) )
    private Job job;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "inventory_item_id", foreignKey = @ForeignKey( name = "fk_vehicle_parts_inventory_item_id" ) )
    private InventoryItem inventoryItem;
}
