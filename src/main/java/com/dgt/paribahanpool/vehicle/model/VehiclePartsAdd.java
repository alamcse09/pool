package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class VehiclePartsAdd {

    private Vehicle vehicle;
    private InventoryItem inventoryItem;
    private Job job;
    private VehicleServiceApplication vehicleServiceApplication;
    private Integer count;
    private LocalDate useDate;
}
