package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.WarrantyType;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class VehiclePartsViewResponse {

    private Long vehiclePartsId;
    private Long inventoryItemId;
    private Long vehicleId;

    private String inventoryItemNameEn;
    private String inventoryItemNameBn;
    private String vendorNameEn;
    private String vendorNameBn;

    private LocalDate warrantyStartDate;
    private LocalDate warrantyEndDate;
    private String warrantyType;

    private String vehicleRegNo;
    private String chassisNo;
    private String engineNo;

    private LocalDate useDate;
}
