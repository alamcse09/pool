package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.OwnerType;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.WorkflowEntity;
import lombok.Data;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table( name = "vehicle_requistion_application" )
@SQLDelete( sql = "UPDATE vehicle_requistion_application SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class VehicleRequisitionApplication extends AuditableEntity implements WorkflowEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "name" )
    private String name;

    @Column( name = "service_id" )
    private String serviceId;

    @Column( name = "designation" )
    private String designation;

    @Column( name = "workplace" )
    private String workplace;

    @Column( name = "email" )
    private String email;

    @Column( name = "phone_no" )
    private String phoneNo;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "state_id", foreignKey = @ForeignKey( name = "fk_vehicle_requisition_app_state_id" ) )
    private State state;

    @Column( name = "owner_type" )
    private OwnerType ownerType;

    @Column( name = "document_group_id" )
    private Long docGroupId;

    @Column( name = "comments" )
    private String comments;

    @Column( name = "division_id")
    public Long divisionId;

    @Column( name = "district_id")
    public Long districtId;

    @Column( name = "upazila_id")
    public Long upazilaId;

    @Column( name = "reject_reason_report_group_id" )
    private Long rejectReasonReportGroupId;

    @OneToMany( cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinTable(
            name = "requisition_application_vehicle_type_counter_map",
            joinColumns = @JoinColumn( name = "requisition_application_id", foreignKey = @ForeignKey( name = "fk_vehicle_type_counter_requisition_application_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "vehicle_type_counter_id", foreignKey = @ForeignKey( name = "fk_requisition_application_id_vehicle_type_counter_map_id" ) )
    )
    private Set<VehicleTypeCounter> vehicleTypeCounterSet = new HashSet<>();

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinTable(
            name = "requisition_vehicle_map",
            joinColumns = @JoinColumn( name = "requisition_application_id", foreignKey = @ForeignKey( name = "fk_requisition_vehicle_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "vehicle_id", foreignKey = @ForeignKey( name = "fk_vehicle_id_requisition_map_requisition_application_id" ) )
    )
    private Set<Vehicle> vehicleSet = new HashSet<>();
}
