package com.dgt.paribahanpool.vehicle.model;

import lombok.Data;

@Data
public class VehicleRequisitionReportData {

    private Long ownerTypeValue;
    public Long divisionValue;
    public Long districtValue;
    public Long upazilaValue;
}
