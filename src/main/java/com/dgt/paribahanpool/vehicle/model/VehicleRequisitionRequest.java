package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.EmployeeType;
import com.dgt.paribahanpool.enums.StateType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Collections;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VehicleRequisitionRequest {

    public Long id;

    @NotNull( message = "{validation.common.required}")
    public String name;

    @NotNull( message = "{validation.common.required}")
    public String identityNumber;

    public String designation;

    public String workingPlace;

    @Email( message = "{validation.common.email}")
    public String eMail;

    public String mobileNumber;

    public StateType stateType;

    public EmployeeType ownerType;

    public String comments;

    public Long divisionValue;

    public Long districtValue;

    public Long upazilaValue;

    private MultipartFile[] files;

    List<DocumentMetadata> documents = Collections.EMPTY_LIST;

    @Valid
    public List<VehicleTypeCounterAddRequest> vehicleTypeCounterAddRequests;
}
