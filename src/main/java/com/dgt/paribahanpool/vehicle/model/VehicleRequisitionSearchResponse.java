package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.OwnerType;
import com.dgt.paribahanpool.workflow.model.StateResponse;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VehicleRequisitionSearchResponse {

    private Long id;
    private String name;
    private String serviceId;
    private String designation;
    private String workplace;
    private String email;
    private String phoneNo;
    private StateResponse stateResponse;
    private String ownerType;
}
