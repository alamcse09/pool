package com.dgt.paribahanpool.vehicle.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VehicleResponseForSelection {

    private Long id;
    private String text;
}
