package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.color.model.Color;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.driver_assign.model.DriverSearchResponse;
import com.dgt.paribahanpool.enums.FuelType;
import com.dgt.paribahanpool.enums.RegistrationType;
import com.dgt.paribahanpool.enums.VehicleType;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Data
public class VehicleSearchResponse {

    private Long id;
    private BrandViewResponse brand;
    private String color;
    private Integer modelYear;
    private String registrationNo;
    private String chassisNo;
    private String engineNo;
    private RegistrationType registrationType;
    private LocalDate regDate;
    private LocalDate taxTokenDate;
    private LocalDate fitnessDate;
    private LocalDate fitnessDateEnd;
    private String engineCC;
    private Integer seatCapacity;
    private String salesOrganizationName;
    private VehicleType vehicleType;
    private Set<FuelType> fuelType;
    private VehicleOwnerSearchResponse vehicleOwner;
    private DriverSearchResponse driver;
    private String driverName;
    private String driverPhoneNumber;

    private List<DocumentMetadata> documentMetadataList;
}