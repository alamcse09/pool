package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.ServiceCategory;
import com.dgt.paribahanpool.enums.ServiceType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
public class VehicleServiceAddRequest {

    private Long id;

    @NotNull( message = "{validation.common.required}" )
    @Min( value = 1, message = "{validation.common.min}" )
    private Long vehicleId;

    private Long driverId;
    private Long vehicleUserId;

    @NotBlank( message = "{validation.common.required}" )
//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    private String regNo;

    @NotBlank( message = "{validation.common.required}" )
//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    private String chassisNo;

//    @NotBlank( message = "{validation.common.required}" )
    private String vehicleUserNameBn;

//    @NotBlank( message = "{validation.common.required}" )
    private String vehicleUserNameEn;

//    @NotBlank( message = "{validation.common.required}" )
    private String designationBn;

//    @NotBlank( message = "{validation.common.required}" )
    private String designationEn;

//    @NotBlank( message = "{validation.common.required}" )
    private String workplaceBn;

//    @NotBlank( message = "{validation.common.required}" )
    private String workplaceEn;

//    @Digits( integer = 11, fraction = 0, message = "{validation.common.numeric}" )
//    @NotBlank( message = "{validation.common.required}")
    private String vehicleUserPhoneNumber;

    private String driverNameBn;

    private String driverNameEn;

    private String vehicleProblemsDetails;

    @Digits( integer = 11, fraction = 0, message = "{validation.common.numeric}" )
    @NotBlank( message = "{validation.common.required}")
    private String driverPhoneNumber;

    private ServiceCategory serviceCategory;

    private ServiceType serviceType;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate serviceDate;

}
