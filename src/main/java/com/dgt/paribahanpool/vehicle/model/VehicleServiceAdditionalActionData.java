package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.JobSection;
import com.dgt.paribahanpool.workflow.model.WorkflowAdditionalData;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

@Data
public class VehicleServiceAdditionalActionData implements WorkflowAdditionalData {

    private Long id;

    private Long mechanicUserId;

    private Long workshopManagerId;

    private Long jobAssistantId;

    private Long vehicleId;

//    private VehicleDispositionAddRequest vehicleDispositionAddRequest;

    private Double litrePerKilo;

    private String vehicleLocation;

    private String maintainer;

    private String details;

    private String overhaulDetails;

    private Double kiloDriverAfterOverhaul;

    private Double kiloDrivenAfterFirstEngineOverhaul;

    private Double costFirstEngineOverhaul;

    private Double kiloDrivenAfterSecondEngineOverhaul;

    private Double costSecondEngineOverhaul;

    private Double kiloDrivenAfterThirdEngineOverhaul;

    private Double costThirdEngineOverhaul;

    private Double costOtherOverhaul;

    private Double costAllTillNow;

    private Double marketPrice;

    private String buyableNeededPart;

    private Double estimatedRepairCost;

    private String reasonForDisposition;

    private Long[] sectionLeaderId;

    private JobSection[] jobSections;

    private String commentFromPoridorshok;

    private String finalPoridorshokComment;

    private transient MultipartFile[] driverCommentReportFiles;
    private List<DocumentMetadata> driverCommentReportDocuments = Collections.EMPTY_LIST;

    @Valid
    private List<DispositionReasonAddRequest> dispositionReasonAddRequestList;

    @Valid
    private List<JobAddRequest> jobAddRequestList;

    @Valid
    private List<InventoryItemWithQuantityRequest> moreInventoryItemWithQuantityRequestList;
}
