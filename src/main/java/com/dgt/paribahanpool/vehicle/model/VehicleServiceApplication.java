package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.driver.model.Driver;
import com.dgt.paribahanpool.enums.ServiceCategory;
import com.dgt.paribahanpool.enums.ServiceType;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.WorkflowEntity;
import lombok.Data;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table( name = "vehicle_service_application" )
@SQLDelete( sql = "UPDATE vehicle_service_application SET is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class VehicleServiceApplication extends AuditableEntity implements WorkflowEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "reg_no" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String regNo;

    @Column( name = "chassis_no" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String chassisNo;

    @Column( name = "vehicle_user_name_bn" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String vehicleUserNameBn;

    @Column( name = "vehicle_user_name_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String vehicleUserNameEn;

    @Column( name = "designation_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String designationEn;

    @Column( name = "designation_bn" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String designationBn;

    @Column( name = "workplace_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String workplaceEn;

    @Column( name = "workplace_bn" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String workplaceBn;

    @Column( name = "vehicle_user_phone_no" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String vehicleUserPhoneNo;

    @Column( name = "service_date" )
    private LocalDate serviceDate;

    @Column( name = "driver_name_en" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String driverNameEn;

    @Column( name = "driver_name_bn" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String driverNameBn;

    @Column( name = "driver_phone_no" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String driverPhoneNo;

    @Column( name = "service_category" )
    private ServiceCategory serviceCategory;

    @Column( name = "service_type" )
    private ServiceType serviceType;

    @Column( name = "approved_date" )
    private LocalDate approvedDate;

    @Column( name = "comment_from_poridorshok", columnDefinition = "TEXT" )
    private String commentFromPoridorshok;

    @Column( name = "final_poridorshok_comment", columnDefinition = "TEXT" )
    private String finalPoridorshokComment;

    @Column( name = "vehicle_problems_details", columnDefinition = "TEXT" )
    private String vehicleProblemsDetails;

    @Column( name = "verdict_document_group_id" )
    private Long verdictDocGroupId;

    @Column( name = "driver_comment_document_group_id" )
    private Long driverCommentDocGroupId;

    @ManyToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "state_id", foreignKey = @ForeignKey( name = "vehicle_service_application_state_id" ) )
    private State state;

    @ManyToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "vehicle_id", foreignKey = @ForeignKey( name = "vehicle_service_application_vehicle_id" ) )
    private Vehicle vehicle;

    @ManyToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "driver_id", foreignKey = @ForeignKey( name = "vehicle_service_application_driver_id" ) )
    private Driver driver;

    @OneToMany( fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE } )
    @JoinTable(
            name = "vehicle_service_app_job_map",
            joinColumns = @JoinColumn( name = "vehicle_service_id", foreignKey = @ForeignKey( name = "fk_vehicle_service_job_map_vehicle_service_id" ) ),
            inverseJoinColumns = @JoinColumn( name = "job_id", foreignKey = @ForeignKey( name = "fk_vehicle_service_job_map_job_id" ) )
    )
    private Set<Job> jobs = new HashSet<>();

    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "workshop_manager_id", foreignKey = @ForeignKey( name = "vehicle_service_application_workshop_manager_id" ) )
    private User workshopManager;

    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = false )
    @JoinColumn( name = "job_assistant_id", foreignKey = @ForeignKey( name = "vehicle_service_application_job_assistant_id" ) )
    private User jobAssistant;
}
