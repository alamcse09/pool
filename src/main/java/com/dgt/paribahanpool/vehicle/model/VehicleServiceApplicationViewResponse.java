package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.StateType;
import com.dgt.paribahanpool.workflow.model.StateResponse;
import lombok.Data;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class VehicleServiceApplicationViewResponse {

    private Long id;
    private Long stateId;
    private String regNo;
    private String chassisNo;
    private String vehicleUserNameBn;
    private String vehicleUserNameEn;
    private String modelName;
    private String vehicleType;
    private String designationEn;
    private String designationBn;
    private String driverNameEn;
    private String driverNameBn;
    private String driverPhoneNo;
    private StateType stateType;
    private String jobNameEn;
    private String jobNameBn;
    private Boolean isJobViewed;
    private Long brandId;
    private Long modelId;
    private Long vehicleId;
    private LocalDate approvedDate;
    private LocalDate appliedDate;

    private String commentFromPoridorshok;
    private String finalPoridorshokComment;
    private String vehicleProblemsDetails;

    List<DocumentMetadata> driverCommentDocuments;

    private StateResponse stateResponse;

    private Double totalCost;

    private DocumentMetadata signMetaData;

    private List<JobResponse> jobResponses = new ArrayList<>();
}
