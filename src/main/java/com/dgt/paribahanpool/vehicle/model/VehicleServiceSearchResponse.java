package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.workflow.model.StateResponse;
import lombok.Data;

@Data
public class VehicleServiceSearchResponse {

    private Long id;
    private String regNo;
    private String chassisNo;
    private String vehicleUserNameBn;
    private String vehicleUserNameEn;
    private String designationEn;
    private String designationBn;
    private StateResponse stateResponse;
}
