package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.VehicleType;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table( name = "vehicle_type_counter")
public class VehicleTypeCounter {

    @Id
    @Column( name = "id" )
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;

    @Column( name = "vehicle_type" )
    private VehicleType vehicleType;

    @Column( name = "counter" )
    private Integer counter;
}
