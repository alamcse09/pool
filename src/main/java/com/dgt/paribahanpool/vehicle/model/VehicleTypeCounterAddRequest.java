package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.enums.VehicleType;
import lombok.Data;

@Data
public class VehicleTypeCounterAddRequest {

    private Long id;

    private VehicleType vehicleType;

    private Integer counter;
}
