package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.color.model.Color;
import com.dgt.paribahanpool.enums.FuelType;
import com.dgt.paribahanpool.enums.RegistrationType;
import com.dgt.paribahanpool.enums.VehicleType;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@Builder
public class VehicleViewResponse {

    public Long id;
    public Integer modelYear;
    public String brand;
    public String color;
    public String registrationNo;
    public String chassisNo;
    public String engineNo;
    public RegistrationType registrationType;
    public LocalDate regDate;
    public LocalDate taxTokenDate;
    public LocalDate fitnessDate;
    public LocalDate fitnessDateEnd;
    public String engineCC;
    public Integer seatCapacity;
    public String salesOrganizationName;
    public VehicleType vehicleType;
    public Set<FuelType> fuelType;
    private LocalDateTime assignedAt;
}
