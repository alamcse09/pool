package com.dgt.paribahanpool.vehicle.model;

import com.dgt.paribahanpool.util.NameResponse;
import lombok.Data;

@Data
public class WorkshopManagerDetails {

    private Long userId;

    private NameResponse name;
}
