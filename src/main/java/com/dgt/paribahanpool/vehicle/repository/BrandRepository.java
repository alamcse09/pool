package com.dgt.paribahanpool.vehicle.repository;

import com.dgt.paribahanpool.budget.model.BudgetCode;
import com.dgt.paribahanpool.vehicle.model.Brand;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandRepository extends DataTablesRepository<Brand, Long> , JpaRepository<Brand,Long> {
}
