package com.dgt.paribahanpool.vehicle.repository;

import com.dgt.paribahanpool.vehicle.model.JobInventoryItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobInventoryRepository extends JpaRepository<JobInventoryItem, Long> {
}
