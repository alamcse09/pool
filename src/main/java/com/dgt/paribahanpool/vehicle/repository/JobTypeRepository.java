package com.dgt.paribahanpool.vehicle.repository;

import com.dgt.paribahanpool.vehicle.model.JobType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobTypeRepository extends JpaRepository<JobType, Long> {
}
