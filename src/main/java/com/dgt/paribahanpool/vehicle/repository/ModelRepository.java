package com.dgt.paribahanpool.vehicle.repository;

import com.dgt.paribahanpool.vehicle.model.Brand;
import com.dgt.paribahanpool.vehicle.model.Model;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ModelRepository extends DataTablesRepository<Model, Long>, JpaRepository<Model,Long> {

    List<Model> getAllByBrandId( Long brandId );
}
