package com.dgt.paribahanpool.vehicle.repository;

import com.dgt.paribahanpool.vehicle.model.VehicleAuctionData;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface VehicleAuctionRepository extends DataTablesRepository<VehicleAuctionData, Long> {
}
