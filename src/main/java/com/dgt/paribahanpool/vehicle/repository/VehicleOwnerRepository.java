package com.dgt.paribahanpool.vehicle.repository;

import com.dgt.paribahanpool.enums.OwnerType;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.vehicle.model.VehicleOwner;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

import java.util.List;
import java.util.Optional;

public interface VehicleOwnerRepository extends DataTablesRepository<VehicleOwner, Long> {

    List<VehicleOwner> findByUserId( Long id );
    List<VehicleOwner> findByOwnerType(OwnerType ownerType);
    Optional<VehicleOwner> findByGeoLocation(GeoLocation geoLocation);
    Optional<VehicleOwner> findByOwnerTypeAndUser( OwnerType ownerType, User user );
}
