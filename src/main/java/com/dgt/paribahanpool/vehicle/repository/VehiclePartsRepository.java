package com.dgt.paribahanpool.vehicle.repository;

import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.model.VehicleParts;
import com.dgt.paribahanpool.vehicle.model.VehicleServiceApplication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VehiclePartsRepository extends JpaRepository<VehicleParts,Long> {

    List<VehicleParts> findByVehicleAndIsActive( Vehicle vehicle, Boolean isActive );

    List<VehicleParts> findByVehicleServiceApplication(VehicleServiceApplication vehicleServiceApplication);
}
