package com.dgt.paribahanpool.vehicle.repository;

import com.dgt.paribahanpool.driver.model.Driver;
import com.dgt.paribahanpool.enums.AlertType;
import com.dgt.paribahanpool.enums.EntityType;
import com.dgt.paribahanpool.enums.Status;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface VehicleRepository extends DataTablesRepository<Vehicle,Long>, JpaRepository<Vehicle,Long> {

    Optional<Vehicle> findByRegistrationNo(String regNo);

    @Query( "FROM Vehicle v LEFT JOIN AlertLog log ON v.id = log.entityId and log.entityType = ?2 and log.alertType = ?1 WHERE log.id IS NULL and v.fitnessDateEnd <= ?3" )
    List<Vehicle> findByFitnessDateAlertTypeNotSent(AlertType alertType, EntityType entityType, LocalDate date );

    @Query( "FROM Vehicle v LEFT JOIN AlertLog log ON v.id = log.entityId and log.entityType = ?2 and log.alertType = ?1 WHERE log.id IS NULL and v.taxTokenDate <= ?3" )
    List<Vehicle> findByTaxTokenDateAlertTypeNotSent( AlertType taxToken, EntityType vehicle, LocalDate taxTokenExpiryDate );

    List<Vehicle> findByVehicleOwnerIsNull();

    List<Vehicle> findAllByVehicleOwnerId( Long ownerId );

    Long countByVehicleOwnerIsNullAndStatusIsNot( Status status );

    Optional<Vehicle> findByDriver( Driver driver );

    List<Vehicle> findAllByVehicleAuctionDataIsNullAndVehicleDispositionIsNull();
}
