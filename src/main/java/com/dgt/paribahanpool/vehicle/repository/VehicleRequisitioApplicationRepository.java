package com.dgt.paribahanpool.vehicle.repository;

import com.dgt.paribahanpool.vehicle.model.VehicleRequisitionApplication;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface VehicleRequisitioApplicationRepository extends DataTablesRepository<VehicleRequisitionApplication, Long> {
}
