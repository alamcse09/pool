package com.dgt.paribahanpool.vehicle.repository;

import com.dgt.paribahanpool.vehicle.model.VehicleServiceApplication;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface VehicleServiceApplicationRepository extends DataTablesRepository<VehicleServiceApplication,Long> {

    @Query( "select count(*), month(s.createdAt) FROM VehicleServiceApplication s where s.createdAt between ?2 and ?1 group by month(s.createdAt)" )
    List<Object[]> findRegisteredCustomersHistory(@Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate);
}
