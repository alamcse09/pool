package com.dgt.paribahanpool.vehicle.repository;

import com.dgt.paribahanpool.vehicle.model.VehicleServiceCost;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VehicleServiceCostRepository extends DataTablesRepository<VehicleServiceCost, Long>, JpaRepository<VehicleServiceCost,Long> {

    List<VehicleServiceCost> findByVehicleIdAndYear( Long vehicleId, Integer year );
}
