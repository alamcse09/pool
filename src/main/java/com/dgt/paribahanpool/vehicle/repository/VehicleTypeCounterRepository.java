package com.dgt.paribahanpool.vehicle.repository;

import com.dgt.paribahanpool.vehicle.model.VehicleTypeCounter;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface VehicleTypeCounterRepository extends DataTablesRepository<VehicleTypeCounter, Long> {
}
