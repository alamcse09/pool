package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.vehicle.model.Brand;
import com.dgt.paribahanpool.vehicle.model.BrandViewResponse;
import com.dgt.paribahanpool.vehicle.repository.BrandRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BrandService {

    private final BrandRepository brandRepository;

    private List<Brand> findAll() {

        return brandRepository.findAll();
    }

    public List<BrandViewResponse> getAllBrandViewResponse() {

        List<Brand> brandList = findAll();

        return brandList.stream()
                .map( this::getBrandViewResponseFromBrand )
                .collect( Collectors.toList() );
    }

    private BrandViewResponse getBrandViewResponseFromBrand(Brand brand) {

        return BrandViewResponse.builder()
                .id( brand.getId() )
                .nameEn( brand.getNameEn() )
                .nameBn(brand.getNameBn() )
                .build();

    }

    public Optional<Brand> findById(Long brandId) {

        return brandRepository.findById( brandId );
    }
}
