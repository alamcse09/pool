package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestValidationResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class BrandValidationService {

    public RestValidationResult isExist(Long id ) throws NotFoundException {

        return RestValidationResult.valid();
    }

}
