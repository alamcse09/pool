package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.vehicle.repository.JobInventoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class JobInventoryService {

    private final JobInventoryRepository jobInventoryRepository;
}
