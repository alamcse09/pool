package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.enums.ProductType;
import com.dgt.paribahanpool.enums.Unit;
import com.dgt.paribahanpool.inventory_item.model.InventoryCategory;
import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import com.dgt.paribahanpool.inventory_item.service.InventoryCategoryService;
import com.dgt.paribahanpool.inventory_item.service.InventoryItemService;
import com.dgt.paribahanpool.mechanic.model.Mechanic;
import com.dgt.paribahanpool.mechanic.service.MechanicService;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.vehicle.model.*;
import com.dgt.paribahanpool.vehicle.repository.JobRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class JobService {

    private final JobRepository jobRepository;
    private final InventoryItemService inventoryItemService;
    private final MechanicService mechanicService;
    private final UserService userService;
    private final InventoryCategoryService inventoryCategoryService;
    private final JobTypeService jobTypeService;

    public Optional<Job> findById( Long id ) {

        return jobRepository.findById( id );
    }

    public Job save ( Job job ) {

        return jobRepository.save( job );
    }

    @Transactional
    public Set<Job> getJobListFromAdditionalActionData(VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData) {

        Set<Job> jobSet = new HashSet<>();

        if( vehicleServiceAdditionalActionData.getJobAddRequestList() != null ){

            jobSet =
                    vehicleServiceAdditionalActionData.getJobAddRequestList()
                            .stream()
                            .map( jobAddRequest -> getJobFromJobAddRequest( jobAddRequest, vehicleServiceAdditionalActionData ) )
                            .filter( job -> job != null )
                            .collect( Collectors.toSet() );

        }

        return jobSet;
    }

    private List<Job> saveAll(List<Job> jobSet) {

        return jobRepository.saveAll( jobSet );
    }

    private Job getJobFromJobAddRequest(JobAddRequest jobAddRequest, VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData) {

        JobType jobType = jobTypeService.getReference( jobAddRequest.getJobType() );

        Job job = new Job();

        job.setJobType( jobType );
        job.setJobId( jobAddRequest.getJobId() );
        job.setSharokNo( jobAddRequest.getSharokNo() );
        job.setServiceCategory( jobAddRequest.getServiceCategory() );
        job.setWorkName( jobAddRequest.getWorkName() );
        job.setServicingCost( jobAddRequest.getServicingCost() );
        job.setComments( jobAddRequest.getComments() );
        
        if( jobAddRequest.getInventoryItemWithQuantityRequestList() != null ) {

            List<JobInventoryItem> jobInventoryItemList = jobAddRequest.getInventoryItemWithQuantityRequestList()
                    .stream()
                    .map(this::getJobInventoryItemFromInventoryItemWithQuantityRequest)
                    .filter( item -> item != null)
                    .collect(Collectors.toList());

            job.getJobInventoryItems().clear();
            job.getJobInventoryItems().addAll(jobInventoryItemList);
        }

        if( jobAddRequest.getMechanicList() != null && jobAddRequest.getMechanicList().length > 0 ) {

            Set<User> userSet = new HashSet<>();

            for(int i=0;i<jobAddRequest.getMechanicList().length;i++) {

                Optional<Mechanic> mechanicOptional = mechanicService.findById( jobAddRequest.getMechanicList()[0] );

                if( mechanicOptional.isPresent() ) {

                    Optional<User> userOptional = userService.findByMechanic( mechanicOptional.get() );

                    if(userOptional.isPresent()) {

                        userSet.add(userOptional.get());
                    }
                }
            }

            job.getMechanics().clear();
            job.getMechanics().addAll( userSet );
        }
        return job;
    }

    private JobInventoryItem getJobInventoryItemFromInventoryItemWithQuantityRequest( InventoryItemWithQuantityRequest inventoryItemWithQuantityRequest ) {

        Optional<InventoryCategory> inventoryCategoryOptional = inventoryCategoryService.findById( inventoryItemWithQuantityRequest.getInventoryCategoryId() );

        if( inventoryCategoryOptional.isPresent() ) {

            InventoryCategory inventoryCategory = inventoryCategoryOptional.get();

            JobInventoryItem jobInventoryItem = new JobInventoryItem();

            jobInventoryItem.setInventoryCategory(inventoryCategory);

            Optional<InventoryItem> inventoryItemOptional = inventoryItemService.findById(inventoryItemWithQuantityRequest.getInventoryItemId());

            if (inventoryItemOptional.isPresent()) {

                InventoryItem inventoryItem = inventoryItemOptional.get();

                jobInventoryItem.setInventoryItem(inventoryItem);

                if (inventoryItem.getProductType().equals(ProductType.STATIONARY) || inventoryItem.getProductType().equals(ProductType.PARTS))
                    jobInventoryItem.setUnit(Unit.SET);
                else if (inventoryItem.getProductType().equals(ProductType.OIL))
                    jobInventoryItem.setUnit(Unit.LITER);

                jobInventoryItem.setQuantity(inventoryItemWithQuantityRequest.getQuantity());
            }

            return jobInventoryItem;
        }

        return null;
    }

    public Set<Job> saveJobsWithSectionLeaderIds( VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData, VehicleServiceApplication vehicleServiceApplication ) {

        Set<Job> jobSet = vehicleServiceApplication.getJobs();

        Set<Job> modifiedJobSet = new HashSet<>();

        Integer indexForTrackingSectionLeaderId = 0;

        for( Job job : jobSet ){

            Optional<User> userOptional = userService.findById( vehicleServiceAdditionalActionData.getSectionLeaderId()[indexForTrackingSectionLeaderId] );

            if( userOptional.isPresent() ){

                job.setSectionLeader( userOptional.get() );
                job.setJobSection( vehicleServiceAdditionalActionData.getJobSections()[ indexForTrackingSectionLeaderId ] );
            }

            indexForTrackingSectionLeaderId += 1;

            modifiedJobSet.add( job );
        }

        return modifiedJobSet;
    }

    public void markJobDone(Long jobId, Boolean isPresent) {

        Optional<Job> jobOptional = findById( jobId );

        if( jobOptional.isPresent() ){

            Job job = jobOptional.get();

            job.setIsDone( isPresent );

            save( job );
        }
    }
}
