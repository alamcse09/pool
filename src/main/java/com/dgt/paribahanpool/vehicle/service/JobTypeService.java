package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.vehicle.model.JobType;
import com.dgt.paribahanpool.vehicle.model.JobTypeResponse;
import com.dgt.paribahanpool.vehicle.repository.JobTypeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class JobTypeService {

    private final JobTypeRepository jobTypeRepository;

    public List<JobType> findAll(){

        return jobTypeRepository.findAll();
    }

    public List<JobTypeResponse> findAllJobTypeResponse(){

        return findAll().stream().map( this::getJobTypeResponseFromJobType ).collect( Collectors.toList() );
    }

    private JobTypeResponse getJobTypeResponseFromJobType( JobType jobType ) {

        return JobTypeResponse.builder()
                .id( jobType.getId() )
                .nameBn( jobType.getNameBn() )
                .nameEn( jobType.getNameEn() )
                .build();
    }

    public JobType getReference( Long id ){

        return jobTypeRepository.getById( id );
    }
}
