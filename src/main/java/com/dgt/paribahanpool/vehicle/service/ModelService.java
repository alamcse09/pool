package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.vehicle.model.Brand;
import com.dgt.paribahanpool.vehicle.model.BrandViewResponse;
import com.dgt.paribahanpool.vehicle.model.Model;
import com.dgt.paribahanpool.vehicle.model.ModelViewResponse;
import com.dgt.paribahanpool.vehicle.repository.ModelRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class ModelService {

    private final ModelRepository modelRepository;

    private List<Model> findAll() {

        return modelRepository.findAll();
    }

    public List<ModelViewResponse> getAllModelViewResponse() {

        List<Model> modelList = findAll();

        return modelList.stream()
                .map( this::getModelViewResponseFromModel)
                .collect( Collectors.toList() );
    }

    private ModelViewResponse getModelViewResponseFromModel(Model model) {

        return ModelViewResponse.builder()
                .id( model.getId() )
                .nameEn( model.getNameEn() )
                .nameBn(model.getNameBn() )
                .build();
    }

    public Optional<Model> findById(Long modelId) {

        return modelRepository.findById( modelId );
    }

    public List<ModelViewResponse> getAllModelViewResponseByBrandId(Long id) {

        List<Model> modelList = modelRepository.getAllByBrandId( id );

        return modelList.stream()
                .map( this::getModelViewResponseFromModel)
                .collect( Collectors.toList() );
    }
}
