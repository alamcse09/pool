package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.vehicle.model.VehicleAuctionBuyerInfoAddRequest;
import com.dgt.paribahanpool.vehicle.model.VehicleAuctionCommitteeDecisionAddRequest;
import com.dgt.paribahanpool.vehicle.model.VehicleAuctionData;
import com.dgt.paribahanpool.vehicle.repository.VehicleAuctionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class VehicleAuctionService {

    private final VehicleAuctionRepository vehicleAuctionRepository;
    private final DocumentService documentService;

    public Optional<VehicleAuctionData> findById( Long id ){

        return vehicleAuctionRepository.findById( id );
    }

    public VehicleAuctionData save( VehicleAuctionData vehicleAuctionData ) {

        return vehicleAuctionRepository.save( vehicleAuctionData );
    }

    @Transactional
    public VehicleAuctionData getVehicleAuctionDataFromCommitteeDecision( VehicleAuctionData vehicleAuctionData, VehicleAuctionCommitteeDecisionAddRequest vehicleAuctionCommitteeDecisionAddRequest ) {

        vehicleAuctionData.setCommitteePrice( vehicleAuctionCommitteeDecisionAddRequest.getCommitteePrice() );
        vehicleAuctionData.setCommitteeDecision( vehicleAuctionCommitteeDecisionAddRequest.getCommitteeDecision() );

        if( vehicleAuctionData.getDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( vehicleAuctionCommitteeDecisionAddRequest.getCommitteeDecisionFiles(), Collections.EMPTY_MAP );
            vehicleAuctionData.setDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        return vehicleAuctionData;
    }

    @Transactional
    public VehicleAuctionData getVehicleAuctionDataFromBuyerInfo( VehicleAuctionData vehicleAuctionData, VehicleAuctionBuyerInfoAddRequest vehicleAuctionBuyerInfoAddRequest ) {

        vehicleAuctionData.setSoldToNameBn( vehicleAuctionBuyerInfoAddRequest.getSoldToNameBn() );
        vehicleAuctionData.setSoldToNameEn( vehicleAuctionBuyerInfoAddRequest.getSoldToNameEn() );
        vehicleAuctionData.setSoldPrice( vehicleAuctionBuyerInfoAddRequest.getCost() );
        vehicleAuctionData.setSoldAt( vehicleAuctionBuyerInfoAddRequest.getSoldAt() );
        vehicleAuctionData.setSoldAddressEn( vehicleAuctionBuyerInfoAddRequest.getSoldAddressEn() );
        vehicleAuctionData.setSoldToNameBn( vehicleAuctionBuyerInfoAddRequest.getSoldToNameBn() );

        if( vehicleAuctionBuyerInfoAddRequest.getIsForPerOrOrg() == 0 ){

            vehicleAuctionData.setTinNumber( vehicleAuctionBuyerInfoAddRequest.getTinNumber() );
        }
        else {

            vehicleAuctionData.setNidNumber(vehicleAuctionBuyerInfoAddRequest.getNidNumber());
        }
        return vehicleAuctionData;
    }
}
