package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.vehicle.model.DispositionReason;
import com.dgt.paribahanpool.vehicle.model.DispositionReasonAddRequest;
import com.dgt.paribahanpool.vehicle.model.VehicleDisposition;
import com.dgt.paribahanpool.vehicle.model.VehicleServiceAdditionalActionData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class VehicleDispositionService {

    public VehicleDisposition getVehicleDispostionFromServiceAdditionalData( VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData ) {

        VehicleDisposition vehicleDisposition = new VehicleDisposition();

        vehicleDisposition.setLitrePerKilo( vehicleServiceAdditionalActionData.getLitrePerKilo() );
        vehicleDisposition.setVehicleLocation( vehicleServiceAdditionalActionData.getVehicleLocation() );
        vehicleDisposition.setMaintainer( vehicleServiceAdditionalActionData.getMaintainer() );
        vehicleDisposition.setDetails( vehicleServiceAdditionalActionData.getDetails() );
        vehicleDisposition.setOverhaulDetails( vehicleServiceAdditionalActionData.getOverhaulDetails() );
        vehicleDisposition.setKiloDriverAfterOverhaul( vehicleServiceAdditionalActionData.getKiloDriverAfterOverhaul() );
        vehicleDisposition.setKiloDrivenAfterFirstEngineOverhaul( vehicleServiceAdditionalActionData.getKiloDrivenAfterFirstEngineOverhaul() );
        vehicleDisposition.setCostFirstEngineOverhaul( vehicleServiceAdditionalActionData.getCostFirstEngineOverhaul() );
        vehicleDisposition.setKiloDrivenAfterSecondEngineOverhaul( vehicleServiceAdditionalActionData.getKiloDrivenAfterSecondEngineOverhaul() );
        vehicleDisposition.setCostSecondEngineOverhaul( vehicleServiceAdditionalActionData.getCostSecondEngineOverhaul() );
        vehicleDisposition.setKiloDrivenAfterThirdEngineOverhaul( vehicleServiceAdditionalActionData.getKiloDrivenAfterThirdEngineOverhaul() );
        vehicleDisposition.setCostThirdEngineOverhaul( vehicleServiceAdditionalActionData.getCostThirdEngineOverhaul() );
        vehicleDisposition.setCostOtherOverhaul( vehicleServiceAdditionalActionData.getCostOtherOverhaul() );
        vehicleDisposition.setCostAllTillNow( vehicleServiceAdditionalActionData.getCostAllTillNow() );
        vehicleDisposition.setMarketPrice( vehicleServiceAdditionalActionData.getMarketPrice() );
        vehicleDisposition.setBuyableNeededPart( vehicleServiceAdditionalActionData.getBuyableNeededPart() );
        vehicleDisposition.setEstimatedRepairCost( vehicleServiceAdditionalActionData.getEstimatedRepairCost() );
        vehicleDisposition.setReasonForDisposition( vehicleServiceAdditionalActionData.getReasonForDisposition() );

        if( vehicleServiceAdditionalActionData.getDispositionReasonAddRequestList() != null ){

            List<DispositionReason> dispositionReasonList =
                    vehicleServiceAdditionalActionData.getDispositionReasonAddRequestList()
                            .stream()
                            .map( dispositionReasonAddRequest -> getDispositionReasonFromDispositionAddRequest( dispositionReasonAddRequest, vehicleDisposition ) )
                            .filter( dispositionReason -> dispositionReason != null )
                            .collect( Collectors.toList() );

            vehicleDisposition.getDispositionReasonSet().clear();
            vehicleDisposition.getDispositionReasonSet().addAll( dispositionReasonList );

        }

        return vehicleDisposition;

    }

    private DispositionReason getDispositionReasonFromDispositionAddRequest( DispositionReasonAddRequest dispositionReasonAddRequest, VehicleDisposition vehicleDisposition ) {

        DispositionReason dispositionReason = new DispositionReason();

        dispositionReason.setReasonBn( dispositionReasonAddRequest.getReasonBn() );

        return dispositionReason;
    }
}
