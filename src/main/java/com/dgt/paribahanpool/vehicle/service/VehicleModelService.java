package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.color.model.Color;
import com.dgt.paribahanpool.color.service.ColorService;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.employee.model.Employee;
import com.dgt.paribahanpool.enums.GeolocationType;
import com.dgt.paribahanpool.enums.OwnerType;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.exceptions.EmailDataBadRequestException;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.geolocation.model.GeoLocationResponse;
import com.dgt.paribahanpool.geolocation.service.GeoLocationService;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.PublicUserInfo;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.*;
import com.dgt.paribahanpool.vehicle_replace_application.model.VehicleReplaceAddRequest;
import com.dgt.paribahanpool.vendor.model.Vendor;
import com.dgt.paribahanpool.vendor.service.VendorModelService;
import com.dgt.paribahanpool.vendor.service.VendorRepository;
import com.dgt.paribahanpool.vendor.service.VendorService;
import com.dgt.paribahanpool.workflow.model.StateActionMapResponse;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor ( onConstructor_ = { @Autowired } )
public class VehicleModelService {

    private final VehicleService vehicleService;
    private final VendorRepository vendorRepository;
    private final MvcUtil mvcUtil;
    private final VehicleRequisitionApplicationService vehicleRequisitionApplicationService;
    private final StateActionMapService stateActionMapService;
    private final GeoLocationService geoLocationService;
    private final UserService userService;
    private final DocumentService documentService;
    private final BrandService brandService;
    private final ModelService modelService;
    private final ColorService colorService;
    private final VendorService vendorService;

    public void addVehicleGet(Model model ){

        VehicleAddRequest vehicleAddRequest = new VehicleAddRequest();

        List<BrandViewResponse> brandViewResponseList = brandService.getAllBrandViewResponse();
        vehicleAddRequest.setBrandViewResponseList( brandViewResponseList );

        List<Vendor> vendorList = vendorService.allVendors();
        List<String> vendorNameList = new ArrayList<>();

        for(Vendor vendor: vendorList){
            vendorNameList.add(vendor.getVendorName());
        }

        List<Color> allColorsList = colorService.getAllColors();

        model.addAttribute( "vehicleAddRequest", vehicleAddRequest );
        model.addAttribute( "vendorNameList", vendorNameList );
        model.addAttribute( "allColorsList", allColorsList );
    }

    @Transactional( propagation = Propagation.REQUIRED )
    public void editVehicle( Model model, Long vehicleId ) {

        Optional<Vehicle> vehicleOptional = vehicleService.findById(vehicleId);

        if( vehicleOptional.isPresent() ) {

            Vehicle vehicle = vehicleOptional.get();

            VehicleAddRequest vehicleAddRequest = vehicleService.getVehicleAddRequestFromVehicle( vehicleOptional.get() );

            List<BrandViewResponse> brandViewResponseList = brandService.getAllBrandViewResponse();
            List<ModelViewResponse> modelViewResponseList = modelService.getAllModelViewResponseByBrandId( vehicle.getBrand().getId() );

            vehicleAddRequest.setBrandViewResponseList( brandViewResponseList );
            vehicleAddRequest.setModelViewResponseList( modelViewResponseList );

            List<Vendor> vendorList = vendorRepository.findAll();
            List<String> vendorNameList = new ArrayList<>();

            for(Vendor vendor: vendorList){
                vendorNameList.add(vendor.getVendorName());
            }

            List<Color> allColorsList = colorService.getAllColors();

            model.addAttribute( "vehicleAddRequest", vehicleAddRequest );
            model.addAttribute( "vendorNameList", vendorNameList );
            model.addAttribute( "allColorsList", allColorsList );
        }
    }

    public Vehicle addVehiclePost(VehicleAddRequest vehicleAddRequest, Model model ) throws EmailDataBadRequestException {

        Vehicle vehicle = vehicleService.save( vehicleAddRequest );
        mvcUtil.addSuccessMessage( model, "vehicle.add.success" );
        return vehicle;
    }

    public void requisition( Model model, UserPrincipal loggedInUser ) {

        List<GeoLocationResponse> geoLocationList = geoLocationService.findByGeoLocationType( GeolocationType.DIVISION );
        model.addAttribute( "divisionList", geoLocationList );

        User user = loggedInUser.getUser();

        if (user.getUserType().equals(UserType.PUBLIC)) {

            PublicUserInfo publicUserInfo = user.getPublicUserInfo();

            if (publicUserInfo != null) {

                VehicleRequisitionRequest vehicleRequisitionRequest = VehicleRequisitionRequest.builder()
                        .designation(publicUserInfo.getDesignationEn())
                        .eMail(publicUserInfo.getEmail())
                        .identityNumber(publicUserInfo.getServiceId())
                        .name(publicUserInfo.getNameEn())
                        .mobileNumber(publicUserInfo.getPhoneNo())
                        .workingPlace(publicUserInfo.getWorkplaceEn())
                        .build();

                model.addAttribute("vehicleRequisitionRequest", vehicleRequisitionRequest);
            }
        } else {

            Employee employeeInfo = user.getEmployee();

            if (employeeInfo != null) {

                VehicleRequisitionRequest vehicleRequisitionRequest = VehicleRequisitionRequest.builder()
                        .designation(employeeInfo.getDesignation().getNameEn())
                        .eMail(employeeInfo.getEmail())
                        .identityNumber(employeeInfo.getEmployeeID())
                        .name(employeeInfo.getNameEn())
                        .mobileNumber(employeeInfo.getMobileNumber())
                        .workingPlace("")
                        .build();

                model.addAttribute("vehicleRequisitionRequest", vehicleRequisitionRequest);
            }
        }
    }

    public void addRequisition( RedirectAttributes redirectAttributes, VehicleRequisitionRequest vehicleRequisitionRequest, UserPrincipal loggedInUser ) {

        vehicleRequisitionApplicationService.save( vehicleRequisitionRequest );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.vehicle.requisition.add" );
    }

    @Transactional
    public void viewRequisition( Model model, Long id, UserPrincipal loggedInUser ) {

        Optional<VehicleRequisitionApplication> applicationOptional = vehicleRequisitionApplicationService.findById( id );

        if( applicationOptional.isPresent() ) {

            VehicleRequisitionApplication application = applicationOptional.get();

            User currentStateUser = null;

            if( loggedInUser.getUser() != null ) {

                currentStateUser = userService.findById(loggedInUser.getUser().getId()).get();
            }

            String name = "";
            if(application.getOwnerType().equals(OwnerType.USER)) {
                name = application.getName();
            } else if(application.getOwnerType().equals(OwnerType.DISTRICT)) {
                Optional<GeoLocation> geoLocation = geoLocationService.findById(application.getDistrictId());
                name = geoLocation.get().getNameEn();
            } else if(application.getOwnerType().equals(OwnerType.DIVISION)) {
                Optional<GeoLocation> geoLocation = geoLocationService.findById(application.getDivisionId());
                name = geoLocation.get().getNameEn();
            } else {
                Optional<GeoLocation> geoLocation = geoLocationService.findById(application.getUpazilaId());
                name = geoLocation.get().getNameEn();
            }

            User user = userService.findById( application.getCreatedBy() ).get();
            List<DocumentMetadata> signMetaData = documentService.findDocumentMetaDataListByGroupId( user.getSignDocGroupId() );

            DocumentMetadata sign = null;

            if( signMetaData.size() > 0 ){
                sign = signMetaData.get(0) ;
            }

            RequisitionViewResponse requisitionViewResponse = RequisitionViewResponse.builder()
                    .id( application.getId() )
                    .name( application.getName() )
                    .designation( application.getDesignation() )
                    .eMail( application.getEmail() )
                    .identityNumber( application.getServiceId() )
                    .mobileNumber( application.getPhoneNo() )
                    .workingPlace( application.getWorkplace() )
                    .stateType( application.getState().getStateType() )
                    .ownerType( application.getOwnerType() )
                    .comments( application.getComments() )
                    .ownerNameEn( name )
                    .signMetaData( sign  )
//                    .creationDate( application.getCreatedAt().toLocalDate() )
                    .vehicles(
                            application.getVehicleSet().stream()
                                            .map(
                                                    vehicle ->
                                                    RequisitionVehicleViewResponse.builder()
                                                            .id( vehicle.getId() )
                                                            .chassisNumber( vehicle.getChassisNo() )
                                                            .engineNumber( vehicle.getEngineNo() )
                                                            .registrationNumber( vehicle.getRegistrationNo() )
                                                            .modelYear( vehicle.getModelYear() )
                                                            .vehicleType( vehicle.getVehicleType() )
                                                            .build()
                                            )
                                            .collect( Collectors.toList() )
                    )
                    .build();

            if( currentStateUser.getUserType() == UserType.SYSTEM_USER ){

                requisitionViewResponse.setCurrentStateUserInfo(
                        UserResponse.builder()
                                .id( currentStateUser.getId() )
                                .nameBn( currentStateUser.getEmployee().getName() )
                                .nameEn( currentStateUser.getEmployee().getNameEn() )
                                .phoneNumber( currentStateUser.getEmployee().getMobileNumber() )
                                .designation( currentStateUser.getEmployee().getDesignation().getNameBn() )
                                .email( currentStateUser.getEmployee().getEmail() )
                                .build()
                );
            }
            else if( currentStateUser.getUserType() == UserType.PUBLIC ){

                requisitionViewResponse.setCurrentStateUserInfo(
                        UserResponse.builder()
                                .id( currentStateUser.getId() )
                                .nameBn( currentStateUser.getPublicUserInfo().getName() )
                                .nameEn( currentStateUser.getPublicUserInfo().getNameEn() )
                                .phoneNumber( currentStateUser.getPublicUserInfo().getPhoneNo() )
                                .designation( currentStateUser.getPublicUserInfo().getDesignation() )
                                .email( currentStateUser.getPublicUserInfo().getEmail() )
                                .build()
                );
            }

            model.addAttribute("requisitionViewResponse", requisitionViewResponse);
        }

        if( applicationOptional.isPresent() && applicationOptional.get().getState() != null ){

            List<VehicleResponseForSelection> vehicleResponseForSelections = vehicleService.findAllWhereOwnerIsNull();
            model.addAttribute( "vehicleResponseForSelections", vehicleResponseForSelections );


            List<StateActionMapResponse> stateActionMapList = stateActionMapService.findStateActionMapResponseListByCurrentStateAndRoleIdSet( applicationOptional.get().getState().getId(), loggedInUser.getRoleIds() );
            model.addAttribute( "stateActionList", stateActionMapList );

            model.addAttribute( "stateId", applicationOptional.get().getState().getId() );
        }
    }

    public void vehicleRequisitionGet(Model model, Long id) {

        Optional<VehicleRequisitionApplication> application = vehicleRequisitionApplicationService.findById( id );

        VehicleReplaceAddRequest vehicleReplaceAddRequest = new VehicleReplaceAddRequest();

        if( application.isPresent() ) {
            vehicleReplaceAddRequest.setName( application.get().getName() );
            vehicleReplaceAddRequest.setIdentityNumber( application.get().getCreatedBy() );
            vehicleReplaceAddRequest.setDesignation( application.get().getDesignation() );
        }

        model.addAttribute("vehicleReplaceAddRequest", vehicleReplaceAddRequest );
    }

    public void takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, RequisitionAdditionalActionData requisitionAdditionalActionData, UserPrincipal loggedInUser) throws Exception {

        vehicleRequisitionApplicationService.takeAction( id, actionId, requisitionAdditionalActionData, loggedInUser );

        if( actionId == AppConstants.VEHICLE_REQUISITION_APPROVE_ACTION_ID ){

            mvcUtil.addSuccessMessage(redirectAttributes, "success.vehicle.requisition.given");
        }
        else {

            mvcUtil.addSuccessMessage(redirectAttributes, "success.common.action.success");
        }
    }

    @Transactional
    public void addCommitteeDecision( VehicleAuctionCommitteeDecisionAddRequest vehicleAuctionCommitteeDecisionAddRequest, RedirectAttributes redirectAttributes ) {

        vehicleService.saveAuctionCommitteeDecision( vehicleAuctionCommitteeDecisionAddRequest );

        mvcUtil.addSuccessMessage( redirectAttributes, "success.vehicle.committee-decision.add" );
    }

    @Transactional
    public void addBuyerInfo( VehicleAuctionBuyerInfoAddRequest vehicleAuctionBuyerInfoAddRequest, RedirectAttributes redirectAttributes ) {

        vehicleService.saveBuyerInfo( vehicleAuctionBuyerInfoAddRequest );

        mvcUtil.addSuccessMessage( redirectAttributes, "success.vehicle.buyer-info.add" );
    }

    @Transactional
    public void getAuctionViewResponseFromVehicleDisposition(Model model, Long id) {

        VehicleAuctionViewResponse vehicleAuctionViewResponse = vehicleService.getAuctionViewResponseFromVehicleDispostion( id );

        model.addAttribute( "vehicleAuctionViewResponse", vehicleAuctionViewResponse );
    }
}
