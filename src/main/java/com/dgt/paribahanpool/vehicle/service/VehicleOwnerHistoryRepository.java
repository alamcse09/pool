package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.vehicle.model.VehicleOwnerHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VehicleOwnerHistoryRepository extends JpaRepository<VehicleOwnerHistory, Long> {

    List<VehicleOwnerHistory> findByUser(User user);

    List<VehicleOwnerHistory> findByGeoLocation(GeoLocation geolocation);
}
