package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.vehicle.model.VehicleOwnerHistory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class VehicleOwnerHistoryService {

    private final VehicleOwnerHistoryRepository vehicleOwnerHistoryRepository;

    public VehicleOwnerHistory save( VehicleOwnerHistory vehicleOwnerHistory ){

        return vehicleOwnerHistoryRepository.save( vehicleOwnerHistory );
    }

    public List<VehicleOwnerHistory> findOwnerHistoryForUser( User user ){

        return vehicleOwnerHistoryRepository.findByUser( user );
    }

    public List<VehicleOwnerHistory> findOwnerHistoryForGeolocation( GeoLocation geolocation ){

        return vehicleOwnerHistoryRepository.findByGeoLocation( geolocation );
    }
}
