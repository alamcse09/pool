package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.enums.OwnerType;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.geolocation.service.GeoLocationService;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.vehicle.model.*;
import com.dgt.paribahanpool.vehicle.repository.VehicleOwnerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.dgt.paribahanpool.vehicle.service.VehicleSpecification.filterByVehicleOwnerId;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class VehicleOwnerService {

    private final VehicleOwnerRepository vehicleOwnerRepository;
    private final GeoLocationService geoLocationService;
    private final VehicleService vehicleService;
    private final VehicleOwnerHistoryService vehicleOwnerHistoryService;

    public Optional<VehicleOwner> findById(Long id ){

        return vehicleOwnerRepository.findById( id );
    }

    public VehicleOwner save( VehicleOwner vehicleOwner ){

        return vehicleOwnerRepository.save( vehicleOwner );
    }

    public List<VehicleOwner> findByUserId( Long id ){

        return vehicleOwnerRepository.findByUserId( id );
    }

    @Transactional
    public RestValidationResult findByOwnerType(Integer ownerTypeValue, User user, Long geoLocationId) throws NotFoundException {

        OwnerType ownerType = null;

        if( ownerTypeValue == 0 ){

            ownerType = OwnerType.DIVISION;

            Optional<VehicleOwner> vehicleOwnerOptional = findByGeolocationId( geoLocationId );

            if( !vehicleOwnerOptional.isPresent() ){

                throw new NotFoundException( "validation.vehicle.no.data" );
            }
        }
        else if( ownerTypeValue == 1 ){

            ownerType = OwnerType.DISTRICT;

            Optional<VehicleOwner> vehicleOwnerOptional = findByGeolocationId( geoLocationId );

            if( !vehicleOwnerOptional.isPresent() ){

                throw new NotFoundException( "validation.vehicle.no.data" );
            }
        }
        else if( ownerTypeValue == 2 ){

            ownerType = OwnerType.UPAZILA;

            Optional<VehicleOwner> vehicleOwnerOptional = findByGeolocationId( geoLocationId );

            if( !vehicleOwnerOptional.isPresent() ){

                throw new NotFoundException( "validation.vehicle.no.data" );
            }
        }
        else if( ownerTypeValue == 3 ) {

            ownerType = OwnerType.USER;

            Optional<VehicleOwner> vehicleOwnerOptional = findByOwnerTypeAndUser(ownerType, user );

            if (!vehicleOwnerOptional.isPresent()) {

                throw new NotFoundException("validation.vehicle.no.data");
            }
        }

        return RestValidationResult.valid();
    }

    private Optional<VehicleOwner> findByGeolocationId(Long geoLocationId) {

        Optional<GeoLocation> geoLocationOptional = geoLocationService.findById( geoLocationId );

        Optional<VehicleOwner> vehicleOwnerOptional = null;

        if( geoLocationOptional.isPresent() ){

            vehicleOwnerOptional = vehicleOwnerRepository.findByGeoLocation( geoLocationOptional.get() );
        }

        return vehicleOwnerOptional;
    }

    private Optional<VehicleOwner> findByOwnerTypeAndUser(OwnerType ownerType, User user) {

        return vehicleOwnerRepository.findByOwnerTypeAndUser( ownerType, user );
    }

    public Integer findVehicleCountOfOwnerType( Integer ownerTypeValue, User user, Long geoLocationId ) {

        OwnerType ownerType = null;

        List<Vehicle> vehicleList = new ArrayList<>();

        if( ownerTypeValue == 0 ){

            ownerType = OwnerType.DIVISION;

            Optional<VehicleOwner> vehicleOwnerOptional = findByGeolocationId( geoLocationId );

            if( vehicleOwnerOptional.isPresent() ) {

                vehicleList = vehicleService.findAllByVehicleOwnerId( vehicleOwnerOptional.get().getId() );
            }
        }
        else if( ownerTypeValue == 1 ){

            ownerType = OwnerType.DISTRICT;

            Optional<VehicleOwner> vehicleOwnerOptional = findByGeolocationId( geoLocationId );

            if( vehicleOwnerOptional.isPresent() ) {

                vehicleList = vehicleService.findAllByVehicleOwnerId( vehicleOwnerOptional.get().getId() );
            }
        }
        else if( ownerTypeValue == 2 ){

            ownerType = OwnerType.UPAZILA;

            Optional<VehicleOwner> vehicleOwnerOptional = findByGeolocationId( geoLocationId );

            if( vehicleOwnerOptional.isPresent() ) {

                vehicleList = vehicleService.findAllByVehicleOwnerId( vehicleOwnerOptional.get().getId() );
            }
        }
        else if( ownerTypeValue == 3 ){

            ownerType = OwnerType.USER;

            Optional<VehicleOwner> vehicleOwnerOptional = findByOwnerTypeAndUser(ownerType, user );

            if( vehicleOwnerOptional.isPresent() ) {

                vehicleList = vehicleService.findAllByVehicleOwnerId( vehicleOwnerOptional.get().getId() );
            }
        }

        return (vehicleList.size());

    }

    public Optional<VehicleOwner> findGeolocationByOwnerTypeValueAndRequisitionReport(Long ownerTypeValue, VehicleRequisitionReportData requisitionReportData) {

        if( ownerTypeValue == 0L ){

            Optional<GeoLocation> geoLocationOptional = geoLocationService.findById( requisitionReportData.getDivisionValue() );

            if(geoLocationOptional.isPresent() ){

                return vehicleOwnerRepository.findByGeoLocation( geoLocationOptional.get() );
            }
        }
        else if( ownerTypeValue == 1L ){

            Optional<GeoLocation> geoLocationOptional = geoLocationService.findById( requisitionReportData.getDistrictValue() );

            if(geoLocationOptional.isPresent() ){

                return vehicleOwnerRepository.findByGeoLocation( geoLocationOptional.get() );
            }
        }
        else if( ownerTypeValue == 2L ){

            Optional<GeoLocation> geoLocationOptional = geoLocationService.findById( requisitionReportData.getUpazilaValue() );

            if(geoLocationOptional.isPresent() ){

                return vehicleOwnerRepository.findByGeoLocation( geoLocationOptional.get() );
            }
        }

        return null;
    }

    @Transactional
    public List<VehicleViewResponse> findAllAssignedVehicle(Integer ownerTypeVal, User user, Long geoLocationId) {

        OwnerType ownerType = null;

        List<VehicleViewResponse> vehicleViewResponseList = new ArrayList<>();

        if( ownerTypeVal == 0 ){

            ownerType = OwnerType.DIVISION;

            Optional<VehicleOwner> vehicleOwnerOptional = findByGeolocationId( geoLocationId );

            if( vehicleOwnerOptional.isPresent() ) {

                List<Vehicle> vehicleList = vehicleService.findAllByVehicleOwnerId( vehicleOwnerOptional.get().getId() );

                vehicleViewResponseList = getVehicleResponseListFromVehicleList( vehicleList );
            }
        }
        else if( ownerTypeVal == 1 ){

            ownerType = OwnerType.DISTRICT;

            Optional<VehicleOwner> vehicleOwnerOptional = findByGeolocationId( geoLocationId );

            if( vehicleOwnerOptional.isPresent() ) {

                List<Vehicle> vehicleList = vehicleService.findAllByVehicleOwnerId( vehicleOwnerOptional.get().getId() );

                vehicleViewResponseList = getVehicleResponseListFromVehicleList( vehicleList );
            }
        }
        else if( ownerTypeVal == 2 ){

            ownerType = OwnerType.UPAZILA;

            Optional<VehicleOwner> vehicleOwnerOptional = findByGeolocationId( geoLocationId );

            if( vehicleOwnerOptional.isPresent() ) {

                List<Vehicle> vehicleList = vehicleService.findAllByVehicleOwnerId( vehicleOwnerOptional.get().getId() );

                vehicleViewResponseList = getVehicleResponseListFromVehicleList( vehicleList );
            }

            return vehicleViewResponseList;
        }
        else if( ownerTypeVal == 3 ){

            ownerType = OwnerType.USER;

            List<Vehicle> vehicleList = new ArrayList<>();

            Optional<VehicleOwner> vehicleOwnerOptional = findByOwnerTypeAndUser(ownerType, user );

            if( vehicleOwnerOptional.isPresent() ) {

                vehicleList = vehicleService.findAllByVehicleOwnerId( vehicleOwnerOptional.get().getId() );
            }

            vehicleViewResponseList = getVehicleResponseListFromVehicleList( vehicleList );
        }

        return vehicleViewResponseList;
    }

    private List<VehicleViewResponse> getVehicleResponseListFromVehicleList(List<Vehicle> vehicleList) {

       return vehicleList.stream().map(vehicle -> {

                            return vehicleService.getVehicleViewResponseFromVehicle(vehicle);

                        }
                )
                .collect(Collectors.toList());
    }

    public void logVehicleGiven(VehicleOwner vehicleOwner, Vehicle vehicle) {

        VehicleOwnerHistory vehicleOwnerHistory = new VehicleOwnerHistory();

        vehicleOwnerHistory.setVehicle( vehicle );
        vehicleOwnerHistory.setOwnerType( vehicleOwner.getOwnerType() );
        vehicleOwnerHistory.setGeoLocation( vehicleOwner.getGeoLocation() );
        vehicleOwnerHistory.setUser( vehicleOwner.getUser() );
        vehicleOwnerHistory.setAssignedAt( LocalDateTime.now() );

        vehicleOwnerHistoryService.save( vehicleOwnerHistory );
    }

    private List<VehicleViewResponse> getVehicleListFromVehicleOwnerList(List<VehicleOwner> vehicleOwnerList) {

        List<Vehicle> vehicleList = new ArrayList<>();

        for( VehicleOwner vehicleOwner : vehicleOwnerList ){

            vehicleList.addAll( vehicleService.findAllByVehicleOwnerId( vehicleOwner.getId() ) );
        }

        return vehicleList.stream().map( vehicle -> {

            return vehicleService.getVehicleViewResponseFromVehicle( vehicle );

            }
        )
        .collect(Collectors.toList());
    }
}
