package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.enums.WarrantyType;
import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import com.dgt.paribahanpool.vehicle.model.*;
import com.dgt.paribahanpool.vehicle.repository.VehiclePartsRepository;
import com.dgt.paribahanpool.vendor.model.Vendor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class VehiclePartsService {

    private final VehiclePartsRepository vehiclePartsRepository;
    private final EntityManager entityManager;

    public List<VehicleParts> getActivePartsOfVehicle( Vehicle vehicle ){

        return vehiclePartsRepository.findByVehicleAndIsActive( vehicle, true );
    }

    public List<VehiclePartsViewResponse> getActiveVehiclePartsViewResponseByVehicleId(Long vehicleId ){

        Vehicle vehicle = entityManager.getReference( Vehicle.class, vehicleId );
        return getActiveVehiclePartsViewResponseByVehicle( vehicle );
    }

    @Transactional( propagation = Propagation.REQUIRED )
    public List<VehiclePartsViewResponse> getActiveVehiclePartsViewResponseByVehicle( Vehicle vehicle ){

        return getActivePartsOfVehicle( vehicle )
                .stream()
                .map( this::getVehiclePartsViewResponseFromVehicleParts )
                .collect( Collectors.toList() );
    }

    public VehiclePartsViewResponse getVehiclePartsViewResponseFromVehicleParts( VehicleParts vehicleParts ){

        InventoryItem inventoryItem = vehicleParts.getInventoryItem();
        Vehicle vehicle = vehicleParts.getVehicle();
        Vendor vendor = inventoryItem.getVendor();

        return VehiclePartsViewResponse.builder()
                .vehiclePartsId( vehicleParts.getId() )
                .vehicleId( vehicle.getId() )
                .inventoryItemId( inventoryItem.getId() )
                .inventoryItemNameEn( inventoryItem.getProductNameEn() )
                .inventoryItemNameBn( inventoryItem.getProductNameBn() )
                .vehicleRegNo( vehicle.getRegistrationNo() )
                .chassisNo( vehicle.getChassisNo() )
                .engineNo( vehicle.getEngineNo() )
                .useDate( vehicleParts.getUseDate() )
                .vendorNameBn( vendor.getVendorName() )
                .vendorNameEn( vendor.getVendorNameEn() )
                .warrantyEndDate( vehicleParts.getWarrantyEndDate() )
                .warrantyStartDate( vehicleParts.getWarrantyStartDate() )
                .warrantyType( WarrantyType.getLabel( inventoryItem.getWarrantyType() ) )
                .build();

    }

    @Transactional( propagation = Propagation.REQUIRED )
    public List<VehicleParts> save( VehiclePartsAdd vehiclePartsAdd ){

        InventoryItem inventoryItem = vehiclePartsAdd.getInventoryItem();

        WarrantyType warrantyType = inventoryItem.getWarrantyType();

        LocalDate warrantyStartDate = inventoryItem.getPurchaseDate();
        if( warrantyType == WarrantyType.FROM_USING_DATE ){

            warrantyStartDate = vehiclePartsAdd.getUseDate();
        }

        LocalDate warrantyEndDate = warrantyStartDate.plusDays( inventoryItem.getWarrantyPeriod() == null ? 0: inventoryItem.getWarrantyPeriod() );

        List<VehicleParts> vehiclePartsList = new ArrayList<>();

        for( int i = 0; i<vehiclePartsAdd.getCount(); i++ ){

            VehicleParts vehicleParts = new VehicleParts();

            vehicleParts.setVehicle( vehiclePartsAdd.getVehicle() );
            vehicleParts.setJob( vehiclePartsAdd.getJob() );
            vehicleParts.setVehicleServiceApplication( vehiclePartsAdd.getVehicleServiceApplication() );
            vehicleParts.setInventoryItem( vehiclePartsAdd.getInventoryItem() );
            vehicleParts.setWarrantyStartDate( warrantyStartDate );
            vehicleParts.setWarrantyEndDate( warrantyEndDate );
            vehicleParts.setUseDate( vehiclePartsAdd.getUseDate() );
            vehicleParts.setIsActive( false );

            vehiclePartsList.add( vehicleParts );
        }

        return vehiclePartsRepository.saveAll( vehiclePartsList );
    }

    @Transactional( propagation = Propagation.REQUIRED )
    public void saveFromVehicleServiceApplicationAndJobSet(VehicleServiceApplication vehicleServiceApplication, Set<Job> jobSet ) {

        Vehicle vehicle = vehicleServiceApplication.getVehicle();

        for( Job job : jobSet ) {

            List<JobInventoryItem> jobInventoryItemList = job.getJobInventoryItems();

            for( JobInventoryItem jobInventoryItem : jobInventoryItemList ) {

                VehiclePartsAdd vehiclePartsAdd = VehiclePartsAdd.builder()
                        .vehicle( vehicle )
                        .inventoryItem( jobInventoryItem.getInventoryItem() )
                        .job( job )
                        .vehicleServiceApplication( vehicleServiceApplication )
                        .count( jobInventoryItem.getQuantity().intValue() )
                        .useDate(LocalDateTime.now().toLocalDate() )
                        .build();

                save( vehiclePartsAdd );

            }

        }
    }

    public Optional<VehicleParts> findById(Long vehicleId) {

        return vehiclePartsRepository.findById( vehicleId );
    }

    public List<VehicleParts> findVehiclePartsByServiceApplication(VehicleServiceApplication vehicleServiceApplication) {

        return vehiclePartsRepository.findByVehicleServiceApplication( vehicleServiceApplication );
    }

    public void saveAll(List<VehicleParts> vehiclePartsList) {

        vehiclePartsRepository.saveAll( vehiclePartsList );
    }
}
