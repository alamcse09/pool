package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.vehicle.model.VehicleRequisitionApplication;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class VehicleRequisitionApplicaitonValidaitonService {

    private final VehicleRequisitionApplicationService vehicleRequisitionApplicationService;

    public RestValidationResult delete(Long id) throws NotFoundException {

        Boolean exist = vehicleRequisitionApplicationService.existById( id );
        if( !exist ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }
}
