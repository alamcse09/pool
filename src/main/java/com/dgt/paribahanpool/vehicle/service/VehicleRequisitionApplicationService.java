package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.enums.EmployeeType;
import com.dgt.paribahanpool.enums.OwnerType;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.geolocation.service.GeoLocationService;
import com.dgt.paribahanpool.locale.LocalizeUtil;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.vehicle.model.*;
import com.dgt.paribahanpool.workflow.model.*;
import com.dgt.paribahanpool.document.model.Document;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.vehicle.repository.VehicleRequisitioApplicationRepository;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.StateResponse;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.dgt.paribahanpool.vehicle.service.VehicleRequisitionSpecification.filterByCreatedBy;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class VehicleRequisitionApplicationService implements WorkflowService<VehicleRequisitionApplication> {

    private final VehicleRequisitioApplicationRepository vehicleRequisitioApplicationRepository;
    private final StateService stateService;
    private final StateActionMapService stateActionMapService;
    private final VehicleTypeCounterService vehicleTypeCounterService;
    private final DocumentService documentService;
    private final VehicleService vehicleService;
    private final UserService userService;
    private final GeoLocationService geoLocationService;
    private final LocalizeUtil localizeUtil;
    private final VehicleOwnerService vehicleOwnerService;

    public Optional<VehicleRequisitionApplication> findById(Long id ) {

        return vehicleRequisitioApplicationRepository.findById( id );
    }

    public VehicleRequisitionApplication save( VehicleRequisitionApplication vehicleRequisitionApplication ){

        return vehicleRequisitioApplicationRepository.save( vehicleRequisitionApplication );
    }

    @Transactional
    public VehicleRequisitionApplication save( VehicleRequisitionRequest vehicleRequisitionRequest ){

        VehicleRequisitionApplication vehicleRequisitionApplication = getVehicleRequisitionApplicationFromRequest( vehicleRequisitionRequest );
        return save( vehicleRequisitionApplication );
    }

    private VehicleRequisitionApplication getVehicleRequisitionApplicationFromRequest(VehicleRequisitionRequest vehicleRequisitionRequest) {

        VehicleRequisitionApplication vehicleRequisitionApplication = new VehicleRequisitionApplication();

        vehicleRequisitionApplication.setDesignation( vehicleRequisitionRequest.getDesignation() );
        vehicleRequisitionApplication.setEmail( vehicleRequisitionRequest.getEMail() );
        vehicleRequisitionApplication.setServiceId( vehicleRequisitionRequest.getIdentityNumber() );
        vehicleRequisitionApplication.setName( vehicleRequisitionRequest.getName() );
        vehicleRequisitionApplication.setPhoneNo( vehicleRequisitionRequest.getMobileNumber() );
        vehicleRequisitionApplication.setWorkplace( vehicleRequisitionRequest.getWorkingPlace() );

        vehicleRequisitionApplication.setOwnerType( getOwnerTypeFromEmployeeType( vehicleRequisitionRequest.getOwnerType() ) );

        vehicleRequisitionApplication.setComments( vehicleRequisitionRequest.getComments() );
        vehicleRequisitionApplication.setDivisionId( vehicleRequisitionRequest.getDivisionValue() );
        vehicleRequisitionApplication.setDistrictId( vehicleRequisitionRequest.getDistrictValue() );
        vehicleRequisitionApplication.setUpazilaId( vehicleRequisitionRequest.getUpazilaValue() );

        if( vehicleRequisitionRequest.getVehicleTypeCounterAddRequests() != null ) {

            Set<VehicleTypeCounter> vehicleTypeCounterSet = vehicleRequisitionRequest.getVehicleTypeCounterAddRequests()
                    .stream()
                    .map(this::getVehicleTypeCounterFromVehicleTypeCounterAddRequest)
                    .filter( vehicleTypeCounter -> vehicleTypeCounter!=null)
                    .collect(Collectors.toSet());

            vehicleRequisitionApplication.getVehicleTypeCounterSet().clear();
            vehicleRequisitionApplication.getVehicleTypeCounterSet().addAll( vehicleTypeCounterSet );
        }


        if( vehicleRequisitionApplication.getState() == null ){

            State state = stateService.findStateReferenceById( AppConstants.VEHICLE_REQUISITION_INIT_STATE_ID);
            vehicleRequisitionApplication.setState( state );
        }

        if( vehicleRequisitionApplication.getDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( vehicleRequisitionRequest.getFiles(), Collections.EMPTY_MAP );
            vehicleRequisitionApplication.setDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( vehicleRequisitionRequest.getFiles(), vehicleRequisitionApplication.getDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        return vehicleRequisitionApplication;
    }

    private OwnerType getOwnerTypeFromEmployeeType(EmployeeType ownerType) {

        switch ( ownerType ){
            case VVIP:
            case PROBATIONARY:
            case ADMINSTRATIVE:
                return OwnerType.USER;
            case UPAZILA:
                return OwnerType.UPAZILA;
            case DISTRICT:
                return OwnerType.DISTRICT;
            case DIVISION:
                return OwnerType.DIVISION;
        }

        return null;
    }

    private VehicleTypeCounter getVehicleTypeCounterFromVehicleTypeCounterAddRequest(VehicleTypeCounterAddRequest vehicleTypeCounterAddRequest) {

        VehicleTypeCounter vehicleTypeCounter = new VehicleTypeCounter();

        if( vehicleTypeCounterAddRequest.getId() != null ) {

            Optional<VehicleTypeCounter> vehicleTypeCounterOptional = vehicleTypeCounterService.findById(vehicleTypeCounterAddRequest.getId());

            if( vehicleTypeCounterOptional.isPresent() ) {

                vehicleTypeCounter = vehicleTypeCounterOptional.get();
            } else {

                return null;
            }

        }

        vehicleTypeCounter.setVehicleType( vehicleTypeCounterAddRequest.getVehicleType() );
        vehicleTypeCounter.setCounter( vehicleTypeCounterAddRequest.getCounter() );

        return vehicleTypeCounter;
    }

    public DataTablesOutput<VehicleRequisitionSearchResponse> search( DataTablesInput dataTablesInput, HttpServletRequest request ) {

        return vehicleRequisitioApplicationRepository.findAll( dataTablesInput, vehicleRequisitionApplication -> getVehicleRequisitionSearchResponseFromVehicleRequisitionApplication( vehicleRequisitionApplication, request ) );
    }

    public DataTablesOutput<VehicleRequisitionSearchResponse> search(DataTablesInput dataTablesInput, HttpServletRequest request, Long id) {

        return vehicleRequisitioApplicationRepository.findAll( dataTablesInput, filterByCreatedBy( id ), null, vehicleRequisitionApplication -> getVehicleRequisitionSearchResponseFromVehicleRequisitionApplication( vehicleRequisitionApplication, request ) );
    }

    private VehicleRequisitionSearchResponse getVehicleRequisitionSearchResponseFromVehicleRequisitionApplication( VehicleRequisitionApplication vehicleRequisitionApplication, HttpServletRequest request  ) {

        String ownerTypeLabel = localizeUtil.getMessageFromMessageSource( OwnerType.getLabel(vehicleRequisitionApplication.getOwnerType()), request );

        VehicleRequisitionSearchResponse vehicleRequisitionSearchResponse = VehicleRequisitionSearchResponse.builder()
                .designation( vehicleRequisitionApplication.getDesignation() )
                .name( vehicleRequisitionApplication.getName() )
                .email( vehicleRequisitionApplication.getEmail() )
                .serviceId( vehicleRequisitionApplication.getServiceId() )
                .phoneNo( vehicleRequisitionApplication.getPhoneNo() )
                .workplace( vehicleRequisitionApplication.getWorkplace() )
                .id( vehicleRequisitionApplication.getId() )
                .ownerType( ownerTypeLabel )
                .build();

        State state = vehicleRequisitionApplication.getState();
        if( state != null ){

            StateResponse stateResponse = StateResponse.builder()
                    .id( state.getId() )
                    .name( state.getName() )
                    .nameEn( state.getNameEn() )
                    .stateType( state.getStateType() )
                    .build();
            vehicleRequisitionSearchResponse.setStateResponse( stateResponse );
        }

        return vehicleRequisitionSearchResponse;
    }

    @Transactional
    public void takeAction(Long id, Long actionId, RequisitionAdditionalActionData requisitionAdditionalActionData, UserPrincipal loggedInUser) throws Exception {

        stateActionMapService.takeAction( this, id, actionId, loggedInUser, ( workflowEntity ) ->  setAdditionalData( workflowEntity, actionId, requisitionAdditionalActionData ) );
    }

    private RequisitionAdditionalActionData setAdditionalData( WorkflowEntity workflowEntity, Long actionId, RequisitionAdditionalActionData requisitionAdditionalActionData ){

        VehicleRequisitionApplication vehicleRequisitionApplication = ( VehicleRequisitionApplication ) workflowEntity;

        if( actionId == AppConstants.VEHICLE_REQUISITION_CHOOSE_ACTION_ID && requisitionAdditionalActionData.getVehicleId() != null && requisitionAdditionalActionData.getVehicleId().size() > 0 ) {

            Set<Vehicle> vehicleSet = new HashSet<>();

            requisitionAdditionalActionData.getVehicleId().forEach( (vehicleId) -> {

                Vehicle vehicle = vehicleService.findReferenceById( vehicleId );
                vehicleSet.add( vehicle );
            });

            vehicleRequisitionApplication.getVehicleSet().addAll( vehicleSet );
        } else if( actionId == AppConstants.VEHICLE_REQUISITION_APPROVE_ACTION_ID ) {

            Set<Vehicle> vehicleSet = ((VehicleRequisitionApplication) workflowEntity).getVehicleSet();

            vehicleSet.forEach( (vehicle) -> {
                setVehicleOwnerFromRequisitionApplication(vehicle, vehicleRequisitionApplication);
                vehicleService.save( vehicle );
            });
        }
        else if( actionId == AppConstants.VEHICLE_REQUISITION_REJECT_ACTION_ID ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( requisitionAdditionalActionData.getRejectReasonsReportFiles(), Collections.EMPTY_MAP );
            vehicleRequisitionApplication.setRejectReasonReportGroupId( documentUploadedResponse.getDocGroupId() );
        }
        if( vehicleRequisitionApplication.getState().getId() == AppConstants.VEHICLE_REQUISITION_DIRECTOR_STATE_ID || vehicleRequisitionApplication.getState().getId() == AppConstants.VEHICLE_REQUISITION_DD_STATE_ID ){

            if( requisitionAdditionalActionData.getVehiclesToDettach() != null && requisitionAdditionalActionData.getVehiclesToDettach().length > 0 ) {

                detachVehiclesWhichAlreadyAssigned(vehicleRequisitionApplication, requisitionAdditionalActionData.getVehiclesToDettach());

                Set<Vehicle> vehicleSet = new HashSet<>();

                requisitionAdditionalActionData.getVehicleId().forEach( (vehicleId) -> {

                    Vehicle vehicle = vehicleService.findReferenceById( vehicleId );
                    vehicleSet.add( vehicle );
                });
            }
        }

        return requisitionAdditionalActionData;
    }

    private void detachVehiclesWhichAlreadyAssigned( VehicleRequisitionApplication vehicleRequisitionApplication, Long[] vehiclesToDettach ) {

        if( vehiclesToDettach != null ) {
            
            for (Long vehicleId : vehiclesToDettach) {

                Optional<Vehicle> optionalVehicle = vehicleService.findById(vehicleId);

                if (optionalVehicle.isPresent()) {

                    vehicleRequisitionApplication.getVehicleSet().remove(optionalVehicle.get());
                }
            }
        }
    }

    private void setVehicleOwnerFromRequisitionApplication(Vehicle vehicle, VehicleRequisitionApplication vehicleRequisitionApplication) {

        VehicleOwner vehicleOwner = new VehicleOwner();

        vehicleOwner.setOwnerType( vehicleRequisitionApplication.getOwnerType() );

        if( vehicleRequisitionApplication.getOwnerType().equals( OwnerType.USER )) {

            Optional<User> user = userService.findByUsername(vehicleRequisitionApplication.getEmail());

            if(user.isPresent()) {

                vehicleOwner.setUser( user.get() );
            }
        } else if( vehicleRequisitionApplication.getOwnerType().equals( OwnerType.DISTRICT )) {

            Optional<GeoLocation> geoLocation = geoLocationService.findById( vehicleRequisitionApplication.getDistrictId() );

            if( geoLocation.isPresent() ) {

                vehicleOwner.setGeoLocation( geoLocation.get() );
            }
        } else if( vehicleRequisitionApplication.getOwnerType().equals( OwnerType.DIVISION ) ) {

            Optional<GeoLocation> geoLocation = geoLocationService.findById( vehicleRequisitionApplication.divisionId );

            if( geoLocation.isPresent() ) {

                vehicleOwner.setGeoLocation( geoLocation.get() );
            }
        } else if( vehicleRequisitionApplication.getOwnerType().equals( OwnerType.UPAZILA ) ) {

            Optional<GeoLocation> geoLocation = geoLocationService.findById( vehicleRequisitionApplication.upazilaId );

            if( geoLocation.isPresent() ) {

                vehicleOwner.setGeoLocation( geoLocation.get() );
            }
        }

        vehicle.setVehicleOwner( vehicleOwner );

        vehicleOwnerService.logVehicleGiven( vehicleOwner, vehicle );
    }

    public Boolean existById(Long id) {

        return vehicleRequisitioApplicationRepository.existsById( id );
    }

    public void delete( Long id ) {

        vehicleRequisitioApplicationRepository.deleteById( id );
    }
}
