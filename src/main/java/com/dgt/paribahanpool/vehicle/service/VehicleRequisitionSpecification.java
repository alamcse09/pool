package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.vehicle.model.VehicleRequisitionApplication;
import com.dgt.paribahanpool.vehicle.model.VehicleRequisitionApplication_;
import org.springframework.data.jpa.domain.Specification;

public class VehicleRequisitionSpecification {

    public static Specification<VehicleRequisitionApplication> filterByCreatedBy(Long id ){

        if( id == null || id <= 0 )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(VehicleRequisitionApplication_.CREATED_BY ), id );
    }
}
