package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.color.service.ColorService;
import com.dgt.paribahanpool.document.model.Document;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.driver.model.Driver;
import com.dgt.paribahanpool.driver_assign.model.DriverAssignSearchResponse;
import com.dgt.paribahanpool.driver_assign.model.DriverSearchResponse;
import com.dgt.paribahanpool.enums.*;
import com.dgt.paribahanpool.locale.LocalizeUtil;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.NameResponse;
import com.dgt.paribahanpool.vehicle.model.*;
import com.dgt.paribahanpool.vehicle.repository.VehicleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static com.dgt.paribahanpool.vehicle.service.VehicleSpecification.*;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired } )
public class VehicleService {

    private final LocalizeUtil localizeUtil;
    private final VehicleRepository vehicleRepository;
    private final DocumentService documentService;
    private final BrandService brandService;
    private final ModelService modelService;
    private final ColorService colorService;

    private final VehicleAuctionService vehicleAuctionService;
    
    public Vehicle save( Vehicle vehicle ){

        return vehicleRepository.save( vehicle );
    }

    public void delete( Long id ) {

        vehicleRepository.deleteById( id );
    }

    public Optional<Vehicle> findById(Long id ){

        return vehicleRepository.findById( id );
    }

    public Vehicle findReferenceById( Long id ){

        return vehicleRepository.getById( id );
    }

    public Boolean existById(Long id ) {

        return vehicleRepository.existsById( id );
    }

    @Transactional
    public Vehicle save( VehicleAddRequest vehicleAddRequest ){

        Vehicle vehicle = new Vehicle();

        if( vehicleAddRequest.getId() != null ) {

            vehicle = findById( vehicleAddRequest.getId() ).get();
            getVehicleFromVehicleAddRequest( vehicle, vehicleAddRequest );

        } else {

            getVehicleFromVehicleAddRequest( vehicle, vehicleAddRequest );
        }

        return save( vehicle );
    }

    public Vehicle getVehicleFromVehicleAddRequest( Vehicle vehicle, VehicleAddRequest vehicleAddRequest ) {

        vehicle.setColor( vehicleAddRequest.getColor() );
        vehicle.setModelYear( vehicleAddRequest.getModelYear() );
        vehicle.setRegistrationNo( vehicleAddRequest.getRegistrationNo() );
        vehicle.setChassisNo( vehicleAddRequest.getChassisNo() );
        vehicle.setEngineNo( vehicleAddRequest.getEngineNo() );
        vehicle.setRegistrationType( vehicleAddRequest.getRegistrationType() );
        vehicle.setRegDate( vehicleAddRequest.getRegDate() );
        vehicle.setTaxTokenDate( vehicleAddRequest.getTaxTokenDate() );
        vehicle.setTaxTokenDateEnd( vehicleAddRequest.getTaxTokenDateEnd() );
        vehicle.setFitnessDate( vehicleAddRequest.getFitnessDate() );
        vehicle.setFitnessDateEnd( vehicleAddRequest.getFitnessDateEnd() );
        vehicle.setEngineCC( vehicleAddRequest.getEngineCC() );
        vehicle.setSeatCapacity( vehicleAddRequest.getSeatCapacity() );
        vehicle.setSalesOrganizationName( vehicleAddRequest.getSalesOrganizationName() );
        vehicle.setVehicleType( vehicleAddRequest.getVehicleType() );
        vehicle.setFuelType( vehicleAddRequest.getFuelType() );

        if( vehicleAddRequest.getBrandId() != null ){

            Optional<Brand> brandOptional = brandService.findById( vehicleAddRequest.getBrandId() );

            if( brandOptional.isPresent() ){

                vehicle.setBrand( brandOptional.get() );
            }
        }

        if( vehicleAddRequest.getModelId() != null ){

            Optional<Model> modelOptional = modelService.findById( vehicleAddRequest.getModelId() );

            if( modelOptional.isPresent() ){

                vehicle.setModel( modelOptional.get() );
            }
        }

        if( vehicle.getDocGroupId() == null ) {

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( vehicleAddRequest.getFiles(), Collections.EMPTY_MAP );
            vehicle.setDocGroupId(documentUploadedResponse.getDocGroupId());
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( vehicleAddRequest.getFiles(), vehicle.getDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        return vehicle;
    }

    public VehicleAddRequest getVehicleAddRequestFromVehicle( Vehicle vehicle ) {

        VehicleAddRequest vehicleAddRequest = new VehicleAddRequest();

        vehicleAddRequest.setId( vehicle.getId() );
        vehicleAddRequest.setBrandId( vehicle.getBrand().getId() );
        vehicleAddRequest.setModelId( vehicle.getBrand().getId() );
        vehicleAddRequest.setColor( vehicle.getColor() );
        vehicleAddRequest.setModelYear( vehicle.getModelYear() );
        vehicleAddRequest.setRegistrationNo( vehicle.getRegistrationNo() );
        vehicleAddRequest.setChassisNo( vehicle.getChassisNo() );
        vehicleAddRequest.setEngineNo( vehicle.getEngineNo() );
        vehicleAddRequest.setRegistrationType( vehicle.getRegistrationType() );
        vehicleAddRequest.setRegDate( vehicle.getRegDate() );
        vehicleAddRequest.setTaxTokenDate( vehicle.getTaxTokenDate() );
        vehicleAddRequest.setTaxTokenDateEnd( vehicle.getTaxTokenDateEnd() );
        vehicleAddRequest.setFitnessDate( vehicle.getFitnessDate() );
        vehicleAddRequest.setFitnessDateEnd( vehicle.getFitnessDateEnd() );
        vehicleAddRequest.setEngineCC( vehicle.getEngineCC() );
        vehicleAddRequest.setSeatCapacity( vehicle.getSeatCapacity() );
        vehicleAddRequest.setSalesOrganizationName( vehicle.getSalesOrganizationName() );
        vehicleAddRequest.setVehicleType( vehicle.getVehicleType() );
        vehicleAddRequest.setFuelType( vehicle.getFuelType() );

        if( vehicle.getDocGroupId() != null && vehicle.getDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( vehicle.getDocGroupId() );
            vehicleAddRequest.setDocuments( documentMetadataList );
        }

        return vehicleAddRequest;
    }

    public VehicleSearchResponse getVehicleSearchResponseFromVehicle( Vehicle vehicle ){

        VehicleSearchResponse vehicleSearchResponse = new VehicleSearchResponse();

        BrandViewResponse brandViewResponse = BrandViewResponse.builder()
                .nameEn((vehicle.getBrand()==null || vehicle.getBrand().getNameEn()==null) ? "N/A" : vehicle.getBrand().getNameEn())
                .nameBn((vehicle.getBrand()==null || vehicle.getBrand().getNameBn()==null) ? "N/A" : vehicle.getBrand().getNameBn())
                .build();

        vehicleSearchResponse.setId( vehicle.getId() );
        vehicleSearchResponse.setBrand( brandViewResponse);
        vehicleSearchResponse.setColor( vehicle.getColor() != null && colorService.getColorById( vehicle.getColor() ).isPresent() ? colorService.getColorById(vehicle.getColor()).get().getColorNameBn() : "" );
        vehicleSearchResponse.setModelYear( vehicle.getModelYear() );
        vehicleSearchResponse.setRegistrationNo( vehicle.getRegistrationNo() );
        vehicleSearchResponse.setChassisNo( vehicle.getChassisNo() );
        vehicleSearchResponse.setEngineNo( vehicle.getEngineNo() );
        vehicleSearchResponse.setRegistrationType( vehicle.getRegistrationType() );
        vehicleSearchResponse.setRegDate( vehicle.getRegDate() );
        vehicleSearchResponse.setTaxTokenDate( vehicle.getTaxTokenDate() );
        vehicleSearchResponse.setFitnessDate( vehicle.getFitnessDate() );
        vehicleSearchResponse.setFitnessDateEnd( vehicle.getFitnessDateEnd() );
        vehicleSearchResponse.setEngineCC( vehicle.getEngineCC() );
        vehicleSearchResponse.setSeatCapacity( vehicle.getSeatCapacity() );
        vehicleSearchResponse.setSalesOrganizationName( vehicle.getSalesOrganizationName() );
        vehicleSearchResponse.setVehicleType( vehicle.getVehicleType() );
//        vehicleSearchResponse.setFuelType( vehicle.getFuelType() );

        if( vehicle.getVehicleOwner() != null && vehicle.getVehicleOwner().getOwnerType() == OwnerType.USER ) {

            User user = vehicle.getVehicleOwner().getUser();
            String nameEn = null;
            String nameBn = null;

            if( user != null ){

                if( user.getUserType() == UserType.PUBLIC ){

                    nameEn = user.getPublicUserInfo().getNameEn();
                    nameBn = user.getPublicUserInfo().getName();
                }
                else if( user.getUserType() == UserType.SYSTEM_USER ){

                    nameEn = user.getEmployee().getNameEn();
                    nameBn = user.getEmployee().getName();
                }
            }

            VehicleOwnerSearchResponse vehicleUserSearchResponse = VehicleOwnerSearchResponse.builder()
                    .ownerNameEn( nameEn== null? "N/A" : nameEn )
                    .ownerNameBn( nameBn== null? "N/A" : nameBn )
                    .build();

            vehicleSearchResponse.setVehicleOwner(vehicleUserSearchResponse);
        }
        else {

            VehicleOwnerSearchResponse vehicleUserSearchResponse = VehicleOwnerSearchResponse.builder()
                    .ownerNameEn((vehicle.getVehicleOwner() == null || vehicle.getVehicleOwner().getGeoLocation() == null || vehicle.getVehicleOwner().getGeoLocation().getNameEn() == null ) ? "N/A" : vehicle.getVehicleOwner().getGeoLocation().getNameEn() )
                    .ownerNameBn((vehicle.getVehicleOwner() == null || vehicle.getVehicleOwner().getGeoLocation() == null || vehicle.getVehicleOwner().getGeoLocation().getNameBn() == null ) ? "N/A" : vehicle.getVehicleOwner().getGeoLocation().getNameBn() )
                    .build();

            vehicleSearchResponse.setVehicleOwner(vehicleUserSearchResponse);
        }

        DriverSearchResponse driverSearchResponse = DriverSearchResponse.builder()
                .name( (vehicle.getDriver()==null) ? "N/A" : vehicle.getDriver().getName() )
                .phoneNumber( (vehicle.getDriver()==null) ? "N/A" : vehicle.getDriver().getName() )
                .build();

        vehicleSearchResponse.setDriver( driverSearchResponse );

        List<DocumentMetadata> documentDataList = documentService.findDocumentMetaDataListByGroupId( vehicle.getDocGroupId() );
        vehicleSearchResponse.setDocumentMetadataList( documentDataList );

        return vehicleSearchResponse;
    }

    public DataTablesOutput<VehicleSearchResponse> searchForDatatable( DataTablesInput dataTablesInput ) {

        return vehicleRepository.findAll( dataTablesInput, this::getVehicleSearchResponseFromVehicle );
    }

    public List<Vehicle> getVehicleByRegistrationNumber( String registrationNumber ) {

        Specification<Vehicle> vehicleSpecification = VehicleSpecification.filterByRegistrationNumber( registrationNumber );

        return  vehicleRepository.findAll( vehicleSpecification );
    }

    public List<Vehicle> getVehicleByEngineNumberAndChassisNumber( String engineNumber, String chassisNumber ) {

        Specification<Vehicle> vehicleSpecification = VehicleSpecification.filterByEngineNumber( engineNumber.trim() ).and( filterByChassisNumber( chassisNumber.trim() ) );

        return  vehicleRepository.findAll(vehicleSpecification);
    }

    public DataTablesOutput<VehicleAuctionResponse> searchForAuction( DataTablesInput dataTablesInput, HttpServletRequest request ) {

        Specification<Vehicle> vehicleFilterByStatus = filterByStatus( Status.PENDING ).or( filterByStatus( Status.AUCTIONABLE )  ).or( filterByStatus( Status.SOLD ) );

        return vehicleRepository.findAll( dataTablesInput, null, vehicleFilterByStatus, ( vehicle ) -> getVehicleAuctionResponseFromVehicle( vehicle, request ) );
    }

    private VehicleAuctionResponse getVehicleAuctionResponseFromVehicle( Vehicle vehicle, HttpServletRequest request ) {

        VehicleAuctionData vehicleAuctionData = null;
        if( vehicle.getVehicleAuctionData() != null ) {
            vehicleAuctionData = vehicle.getVehicleAuctionData();
        }

        String status = vehicle.getStatus() == null? "": localizeUtil.getMessageFromMessageSource( Status.getLabel( vehicle.getStatus() ), request );

        BrandViewResponse brandViewResponse = BrandViewResponse.builder()
                .nameEn((vehicle.getBrand()==null || vehicle.getBrand().getNameEn()==null) ? "N/A" : vehicle.getBrand().getNameEn())
                .nameBn((vehicle.getBrand()==null || vehicle.getBrand().getNameBn()==null) ? "N/A" : vehicle.getBrand().getNameBn())
                .build();

        return VehicleAuctionResponse.builder()
                .status( status )
                .statusType( vehicle.getStatus() )
                .brand( brandViewResponse )
                .buyer(
                        vehicleAuctionData == null? NameResponse.builder().build():
                                NameResponse.builder()
                                        .nameBn( vehicleAuctionData.getSoldToNameBn() )
                                        .nameEn( vehicleAuctionData.getSoldToNameEn() )
                                        .build()
                )
                .chassisNo( vehicle.getChassisNo() )
                .committeeDecision( vehicleAuctionData == null? "": vehicleAuctionData.getCommitteeDecision() )
                .modelYear( vehicle.getModelYear() )
                .vendor(
                        NameResponse.builder()
                                .nameBn( vehicle.getSalesOrganizationName() )
                                .nameEn( vehicle.getSalesOrganizationNameEn() )
                        .build()
                )
                .buyDate( LocalDate.now() ) //TODO remove hardcoded date
                .price( vehicleAuctionData == null? null: vehicleAuctionData.getSoldPrice() )
                .registrationNo( vehicle.getRegistrationNo() )
                .id( vehicle.getId() )
                .build();
    }

    public Optional<Vehicle> findByRegistrationNo( String regNo ) {

        return vehicleRepository.findByRegistrationNo( regNo );
    }

    public Boolean existsByDriverId( Long driverId ){

        Specification<Vehicle> filterByDriverIdSpecification = filterByDriverId( driverId );
        Page<Vehicle> vehiclePage = vehicleRepository.findAll( filterByDriverIdSpecification, PageRequest.of( 0, 1 ) );
        return vehiclePage.getTotalElements() > 0;
    }

    @Transactional
    public DataTablesOutput<DriverAssignSearchResponse> searchForDataTable( DataTablesInput dataTablesInput ) {

        Specification<Vehicle> searchSpecification = filterByDriverNotNull();
        return vehicleRepository.findAll( dataTablesInput, null, searchSpecification, this::getDriverAssignSearchResponseFromVehicle );
    }

    private DriverAssignSearchResponse getDriverAssignSearchResponseFromVehicle( Vehicle vehicle ) {

        Driver driver = vehicle.getDriver();

        if( driver != null ) {

            DriverSearchResponse driverSearchResponse = DriverSearchResponse.builder()
                    .id( driver.getId() )
                    .name( driver.getName() )
                    .educationalQualification( driver.getEducationalQualification() )
                    .totalExperienceDuration( driver.getTotalExperienceDuration() )
                    .build();

            return DriverAssignSearchResponse.builder()
                    .driver( driverSearchResponse )
                    .chassisNo(vehicle.getChassisNo())
                    .engineNo(vehicle.getEngineNo())
                    .registrationNo(vehicle.getRegistrationNo())
                    .id( vehicle.getId() )
                    .build();
        }

        return null;
    }

    public void removeDriver(Long vehicleId, Long driverId) {

        Optional<Vehicle> vehicleOptional = findById( vehicleId );

        if( vehicleOptional.isPresent() ){

            Vehicle vehicle = vehicleOptional.get();
            vehicle.setDriver( null );
            save( vehicle );
        }
    }

    public void saveAuctionCommitteeDecision( VehicleAuctionCommitteeDecisionAddRequest vehicleAuctionCommitteeDecisionAddRequest ) {

        Optional<Vehicle> vehicleOptional = findById( vehicleAuctionCommitteeDecisionAddRequest.getCommitteeDecisionId() );

        VehicleAuctionData vehicleAuctionData = new VehicleAuctionData();

        if( vehicleOptional.isPresent() ){

            Vehicle vehicle = vehicleOptional.get();

            if( vehicle.getVehicleAuctionData() != null ) {

                vehicleAuctionData = vehicleAuctionService.getVehicleAuctionDataFromCommitteeDecision( vehicle.getVehicleAuctionData(), vehicleAuctionCommitteeDecisionAddRequest );
            }
            else{

                vehicleAuctionData = vehicleAuctionService.getVehicleAuctionDataFromCommitteeDecision( vehicleAuctionData, vehicleAuctionCommitteeDecisionAddRequest );
            }

            vehicle.setVehicleAuctionData( vehicleAuctionData );

            if( vehicle.getStatus() == Status.PENDING ){

                vehicle.setStatus( Status.AUCTIONABLE );
            }

            save( vehicle );
        }
    }

    public void saveBuyerInfo( VehicleAuctionBuyerInfoAddRequest vehicleAuctionBuyerInfoAddRequest ) {

        Optional<Vehicle> vehicleOptional = findById( vehicleAuctionBuyerInfoAddRequest.getBuyerFromVehicleId() );

        VehicleAuctionData vehicleAuctionData = new VehicleAuctionData();

        if( vehicleOptional.isPresent() ){

            Vehicle vehicle = vehicleOptional.get();

            if( vehicle.getVehicleAuctionData() != null ) {

                vehicleAuctionData = vehicleAuctionService.getVehicleAuctionDataFromBuyerInfo( vehicle.getVehicleAuctionData(), vehicleAuctionBuyerInfoAddRequest );
            }
            else{

                vehicleAuctionData = vehicleAuctionService.getVehicleAuctionDataFromBuyerInfo( vehicleAuctionData, vehicleAuctionBuyerInfoAddRequest );
            }

            vehicle.setVehicleAuctionData( vehicleAuctionData );

            if( vehicle.getStatus() != Status.SOLD ){

                vehicle.setStatus( Status.SOLD );
            }

            save( vehicle );
        }
    }

    @Transactional
    public VehicleAuctionCommitteeDecisionAddRequest getCommitteeDecisionAddRequestFromCommitteeDecisionId( Long committeeDecisionId ) {

        VehicleAuctionCommitteeDecisionAddRequest vehicleAuctionCommitteeDecisionAddRequest = new VehicleAuctionCommitteeDecisionAddRequest();

        Optional<Vehicle> vehicleOptional = findById( committeeDecisionId );

        Vehicle vehicle = vehicleOptional.get();

        VehicleAuctionData vehicleAuctionData = vehicle.getVehicleAuctionData();

        vehicleAuctionCommitteeDecisionAddRequest.setCommitteeDecision( vehicleAuctionData.getCommitteeDecision() );
        vehicleAuctionCommitteeDecisionAddRequest.setCommitteePrice( vehicleAuctionData.getCommitteePrice() );

        if( vehicleAuctionData.getDocGroupId() != null && vehicleAuctionData.getDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( vehicleAuctionData.getDocGroupId() );
            vehicleAuctionCommitteeDecisionAddRequest.setDocuments( documentMetadataList );
        }

        return vehicleAuctionCommitteeDecisionAddRequest;
    }

    public List<Vehicle> findVehicleForFitnessAlert( LocalDate localDate ){

        return vehicleRepository.findByFitnessDateAlertTypeNotSent( AlertType.FITNESS, EntityType.VEHICLE, localDate );
    }

    public List<Vehicle> findVehicleForTaxTokenAlert( LocalDate taxTokenExpiryDate ) {

        return vehicleRepository.findByTaxTokenDateAlertTypeNotSent( AlertType.TAX_TOKEN, EntityType.VEHICLE, taxTokenExpiryDate );
    }

    public List<VehicleResponseForSelection> findAllWhereOwnerIsNull() {

        List<Vehicle> vehicleList = vehicleRepository.findByVehicleOwnerIsNull();
        return vehicleList.stream().map( this::getVehicleResponseForSelectionFromVehicle ).collect(Collectors.toList() );
    }

    public List<VehicleResponseForSelection> findByVehicleAuctionDataIsNullAndVehicleDispositionIsNull(){

        List<Vehicle> vehicleList = vehicleRepository.findAllByVehicleAuctionDataIsNullAndVehicleDispositionIsNull();
        return vehicleList.stream().map( this::getVehicleResponseForSelectionFromVehicle ).collect(Collectors.toList() );
    }

    private VehicleResponseForSelection getVehicleResponseForSelectionFromVehicle( Vehicle vehicle ) {

        return VehicleResponseForSelection.builder()
                .id( vehicle.getId() )
                .text( vehicle.getRegistrationNo() )
                .build();
    }

    public VehicleViewResponse getVehicleViewResponseFromVehicle( Vehicle vehicle ) {

        return VehicleViewResponse.builder()
                .id( vehicle.getId() )
                .modelYear( vehicle.getModelYear() )
                .brand(  (vehicle.getBrand() != null) ? vehicle.getBrand().getNameBn() : "" )
                .color( colorService.getColorById(vehicle.getColor()).isPresent() ? colorService.getColorById(vehicle.getColor()).get().getColorNameBn() : "" )
                .registrationNo( vehicle.getRegistrationNo() )
                .chassisNo( vehicle.getChassisNo() )
                .engineNo( vehicle.getEngineNo() )
                .registrationNo( vehicle.getRegistrationNo() )
                .regDate( vehicle.getRegDate() )
                .taxTokenDate( vehicle.getTaxTokenDate() )
                .fitnessDate( vehicle.getFitnessDate() )
                .fitnessDateEnd( vehicle.getFitnessDateEnd() )
                .engineCC( vehicle.getEngineCC() )
                .seatCapacity( vehicle.getSeatCapacity() )
                .salesOrganizationName( vehicle.getSalesOrganizationName() )
                .vehicleType( vehicle.getVehicleType() )
                .fuelType( vehicle.getFuelType() )
                .build();
    }

    public void saveVehicleDispostionAndVehicleServiceApplication(VehicleDisposition vehicleDisposition, VehicleServiceApplication vehicleServiceApplication) {

        Optional<Vehicle> vehicleOptional = findByRegistrationNo( vehicleServiceApplication.getRegNo() );

        if( vehicleOptional.isPresent() ) {

            Vehicle vehicle = getVehicleFromVehicleServiceApplication(vehicleOptional.get(), vehicleServiceApplication);

            vehicle.setVehicleDisposition(vehicleDisposition);

            save( vehicle );

        }

    }

    private Vehicle getVehicleFromVehicleServiceApplication( Vehicle vehicle, VehicleServiceApplication vehicleServiceApplication ) {

        vehicle.setRegistrationNo( vehicleServiceApplication.getRegNo() );
        vehicle.setChassisNo( vehicleServiceApplication.getChassisNo() );
        vehicle.setStatus( Status.AUCTIONABLE );
        return vehicle;
    }

    public Long getNumberOfVehicle() {

        return vehicleRepository.count();
    }

    public Long getNumberOfFreeVehicle() {

        return vehicleRepository.countByVehicleOwnerIsNullAndStatusIsNot( Status.SOLD );
    }

    public List<Vehicle> getAllAssignedDrivers() {

        Specification<Vehicle> vehicleSpecification = filterByDriverNotNull();

        return findAll( vehicleSpecification );
    }

    private List<Vehicle> findAll(){

        return vehicleRepository.findAll();
    }

    public List<Vehicle> findAll(Specification<Vehicle> vehicleSpecification) {

        return vehicleRepository.findAll( vehicleSpecification );
    }

    public List<Driver> getAllFreeDrivers(List<Driver> driverList) {

        List<Driver> returningDriverList = new ArrayList<>();

        for( Driver driver : driverList ){

            Optional<Vehicle> vehicleOptional = findByDriver( driver );

            if( !vehicleOptional.isPresent() ){

                returningDriverList.add( driver );
            }
        }

        return returningDriverList;
    }

    private Optional<Vehicle> findByDriver(Driver driver) {

        return vehicleRepository.findByDriver( driver );
    }

    @Transactional( propagation = Propagation.REQUIRED )
    public List<VehicleViewResponse> getVehicleViewResponseList() {

        List<Vehicle> vehicleList = findAll();

        return getVehicleViewResponseListFromVehicle( vehicleList );

    }

    private List<VehicleViewResponse> getVehicleViewResponseListFromVehicle(List<Vehicle> vehicleList) {

        return vehicleList
                .stream()
                .map( this::getVehicleViewResponseFromVehicle )
                .collect( Collectors.toList() );
    }

    public VehicleAuctionViewResponse getAuctionViewResponseFromVehicleDispostion( Long id ) {

        Optional<Vehicle> vehicleOptional = findById( id );

        if( vehicleOptional.isPresent() ){

            Vehicle vehicle = vehicleOptional.get();

            if( vehicle.getVehicleDisposition() != null ){

                VehicleDisposition vehicleDisposition = vehicle.getVehicleDisposition();

                return VehicleAuctionViewResponse.builder()
                        .id( vehicleDisposition.getId() )
                        .litrePerKilo( vehicleDisposition.getLitrePerKilo() )
                        .vehicleLocation( vehicleDisposition.getVehicleLocation() )
                        .maintainer( vehicleDisposition.getMaintainer() )
                        .details( vehicleDisposition.getDetails() )
                        .overhaulDetails( vehicleDisposition.getOverhaulDetails() )
                        .kiloDriverAfterOverhaul( vehicleDisposition.getKiloDriverAfterOverhaul() )
                        .kiloDrivenAfterFirstEngineOverhaul( vehicleDisposition.getKiloDrivenAfterFirstEngineOverhaul() )
                        .costFirstEngineOverhaul( vehicleDisposition.getCostFirstEngineOverhaul() )
                        .kiloDrivenAfterSecondEngineOverhaul( vehicleDisposition.getKiloDrivenAfterSecondEngineOverhaul() )
                        .costSecondEngineOverhaul( vehicleDisposition.getCostSecondEngineOverhaul() )
                        .kiloDrivenAfterThirdEngineOverhaul( vehicleDisposition.getKiloDrivenAfterThirdEngineOverhaul() )
                        .costThirdEngineOverhaul( vehicleDisposition.getCostThirdEngineOverhaul() )
                        .costOtherOverhaul( vehicleDisposition.getCostOtherOverhaul() )
                        .costAllTillNow(vehicleDisposition.getCostAllTillNow() )
                        .marketPrice(  vehicleDisposition.getMarketPrice() )
                        .buyableNeededPart( vehicleDisposition.getBuyableNeededPart() )
                        .estimatedRepairCost( vehicleDisposition.getEstimatedRepairCost() )
                        .dispositionViewResponseList( vehicleDisposition.getDispositionReasonSet().stream().map(
                                reason ->
                                    DispositionViewResponse.builder()
                                            .id(reason.getId() )
                                            .reasonBn(reason.getReasonBn() )
                                            .build()
                                )
                                .collect(Collectors.toList())
                        )
                        .build();
            }

        }

        return null;
    }

    public List<Vehicle> findAllByVehicleOwnerId(Long ownerId) {

        return vehicleRepository.findAllByVehicleOwnerId( ownerId );
    }
}