package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.inventory_item.service.InventoryCategoryService;
import com.dgt.paribahanpool.inventory_item.service.InventoryItemService;
import com.dgt.paribahanpool.mechanic.model.Mechanic;
import com.dgt.paribahanpool.mechanic.model.MechanicAddRequest;
import com.dgt.paribahanpool.mechanic.service.MechanicService;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.*;
import com.dgt.paribahanpool.workflow.model.StateActionMapResponse;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class VehicleServiceApplicationModelService {

    private final VehicleServiceApplicationService vehicleServiceApplicationService;
    private final StateActionMapService stateActionMapService;
    private final MvcUtil mvcUtil;
    private final MechanicService mechanicService;
    private final UserService userService;
    private final VehicleService vehicleService;
    private final InventoryCategoryService inventoryCategoryService;
    private final VehiclePartsService vehiclePartsService;
    private final JobTypeService jobTypeService;

    @Transactional
    public void addPage( Model model ){

        List<VehicleViewResponse> vehicleViewResponseList = vehicleService.getVehicleViewResponseList();

        model.addAttribute( "vehicleServiceAddRequest", new VehicleServiceAddRequest() );

        model.addAttribute( "vehicleViewResponseList", vehicleViewResponseList );
    }

    public VehicleServiceApplication addVehiclePost( VehicleServiceAddRequest vehicleServiceAddRequest, Model model ) {

        VehicleServiceApplication vehicleServiceApplication = vehicleServiceApplicationService.save( vehicleServiceAddRequest );
        mvcUtil.addSuccessMessage( model, "vehicle.add.success" );
        return vehicleServiceApplication;
    }

    @Transactional
    public void view(Model model, Long id, UserPrincipal loggedInUser) {

        Optional<VehicleServiceApplication> vehicleServiceApplication = vehicleServiceApplicationService.findById( id );

        if( vehicleServiceApplication.isPresent() ) {

            VehicleServiceApplicationViewResponse vehicleServiceApplicationViewResponse = vehicleServiceApplicationService.getVehicleServiceApplicationViewResponseFromVehicleServiceApplication( vehicleServiceApplication.get() );
            model.addAttribute( "vehicleServiceApplicationViewResponse", vehicleServiceApplicationViewResponse );

            model.addAttribute("viewAction","true");

            model.addAttribute("modelId", vehicleServiceApplicationViewResponse.getModelId());

            if( vehicleServiceApplicationViewResponse.getStateId() != null ){

//                if(vehicleServiceApplicationViewResponse.getStateId() == AppConstants.JOB_APPROVED_STATE_ID) {
//                    model.addAttribute("approved","true");
//                }

                List<StateActionMapResponse> stateActionMapList = stateActionMapService.findStateActionMapResponseListByCurrentStateAndRoleIdSet( vehicleServiceApplicationViewResponse.getStateId(), loggedInUser.getRoleIds() );
                model.addAttribute( "stateActionList", stateActionMapList );

                model.addAttribute( "dispositionActionId", AppConstants.VEHICLE_SERVICE_DISPOSITION_ACTION_ID );
            }

            List<VehiclePartsViewResponse> vehiclePartsViewResponseList = vehiclePartsService.getActiveVehiclePartsViewResponseByVehicle( vehicleServiceApplication.get().getVehicle() );
            model.addAttribute( "vehicleParts", vehiclePartsViewResponseList );

            List<JobTypeResponse> jobTypeResponseList = jobTypeService.findAllJobTypeResponse();
            model.addAttribute( "jobTypeList", jobTypeResponseList );
        }

        setMechanic(model);

        model.addAttribute( "mechanicInChargeList", userService.getMechanicInChargeUser() );

        model.addAttribute( "workshopManagerList", userService.getWorkshopManagerUser() );

//        model.addAttribute( "inventoryItems", inventoryItemService.getInventoryItems() );

        model.addAttribute( "jobAssistantList", userService.getJobAssistantUser() );

        model.addAttribute( "inventoryCategory", inventoryCategoryService.getAllInventoryCategoryViewResponse() );

        model.addAttribute( "vehicleServiceApplicationViewResponse", vehicleServiceApplicationService.getVehicleServiceViewResponse( id ) );
    }

    public void takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData, UserPrincipal loggedInUser) throws Exception {

        vehicleServiceApplicationService.takeAction( id, actionId, vehicleServiceAdditionalActionData, loggedInUser );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.common.action.success" );
    }

    private void setMechanic(Model model) {

        List<Mechanic> mechanicList = mechanicService.findAll();

        List<MechanicAddRequest> mechanicAddRequestList = mechanicList.stream()
                .map(this::getMechanicAddRequestFromMechanic)
                .collect( Collectors.toList());

        model.addAttribute("mechanicList", mechanicAddRequestList);
    }

    private MechanicAddRequest getMechanicAddRequestFromMechanic(Mechanic mechanic) {

        MechanicAddRequest mechanicAddRequest = new MechanicAddRequest();
        mechanicAddRequest.setId( mechanic.getId() );
        mechanicAddRequest.setNameEn( mechanic.getName() );
        mechanicAddRequest.setNameBn( mechanic.getName() );

        return mechanicAddRequest;
    }

    public void addMoreProduct(RedirectAttributes redirectAttributes, Long id, Long jobId, VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData, UserPrincipal loggedInUser) {

        vehicleServiceApplicationService.addMoreProduct( id, jobId, vehicleServiceAdditionalActionData, loggedInUser );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.common.action.success" );
    }

    @Transactional
    public void getGatePass(Model model, Long id, UserPrincipal loggedInUser) {

        Optional<VehicleServiceApplication> vehicleServiceApplicationOptional = vehicleServiceApplicationService.findById( id );

        if( vehicleServiceApplicationOptional.isPresent() ){

            VehicleServiceApplicationViewResponse vehicleServiceApplicationViewResponse = vehicleServiceApplicationService.getVehicleServiceViewResponse( id );
            model.addAttribute( "vehicleServiceApplicationViewResponse", vehicleServiceApplicationViewResponse );
        }
    }
}
