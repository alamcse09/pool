package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.driver.model.Driver;
import com.dgt.paribahanpool.driver.service.DriverService;
import com.dgt.paribahanpool.enums.ProductType;
import com.dgt.paribahanpool.enums.Unit;
import com.dgt.paribahanpool.enums.VehicleType;
import com.dgt.paribahanpool.incident.model.Incident;
import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import com.dgt.paribahanpool.inventory_item.service.InventoryItemService;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.vehicle.model.*;
import com.dgt.paribahanpool.vehicle.repository.VehicleServiceApplicationRepository;
import com.dgt.paribahanpool.workflow.model.*;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.dgt.paribahanpool.vehicle.service.VehicleServiceApplicationSpecification.filterByMechanicOrSectionLeaderUserId;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class VehicleServiceApplicationService implements WorkflowService<VehicleServiceApplication> {

    private final VehicleServiceApplicationRepository vehicleServiceApplicationRepository;
    private final StateService stateService;
    private final VehicleService vehicleService;
    private final DriverService driverService;
    private final StateActionMapService stateActionMapService;
    private final JobService jobService;
    private final InventoryItemService inventoryItemService;
    private final UserService userService;
    private final VehicleDispositionService vehicleDispositionService;
    private final DocumentService documentService;
    private final VehicleServiceCostService vehicleServiceCostService;
    private final VehiclePartsService vehiclePartsService;

    public Optional<VehicleServiceApplication> findById( Long id ) {

        return vehicleServiceApplicationRepository.findById( id );
    }

    public VehicleServiceApplication save( VehicleServiceApplication vehicleServiceApplication ){

        return vehicleServiceApplicationRepository.save( vehicleServiceApplication );
    }

    @Transactional
    public VehicleServiceApplication save( VehicleServiceAddRequest vehicleServiceAddRequest ) {

        VehicleServiceApplication vehicleServiceApplication = new VehicleServiceApplication();

        if( vehicleServiceAddRequest.getId() != null ) {

            Optional<VehicleServiceApplication> vehicleServiceApplicationOptional = vehicleServiceApplicationRepository.findById( vehicleServiceAddRequest.getId() );

            if( vehicleServiceApplicationOptional.isPresent() ){

                vehicleServiceApplication = vehicleServiceApplicationOptional.get();
            }
        }

        vehicleServiceApplication = getVehicleServiceApplicationFromAddRequest( vehicleServiceApplication, vehicleServiceAddRequest );

        return save( vehicleServiceApplication );
    }

    private VehicleServiceApplication getVehicleServiceApplicationFromAddRequest(VehicleServiceApplication vehicleServiceApplication, VehicleServiceAddRequest vehicleServiceAddRequest) {

        vehicleServiceApplication.setServiceType( vehicleServiceAddRequest.getServiceType() );
        vehicleServiceApplication.setServiceCategory( vehicleServiceAddRequest.getServiceCategory() );
        vehicleServiceApplication.setDesignationBn( vehicleServiceAddRequest.getDesignationBn() );
        vehicleServiceApplication.setDesignationEn( vehicleServiceAddRequest.getDesignationEn() );
        vehicleServiceApplication.setDriverNameBn( vehicleServiceAddRequest.getDriverNameBn() );
        vehicleServiceApplication.setDriverNameEn( vehicleServiceAddRequest.getDriverNameEn() );
        vehicleServiceApplication.setDriverPhoneNo( vehicleServiceAddRequest.getDriverPhoneNumber() );
        vehicleServiceApplication.setChassisNo( vehicleServiceAddRequest.getChassisNo() );
        vehicleServiceApplication.setRegNo( vehicleServiceAddRequest.getRegNo() );
        vehicleServiceApplication.setVehicleUserNameBn( vehicleServiceAddRequest.getVehicleUserNameBn() );
        vehicleServiceApplication.setVehicleUserNameEn( vehicleServiceAddRequest.getVehicleUserNameEn() );
        vehicleServiceApplication.setVehicleUserPhoneNo( vehicleServiceAddRequest.getVehicleUserPhoneNumber() );
        vehicleServiceApplication.setWorkplaceBn( vehicleServiceAddRequest.getWorkplaceBn() );
        vehicleServiceApplication.setWorkplaceEn( vehicleServiceAddRequest.getWorkplaceEn() );
        vehicleServiceApplication.setServiceDate( LocalDate.now() );
        vehicleServiceApplication.setVehicleProblemsDetails( vehicleServiceAddRequest.getVehicleProblemsDetails() );


        if( vehicleServiceApplication.getState() == null ){

            State state = stateService.findStateReferenceById( AppConstants.VEHICLE_SERVICE_INIT_STATE_ID );
            vehicleServiceApplication.setState( state );
        }

        if( vehicleServiceAddRequest.getVehicleId() != null ){

            Optional<Vehicle> vehicleOptional = vehicleService.findById( vehicleServiceAddRequest.getVehicleId() );
            if( vehicleOptional.isPresent() ){

                Vehicle vehicle = vehicleOptional.get();
                vehicleServiceApplication.setVehicle( vehicle );
            }
        }

        if( vehicleServiceAddRequest.getDriverId() != null ){

            Optional<Driver> driverOptional = driverService.findById( vehicleServiceAddRequest.getDriverId() );
            if( driverOptional.isPresent() ){

                Driver driver = driverOptional.get();
                vehicleServiceApplication.setDriver( driver );
            }
        }

        return vehicleServiceApplication;
    }

    public DataTablesOutput<VehicleServiceSearchResponse> searchForDatatable(DataTablesInput dataTablesInput ) {

        return vehicleServiceApplicationRepository.findAll( dataTablesInput, this::getVehicleServiceSearchResponseFromVehicleServiceApplication );
    }

    private VehicleServiceSearchResponse getVehicleServiceSearchResponseFromVehicleServiceApplication(VehicleServiceApplication vehicleServiceApplication) {

        VehicleServiceSearchResponse vehicleServiceSearchResponse = new VehicleServiceSearchResponse();

        Vehicle vehicle = vehicleServiceApplication.getVehicle();

        vehicleServiceSearchResponse.setId( vehicleServiceApplication.getId() );
        vehicleServiceSearchResponse.setRegNo( vehicleServiceApplication.getRegNo() == null ? vehicle.getRegistrationNo() : vehicleServiceApplication.getRegNo() );
        vehicleServiceSearchResponse.setChassisNo( vehicleServiceApplication.getChassisNo() == null ? vehicle.getChassisNo() : vehicleServiceApplication.getChassisNo() );
        vehicleServiceSearchResponse.setVehicleUserNameBn( vehicleServiceApplication.getVehicleUserNameBn() );
        vehicleServiceSearchResponse.setVehicleUserNameEn( vehicleServiceApplication.getVehicleUserNameEn() );
        vehicleServiceSearchResponse.setDesignationBn( vehicleServiceApplication.getDesignationBn() );
        vehicleServiceSearchResponse.setDesignationEn( vehicleServiceApplication.getDesignationEn() );

        State state = vehicleServiceApplication.getState();
        if( state != null ){

            StateResponse stateResponse = StateResponse.builder()
                    .id( state.getId() )
                    .name( state.getName() )
                    .nameEn( state.getNameEn() )
                    .stateType( state.getStateType() )
                    .build();
            vehicleServiceSearchResponse.setStateResponse( stateResponse );
        }

        return vehicleServiceSearchResponse;
    }

    public VehicleServiceApplicationViewResponse getVehicleServiceApplicationViewResponseFromVehicleServiceApplication( VehicleServiceApplication vehicleServiceApplication ) {

        VehicleServiceApplicationViewResponse vehicleServiceApplicationViewResponse = new VehicleServiceApplicationViewResponse();

        User user = userService.findById( vehicleServiceApplication.getCreatedBy() ).get();
        List<DocumentMetadata> signMetaData = documentService.findDocumentMetaDataListByGroupId( user.getSignDocGroupId() );

        List<DocumentMetadata> driverCommentDocuments = documentService.findDocumentMetaDataListByGroupId( vehicleServiceApplication.getDriverCommentDocGroupId() );

        vehicleServiceApplicationViewResponse.setId( vehicleServiceApplication.getId() );
        vehicleServiceApplicationViewResponse.setRegNo( vehicleServiceApplication.getRegNo() );
        vehicleServiceApplicationViewResponse.setChassisNo( vehicleServiceApplication.getChassisNo() );
        vehicleServiceApplicationViewResponse.setVehicleUserNameBn( vehicleServiceApplication.getVehicleUserNameBn() );
        vehicleServiceApplicationViewResponse.setVehicleUserNameEn( vehicleServiceApplication.getVehicleUserNameEn() );
        vehicleServiceApplicationViewResponse.setDesignationBn( vehicleServiceApplication.getDesignationBn() );
        vehicleServiceApplicationViewResponse.setDesignationEn( vehicleServiceApplication.getDesignationEn() );
        vehicleServiceApplicationViewResponse.setDriverNameBn( vehicleServiceApplication.getDriverNameBn() );
        vehicleServiceApplicationViewResponse.setDriverNameEn( vehicleServiceApplication.getDriverNameEn() );
        vehicleServiceApplicationViewResponse.setDriverPhoneNo( vehicleServiceApplication.getDriverPhoneNo() );
        vehicleServiceApplicationViewResponse.setStateId( vehicleServiceApplication.getState() != null? vehicleServiceApplication.getState().getId(): null );
        vehicleServiceApplicationViewResponse.setStateType( vehicleServiceApplication.getState().getStateType() );
        vehicleServiceApplicationViewResponse.setCommentFromPoridorshok( (vehicleServiceApplication.getCommentFromPoridorshok() == null) ? "N/A" : vehicleServiceApplication.getCommentFromPoridorshok() );
        vehicleServiceApplicationViewResponse.setFinalPoridorshokComment( (vehicleServiceApplication.getFinalPoridorshokComment() == null) ? "N/A" : vehicleServiceApplication.getFinalPoridorshokComment() );
        vehicleServiceApplicationViewResponse.setVehicleProblemsDetails( (vehicleServiceApplication.getVehicleProblemsDetails() == null) ? "N/A" : vehicleServiceApplication.getVehicleProblemsDetails() );
        vehicleServiceApplicationViewResponse.setIsJobViewed( true );
        vehicleServiceApplicationViewResponse.setDriverCommentDocuments( driverCommentDocuments );

        vehicleServiceApplicationViewResponse.setApprovedDate( vehicleServiceApplication.getApprovedDate());
        vehicleServiceApplicationViewResponse.setAppliedDate( vehicleServiceApplication.getCreatedAt().toLocalDate() );

        Vehicle vehicle = vehicleServiceApplication.getVehicle();

        vehicleServiceApplicationViewResponse.setBrandId( vehicle.getBrand().getId() );
        vehicleServiceApplicationViewResponse.setModelId( vehicle.getModel().getId() );
        vehicleServiceApplicationViewResponse.setVehicleId( vehicle.getId() );
        vehicleServiceApplicationViewResponse.setVehicleType( VehicleType.getLabel( vehicle.getVehicleType() ) );
        vehicleServiceApplicationViewResponse.setModelName( vehicle.getModel().getNameBn() );

        Set<Job> jobs = vehicleServiceApplication.getJobs();

        List<JobResponse> jobResponseList = jobs.stream().map(
                job -> {
                    List<InventoryItemViewResponse> inventoryItemViewResponseList = job.getJobInventoryItems()
                            .stream()
                            .map(this::getInventoryItemViewResponseFromJobInventoryItem)
                            .filter( item -> item != null)
                            .collect(Collectors.toList());

                    List<MechanicViewResponse> mechanicViewResponseList = job.getMechanics()
                            .stream()
                            .map(this::getMechanicViewResponseFromUser)
                            .filter( item -> item!=null)
                            .collect( Collectors.toList());

                    return JobResponse.builder()
                            .jobNameBn( (job.getJobType() == null) ? "" : job.getJobType().getNameBn() )
                            .workName( job.getWorkName() )
                            .jobId(job.getJobId())
                            .sharokNo( job.getSharokNo() )
                            .id( job.getId() )
                            .cost(job.getServicingCost())
                            .sectionLeaderName( (job.getSectionLeader() == null) ? "" : job.getSectionLeader().getEmployee().getName() )
                            .jobSection( job.getJobSection() )
                            .isJobViewed(true)
                            .inventoryItemViewResponseList( inventoryItemViewResponseList )
                            .mechanicViewResponseList( mechanicViewResponseList )
                            .isDone( job.getIsDone() )
                            .build();
                }
        ).collect( Collectors.toList() );

        vehicleServiceApplicationViewResponse.setJobResponses( jobResponseList );

        if( signMetaData.size() > 0 ){
            vehicleServiceApplicationViewResponse.setSignMetaData( signMetaData.get(0) );
        }
        else{

            vehicleServiceApplicationViewResponse.setSignMetaData( null );
        }

        if( signMetaData.size() > 0 ){
            vehicleServiceApplicationViewResponse.setSignMetaData( signMetaData.get(0) );
        }
        else{

            vehicleServiceApplicationViewResponse.setSignMetaData( null );
        }

        State state = vehicleServiceApplication.getState();
        if( state != null ){

            StateResponse stateResponse = StateResponse.builder()
                    .id( state.getId() )
                    .name( state.getName() )
                    .nameEn( state.getNameEn() )
                    .stateType( state.getStateType() )
                    .build();
            vehicleServiceApplicationViewResponse.setStateResponse( stateResponse );
        }

        return vehicleServiceApplicationViewResponse;
    }

    private MechanicViewResponse getMechanicViewResponseFromUser(User user) {

        MechanicViewResponse mechanicViewResponse = new MechanicViewResponse();

        if(user.getMechanic() != null) {

            mechanicViewResponse.setMechanicId( user.getMechanic().getMechanicId() );
            mechanicViewResponse.setName( user.getMechanic().getName() );
            mechanicViewResponse.setPhoneNumber( user.getMechanic().getPhoneNumber() );
        } else return null;

        return mechanicViewResponse;
    }

    private InventoryItemViewResponse getInventoryItemViewResponseFromJobInventoryItem(JobInventoryItem jobInventoryItem) {

        InventoryItemViewResponse inventoryItemViewResponse = new InventoryItemViewResponse();
        if(jobInventoryItem == null || jobInventoryItem.getInventoryItem() == null ) {

            return null;
        } else {

            inventoryItemViewResponse.setProductNameBn( jobInventoryItem.getInventoryItem().getProductNameBn() );
            inventoryItemViewResponse.setProductNameEn( jobInventoryItem.getInventoryItem().getProductNameEn() );
            inventoryItemViewResponse.setQuantity( jobInventoryItem.getQuantity() );
            inventoryItemViewResponse.setUnit( jobInventoryItem.getUnit() );
            inventoryItemViewResponse.setUnitPrice( jobInventoryItem.getInventoryItem().getUnitPrice() );
            return inventoryItemViewResponse;
        }

    }

    @Transactional
    public void takeAction(Long id, Long actionId, VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData, UserPrincipal loggedInUser ) throws Exception {

        final Long nextActionId = getNextActionId( id, vehicleServiceAdditionalActionData, actionId );

        stateActionMapService.takeAction( this, id, nextActionId, loggedInUser, ( workflowEntity ) -> setAdditionalData( id,  workflowEntity, nextActionId, vehicleServiceAdditionalActionData) );

    }

    private Long getNextActionId(Long id, VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData, Long actionId ) {

        Long nextActionId = null;

        if( actionId == AppConstants.VEHICLE_SERVICE_CREATE_JOBS_ACTION_ID ){

            Double inventoryItemPrice = calculatePrice(vehicleServiceAdditionalActionData, id);

            if (inventoryItemPrice < 75000) {

                nextActionId = AppConstants.VEHICLE_SERVICE_CREATE_JOBS_ACTION_ID;

            } else if (inventoryItemPrice >= 75000 && inventoryItemPrice < 100000) {

                nextActionId = AppConstants.VEHICLE_SERVICE_APPROVAL_FROM_MOPA_ACTION_ID;
            } else {

                nextActionId = AppConstants.VEHICLE_SERVICE_APPROVAL_FROM_FM_ACTION_ID;
            }
        }

        return nextActionId == null? actionId:nextActionId;
    }

    private WorkflowAdditionalData setAdditionalData( Long applicationId, WorkflowEntity workflowEntity, Long actionId, WorkflowAdditionalData workflowAdditionalData ) {

        VehicleServiceApplication vehicleServiceApplication = (VehicleServiceApplication) workflowEntity;

        if( actionId == AppConstants.VEHICLE_SERVICE_CREATE_JOBS_ACTION_ID ) {
            VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData = (VehicleServiceAdditionalActionData)workflowAdditionalData;
            Set<Job> jobSet = jobService.getJobListFromAdditionalActionData( vehicleServiceAdditionalActionData );

            if( vehicleServiceApplication.getJobs() != null )
                jobSet.addAll( vehicleServiceApplication.getJobs() );

            vehicleServiceApplication.setJobs( jobSet );

            vehiclePartsService.saveFromVehicleServiceApplicationAndJobSet( vehicleServiceApplication, jobSet );

        }
        else if( actionId == AppConstants.VEHICLE_SERVICE_APPROVE_ACTION_ID ){

            VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData = (VehicleServiceAdditionalActionData) workflowAdditionalData;

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( vehicleServiceAdditionalActionData.getDriverCommentReportFiles(), Collections.EMPTY_MAP );
            vehicleServiceApplication.setDriverCommentDocGroupId( documentUploadedResponse.getDocGroupId() );

            List<VehicleParts> vehiclePartsList = vehiclePartsService.findVehiclePartsByServiceApplication( vehicleServiceApplication );
            vehiclePartsList.forEach( vehiclePart -> vehiclePart.setIsActive( true ) );

            vehicleServiceApplication.setApprovedDate( LocalDate.now() );

            vehiclePartsService.saveAll( vehiclePartsList );
        }
        else if(Objects.equals(actionId, AppConstants.VEHICLE_SERVICE_PORIDORSHOK_ACTION_ID)){

            VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData = (VehicleServiceAdditionalActionData) workflowAdditionalData;

            vehicleServiceApplication.setFinalPoridorshokComment( vehicleServiceAdditionalActionData.getFinalPoridorshokComment() );
        }
        else if( actionId == AppConstants.VEHICLE_SERVICE_APPLICATION_REJECTION_ACTION_ID ) {

            Set<Job> jobs = vehicleServiceApplication.getJobs();

            if( jobs != null && jobs.size() > 0 ) {

                jobs.forEach(
                        job ->
                        {
                            job.getJobInventoryItems().forEach(
                                    items -> {

                                        Double quantity = items.getQuantity();
                                        InventoryItem inventoryItem = items.getInventoryItem();
                                        inventoryItem.setQuantity(inventoryItem.getQuantity() + quantity);
                                    }
                            );

                            inventoryItemService.save(job.getJobInventoryItems());
                        });
            }

        } else if( actionId == AppConstants.VEHICLE_SERVICE_ASSIGN_SECTION_LEADERS_ACTION_ID) {

            VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData = (VehicleServiceAdditionalActionData)workflowAdditionalData;

            Set<Job> newJobSetWithSectionLeaderIds = jobService.saveJobsWithSectionLeaderIds( vehicleServiceAdditionalActionData, vehicleServiceApplication );

            vehicleServiceApplication.getJobs().clear();

            vehicleServiceApplication.setJobs( newJobSetWithSectionLeaderIds );
        }
        else if( actionId == AppConstants.CHOOSE_WORKSHOP_MANAGER_ACTION_ID ) {

            VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData = (VehicleServiceAdditionalActionData)workflowAdditionalData;
            Optional<User> userOptional = userService.findById( vehicleServiceAdditionalActionData.getWorkshopManagerId() );

            if( userOptional.isPresent() ) {

                User user = userOptional.get();
                vehicleServiceApplication.setWorkshopManager( user );
            }
        }
        else if( actionId == AppConstants.VEHICLE_SERVICE_CHOOSE_JOB_ASSISTANT_ACTION_ID ){

            VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData = (VehicleServiceAdditionalActionData)workflowAdditionalData;
            Optional<User> userOptional = userService.findById( vehicleServiceAdditionalActionData.getJobAssistantId() );

            if( userOptional.isPresent() ) {

                User user = userOptional.get();
                vehicleServiceApplication.setJobAssistant( user );
            }
        }

        else if( actionId == AppConstants.VEHICLE_SERVICE_DISPOSITION_ACTION_ID ) {

            VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData = (VehicleServiceAdditionalActionData) workflowAdditionalData;

            VehicleDisposition vehicleDisposition = vehicleDispositionService.getVehicleDispostionFromServiceAdditionalData(vehicleServiceAdditionalActionData);

            vehicleService.saveVehicleDispostionAndVehicleServiceApplication(vehicleDisposition, vehicleServiceApplication);
        }

        else if( actionId.equals( AppConstants.VEHICLE_SERVICE_FORWARD_TO_PORIDORSHOK_FROM_FOREMAN ) ){

            VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData = (VehicleServiceAdditionalActionData) workflowAdditionalData;

            vehicleServiceApplication.setCommentFromPoridorshok( vehicleServiceAdditionalActionData.getCommentFromPoridorshok() );
        }

        return workflowAdditionalData;
    }

    private Double calculatePrice( VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData, Long applicationId ) {

//        List<InventoryItemWithQuantityRequest> inventoryItemWithQuantityRequestList = vehicleServiceAdditionalActionData.getJobAddRequestList().getInventoryItemWithQuantityRequestList();

        Double totalPrice = 0.0;

        List<JobAddRequest> jobAddRequestList = vehicleServiceAdditionalActionData.getJobAddRequestList();

        if( jobAddRequestList != null ) {

            for( JobAddRequest jobAddRequest : jobAddRequestList ) {

                List<InventoryItemWithQuantityRequest> inventoryItemWithQuantityRequestList = jobAddRequest.getInventoryItemWithQuantityRequestList();

//                totalPrice += jobAddRequest.getTotalCost();

                if (inventoryItemWithQuantityRequestList != null) {

                    totalPrice += inventoryItemWithQuantityRequestList.stream()
                            .filter(item -> item.getQuantity() >= 0)
                            .map(item -> {
                                Optional<InventoryItem> inventoryItemOptional = inventoryItemService.findById(item.getInventoryItemId());

                                if (inventoryItemOptional.isPresent()) {

                                    InventoryItem inventoryItem = inventoryItemOptional.get();
                                    inventoryItem.setQuantity(inventoryItem.getQuantity() - item.getQuantity());
                                    inventoryItemService.save(inventoryItem);

                                    return inventoryItem.getUnitPrice() * item.getQuantity();
                                }
                                return null;
                            })
                            .filter(price -> price != null)
                            .collect(Collectors.summingDouble(Double::doubleValue));
                }
            }

        }

        List<VehicleServiceCost> vehicleServiceCostList = vehicleServiceCostService.findByVehicleIdAndCurrentYear(  vehicleServiceAdditionalActionData.getVehicleId(), LocalDate.now().getYear() );

        for( VehicleServiceCost vehicleServiceCost : vehicleServiceCostList ){

            totalPrice += vehicleServiceCost.getCost();
        }


//        if( vehicleServiceAdditionalActionData.getServicingCost() != null && vehicleServiceAdditionalActionData.getServicingCost()>=0 ) {
//
//            totalPrice+=vehicleServiceAdditionalActionData.getServicingCost();
//        }
//
        Optional<VehicleServiceApplication> vehicleServiceApplicationOptional =  findById( applicationId );

        if( vehicleServiceApplicationOptional.isPresent() ) {

            VehicleServiceApplication vehicleServiceApplication = vehicleServiceApplicationOptional.get();

            Set<Job> jobs = vehicleServiceApplication.getJobs();

            for( Job job:jobs ){

                if (job != null) {

                    List<JobInventoryItem> jobInventoryItemList = job.getJobInventoryItems();

                    Double subTotalPrice = 0D;

//                    subTotalPrice += job.getServicingCost();

                    for (JobInventoryItem jobInventoryItem : jobInventoryItemList) {

                        Optional<InventoryItem> inventoryItemOptional = inventoryItemService.findById(jobInventoryItem.getInventoryItem().getId());

                        if (inventoryItemOptional.isPresent()) {

                            subTotalPrice += inventoryItemOptional.get().getUnitPrice() * jobInventoryItem.getQuantity();
                        }
                    }

                    totalPrice += subTotalPrice;
                }
            }
        }




        return totalPrice;
    }

    public DataTablesOutput<VehicleServiceSearchResponse> searchForMyDatatable(DataTablesInput dataTablesInput, Long id) {

        return vehicleServiceApplicationRepository.findAll(
                dataTablesInput,
                filterByMechanicOrSectionLeaderUserId( id ),
                null,
                this::getVehicleServiceSearchResponseFromVehicleServiceApplication
        );
    }

    @Transactional
    public void addMoreProduct(Long applicationId, Long jobId, VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData, UserPrincipal loggedInUser) {

        Optional<Job> jobOptional = jobService.findById( jobId );

        if( jobOptional.isPresent() ) {

            Job job = jobOptional.get();

            List<InventoryItemWithQuantityRequest> inventoryItemWithQuantityRequestList = vehicleServiceAdditionalActionData.getMoreInventoryItemWithQuantityRequestList();

            for(InventoryItemWithQuantityRequest inventoryItemWithQuantity: inventoryItemWithQuantityRequestList) {

                Optional<InventoryItem> inventoryItemOptional = inventoryItemService.findById( inventoryItemWithQuantity.getInventoryItemId() );

                if( inventoryItemOptional.isPresent() ) {

                    List<JobInventoryItem> jobInventoryItemList =  job.getJobInventoryItems();
                    boolean isFound = false;
                    for(JobInventoryItem jobInventoryItem: jobInventoryItemList) {

                        if(jobInventoryItem.getInventoryItem().getId().equals(inventoryItemOptional.get().getId())) {

                            jobInventoryItem.setQuantity( jobInventoryItem.getQuantity() +  inventoryItemWithQuantity.getQuantity());

                            inventoryItemOptional.get().setQuantity( inventoryItemOptional.get().getQuantity() - inventoryItemWithQuantity.getQuantity() );
                            inventoryItemService.save( inventoryItemOptional.get() );
                            isFound = true;
                            break;
                        }

                    }

                    if(isFound==false) {

                        JobInventoryItem jobInventoryItem = new JobInventoryItem();

                        jobInventoryItem.setQuantity( inventoryItemWithQuantity.getQuantity() );
                        jobInventoryItem.setInventoryItem( inventoryItemOptional.get() );
                        if(inventoryItemOptional.get().getProductType().equals(ProductType.PARTS) || inventoryItemOptional.get().getProductType().equals(ProductType.STATIONARY)) {
                            jobInventoryItem.setUnit( Unit.SET );
                        } else if(inventoryItemOptional.get().getProductType().equals(ProductType.OIL)){
                            jobInventoryItem.setUnit( Unit.LITER);
                        }

                        job.getJobInventoryItems().add(jobInventoryItem);

                        inventoryItemOptional.get().setQuantity( inventoryItemOptional.get().getQuantity() - inventoryItemWithQuantity.getQuantity() );
                        inventoryItemService.save( inventoryItemOptional.get() );

                    }
                }
            }

            jobService.save(job);

        }

    }

    public Boolean existById(Long id ) {

        return vehicleServiceApplicationRepository.existsById( id );
    }

    public void delete( Long id ) {

        vehicleServiceApplicationRepository.deleteById( id );
    }

    public Long getCurrentStateId( Long applicationId ) {

        Optional<VehicleServiceApplication> vehicleServiceApplication = findById( applicationId );

        if(vehicleServiceApplication.isPresent()) {

            return vehicleServiceApplication.get().getState().getId();
        }

        return 0L;
    }

    public VehicleServiceApplicationViewResponse getVehicleServiceViewResponse( Long id ) {

        VehicleServiceApplicationViewResponse vehicleServiceApplicationViewResponse  = new VehicleServiceApplicationViewResponse();

        Optional<VehicleServiceApplication> vehicleServiceApplication = findById( id );

        if( vehicleServiceApplication.isPresent() ) {

            vehicleServiceApplicationViewResponse = getVehicleServiceApplicationViewResponseFromVehicleServiceApplication(vehicleServiceApplication.get());
        }

        return vehicleServiceApplicationViewResponse;
    }

    public List<Object[]> getListOfServiceGroupByMonth(LocalDateTime startDate, LocalDateTime endDate) {

        return vehicleServiceApplicationRepository.findRegisteredCustomersHistory( startDate, endDate );
    }

    public void saveFromIncident(Incident incident) {

        VehicleServiceApplication vehicleServiceApplication = new VehicleServiceApplication();

        vehicleServiceApplication = getVehicleServiceApplicationFromIncident( vehicleServiceApplication, incident );

        save( vehicleServiceApplication );
    }

    private VehicleServiceApplication getVehicleServiceApplicationFromIncident(VehicleServiceApplication vehicleServiceApplication, Incident incident) {

        vehicleServiceApplication.setRegNo( incident.getRegNo() );
        vehicleServiceApplication.setChassisNo( incident.getChassisNo() );
        vehicleServiceApplication.setVehicleUserNameEn( incident.getUserNameEn() );
        vehicleServiceApplication.setVehicleUserNameBn( incident.getUserName() );
        vehicleServiceApplication.setDriverNameEn( incident.getDriverNameEn() );
        vehicleServiceApplication.setDriverNameBn( incident.getDriverName() );
        vehicleServiceApplication.setVehicle( incident.getVehicle() );

        if( vehicleServiceApplication.getState() == null ){

            State state = stateService.findStateReferenceById( AppConstants.VEHICLE_SERVICE_INIT_STATE_ID );
            vehicleServiceApplication.setState( state );
        }

        return vehicleServiceApplication;
    }
}
