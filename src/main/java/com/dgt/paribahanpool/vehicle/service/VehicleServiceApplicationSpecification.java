package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.model.User_;
import com.dgt.paribahanpool.vehicle.model.Job;
import com.dgt.paribahanpool.vehicle.model.Job_;
import com.dgt.paribahanpool.vehicle.model.VehicleServiceApplication;
import com.dgt.paribahanpool.vehicle.model.VehicleServiceApplication_;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;

public class VehicleServiceApplicationSpecification {

    public static Specification<VehicleServiceApplication> filterByMechanicOrSectionLeaderUserId(Long userId ) {

        return (root, query, criteriaBuilder) -> {

            Join<VehicleServiceApplication, Job> serviceApplicationJobJoin = root.join( VehicleServiceApplication_.JOBS );
            Join<Job,User> jobUserJoin = serviceApplicationJobJoin.join( Job_.MECHANICS, JoinType.LEFT );
            Join<Job,User> jobSectionLeaderJoin = serviceApplicationJobJoin.join( Job_.SECTION_LEADER, JoinType.LEFT );

            return criteriaBuilder.or( criteriaBuilder.equal( jobUserJoin.get( User_.ID ), userId ), criteriaBuilder.equal( jobSectionLeaderJoin.get( User_.ID ), userId ) );
        };
    }

    public static Specification<VehicleServiceApplication> filterBySectionLeaderUserId( Long userId ) {

        return (root, query, criteriaBuilder) -> {

            Join<VehicleServiceApplication, Job> serviceApplicationJobJoin = root.join( VehicleServiceApplication_.JOBS );
            Join<Job,User> jobUserJoin = serviceApplicationJobJoin.join( Job_.SECTION_LEADER, JoinType.LEFT );

            return criteriaBuilder.equal( jobUserJoin.get( User_.ID ), userId );
        };
    }
}
