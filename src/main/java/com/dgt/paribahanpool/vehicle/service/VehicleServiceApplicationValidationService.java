package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.enums.*;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.inventory_item.model.InventoryItem;
import com.dgt.paribahanpool.inventory_item.service.InventoryItemService;
import com.dgt.paribahanpool.noc_application.model.NocApplication;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class VehicleServiceApplicationValidationService {

    private final MvcUtil mvcUtil;
    private final VehicleServiceApplicationService vehicleServiceApplicationService;
    private final InventoryItemService inventoryItemService;
    private final VehicleService vehicleService;
    private final JobService jobService;
    private final UserService userService;

    public Boolean handleBindingResultForAddFormPost( Model model, BindingResult bindingResult, VehicleServiceAddRequest vehicleServiceAddRequest ) {

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "vehicleServiceAddRequest" )
                .object( vehicleServiceAddRequest )
                .title( "title.vehicle.service.add" )
                .content( "vehicle/service/vehicle-service-add" )
                .activeMenu( Menu.VEHICLE_SERVICE_ADD )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.VEHICLE_SERVICE_ADD } )
                .build();

        Boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        if( !isValid ){

            List<VehicleViewResponse> vehicleViewResponseList = vehicleService.getVehicleViewResponseList();

            model.addAttribute( "vehicleViewResponseList", vehicleViewResponseList );
        }

        return isValid;
    }

    public Boolean addVehiclePost( RedirectAttributes redirectAttributes, VehicleServiceAddRequest vehicleServiceAddRequest ) {

        return true;
    }

    public boolean view(RedirectAttributes redirectAttributes, Long id) {

        Optional<VehicleServiceApplication> vehicleServiceApplication = vehicleServiceApplicationService.findById( id );

        if( !vehicleServiceApplication.isPresent() ) {

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    @Transactional
    public Boolean takeActionValidation(VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData, Long actionId, RedirectAttributes redirectAttributes) {

        if( actionId.equals(AppConstants.VEHICLE_SERVICE_CREATE_JOBS_ACTION_ID) ) {

            List<JobAddRequest> jobAddRequestList = vehicleServiceAdditionalActionData.getJobAddRequestList();

            if (jobAddRequestList != null) {

                for (JobAddRequest jobAddRequest : jobAddRequestList) {

                    List<InventoryItemWithQuantityRequest> inventoryItemWithQuantityRequestList = jobAddRequest.getInventoryItemWithQuantityRequestList();

                    if( inventoryItemWithQuantityRequestList != null ) {

                        for ( InventoryItemWithQuantityRequest item : inventoryItemWithQuantityRequestList ) {

                            Optional<InventoryItem> inventoryItemOptional = inventoryItemService.findById(item.getInventoryItemId());

                            if (inventoryItemOptional.isPresent()) {

                                if (inventoryItemOptional.get().getQuantity() < item.getQuantity()) {

                                    mvcUtil.addErrorMessage(redirectAttributes, "validation.item-quantity-not-available");
                                    return false;
                                }
                            }

                            if (isFraction(item.getQuantity())) {

                                Unit unit = null;

                                if (inventoryItemOptional.get().getProductType().equals(ProductType.STATIONARY) || inventoryItemOptional.get().getProductType().equals(ProductType.PARTS))
                                    unit = Unit.SET;
                                else if (inventoryItemOptional.get().getProductType().equals(ProductType.OIL))
                                    unit = Unit.LITER;


                                if (!Unit.isSupportFraction(unit)) {

                                    mvcUtil.addErrorMessage(redirectAttributes, "validation.invalid-quantity-provided");
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }

        if( actionId.equals( AppConstants.VEHICLE_SERVICE_ASSIGN_SECTION_LEADERS_ACTION_ID ) ){

            Optional<VehicleServiceApplication> vehicleServiceApplicationOptional = vehicleServiceApplicationService.findById( vehicleServiceAdditionalActionData.getId() );

            if( vehicleServiceApplicationOptional.isPresent() ){

                if( vehicleServiceAdditionalActionData.getSectionLeaderId().length != vehicleServiceApplicationOptional.get().getJobs().size() || vehicleServiceAdditionalActionData.getSectionLeaderId().equals(-1L) ){

                    return false;
                }
            }
        }

        if( actionId.equals( AppConstants.VEHICLE_SERVICE_FORWARD_TO_PORIDORSHOK_FROM_FOREMAN ) ){

            Optional<VehicleServiceApplication> vehicleServiceApplicationOptional = vehicleServiceApplicationService.findById( vehicleServiceAdditionalActionData.getId() );

            if( vehicleServiceApplicationOptional.isPresent() ){

                VehicleServiceApplication vehicleServiceApplication = vehicleServiceApplicationOptional.get();

                for( Job job : vehicleServiceApplication.getJobs() ){

                    if( job.getIsDone() == null || job.getIsDone() != true ){

                        mvcUtil.addErrorMessage(redirectAttributes, "validation.job-not-done");
                        return false;
                    }
                }
            }
        }

        return true;
    }

    private Boolean isFraction(double value) {

        return (value - (int) value ) != 0;
    }

    public boolean addMoreProductValidation(VehicleServiceAdditionalActionData vehicleServiceAdditionalActionData, RedirectAttributes redirectAttributes) {

        List<InventoryItemWithQuantityRequest> inventoryItemWithQuantityRequestList = vehicleServiceAdditionalActionData.getMoreInventoryItemWithQuantityRequestList();

        if( inventoryItemWithQuantityRequestList != null ) {
            for (InventoryItemWithQuantityRequest item : inventoryItemWithQuantityRequestList) {

                Optional<InventoryItem> inventoryItemOptional = inventoryItemService.findById( item.getInventoryItemId() );

                if (inventoryItemOptional.isPresent()) {

                    if (inventoryItemOptional.get().getQuantity() < item.getQuantity()) {

                        mvcUtil.addErrorMessage(redirectAttributes, "validation.item-quantity-not-available");
                        return false;
                    }
                }

                if (isFraction(item.getQuantity())) {

                    Unit unit = null;

                    if (inventoryItemOptional.get().getProductType().equals(ProductType.STATIONARY) || inventoryItemOptional.get().getProductType().equals(ProductType.PARTS))
                        unit = Unit.SET;
                    else if (inventoryItemOptional.get().getProductType().equals(ProductType.OIL))
                        unit = Unit.LITER;


                    if (!Unit.isSupportFraction(unit)) {

                        mvcUtil.addErrorMessage(redirectAttributes, "validation.invalid-quantity-provided");
                        return false;
                    }
                }
            }
        }
        return true;

    }

    public RestValidationResult delete(Long id ) throws NotFoundException {

        Boolean exist = vehicleServiceApplicationService.existById( id );
        if( !exist ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    @Transactional
    public Boolean getGatePass(RedirectAttributes redirectAttributes, Long id) {

        Optional<VehicleServiceApplication> vehicleServiceApplicationOptional = vehicleServiceApplicationService.findById( id );

        if( !vehicleServiceApplicationOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }
        else{

            VehicleServiceApplication vehicleServiceApplication = vehicleServiceApplicationOptional.get();

            if( vehicleServiceApplication.getState() == null || ( vehicleServiceApplication.getState().getStateType() != StateType.POSITIVE_END ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.not-positive-end-of-state" );
                return false;
            }
        }

        return true;
    }

    public Boolean isValidActionTaker(Long jobId, UserPrincipal loggedInUser, Long stateId) {

        Optional<Job> optionalJob = jobService.findById( jobId );

        if( optionalJob.isPresent() ){

            Job job = optionalJob.get();

            if( stateId == AppConstants.VEHICLE_SERVICE_FOREMAN_APPROVE_STATE_ID && loggedInUser.getUser().getId() != job.getSectionLeader().getId() ){

                return false;
            }

        }

        return true;
    }
}
