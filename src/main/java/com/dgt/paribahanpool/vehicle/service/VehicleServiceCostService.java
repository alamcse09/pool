package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.vehicle.model.VehicleServiceCost;
import com.dgt.paribahanpool.vehicle.repository.VehicleServiceCostRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class VehicleServiceCostService {

    private final VehicleServiceCostRepository vehicleServiceCostRepository;

    public List<VehicleServiceCost> findByVehicleIdAndCurrentYear(Long vehicleId, Integer year) {

        return vehicleServiceCostRepository.findByVehicleIdAndYear( vehicleId, year );
    }
}
