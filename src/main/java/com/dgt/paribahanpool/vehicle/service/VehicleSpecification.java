package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.driver.model.Driver_;
import com.dgt.paribahanpool.enums.Status;
import com.dgt.paribahanpool.user.model.User_;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.model.Vehicle_;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;

public class VehicleSpecification {

    public static Specification<Vehicle> filterByIsDeleted(Boolean isDeleted ){

        if( isDeleted == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Vehicle_.IS_DELETED ), isDeleted );
    }

    public static Specification<Vehicle> filterByRegistrationNumber( String registrationNumber ) {

        if( registrationNumber== null || registrationNumber.isEmpty())
            return Specification.where( null );

        return ( root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(Vehicle_.REGISTRATION_NO ), registrationNumber.trim() );
    }

    public static Specification<Vehicle> filterByChassisNumber( String chassisNumber ){

        if( chassisNumber == null || chassisNumber.isEmpty() ) {

            return Specification.where( null );
        }
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Vehicle_.CHASSIS_NO ), chassisNumber );
    }

    public static Specification<Vehicle> filterByEngineNumber( String engineNumber ) {

        if( engineNumber == null || engineNumber.isEmpty() ) {
            return Specification.where( null );
        }

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(Vehicle_.ENGINE_NO ), engineNumber );
    }

    public static Specification<Vehicle> filterByDriverNotNull(){

        return (root, query, criteriaBuilder) -> criteriaBuilder.isNotNull( root.get( Vehicle_.DRIVER ) );
    }

    public static Specification<Vehicle> filterByDriverId( Long id ){

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Vehicle_.DRIVER ).get(Driver_.ID ), id );
    }

    public static Specification<Vehicle> filterByVehicleOwnerId( Long id ) {

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Vehicle_.vehicleOwner ).get(User_.ID ), id );
    }

    public static Specification<Vehicle> filterByStatus( Status status ){

        if( status == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Vehicle_.STATUS ), status );
    }

    public static Specification<Vehicle> filterByStatusNotNull(){

        return (root, query, criteriaBuilder) -> criteriaBuilder.isNotNull( root.get( Vehicle_.STATUS ) );
    }

    public static Specification<Vehicle> filterByUserId( Long userId ){

        if( userId == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Vehicle_.VEHICLE_OWNER ), userId );
    }

}
