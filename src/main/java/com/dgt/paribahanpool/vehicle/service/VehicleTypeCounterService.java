package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.vehicle.model.VehicleTypeCounter;
import com.dgt.paribahanpool.vehicle.repository.VehicleTypeCounterRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class VehicleTypeCounterService {

    private final VehicleTypeCounterRepository vehicleTypeCounterRepository;

    public Optional<VehicleTypeCounter> findById(Long id ){

        return vehicleTypeCounterRepository.findById( id );
    }

    public VehicleTypeCounter save( VehicleTypeCounter vehicleTypeCounter ){

        return vehicleTypeCounterRepository.save( vehicleTypeCounter );
    }
}
