package com.dgt.paribahanpool.vehicle.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.color.model.Color;
import com.dgt.paribahanpool.color.service.ColorService;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.enums.StateType;
import com.dgt.paribahanpool.enums.Status;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.noc_application.model.NocApplication;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.*;
import com.dgt.paribahanpool.vendor.model.Vendor;
import com.dgt.paribahanpool.vendor.service.VendorRepository;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class VehicleValidationService {

    private final MvcUtil mvcUtil;
    private final VehicleService vehicleService;
    private final VehicleRequisitionApplicationService vehicleRequisitionApplicationService;
    private final StateActionMapService stateActionMapService;
    private final BrandService brandService;
    private final VendorRepository vendorRepository;
    private final ColorService colorService;

    public Boolean handleBindingResultForAddFormPost( Model model, BindingResult bindingResult, VehicleAddRequest vehicleAddRequest ){

        List<BrandViewResponse> brandViewResponseList = brandService.getAllBrandViewResponse();
        vehicleAddRequest.setBrandViewResponseList( brandViewResponseList );

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "vehicleAddRequest" )
                .object( vehicleAddRequest )
                .title( "title.vehicle.add" )
                .content( "vehicle/add" )
                .activeMenu( Menu.VEHICLE_ADD )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.VEHICLE_ADD_FORM, FrontEndLibrary.BRAND_MODEL_ADD  } )
                .build();

        boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        if( vehicleAddRequest.getId() == null ) {

            /*if( vehicleAddRequest.getTaxTokenDateEnd() != null && vehicleAddRequest.getTaxTokenDateEnd().isBefore( LocalDate.now() ) ) {

                vehicleAddRequest.setBrandViewResponseList( brandViewResponseList );

                List<Vendor> vendorList = vendorRepository.findAll().stream().collect(Collectors.toList());
                List<String> vendorNameList = new ArrayList<>();

                for(Vendor vendor: vendorList){
                    vendorNameList.add(vendor.getVendorName());
                }

                List<Color> allColorsList = colorService.getAllColors();

                model.addAttribute( "vehicleAddRequest", vehicleAddRequest );
                model.addAttribute( "vendorNameList", vendorNameList );
                model.addAttribute( "allColorsList", allColorsList );
                return setModelWithError( model, params, "error.vehicle.invalid.tax.token.date");
            }*/
        }

        if(!isValid) return false;

        return validateVehicleUniqueConstraints( model, vehicleAddRequest.getId(), vehicleAddRequest.getRegistrationNo(), vehicleAddRequest.getEngineNo(), vehicleAddRequest.getChassisNo(), params );
    }

    public Boolean requisition( Model model, BindingResult bindingResult, VehicleRequisitionRequest vehicleRequisitionRequest ) {

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "vehicleRequisitionRequest" )
                .object( vehicleRequisitionRequest )
                .title( "title.vehicle.requistion" )
                .content( "vehicle/requisition-add" )
                .activeMenu( Menu.VEHICLE_USER )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.REQUISITION_ADD, FrontEndLibrary.GEOLOCATION_ADD })
                .build();

        boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        return isValid;
    }

    public boolean validateVehicleUniqueConstraints( Model model, Long id, String registrationNo, String engineNo, String chassisNo, HandleBindingResultParams params) {

        if( id==null ) {

            if( vehicleService.getVehicleByRegistrationNumber( registrationNo ).size() > 0 ) {
                return setModelWithError( model, params, "error.vehicle.registration_number.already.exist");
            }

            if( vehicleService.getVehicleByEngineNumberAndChassisNumber( engineNo, chassisNo ).size() > 0 ) {
                return setModelWithError( model, params, "error.vehicle.engine_number_and_chassis_number.already.exist");
            }
        } else {

            List<Vehicle> vehicleList = vehicleService.getVehicleByRegistrationNumber( registrationNo );

            if( vehicleList.size()>0 && !vehicleList.get(0).getId().equals(id)) {
                return setModelWithError( model, params, "error.vehicle.registration_number.already.exist");
            }

            vehicleList = vehicleService.getVehicleByEngineNumberAndChassisNumber( engineNo, chassisNo );

            if( vehicleList.size()>0 && !vehicleList.get(0).getId().equals(id)) {
                return setModelWithError( model, params, "error.vehicle.engine_number_and_chassis_number.already.exist");
            }
        }

        return true;
    }

    private boolean setModelWithError( Model model, HandleBindingResultParams params , String errorMessage ) {

        mvcUtil.addErrorMessage( model, errorMessage );
        model.addAttribute( params.getKey(), params.getObject() );
        mvcUtil.addTitleAndContent( model, params.getTitle(), params.getContent(), params.getActiveMenu() );

        if( params.getFrontEndLibraries() != null ){

            mvcUtil.addCssAndJsByLibraryName( model, params.getFrontEndLibraries() );
        }

        return false;
    }

    public Boolean editVehicle( RedirectAttributes redirectAttributes, Long vehicleId ) {

        Optional<Vehicle> vehicle = vehicleService.findById(vehicleId);
        if(!vehicle.isPresent()) {

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    public RestValidationResult delete( Long id ) throws NotFoundException {

        Boolean exist = vehicleService.existById( id );
        if( !exist ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean addVehiclePost( RedirectAttributes redirectAttributes, VehicleAddRequest vehicleAddRequest ) {

        if( vehicleAddRequest.getId() != null ) {

            Optional<Vehicle> vehicle = vehicleService.findById( vehicleAddRequest.getId() );

            if ( !vehicle.isPresent() ) {

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
                return false;
            }
        }

        return true;
    }

    public Boolean validNewVehicleRequisitionRequest(RedirectAttributes redirectAttributes, Long id) {

        Optional<VehicleRequisitionApplication> application = vehicleRequisitionApplicationService.findById( id );

        if( !application.isPresent() ) {

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    @Transactional
    public Boolean takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, UserPrincipal loggedInUser) {

        Optional<VehicleRequisitionApplication> vehicleRequisitionApplicationOptional = vehicleRequisitionApplicationService.findById( id );

        if( !vehicleRequisitionApplicationOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }
        else{

            VehicleRequisitionApplication vehicleRequisitionApplication = vehicleRequisitionApplicationOptional.get();
            State state = vehicleRequisitionApplication.getState();

            if( !stateActionMapService.actionAllowed( state.getId(), loggedInUser.getRoleIds(), actionId ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.common.action.not-allowed" );
                return false;
            }

            return true;
        }
    }

    public Boolean handleBindingResultForAuctionInfo(Model model, BindingResult bindingResult) {

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .title( "title.vehicle.auction" )
                        .content( "vehicle/vehicle-auction" )
                        .activeMenu( Menu.VEHICLE_AUCTION )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.VEHICLE_AUCTION } )
                        .build()
        );
    }

    public Boolean handleBindingResultForBuyerInfo( Model model, BindingResult bindingResult ) {

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .title( "title.vehicle.auction" )
                        .content( "vehicle/vehicle-auction" )
                        .activeMenu( Menu.VEHICLE_AUCTION )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.VEHICLE_AUCTION } )
                        .build()
        );
    }

    public Boolean addCommitteeDecision( RedirectAttributes redirectAttributes, VehicleAuctionCommitteeDecisionAddRequest vehicleAuctionCommitteeDecisionAddRequest) {

        if( vehicleAuctionCommitteeDecisionAddRequest.getCommitteeDecisionId() != null ){

            Optional<Vehicle> vehicleOptional = vehicleService.findById( vehicleAuctionCommitteeDecisionAddRequest.getCommitteeDecisionId() );

            if( !vehicleOptional.isPresent() ) {

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
                return false;
            }
        }

        return true;
    }

    public Boolean addBuyerInfo( RedirectAttributes redirectAttributes, VehicleAuctionBuyerInfoAddRequest vehicleAuctionBuyerInfoAddRequest ) {

        if( vehicleAuctionBuyerInfoAddRequest.getBuyerFromVehicleId() != null ){

            Optional<Vehicle> vehicleOptional = vehicleService.findById( vehicleAuctionBuyerInfoAddRequest.getBuyerFromVehicleId() );

            if( !vehicleOptional.isPresent() ) {

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
                return false;
            }
        }

        return true;
    }

    public RestValidationResult findByCommitteeDecisionId( Long committeeDecisionId ) throws NotFoundException {

        Optional<Vehicle> vehicleOptional = vehicleService.findById( committeeDecisionId );

        if( !vehicleOptional.isPresent() ){

            throw new NotFoundException( "validation.vehicle.notExist" );
        }

        return RestValidationResult.valid();
    }

    @Transactional
    public Boolean isValidVehicle(RedirectAttributes redirectAttributes, Long id) {

        Optional<Vehicle> vehicleOptional = vehicleService.findById( id );

        if( vehicleOptional.isPresent() ){

            Vehicle vehicle = vehicleOptional.get();

            if( vehicle.getVehicleDisposition() != null ){

                return true;
            }

            mvcUtil.addErrorMessage( redirectAttributes, "validation.vehicle.notfound.disposition" );
            return false;

        }

        mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );

        return false;
    }
}
