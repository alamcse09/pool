package com.dgt.paribahanpool.vehicle_replace_application.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.noc_application.model.NocAdditionalActionData;
import com.dgt.paribahanpool.noc_application.model.NocApplication;
import com.dgt.paribahanpool.noc_application.model.NocApplicationAddRequest;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.vehicle_replace_application.model.VehicleReplaceAddRequest;
import com.dgt.paribahanpool.vehicle_replace_application.model.VehicleReplaceAdditionalData;
import com.dgt.paribahanpool.vehicle_replace_application.model.VehicleReplaceApplication;
import com.dgt.paribahanpool.vehicle_replace_application.model.VehicleReplaceApplicationAddRequest;
import com.dgt.paribahanpool.vehicle_replace_application.service.VehicleReplaceApplicationModelService;
import com.dgt.paribahanpool.vehicle_replace_application.service.VehicleReplaceApplicationValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/replace-application" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class VehicleReplaceApplicationController extends MVCController {

    private final VehicleReplaceApplicationValidationService vehicleReplaceApplicationValidationService;
    private final VehicleReplaceApplicationModelService vehicleReplaceApplicationModelService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.replace.application.add", content = "replace-application/replace-application-add",activeMenu = Menu.REPLACE_APPLICATION_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.REPLACE_APPLICATION_ADD_FORM, FrontEndLibrary.GEOLOCATION_ADD } )
    public String add(
            HttpServletRequest httpServletRequest,
            RedirectAttributes redirectAttributes,
            Model model
    ) {
        User user = getLoggedInUser().getUser();
        if( vehicleReplaceApplicationValidationService.isValidVehicleUser( user ) ) {

            vehicleReplaceApplicationModelService.addGet( model, user );
            return viewRoot;

        } else {

            vehicleReplaceApplicationValidationService.addValidationMessage( redirectAttributes );
            return "redirect:/replace-application/my-search";
        }
    }

    @GetMapping( "/{id}" )
    @TitleAndContent( title = "title.replace.application.view", content = "replace-application/replace-application-view", activeMenu = Menu.REPLACE_APPLICATION_VIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.JQUERY_PRINT, FrontEndLibrary.REPLACE_APPLICATION_VIEW } )
    public String view(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ){

        User user = getLoggedInUser().getUser();

        if( vehicleReplaceApplicationValidationService.view(  user ) ) {

            vehicleReplaceApplicationModelService.view( model, id, getLoggedInUser() );
            return viewRoot;
        }

        return "";
    }

    @PostMapping( "/add" )
    public String addReplaceApplication(
            RedirectAttributes redirectAttributes,
            Model model,
            @Valid VehicleReplaceApplicationAddRequest vehicleReplaceApplicationAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){
        if( !vehicleReplaceApplicationValidationService.handleBindingResultForAddFormPost(model,bindingResult,vehicleReplaceApplicationAddRequest) ) {

            return viewRoot;
        }

        if( vehicleReplaceApplicationValidationService.validatePostRequest(redirectAttributes, vehicleReplaceApplicationAddRequest) ) {

            vehicleReplaceApplicationModelService.addPost(vehicleReplaceApplicationAddRequest, model);
            log( LogEvent.ADD_VEHICLE_REPLACE_APPLICATION, vehicleReplaceApplicationAddRequest.getId(), "Add vehicle replace application", request );
        }

        return "redirect:/replace-application/my-search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.replace.application.search", content = "replace-application/replace-application-search", activeMenu = Menu.REPLACE_APPLICATION_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.REPLACE_APPLICATION_SEARCH })
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ){

        return viewRoot;
    }

    @PostMapping( "/take-action" )
    @TitleAndContent( title = "title.replace.application.search", content = "replace-application/replace-application-search", activeMenu = Menu.REPLACE_APPLICATION_SEARCH )
    public String takeAction(
            @RequestParam( "id" ) Long id,
            @RequestParam( "action" ) Long actionId,
            VehicleReplaceAdditionalData vehicleReplaceAdditionalData,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ) throws Exception {

        if( vehicleReplaceApplicationValidationService.takeAction( redirectAttributes, id, actionId, getLoggedInUser(), vehicleReplaceAdditionalData ) ){

            vehicleReplaceApplicationModelService.takeAction( redirectAttributes, id, actionId, vehicleReplaceAdditionalData, getLoggedInUser() );
            log( LogEvent.VEHICLE_REPLACE_APPLICATION_TAKE_ACTION, id, "Vehicle replace application take action", request );
        }
        else{

            return "redirect:/replace-application/" + id;
        }

        return "redirect:/replace-application/search";
    }

    @GetMapping( "/replace/{id}")
    @TitleAndContent( title = "title.replace.vehicle", content = "replace-application/replace-vehicle",activeMenu = Menu.REPLACE_VEHICLE_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.REPLACE_VEHICLE_ADD } )
    public String replace(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable("id") Long id
    ){
        if( vehicleReplaceApplicationValidationService.validateReplace(redirectAttributes, id) ) {

            vehicleReplaceApplicationModelService.replaceVehicleGet( model, id );
            return viewRoot;
        }
        return "redirect:/replace-application/search";
    }

    @PostMapping( "/replace" )
    public String replaceVehicle(
            RedirectAttributes redirectAttributes,
            Model model,
            @Valid VehicleReplaceAddRequest vehicleReplaceAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){
        if( !vehicleReplaceApplicationValidationService.handleBindingResultForVehicleReplace(model,bindingResult,vehicleReplaceAddRequest) ) {

            return viewRoot;
        }

        if( vehicleReplaceApplicationValidationService.isVehicleVacantToAssign( redirectAttributes, vehicleReplaceAddRequest) ) {

            vehicleReplaceApplicationModelService.replaceVehiclePost( redirectAttributes, vehicleReplaceAddRequest );
            log( LogEvent.VEHICLE_REPLACE, vehicleReplaceAddRequest.getVehicleId(), "Vehicle replace", request );
        }

        return "redirect:/replace-application/search";
    }

    @GetMapping( value="/my-search" )
    @TitleAndContent( title = "title.replace.application.search", content = "replace-application/replace-application-my-search", activeMenu = Menu.REPLACE_APPLICATION_MY_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.REPLACE_APPLICATION_MY_SEARCH })
    public String getMySearchPage(
            HttpServletRequest request,
            Model model
    ){

        return viewRoot;
    }

    @GetMapping( "/create/{id}" )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.JQUERY_PRINT, FrontEndLibrary.LETTER_PRINT } )
    public String create(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ){

        if( vehicleReplaceApplicationValidationService.create( redirectAttributes, id ) ){

            vehicleReplaceApplicationModelService.create( model, id, getLoggedInUser() );
            return "replace-application/replace-create";
        }

        return "redirect:/replace-application/search";
    }
}
