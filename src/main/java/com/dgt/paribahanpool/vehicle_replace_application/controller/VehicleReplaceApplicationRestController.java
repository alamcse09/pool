package com.dgt.paribahanpool.vehicle_replace_application.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.noc_application.model.NocSearchResponse;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.vehicle_replace_application.model.VehicleReplaceApplicationSearchResponse;
import com.dgt.paribahanpool.vehicle_replace_application.service.VehicleReplaceApplicationService;
import com.dgt.paribahanpool.vehicle_replace_application.service.VehicleReplaceApplicationValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/replace-application" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class VehicleReplaceApplicationRestController extends BaseRestController {

    private final VehicleReplaceApplicationService vehicleReplaceApplicationService;
    private final VehicleReplaceApplicationValidationService vehicleReplaceApplicationValidationService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<VehicleReplaceApplicationSearchResponse> searchVehicle(
            DataTablesInput dataTablesInput,
            HttpServletRequest request
    ){

        return vehicleReplaceApplicationService.searchForDatatable( dataTablesInput, getLoggedInUser(), request );
    }

    @GetMapping(
            value = "/my-search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<VehicleReplaceApplicationSearchResponse> searchMyVehicle(
            DataTablesInput dataTablesInput,
            HttpServletRequest request
    ){

        return vehicleReplaceApplicationService.searchForMyDatatable( dataTablesInput, getLoggedInUser().getUser().getId(), request);
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ) throws NotFoundException {

        RestValidationResult restValidationResult = vehicleReplaceApplicationValidationService.delete( id );
        if( restValidationResult.getSuccess() ) {

            vehicleReplaceApplicationService.delete( id );
            log( LogEvent.REPLACE_VEHICLE_DELETE, id, "", request );
            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        }
        else{

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }
}
