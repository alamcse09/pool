package com.dgt.paribahanpool.vehicle_replace_application.model;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class VehicleReplaceAddRequest {

    @Min( value = 1, message = "{validation.common.min}" )
    @NotNull( message = "{validation.common.required}")
    public Long vehicleId;

    @NotBlank( message = "{validation.common.required}" )
//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    public String registrationNo;

    @NotBlank( message = "{validation.common.required}" )
//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    public String chassisNo;

    public String name;

    @NotNull( message = "{validation.common.required}")
    public Long identityNumber;

    public String designation;
}
