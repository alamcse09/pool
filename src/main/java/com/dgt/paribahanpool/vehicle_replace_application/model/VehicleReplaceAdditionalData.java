package com.dgt.paribahanpool.vehicle_replace_application.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.workflow.model.WorkflowAdditionalData;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;

@Data
public class VehicleReplaceAdditionalData implements WorkflowAdditionalData {

    private Long vehicleId;

    private transient MultipartFile[] rejectReasonsReportFiles;
    private List<DocumentMetadata> rejectReasonsReportDocuments = Collections.EMPTY_LIST;
}
