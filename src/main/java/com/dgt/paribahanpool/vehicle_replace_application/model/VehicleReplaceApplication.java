package com.dgt.paribahanpool.vehicle_replace_application.model;

import com.dgt.paribahanpool.enums.OwnerType;
import com.dgt.paribahanpool.enums.Status;
import com.dgt.paribahanpool.enums.VehicleType;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.VersionableAuditableEntity;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.WorkflowEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name ="vehicle_replacement_application")
public class VehicleReplaceApplication extends VersionableAuditableEntity implements WorkflowEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name="name" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String name;

    @Column( name="identity_number" )
    private Long identityNumber;

    @Column( name="designation" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String designation;

    @Column( name="working_place" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String workingPlace;

    @Column( name="mobile_number" )
    @Convert( converter = EscapeHtmlConverter.class )
    @Convert( converter = StringTrimConverter.class )
    private String mobileNumber;

    @Column( name = "email" )
    private String email;

    @Column( name="vehicle_type" )
    private VehicleType vehicleType;

    @Column( name="vehicle_id" )
    private Long vehicleId;

    @Column( name="assigned_vehicle_id" )
    private Long assignedVehicleId;

    @Column( name = "registration_no" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String registrationNo;

    @Column( name = "application_date" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate applicationDate;

    @Column( name = "replacement_reason" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String replacementReason;

    @Column( name = "status" )
    private Status status;

    @Column( name = "owner_type" )
    private OwnerType ownerType;

    @Column( name = "division_id")
    public Long divisionId;

    @Column( name = "district_id")
    public Long districtId;

    @Column( name = "upazila_id")
    public Long upazilaId;

    @Column( name = "approved_date" )
    private LocalDate approvedDate;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "approved_by_id", foreignKey = @ForeignKey( name = "fk_vehicle_replacement_application_approved_by_id" ) )
    private User approvedBy;

    @Column( name = "reject_reason_report_group_id" )
    private Long rejectReasonReportGroupId;

    @OneToOne
    @JoinColumn( name = "state_id", foreignKey = @ForeignKey( name = "fk_replace_app_state_id" ) )
    private State state;
}
