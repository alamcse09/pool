package com.dgt.paribahanpool.vehicle_replace_application.model;

import com.dgt.paribahanpool.enums.EmployeeType;
import com.dgt.paribahanpool.enums.OwnerType;
import com.dgt.paribahanpool.enums.Status;
import com.dgt.paribahanpool.enums.VehicleType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.vehicle.model.VehicleViewResponse;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class VehicleReplaceApplicationAddRequest {

    public Long id;

    @NotNull( message = "{validation.common.required}")
//    @Pattern( regexp = "[a-zA-Z ]+", message = "{validation.common.alphanumeric}" )
    public String name;

    @NotNull( message = "{validation.common.required}")
    public Long identityNumber;

    public String designation;

    public String workingPlace;

    private String mobileNumber;

    private String email;

    private VehicleType vehicleType;

    @NotNull( message = "{validation.common.required}")
    private Long vehicleId;

//    @NotNull( message = "{validation.common.required}")
    private String registrationNo;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate applicationDate;

    private String replacementReason;

    private Status status;

    private EmployeeType ownerType;

    public Long divisionValue;

    public Long districtValue;

    public Long upazilaValue;

    List<VehicleViewResponse> vehicleViewResponseList = new ArrayList<>();
}
