package com.dgt.paribahanpool.vehicle_replace_application.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.NocApplicationType;
import com.dgt.paribahanpool.enums.VehicleAllotment;
import com.dgt.paribahanpool.workflow.model.StateResponse;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class VehicleReplaceApplicationSearchResponse {

    private Long id;
    public String name;
    public String designation;
    public String workingPlace;
    public String ownerType;
    private String phoneNo;
    private String registrationNo;
    public Long identityNumber;

    private StateResponse stateResponse;
    private String statusEn;

}
