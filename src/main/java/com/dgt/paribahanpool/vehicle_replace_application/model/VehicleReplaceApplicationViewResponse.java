package com.dgt.paribahanpool.vehicle_replace_application.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.VehicleType;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.vehicle.model.RequisitionVehicleViewResponse;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class VehicleReplaceApplicationViewResponse {

    public Long id;

    public Long stateId;

    public String name;

    public Long identityNumber;

    public String designation;

    public String workingPlace;

    private String mobileNumber;

    private VehicleType vehicleType;

    private Long vehicleId;

    private String registrationNo;

    private String replacementReason;

    private LocalDate approvedDate;

    private LocalDate creationDate;

    private UserResponse approvedBy;

    RequisitionVehicleViewResponse assignedVehicle;

    private DocumentMetadata signMetaData;
}
