package com.dgt.paribahanpool.vehicle_replace_application.service;

import com.dgt.paribahanpool.noc_application.model.NocApplication;
import com.dgt.paribahanpool.noc_application.model.NocApplication_;
import com.dgt.paribahanpool.vehicle_replace_application.model.VehicleReplaceApplication;
import com.dgt.paribahanpool.vehicle_replace_application.model.VehicleReplaceApplication_;
import com.dgt.paribahanpool.workflow.model.StateActionMap;
import com.dgt.paribahanpool.workflow.model.StateActionMap_;
import com.dgt.paribahanpool.workflow.model.State_;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Set;

public class ReplaceSpecification {

    public static Specification<VehicleReplaceApplication> distinct(){

        return (root, query, criteriaBuilder) -> {

            query.distinct( true );
            return null;
        };
    }

    public static Specification<VehicleReplaceApplication> filterByAvailableAction(Set<Long> roleIdSet ){

        return ( root, query, criteriaBuilder ) -> {

            Root<StateActionMap> stateActionMapRoot = query.from( StateActionMap.class );

            Predicate joinTable = criteriaBuilder.equal( root.get( VehicleReplaceApplication_.STATE ).get( State_.ID ) , stateActionMapRoot.get( StateActionMap_.STATE ).get( State_.ID ) );
            Predicate roleIdIn = stateActionMapRoot.get( StateActionMap_.ROLE_ID ).in( roleIdSet );

            return criteriaBuilder.and( joinTable, roleIdIn );
        };
    }

    public static Specification<VehicleReplaceApplication> filterByLoggedInUser(Long id ){

        if( id == null ){
            return Specification.where( null );
        }

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( VehicleReplaceApplication_.CREATED_BY ), id );
    }

    public static Specification<VehicleReplaceApplication> filterByCreatedBy(Long id ){

        if( id == null || id <= 0 )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(VehicleReplaceApplication_.CREATED_BY ), id );
    }
}
