package com.dgt.paribahanpool.vehicle_replace_application.service;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.employee.model.Employee;
import com.dgt.paribahanpool.enums.GeolocationType;
import com.dgt.paribahanpool.enums.Status;
import com.dgt.paribahanpool.enums.UserType;
import com.dgt.paribahanpool.geolocation.model.GeoLocationResponse;
import com.dgt.paribahanpool.geolocation.service.GeoLocationService;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.PublicUserInfo;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.model.UserResponse;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.*;
import com.dgt.paribahanpool.vehicle.service.VehicleOwnerService;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import com.dgt.paribahanpool.vehicle_replace_application.model.*;
import com.dgt.paribahanpool.workflow.model.StateActionMapResponse;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.swing.text.html.Option;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class VehicleReplaceApplicationModelService {

    private final VehicleReplaceApplicationService vehicleReplaceApplicationService;
    private final MvcUtil mvcUtil;
    private final StateActionMapService stateActionMapService;
    private final UserService userService;
    private final DocumentService documentService;
    private final GeoLocationService geoLocationService;
    private final VehicleService vehicleService;

    public void view(Model model, Long id, UserPrincipal loggedInUser ) {

        Optional<VehicleReplaceApplication> application = vehicleReplaceApplicationService.findById( id );

        VehicleReplaceApplicationViewResponse vehicleReplaceApplicationViewResponse = new VehicleReplaceApplicationViewResponse();

        if( application.isPresent() ) {

            vehicleReplaceApplicationViewResponse = getResponseFromApplication( vehicleReplaceApplicationViewResponse,  application.get() );
        }
        model.addAttribute("vehicleReplaceApplicationViewResponse", vehicleReplaceApplicationViewResponse);


        if( application.isPresent() && application.get().getId() != null ){

            List<VehicleResponseForSelection> vehicleResponseForSelections = vehicleService.findAllWhereOwnerIsNull();
            model.addAttribute( "availableVehicles", vehicleResponseForSelections );

            List<StateActionMapResponse> stateActionMapList = stateActionMapService.findStateActionMapResponseListByCurrentStateAndRoleIdSet( vehicleReplaceApplicationViewResponse.getStateId(), loggedInUser.getRoleIds() );
            model.addAttribute( "stateActionList", stateActionMapList );
        }
    }

    @Transactional
    public VehicleReplaceApplicationViewResponse getResponseFromApplication( VehicleReplaceApplicationViewResponse vehicleReplaceApplicationViewResponse, VehicleReplaceApplication vehicleReplaceApplication ) {

        User user = userService.findById( vehicleReplaceApplication.getCreatedBy() ).get();
        List<DocumentMetadata> signMetaData = documentService.findDocumentMetaDataListByGroupId( user.getSignDocGroupId() );

        Optional<Vehicle> vehicleOptional = vehicleService.findById( vehicleReplaceApplication.getVehicleId() );

        User approvedBy = null;

        if( vehicleReplaceApplication.getApprovedBy() != null ) {

            approvedBy = userService.findById(vehicleReplaceApplication.getApprovedBy().getId()).get();
        }

        if( vehicleOptional.isPresent() ){

            Vehicle vehicle = vehicleOptional.get();

            vehicleReplaceApplicationViewResponse.setRegistrationNo( vehicle.getRegistrationNo() );
            vehicleReplaceApplicationViewResponse.setVehicleType( vehicle.getVehicleType() );
        }

        if( vehicleReplaceApplication.getAssignedVehicleId() != null ){

            Optional<Vehicle> assignedVehicleOptional = vehicleService.findById( vehicleReplaceApplication.getAssignedVehicleId() );

            vehicleReplaceApplicationViewResponse.setAssignedVehicle(
                    RequisitionVehicleViewResponse.builder()
                            .id( assignedVehicleOptional.get().getId() )
                            .chassisNumber( assignedVehicleOptional.get().getChassisNo() )
                            .engineNumber( assignedVehicleOptional.get().getEngineNo() )
                            .registrationNumber( assignedVehicleOptional.get().getRegistrationNo() )
                            .modelYear( assignedVehicleOptional.get().getModelYear() )
                            .vehicleType( assignedVehicleOptional.get().getVehicleType() )
                            .build()
            );
        }
        else{

            vehicleReplaceApplicationViewResponse.setAssignedVehicle(
                    RequisitionVehicleViewResponse.builder()
                            .registrationNumber( "N/A" )
                            .build()
            );
        }

        vehicleReplaceApplicationViewResponse.setId( vehicleReplaceApplication.getId() );
        vehicleReplaceApplicationViewResponse.setName( vehicleReplaceApplication.getName() );
        vehicleReplaceApplicationViewResponse.setIdentityNumber( vehicleReplaceApplication.getIdentityNumber() );
        vehicleReplaceApplicationViewResponse.setDesignation( vehicleReplaceApplication.getDesignation() );
        vehicleReplaceApplicationViewResponse.setWorkingPlace( vehicleReplaceApplication.getWorkingPlace() );
        vehicleReplaceApplicationViewResponse.setMobileNumber( vehicleReplaceApplication.getMobileNumber() );
        vehicleReplaceApplicationViewResponse.setVehicleId( vehicleReplaceApplication.getVehicleId() );
        vehicleReplaceApplicationViewResponse.setReplacementReason( vehicleReplaceApplication.getReplacementReason() );
        vehicleReplaceApplicationViewResponse.setStateId( vehicleReplaceApplication.getState().getId() );
        vehicleReplaceApplicationViewResponse.setCreationDate( vehicleReplaceApplication.getCreatedAt().toLocalDate() );
        vehicleReplaceApplicationViewResponse.setApprovedBy( (approvedBy == null) ? null :
                UserResponse.builder()
                        .id( approvedBy.getId() )
                        .nameBn( approvedBy.getEmployee().getName() )
                        .nameEn( approvedBy.getEmployee().getNameEn() )
                        .phoneNumber( approvedBy.getEmployee().getMobileNumber() )
                        .designation( approvedBy.getEmployee().getDesignation().getNameBn() )
                        .email( approvedBy.getEmployee().getEmail() )
                        .build()
        );

        if( vehicleReplaceApplication.getApprovedDate() != null ) {

            vehicleReplaceApplicationViewResponse.setApprovedDate(vehicleReplaceApplication.getApprovedDate());
        }

        if( signMetaData.size() > 0 ){
            vehicleReplaceApplicationViewResponse.setSignMetaData( signMetaData.get(0) );
        }
        else{

            vehicleReplaceApplicationViewResponse.setSignMetaData( null );
        }

        return vehicleReplaceApplicationViewResponse;
    }

    public void addGet( Model model, User user ) {

        List<GeoLocationResponse> geoLocationList = geoLocationService.findByGeoLocationType( GeolocationType.DIVISION );
        model.addAttribute( "divisionList", geoLocationList );

        VehicleReplaceApplicationAddRequest vehicleReplaceApplicationAddRequest = new VehicleReplaceApplicationAddRequest();

        vehicleReplaceApplicationAddRequest.setIdentityNumber( user.getId() );

        if( user.getUserType().equals(UserType.PUBLIC)) {

            PublicUserInfo userInfo = user.getPublicUserInfo();

            vehicleReplaceApplicationAddRequest.setName( userInfo.getNameEn() );
            vehicleReplaceApplicationAddRequest.setDesignation( userInfo.getDesignation() );
            vehicleReplaceApplicationAddRequest.setWorkingPlace( userInfo.getWorkplace() );
            vehicleReplaceApplicationAddRequest.setMobileNumber( userInfo.getPhoneNo() );
            vehicleReplaceApplicationAddRequest.setEmail( userInfo.getEmail() );
        } else {

            Employee employee = user.getEmployee();
            vehicleReplaceApplicationAddRequest.setName( employee.getName() );
            vehicleReplaceApplicationAddRequest.setDesignation( employee.getDesignation().getNameEn() );
            vehicleReplaceApplicationAddRequest.setWorkingPlace( employee.getPresentAddress() );
            vehicleReplaceApplicationAddRequest.setMobileNumber( employee.getMobileNumber() );
            vehicleReplaceApplicationAddRequest.setEmail( employee.getEmail() );
        }

//        vehicleReplaceApplicationAddRequest.setVehicleId( vehicle.get().getId() );
//        vehicleReplaceApplicationAddRequest.setVehicleType( vehicle.get().getVehicleType() );
//        vehicleReplaceApplicationAddRequest.setRegistrationNo( vehicle.get().getRegistrationNo() );
        vehicleReplaceApplicationAddRequest.setApplicationDate( LocalDate.now() );
        vehicleReplaceApplicationAddRequest.setStatus( Status.PENDING );

        model.addAttribute("vehicleReplaceApplicationAddRequest", vehicleReplaceApplicationAddRequest );
    }

    public void addPost(VehicleReplaceApplicationAddRequest vehicleReplaceApplicationAddRequest, Model model) {

        vehicleReplaceApplicationService.save(vehicleReplaceApplicationAddRequest);
    }

    public void takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, VehicleReplaceAdditionalData vehicleReplaceAdditionalData, UserPrincipal loggedInUser) throws Exception {

        vehicleReplaceApplicationService.takeAction( id, actionId, vehicleReplaceAdditionalData, loggedInUser );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.common.action.success" );
    }

    public void replaceVehicleGet(Model model, Long id) {

        Optional<VehicleReplaceApplication> application = vehicleReplaceApplicationService.findById( id );

        VehicleReplaceAddRequest vehicleReplaceAddRequest = new VehicleReplaceAddRequest();

        if( application.isPresent() ) {
            vehicleReplaceAddRequest.setName( application.get().getName() );
            vehicleReplaceAddRequest.setIdentityNumber( application.get().getIdentityNumber() );
            vehicleReplaceAddRequest.setDesignation( application.get().getDesignation() );
        }

        model.addAttribute("vehicleReplaceAddRequest", vehicleReplaceAddRequest );
    }

    public void replaceVehiclePost( RedirectAttributes redirectAttributes, VehicleReplaceAddRequest vehicleReplaceAddRequest ) {

        vehicleReplaceApplicationService.replaceVehicle(vehicleReplaceAddRequest);
        mvcUtil.addSuccessMessage( redirectAttributes, "success.common.action.success" );
    }

    @Transactional
    public void create(Model model, Long id, UserPrincipal loggedInUser) {

        Optional<VehicleReplaceApplication> application = vehicleReplaceApplicationService.findById( id );

        VehicleReplaceApplicationViewResponse vehicleReplaceApplicationViewResponse = new VehicleReplaceApplicationViewResponse();

        if( application.isPresent() ) {

            vehicleReplaceApplicationViewResponse = getResponseFromApplication( vehicleReplaceApplicationViewResponse,  application.get() );
        }
        model.addAttribute("vehicleReplaceApplicationViewResponse", vehicleReplaceApplicationViewResponse);
    }
}
