package com.dgt.paribahanpool.vehicle_replace_application.service;

import com.dgt.paribahanpool.vehicle_replace_application.model.VehicleReplaceApplication;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface VehicleReplaceApplicationRepository extends DataTablesRepository<VehicleReplaceApplication, Long> {
}
