package com.dgt.paribahanpool.vehicle_replace_application.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.enums.EmployeeType;
import com.dgt.paribahanpool.enums.OwnerType;
import com.dgt.paribahanpool.geolocation.model.GeoLocation;
import com.dgt.paribahanpool.geolocation.service.GeoLocationService;
import com.dgt.paribahanpool.locale.LocalizeUtil;
import com.dgt.paribahanpool.noc_application.model.NocApplication;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.vehicle.model.RequisitionAdditionalActionData;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.model.VehicleOwner;
import com.dgt.paribahanpool.vehicle.model.VehicleRequisitionApplication;
import com.dgt.paribahanpool.vehicle.service.VehicleOwnerService;
import com.dgt.paribahanpool.vehicle.repository.VehicleRepository;
import com.dgt.paribahanpool.vehicle.service.VehicleService;
import com.dgt.paribahanpool.vehicle.service.VehicleSpecification;
import com.dgt.paribahanpool.vehicle_replace_application.model.*;
import com.dgt.paribahanpool.workflow.model.*;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class VehicleReplaceApplicationService implements WorkflowService<VehicleReplaceApplication> {

    private final VehicleRepository vehicleRepository;
    private final VehicleReplaceApplicationRepository vehicleReplaceApplicationRepository;
    private final StateActionMapService stateActionMapService;
    private final VehicleService vehicleService;
    private final VehicleOwnerService vehicleOwnerService;
    private final StateService stateService;
    private final LocalizeUtil localizeUtil;
    private final GeoLocationService geoLocationService;
    private final UserService userService;
    private final DocumentService documentService;

    public Optional<Vehicle> getVehicleByVehicleOwnerId( Long vehicleOwnerId ) {

        Specification<Vehicle> filterByVehicleOwnerIdSpecification = VehicleSpecification.filterByVehicleOwnerId(vehicleOwnerId);
        List<Vehicle> vehicleList = vehicleRepository.findAll(filterByVehicleOwnerIdSpecification);

        Vehicle vehicle = null;
        if(vehicleList.size() > 0 )
            vehicle = vehicleList.get(0);

        return Optional.ofNullable(vehicle);

    }

    public Optional<VehicleReplaceApplication> findById( Long id ) {

        return vehicleReplaceApplicationRepository.findById(id);
    }

    public VehicleReplaceApplication save( VehicleReplaceApplication vehicleReplaceApplication ) {

        return vehicleReplaceApplicationRepository.save( vehicleReplaceApplication);
    }

    public VehicleReplaceApplication save( VehicleReplaceApplicationAddRequest vehicleReplaceApplicationAddRequest ) {

        VehicleReplaceApplication vehicleReplaceApplication = new VehicleReplaceApplication();

        if( vehicleReplaceApplicationAddRequest.getId() != null) {

            vehicleReplaceApplication = findById( vehicleReplaceApplicationAddRequest.getId() ).get();
        }

        getVehicleReplaceApplicationFromGetRequest(vehicleReplaceApplication,vehicleReplaceApplicationAddRequest);

        return save(vehicleReplaceApplication);
    }

    public VehicleReplaceApplication getVehicleReplaceApplicationFromGetRequest( VehicleReplaceApplication vehicleReplaceApplication, VehicleReplaceApplicationAddRequest vehicleReplaceApplicationAddRequest ) {

        if( vehicleReplaceApplicationAddRequest.getOwnerType() == EmployeeType.DIVISION ) {

            vehicleReplaceApplication.setOwnerType( OwnerType.DIVISION );
        }
        else if( vehicleReplaceApplicationAddRequest.getOwnerType() == EmployeeType.DISTRICT ) {

            vehicleReplaceApplication.setOwnerType( OwnerType.DISTRICT );
        }
        else if( vehicleReplaceApplicationAddRequest.getOwnerType() == EmployeeType.UPAZILA ) {

            vehicleReplaceApplication.setOwnerType( OwnerType.UPAZILA );
        }
        else {

            vehicleReplaceApplication.setOwnerType( OwnerType.USER );
        }


        vehicleReplaceApplication.setName( vehicleReplaceApplicationAddRequest.getName() );
        vehicleReplaceApplication.setIdentityNumber( vehicleReplaceApplicationAddRequest.getIdentityNumber() );
        vehicleReplaceApplication.setDesignation( vehicleReplaceApplicationAddRequest.getDesignation() );
        vehicleReplaceApplication.setWorkingPlace( vehicleReplaceApplicationAddRequest.getWorkingPlace() );
        vehicleReplaceApplication.setMobileNumber( vehicleReplaceApplicationAddRequest.getMobileNumber() );
        vehicleReplaceApplication.setVehicleType( vehicleReplaceApplicationAddRequest.getVehicleType() );
        vehicleReplaceApplication.setVehicleId( vehicleReplaceApplicationAddRequest.getVehicleId() );
        vehicleReplaceApplication.setRegistrationNo( vehicleReplaceApplicationAddRequest.getRegistrationNo() );
        vehicleReplaceApplication.setApplicationDate( vehicleReplaceApplicationAddRequest.getApplicationDate() );
        vehicleReplaceApplication.setReplacementReason( vehicleReplaceApplicationAddRequest.getReplacementReason() );
        vehicleReplaceApplication.setStatus( vehicleReplaceApplicationAddRequest.getStatus() );
        vehicleReplaceApplication.setDivisionId( vehicleReplaceApplicationAddRequest.getDivisionValue() );
        vehicleReplaceApplication.setDistrictId( vehicleReplaceApplicationAddRequest.getDivisionValue() );
        vehicleReplaceApplication.setUpazilaId( vehicleReplaceApplicationAddRequest.getUpazilaValue() );
        vehicleReplaceApplication.setEmail( vehicleReplaceApplicationAddRequest.getEmail() );

        if( vehicleReplaceApplication.getState() == null ){

            Long startStateId = AppConstants.VEHICLE_REQUISITION_INIT_STATE_ID;

            State state = stateService.findStateReferenceById( startStateId );
            vehicleReplaceApplication.setState( state );
        }

        return vehicleReplaceApplication;
    }

    public DataTablesOutput<VehicleReplaceApplicationSearchResponse> searchForDatatable(DataTablesInput dataTablesInput, UserPrincipal loggedInUser, HttpServletRequest request) {

        return vehicleReplaceApplicationRepository.findAll( dataTablesInput,  vehicleReplaceApplication -> getReplaceSearchResponseFromReplaceApplication( vehicleReplaceApplication, request ) );
    }

    @Transactional
    public VehicleReplaceApplicationSearchResponse getReplaceSearchResponseFromReplaceApplication(VehicleReplaceApplication vehicleReplaceApplication, HttpServletRequest request) {

        VehicleReplaceApplicationSearchResponse vehicleReplaceApplicationSearchResponse = new VehicleReplaceApplicationSearchResponse();

        Optional<Vehicle> vehicleOptional = vehicleService.findById( vehicleReplaceApplication.getVehicleId() );

        if( vehicleOptional.isPresent() ){

            vehicleReplaceApplicationSearchResponse.setRegistrationNo( vehicleOptional.get().getRegistrationNo() );
        }

        vehicleReplaceApplicationSearchResponse.setId(vehicleReplaceApplication.getId());
        vehicleReplaceApplicationSearchResponse.setDesignation(vehicleReplaceApplication.getDesignation());
        vehicleReplaceApplicationSearchResponse.setName(vehicleReplaceApplication.getName());
        vehicleReplaceApplicationSearchResponse.setWorkingPlace( vehicleReplaceApplication.getWorkingPlace() );
        vehicleReplaceApplicationSearchResponse.setOwnerType( localizeUtil.getMessageFromMessageSource( OwnerType.getLabel(vehicleReplaceApplication.getOwnerType() ), request ) );
        vehicleReplaceApplicationSearchResponse.setPhoneNo(vehicleReplaceApplication.getMobileNumber() );

        State state = vehicleReplaceApplication.getState();
        if( state != null ){

            StateResponse stateResponse = StateResponse.builder()
                    .id( state.getId() )
                    .name( state.getName() )
                    .nameEn( state.getNameEn() )
                    .stateType( state.getStateType() )
                    .build();
            vehicleReplaceApplicationSearchResponse.setStateResponse( stateResponse );
        }

        return vehicleReplaceApplicationSearchResponse;
    }

    @Transactional
    public void takeAction(Long id, Long actionId, VehicleReplaceAdditionalData vehicleReplaceAdditionalData, UserPrincipal loggedInUser ) throws Exception {

        stateActionMapService.takeAction( this, id, actionId, loggedInUser, ( workflowEntity ) ->  setAdditionalData( workflowEntity, actionId, vehicleReplaceAdditionalData, loggedInUser ) );
    }

    private WorkflowAdditionalData setAdditionalData(WorkflowEntity workflowEntity, Long actionId, VehicleReplaceAdditionalData vehicleReplaceAdditionalData, UserPrincipal loggedInUser) {

        VehicleReplaceApplication vehicleReplaceApplication = (VehicleReplaceApplication) workflowEntity;

        if( actionId == AppConstants.VEHICLE_REQUISITION_CHOOSE_ACTION_ID   ){

            vehicleReplaceApplication.setAssignedVehicleId( vehicleReplaceAdditionalData.getVehicleId() );
        }
        else if( actionId == AppConstants.VEHICLE_REQUISITION_REJECT_ACTION_ID ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( vehicleReplaceAdditionalData.getRejectReasonsReportFiles(), Collections.EMPTY_MAP );
            vehicleReplaceApplication.setRejectReasonReportGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else if( actionId == AppConstants.VEHICLE_REQUISITION_APPROVE_ACTION_ID ) {

            Optional<Vehicle> beforeVehicleOptional = vehicleService.findById( vehicleReplaceApplication.getVehicleId() );

            Optional<Vehicle> assignedVehicleOptional = vehicleService.findById( vehicleReplaceApplication.getAssignedVehicleId() );

            vehicleReplaceApplication.setApprovedBy( loggedInUser.getUser() );

            vehicleReplaceApplication.setApprovedDate( LocalDate.now() );

            if( assignedVehicleOptional.isPresent() ){

                setVehicleOwnerFromReplaceApplication( assignedVehicleOptional.get(), vehicleReplaceApplication, beforeVehicleOptional.get() );
                vehicleService.save( assignedVehicleOptional.get() );
            }

            if( beforeVehicleOptional.isPresent() ){

                beforeVehicleOptional.get().setVehicleOwner( null );
            }
        }

        return vehicleReplaceAdditionalData;
    }

    private void setVehicleOwnerFromReplaceApplication(Vehicle vehicle, VehicleReplaceApplication vehicleReplaceApplication, Vehicle beforeVehicle) {

        VehicleOwner vehicleOwner = beforeVehicle.getVehicleOwner();

        vehicleOwner.setOwnerType( vehicleReplaceApplication.getOwnerType() );

        if( vehicleReplaceApplication.getOwnerType().equals( OwnerType.USER )) {

            Optional<User> user = userService.findByUsername(vehicleReplaceApplication.getEmail());

            if(user.isPresent()) {

                vehicleOwner.setUser( user.get() );
            }
        } else if( vehicleReplaceApplication.getOwnerType().equals( OwnerType.DISTRICT )) {

            Optional<GeoLocation> geoLocation = geoLocationService.findById( vehicleReplaceApplication.getDistrictId() );

            if( geoLocation.isPresent() ) {

                vehicleOwner.setGeoLocation( geoLocation.get() );
            }
        } else if( vehicleReplaceApplication.getOwnerType().equals( OwnerType.DIVISION ) ) {

            Optional<GeoLocation> geoLocation = geoLocationService.findById( vehicleReplaceApplication.getDistrictId() );

            if( geoLocation.isPresent() ) {

                vehicleOwner.setGeoLocation( geoLocation.get() );
            }
        } else if( vehicleReplaceApplication.getOwnerType().equals( OwnerType.UPAZILA ) ) {

            Optional<GeoLocation> geoLocation = geoLocationService.findById( vehicleReplaceApplication.getUpazilaId() );

            if( geoLocation.isPresent() ) {

                vehicleOwner.setGeoLocation( geoLocation.get() );
            }
        }

        vehicle.setVehicleOwner( vehicleOwner );

        vehicleOwnerService.logVehicleGiven( vehicleOwner, vehicle );
    }

    @Transactional
    public void replaceVehicle( VehicleReplaceAddRequest vehicleReplaceAddRequest ) {

        freeUserCurrentVehicle( vehicleReplaceAddRequest.getIdentityNumber() );
        Optional<Vehicle> vehicleOptional = vehicleService.findById( vehicleReplaceAddRequest.getVehicleId() );

        if( vehicleOptional.isPresent() ) {

            Optional<VehicleOwner> vehicleOwner = vehicleOwnerService.findById( vehicleReplaceAddRequest.getIdentityNumber() );

            if( vehicleOwner.isPresent() ) {

                vehicleOptional.get().setVehicleOwner( vehicleOwner.get() );
                vehicleService.save( vehicleOptional.get() );
            }


        }
    }

    public void freeUserCurrentVehicle( Long vehicleOwnerId ) {

       Optional<Vehicle> vehicle = getVehicleByVehicleOwnerId( vehicleOwnerId );

        if( vehicle.isPresent() ) {

            vehicle.get().setVehicleOwner( null );
            vehicleService.save( vehicle.get() );
        }
    }

    public DataTablesOutput<VehicleReplaceApplicationSearchResponse> searchForMyDatatable(DataTablesInput dataTablesInput, Long loggedInUserId, HttpServletRequest request ) {

        return vehicleReplaceApplicationRepository.findAll( dataTablesInput,  ReplaceSpecification.filterByCreatedBy( loggedInUserId ), null,  vehicleReplaceApplication -> getReplaceSearchResponseFromReplaceApplication( vehicleReplaceApplication, request ) );
    }

    public Boolean existsById( Long id ) {

        return vehicleReplaceApplicationRepository.existsById( id );
    }

    public void delete( Long id ) {

        vehicleReplaceApplicationRepository.deleteById( id );
    }
}
