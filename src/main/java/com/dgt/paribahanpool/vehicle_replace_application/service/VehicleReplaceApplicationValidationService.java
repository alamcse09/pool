package com.dgt.paribahanpool.vehicle_replace_application.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.enums.StateType;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.noc_application.model.NocApplication;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.model.VehicleOwner;
import com.dgt.paribahanpool.vehicle.repository.VehicleRepository;
import com.dgt.paribahanpool.vehicle.service.VehicleOwnerService;
import com.dgt.paribahanpool.vehicle_replace_application.model.VehicleReplaceAddRequest;
import com.dgt.paribahanpool.vehicle_replace_application.model.VehicleReplaceAdditionalData;
import com.dgt.paribahanpool.vehicle_replace_application.model.VehicleReplaceApplication;
import com.dgt.paribahanpool.vehicle_replace_application.model.VehicleReplaceApplicationAddRequest;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

import static com.dgt.paribahanpool.vehicle.service.VehicleSpecification.filterByUserId;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class VehicleReplaceApplicationValidationService {

    private final MvcUtil mvcUtil;
    private final VehicleRepository vehicleRepository;
    private final StateActionMapService stateActionMapService;
    private final VehicleReplaceApplicationService vehicleReplaceApplicationService;
    private final VehicleOwnerService vehicleOwnerService;

    public boolean view( User user ) {
        return true;
    }

    @Transactional
    public Boolean isValidVehicleUser(User currentUser) {

//        List<VehicleOwner> vehicleOwner = vehicleOwnerService.findByUserId( currentUser.getId() );
//
//        if( vehicleOwner.size() > 0 ){
//
//            return true;
//        }

        return true;
    }

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, VehicleReplaceApplicationAddRequest vehicleReplaceApplicationAddRequest ){

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "vehicleReplaceApplicationAddRequest" )
                        .object( vehicleReplaceApplicationAddRequest )
                        .title( "title.replace.application.add" )
                        .content( "replace-application/replace-application-add" )
                        .activeMenu( Menu.REPLACE_APPLICATION_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.REPLACE_APPLICATION_ADD_FORM, FrontEndLibrary.GEOLOCATION_ADD } )
                        .build()
        );
    }

    public boolean validatePostRequest(RedirectAttributes redirectAttributes, VehicleReplaceApplicationAddRequest vehicleReplaceApplicationAddRequest) {

        if( vehicleReplaceApplicationAddRequest.getId() != null ) {

            Optional<Vehicle> vehicle = vehicleRepository.findById( vehicleReplaceApplicationAddRequest.getId() );

            if( !vehicle.isPresent() ) {

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound");
                return false;
            }
        }

        return true;
    }

    @Transactional
    public boolean takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, UserPrincipal loggedInUser, VehicleReplaceAdditionalData vehicleReplaceAdditionalData ) {

        Optional<VehicleReplaceApplication> vehicleReplaceApplicationOptional = vehicleReplaceApplicationService.findById( id );

        if( !vehicleReplaceApplicationOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }
        else{

            VehicleReplaceApplication vehicleReplaceApplication = vehicleReplaceApplicationOptional.get();
            State state = vehicleReplaceApplication.getState();

            if( !stateActionMapService.actionAllowed( state.getId(), loggedInUser.getRoleIds(), actionId ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.common.action.not-allowed" );
                return false;
            }

            if( actionId.equals( AppConstants.NOC_APP_ENCASHMENT_CHOOSE_MECHANIC_INCHARGE_ACTION_ID ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "error.noc.action.mechanic-incharge-id.not-given" );
                return false;
            }

            if( actionId.equals( AppConstants.VEHICLE_REQUISITION_CHOOSE_ACTION_ID ) && vehicleReplaceAdditionalData.getVehicleId() == null  ){

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
                return false;
            }

            return true;
        }
    }

    public boolean validateReplace(RedirectAttributes redirectAttributes, Long id) {
        // TODO: validation is remaining // need to check valid request

        Optional<VehicleReplaceApplication> application = vehicleReplaceApplicationService.findById( id );

        if( !application.isPresent() ) {

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound");
            return false;
        }

        return true;
    }

    public boolean handleBindingResultForVehicleReplace(Model model, BindingResult bindingResult, VehicleReplaceAddRequest vehicleReplaceAddRequest) {
        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "vehicleReplaceAddRequest" )
                        .object( vehicleReplaceAddRequest )
                        .title( "title.replace.vehicle" )
                        .content( "replace-application/replace-vehicle" )
                        .activeMenu( Menu.REPLACE_VEHICLE_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.REPLACE_VEHICLE_ADD } )
                        .build()
        );
    }

    public boolean isVehicleVacantToAssign( RedirectAttributes redirectAttributes, VehicleReplaceAddRequest vehicleReplaceAddRequest) {

        Optional<Vehicle> vehicleOptional = vehicleRepository.findById(vehicleReplaceAddRequest.getVehicleId());

        if( vehicleOptional.isPresent() && vehicleOptional.get().getVehicleOwner()==null ) {

            return true;
        }
        else {

            mvcUtil.addErrorMessage( redirectAttributes, "validation.vehicle.already-assigned");
            return false;
        }
    }

    public void addValidationMessage(RedirectAttributes redirectAttributes) {

        mvcUtil.addErrorMessage( redirectAttributes, "validation.vehicle.user.do-not-permission");
    }

    public RestValidationResult delete( Long id ) throws NotFoundException {

        Boolean exist = vehicleReplaceApplicationService.existsById( id );

        if( !exist ){

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean create(RedirectAttributes redirectAttributes, Long id) {

        Optional<VehicleReplaceApplication> replaceApplicationOptional = vehicleReplaceApplicationService.findById( id );

        if( !replaceApplicationOptional.isPresent() ) {

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }
        else{

            VehicleReplaceApplication vehicleReplaceApplication = replaceApplicationOptional.get();

            if( vehicleReplaceApplication.getState() == null || ( vehicleReplaceApplication.getState().getStateType() != StateType.POSITIVE_END ) ){

                mvcUtil.addErrorMessage( redirectAttributes, "validation.common.not-positive-end-of-state" );
                return false;
            }
        }

        return true;
    }
}
