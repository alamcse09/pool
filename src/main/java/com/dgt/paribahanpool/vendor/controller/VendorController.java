package com.dgt.paribahanpool.vendor.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.vehicle.model.VehicleAddRequest;
import com.dgt.paribahanpool.vendor.model.VendorAddRequest;
import com.dgt.paribahanpool.vendor.service.VendorModelService;
import com.dgt.paribahanpool.vendor.service.VendorValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/vendor" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired })
public class VendorController extends MVCController {

    private final VendorModelService vendorModelService;
    private final VendorValidationService vendorValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.vendor.add", content = "vendor/vendor-add", activeMenu = Menu.VENDOR_ADD)
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.VENDOR_ADD_FORM } )
    public String add(
            HttpServletRequest httpServletRequest,
            Model model
    ){

        vendorModelService.addVendorGet( model );

        return viewRoot;
    }

    @PostMapping( value = "/add" )
    public String addVendor(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid VendorAddRequest vendorAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !vendorValidationService.handleBindingResultForAddFormPost( model, bindingResult, vendorAddRequest ) ) {

            return viewRoot;
        }

        vendorModelService.addVendorPost( vendorAddRequest, model );
        log( LogEvent.VENDOR_ADD, vendorAddRequest.getId(), "Vendor Added", request );

        return "redirect:/vendor/search";
    }

    @PostMapping( value = "/edit" )
    public String editVendor(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid VendorAddRequest vendorAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !vendorValidationService.handleBindingResultForAddFormPost( model, bindingResult, vendorAddRequest ) ) {

            return viewRoot;
        }
        if( vendorValidationService.editVendor( redirectAttributes, vendorAddRequest.getId() ) ) {

            vendorModelService.addVendorPost( vendorAddRequest, model );
            log( LogEvent.VENDOR_EDIT, vendorAddRequest.getId(), "Vendor Edited", request );
        }

        return "redirect:/vendor/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title = "title.vendor.search", content = "vendor/vendor-search", activeMenu = Menu.VENDOR_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.VENDOR_SEARCH })
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ){

        log.debug( "Vendor Search page called" );
        return viewRoot;
    }

    @GetMapping( "/edit/{vendorId}" )
    @TitleAndContent( title = "title.vendor.edit", content = "vendor/vendor-add", activeMenu = Menu.VENDOR_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.VENDOR_ADD_FORM })
    public String edit(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable("vendorId")  Long vendorId
    ){

        if( vendorValidationService.editVendor( redirectAttributes, vendorId ) ) {

            vendorModelService.editVendor( model, vendorId );
            return viewRoot;
        }

        return "redirect:/vendor/search";
    }

}
