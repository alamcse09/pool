package com.dgt.paribahanpool.vendor.controller;

import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestResponse;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.vendor.model.VendorSearchResponse;
import com.dgt.paribahanpool.vendor.service.VendorService;
import com.dgt.paribahanpool.vendor.service.VendorValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/vendor" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class VendorRestController {

    private final VendorService vendorService;
    private final VendorValidationService vendorValidationService;

    @GetMapping(
            value = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public DataTablesOutput<VendorSearchResponse> searchVehicle(
            DataTablesInput dataTablesInput,
            HttpServletRequest request
    ){

        log.debug( "Request params, {}", dataTablesInput );
        return vendorService.searchForDatatable( dataTablesInput, request );
    }

    @DeleteMapping( "/{id}" )
    public RestResponse delete(
            @PathVariable( "id" ) Long id
    ) throws NotFoundException {

        RestValidationResult restValidationResult = vendorValidationService.delete( id );

        if( restValidationResult.getSuccess() ) {

            vendorService.delete( id );

            return RestResponse.builder().success( true ).message( "success.common.deleted.success" ).build();
        } else {

            return RestResponse.builder().success( false ).message( restValidationResult.getMessage() ).build();
        }
    }
}
