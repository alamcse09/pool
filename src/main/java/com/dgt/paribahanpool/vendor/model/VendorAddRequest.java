package com.dgt.paribahanpool.vendor.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.ItemSupplyType;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
public class VendorAddRequest {

    public Long id;

//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    @NotBlank( message = "{validation.common.required}" )
    public String vendorIdentity;

//    @Pattern( regexp = "[a-zA-Z0-9 ]+", message = "{validation.common.alphanumeric}" )
    @NotBlank( message = "{validation.common.required}" )
    public String vendorName;

    public String address;

    public String contactNumber;

    @Email( message = "{validation.common.email}")
    public String email;

    public ItemSupplyType itemSupplyType;

    public Integer totalExperience;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy)
    public LocalDate contractStartDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy)
    public LocalDate contractEndDate;

    public String comments;

    private MultipartFile[] files;

    List<DocumentMetadata> documents = Collections.EMPTY_LIST;
}
