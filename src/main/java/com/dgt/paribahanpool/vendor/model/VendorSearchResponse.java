package com.dgt.paribahanpool.vendor.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.enums.ItemSupplyType;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class VendorSearchResponse {

    private Long id;
    private String vendorIdentity;
    private String vendorName;
    private String address;
    private String contactNumber;
    private String email;
    private ItemSupplyType itemSupplyType;
    private String itemSupplyTypeStr;
    private Integer totalExperience;
    private LocalDate contractStartDate;
    private LocalDate contractEndDate;
    private String comments;

    private List<DocumentMetadata> documentMetadataList;
}
