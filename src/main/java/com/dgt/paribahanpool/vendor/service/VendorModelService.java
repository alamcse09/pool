package com.dgt.paribahanpool.vendor.service;

import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.model.VehicleAddRequest;
import com.dgt.paribahanpool.vendor.model.Vendor;
import com.dgt.paribahanpool.vendor.model.VendorAddRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class VendorModelService {

    private final MvcUtil mvcUtil;
    private final VendorService vendorService;

    public void addVendorGet(Model model ){

        model.addAttribute( "vendorAddRequest", new VendorAddRequest() );
    }

    public void addVendorPost( VendorAddRequest vendorAddRequest, Model model ) {

        vendorService.save( vendorAddRequest );
        mvcUtil.addSuccessMessage( model, "vendor.add.success" );
    }

    public void editVendor( Model model, Long vendorId ) {

        Optional<Vendor> vendor = vendorService.findById(vendorId);

        if(vendor.isPresent()) {

            VendorAddRequest vendorAddRequest = vendorService.getVendorAddRequestFromVendor( vendor.get() );

            model.addAttribute( "vendorAddRequest", vendorAddRequest );
        }
    }
}
