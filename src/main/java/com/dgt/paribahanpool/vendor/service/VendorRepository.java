package com.dgt.paribahanpool.vendor.service;

import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vendor.model.Vendor;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendorRepository extends DataTablesRepository<Vendor, Long>, JpaRepository<Vendor,Long> {

}
