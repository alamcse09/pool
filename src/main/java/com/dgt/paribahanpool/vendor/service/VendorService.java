package com.dgt.paribahanpool.vendor.service;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.enums.ItemSupplyType;
import com.dgt.paribahanpool.locale.LocalizeUtil;
import com.dgt.paribahanpool.vendor.model.Vendor;
import com.dgt.paribahanpool.vendor.model.VendorAddRequest;
import com.dgt.paribahanpool.vendor.model.VendorSearchResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class VendorService {

    private final DocumentService documentService;
    private final VendorRepository vendorRepository;
    private final LocalizeUtil localizeUtil;

    public Vendor save( Vendor vendor ){

        return vendorRepository.save( vendor );
    }

    private List<Vendor> findAll() {

        return vendorRepository.findAll();
    }

    public Optional<Vendor> findById( Long id ){

        return vendorRepository.findById( id );
    }

    public boolean existById( Long id ) {

        return vendorRepository.existsById(id);
    }

    public void delete( Long id ) {

        vendorRepository.deleteById( id );
    }

    public Vendor save( VendorAddRequest vendorAddRequest ) {

        Vendor vendor = new Vendor();

        if( vendorAddRequest.getId() != null ) {

            vendor = findById( vendorAddRequest.getId() ).get();
        }

        vendor = getVendorFromVendorAddRequest( vendor, vendorAddRequest );

        return save(vendor);
    }

    private Vendor getVendorFromVendorAddRequest(Vendor vendor, VendorAddRequest vendorAddRequest ) {

        vendor.setVendorIdentity( vendorAddRequest.getVendorIdentity() );
        vendor.setVendorName( vendorAddRequest.getVendorName() );
        vendor.setAddress( vendorAddRequest.getAddress() );
        vendor.setContactNumber( vendorAddRequest.getContactNumber() );
        vendor.setEmail( vendorAddRequest.getEmail() );
        vendor.setItemSupplyType( vendorAddRequest.getItemSupplyType() );
        vendor.setTotalExperience( vendorAddRequest.getTotalExperience() );
        vendor.setContractStartDate( vendorAddRequest.getContractStartDate() );
        vendor.setContractEndDate( vendorAddRequest.getContractEndDate() );
        vendor.setComments( vendorAddRequest.getComments() );

        if( vendor.getDocGroupId() == null ) {

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( vendorAddRequest.getFiles(), Collections.EMPTY_MAP );
            vendor.setDocGroupId(documentUploadedResponse.getDocGroupId());
        }

        return vendor;
    }

    public DataTablesOutput<VendorSearchResponse> searchForDatatable(DataTablesInput dataTablesInput, HttpServletRequest request) {

        Specification<Vendor> vendorIsDeletedSpecification = VendorSpecification.filterByIsDeleted( false );
        return vendorRepository.findAll( dataTablesInput, null, vendorIsDeletedSpecification, ( vendor ) -> getVendorSearchResponseFromVendor( vendor, request ) );
    }

    public VendorSearchResponse getVendorSearchResponseFromVendor( Vendor vendor ){

        return getVendorSearchResponseFromVendor( vendor, null );
    }

    public VendorSearchResponse getVendorSearchResponseFromVendor( Vendor vendor, HttpServletRequest request ){

        VendorSearchResponse vendorSearchResponse = new VendorSearchResponse();

        vendorSearchResponse.setId( vendor.getId() );
        vendorSearchResponse.setVendorIdentity( vendor.getVendorIdentity() );
        vendorSearchResponse.setVendorName( vendor.getVendorName() );
        vendorSearchResponse.setAddress( vendor.getAddress() );
        vendorSearchResponse.setContactNumber( vendor.getContactNumber() );
        vendorSearchResponse.setEmail( vendor.getEmail() );

        vendorSearchResponse.setItemSupplyType( vendor.getItemSupplyType() );

        if( request != null )
            vendorSearchResponse.setItemSupplyTypeStr( localizeUtil.getMessageFromMessageSource( ItemSupplyType.getLabel( vendor.getItemSupplyType() ), request ) );

        vendorSearchResponse.setTotalExperience( vendor.getTotalExperience() );
        vendorSearchResponse.setContractStartDate( vendor.getContractStartDate() );
        vendorSearchResponse.setContractEndDate( vendor.getContractEndDate() );
        vendorSearchResponse.setComments( vendor.getComments() );

        List<DocumentMetadata> documentDataList = documentService.findDocumentMetaDataListByGroupId( vendor.getDocGroupId() );
        vendorSearchResponse.setDocumentMetadataList( documentDataList );

        return vendorSearchResponse;
    }

    public VendorAddRequest getVendorAddRequestFromVendor(Vendor vendor) {

        VendorAddRequest vendorAddRequest = new VendorAddRequest();

        vendorAddRequest.setId( vendor.getId() );
        vendorAddRequest.setVendorIdentity( vendor.getVendorIdentity() );
        vendorAddRequest.setVendorName( vendor.getVendorName() );
        vendorAddRequest.setAddress( vendor.getAddress() );
        vendorAddRequest.setContactNumber( vendor.getContactNumber() );
        vendorAddRequest.setEmail( vendor.getEmail() );
        vendorAddRequest.setItemSupplyType( vendor.getItemSupplyType() );
        vendorAddRequest.setTotalExperience( vendor.getTotalExperience() );
        vendorAddRequest.setContractStartDate( vendor.getContractStartDate() );
        vendorAddRequest.setContractEndDate( vendor.getContractEndDate() );
        vendorAddRequest.setComments( vendor.getComments() );

        if( vendor.getDocGroupId() != null && vendor.getDocGroupId() > 0 ){

            List<DocumentMetadata> documentMetadataList = documentService.findDocumentMetaDataListByGroupId( vendor.getDocGroupId() );
            vendorAddRequest.setDocuments( documentMetadataList );
        }

        return vendorAddRequest;
    }

    public List<VendorSearchResponse> getAllVendorSearchResponseList() {

        List<Vendor> vendorList = findAll();

        return getVendorSearchListFromVendorList( vendorList )
;    }

    private List<VendorSearchResponse> getVendorSearchListFromVendorList(List<Vendor> vendorList) {

        return vendorList
                .stream()
                .map( this::getVendorSearchResponseFromVendor )
                .collect( Collectors.toList() );
    }

    public List<Vendor> allVendors() {
        return vendorRepository.findAll();
    }
}
