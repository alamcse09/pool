package com.dgt.paribahanpool.vendor.service;

import com.dgt.paribahanpool.vendor.model.Vendor;
import com.dgt.paribahanpool.vendor.model.Vendor_;
import org.springframework.data.jpa.domain.Specification;

public class VendorSpecification {

    public static Specification<Vendor> filterByIsDeleted(Boolean isDeleted ){

        if( isDeleted == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( Vendor_.IS_DELETED ), isDeleted );
    }

}
