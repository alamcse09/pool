package com.dgt.paribahanpool.vendor.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vendor.model.Vendor;
import com.dgt.paribahanpool.vendor.model.VendorAddRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class VendorValidationService {

    private final MvcUtil mvcUtil;
    private final VendorService vendorService;

    public Boolean handleBindingResultForAddFormPost(Model model, BindingResult bindingResult, VendorAddRequest vendorAddRequest ){

        return mvcUtil.handleBindingResult(

                bindingResult,
                model,
                HandleBindingResultParams
                        .builder()
                        .key( "vendorAddRequest" )
                        .object( vendorAddRequest )
                        .title( "title.vendor.add" )
                        .content( "vendor/vendor-add" )
                        .activeMenu( Menu.VENDOR_ADD )
                        .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.VENDOR_ADD_FORM } )
                        .build()
        );
    }

    public Boolean editVendor(RedirectAttributes redirectAttributes, Long vendorId ) {

        Optional<Vendor> vendor = vendorService.findById(vendorId);
        if(!vendor.isPresent()) {

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    public RestValidationResult delete ( Long id ) throws NotFoundException {

        if( !vendorService.existById( id ) ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }
}
