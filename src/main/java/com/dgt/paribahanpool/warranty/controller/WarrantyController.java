package com.dgt.paribahanpool.warranty.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.incident.model.IncidentAdditionalData;
import com.dgt.paribahanpool.log.model.LogEvent;
import com.dgt.paribahanpool.warranty.model.WarrantyAddRequest;
import com.dgt.paribahanpool.warranty.model.WarrantyAdditionalData;
import com.dgt.paribahanpool.warranty.service.WarrantyModelService;
import com.dgt.paribahanpool.warranty.service.WarrantyValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping( "/warranty" )
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class WarrantyController extends MVCController {
    private final WarrantyModelService warrantyModelService;
    private final WarrantyValidationService warrantyValidationService;

    @GetMapping( "/add" )
    @TitleAndContent( title = "title.warranty.add", content = "warranty/warranty-add", activeMenu = Menu.WARRANTY_ADD )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.WARRANTY_ADD_FORM } )
    public String add(
            HttpServletRequest request,
            Model model
    ){

        warrantyModelService.addWarrantyGet( model );
        return viewRoot;
    }

    @PostMapping( "/add" )
    public String addBillPost(
            Model model,
            RedirectAttributes redirectAttributes,
            @Valid WarrantyAddRequest warrantyAddRequest,
            BindingResult bindingResult,
            HttpServletRequest request
    ){

        if( !warrantyValidationService.handleBindingResultForAddFormPost( model, bindingResult, warrantyAddRequest ) ){

            return viewRoot;
        }

        warrantyModelService.addWarrantyPost( warrantyAddRequest, redirectAttributes );
        return "redirect:/warranty/search";
    }

    @GetMapping( "/search" )
    @TitleAndContent( title="title.warranty.search", content="warranty/warranty-search", activeMenu = Menu.WARRANTY_SEARCH )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.WARRANTY_SEARCH })
    public String getSearchPage(
            HttpServletRequest request,
            Model model
    ) {

        log.debug( "Meeting Search page called" );
        return viewRoot;
    }

    @GetMapping( "/{id}" )
    @TitleAndContent( title = "title.warranty.view", content = "warranty/warranty-view", activeMenu = Menu.WARRANTY_VIEW )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.JQUERY_PRINT, FrontEndLibrary.WARRANTY_VIEW } )
    public String view(
            @PathVariable( "id" ) Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest,
            Model model
    ){

        if( warrantyValidationService.view( redirectAttributes, id ) ){

            warrantyModelService.view( model, id, getLoggedInUser() );
            return viewRoot;
        }

        return "redirect:/warranty/search";
    }

    @PostMapping( "/take-action" )
    public String takeAction(
            @RequestParam( "id" ) Long id,
            @RequestParam( "action" ) Long actionId,
            @Valid WarrantyAdditionalData warrantyAdditionalData,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            Model model
    ) throws Exception {

        if( warrantyValidationService.takeActionValidation( actionId, redirectAttributes ) ) {

            warrantyModelService.takeAction( redirectAttributes, id, actionId, warrantyAdditionalData, getLoggedInUser() );
            log( LogEvent.WARRANTY_DECISION_TAKEN, actionId, "Warranty Take action", request );
        }

        return "redirect:/warranty/search";
    }
}
