package com.dgt.paribahanpool.warranty.model;

import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.converter.EscapeHtmlConverter;
import com.dgt.paribahanpool.util.converter.StringTrimConverter;
import com.dgt.paribahanpool.util.model.AuditableEntity;
import com.dgt.paribahanpool.vendor.model.Vendor;
import com.dgt.paribahanpool.workflow.model.State;
import com.dgt.paribahanpool.workflow.model.WorkflowEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table( name = "warranty")
@SQLDelete( sql = "UPDATE warranty set is_deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT )
@Where( clause = "is_deleted = false" )
public class Warranty extends AuditableEntity implements WorkflowEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "warrantyDate" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyDate;

    @Column( name = "registrationNumber" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String registrationNumber;

    @Column( name = "chassisNumber" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String chassisNumber;

    @Column( name = "brandName" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String brandName;

    @Column( name = "vendorName" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String vendorName;

    @Column( name = "model" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String model;

    @Column( name = "partsName" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String partsName;

    @Column( name = "partsId" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String partsId;

    @Column( name = "comment", columnDefinition = "TEXT" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String comment;

    @Column( name = "comment_decision", columnDefinition = "TEXT" )
    @Convert( converter = StringTrimConverter.class )
    @Convert( converter = EscapeHtmlConverter.class )
    private String commentDecision;

    @Column( name = "warrantyBeginningDate" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyBeginningDate;

    @Column( name = "warrantyEndingDate" )
    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyEndingDate;

    @Column( name = "document_group_id" )
    private Long docGroupId;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "vendor_id", foreignKey = @ForeignKey( name = "fk_warranty_vendor_id" ) )
    private Vendor vendor;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumn( name = "state_id", foreignKey = @ForeignKey( name = "fk_warranty_state_id" ) )
    private State state;
}
