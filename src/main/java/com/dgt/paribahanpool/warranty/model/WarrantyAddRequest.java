package com.dgt.paribahanpool.warranty.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.util.NameResponse;
import com.dgt.paribahanpool.vendor.model.VendorSearchResponse;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
public class WarrantyAddRequest {

    private Long id;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyBeginningDate;

    @DateTimeFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyEndingDate;

    private String registrationNumber;
    private String chassisNumber;
    private String brandName;
    private String vendorName;
    private String model;
    private String partsName;
    private String partsId;
    private String comment;
    private List<VendorSearchResponse> vendorViewResponseList;

    private Long vendorId;

    private MultipartFile[] files;

    private List<DocumentMetadata> documents = Collections.EMPTY_LIST;
}
