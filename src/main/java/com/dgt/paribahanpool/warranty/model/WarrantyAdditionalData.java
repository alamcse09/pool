package com.dgt.paribahanpool.warranty.model;

import com.dgt.paribahanpool.workflow.model.WorkflowAdditionalData;
import lombok.Data;

@Data
public class WarrantyAdditionalData implements WorkflowAdditionalData {

    private String commentDecision;
}
