package com.dgt.paribahanpool.warranty.model;

import com.dgt.paribahanpool.document.model.DocumentMetadata;
import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.dgt.paribahanpool.workflow.model.StateResponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class WarrantySearchResponse {

    private Long id;

    @JsonFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyDate;

    private String registrationNumber;
    private String chassisNumber;
    private String brandName;
    private String vendorName;
    private String model;
    private String partsName;
    private String partsId;
    private String comment;

    @JsonFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyBeginningDate;

    @JsonFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy )
    private LocalDate warrantyEndingDate;

    private Long daysRemaining;

    private List<DocumentMetadata> documents;

    private StateResponse stateResponse;
}
