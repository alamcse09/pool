package com.dgt.paribahanpool.warranty.service;

import com.dgt.paribahanpool.incident.model.Incident;
import com.dgt.paribahanpool.incident.model.IncidentViewResponse;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vendor.model.VendorSearchResponse;
import com.dgt.paribahanpool.vendor.service.VendorService;
import com.dgt.paribahanpool.warranty.model.Warranty;
import com.dgt.paribahanpool.warranty.model.WarrantyAddRequest;
import com.dgt.paribahanpool.warranty.model.WarrantyAdditionalData;
import com.dgt.paribahanpool.warranty.model.WarrantySearchResponse;
import com.dgt.paribahanpool.workflow.model.StateActionMapResponse;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class WarrantyModelService {

    private final WarrantyService warrantyService;
    private final MvcUtil mvcUtil;
    private final VendorService vendorService;
    private final StateActionMapService stateActionMapService;

    public void addWarrantyGet(Model model){

        WarrantyAddRequest warrantyAddRequest = new WarrantyAddRequest();

        List<VendorSearchResponse> vendorSearchResponseList = vendorService.getAllVendorSearchResponseList();

        warrantyAddRequest.setVendorViewResponseList( vendorSearchResponseList );

        model.addAttribute("warrantyAddRequest",warrantyAddRequest);
    }

    public void addWarrantyPost(WarrantyAddRequest warrantyAddRequest, RedirectAttributes redirectAttributes) {
        warrantyService.save(warrantyAddRequest);
        mvcUtil.addSuccessMessage(redirectAttributes,"success.warranty.add");
    }

    @Transactional
    public void view(Model model, Long id, UserPrincipal loggedInUser) {

        Optional<Warranty> warrantyOptional = warrantyService.findById( id );

        if( warrantyOptional.isPresent() ){

            WarrantySearchResponse warrantyViewResponse = warrantyService.getWarrantySearchResponseFromWarranty( warrantyOptional.get() );
            model.addAttribute( "warrantyViewResponse", warrantyViewResponse );



            if( warrantyOptional.get().getState().getId() != null ){

                List<StateActionMapResponse> stateActionMapResponseList = stateActionMapService.findStateActionMapResponseListByCurrentStateAndRoleIdSet( warrantyOptional.get().getState().getId(), loggedInUser.getRoleIds() );
                model.addAttribute( "stateActionList", stateActionMapResponseList );
            }
        }
    }

    public void takeAction(RedirectAttributes redirectAttributes, Long id, Long actionId, WarrantyAdditionalData warrantyAdditionalData, UserPrincipal loggedInUser) throws Exception {

        warrantyService.takeAction( id, actionId, warrantyAdditionalData, loggedInUser );
        mvcUtil.addSuccessMessage( redirectAttributes, "success.common.action.success" );
    }
}
