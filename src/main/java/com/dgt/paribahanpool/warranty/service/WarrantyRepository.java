package com.dgt.paribahanpool.warranty.service;

import com.dgt.paribahanpool.warranty.model.Warranty;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface WarrantyRepository extends DataTablesRepository<Warranty,Long> {
}
