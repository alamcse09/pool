package com.dgt.paribahanpool.warranty.service;

import com.dgt.paribahanpool.committee.model.Committee;
import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.document.model.Document;
import com.dgt.paribahanpool.document.model.DocumentUploadedResponse;
import com.dgt.paribahanpool.document.service.DocumentService;
import com.dgt.paribahanpool.incident.model.Incident;
import com.dgt.paribahanpool.incident.model.IncidentAdditionalData;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vendor.model.Vendor;
import com.dgt.paribahanpool.vendor.service.VendorService;
import com.dgt.paribahanpool.warranty.model.Warranty;
import com.dgt.paribahanpool.warranty.model.WarrantyAddRequest;
import com.dgt.paribahanpool.warranty.model.WarrantyAdditionalData;
import com.dgt.paribahanpool.warranty.model.WarrantySearchResponse;
import com.dgt.paribahanpool.workflow.model.*;
import com.dgt.paribahanpool.workflow.service.StateActionMapService;
import com.dgt.paribahanpool.workflow.service.StateService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class WarrantyService implements WorkflowService<Warranty> {

    private final WarrantyRepository warrantyRepository;
    private final DocumentService documentService;
    private final StateService stateService;
    private final StateActionMapService stateActionMapService;
    private final VendorService vendorService;

    public Optional<Warranty> findById(Long id ){

        return warrantyRepository.findById( id );
    }

    public Warranty save(Warranty warranty) {

        return warrantyRepository.save( warranty );
    }

    @Transactional
    public void save(WarrantyAddRequest warrantyAddRequest) {

        Warranty warranty = new Warranty();
        
        if( warrantyAddRequest.getId() != null){
            
            warranty = findById( warrantyAddRequest.getId() ).get();
        }

        warranty = getWarrantyFromWarrantyAddRequest( warranty, warrantyAddRequest);
        save( warranty);
    }

    private Warranty getWarrantyFromWarrantyAddRequest(Warranty warranty, WarrantyAddRequest warrantyAddRequest) {

        warranty.setWarrantyDate(warrantyAddRequest.getWarrantyDate());
        warranty.setRegistrationNumber(warrantyAddRequest.getRegistrationNumber());
        warranty.setChassisNumber(warrantyAddRequest.getChassisNumber());
        warranty.setBrandName(warrantyAddRequest.getBrandName());
        warranty.setVendorName(warrantyAddRequest.getVendorName());
        warranty.setModel(warrantyAddRequest.getModel());
        warranty.setPartsName(warrantyAddRequest.getPartsName());
        warranty.setPartsId(warrantyAddRequest.getPartsId());
        warranty.setComment(warrantyAddRequest.getComment());
        warranty.setWarrantyBeginningDate(warrantyAddRequest.getWarrantyBeginningDate());
        warranty.setWarrantyEndingDate(warrantyAddRequest.getWarrantyEndingDate());

        if( warrantyAddRequest.getVendorId() != null ){

            Optional<Vendor> vendorOptional = vendorService.findById( warrantyAddRequest.getVendorId() );
            if( vendorOptional.isPresent() ){

                Vendor vendor = vendorOptional.get();
                warranty.setVendor( vendor );
            }
        }

        if( warranty.getState() == null ){

            Long startStateId = AppConstants.WARRANTY_SENT_TO_VENDOR_STATE_ID;

            State state = stateService.findStateReferenceById( startStateId );
            warranty.setState( state );
        }

        if( warranty.getDocGroupId() == null ){

            DocumentUploadedResponse documentUploadedResponse = documentService.buildFromMultipartFile( warrantyAddRequest.getFiles(), Collections.EMPTY_MAP );
            warranty.setDocGroupId( documentUploadedResponse.getDocGroupId() );
        }
        else{

            List<Document> documentList = documentService.buildFromMultipartFile( warrantyAddRequest.getFiles(), warranty.getDocGroupId(), Collections.EMPTY_MAP );
            documentService.save( documentList );
        }

        return warranty;
    }

    public DataTablesOutput<WarrantySearchResponse> searchForDatatable( DataTablesInput dataTablesInput ) {

        return warrantyRepository.findAll( dataTablesInput, this::getWarrantySearchResponseFromWarranty );
    }

    public WarrantySearchResponse getWarrantySearchResponseFromWarranty( Warranty warranty ) {

        State state = warranty.getState();

        StateResponse stateResponse = null;

        if( state != null ){

            stateResponse = state.getResponse();
        }

        return WarrantySearchResponse.builder()
                .id( warranty.getId() )
                .warrantyDate( warranty.getWarrantyDate() )
                .registrationNumber( warranty.getRegistrationNumber() )
                .chassisNumber( warranty.getChassisNumber() )
                .brandName( warranty.getBrandName() )
                .vendorName( warranty.getVendorName() )
                .model( warranty.getModel() )
                .partsName( warranty.getPartsName() )
                .partsId( warranty.getPartsId() )
                .comment( warranty.getComment() )
                .warrantyBeginningDate( warranty.getWarrantyBeginningDate() )
                .warrantyEndingDate( warranty.getWarrantyEndingDate() )
                .stateResponse( stateResponse )
                .daysRemaining( DAYS.between( LocalDate.now(), warranty.getWarrantyEndingDate() ) )
                .documents( documentService.findDocumentMetaDataListByGroupId( warranty.getDocGroupId() ) )
                .build();
    }

    public Boolean existById( Long id ) {

        return warrantyRepository.existsById( id );
    }

    public void delete( Long id ) {

        warrantyRepository.deleteById( id );
    }

    public void takeAction(Long id, Long actionId, WarrantyAdditionalData warrantyAdditionalData, UserPrincipal loggedInUser) throws Exception {

        stateActionMapService.takeAction( this, id, actionId, loggedInUser, (workflowEntity ) -> setAdditionalData( workflowEntity, actionId, warrantyAdditionalData ));
    }

    private WorkflowAdditionalData setAdditionalData(WorkflowEntity workflowEntity, Long actionId, WorkflowAdditionalData additionalActionData ) {

        Warranty warranty = (Warranty) workflowEntity;

        WarrantyAdditionalData warrantyAdditionalData = (WarrantyAdditionalData) additionalActionData;

        if( actionId.equals( AppConstants.WARRANTY_REJECT_ACTION_ID) ){

            warranty.setCommentDecision( warrantyAdditionalData.getCommentDecision() );
        }

        return additionalActionData;
    }
}
