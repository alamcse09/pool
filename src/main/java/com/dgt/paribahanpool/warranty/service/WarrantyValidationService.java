package com.dgt.paribahanpool.warranty.service;

import com.dgt.paribahanpool.base.HandleBindingResultParams;
import com.dgt.paribahanpool.bill.model.BillAddRequest;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.exceptions.NotFoundException;
import com.dgt.paribahanpool.rest.RestValidationResult;
import com.dgt.paribahanpool.util.MvcUtil;
import com.dgt.paribahanpool.vehicle.model.Vehicle;
import com.dgt.paribahanpool.vehicle.model.VehicleAddRequest;
import com.dgt.paribahanpool.warranty.model.Warranty;
import com.dgt.paribahanpool.warranty.model.WarrantyAddRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired} )
public class WarrantyValidationService {

    private final MvcUtil mvcUtil;
    private final WarrantyService warrantyService;

    public boolean validateWarrantyUniqueConstraints( WarrantyAddRequest warrantyAddRequest) {

        if (    warrantyAddRequest.getId() == null ||
                warrantyAddRequest.getWarrantyDate() == null ||
                warrantyAddRequest.getChassisNumber() == null ||
                warrantyAddRequest.getRegistrationNumber() == null ||
                warrantyAddRequest.getWarrantyBeginningDate() == null ||
                warrantyAddRequest.getWarrantyEndingDate() != null) return false;

        return true;
    }

    public Boolean handleBindingResultForAddFormPost( Model model, BindingResult bindingResult, WarrantyAddRequest warrantyAddRequest ){

        HandleBindingResultParams params = HandleBindingResultParams
                .builder()
                .key( "warrantyAddRequest" )
                .object( warrantyAddRequest )
                .title( "title.warranty.add" )
                .content( "warranty/warranty-add" )
                .activeMenu( Menu.WARRANTY_ADD )
                .frontEndLibraries( new FrontEndLibrary[]{ FrontEndLibrary.WARRANTY_ADD_FORM } )
                .build();

        Boolean isValid = mvcUtil.handleBindingResult(

                bindingResult,
                model,
                params
        );

        if( warrantyAddRequest.getId() == null ) {

            if( warrantyAddRequest.getWarrantyBeginningDate() != null && warrantyAddRequest.getWarrantyEndingDate().isBefore( warrantyAddRequest.getWarrantyBeginningDate() ) ) {

                return setModelWithError( model, params, "error.warranty.invalid.begin.end.date");
            }
        }

        return isValid;
    }

    private Boolean setModelWithError(Model model, HandleBindingResultParams params, String errorMessage) {
        mvcUtil.addErrorMessage( model, errorMessage );
        model.addAttribute( params.getKey(), params.getObject() );
        mvcUtil.addTitleAndContent( model, params.getTitle(), params.getContent(), params.getActiveMenu() );

        if( params.getFrontEndLibraries() != null ){

            mvcUtil.addCssAndJsByLibraryName( model, params.getFrontEndLibraries() );
        }

        return false;
    }

    public RestValidationResult delete( Long id ) throws NotFoundException {

        Boolean exist = warrantyService.existById( id );
        if( !exist ) {

            throw new NotFoundException( "validation.common.notfound" );
        }

        return RestValidationResult.valid();
    }

    public Boolean view(RedirectAttributes redirectAttributes, Long id) {

        Optional<Warranty> warrantyOptional = warrantyService.findById( id );

        if( !warrantyOptional.isPresent() ){

            mvcUtil.addErrorMessage( redirectAttributes, "validation.common.notfound" );
            return false;
        }

        return true;
    }

    public Boolean takeActionValidation(Long actionId, RedirectAttributes redirectAttributes) {

        return true;
    }
}
