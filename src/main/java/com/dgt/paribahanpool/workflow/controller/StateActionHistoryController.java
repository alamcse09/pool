package com.dgt.paribahanpool.workflow.controller;

import com.dgt.paribahanpool.annotations.AddFrontEndLibrary;
import com.dgt.paribahanpool.annotations.TitleAndContent;
import com.dgt.paribahanpool.base.MVCController;
import com.dgt.paribahanpool.enums.FrontEndLibrary;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.workflow.service.StateActionHistoryModelService;
import com.dgt.paribahanpool.workflow.service.StateActionHistoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Slf4j
@Controller
@RequestMapping( "/workflow/history" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class StateActionHistoryController extends MVCController {

    private final StateActionHistoryService stateActionHistoryService;
    private final StateActionHistoryModelService stateActionHistoryModelService;

    @GetMapping( "/{table}/{id}" )
    @TitleAndContent( title = "title.workflow.history", content = "workflow/workflow-history" )
    @AddFrontEndLibrary( libraries = { FrontEndLibrary.WORKFLOW_HISTORY } )
    public String showHistory(
            @PathVariable( "table" ) String tableName,
            @PathVariable( "id" ) Long id,
            @RequestParam( "active" ) Menu acitveMenu,
            Model model
    ){

        stateActionHistoryModelService.getHistoryPage( model, tableName, id, acitveMenu );
        return viewRoot;
    }
}
