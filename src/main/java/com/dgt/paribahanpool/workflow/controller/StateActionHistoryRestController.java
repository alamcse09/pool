package com.dgt.paribahanpool.workflow.controller;

import com.dgt.paribahanpool.base.BaseRestController;
import com.dgt.paribahanpool.workflow.model.StateActionHistorySearchResponse;
import com.dgt.paribahanpool.workflow.service.StateActionHistoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping( "/api/workflow/history" )
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class StateActionHistoryRestController extends BaseRestController {

    private final StateActionHistoryService stateActionHistoryService;

    @GetMapping( "/search/{tableName}/{id}" )
    public DataTablesOutput<StateActionHistorySearchResponse> search(

            DataTablesInput dataTablesInput,
            @PathVariable( "tableName" ) String tableName,
            @PathVariable( "id" ) Long id,
            HttpServletRequest request
    ){

        return stateActionHistoryService.search( dataTablesInput, tableName, id, getLang( request ) );
    }
}
