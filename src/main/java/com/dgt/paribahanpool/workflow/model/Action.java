package com.dgt.paribahanpool.workflow.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table( name = "action" )
public class Action {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @NotNull
    @Column( name = "name" )
    private String name;

    @NotNull
    @Column( name = "name_en" )
    private String nameEn;
}
