package com.dgt.paribahanpool.workflow.model;

import com.dgt.paribahanpool.enums.StateType;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table( name = "state" )
public class State {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @NotNull
    @Column( name = "name" )
    private String name;

    @NotNull
    @Column( name = "name_en" )
    private String nameEn;

    @Column( name = "state_type" )
    private StateType stateType;

    @Column( name = "description", columnDefinition = "TEXT" )
    private String description;

    public StateResponse getResponse(){

        return StateResponse.builder()
                .id( this.getId() )
                .name( this.getName() )
                .nameEn( this.getNameEn() )
                .stateType( this.getStateType() )
                .build();
    }
}
