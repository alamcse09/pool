package com.dgt.paribahanpool.workflow.model;

import com.dgt.paribahanpool.util.model.AuditableEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "state_action_history" )
public class StateActionHistory extends AuditableEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Long id;

    @Column( name = "previous_state_en" )
    private String previousStateEn;

    @Column( name = "previous_state_bn" )
    private String previousStateBn;

    @Column( name = "next_state_en" )
    private String nextStateEn;

    @Column( name = "next_state_bn" )
    private String nextStateBn;

    @Column( name = "action_bn" )
    private String actionBn;

    @Column( name = "action_en" )
    private String actionEn;

    @Column( name = "action_by_designation_en" )
    private String actionByDesignationEn;

    @Column( name = "action_by_designation_bn" )
    private String actionByDesignationBn;

    @Column( name = "action_by_name_en" )
    private String actionByNameEn;

    @Column( name = "action_by_name_bn" )
    private String actionByNameBn;

    @Column( name = "additional_data", columnDefinition = "TEXT" )
    private String additionalData;

    @Column( name = "table_name" )
    private String table_name;

    @Column( name = "entity_id" )
    private Long entityId;
}
