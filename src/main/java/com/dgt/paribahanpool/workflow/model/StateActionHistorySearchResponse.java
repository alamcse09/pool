package com.dgt.paribahanpool.workflow.model;

import com.dgt.paribahanpool.util.DateTimeFormatPattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class StateActionHistorySearchResponse {

    private Long id;
    private String prevState;
    private String action;
    private String nextState;

    @JsonFormat( pattern = DateTimeFormatPattern.dateFormat_slash_ddMMyyyy_hhmma )
    private LocalDateTime timestamp;

    private String actionBy;
    private String designation;
    private String description;
}
