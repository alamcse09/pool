package com.dgt.paribahanpool.workflow.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(
        name = "state_action_map",
        uniqueConstraints = {
                @UniqueConstraint( name = "uk_state_action_map_state_action_id", columnNames = { "state_id", "action_id" } )
        },
        indexes = {
                @Index( name = "idx_state_action_map_state_role_id", columnList = "state_id,role_id" ),
                @Index( name = "idx_state_action_map_state_action_id", columnList = "state_id,action_id" )
        }
)
@ToString( exclude = { "state", "action", "nextState" })
@EqualsAndHashCode( exclude = { "state", "action", "nextState" })
public class StateActionMap {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @NotNull
    @OneToOne
    @JoinColumn( name = "state_id", foreignKey = @ForeignKey( name = "fk_state_action_map_state_id" ) )
    private State state;

    @NotNull
    @Column( name = "role_id" )
    private Long roleId;

    @NotNull
    @OneToOne
    @JoinColumn( name = "action_id", foreignKey = @ForeignKey( name = "fk_state_action_map_action_id" ) )
    private Action action;

    @NotNull
    @OneToOne
    @JoinColumn( name = "next_state_id", foreignKey = @ForeignKey( name = "fk_state_action_map_state_id_next" ) )
    private State nextState;

    @Column( name = "view_only" )
    private Boolean viewOnly = false;
}
