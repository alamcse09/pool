package com.dgt.paribahanpool.workflow.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StateActionMapResponse {

    private Long actionId;
    private String actionName;
    private String actionNameEn;
    private Boolean viewOnly;
}
