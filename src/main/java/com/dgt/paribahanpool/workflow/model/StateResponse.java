package com.dgt.paribahanpool.workflow.model;

import com.dgt.paribahanpool.enums.StateType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StateResponse {

    private Long id;
    private String name;
    private String nameEn;
    private StateType stateType;
}
