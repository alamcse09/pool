package com.dgt.paribahanpool.workflow.model;

public interface WorkflowEntity {

    State getState();
    void setState( State state );

    Long getId();

    default String getModuleName(){

        return "";
    }
}
