package com.dgt.paribahanpool.workflow.model;

import java.util.Optional;

public interface WorkflowService<T extends WorkflowEntity> {

    Optional<T> findById( Long id );
    T save( T t );
}
