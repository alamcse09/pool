package com.dgt.paribahanpool.workflow.service;

import com.dgt.paribahanpool.workflow.model.Action;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActionRepository extends JpaRepository<Action,Long> {
}
