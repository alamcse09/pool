package com.dgt.paribahanpool.workflow.service;

import com.dgt.paribahanpool.workflow.model.Action;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired})
public class ActionService {

    private final ActionRepository actionRepository;

    public Optional<Action> findById( Long id ){

        return actionRepository.findById( id );
    }
}
