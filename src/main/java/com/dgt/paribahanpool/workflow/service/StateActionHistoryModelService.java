package com.dgt.paribahanpool.workflow.service;

import com.dgt.paribahanpool.config.AppConstants;
import com.dgt.paribahanpool.enums.Menu;
import com.dgt.paribahanpool.util.NameResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class StateActionHistoryModelService {

    public void getHistoryPage( Model model, String tableName, Long id, Menu activeMenu ){

        model.addAttribute( "tableName", tableName );
        model.addAttribute( "id", id );
        model.addAttribute( "activeMenu", activeMenu );
        model.addAttribute( "moduleNameResponse", AppConstants.tableNameToModuleMap.getOrDefault( tableName, NameResponse.builder().nameBn("").nameEn("").build() ) );
    }
}
