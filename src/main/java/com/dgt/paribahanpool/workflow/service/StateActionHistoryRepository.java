package com.dgt.paribahanpool.workflow.service;

import com.dgt.paribahanpool.workflow.model.StateActionHistory;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface StateActionHistoryRepository extends DataTablesRepository<StateActionHistory,Long> {
}
