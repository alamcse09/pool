package com.dgt.paribahanpool.workflow.service;

import com.dgt.paribahanpool.locale.LocalizeUtil;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.workflow.model.StateActionHistory;
import com.dgt.paribahanpool.workflow.model.StateActionHistorySearchResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

import static com.dgt.paribahanpool.workflow.service.StateActionHistorySpecification.filterByEntityId;
import static com.dgt.paribahanpool.workflow.service.StateActionHistorySpecification.filterByTableName;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class StateActionHistoryService {

    private final StateActionHistoryRepository stateActionHistoryRepository;

    public StateActionHistory save( StateActionHistory stateActionHistory ){

        return stateActionHistoryRepository.save( stateActionHistory );
    }

    public DataTablesOutput<StateActionHistorySearchResponse> search( DataTablesInput dataTablesInput, String tableName, Long id, String lang ) {

        Specification<StateActionHistory> filterByTableNameAndEntityIdSpecification = filterByTableName( tableName ).and( filterByEntityId( id ) );
        return stateActionHistoryRepository.findAll( dataTablesInput, null, filterByTableNameAndEntityIdSpecification, ( stateActionHistory ) -> getStateActionHistorySearchResponseFromStateActionHistory( stateActionHistory, lang ) );
    }

    private StateActionHistorySearchResponse getStateActionHistorySearchResponseFromStateActionHistory( StateActionHistory stateActionHistory, String lang ) {

        return StateActionHistorySearchResponse.builder()
                .id( stateActionHistory.getId() )
                .action( lang.equals( "en" )? stateActionHistory.getActionEn(): stateActionHistory.getActionBn() )
                .prevState( lang.equals( "en" )? stateActionHistory.getPreviousStateEn(): stateActionHistory.getPreviousStateBn() )
                .nextState( lang.equals( "en" )? stateActionHistory.getNextStateEn(): stateActionHistory.getNextStateBn() )
                .designation( lang.equals( "en" )? stateActionHistory.getActionByDesignationEn(): stateActionHistory.getActionByDesignationBn() )
                .actionBy( lang.equals( "en" )? stateActionHistory.getActionByNameEn(): stateActionHistory.getActionByNameBn() )
                .description( stateActionHistory.getAdditionalData() )
                .timestamp( stateActionHistory.getCreatedAt() )
                .build();
    }
}
