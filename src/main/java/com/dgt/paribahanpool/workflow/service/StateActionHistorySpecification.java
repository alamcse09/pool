package com.dgt.paribahanpool.workflow.service;

import com.dgt.paribahanpool.workflow.model.StateActionHistory;
import com.dgt.paribahanpool.workflow.model.StateActionHistory_;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

public class StateActionHistorySpecification {

    public static Specification<StateActionHistory> filterByTableName( String tableName ){

        if( StringUtils.isBlank( tableName ) )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(StateActionHistory_.TABLE_NAME ), tableName );
    }

    public static Specification<StateActionHistory> filterByEntityId( Long entityId ){

        if( entityId == null || entityId <= 0 )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(StateActionHistory_.ENTITY_ID ), entityId );
    }
}
