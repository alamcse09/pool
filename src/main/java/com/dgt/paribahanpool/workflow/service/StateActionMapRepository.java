package com.dgt.paribahanpool.workflow.service;

import com.dgt.paribahanpool.workflow.model.StateActionMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface StateActionMapRepository extends JpaRepository<StateActionMap, Long>, JpaSpecificationExecutor<StateActionMap> {
}
