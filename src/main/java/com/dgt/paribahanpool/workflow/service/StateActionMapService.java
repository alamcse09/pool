package com.dgt.paribahanpool.workflow.service;

import com.dgt.paribahanpool.notification.model.Notification;
import com.dgt.paribahanpool.notification.model.NotificationMetadata;
import com.dgt.paribahanpool.notification.model.NotificationType;
import com.dgt.paribahanpool.notification.service.NotificationService;
import com.dgt.paribahanpool.security.model.UserPrincipal;
import com.dgt.paribahanpool.user.model.User;
import com.dgt.paribahanpool.user.service.UserService;
import com.dgt.paribahanpool.util.Util;
import com.dgt.paribahanpool.workflow.model.*;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.ast.Not;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.dgt.paribahanpool.workflow.service.StateActionMapSpecification.*;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = { @Autowired } )
public class StateActionMapService {

    private final StateActionMapRepository stateActionMapRepository;
    private final ActionService actionService;
    private final StateActionHistoryService stateActionHistoryService;
    private final UserService userService;
    private final NotificationService notificationService;

    @Autowired
    @Qualifier( "state_action_service" )
    private Gson gson;

    @Transactional
    public List<StateActionMap> findStateActionMapSetByCurrentStateAndRoleIdSet( Long currentStateId, Set<Long> roleIds ){

        Specification<StateActionMap> findByCurrentStateAndRoleIdsSpecification = findByCurrentStateId( currentStateId ).and( findByRoleIdSet( roleIds ) );
        return stateActionMapRepository.findAll( findByCurrentStateAndRoleIdsSpecification );
    }

    public List<StateActionMapResponse> findStateActionMapResponseListByCurrentStateAndRoleIdSet(Long currentStateId, Set<Long> roleIds ){

        List<StateActionMap> stateActionMapList = findStateActionMapSetByCurrentStateAndRoleIdSet( currentStateId, roleIds );
        return stateActionMapList.stream().map( this::getStateActionMapResponseFromStateActionMap ).collect(Collectors.toList());
    }

    private StateActionMapResponse getStateActionMapResponseFromStateActionMap( StateActionMap stateActionMap ) {

        Action action = stateActionMap.getAction();
        return StateActionMapResponse.builder()
                .actionId( action.getId() )
                .actionName( action.getName() )
                .actionNameEn( action.getNameEn() )
                .viewOnly( stateActionMap.getViewOnly() )
                .build();
    }

    public StateActionMap findByCurrentStateIdAndActionId( Long currentStateId, Long actionId ) {

        Specification<StateActionMap> findByCurrentStateIdAndActionIdSpecification = findByCurrentStateId( currentStateId ).and( findByActionId( actionId ) );
        List<StateActionMap> stateActionMapList = stateActionMapRepository.findAll( findByCurrentStateIdAndActionIdSpecification );

        if( stateActionMapList.size() > 1 )
            throw new RuntimeException( "error.stateactionmap.stateaction.unique-violation" );

        if( stateActionMapList.size() == 0 )
            throw  new RuntimeException( String.format( "No next state found for given action. Current State %d, Action id %d", currentStateId, actionId ) );

        return stateActionMapList.get( 0 );
    }

    @Transactional
    public WorkflowEntity takeAction( WorkflowService workflowService, Long workflowEntityId, Long actionId, UserPrincipal loggedInUser ) {

        return takeAction( workflowService, workflowEntityId, actionId, loggedInUser, null );
    }

    @Transactional
    public WorkflowEntity takeAction( WorkflowService workflowService, Long workflowEntityId, Long actionId, UserPrincipal loggedInUser, Function<WorkflowEntity,WorkflowAdditionalData> additionalDataSetter ) {

        Optional<WorkflowEntity> workflowEntityOptional = workflowService.findById( workflowEntityId );

        State previousState = null;

        if( workflowEntityOptional.isPresent() ){

            WorkflowEntity workflowEntity = workflowEntityOptional.get();

            previousState = workflowEntity.getState();

            StateActionMap stateActionMap = takeAction( workflowEntity, actionId );

            WorkflowAdditionalData additionalData = null;
            if( additionalDataSetter != null ) {

                additionalData = additionalDataSetter.apply( workflowEntity );
            }

            addHistory( workflowEntity, previousState, actionId, additionalData, loggedInUser );

            sendNotification( workflowEntity, stateActionMap );

            return workflowService.save( workflowEntity );
        }else{

            return null;
        }
    }

    private void sendNotification( WorkflowEntity workflowEntity, StateActionMap stateActionMap ) {

        Long roleId = stateActionMap.getRoleId();
        List<User> users = userService.findUserByRoleId( roleId );
        NotificationMetadata notificationMetadata = Util.getNotificationMetadataForModule( workflowEntity.getModuleName() );

        if( notificationMetadata != null ) {

            List<Notification> notifications = new ArrayList<>();

            for (User user : users) {

                notifications.add(
                        Notification.of(
                                NotificationType.WORKFLOW_ACTION,
                                notificationMetadata.getTitleEn(),
                                notificationMetadata.getTitleBn(),
                                notificationMetadata.getTextEn(),
                                notificationMetadata.getTextBn(),
                                notificationMetadata.getRedirectUrl() == null ? "#" : notificationMetadata.getRedirectUrl().replace("{id}", workflowEntity.getId() + ""),
                                workflowEntity.getModuleName(),
                                workflowEntity.getId(),
                                user
                        )
                );
            }

            notificationService.save(notifications);
        }
    }

    private void addHistory( WorkflowEntity workflowEntity, State previousState, Long actionId, WorkflowAdditionalData additionalData, UserPrincipal loggedInUser ) {

        Optional<Action> actionOptional = actionService.findById( actionId );
        State nextState = workflowEntity.getState();

        if( actionOptional.isPresent() ) {

            Action action = actionOptional.get();

            StateActionHistory stateActionHistory = new StateActionHistory();

            stateActionHistory.setActionBn( action.getName() );
            stateActionHistory.setActionEn( action.getNameEn() );
            stateActionHistory.setActionByDesignationBn(loggedInUser.getDesignation() );
            stateActionHistory.setActionByDesignationEn(loggedInUser.getDesignationEn() );
            stateActionHistory.setNextStateBn( nextState.getName() );
            stateActionHistory.setNextStateEn( nextState.getNameEn() );
            stateActionHistory.setPreviousStateBn( previousState.getName() );
            stateActionHistory.setPreviousStateEn( previousState.getNameEn() );
            stateActionHistory.setEntityId( workflowEntity.getId() );
            stateActionHistory.setAdditionalData( gson.toJson( additionalData ) );

            Table table = workflowEntity.getClass().getAnnotation( Table.class );
            if( table == null ){

                throw new RuntimeException( "Workflow entity didnt use Table annotation" );
            }

            stateActionHistory.setTable_name( table.name() );

            stateActionHistoryService.save( stateActionHistory );
        }
    }

    private StateActionMap takeAction( WorkflowEntity workflowEntity, Long actionId ) {

        Long currentStateId = workflowEntity.getState().getId();
        if( currentStateId == null || currentStateId <= 0 )
            throw new RuntimeException( "Invalid state id" );

        StateActionMap stateActionMap = findByCurrentStateIdAndActionId( currentStateId, actionId );

        workflowEntity.setState( stateActionMap.getNextState() );

        return stateActionMap;
    }

    public Boolean actionAllowed( Long currentState, Set<Long> roleIds, Long actionWantsToTake ){

        StateActionMapResponse allowedStateActionMapResponse = findStateActionMapResponseListByCurrentStateAndRoleIdSet( currentState, roleIds ).stream()
                .filter( stateActionMapResponse -> stateActionMapResponse.getActionId().equals( actionWantsToTake ) )
                .findAny()
                .orElse( null );

        return allowedStateActionMapResponse != null;
    }

    public static Boolean onlyViewActions( List<StateActionMapResponse> stateActionMapResponseList ){

        return stateActionMapResponseList.stream()
                .filter( stateActionMapResponse -> !stateActionMapResponse.getViewOnly() )
                .count() == 0;
    }
}
