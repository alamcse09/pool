package com.dgt.paribahanpool.workflow.service;

import com.dgt.paribahanpool.workflow.model.StateActionMap;
import com.dgt.paribahanpool.workflow.model.StateActionMap_;
import org.springframework.data.jpa.domain.Specification;

import java.util.Set;

public class StateActionMapSpecification {

    public static Specification<StateActionMap> findByCurrentStateId(Long currentStateId ){

        if( currentStateId == null )
            return Specification.where( null );
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get(StateActionMap_.STATE ).get( "id" ), currentStateId );
    }

    public static Specification<StateActionMap> findByActionId( Long actionId ){

        if( actionId == null )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal( root.get( StateActionMap_.ACTION ).get( "id" ), actionId );
    }

    public static Specification<StateActionMap> findByRoleIdSet( Set<Long> roleIdSet ){

        if( roleIdSet == null || roleIdSet.size() == 0 )
            return Specification.where( null );

        return (root, query, criteriaBuilder) -> root.get( StateActionMap_.ROLE_ID ).in( roleIdSet );
    }
}
