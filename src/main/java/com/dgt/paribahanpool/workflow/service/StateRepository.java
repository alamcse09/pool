package com.dgt.paribahanpool.workflow.service;

import com.dgt.paribahanpool.workflow.model.State;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StateRepository extends JpaRepository<State,Long> {
}
