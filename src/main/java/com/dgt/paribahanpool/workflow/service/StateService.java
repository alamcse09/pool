package com.dgt.paribahanpool.workflow.service;

import com.dgt.paribahanpool.workflow.model.State;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor( onConstructor_ = {@Autowired} )
public class StateService {

    private final StateRepository stateRepository;

    public Optional<State> findById(Long id ){

        return stateRepository.findById( id );
    }

    public State findStateReferenceById( Long id ){

        return stateRepository.getById( id );
    }
}
