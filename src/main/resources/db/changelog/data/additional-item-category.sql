INSERT INTO inventory_category (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en) VALUES (3, null, 1, false, null, 1, 'ব্রেক প্যাড', 'Brake Pad');
INSERT INTO inventory_category (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en) VALUES (4, null, 1, false, null, 1, 'ক্লাচ প্লেট', 'Clutch Plate');
INSERT INTO inventory_category (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en) VALUES (5, null, 1, false, null, 1, 'প্রেশার প্লেট', 'Pressure pLate');
INSERT INTO inventory_category (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en) VALUES (6, null, 1, false, null, 1, 'এফ আর শক', 'Fr shock');
INSERT INTO inventory_category (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en) VALUES (7, null, 1, false, null, 1, 'রিলিজ বিয়ারিং', 'Release Bearing');
INSERT INTO inventory_category (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en) VALUES (8, null, 1, false, null, 1, 'ব্রেক শু', 'Brake Shoe');

