INSERT INTO state (id, description, name, name_en, state_type)
VALUES (48, 'Bill created', 'বিল তৈরি করা হয়েছে', 'Bill created', 0);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (49, 'Forwarded to TO', 'টি ওর কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded to TO', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (50, 'Forwarded to AD', 'এ ডির কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded to AD', 3);

INSERT INTO action (name, name_en)
VALUES ('টি ওর কাছে ফরোয়ার্ড করুন', 'Forward to TO');

INSERT INTO action (name, name_en)
VALUES ('এ ডির কাছে ফরোয়ার্ড করুন', 'Forward to AD');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2622, 'ROLE_FORWARD_TO_TO', 'বিল ব্যবস্থাপনা', 'Bill Management', 'টি ওর কাছে ফরোয়ার্ড', 'Forward to TO');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2623, 'ROLE_FORWARD_TO_AD', 'বিল ব্যবস্থাপনা', 'Bill Management', 'এ ডির কাছে ফরোয়ার্ড', 'Forward to AD');

INSERT INTO state_action_map (role_id, view_only, action_id, next_state_id, state_id) VALUES (2622, false, 39, 49, 48);

INSERT INTO state_action_map (role_id, view_only, action_id, next_state_id, state_id) VALUES (2623, false, 40, 50, 48);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 2622);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 2623);

