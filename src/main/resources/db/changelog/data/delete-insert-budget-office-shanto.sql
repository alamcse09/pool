DELETE
FROM budget_office
WHERE id = 1;

INSERT INTO budget_office (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn,
                                          name_en)
VALUES (1, '2021-09-01 09:57:38.438519', 1, false, '2021-09-01 09:57:38.438519', 1, 'সরকারি সড়ক পরিবহন পুল ',
        'Government Road Transport Pool');

INSERT INTO budget_office (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn,
                                          name_en)
VALUES (2, '2021-09-01 09:57:38.438519', 1, false, '2021-09-01 09:57:38.438519', 1, 'জেলা সরকারি সড়ক পরিবহন পুল ',
        'District Government Road Transport Pool');

INSERT INTO budget_office (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn,
                                          name_en)
VALUES (3, '2021-09-01 09:57:38.438519', 1, false, '2021-09-01 09:57:38.438519', 1, 'উপজেলা সরকারি সড়ক পরিবহন পুল ',
        'Upazila Government Road Transport Pool');

INSERT INTO budget_office (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn,
                                          name_en)
VALUES (4, '2021-09-01 09:57:38.438519', 1, false, '2021-09-01 09:57:38.438519', 1, 'সরকারি নৌ পরিবহন পুল ',
        'Government Marine Transport Pool');

INSERT INTO budget_office (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn,
                                          name_en)
VALUES (5, '2021-09-01 09:57:38.438519', 1, false, '2021-09-01 09:57:38.438519', 1, 'জেলা সরকারি নৌ পরিবহন পুল',
        'District Government Marine Transport Pool');

INSERT INTO budget_office (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn,
                                          name_en)
VALUES (6, '2021-09-01 09:57:38.438519', 1, false, '2021-09-01 09:57:38.438519', 1, 'উপজেলা সরকারি নৌ পরিবহন পুল',
        'Upazila Government Marine Transport Pool');

INSERT INTO budget_office (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn,
                                          name_en)
VALUES (7, '2021-09-01 09:57:38.438519', 1, false, '2021-09-01 09:57:38.438519', 1, 'সরকারি যানবাহন মেরামত কারখানা ',
        'Government vehicle repair factory');

