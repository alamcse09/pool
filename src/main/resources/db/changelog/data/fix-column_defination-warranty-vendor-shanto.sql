alter table warranty modify comment text null;

alter table vendor modify comments text null;

alter table vehicle_auction_data modify committee_decision text null;

alter table vehicle_auction_data modify sold_address_en text null;

alter table vehicle_auction_data modify sold_address_bn text null;

alter table complains modify details text null;

alter table budget_code modify discription_en text null;

alter table budget_code modify discription_bn text null;

alter table incident modify incident_details text null;

