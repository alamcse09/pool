UPDATE action t
SET t.name    = 'উচ্চমান সহকারী এর কাছে ফরোয়ার্ড করুন',
    t.name_en = 'Forward to Upper Division Officer'
WHERE t.id = 116;

UPDATE action t
SET t.name    = 'উচ্চমান সহকারী এর কাছে ফরোয়ার্ড করুন ভেরিফাই এর জন্য',
    t.name_en = 'Forward to Upper Division For Verification'
WHERE t.id = 103;

UPDATE action t
SET t.name    = 'উচ্চমান সহকারী এর কাছে ফরোয়ার্ড করুন',
    t.name_en = 'Forward to Upper Division Officer'
WHERE t.id = 127;

UPDATE action t
SET t.name    = 'উচ্চমান সহকারী এর কাছে ফরোয়ার্ড করুন',
    t.name_en = 'Forward to Upper Division'
WHERE t.id = 108;

UPDATE action t
SET t.name    = 'উচ্চমান সহকারী এর কাছে ফরোয়ার্ড করুন',
    t.name_en = 'Forward to Upper Division Officer'
WHERE t.id = 132;

UPDATE state t
SET t.description = 'Forwarded to Upper Division for Correctness',
    t.name        = 'উচ্চমান সহকারী এর কাছে পাঠানো হয়েছে সঠিকঠা নির্ণয় এর জন্য '
WHERE t.id = 132;

UPDATE state t
SET t.description = 'Forward to Upper Division',
    t.name        = 'উচ্চমান সহকারী এর কাছে পাঠানো হয়েছে'
WHERE t.id = 142;

UPDATE state t
SET t.description = 'Sent To Upper Division',
    t.name        = 'উচ্চমান সহকারী এর কাছে পাঠানো হয়েছে'
WHERE t.id = 123;

UPDATE state t
SET t.description = 'Sent To Upper Division For Verification',
    t.name        = 'উচ্চমান সহকারী এর কাছে পাঠানো হয়েছে ভেরিফাই এর জন্য'
WHERE t.id = 119;


UPDATE role t
SET t.role_text_bn = 'উচ্চমান সহকারী এর কাছে ফরোয়ার্ড করা',
    t.role_text_en = 'Forward to Upper Division'
WHERE t.id = 1247;

UPDATE role t
SET t.role_text_bn = 'উচ্চমান সহকারী এর কাছে ফরোয়ার্ড করা ভেরিফাই এর জন্য',
    t.role_text_en = 'Forward to Upper Division For Verification'
WHERE t.id = 1242;

UPDATE role t
SET t.role_text_bn = 'উচ্চমান সহকারী এর সকল অ্যাকশন',
    t.role_text_en = 'All actions of Upper Division'
WHERE t.id = 1256;


INSERT INTO state (id, description, name, name_en, state_type)
VALUES (152, 'Forward to Commissioner', 'ডকমিশনারের কাছে পাঠানো হয়েছে', 'Forward to Commissioner', 3);


INSERT INTO action (id, name, name_en)
VALUES (145, 'কমিশনারের কাছে ফরোয়ার্ড করুন', 'Forward to Commissioner');

UPDATE state_action_map t
SET t.action_id     = 145,
    t.next_state_id = 152
WHERE t.id = 196;

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (207, 1261, false, 135, 145, 152);


INSERT INTO action (id, name, name_en)
VALUES (146, 'বিবেচনা করা হউক', 'May it be considered');


INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (208, 1261, false, 146, 145, 148);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (209, 1261, false, 146, 145, 149);

UPDATE action t
SET t.name    = 'স্ট্যান্ডিং কমিটি এর রায় রিপোর্ট আপলোড করুন',
    t.name_en = 'Upload stand-in committee verdict report'
WHERE t.id = 137;


UPDATE action t
SET t.name    = 'ডিএ এর কাছে সংশোধনের জন্য প্রেরণ করুন',
    t.name_en = 'Forward to DA for correction'
WHERE t.id = 117;

UPDATE action t
SET t.name    = 'স্ট্যান্ডিং কমিটি এর রায় রিপোর্ট আপলোড করুন',
    t.name_en = 'Upload stand-in committee verdict report'
WHERE t.id = 137;

