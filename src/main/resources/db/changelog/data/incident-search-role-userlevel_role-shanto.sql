INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1149, 'ROLE_INCIDENT_SEARCH', 'ইনসিডেন্ট', 'Incident', 'ইনসিডেন্ট অনুসন্ধান', 'Incident Search');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1150, 'ROLE_INCIDENT_DELETE', 'ইনসিডেন্ট', 'Incident', 'ইনসিডেন্ট মুছে ফেলা', 'Incident Delete');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1151, 'ROLE_INCIDENT_VIEW', 'ইনসিডেন্ট', 'Incident', 'ইনসিডেন্ট ভিউ', 'Incident View');


INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1149);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1150);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1151);