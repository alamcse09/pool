UPDATE state t
SET t.description = 'Forwarded to Commissioner',
    t.name        = 'কমিশনারের কাছে ফরোয়ার্ড করা হয়েছে ',
    t.name_en     = 'Forwarded to Commissioner'
WHERE t.id = 60;


INSERT INTO state (id, description, name, name_en, state_type)
VALUES (81, 'Sent for Investigation', 'তদন্তের জন্য পাঠানো হয়েছে', 'Sent for Investigation', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (82, 'Submit Report to Commissioner', 'কমিশনারের কাছে প্রতিবেদন দাখিল করা হয়েছে',
        'Submit Report to Commissioner', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (83, 'Action Taken (Fine)', 'জরিমানা পদক্ষেপ নেয়া হয়েছে ', 'Action Taken (Fine)', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (84, 'Fine Collected', 'জরিমানা সংগৃহীত', 'Fine Collected', 3);



UPDATE role t
SET t.name         = 'ROLE_STANDIND_COMMITTEE_VERIFY_REPORT',
    t.role_text_bn = 'প্রতিবেদন যাচাই-বাছাই করবে স্থায়ী কমিটি',
    t.role_text_en = 'Standing Committee to verify report'
WHERE t.id = 1153;

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1170, 'ROLE_FORWARD_TO_COMMISSIONER', 'ইনসিডেন্ট', 'Incident', 'কমিশনারের কাছে ফরোয়ার্ড করা হয়েছে ',
        'Incident Forwarded to commissioner');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1171, 'ROLE_SENT_FOR_INVESTIGATION', 'ইনসিডেন্ট', 'Incident', 'তদন্তের জন্য পাঠানো হয়েছে',
        'Sent for Investigation');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1172, 'ROLE_SUBMITTED_TO_COMMISSIONER', 'ইনসিডেন্ট', 'Incident', 'কমিশনারের কাছে প্রতিবেদন দাখিল করা হয়েছে',
        'Submitted to Commissioner');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1173, 'ROLE_FINE_ACTION_TAKEN', 'ইনসিডেন্ট', 'Incident', 'জরিমানা পদক্ষেপ নেয়া হয়েছে ', 'Action Taken (Fine)');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1174, 'ROLE_FINE_COLLECTED', 'ইনসিডেন্ট', 'Incident', 'জরিমানা সংগৃহীত', 'Fine Collected');



UPDATE action t
SET t.name    = 'তদন্ত প্রতিবেদন তৈরি করুন এবং স্থায়ী কমিটিতে পেশ করুন',
    t.name_en = 'Generate Investigation Report & Submit to standing committee'
WHERE t.id = 51;

INSERT INTO action (id, name, name_en)
VALUES (70, 'তদন্তের জন্য পাঠানো হোক ', 'Send For Investigation');

INSERT INTO action (id, name, name_en)
VALUES (71, 'চালান এর রিপোর্ট তৈরী করুন ও কমিশনারের কাছে ফরওয়ার্ড করুন ',
        'Make a report of chalan and forward it to the Commissioner');

INSERT INTO action (id, name, name_en)
VALUES (72, 'ফাইন সেট করুন ', 'Set fine');

INSERT INTO action (id, name, name_en)
VALUES (73, 'জরিমানা সংগ্রহ', 'Collect Fine');


DELETE
FROM state_action_map
WHERE id = 108;

UPDATE state_action_map t
SET t.role_id = 1152
WHERE t.id = 109;

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (120, 1170, false, 70, 81, 60);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (121, 1171, false, 50, 61, 81);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (122, 1153, false, 71, 82, 62);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (123, 1172, false, 72, 83, 82);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (124, 1173, false, 73, 84, 83);


INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1170);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1171);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1172);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1173);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1174);