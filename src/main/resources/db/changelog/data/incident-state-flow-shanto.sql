INSERT INTO state (id, description, name, name_en, state_type) VALUES (60, 'Incident Report Instantiated', 'ঘটনা রিপোর্ট তৈরি করা হয়েছে ', 'Incident Report Instantiated', 0);
INSERT INTO state (id, description, name, name_en, state_type) VALUES (61, 'Forwarded to Action Taker', 'অ্যাকশন টেকারের কাছে পাঠানো হয়েছে', 'Forwarded to Action Taker', 3);
INSERT INTO state (id, description, name, name_en, state_type) VALUES (62, 'Upload Inquiry Report', 'ইনসিডেন্টের তদন্ত রিপোর্ট আপলোড করুন', 'Upload Inquiry Report', 3);


INSERT INTO action (id, name, name_en) VALUES (50, 'তদন্ত টিম তৈরি করুন', 'Create Inquiry Team');
INSERT INTO action (id, name, name_en) VALUES (51, 'তদন্ত রিপোর্ট আপলোড করুন', 'Upload Inquiry Report');




INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (1152, 'ROLE_CREATE_INQUIRY_TEAM', 'ইনসিডেন্ট', 'Incident', 'ইন্সিডেন্টের জন্য তদন্ত টিম তৈরি করুন ', 'Create Inquiry Team for Incident');
INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (1153, 'ROLE_UPLOAD_INQUIRY_REPORT', 'ইনসিডেন্ট', 'Incident', 'ইনসিডেন্টের তদন্ত রিপোর্ট আপলোড করুন', 'Upload Inquiry Report');


INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 1152);
INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 1153);


INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) VALUES (108, 1152, false, 50, 61, 60);
INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) VALUES (109, 1153, false, 51, 62, 61);