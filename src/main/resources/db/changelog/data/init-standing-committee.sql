INSERT INTO committee (id, created_at, created_by, is_deleted, modified_at, modified_by, name, name_en,
                                desination, position_in_committee)
VALUES (-1, '2022-01-31 15:13:20.000000', 1, false, '2022-01-31 15:13:26.000000', 1, 'স্ট্যান্ডিং কমিটি মেম্বার ১',
        'Standing Committee Member 1', 'Secretary', 1);

INSERT INTO committee (id, created_at, created_by, is_deleted, modified_at, modified_by, name, name_en,
                                desination, position_in_committee)
VALUES (-2, '2022-01-31 15:13:20.000000', 1, false, '2022-01-31 15:13:26.000000', 1, 'স্ট্যান্ডিং কমিটি মেম্বার ২',
        'Standing Committee Member 2', 'Secretary', 0);

INSERT INTO committee (id, created_at, created_by, is_deleted, modified_at, modified_by, name, name_en,
                                desination, position_in_committee)
VALUES (-3, '2022-01-31 15:13:20.000000', 1, false, '2022-01-31 15:13:26.000000', 1, 'স্ট্যান্ডিং কমিটি মেম্বার ৩',
        'Standing Committee Member 3', 'Secretary', 2);

