INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (1, 'ROLE_ADMIN', 'এডমিন', 'Admin', 'সিস্টেম এডমিন', 'System Admin');
INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (100, 'ROLE_VEHICLE_ADD', 'যানবাহন', 'Vehicle', 'যানবাহন সংযুক্ত করুন', 'Add Vehicle');

INSERT INTO user_level (id, name_bn, name_en, level) VALUES (1, 'টেস্ট', 'Test', 1);
INSERT INTO user_level (id, name_bn, name_en, level) VALUES (2, 'এডমিন', 'Admin', 1);
INSERT INTO user_level (id, name_bn, name_en, level) VALUES (3, 'ইনচার্জ', 'Incharge', 1);
INSERT INTO user_level (id, name_bn, name_en, level) VALUES (4, 'মেকানিক', 'Mechanic', 1);

INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 1);
INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 100);