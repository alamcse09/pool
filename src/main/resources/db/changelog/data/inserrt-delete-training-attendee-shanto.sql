INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1185, 'ROLE_TRAINING_ATTENDEE_ADD', 'প্রশিক্ষণ', 'Training', 'প্রশিক্ষণের অংশগ্রহণকারী সংযুক্তকরণ',
        'Training Attendee Add');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1186, 'ROLE_TRAINING_ATTENDEE_SEARCH', 'প্রশিক্ষণ', 'Training', 'প্রশিক্ষণের অংশগ্রহণকারী তালিকা',
        'Training Attendee Search');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1187, 'ROLE_TRAINING_TRAINER_DELETE', 'প্রশিক্ষণ', 'Training', 'প্রশিক্ষণের প্রশিক্ষক মুছে ফেলা',
        'Training Trainer Delete');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1188, 'ROLE_TRAINING_TRAINEE_DELETE', 'প্রশিক্ষণ', 'Training', 'প্রশিক্ষণের শিক্ষানবিস মুছে ফেলা',
        'Training Trainer Delete');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1189, 'ROLE_TRAINING_COURSE_DIRECTOR_DELETE', 'প্রশিক্ষণ', 'Training',
        'প্রশিক্ষণের কোর্স পরিচালক / সমন্বয়ক মুছে ফেলা', 'Training Course Director Delete');



INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1185);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1186);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1187);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1188);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1189);

