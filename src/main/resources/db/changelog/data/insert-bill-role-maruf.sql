INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2620, 'ROLE_BILL_ADD', 'বিল ব্যবস্থাপনা', 'Bill Management', 'বিল যুক্ত করুন', 'Add Bill');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2621, 'ROLE_BILL_SEARCH', 'বিল ব্যবস্থাপনা', 'Bill Management', 'বিল খুজুন', 'Search Bill');