INSERT INTO brand (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en) VALUES (1, '2022-01-03 15:34:59', 1, false, '2022-01-03 15:34:59', 1, 'নিসান', 'NISSAN');
INSERT INTO brand (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en) VALUES (2, '2022-01-03 15:34:59', 1, false, '2022-01-03 15:34:59', 1, 'টয়োটা', 'TOYOTA');
INSERT INTO brand (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en) VALUES (3, '2022-01-03 15:34:59', 1, false, '2022-01-03 15:34:59', 1, 'মিতসুবিশি', 'MITSUBISHI');


INSERT INTO model (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en, brand_id) VALUES (1, '2022-01-03 15:34:59', 1, false, '2022-01-03 15:34:59', 1, 'N 16', 'N 16', 1);
INSERT INTO model (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en, brand_id) VALUES (3, null, 2, false, '2022-01-03 15:34:59', 1, 'হায়েস ', 'HIACE', 2);
INSERT INTO model (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en, brand_id) VALUES (4, null, 3, false, '2022-01-03 15:34:59', 1, 'পাজেরো স্পোর্ট', 'PAJERO SPORT', 3);


INSERT INTO color (id, color_name_bn, color_name_en) VALUES (11, 'সিলভার', 'SILVER');