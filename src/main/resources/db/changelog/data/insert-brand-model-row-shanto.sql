INSERT INTO brand (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en)
VALUES (1, '2021-12-09 12:22:13.000000', 1, false, '2021-12-09 12:22:13.000000', 1, 'টয়োটা ', 'Toyota');

INSERT INTO brand (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en)
VALUES (2, '2021-12-09 12:22:13.000000', 1, false, '2021-12-09 12:22:13.000000', 1, 'হোন্ডা ', 'honda');

INSERT INTO model (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en,
                                  brand_id)
VALUES (1, '2021-12-09 12:22:13.000000', 1, false, '2021-12-09 12:22:13.000000', 1, 'আলিওন', 'Allion', 1);

INSERT INTO model (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn, name_en,
                                  brand_id)
VALUES (2, '2021-12-09 12:22:13.000000', 1, false, '2021-12-09 12:22:13.000000', 1, 'প্রিমিও ', 'Premio', 2);
