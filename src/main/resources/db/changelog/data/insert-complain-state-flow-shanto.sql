INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1158, 'ROLE_COMPLAIN_UPLOAD_SHOW_CAUSE', 'অভিযোগ', 'Complain', 'কারণ দর্শানোর পরিপেক্ষিতে পত্র সংযুক্ত করুন ',
        'Upload Show Cause Letter');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1159, 'ROLE_COMPLAIN_UPLOAD_RESPONSE', 'অভিযোগ', 'Complain', 'কারণ দর্শানোর জবাব পত্র সংযুক্ত করুন ',
        'Upload Reply Letter To Show Cause');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1160, 'ROLE_COMPLAIN_CREATE_COMMITTEE', 'অভিযোগ', 'Complain', 'তদন্ত টিম তৈরি করুন ', 'Create Inquiry Team');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1161, 'ROLE_COMPLAIN_REQUEST_FOR_INVESTIGATION_REPORT', 'অভিযোগ', 'Complain',
        'তদন্ত পত্রের জন্য অনুরোধ পত্র আপলোড করুন', 'Upload Request For Investigation Letter');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1162, 'ROLE_COMPLAIN_LEGAL_ISSUE', 'অভিযোগ', 'Complain', 'আইনি অভিযোগ', 'Legal Complaint  ');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1163, 'ROLE_COMPLAIN_NORMAL_COMPLAIN_GENERATE_REPORT', 'অভিযোগ', 'Complain',
        'সাধারণ অভিযোগ, রিপোর্ট তৈরি করুন ', 'Normal Complain, Generate A Report');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1164, 'ROLE_COMPLAIN_GENERATE_JUDGEMENT_REPORT', 'অভিযোগ', 'Complain',
        'আইনি অভিযোগ, রিপোর্ট তৈরি করুন রায় দিয়ে', 'Legal Complain, Generate A Report With Judgement');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1165, 'ROLE_COMPLAIN_ACTION_TAKEN', 'অভিযোগ', 'Complain', 'ব্যবস্থা নেওয়া হয়েছে', 'Action Taken');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1166, 'ROLE_COMPLAIN_VIEW', 'অভিযোগ', 'Complain', 'অভিযোগ ভিউ ', 'Complain View');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1167, 'ROLE_COMPLAIN_RESPONSE_RECEIVED', 'অভিযোগ', 'Complain', 'অভিযোগ প্রতিক্রিয়া চিঠি পেয়েছি ',
        'Complain Received Response Letter');


INSERT INTO state (id, description, name, name_en, state_type)
VALUES (63, 'Forward To Commissioner', 'কমিশনারের কাছে ফরোয়ার্ড করুন', 'Forward To Commissioner', 0);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (64, 'Forward To Accused User', 'অভিযুক্ত ব্যবহারকারীর কাছে ফরোয়ার্ড করুন', 'Forward To Accused User', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (65, 'Create Committee', 'কমিটি গঠন করুন', 'Create Committee', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (66, 'Request for Investigation Report Uploaded', 'তদন্ত প্রতিবেদনের জন্য অনুরোধ পত্র সংযুক্ত করা হয়েছে ',
        'Request for Investigation Report Uploaded', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (67, 'Legal Issue', 'আইনি সমস্যা', 'Legal Issue', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (68, 'Forward to Commissioner', 'কমিশনারের কাছে ফরোয়ার্ড করুন', 'Forward to Commissioner', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (69, 'Action Taken', 'ব্যবস্থা নেওয়া হয়েছে', 'Action Taken', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (70, 'Response Recieved', 'একটি প্রতিক্রিয়া চিঠি পেয়েছি', 'Received a response letter', 3);




INSERT INTO action (id, name, name_en)
VALUES (52, 'কারণ দর্শানোর পরিপেক্ষিতে পত্র সংযুক্ত করুন ', 'Upload Show Cause Letter');

INSERT INTO action (id, name, name_en)
VALUES (53, 'কারণ দর্শানোর জবাব পত্র সংযুক্ত করুন ', 'Upload Response Letter');

INSERT INTO action (id, name, name_en)
VALUES (54, 'তদন্ত টিম তৈরি করুন ', 'Create Committee');

INSERT INTO action (id, name, name_en)
VALUES (55, 'তদন্ত পত্রের জন্য অনুরোধ পত্র আপলোড করুন', 'Request for Investigation Report');

INSERT INTO action (id, name, name_en)
VALUES (56, 'আইনি অভিযোগ', 'Legal Issue');

INSERT INTO action (id, name, name_en)
VALUES (57, 'সাধারণ অভিযোগের রিপোর্ট তৈরি করুন ও কমিশনারের কাছে ফরোয়ার্ড করুন ', 'Normal Issue');

INSERT INTO action (id, name, name_en)
VALUES (58, 'আইনি অভিযোগের রিপোর্ট তৈরি করুন ও কমিশনারের কাছে ফরোয়ার্ড করুন ', 'Upload Report With Judgement');

INSERT INTO action (id, name, name_en)
VALUES (59, 'পদক্ষেপ গ্রহণ করুন', 'Take Action');



INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (110, 1158, false, 52, 64, 63);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (111, 1159, false, 53, 70, 64);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (112, 1160, false, 54, 65, 63);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (113, 1161, false, 55, 66, 65);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (114, 1162, false, 56, 67, 66);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (115, 1163, false, 57, 68, 66);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (116, 1164, false, 58, 68, 67);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (117, 1165, false, 59, 69, 68);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (118, 1166, true, 7, 69, 69);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (119, 1167, false, 54, 65, 70);


INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1158);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1159);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1160);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1161);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1162);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1163);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1164);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1165);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1166);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1167);

