INSERT INTO user_level (id, level, name_bn, name_en)
VALUES (16, 2, 'জব সহকারী', 'Job Assistant');


INSERT INTO designation (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn,
                                        name_en, parent_id, user_level_id)
VALUES (14, '2021-09-02 15:39:04.000000', 1, false, '2021-09-02 15:39:04.000000', 1, 'জব সহকারী', 'Job Assistant', null,
        16);
