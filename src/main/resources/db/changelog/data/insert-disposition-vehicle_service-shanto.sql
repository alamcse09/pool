INSERT INTO state (id, description, name, name_en, state_type)
VALUES (106, 'Vehicle Disposed', 'মোটরযান অকেজোকারণ করা হয়েছে', 'Vehicle Disposed', 3);

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1218, 'ROLE_VEHICLE_SERVICE_DISPOSITION', 'যানবাহন সার্ভিসিং', 'Vehicle Service', 'যানবাহন সার্ভিসিং অকেজো করা',
        'Vehicle Service Disposition');
		

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (143, 1218, false, 78, 106, 98);
		
INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1218);
		
		