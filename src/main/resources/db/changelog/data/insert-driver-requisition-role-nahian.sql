INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (4006, 'ROLE_DRIVER_REQUISITION', 'চালক ব্যবস্থাপনা ', 'Driver Management', 'চালক অনুরোধ ','Driver Requisition'),
       (4007, 'ROLE_DRIVER_REQUISITION_SEARCH', 'চালক ব্যবস্থাপনা ', 'Driver Management', 'চালক অনুরোধ অনুসন্ধান','Driver Requisition');

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (1, 4006),
       (1, 4007);
