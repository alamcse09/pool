INSERT INTO state (id, description, name, name_en, state_type)
VALUES (153, 'Send to Commissioner and Director', 'কমিশনার ও ডিরেক্টর এর কাছে পাঠান হয়েছে',
        'Send to Commissioner and Director', 0);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (154, 'Forwarded to AD for Driver Requisition', 'এডি র কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded To AD', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (155, 'Forwarded To DD for Driver Requisition', 'ডিডি র কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded To DD', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (156, 'Forwarded To Director for Driver Requisition', 'ডিরেক্টর এর কাছে ফরোয়ার্ড করা হয়েছে',
        'Forwarded To Director', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (157, 'Forwarded To Commissioner for Driver Requisition', 'কমিশনার এর কাছে ফরোয়ার্ড করা হয়েছে',
        'Forwarded To Commissioner', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (158, 'Rejected for Driver Requistion', 'বাতিল', 'Rejected', 2);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (159, 'Forwarded to Upper Division Officer', 'আপার ডিভিশন অফিসারের কাছে ফরোয়ার্ড করা হয়েছে',
        'Forwarded to Upper Division Officer', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (160, 'Forwareded to AD to choose Driver', 'এডি র কাছে ড্রাইভার নির্ধারণের জন্য ফরোয়ার্ড করা হয়েছে',
        'Forwareded to AD to choose Vehicle', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (161, 'Approved for Driver Requisition', 'অনুমোদিত', 'Approved', 1);


INSERT INTO action (id, name, name_en)
VALUES (147, 'এডি র কাছে ফরোয়ার্ড করুন', 'Forward To AD');

INSERT INTO action (id, name, name_en)
VALUES (148, 'ডিডি র কাছে ফরোয়ার্ড করুন', 'Forward To DD');

INSERT INTO action (id, name, name_en)
VALUES (149, 'ডিরেক্টর এর কাছে ফরোয়ার্ড করুন', 'Forward To Director');

INSERT INTO action (id, name, name_en)
VALUES (150, 'কমিশনার এর কাছে ফরোয়ার্ড করুন', 'Forward To Commissioner');

INSERT INTO action (id, name, name_en)
VALUES (151, 'বাতিল করা হল', 'Reject');

INSERT INTO action (id, name, name_en)
VALUES (152, 'আপার ডিভিশন অফিসারের কাছে ফরোয়ার্ড করুন', 'Forward to Upper Division Officer');

INSERT INTO action (id, name, name_en)
VALUES (153, 'অনুমোদন করা হল', 'Approve');




INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1263, 'ROLE_DRIVER_REQUISITION_FORWARD_TO_AD', 'চালক ব্যবস্থাপনা ', 'Driver Management', 'চালক বরাদ্দ এডি',
        'Driver Requisition Forward to AD');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1264, 'ROLE_DRIVER_REQUISITION_FORWARD_TO_DD', 'চালক ব্যবস্থাপনা ', 'Driver Management', 'চালক বরাদ্দ ডিডি ',
        'Driver Requisition Forward to DD');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1265, 'ROLE_DRIVER_REQUISITION_FORWARD_TO_DIRECTOR', 'চালক ব্যবস্থাপনা ', 'Driver Management',
        'চালক বরাদ্দ ডাইরেক্টর', 'Driver Requisition Forward to Director');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1266, 'ROLE_DRIVER_REQUISITION_FORWARD_TO_COMMISSIONER', 'চালক ব্যবস্থাপনা ', 'Driver Management',
        'চালক বরাদ্দ কমিশনার', 'Driver Requisition Forward to Commissioner');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1267, 'ROLE_DRIVER_REQUISITION_REJECT', 'চালক ব্যবস্থাপনা ', 'Driver Management', 'চালক বরাদ্দ বাতিল',
        'Driver Requisition Reject');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1268, 'ROLE_DRIVER_REQUISITION_APPROVE', 'চালক ব্যবস্থাপনা ', 'Driver Management', 'চালক বরাদ্দ অনুমোদন',
        'Driver Requisition Approve');
		

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1269, 'ROLE_VEHICLE_REQUISITION_FORWARD_TO_UDO', 'চালক ব্যবস্থাপনা ', 'Driver Management',
        'চালক বরাদ্দ আপার ডিভিশন অফিসার', 'Driver Requisition Forward to UDO');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1270, 'ROLE_VEHICLE_REQUISITION_FORWARD_TO_AD_BY_UDO_OA', 'চালক ব্যবস্থাপনা ', 'Driver Management',
        'চালক বরাদ্দ এডি', 'Driver Requisition Forward AD');		
		
		
		
INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1263);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1264);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1265);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1266);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1267);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1268);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1269);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1270);


INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (210, 1264, false, 148, 155, 160);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (211, 1263, false, 147, 154, 153);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (212, 1269, false, 152, 159, 154);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (213, 1266, false, 150, 157, 155);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (214, 1267, false, 151, 158, 155);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (215, 1266, false, 150, 157, 156);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (216, 1267, false, 151, 158, 156);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (217, 1267, false, 151, 158, 157);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (218, 1268, false, 153, 161, 157);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (219, 1263, false, 147, 160, 159);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (220, 4009, true, 7, 161, 161);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (221, 4009, true, 7, 158, 158);

