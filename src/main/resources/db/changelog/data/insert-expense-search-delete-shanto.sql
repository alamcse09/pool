INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1194, 'ROLE_TRAINING_EXPENSE_SEARCH', 'প্রশিক্ষণ', 'Training', 'প্রশিক্ষণের ব্যয় অনুসন্ধান',
        'Training Expense Search');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1195, 'ROLE_TRAINING_EXPENSE_DELETE', 'প্রশিক্ষণ', 'Training', 'প্রশিক্ষণের ব্যয় মুছে ফেলা',
        'Training Expense Delete');


INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1194);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1195);

