INSERT INTO state (id, description, name, name_en, state_type)
VALUES (107, 'Vehicle Sent For Servicing', 'সার্ভিসিং এর জন্য যানবাহন পাঠানো হয়েছে', 'Vehicle Sent For Servicing', 1);

UPDATE state_action_map t
SET t.role_id       = 1121,
    t.next_state_id = 107
WHERE t.id = 124;

INSERT INTO action (id, name, name_en)
VALUES (92, 'সার্ভিসিং এর জন্য পাঠানো হোক ', 'Sent for Servicing');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1219, 'ROLE_INCIDENT_SENT_FOR_SERVICE_INIT_COMMI', 'ইনসিডেন্ট', 'Incident',
        'প্রাথমিক অবস্থায় কমিশনার থেকে সার্ভিসিং এর জন্য পাঠানো', 'Sent to Servicing From Initial Commissioner State');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1220, 'ROLE_INCIDENT_SENT_FOR_SERVICE_FROM_REPORT', 'ইনসিডেন্ট', 'Incident',
        'রিপোর্ট জমা দেয়ার পর সার্ভিসিং এর জন্য পাঠানো', 'Sent to Servicing After Report Submission');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1221, 'ROLE_INCIDENT_SENT_FOR_SERVICE_FROM_FINE', 'ইনসিডেন্ট', 'Incident',
        'জরিমানা সংগ্রহ করার পর সার্ভিসিং এর জন্য পাঠানো', 'Sent to Servicing After Collecting Fine');
		

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (144, 1219, false, 92, 107, 60);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (145, 1220, false, 92, 107, 82);


INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1219);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1220);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1221);


