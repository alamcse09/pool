INSERT INTO state (id, description, name, name_en, state_type)
VALUES (90, 'Applied For Marine Disposition', 'এপ্লাইড', 'Applied', 0);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (91, 'Marine Disposition Committee Create', 'কমিটি তৈরি করা হয়েছে', 'Committee Created', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (92, 'Marine Disposition Commitee Report Submitted', 'রিপোর্ট দেয়া হয়েছে', 'Report Submitted', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (93, 'Marine Disposition Approved', 'এপ্রুভড', 'Approved', 1);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (94, 'Marine Disposition Rejected', 'রিজেক্টেড', 'Rejected', 2);


INSERT INTO action (id, name, name_en)
VALUES (79, 'কমিটি তৈরি করুন', 'Create Committee');

INSERT INTO action (id, name, name_en)
VALUES (80, 'রিপোর্ট জমা দিন', 'Submit Report');

INSERT INTO action (id, name, name_en)
VALUES (81, 'অনুমোদন', 'Approve');

INSERT INTO action (id, name, name_en)
VALUES (82, 'বাতিল', 'Reject');


INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1198, 'ROLE_MARINE_DISPOSITION_SEARCH', 'মেরিন অকেজোকরণ ', 'Marine Disposition', 'খুজুন', 'Disposition Search');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1199, 'ROLE_MARINE_DISPOSITION_CREATE_COMMITTEE', 'মেরিন অকেজোকরণ ', 'Marine Disposition', 'কমিটি তৈরি করন',
        'Create Committee');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1200, 'ROLE_MARINE_DISPOSITION_SUBMIT_REPORT', 'মেরিন অকেজোকরণ ', 'Marine Disposition', 'কমিটির রিপোর্ট জমাদান',
        'Submit Committee Report');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1201, 'ROLE_MARINE_DISPOSITION_APPROVE', 'মেরিন অকেজোকরণ ', 'Marine Disposition', 'অকেজোকরণ আবেদন অনুমোদন',
        'Approve');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1202, 'ROLE_MARINE_DISPOSITION_REJECT', 'মেরিন অকেজোকরণ ', 'Marine Disposition', 'অকেজোকরণ আবেদন বাতিল ',
        'Reject');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1203, 'ROLE_MARINE_DISPOSITION_VIEW', 'মেরিন অকেজোকরণ ', 'Marine Disposition', 'ভিউ ', 'View');


INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1198);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1199);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1200);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1201);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1202);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1203);


INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (129, 1199, false, 79, 91, 90);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (130, 1200, false, 80, 92, 91);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (131, 1202, false, 81, 93, 92);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (132, 1203, false, 82, 94, 92);

