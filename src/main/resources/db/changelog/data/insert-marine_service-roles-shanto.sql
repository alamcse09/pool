INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1204, 'ROLE_MARINE_SERVICE_APPLICATION_SEARCH', 'মেরিন', 'Marine', 'মেরিন সার্ভিসিং খুজুন',
        'Search Marine Service');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1205, 'ROLE_MARINE_SERVICE_APPLICATION_EDIT', 'মেরিন', 'Marine', 'মেরিন সার্ভিসিং সম্পাদনা',
        'Edit Marine Service');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1206, 'ROLE_MARINE_SERVICE_APPLICATION_DELETE', 'মেরিন', 'Marine', 'মেরিন সার্ভিসিং মুছে ফেলা',
        'Delete Marine Service');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1207, 'ROLE_MARINE_SERVICE_APPLICATION_VIEW', 'মেরিন', 'Marine', 'মেরিন সার্ভিসিং দেখুন',
        'View Marine Service');
		
		
		
		
		INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1204);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1205);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1206);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1207);

