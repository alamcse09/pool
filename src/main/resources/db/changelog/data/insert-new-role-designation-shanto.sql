INSERT INTO user_level (id, level, name_bn, name_en)
VALUES (14, 2, 'সেকশন লিডার ', 'Section Leader');


INSERT INTO designation (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn,
                                        name_en, parent_id, user_level_id)
VALUES (12, '2021-09-02 15:39:04.000000', 1, false, '2021-09-02 15:39:04.000000', 1, 'সেকশন লিডার ', 'Section Leader',
        null, 14);

