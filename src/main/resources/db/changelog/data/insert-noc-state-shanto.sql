INSERT INTO state (id, description, name, name_en, state_type)
VALUES (115, 'Sent To Commissioner', 'কমিশনারের কাছে পাঠানো হয়েছে', 'Sent To Commissioner', 0);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (116, 'Sent To Director', 'পরিচালকের কাছে পাঠানো হয়েছে', 'Sent To Director', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (117, 'Sent To AD For Investigation', 'এডি এর কাছে পাঠানো হয়েছে তদন্তের জন্য', 'Sent To AD For Investigation', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (118, 'Sent To DD For Investigation', 'ডিডি এর কাছে পাঠানো হয়েছে তদন্তের জন্য', 'Sent To DD For Investigation', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (119, 'Sent To UD For Verification', 'উডি এর কাছে পাঠানো হয়েছে ভেরিফাই এর জন্য', 'Sent To UD For Verification', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (120, 'Sent To OA For Verification', 'ওএ এর কাছে পাঠানো হয়েছে ভেরিফাই এর জন্য', 'Sent To OA For Verification', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (121, 'Sent To Mechanic Incharge', 'মেকানিক ইনচার্জকে পাঠানো হয়েছে', 'Sent To Mechanic Incharge', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (122, 'Sent To ATO', 'এটিও এর কাছে পাঠানো হয়েছে', 'Sent To ATO', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (123, 'Sent To UD', 'উডি এর কাছে পাঠানো হয়েছে', 'Sent To UD', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (124, 'Sent To AD', 'এডি এর কাছে পাঠানো', 'Sent To AD', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (125, 'Sent To DD', 'ডিডি এর  কাছেপাঠানো', 'Sent To DD', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (126, 'Sent To Director', 'পরিচালকের কাছে পাঠানো হয়েছে', 'Sent To Director', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (127, 'Noc Approved', 'অনুমোদিত', 'Noc Approved', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (128, 'Noc Rejected', 'রিজেক্টেড', 'Noc Rejected', 2);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (129, 'Sent To AD For Uploading NOC', 'এডি এর কাছে পাঠানো হয়েছে এনওসি জারি আপলোড করার জন্য ',
        'Sent To AD For Uploading NOC', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (130, 'NOC Issue Uploaded', 'এনওসি জারি আপলোড করা হয়েছে', 'NOC Issue Uploaded', 1);




INSERT INTO action (id, name, name_en)
VALUES (100, 'পরিচালকের কাছে ফরোয়ার্ড করুন ', 'Forward to Director');

INSERT INTO action (id, name, name_en)
VALUES (101, 'এডি এর কাছে ফরোয়ার্ড করুন তদন্তের জন্য', 'Forward to AD For Investigation');

INSERT INTO action (id, name, name_en)
VALUES (102, 'ডিডি এর কাছে ফরোয়ার্ড করুন তদন্তের জন্য', 'Forward to DD For Investigation');

INSERT INTO action (id, name, name_en)
VALUES (103, 'উডি এর কাছে ফরোয়ার্ড করুন ভেরিফাই এর জন্য', 'Forward to UD For Verification');

INSERT INTO action (id, name, name_en)
VALUES (104, 'ওএ এর কাছে ফরোয়ার্ড করুন ভেরিফাই এর জন্য', 'Forward to OA For Verification');

INSERT INTO action (id, name, name_en)
VALUES (105, 'মেকানিক ইনচার্জ এর কাছে ফরোয়ার্ড করুন', 'Forward to Mechanic Incharge');

INSERT INTO action (id, name, name_en)
VALUES (106, 'বাদ পড়া যানবাহন এড করুন', 'Add Missing Assigned Vehicle');

INSERT INTO action (id, name, name_en)
VALUES (107, 'এটিও এর কাছে ফরোয়ার্ড করুন', 'Forward to ATO');

INSERT INTO action (id, name, name_en)
VALUES (108, 'উডি এর কাছে ফরোয়ার্ড করুন', 'Forward to UD');

INSERT INTO action (id, name, name_en)
VALUES (109, 'এডি এর কাছে ফরোয়ার্ড করুন', 'Forward to AD');

INSERT INTO action (id, name, name_en)
VALUES (110, 'ডিডি এর কাছে ফরোয়ার্ড করুন', 'Forward to DD');

INSERT INTO action (id, name, name_en)
VALUES (111, 'পরিচালকের কাছে ফরোয়ার্ড করুন ', 'Forward to Director');

INSERT INTO action (id, name, name_en)
VALUES (112, 'অনুমোদন', 'Approve');

INSERT INTO action (id, name, name_en)
VALUES (113, 'বাতিল', 'Reject');

INSERT INTO action (id, name, name_en)
VALUES (114, 'এডি এর কাছে ফরোয়ার্ড করুন এনওসি জারি আপলোড এর জন্য', 'Sent to AD to upload Noc Issue');

INSERT INTO action (id, name, name_en)
VALUES (115, 'এনওসি জারি আপলোড করুন ', 'Upload Noc Issue');



INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1239, 'ROLE_NOC_SENT_TO_DIRECTOR', 'অনাপত্তি', 'Noc', 'পরিচালক এর কাছে ফরওয়ার্ড করা ', 'Forward to Director');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1240, 'ROLE_NOC_SENT_TO_AD_FOR_INVESTIGATION', 'অনাপত্তি', 'Noc', 'এডি এর কাছে ফরওয়ার্ড করা তদন্তের জন্য',
        'Forward to AD For Investigation');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1241, 'ROLE_NOC_SENT_TO_DD_FOR_INVESTIGATION', 'অনাপত্তি', 'Noc', 'ডিডি এর কাছে ফরওয়ার্ড করা তদন্তের জন্য',
        'Forward to DD For Investigation');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1242, 'ROLE_NOC_SENT_TO_UD_FOR_VERIFICATION', 'অনাপত্তি', 'Noc', 'উডি এর কাছে ফরোয়ার্ড করা ভেরিফাই এর জন্য',
        'Forward to UD For Verification');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1243, 'ROLE_NOC_SENT_TO_OA_FOR_VERIFICATION', 'অনাপত্তি', 'Noc', 'ওএ এর কাছে ফরোয়ার্ড করা ভেরিফাই এর জন্য',
        'Forward to OA For Verification');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1244, 'ROLE_NOC_SENT_TO_MECHANIC_INCHARGE', 'অনাপত্তি', 'Noc', 'মেকানিক ইনচার্জ এর কাছে ফরোয়ার্ড করা',
        'Forward to Mechanic Incharge');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1245, 'ROLE_NOC_ADD_MISSING_VEHICLE', 'অনাপত্তি', 'Noc', 'বাদ পড়া যানবাহন এড করা',
        'Add Missing Assigned Vehicle');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1246, 'ROLE_NOC_SENT_TO_ATO', 'অনাপত্তি', 'Noc', 'এটিও এর কাছে ফরোয়ার্ড করা', 'Forward to ATO');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1247, 'ROLE_NOC_SENT_TO_UD', 'অনাপত্তি', 'Noc', 'উডি এর কাছে ফরোয়ার্ড করা', 'Forward to UD');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1248, 'ROLE_NOC_SENT_TO_AD', 'অনাপত্তি', 'Noc', 'এডি এর কাছে ফরোয়ার্ড করা', 'Forward to AD');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1249, 'ROLE_NOC_SENT_TO_DD', 'অনাপত্তি', 'Noc', 'ডিডি এর কাছে ফরোয়ার্ড করা', 'Forward to DD');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1250, 'ROLE_NOC_SENT_TO_DIRECTOR', 'অনাপত্তি', 'Noc', 'পরিচালকের কাছে ফরোয়ার্ড করা', 'Forward to Director');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1251, 'ROLE_NOC_APPROVE', 'অনাপত্তি', 'Noc', 'অনুমোদন করা', 'Approve');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1252, 'ROLE_NOC_REJECT', 'অনাপত্তি', 'Noc', 'বাতিল করা', 'Reject');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1253, 'ROLE_NOC_SENT_TO_AD_UPLOAD_ISSUE', 'অনাপত্তি', 'Noc', 'এডি এর কাছে ফরোয়ার্ড করা এনওসি জারি আপলোড জন্য',
        'Sent to AD to upload Noc Issue');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1254, 'ROLE_NOC_UPLOAD_NOC_ISSUE', 'অনাপত্তি', 'Noc', 'এনওসি জারি আপলোড করা', 'Upload Noc Issue');


INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (157, 1239, false, 100, 116, 115);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (158, 1240, false, 101, 117, 116);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (159, 1241, false, 102, 118, 116);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (160, 1242, false, 103, 119, 117);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (161, 1243, false, 104, 120, 117);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (162, 1242, false, 103, 119, 118);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (163, 1243, false, 104, 120, 118);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (164, 1244, false, 105, 121, 119);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (165, 1244, false, 105, 121, 120);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (166, 1246, false, 107, 122, 121);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (167, 1247, false, 108, 123, 121);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (168, 1248, false, 109, 124, 122);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (169, 1248, false, 109, 124, 123);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (170, 1245, false, 106, 121, 121);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (171, 1249, false, 110, 125, 124);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (172, 1250, false, 111, 126, 125);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (173, 1251, false, 112, 127, 126);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (174, 1253, false, 114, 129, 127);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (175, 1254, false, 115, 130, 129);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (176, 1252, false, 113, 128, 126);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1239);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1240);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1241);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1242);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1243);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1244);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1245);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1246);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1247);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1248);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1249);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1250);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1251);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1252);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1253);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1254);


