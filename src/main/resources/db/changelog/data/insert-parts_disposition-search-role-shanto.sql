INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1228, 'ROLE_PARTS_DISPOSITION_SEARCH', 'স্টোর ব্যবস্থাপনা ', 'Store Management', 'যন্ত্রাংশ অকেজোকরণ খোঁজা',
        'Search Parts Disposition');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1229, 'ROLE_PARTS_DISPOSITION_VIEW', 'স্টোর ব্যবস্থাপনা ', 'Store Management', 'যন্ত্রাংশ অকেজোকরণ ভিউ',
        'View Parts Disposition');

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1228);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1229);

