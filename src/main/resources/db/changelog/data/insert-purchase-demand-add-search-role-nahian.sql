INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (4001, 'ROLE_PURCHASE_DEMAND_ADD', 'ক্রয় সংগ্রহ ', 'Purchase Demand', 'ক্রয় সংগ্রহ সংযুক্ত করা ','Purchase Demand Add'),
       (4002, 'ROLE_PURCHASE_DEMAND_SEARCH', 'ক্রয় সংগ্রহ ', 'Purchase Demand', 'ক্রয় সংগ্রহ খোঁজা  ','Purchase Demand Search');


INSERT INTO user_level_role (user_level_id, role_id)
VALUES (1, 4001),
       (1, 4002);
