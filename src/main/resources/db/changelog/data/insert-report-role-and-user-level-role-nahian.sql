INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (4005, 'ROLE_INVENTORY_REPORT', 'স্টোর ব্যবস্থাপনা ', 'Store Management', 'প্রতিবেদন ', 'Report');

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (1, 4005);
