INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1146, 'ROLE_BUDGET_SEARCH', 'বাজেট', 'Budget', 'বাজেট অনুসন্ধান', 'Budget Search');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1147, 'ROLE_BUDGET_DELETE', 'বাজেট', 'Budget', 'বাজেট মুছে ফেলা', 'Budget Delete');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1148, 'ROLE_BUDGET_VIEW', 'বাজেট', 'Budget', 'বাজেট ভিউ', 'Budget View');



INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1146);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1147);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1148);