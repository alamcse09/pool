INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (1129, 'ROLE_LEAVE_APPLICATION_APPROVAL_SEARCH', 'লিভ ম্যানেজমেন্ট', 'Leave Management', 'লিভ অনুমোদন ম্যানেজমেন্ট তালিকা', 'Search Leave Approval Management');

INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 1129);