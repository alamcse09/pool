INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (1139, 'ROLE_MEETING_SEARCH', 'মিটিং ', 'Meeting', 'মিটিং খুঁজুন', 'Meeting Search');
INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (1140, 'ROLE_MEETING_DELETE', 'মিটিং ', 'Meeting', 'মিটিং মুছে ফেলা', 'Meeting Delete');


INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 1139);
INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 1140);