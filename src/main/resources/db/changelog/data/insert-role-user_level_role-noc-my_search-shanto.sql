INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (1132, 'ROLE_NOC_MY_SEARCH', 'যানবাহন', 'Vehicle', 'আমার অনাপত্তি আবেদন খুঁজুন', 'Search My NOC');
INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (1133, 'ROLE_NOC_DELETE', 'যানবাহন', 'Vehicle', 'অনাপত্তি মুছে ফেলা', 'Delete NOC');



INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 1132);
INSERT INTO user_level_role (user_level_id, role_id) VALUES (10, 1132);
INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 1133);
INSERT INTO user_level_role (user_level_id, role_id) VALUES (10, 1133);