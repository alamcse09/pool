INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1155, 'ROLE_COMPLAIN_SEARCH', 'অভিযোগ', 'Complain', 'অভিযোগ খুঁজুন', 'Complain Search');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1156, 'ROLE_COMPLAIN_DELETE', 'অভিযোগ', 'Complain', 'অভিযোগ মুছে ফেলা', 'Complain Delete');


INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1155);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1156);



