insert into state (id, description, name, name_en, state_type) values (27, 'Applied for Requisition', 'এপ্লাইড', 'Applied', 0);
insert into state (id, description, name, name_en, state_type) values (28, 'Forwarded to AD for Requisition', 'এডি র কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded To AD', 3);
insert into state (id, description, name, name_en, state_type) values (29, 'Forwarded To DD for Requisition', 'ডিডি র কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded To DD', 3);
insert into state (id, description, name, name_en, state_type) values (30, 'Forwarded To Director for Requisition', 'ডিরেক্টর এর কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded To Director', 3);
insert into state (id, description, name, name_en, state_type) values (31, 'Forwarded To Commissioner for Requisition', 'কমিশনার এর কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded To Commissioner', 3);
insert into state (id, description, name, name_en, state_type) values (32, 'Approved for Requisition', 'অনুমোদিত', 'Approved', 1);
insert into state (id, description, name, name_en, state_type) values (33, 'Rejected for Requistion', 'বাতিল', 'Rejected', 2);

insert into action (id, name, name_en) values (24, 'এডি র কাছে ফরোয়ার্ড করুন', 'Forward To AD');
insert into action (id, name, name_en) values (25, 'ডিডি র কাছে ফরোয়ার্ড করুন', 'Forward To DD');
insert into action (id, name, name_en) values (26, 'ডিরেক্টর এর কাছে ফরোয়ার্ড করুন', 'Forward To Director');
insert into action (id, name, name_en) values (27, 'কমিশনার এর কাছে ফরোয়ার্ড করুন', 'Forward To Commissioner');
insert into action (id, name, name_en) values (28, 'বাতিল করুন', 'Reject');
insert into action (id, name, name_en) values (29, 'অনুমোদন দিন', 'Approve');
insert into action (id, name, name_en) values (30, 'ভিউ', 'View');


insert into role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) values (2100, 'ROLE_VEHICLE_REQUISITION_FORWARD_TO_AD', 'যানবাহন বরাদ্দ', 'Vehicle Requisition', 'যানবাহন বরাদ্দ এডি', 'Vehicle Requisition Forward to AD');
insert into role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) values (2101, 'ROLE_VEHICLE_REQUISITION_FORWARD_TO_DD', 'যানবাহন বরাদ্দ', 'Vehicle Requisition', 'যানবাহন বরাদ্দ ডিডি ', 'Vehicle Requisition Forward to DD');
insert into role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) values (2102, 'ROLE_VEHICLE_REQUISITION_FORWARD_TO_DIRECTOR', 'যানবাহন বরাদ্দ', 'Vehicle Requisition', 'যানবাহন বরাদ্দ ডাইরেক্টর', 'Vehicle Requisition Forward to Director');
insert into role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) values (2103, 'ROLE_VEHICLE_REQUISITION_FORWARD_TO_COMMISSIONER', 'যানবাহন বরাদ্দ', 'Vehicle Requisition', 'যানবাহন বরাদ্দ কমিশনার', 'Vehicle Requisition Forward to Commissioner');
insert into role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) values (2104, 'ROLE_VEHICLE_REQUISITION_REJECT', 'যানবাহন বরাদ্দ', 'Vehicle Requisition', 'যানবাহন বরাদ্দ বাতিল', 'Vehicle Requisition Reject');
insert into role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) values (2105, 'ROLE_VEHICLE_REQUISITION_APPROVE', 'যানবাহন বরাদ্দ', 'Vehicle Requisition', 'যানবাহন বরাদ্দ অনুমোদন', 'Vehicle Requisition Approve');
insert into role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) values (2106, 'ROLE_VEHICLE_REQUISITION_VIEW', 'যানবাহন বরাদ্দ', 'Vehicle Requisition', 'যানবাহন বরাদ্দ ভিউ', 'Vehicle Requisition View');

insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (55, 2100, false, 24, 28, 27);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (56, 2101, false, 25, 29, 27);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (57, 2102, false, 26, 30, 27);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (58, 2103, false, 27, 31, 27);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (59, 2101, false, 25, 29, 28);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (60, 2102, false, 26, 30, 28);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (61, 2103, false, 27, 31, 28);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (62, 2104, false, 28, 33, 28);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (63, 2106, true, 7, 28, 28);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (64, 2102, false, 26, 30, 29);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (65, 2103, false, 27, 31, 29);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (66, 2104, false, 28, 33, 29);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (67, 2106, true, 7, 29, 29);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (68, 2103, false, 27, 31, 30);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (69, 2104, false, 28, 33, 30);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (70, 2106, true, 7, 30, 30);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (71, 2105, false, 29, 32, 31);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (72, 2104, false, 28, 33, 31);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (73, 2106, true, 7, 31, 31);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (74, 2104, false, 28, 33, 27);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (91, 2106, true, 7, 27, 27);



insert into user_level_role (user_level_id, role_id) values (2, 2100);
insert into user_level_role (user_level_id, role_id) values (11, 2100);
insert into user_level_role (user_level_id, role_id) values (2, 2101);
insert into user_level_role (user_level_id, role_id) values (5, 2101);
insert into user_level_role (user_level_id, role_id) values (2, 2102);
insert into user_level_role (user_level_id, role_id) values (5, 2102);
insert into user_level_role (user_level_id, role_id) values (6, 2102);
insert into user_level_role (user_level_id, role_id) values (2, 2103);
insert into user_level_role (user_level_id, role_id) values (5, 2103);
insert into user_level_role (user_level_id, role_id) values (6, 2103);
insert into user_level_role (user_level_id, role_id) values (7, 2103);
insert into user_level_role (user_level_id, role_id) values (2, 2104);
insert into user_level_role (user_level_id, role_id) values (8, 2104);
insert into user_level_role (user_level_id, role_id) values (2, 2105);
insert into user_level_role (user_level_id, role_id) values (8, 2105);
insert into user_level_role (user_level_id, role_id) values (2, 2106);
insert into user_level_role (user_level_id, role_id) values (5, 2106);
insert into user_level_role (user_level_id, role_id) values (6, 2106);
insert into user_level_role (user_level_id, role_id) values (7, 2106);
insert into user_level_role (user_level_id, role_id) values (8, 2106);
insert into user_level_role (user_level_id, role_id) values (11, 2106);
