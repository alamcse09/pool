INSERT INTO state (id, description, name, name_en, state_type)
VALUES (95, 'Sent For Parts Disposition', 'যন্ত্রাংশ অকেজোকরণ এর জন্য পাঠানো হয়েছে ', 'Sent For Parts Disposition', 0);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (111, 'Parts Disposited', 'যন্ত্রাংশ অকেজোকরণ করা হয়েছে ', 'Parts Have Been Disposited', 1);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (112, 'Parts Not Disposited', 'যন্ত্রাংশ অকেজোকরণ করা হয়নি ', 'Parts Not Disposited', 2);

INSERT INTO action (id, name, name_en)
VALUES (95, 'যন্ত্রাংশ অকেজোকরণ করুন', 'Approve Parts Dispose');

INSERT INTO action (id, name, name_en)
VALUES (96, 'যন্ত্রাংশ অকেজোকরণ বাতিল করুন', 'Do not Dispose Parts');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1226, 'ROLE_APPROVE_DISPOSE_PARTS', 'স্টোর ব্যবস্থাপনা ', 'Store Management', 'যন্ত্রাংশ অকেজোকরণ করা',
        'Parts Disposition');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1227, 'ROLE_REJECT_DISPOSE_PARTS', 'স্টোর ব্যবস্থাপনা ', 'Store Management', 'যন্ত্রাংশ অকেজোকরণ বাতিল করা',
        'Reject Parts Disposition');
		
		
INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (150, 1226, false, 95, 111, 95);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (151, 1227, false, 96, 112, 95);


INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1226);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1227);

