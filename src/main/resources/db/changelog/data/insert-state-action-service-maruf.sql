INSERT INTO state (id, description, name, name_en, state_type) VALUES (41, 'Bill Approved', 'বিল অনুমোদিত', 'Bill Approved', 1);

INSERT INTO state (id, description, name, name_en, state_type) VALUES (42, 'Waiting for Approval of MOPA', 'মপা থেকে অনুমোদন পাওয়ার অপেক্ষায়', 'Waiting for Approval of MOPA', 3);

INSERT INTO state (id, description, name, name_en, state_type) VALUES (43, 'Waiting for Approval of FM', 'অর্থ মন্ত্রানলায় থেকে অনুমোদনের অপেক্ষায়', 'Waiting for Approval of FM', 3);


INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (2501, 'ROLE_BILL_OF_JOB_APPROVE', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (2502, 'ROLE_GET_APPROVAL_FROM_MOPA', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (2503, 'ROLE_GET_APPROVAL_FROM_FM', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing');


INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 2501);

INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 2502);

INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 2503);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) VALUES (81, 2501, true, 33, 41, 37);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) VALUES (82, 2502, true, 34, 42, 37);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) VALUES (83, 2503, true, 35, 43, 37);



