INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (4003, 'ROLE_SUPPLY_ORDER_ADD', 'সরবরাহ আদেশ ', 'Supply Order', 'সরবরাহ আদেশ সংযুক্ত করা ','Supply Order Add'),
       (4004, 'ROLE_SUPPLY_ORDER_SEARCH', 'সরবরাহ আদেশ ', 'Supply Order', 'সরবরাহ আদেশ খোঁজা  ','Supply Order Search');

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (1, 4003),
       (1, 4004);
