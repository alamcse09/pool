INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1193, 'ROLE_TRAINING_EXPENSE_MANAGEMENT', 'প্রশিক্ষণ', 'Training', 'প্রশিক্ষণের সম্ভাব্য ব্যয়',
        'Training Potential Expense');


INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1193);

