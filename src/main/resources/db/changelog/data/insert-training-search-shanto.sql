INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1181, 'ROLE_TRAINING_SEARCH', 'প্রশিক্ষণ', 'Training', 'প্রশিক্ষণ খুঁজুন', 'Training Search');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1182, 'ROLE_TRAINING_DELETE', 'প্রশিক্ষণ', 'Training', 'প্রশিক্ষণ মুছে ফেলা', 'Training Delete');



INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1181);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1182);

