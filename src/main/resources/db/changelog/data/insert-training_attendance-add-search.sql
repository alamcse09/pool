INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1183, 'ROLE_TRAINING_ATTENDANCE_SEARCH', 'প্রশিক্ষণ', 'Training', 'প্রশিক্ষণ উপস্থিতি অনুসন্ধান',
        'Training Attendance Search');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1184, 'ROLE_TRAINING_ATTENDANCE_ADD', 'প্রশিক্ষণ', 'Training', 'প্রশিক্ষণ উপস্থিতি সংযুক্তকরণ',
        'Training Attendance Add');

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1183);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1184);