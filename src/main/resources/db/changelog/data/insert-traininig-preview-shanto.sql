INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1196, 'ROLE_TRAINING_REPORT', 'প্রশিক্ষণ', 'Training', 'প্রশিক্ষণ রিপোর্ট ', 'Training Report');

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1196);

