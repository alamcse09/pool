UPDATE action t
SET t.name = 'এডি এর কাছে ফরওয়ার্ড করুন '
WHERE t.id = 87;

UPDATE role t
SET t.role_text_bn = 'এডি এর কাছে ফরওয়ার্ড করা এটিও থেকে '
WHERE t.id = 1213;

UPDATE role t
SET t.role_text_bn = 'এডি এর কাছে ফরওয়ার্ড করা ফোরম্যান থেকে '
WHERE t.id = 1212;

UPDATE action t
SET t.name = 'সেকশন লিডার নিযুক্ত করুন'
WHERE t.id = 91;

UPDATE state_action_map t
SET t.next_state_id = 105
WHERE t.id = 134;



INSERT INTO employee (id, created_at, created_by, is_deleted, modified_at, modified_by, achievements, actual_salary_of_join, additional_increment_number_of_join, appointment_date, appointment_order, attendance_and_leave_info, blood_group, comments, current_actual_salary, current_additional_increment_number, current_grade, current_initial_grade_salary, current_step, date_of_birth, email, employee_doc_group_id, employee_id, father_name, gender, initial_grade_salary_of_join, job_type, joining_date, joining_designation, joining_grade, marital_status, mobile_number, mother_name, name, name_en, nid, number_of_children, office_ministry, passport_number, password, password_again, permanent_address, permanent_date, personal_id, present_address, promotion_info, quota, reporting_manager, retirement_date, step_of_join, transfer_info, designation_id, current_geolocation_id, permanent_geolocation_id, is_permanent) VALUES (21, '2021-12-29 09:01:03.000000', 1, false, '2021-12-29 09:01:03.000000', 1, '', 5, 23, '2021-12-04', 'ম্প্রদায়কে সহযোগিতা করার', 'ম্প্রদায়কে সহযোগিতা করার', 0, 'কেনটাকিতে অসময়ের টর্নেডোয়', 6, 6, 0, 6, 'cxzc', '1997-04-23', 'sl2@gmail.com', 1602, '11547', 'Jahangir Alam', 0, 5, 0, '2021-11-30', 'ম্প্রদায়কে সহযোগিতা করার', 0, 0, '01886414784', 'Nusrat Nesa', 'মোস্তফা হোসেন', 'Mustafa Hossain', 'A34554', 5, 'ম্প্রদায়কে সহযোগিতা করার', 'A34554', null, null, 'বাড্ডা', '2021-12-01', '5547', 'বাড্ডা', 'ম্প্রদায়কে সহযোগিতা করার', null, 'ম্প্রদায়কে সহযোগিতা করার', '2021-12-11', 'cxzz', 'ম্প্রদায়কে সহযোগিতা করার', 12, 25, 25, 0);



INSERT INTO user (id, created_at, created_by, is_deleted, modified_at, modified_by, is_enabled, password, profile_pic_document_group_id, sign_document_group_id, user_type, username, employee_id, mechanic_id, public_user_info_id, user_level_id) VALUES (34, '2021-12-29 09:01:03.000000', 1, false, '2021-12-29 09:01:03.000000', 1, true, '$2a$10$.Qpb7Ku.mssUvdxgBK96pebLOhGQRcG0Lptxm58jVhtI/kjKmGTDy', null, null, 0, 'sl2@gmail.com', 21, null, null, 14);

INSERT INTO user_level (id, level, name_bn, name_en)
VALUES (15, 2, 'ওয়ার্কশপ ম্যানেজার', 'Workshop Manager');

INSERT INTO designation (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn,
                                        name_en, parent_id, user_level_id)
VALUES (13, '2021-09-02 15:39:04.000000', 1, false, '2021-09-02 15:39:04.000000', 1, 'ওয়ার্কশপ ম্যানেজার',
        'Workshop manager', null, 15);


INSERT INTO employee (id, created_at, created_by, is_deleted, modified_at, modified_by, achievements,
                                     actual_salary_of_join, additional_increment_number_of_join, appointment_date,
                                     appointment_order, attendance_and_leave_info, blood_group, comments,
                                     current_actual_salary, current_additional_increment_number, current_grade,
                                     current_initial_grade_salary, current_step, date_of_birth, email,
                                     employee_doc_group_id, employee_id, father_name, gender,
                                     initial_grade_salary_of_join, job_type, joining_date, joining_designation,
                                     joining_grade, marital_status, mobile_number, mother_name, name, name_en, nid,
                                     number_of_children, office_ministry, passport_number, password, password_again,
                                     permanent_address, permanent_date, personal_id, present_address, promotion_info,
                                     quota, reporting_manager, retirement_date, step_of_join, transfer_info,
                                     designation_id, current_geolocation_id, permanent_geolocation_id, is_permanent)
VALUES (22, '2021-12-28 12:03:24.000000', 1, false, '2021-12-28 12:03:24.000000', 1, '', 4, 4, '2021-12-01',
        'ম্প্রদায়কে সহযোগিতা করার', 'ম্প্রদায়কে সহযোগিতা করার', 0, 'কেনটাকিতে অসময়ের টর্নেডোয়', 5, 5, 12, 7, 'cxzc',
        '1997-04-23', 'chowdhary@gmail.com', 1502, '100', 'Mir Hasan', 0, 4, 0, '2021-12-05',
        'ম্প্রদায়কে সহযোগিতা করার', 0, null, '01895456541', 'Mita', 'জাহিদ রেজা', 'Jahid Reza', 'A34554', 4,
        'ম্প্রদায়কে সহযোগিতা করার', 'A34554', null, null, 'ঢাকাইয়া', '2021-11-29', '21414', 'সিমন লিউস',
        'ম্প্রদায়কে সহযোগিতা করার', null, 'ম্প্রদায়কে সহযোগিতা করার', '2021-11-30', 'cxzz', 'ম্প্রদায়কে সহযোগিতা করার',
        13, 19, 16, 0);


INSERT INTO user (id, created_at, created_by, is_deleted, modified_at, modified_by, is_enabled, password,
                                 profile_pic_document_group_id, sign_document_group_id, user_type, username,
                                 employee_id, mechanic_id, public_user_info_id, user_level_id)
VALUES (35, '2021-12-28 16:28:17.000000', 1, false, '2021-12-28 16:28:17.000000', 1, true,
        '$2a$10$7WgovYowFRO4GrKb6sFoW.6UFiKILFzp9N2hEpmqJfEbMOVaoaNbW', null, null, 0, 'wm2@gmail.com', 22, null, null,
        15);

