INSERT INTO state (id, description, name, name_en, state_type)
VALUES (146, 'Forwarded to the Stand-in Committee', 'স্ট্যান্ডিং কমিটি এর কাছে পাঠানো হয়েছে',
        'Forwarded to the Stand-in Committee', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (147, 'Forwarded to DA after uploading the verdict', 'রায় আপলোড করার পর ডিএ এর কাছে ফরোয়ার্ড করা হয়েছে',
        'Forwarded to DA after uploading the verdict', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (148, 'Forwarded to Commissioner without asserting fine', 'কমিশনারের কাছে পাঠানো হয়েছে',
        'Forwarded to Commissioner', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (149, 'Forwarded to Commissioner with asserting fine', 'কমিশনারের কাছে পাঠানো হয়েছে',
        'Forwarded to Commissioner', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (150, 'Forwarded to DA to collect fine', 'ডিএ এর কাছে পাঠানো হয়েছে', 'Forwarded to DA', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (151, 'Approved and Sent to DA', 'অনুমোদিত করা হয়েছে ও ডিএ এর কাছে পাঠানো হয়েছে', 'Approved and Sent to DA', 1);


INSERT INTO action (id, name, name_en)
VALUES (136, 'স্ট্যান্ডিং কমিটি এর কাছে ফরোয়ার্ড করুন', 'Forward to Stand-in Committee');

INSERT INTO action (id, name, name_en)
VALUES (137, 'চূড়ান্ত রায় রিপোর্ট আপলোড করুন', 'Upload final verdict report');

INSERT INTO action (id, name, name_en)
VALUES (138, 'স্ট্যান্ডিং কমিটি এর কাছে পুনরায় ফরোয়ার্ড করুন', 'Forward to Stand-in Committee Again');

INSERT INTO action (id, name, name_en)
VALUES (139, 'জরিমানা ধার্য করা ছাড়া কমিশনারের কাছে পাঠান', 'Send to the Commissioner without imposing a fine');

INSERT INTO action (id, name, name_en)
VALUES (140, 'জরিমানা ধার্য করে কমিশনারের কাছে পাঠান', 'Impose a fine and send it to the commissioner');

INSERT INTO action (id, name, name_en)
VALUES (141, 'পুনরায় তদন্তের জন্য ডিএ এর কাছে ফরওয়ার্ড করুন', 'Forward to DA for re-investigation');

INSERT INTO action (id, name, name_en)
VALUES (142, 'অনুমোদন করুন', 'Approve');

INSERT INTO action (id, name, name_en)
VALUES (143, 'অনুমোদন করুন ও জরিমানা আদায় করতে ডিএ ফরোয়ার্ড করুন', 'Approve it And Forward to DA to collect fine');

INSERT INTO action (id, name, name_en)
VALUES (144, 'জরিমানা সংগ্রহ করুন', 'Collect fine');

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (197, 1255, false, 136, 146, 145);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (198, 1255, false, 137, 147, 146);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (199, 1255, false, 138, 146, 147);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (200, 1255, false, 139, 148, 147);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (201, 1255, false, 140, 149, 147);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (202, 1261, false, 141, 133, 148);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (203, 1261, false, 142, 151, 148);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (204, 1261, false, 141, 143, 149);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (205, 1261, false, 143, 150, 149);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (206, 1255, false, 144, 151, 150);

