INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1255, 'ROLE_INCIDENT_DA_ACTION', 'ইনসিডেন্ট', 'Incident', 'ডিএ এর সকল অ্যাকশন  ', 'All actions of DA');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1256, 'ROLE_INCIDENT_UDO_ACTION', 'ইনসিডেন্ট', 'Incident', 'উডিও এর সকল অ্যাকশন', 'All actions of UDO');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1257, 'ROLE_INCIDENT_TO_ACTION', 'ইনসিডেন্ট', 'Incident', 'টিও এর সকল অ্যাকশন', 'All actions of TO');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1258, 'ROLE_INCIDENT_ADR_ACTION', 'ইনসিডেন্ট', 'Incident', 'এডি (রোড) এর সকল অ্যাকশন', 'All actions of Roads (AD)');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1259, 'ROLE_INCIDENT_DD_ACTION', 'ইনসিডেন্ট', 'Incident', 'ডিডি এর সকল অ্যাকশন', 'All actions of DD');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1260, 'ROLE_INCIDENT_DIR_ACTION', 'ইনসিডেন্ট', 'Incident', 'ডিরেক্টর এর সকল অ্যাকশন',
        'All actions of Director');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1261, 'ROLE_INCIDENT_COMMISSIONER_ACTION', 'ইনসিডেন্ট', 'Incident', 'কমিশনারের সকল অ্যাকশন',
        'All actions of Commissioner');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1262, 'ROLE_INCIDENT_COMMITTEE_ACTION', 'ইনসিডেন্ট', 'Incident', 'কমিটি সকল অ্যাকশন',
        'All actions of Committee');
		
INSERT INTO action (id, name, name_en)
VALUES (116, 'উডিও এর কাছে ফরোয়ার্ড করুন', 'Forward to UDO');

INSERT INTO action (id, name, name_en)
VALUES (117, 'ডিএ এর কাছে ফেরত করুন', 'Forward to DA');

INSERT INTO action (id, name, name_en)
VALUES (118, 'টিও এর কাছে ফরোয়ার্ড করুন', 'Forward to TO');

INSERT INTO action (id, name, name_en)
VALUES (119, 'এডি (রোড) এর কাছে ফরোয়ার্ড করুন', 'Forward to AD (ROADS)');

INSERT INTO action (id, name, name_en)
VALUES (120, 'ডিডি এর কাছে ফরোয়ার্ড করুন', 'Forward to DD');

INSERT INTO action (id, name, name_en)
VALUES (121, 'ডিরেক্টর এর কাছে ফরোয়ার্ড করুন', 'Forward to Director');

INSERT INTO action (id, name, name_en)
VALUES (122, 'কমিশনার এর কাছে ফরোয়ার্ড করুন', 'Forward to Commissioner');

INSERT INTO action (id, name, name_en)
VALUES (123, 'ডিরেক্টর এর কাছে ফরোয়ার্ড করুন', 'Forward to Director');

INSERT INTO action (id, name, name_en)
VALUES (124, 'ডিডি এর কাছে ফরোয়ার্ড করুন', 'Forward to DD');

INSERT INTO action (id, name, name_en)
VALUES (125, 'এডি (রোড) এর কাছে ফরোয়ার্ড করুন', 'Forward to AD (ROADS)');

INSERT INTO action (id, name, name_en)
VALUES (126, 'টিও এর কাছে ফরোয়ার্ড করুন', 'Forward to TO');

INSERT INTO action (id, name, name_en)
VALUES (127, 'উডিও এর কাছে ফরোয়ার্ড করুন', 'Forward to UDO');

INSERT INTO action (id, name, name_en)
VALUES (128, 'ডিএ এর কাছে ফরোয়ার্ড করুন', 'Forward to DA');

INSERT INTO action (id, name, name_en)
VALUES (129, 'ডিডি এর কাছে ফরোয়ার্ড করুন', 'Forward to DD');

INSERT INTO action (id, name, name_en)
VALUES (130, 'এডি (রোড) এর কাছে ফরোয়ার্ড করুন', 'Forward to AD (ROADS)');

INSERT INTO action (id, name, name_en)
VALUES (131, 'টিও এর কাছে ফরোয়ার্ড করুন', 'Forward to TO');

INSERT INTO action (id, name, name_en)
VALUES (132, 'উডিও এর কাছে ফরোয়ার্ড করুন', 'Forward to UDO');

INSERT INTO action (id, name, name_en)
VALUES (133, 'ডিএ এর কাছে ফরোয়ার্ড করুন', 'Forward to DA');

INSERT INTO action (id, name, name_en)
VALUES (134, 'কমিটি তৈরী করুন তদন্ত এর জন্য ', 'Create Committee For Investigation');

INSERT INTO action (id, name, name_en)
VALUES (135, 'ডিএ এর কাছে ফরোয়ার্ড করুন', 'Forward to DA');

		
		INSERT INTO state (id, description, name, name_en, state_type)
VALUES (131, 'Applied to DA', 'ডিএ এর কাছে আবেদন করা হয়েছে সঠিকঠা নির্ণয় এর জন্য ', 'Applied to DA', null);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (132, 'Forwarded to UDO for Correctness', 'উডিও এর কাছে পাঠানো হয়েছে সঠিকঠা নির্ণয় এর জন্য ',
        'Forwarded to UDO for Correctness', null);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (133, 'Forwarded to TO for Correctness', 'টিও এর কাছে পাঠানো হয়েছে সঠিকঠা নির্ণয় এর জন্য ',
        'Forwarded to TO for Correctness', null);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (134, 'Forwarded to AD(Road) for Correctness', 'এডি (রোড) এর কাছে পাঠানো হয়েছে সঠিকঠা নির্ণয় এর জন্য ',
        'Forwarded to AD(Road) for Correctness', null);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (135, 'Forwarded to DD for Correctness', 'ডিডি এর কাছে পাঠানো হয়েছে সঠিকঠা নির্ণয় এর জন্য ',
        'Forwarded to DD for Correctness', null);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (136, 'Forwarded to Director for Correctness', 'ডিরেক্টর এর কাছে পাঠানো হয়েছে সঠিকঠা নির্ণয় এর জন্য ',
        'Forwarded to Director for Correctness', null);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (137, 'Forwarded to Commissioner for Correctness', 'কমিশনারের কাছে পাঠানো হয়েছে সঠিকঠা নির্ণয় এর জন্য ',
        'Forwarded to Commissioner for Correctness', null);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (138, 'Forward to Director', 'ডিরেক্টর এর কাছে পাঠানো হয়েছে', 'Forward to Director', null);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (139, 'Forward to DD', 'ডিডি এর কাছে পাঠানো হয়েছে', 'Forward to DD', null);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (140, 'Forward to AD (Road)', 'এডি (রোড) এর কাছে পাঠানো হয়েছে', 'Forward to AD (Road)', null);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (141, 'Forward to TO', 'টিও এর কাছে পাঠানো হয়েছে', 'Forward to TO', null);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (142, 'Forward to UDO', 'উডিও এর কাছে পাঠানো হয়েছে', 'Forward to UDO', null);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (143, 'Forward to DA', 'ডিএ এর কাছে পাঠানো হয়েছে', 'Forward to DA', null);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (144, 'Committee Created For Investigation', 'কমিটি তৈরী করা হয়েছে তদন্তের জন্য ',
        'Committee Created For Investigation', null);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (145, 'Forward to DA', 'ডিএ এর কাছে পাঠানো হয়েছে', 'Forward to DA', null);


UPDATE state t
SET t.state_type = 3
WHERE t.id = 145;

UPDATE state t
SET t.state_type = 3
WHERE t.id = 144;

UPDATE state t
SET t.state_type = 3
WHERE t.id = 135;

UPDATE state t
SET t.state_type = 3
WHERE t.id = 134;

UPDATE state t
SET t.state_type = 3
WHERE t.id = 133;

UPDATE state t
SET t.state_type = 3
WHERE t.id = 132;

UPDATE state t
SET t.state_type = 3
WHERE t.id = 139;

UPDATE state t
SET t.state_type = 3
WHERE t.id = 138;

UPDATE state t
SET t.state_type = 0
WHERE t.id = 131;

UPDATE state t
SET t.state_type = 3
WHERE t.id = 136;

UPDATE state t
SET t.state_type = 3
WHERE t.id = 137;

UPDATE state t
SET t.state_type = 3
WHERE t.id = 143;

UPDATE state t
SET t.state_type = 3
WHERE t.id = 142;

UPDATE state t
SET t.state_type = 3
WHERE t.id = 141;

UPDATE state t
SET t.state_type = 3
WHERE t.id = 140;




INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (177, 1255, false, 116, 132, 131);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (178, 1256, false, 117, 131, 132);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (179, 1256, false, 118, 133, 132);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (180, 1257, false, 119, 134, 133);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (181, 1258, false, 120, 135, 134);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (182, 1259, false, 121, 136, 135);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (183, 1260, false, 122, 137, 136);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (184, 1261, false, 123, 138, 137);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (185, 1261, false, 124, 139, 137);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (186, 1261, false, 125, 140, 137);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (187, 1261, false, 126, 141, 137);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (188, 1261, false, 127, 142, 137);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (189, 1261, false, 128, 143, 137);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (190, 1260, false, 129, 139, 138);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (191, 1259, false, 130, 140, 139);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (192, 1258, false, 131, 141, 140);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (193, 1257, false, 132, 142, 141);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (194, 1256, false, 133, 143, 142);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (195, 1255, false, 134, 144, 143);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (196, 1262, false, 135, 145, 144);


INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1255);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1256);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1257);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1258);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1259);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1260);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1261);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1262);

