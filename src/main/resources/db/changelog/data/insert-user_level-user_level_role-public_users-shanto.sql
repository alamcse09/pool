INSERT INTO user_level (id, level, name_bn, name_en) VALUES (5, 2, 'এড', 'AD');
INSERT INTO user_level (id, level, name_bn, name_en) VALUES (6, 2, 'ডড', 'DD');
INSERT INTO user_level (id, level, name_bn, name_en) VALUES (7, 2, 'কমিশনের', 'Commissioner');
INSERT INTO user_level (id, level, name_bn, name_en) VALUES (8, 2, 'ডিরেক্টর', 'Director');
INSERT INTO user_level (id, level, name_bn, name_en) VALUES (9, 2, 'মেকানিক-ইনচার্জ', 'Mechanic Incharge');
INSERT INTO user_level (id, level, name_bn, name_en) VALUES (10, 2, 'পাবলিক ইউসার', 'Public User');
INSERT INTO user_level (id, level, name_bn, name_en) VALUES (11, 2, 'আপ্পার ডিভিশন অফিসার', 'Upper Division Officer');