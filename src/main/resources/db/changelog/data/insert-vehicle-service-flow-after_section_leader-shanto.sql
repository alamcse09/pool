INSERT INTO state (id, description, name, name_en, state_type)
VALUES (113, 'Sent To Foreman From Section Leader', 'সেকশন লিডার থেকে ফোরম্যান এর কাছে পাঠানো হয়েছে ',
        'Sent To Foreman From Section Leader', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (114, 'Sent to Poridorshok', 'পরিদর্শক এর কাছে পাঠানো হয়েছে ', 'Sent to Poridorshok', 3);


INSERT INTO action (id, name, name_en)
VALUES (97, 'ফোরম্যান এর কাছে ফরওয়ার্ড করুন ', 'Forward to Foreman');

INSERT INTO action (id, name, name_en)
VALUES (98, 'পরিদর্শক এর কাছে ফরওয়ার্ড করুন ', 'Forward to Poridorshok');

INSERT INTO action (id, name, name_en)
VALUES (99, 'অনুমোদন', 'Approve');


INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1230, 'ROLE_FORWARD_TO_FOREMAN', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'ফোরম্যান এর কাছে পাঠানো সেকশন লিডার থেকে ', 'Forward to Foreman From Section Leader');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1231, 'ROLE_FORWARD_TO_PORIDORSHOK', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'পরিদর্শক এর কাছে পাঠানো ', 'Forward to Poridorshok');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1232, 'ROLE_APPROVE_BILL', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application', 'বিল অনুমোদন  করা ',
        'Approve Bill');
		
		

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1230);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1231);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1232);


INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (152, 1230, false, 97, 113, 104);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (153, 1231, false, 98, 114, 113);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (154, 1232, false, 99, 57, 114);

