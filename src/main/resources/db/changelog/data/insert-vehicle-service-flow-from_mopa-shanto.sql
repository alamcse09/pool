INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1233, 'ROLE_FORWARD_TO_FOREMAN_FROM_FM', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'অর্থ মন্ত্রানালয় থেকে ফোরম্যান এর কাছে পাঠানো', 'Forward to Foreman From FM');


INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1233);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (155, 1233, false, 85, 105, 43);