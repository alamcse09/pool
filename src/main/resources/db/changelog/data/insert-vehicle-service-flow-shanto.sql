INSERT INTO state (id, description, name, name_en, state_type)
VALUES (97, 'Applied To Director', 'যানবাহন সার্ভিসিং এর জন্য আবেদন করা হয়েছে', 'Applied For Vehicle Service', 0);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (98, 'Forwarded to Foreman', 'ফোরম্যানের কাছে ফরোয়ার্ড করা হয়েছে ', 'Forwarded to Foreman', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (99, 'Created Jobs', 'জব তৈরি করা হয়েছে ', 'Job Created', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (100, 'Forwarded to ATO', 'এ.টি.ও এর কাছে ফরওয়ার্ড করা হয়েছে  ', 'Forwarded to ATO', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (101, 'Forwarded to AD', 'এ ডির কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded to AD', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (102, 'Forwarded to DD', 'ডি ডির কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded to DD', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (103, 'Workshop Manager Assigned', 'ওয়ার্কশপ ম্যানেজার বাছাই করা হয়েছে', 'Workshop Manager Assigned', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (104, 'Section Leader Provided', 'সেকশন লিডার দেয়া হয়েছে ', 'Section Leader Provided', 3);


INSERT INTO state (id, description, name, name_en, state_type)
VALUES (105,'Forwarded to Foreman','ফোরম্যানের কাছে ফরোয়ার্ড করা হয়েছে' ,'Forwarded to Foreman', 3);

INSERT INTO action (id, name, name_en)
VALUES (83, 'ফোরম্যান এর কাছে ফরওয়ার্ড করুন ', 'Forward to Foreman');

INSERT INTO action (id, name, name_en)
VALUES (84, 'জব তৈরী করুন', 'Create Job');

INSERT INTO action (id, name, name_en)
VALUES (85, 'ফোরম্যান এর কাছে ফরওয়ার্ড করুন ', 'Forward to Foreman');

INSERT INTO action (id, name, name_en)
VALUES (86, 'এ.টি.ও এর কাছে ফরওয়ার্ড করুন ', 'Forward to ATO');

INSERT INTO action (id, name, name_en)
VALUES (87, 'এড এর কাছে ফরওয়ার্ড করুন ', 'Forward to AD');

INSERT INTO action (id, name, name_en)
VALUES (89, 'ডিডি এর কাছে ফরওয়ার্ড করুন ', 'Forward to DD');

INSERT INTO action (id, name, name_en)
VALUES (90, 'ওয়ার্কসপ ম্যানেজার বাছাই করুন', 'Choose Workshop Manager');

INSERT INTO action (id, name, name_en)
VALUES (91, 'সেকশন লিডার বরাদ্দ করুন', 'Assign Section Leader');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1208, 'ROLE_VEHICLE_SERVICE_FORWARD_TO_FOREMAN', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'ফোরম্যান এর কাছে ফরওয়ার্ড করা', 'Forward to Foreman');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1209, 'ROLE_VEHICLE_SERVICE_CREATE_JOBS', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application', 'জব তৈরী করা ',
        'Create Job');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1210, 'ROLE_VEHICLE_SERVICE_FORWARD_TO_FOREMAN', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'জব তৈরী থেকে ফোরম্যান এর কাছে পাঠানো ', 'Forward to Foreman After Job Creation');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1211, 'ROLE_VEHICLE_SERVICE_FORWARD_TO_ATO', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'এটিও এর কাছে ফরওয়ার্ড করা ', 'Forward to ATO');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1212, 'ROLE_VEHICLE_SERVICE_FORWARD_TO_AD_FROM_FOREMAN', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'এড এর কাছে ফরওয়ার্ড করা ফোরম্যান থেকে ', 'Forward to AD From Foreman');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1213, 'ROLE_VEHICLE_SERVICE_FORWARD_TO_AD_FROM_ATO', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'এড এর কাছে ফরওয়ার্ড করা এটিও থেকে ', 'Forward to AD From ATO');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1214, 'ROLE_VEHICLE_SERVICE_FORWARD_TO_DD', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'ডিডি এর কাছে ফরওয়ার্ড করা ', 'Forward to DD');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1215, 'ROLE_VEHICLE_SERVICE_CHOOSE_WORKSHOP_MANAGER_FROM_DD', 'যানবাহন সার্ভিসিং',
        'Vehicle Servicing Application', 'ওয়ার্কশপ ম্যানেজার যাচাই ডিডি থেকে ', 'Choose Workshop Manager From DD');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1216, 'ROLE_VEHICLE_SERVICE_CHOOSE_WORKSHOP_MANAGER_FROM_AD', 'যানবাহন সার্ভিসিং',
        'Vehicle Servicing Application', 'ওয়ার্কশপ ম্যানেজার যাচাই এড থেকে ', 'Choose Workshop Manager From AD');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1217, 'ROLE_VEHICLE_SERVICE_ASSIGN_SECTION_LEADERS', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'সেকশন লিডার নিয়োগ দেয়া ', 'Assign Section Leader');

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (133, 1208, false, 83, 98, 97);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (134, 1209, false, 84, 99, 98);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (135, 1210, false, 85, 105, 99);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (136, 1211, false, 86, 100, 105);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (137, 1212, false, 87, 101, 105);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (138, 1213, false, 87, 101, 100);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (139, 1214, false, 89, 102, 101);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (140, 1215, false, 90, 103, 102);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (141, 1216, false, 91, 104, 103);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (142, 1217, false, 90, 103, 101);


INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1208);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1209);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1210);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1211);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1212);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1213);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1214);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1215);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1216);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1217);

