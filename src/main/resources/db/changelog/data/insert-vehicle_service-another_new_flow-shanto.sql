INSERT INTO state (id, description, name, name_en, state_type)
VALUES (163, 'Forward to AD (Road)', 'সহকারি পরিচালক (সড়ক) এর কাছে পাঠানো হয়েছে', 'Forward to AD (Road)', 0);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (164, 'Forward to Workshop Manager', 'ওয়ার্কশপ ম্যানেজার এর কাছে পাঠানো হয়েছে', 'Forward to Workshop Manager',
        3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (165, 'Sent to Poridorshok', 'পরিদর্শক এর কাছে পাঠানো হয়েছে ', 'Sent to Poridorshok', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (166, 'Forwarded to Job Assistant', 'জব সহকারীর কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded to Job Assistant', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (167, 'Created Jobs', 'জব তৈরি করা হয়েছে ', 'Job Created', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (168, 'Forwarded to Foreman', 'ফোরম্যানের কাছে ফরোয়ার্ড করা হয়েছে ', 'Forwarded to Foreman', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (169, 'Sent to Poridorshok', 'পরিদর্শক এর কাছে পাঠানো হয়েছে ', 'Sent to Poridorshok', 3);

INSERT INTO action (id, name, name_en)
VALUES (155, 'জব সহকারীর বাছাই করুন', 'Choose Job Assistant');


INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1271, 'ROLE_VEHICLE_REQUISITION_JOB_ASSISTANT', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'জব সহকারীর কাছে ফরোয়ার্ড করা', 'Forwarding to Job Assistant');

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (222, 1215, false, 90, 164, 163);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (223, 1231, false, 98, 165, 164);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (224, 1271, false, 155, 166, 165);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (225, 1209, false, 84, 167, 166);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (226, 1217, false, 91, 168, 167);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (227, 1231, false, 98, 169, 168);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1271);

