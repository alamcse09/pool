INSERT INTO action (id, name, name_en)
VALUES (157, 'পরিদর্শক এর কাছে ফরওয়ার্ড করুন ম্যানেজার থেকে ', 'Forward to Poridorshok From Manager');

UPDATE state_action_map t
SET t.action_id = 157
WHERE t.id = 223;
