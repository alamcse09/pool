INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1225, 'ROLE_WARRANTY_CLAIM_MANAGEMENT', 'ওয়ারেন্টি দাবি ব্যাবস্থাপনা', 'Warranty Claim',
        'ওয়ারেন্টি দাবি সংযুক্তি', 'Warranty Claim Add');
		
INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1225);
