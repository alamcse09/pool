INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1222, 'ROLE_WARRANTY_CLAIM_VIEW', 'ওয়ারেন্টি দাবি ব্যাবস্থাপনা', 'Warranty Claim', 'ওয়ারেন্টি দাবি ভিউ করা ',
        'Warranty Claim View');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1223, 'ROLE_WARRANTY_CLAIM_APPROVE', 'ওয়ারেন্টি দাবি ব্যাবস্থাপনা', 'Warranty Claim',
        'ওয়ারেন্টি দাবি অনুমোদন দেয়া ', 'Warranty Claim Approve');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1224, 'ROLE_WARRANTY_CLAIM_REJECT', 'ওয়ারেন্টি দাবি ব্যাবস্থাপনা', 'Warranty Claim',
        'ওয়ারেন্টি দাবি বাতিল করা ', 'Warranty Claim Reject');
		
		
		INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1222);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1223);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1224);


INSERT INTO state (id, description, name, name_en, state_type)
VALUES (108, 'Sent to Vendor', 'ভেন্ডর এর কাছে পাঠানো হয়েছে ', 'Sent to Vendor', 0);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (109, 'Vendor Approved', 'ভেন্ডর অনুমোদিত দিয়েছে ', 'Vendor Approved', 1);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (110, 'Vendor Rejected', 'ভেন্ডর বাতিল করেছে ', 'Vendor Rejected', 2);

INSERT INTO action (id, name, name_en)
VALUES (93, 'অনুমোদন', 'Approve');

INSERT INTO action (id, name, name_en)
VALUES (94, 'বাতিল', 'Reject');

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (146, 1223, false, 93, 109, 108);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (147, 1224, false, 94, 110, 108);