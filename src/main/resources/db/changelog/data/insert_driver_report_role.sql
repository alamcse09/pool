INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (4008, 'ROLE_REPORT_DRIVER', 'রিপোর্ট ব্যবস্থাপনা', 'Report Management', ' ড্রাইভার রিপোর্ট ', 'Driver Report');

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (1, 4008);
