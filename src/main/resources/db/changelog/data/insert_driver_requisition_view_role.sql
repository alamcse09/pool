INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (4009, 'ROLE_DRIVER_REQUISITION_VIEW', 'চালক ব্যবস্থাপনা ', 'Driver Management', 'চালক অনুরোধ ভিউ','Driver Requisition View');

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (1, 4009);

