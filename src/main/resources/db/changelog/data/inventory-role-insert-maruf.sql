insert into role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) values (2110, 'ROLE_INVENTORY_ADD', 'ইনভেন্টরি', 'Inventory', 'ইনভেন্টরি সংযুক্তি', 'Inventory Add');
insert into role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) values (2111, 'ROLE_INVENTORY_EDIT', 'ইনভেন্টরি', 'Inventory', 'ইনভেন্টরি সম্পাদনা', 'Inventory Edit');
insert into role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) values (2112, 'ROLE_INVENTORY_SEARCH', 'ইনভেন্টরি', 'Inventory', 'ইনভেন্টরি খুঁজুন', 'Inventory Search');
insert into role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) values (2113, 'ROLE_INVENTORY_DELETE', 'ইনভেন্টরি', 'Inventory', 'ইনভেন্টরি মুছে ফেলা', 'Inventory Delete');

INSERT INTO user_level_role (user_level_id, role_id) VALUES (11, 2110);

INSERT INTO user_level_role (user_level_id, role_id) VALUES (11, 2111);

INSERT INTO user_level_role (user_level_id, role_id) VALUES (11, 2112);

INSERT INTO user_level_role (user_level_id, role_id) VALUES (11, 2113);
