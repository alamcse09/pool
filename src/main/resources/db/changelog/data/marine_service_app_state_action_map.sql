INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (523, 'ROLE_MARINE_SERVICE_CREATE_COMMITTEE', 'মেরিন সার্ভিসিং', 'Marine Service', 'কমিটি তৈরি করন', 'Create committee');
INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (524, 'ROLE_MARINE_SERVICE_SUBMIT_REPORT', 'মেরিন সার্ভিসিং', 'Marine Service', 'কমিটির রিপোর্ট জমাদান', 'Submit committee report');
INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (525, 'ROLE_MARINE_SERVICE_APPROVE', 'মেরিন সার্ভিসিং', 'Marine Service', 'সার্ভিস আবেদন অনুমোদন', 'Service application approve');
INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (526, 'ROLE_MARINE_SERVICE_REJECT', 'মেরিন সার্ভিসিং', 'Marine Service', 'সার্ভিস আবেদন বাতিল ', 'Service application reject');

INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 523);
INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 524);
INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 525);
INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 526);

INSERT INTO state (id, description, name, name_en, state_type) VALUES (85, 'Applied for marine Service', 'এপ্লাইড', 'Applied', 0);
INSERT INTO state (id, description, name, name_en, state_type) VALUES (86, 'Marine service committee created', 'কমিটি তৈরি করা হয়েছে', 'Committee Created', 1);
INSERT INTO state (id, description, name, name_en, state_type) VALUES (87, 'Marine service committee report submitted', 'রিপোর্ট দেয়া হয়েছে', 'Report Submitted', 1);
INSERT INTO state (id, description, name, name_en, state_type) VALUES (88, 'Marine service Approved', 'এপ্রুভড', 'Approved', 2);
INSERT INTO state (id, description, name, name_en, state_type) VALUES (89, 'Marine service Rejected', 'রিজেক্টেড', 'Rejected', 3);

INSERT INTO action (id, name, name_en) VALUES (74, 'কমিটি তৈরি করুন', 'Create Committee');
INSERT INTO action (id, name, name_en) VALUES (75, 'রিপোর্ট জমা দিন', 'Submit Report');
INSERT INTO action (id, name, name_en) VALUES (76, 'অনুমোদন', 'Approve');
INSERT INTO action (id, name, name_en) VALUES (77, 'বাতিল', 'Reject');

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) VALUES (125, 523, false, 74, 86, 85);
INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) VALUES (126, 524, false, 75, 87, 86);
INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) VALUES (127, 525, false, 76, 88, 87);
INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) VALUES (128, 526, false, 77, 89, 87);