INSERT INTO user_level (id, level, name_bn, name_en) VALUES (13, 2, 'মিটিং মিনিট টেকার ', 'Meeting Minute Taker');

INSERT INTO designation (id, created_at, created_by, is_deleted, modified_at, modified_by, name_bn,                                        name_en, parent_id, user_level_id) 
VALUES (11, '2021-09-02 15:39:04.000000', 1, false, '2021-09-02 15:39:04.000000', 1, 'মিটিং মিনিট টেকার ',
        'Meeting Minute Taker', null, 11);
		
		
INSERT INTO user_level_role (user_level_id, role_id) VALUES (13, 1141);


INSERT INTO employee (id, created_at, created_by, is_deleted, modified_at, modified_by, achievements,
                                     actual_salary_of_join, additional_increment_number_of_join, appointment_date,
                                     appointment_order, attendance_and_leave_info, blood_group, comments,
                                     current_actual_salary, current_additional_increment_number, current_grade,
                                     current_initial_grade_salary, current_step, date_of_birth, email,
                                     employee_doc_group_id, employee_id, father_name, gender,
                                     initial_grade_salary_of_join, job_type, joining_date, joining_designation,
                                     joining_grade, marital_status, mobile_number, mother_name, name, name_en, nid,
                                     number_of_children, office_ministry, passport_number, password, password_again,
                                     permanent_address, permanent_date, personal_id, present_address, promotion_info,
                                     quota, reporting_manager, retirement_date, step_of_join, transfer_info,
                                     designation_id)
VALUES (17, '2021-10-03 12:23:52.000000', 1, false, '2021-10-03 12:23:52.000000', 1, null, null, null, '2021-09-29',
        'fdfg', null, 2, null, null, null, 0, null, null, '1994-06-14', 'minutetaker@gmail.com', null, '6565', 'Manik',
        null, null, 0, '2021-09-29', 'dfg', 0, 0, '01750889597', 'Rokeya', 'Ullah', 'Ullah', '9575258415', null, 'dfg',
        '423428', null, null, 'chuadanga', null, '5005', 'Dhaka', null, null, 'dfg', null, null, 'dfg', 11);
		

INSERT INTO user (id, created_at, created_by, is_deleted, modified_at, modified_by, is_enabled, password,
                                 profile_pic_document_group_id, sign_document_group_id, user_type, username,
                                 employee_id, mechanic_id, public_user_info_id, user_level_id)
VALUES (28, '2021-10-11 15:36:19.767968', 1, false, '2021-10-11 15:36:24.269440', 1, true,
        '$2a$10$qnoLqN3KJWDGmjoF4XxxTe07VJvSXNjD1/cr0lKMFh/LeH1EEogIW', null, null, 0, 'minutetaker@gmail.com', 17,
        null, null, 13);

