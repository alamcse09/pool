INSERT INTO color (id,color_name_bn,color_name_en)
VALUES (1,'নীল' , 'Blue'),
       (2,'কালো' , 'Black'),
       (3,'লাল', 'Red'),
       (4,'রেড ওয়াইন', 'Red Wine'),
       (5,'নীলাভ সবুজ', 'Teal'),
       (6,'সবুজ', 'Green'),
       (7,'ধূসর', 'Grey'),
       (8,'সাদা', 'White'),
       (9,'পার্ল হোয়াইট', 'Pearl White'),
       (10,'পার্ল ব্ল্যাক', 'Pearl Black');
