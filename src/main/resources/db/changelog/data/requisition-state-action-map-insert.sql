insert into action (id, name, name_en) values (31, 'আপার ডিভিশন অফিসারের কাছে ফরোয়ার্ড করুন', 'Forward to Upper Division Officer');

insert into state (id, description, name, name_en, state_type) values (34, 'Send to Commissioner and Director', 'কমিশনার ও ডিরেক্টর এর কাছে পাঠান হয়েছে', 'Send to Commissioner and Director', 3);
insert into state (id, description, name, name_en, state_type) values (35, 'Forwarded to Upper Division Officer', 'আপার ডিভিশন অফিসারের কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded to Upper Division Officer', 3);
insert into state (id, description, name, name_en, state_type) values (36, 'Forwareded to AD to choose Vehicle', 'এডি র কাছে গারি নির্ধারণের জন্য ফরোয়ার্ড করা হয়েছে', 'Forwareded to AD to choose Vehicle', 3);

insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (75, 2100, false, 24, 28, 34);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (76, 2106, true, 7, 34, 34);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (77, 2115, false, 31, 35, 28);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (78, 2116, false, 24, 36, 35);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (79, 2101, false, 25, 29, 36);

insert into role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) values (2115, 'ROLE_VEHICLE_REQUISITION_FORWARD_TO_UDO', 'যানবাহন বরাদ্দ', 'Vehicle Requisition', 'যানবাহন বরাদ্দ আপার ডিভিশন অফিসার', 'Vehicle Requisition Forward to UDO');
insert into role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) values (2116, 'ROLE_VEHICLE_REQUISITION_FORWARD_TO_AD_BY_UDO_OA', 'যানবাহন বরাদ্দ', 'Vehicle Requisition', 'যানবাহন বরাদ্দ এডী', 'Vehicle Requisition Forward AD');

insert into user_level_role (user_level_id, role_id) values (2, 2116);
insert into user_level_role (user_level_id, role_id) values (11, 2116);

insert into user_level_role (user_level_id, role_id) values (2, 2115);
insert into user_level_role (user_level_id, role_id) values (5, 2115);

insert into user_level_role (user_level_id, role_id) values (8, 2100);

insert into user_level_role (user_level_id, role_id) values (7, 2100);





