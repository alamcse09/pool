INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2510, 'ROLE_VEHICLE_SERVICE_MY_SEARCH', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'যানবাহন আমার সার্ভিসিং খুজুন', 'Search my servicing');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2511, 'ROLE_VEHICLE_SERVICE_VIEW', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'যানবাহন সার্ভিসিং দেখুন', 'View vehicle servicing');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2512, 'ROLE_VEHICLE_SERVICE_TAKE_ACTION', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'যানবাহন সার্ভিসিং একশন গ্রহন করুন', 'Vehicle Servicing take action');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2513, 'ROLE_VEHICLE_SERVICE_ADD_MORE_PRODUCT', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application',
        'যানবাহন সার্ভিসিং এ প্রডাক্ট যুক্ত করুন', 'Vehicle servicing add more product');


UPDATE role t
SET t.role_text_bn = 'যানবাহন সার্ভিসিং ওয়ার্কশপ ম্যেনেজার বাছাই',
    t.role_text_en = 'Vehicle servicing choose workshop manager'
WHERE t.id = 2506;

UPDATE role t
SET t.role_text_bn = 'যানবাহন সার্ভিসিং অর্থ মন্ত্রানালয় থেকে এপ্রুভ',
    t.role_text_en = 'Vehicle servicing approve FM'
WHERE t.id = 2503;

UPDATE role t
SET t.role_text_bn = 'যানবাহন সার্ভিসিং মপা থেকে এপ্রুভ',
    t.role_text_en = 'Vehicle servicing approve mopa'
WHERE t.id = 2502;

UPDATE role t
SET t.role_text_bn = 'যানবাহন সার্ভিসিং জব এপ্রুভ',
    t.role_text_en = 'Vehicle servicing approve job'
WHERE t.id = 2501;

UPDATE role t
SET t.role_text_bn = 'যানবাহন সার্ভিসিং সার্ভিস সংযুক্তি',
    t.role_text_en = 'Vehicle servicing add service'
WHERE t.id = 3100;

UPDATE role t
SET t.role_text_bn = 'যানবাহন সার্ভিসিং জব তৈরি করুন',
    t.role_text_en = 'Vehicle servicing create job'
WHERE t.id = 2500;

UPDATE role t
SET t.role_text_bn = 'যানবাহন সার্ভিসিং মেকানিক বাছাই',
    t.role_text_en = 'Vehicle servicing choose mechanic'
WHERE t.id = 2505;

