INSERT INTO state (id, description, name, name_en, state_type) VALUES (46, 'Mechanic In-charge Assigned', 'মেকানিক ইনচার্জ দেয়া হয়েছে', 'Mechanic In-charge Assigned', 3);

UPDATE state_action_map t
SET t.action_id     = 36,
    t.next_state_id = 46
WHERE t.id = 86;

UPDATE state t
SET t.name = 'ওয়ার্কশপ ম্যানেজার বাছাই করা হয়েছে'
WHERE t.id = 45;

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (47, 'Waiting for mechanic input', 'মেকানিক ইনপুটের অপেক্ষায়', 'Waiting for mechanic input', 3);


UPDATE state_action_map t
SET t.action_id     = 32,
    t.next_state_id = 47,
    t.state_id      = 46
WHERE t.id = 87;

UPDATE state_action_map t
SET t.view_only = false
WHERE t.id = 87;

INSERT INTO action (id, name, name_en)
VALUES (38, 'জব সাবমিট করুন', 'Submit Job');

UPDATE state_action_map t
SET t.view_only     = false,
    t.action_id     = 38,
    t.next_state_id = 38,
    t.state_id      = 47
WHERE t.id = 88;

UPDATE state_action_map t
SET t.action_id     = 33,
    t.next_state_id = 41,
    t.state_id      = 47
WHERE t.id = 89;

INSERT INTO state_action_map (role_id, view_only, action_id, next_state_id, state_id)
VALUES (2500, true, 34, 42, 47);

INSERT INTO state_action_map (role_id, view_only, action_id, next_state_id, state_id)
VALUES (2500, true, 35, 43, 47);

UPDATE state_action_map t
SET t.role_id = 2500
WHERE t.id = 83;

UPDATE state_action_map t
SET t.role_id = 2500
WHERE t.id = 81;

UPDATE state_action_map t
SET t.role_id = 2500
WHERE t.id = 82;

UPDATE state_action_map t
SET t.role_id = 2500
WHERE t.id = 89;


