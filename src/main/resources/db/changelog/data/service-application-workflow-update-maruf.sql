INSERT INTO action (id, name, name_en) VALUES (36, 'মেকানিক ইনচার্জ বাছাই করুন', 'Choose Mechanic In-charge');
insert into action (id, name, name_en) values (37, 'ওয়ার্কসপ ম্যানেজার বাছাই করুন', 'Choose Workshop Manager');


INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (2505, 'ROLE_CHOOSE_MECHANIC_IN_CHARGE', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application','যানবাহন সার্ভিসিং', 'Vehicle Servicing');
		
INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) VALUES (2506, 'ROLE_CHOOSE_WORKSHOP_MANAGER', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing');
		

INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 2505);
INSERT INTO user_level_role (user_level_id, role_id) VALUES (2, 2506);

INSERT INTO state (id, description, name, name_en, state_type) VALUES (44, 'Mechanic In-charge Assigned', 'মেকানিক ইনচার্জ দেয়া হয়েছে', 'Mechanic In-charge Assigned', 3);
insert into state (id, description, name, name_en, state_type) values (45, 'Workshop manager assigned', 'ম্যানেজার বাছাই করা হয়েছে', 'Workshop manager assigned', 3);


INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) VALUES (84, 2505, false, 36, 44, 37);
insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (85, 2506, false, 37, 45, 37);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) VALUES (86, 2500, false, 32, 38, 45);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) VALUES (87, 2501, true, 33, 41, 45);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) VALUES (88, 2502, true, 34, 42, 45);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) VALUES (89, 2503, true, 35, 43, 45);

UPDATE state t SET t.description = 'Applied To Director' WHERE t.id = 37;

UPDATE state_action_map t SET t.state_id = 44 WHERE t.id = 80;

UPDATE state_action_map t SET t.state_id = 44 WHERE t.id = 81;

UPDATE state_action_map t SET t.state_id = 44 WHERE t.id = 82;

UPDATE state_action_map t SET t.state_id = 44 WHERE t.id = 83;
