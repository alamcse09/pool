INSERT INTO action (id, name, name_en)
VALUES (41, 'ডি ডির কাছে ফরোয়ার্ড করুন', 'Forward to DD' );

INSERT INTO action (id, name, name_en)
VALUES (42, 'ডিরেক্টরের কাছে ফরোয়ার্ড করুন', 'Forward to Director' );

INSERT INTO action (id, name, name_en)
VALUES (43, 'কমিশনারের কাছে ফরোয়ার্ড করুন', 'Forward to commissioner');

INSERT INTO action (id, name, name_en)
VALUES (44, 'একাউন্টস এর কাছে ফরোয়ার্ড করুন', 'Forward to accounts');

INSERT INTO action (id, name, name_en)
VALUES (45, 'এ ওর কাছে ফরোয়ার্ড করুন', 'Forward to AO');

INSERT INTO action (id, name, name_en)
VALUES (46, 'এম ডির কাছে ফরোয়ার্ড করুন', 'Forward to MD');

INSERT INTO action (id, name, name_en)
VALUES (47, 'অনুমোদন দিন', 'Approve');

INSERT INTO action (id, name, name_en)
VALUES (48, 'বাতিল করুন', 'Reject');

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (51, 'Forwarded to DD', 'ডি ডির কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded to DD', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (52, 'Forwarded To Director', 'ডিরেক্টর এর কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded To director', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (53, 'Forwarded To Commissioner', 'কমিশনার এর কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded To commissioner', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (54, 'Forwarded To accounts', 'একাউন্টস এর কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded To accounts', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (55, 'Forwarded to AO', 'এ ওর কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded to AO', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (56, 'Forwarded to MD', 'এম ডির কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded to MD', 3);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (57, 'Bill Approved', 'বিল অনুমোদিত', 'Bill Approved', 1);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (58, 'Rejected', 'বাতিল', 'Rejected', 1);

INSERT INTO state (id, description, name, name_en, state_type)
VALUES (59, 'Forwarded to commissioner for approval', 'কমিশনার এর কাছে অনুমোদন এর জন্য ফরোয়ার্ড করা হয়েছে',
        'Forwarded to commissioner for approval', 3);


INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2624, 'ROLE_FORWARD_TO_DD', 'বিল ব্যবস্থাপনা', 'Bill Management', 'ডি ডির কাছে ফরোয়ার্ড', 'Forward to DD');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2625, 'ROLE_FORWARD_TO_DIRECTOR', 'বিল ব্যবস্থাপনা', 'Bill Management', 'ডিরেক্টরের কাছে ফরোয়ার্ড',
        'Forward To director');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2626, 'ROLE_FORWARD_TO_COMMISSIONER', 'বিল ব্যবস্থাপনা', 'Bill Management', 'কমিশনারের কাছে ফরোয়ার্ড',
        'Forward to commissioner');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2627, 'ROLE_FORWARD_TO_ACCOUNTS', 'বিল ব্যবস্থাপনা', 'Bill Management', 'একাউন্টস এর কাছে ফরোয়ার্ড',
        'Forward to accounts');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2628, 'ROLE_FORWARD_TO_AO', 'বিল ব্যবস্থাপনা', 'Bill Management', 'এ ওর কাছে ফরোয়ার্ড', 'Forward to AO');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2629, 'ROLE_FORWARD_TO_MD', 'বিল ব্যবস্থাপনা', 'Bill Management', 'এম ডির কাছে ফরোয়ার্ড', 'Forward to MD');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2630, 'ROLE_APPROVE_BILL', 'বিল ব্যবস্থাপনা', 'Bill Management', 'বিল অনুমোদন', 'Approve bill');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (2631, 'ROLE_REJECT_BILL', 'বিল ব্যবস্থাপনা', 'Bill Management', 'বিল বাতিল', 'Bill rejected');


INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 2624);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 2625);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 2626);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 2627);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 2628);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 2629);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 2630);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 2631);



INSERT INTO state_action_map (role_id, view_only, action_id, next_state_id, state_id)
VALUES (2624, false, 41, 51, 49);

INSERT INTO state_action_map (role_id, view_only, action_id, next_state_id, state_id)
VALUES (2624, false, 41, 51, 50);

INSERT INTO state_action_map (role_id, view_only, action_id, next_state_id, state_id)
VALUES (2625, false, 42, 52, 51);

INSERT INTO state_action_map (role_id, view_only, action_id, next_state_id, state_id)
VALUES (2626, false, 43, 53, 52);

INSERT INTO state_action_map (role_id, view_only, action_id, next_state_id, state_id)
VALUES (2627, false, 44, 54, 53);

INSERT INTO state_action_map (role_id, view_only, action_id, next_state_id, state_id)
VALUES (2628, false, 45, 55, 54);

INSERT INTO state_action_map (role_id, view_only, action_id, next_state_id, state_id)
VALUES (2629, false, 46, 56, 55);

INSERT INTO state_action_map (role_id, view_only, action_id, next_state_id, state_id)
VALUES (2626, false, 43, 59, 56);

INSERT INTO state_action_map (role_id, view_only, action_id, next_state_id, state_id)
VALUES (2630, false, 47, 57, 59);

INSERT INTO state_action_map (role_id, view_only, action_id, next_state_id, state_id)
VALUES (2630, false, 48, 58, 59);

