UPDATE employee t
SET t.email = 'dir@gmail.com'
WHERE t.id = 4;

UPDATE employee t
SET t.email = 'mi@gmail.com'
WHERE t.id = 3;

UPDATE employee t
SET t.email = 'com@gmail.com'
WHERE t.id = 5;

UPDATE employee t
SET t.email = 'dd@gmail.com'
WHERE t.id = 6;

UPDATE employee t
SET t.email = 'udo@gmail.com'
WHERE t.id = 2;

UPDATE employee t
SET t.email = 'ad@gmail.com'
WHERE t.id = 7;

