UPDATE state SET id = 60 WHERE description = 'Incident Report Instantiated';
UPDATE state SET id = 61 WHERE description = 'Forwarded to Action Taker';
UPDATE state SET id = 62 WHERE description = 'Upload Inquiry Report';


UPDATE action SET id = 50 WHERE name_en = 'Create Inquiry Team';
UPDATE action SET id = 51 WHERE name_en = 'Upload Inquiry Report';


UPDATE state_action_map SET id = 108 AND action_id = 50 AND next_state_id = 61 AND state_id = 60 WHERE role_id = 1152;
UPDATE state_action_map SET id = 109 AND action_id = 51 AND next_state_id = 62 AND state_id = 61 WHERE role_id = 1153;

