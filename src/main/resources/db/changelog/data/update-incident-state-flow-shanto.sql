UPDATE state SET id = 51 WHERE description = 'Incident Report Instantiated';
UPDATE state SET id = 52 WHERE description = 'Forwarded to Action Taker';
UPDATE state SET id = 53 WHERE description = 'Upload Inquiry Report';


UPDATE action SET id = 41 WHERE name_en = 'Create Inquiry Team';
UPDATE action SET id = 42 WHERE name_en = 'Upload Inquiry Report';


UPDATE state_action_map SET id = 96 WHERE role_id = 1152;
UPDATE state_action_map SET id = 97 WHERE role_id = 1153;