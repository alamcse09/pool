INSERT INTO state (id, description, name, name_en, state_type)
VALUES (170, 'Sent to Job Assistant', 'জব সহকারীর কাছে ফরোয়ার্ড করা হয়েছে', 'Forwarded to Job Assistant', 3);

INSERT INTO action (id, name, name_en)
VALUES (156, 'জব সহকারীর কাছে ফরোয়ার্ড করুন', 'Forwarded to Job Assistant');

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (228, 1271, false, 156, 170, 169);

INSERT INTO state_action_map (id, role_id, view_only, action_id, next_state_id, state_id)
VALUES (229, 1272, false, 99, 38, 170);

UPDATE role t
SET t.role_group_bn = 'বিল ব্যবস্থাপনা',
    t.role_group_en = 'Bill Management'
WHERE t.id = 1232;

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1272, 'ROLE_SERVICE_APPROVE', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application', 'যানবাহন সার্ভিসিং অনুমোদন',
        'Approve Service');

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1272);

