UPDATE action t
SET t.name    = 'ওয়ারেন্টি পাওয়া গিয়েছে',
    t.name_en = 'Warranty Found'
WHERE t.id = 93;

UPDATE action t
SET t.name    = 'ওয়ারেন্টি পাওয়া যায়নি ',
    t.name_en = 'Warranty Not Found'
WHERE t.id = 94;

