UPDATE action t
SET t.name = 'জব সহকারী বাছাই করুন'
WHERE t.id = 155;

UPDATE action t
SET t.name = 'ম্যানেজার বাছাই করুন'
WHERE t.id = 90;


UPDATE state t
SET t.description = 'Forward to Manager',
    t.name        = 'ম্যানেজার এর কাছে পাঠানো হয়েছে',
    t.name_en     = 'Forward to Manager'
WHERE t.id = 164;