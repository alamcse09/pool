insert into state (id, description, name, name_en, state_type) values (37, 'Applied For Vehicle Service', 'যানবাহন সার্ভিসিং এর জন্য আবেদন করা হয়েছে', 'Applied For Vehicle Service', 0);
insert into state (id, description, name, name_en, state_type) values (38, 'Servicing Application Accepted', 'সার্ভিসিং আবেদন অনুমোদিত', 'Servicing Application Approved', 1);
insert into state (id, description, name, name_en, state_type) values (39, 'Send to MOPA', 'মপা কাছে পাঠানো হয়েছে', 'Send to MOPA', 3);
insert into state (id, description, name, name_en, state_type) values (40, 'Send to FM', 'অর্থ মন্ত্রানলয়ে পাঠানো হয়েছে', 'Send to FM', 3);


insert into action (id, name, name_en) values (32, 'জব তৈরি করুন', 'Create Job');

insert into role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en) values (2500, 'ROLE_CREATE_JOB', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing Application', 'যানবাহন সার্ভিসিং', 'Vehicle Servicing');


insert into state_action_map (id, role_id, view_only, action_id, next_state_id, state_id) values (80, 2500, false, 32, 38, 37);

insert into user_level_role (user_level_id, role_id) values (2, 2500);


