INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1191, 'ROLE_WARRANTY_CLAIM_SEARCH', 'ওয়ারেন্টি দাবি ব্যাবস্থাপনা', 'Warranty Claim', 'ওয়ারেন্টি দাবি খুজুন ',
        'Warranty Search');

INSERT INTO role (id, name, role_group_bn, role_group_en, role_text_bn, role_text_en)
VALUES (1192, 'ROLE_WARRANTY_CLAIM_DELETE', 'ওয়ারেন্টি দাবি ব্যাবস্থাপনা', 'Warranty Claim', 'ওয়ারেন্টি দাবি মুছে ফেলা',
        'Warranty Delete');

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1191);

INSERT INTO user_level_role (user_level_id, role_id)
VALUES (2, 1192);