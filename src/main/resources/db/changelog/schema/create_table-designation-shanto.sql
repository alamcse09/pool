create table if not exists designation
(
	id bigint auto_increment
		primary key,
	version bigint null,
	created_at datetime(6) null,
	created_by bigint null,
	is_deleted bit null,
	modified_at datetime(6) null,
	modified_by bigint null,
	name_bn varchar(255) null,
	name_en varchar(255) null,
	parent_id bigint null
);