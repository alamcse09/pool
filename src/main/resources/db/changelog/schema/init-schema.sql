
create table action (
                        id bigint not null auto_increment,
                        name varchar(255) not null,
                        name_en varchar(255) not null,
                        primary key (id)
) engine=InnoDB;
    

create table audit_log (
                           id bigint not null auto_increment,
                           description varchar(255),
                           ip_address varchar(255),
                           log_event integer,
                           related_entity_id bigint,
                           timestamp datetime(6),
                           user_id bigint,
                           primary key (id)
) engine=InnoDB;
    

create table complains (
                           id bigint not null auto_increment,
                           discard varchar(255),
                           misconduct varchar(255),
                           other_objections varchar(255),
                           primary key (id)
) engine=InnoDB;
    

create table doc_group (
                           id bigint not null,
                           primary key (id)
) engine=InnoDB;
    

create table doc_group_sequencce (
    next_val bigint
) engine=InnoDB;
    

insert into doc_group_sequencce values ( 1 );
    

create table document (
                          id bigint not null auto_increment,
                          created_at datetime(6),
                          created_by bigint,
                          is_deleted bit,
                          modified_at datetime(6),
                          modified_by bigint,
                          content_type varchar(1000),
                          data LONGBLOB,
                          doc_group_id bigint,
                          document_type integer,
                          name varchar(4000),
                          path varchar(2048),
                          primary key (id)
) engine=InnoDB;
    

create table driver (
                        id bigint not null auto_increment,
                        created_at datetime(6),
                        created_by bigint,
                        is_deleted bit,
                        modified_at datetime(6),
                        modified_by bigint,
                        average_ratings double precision,
                        blood_group varchar(255),
                        count_of_people_rated double precision,
                        current_address varchar(255),
                        current_salary bigint,
                        date_of_birth date,
                        driver_document_group_id bigint,
                        driver_id bigint,
                        driver_type integer,
                        educational_qualification varchar(255),
                        enrolment_date date,
                        father_name varchar(255),
                        gender_type integer,
                        leave_info varchar(255),
                        licence_expiry_date date,
                        licence_number varchar(255),
                        marital_status integer,
                        mother_name varchar(255),
                        name varchar(255),
                        nid_number varchar(255),
                        passport_number varchar(255),
                        permanent_address varchar(255),
                        phone_number varchar(255),
                        police_verification_document_group_id bigint,
                        retirement_date date,
                        total_experience_duration varchar(255),
                        total_ratings double precision,
                        primary key (id)
) engine=InnoDB;
    

create table driver_assign (
                               id bigint not null auto_increment,
                               created_at datetime(6),
                               created_by bigint,
                               is_deleted bit,
                               modified_at datetime(6),
                               modified_by bigint,
                               chassis_number varchar(255),
                               driver_id bigint,
                               driver_type integer,
                               name varchar(255),
                               reg_number varchar(255),
                               user_designation varchar(255),
                               user_workplace varchar(255),
                               primary key (id)
) engine=InnoDB;
    

create table driver_complaint_record (
                                         id bigint not null auto_increment,
                                         abandonment varchar(255),
                                         accident varchar(255),
                                         comments varchar(255),
                                         complaint_document_group_id bigint,
                                         mischief varchar(255),
                                         others varchar(255),
                                         primary key (id)
) engine=InnoDB;
    

create table driver_driver_complaint_record_map (
                                                    driver_id bigint not null,
                                                    driver_complaint_record_id bigint not null,
                                                    primary key (driver_id, driver_complaint_record_id)
) engine=InnoDB;
    

create table driver_driver_fitness_record_map (
                                                  driver_id bigint not null,
                                                  driver_fitness_record_id bigint not null,
                                                  primary key (driver_id, driver_fitness_record_id)
) engine=InnoDB;
    

create table driver_driver_transfer_record_map (
                                                   driver_id bigint not null,
                                                   driver_transfer_record_id bigint not null,
                                                   primary key (driver_id, driver_transfer_record_id)
) engine=InnoDB;
    

create table driver_fitness_record (
                                       id bigint not null auto_increment,
                                       driver_fitness varchar(255),
                                       fitness_date date,
                                       primary key (id)
) engine=InnoDB;
    

create table driver_transfer_record (
                                        id bigint not null auto_increment,
                                        designation varchar(255),
                                        end_date date,
                                        phone_number varchar(255),
                                        reg_number varchar(255),
                                        start_date date,
                                        user_name varchar(255),
                                        workplace varchar(255),
                                        primary key (id)
) engine=InnoDB;
    

create table email (
                       id bigint not null auto_increment,
                       created_at datetime(6),
                       created_by bigint,
                       is_deleted bit,
                       modified_at datetime(6),
                       modified_by bigint,
                       bcc varchar(3000),
                       cc varchar(3000),
                       doc_group_id bigint,
                       is_html bit,
                       params TEXT,
                       subject TEXT,
                       template_path varchar(2000),
                       text TEXT,
                       email_to varchar(3000),
                       primary key (id)
) engine=InnoDB;
    

create table employee (
                          id bigint not null auto_increment,
                          created_at datetime(6),
                          created_by bigint,
                          is_deleted bit,
                          modified_at datetime(6),
                          modified_by bigint,
                          achievements varchar(255),
                          actual_salary_of_join integer,
                          additional_increment_number_of_join integer,
                          appointment_date date,
                          appointment_order varchar(255),
                          attendance_and_leave_info varchar(255),
                          blood_group integer,
                          comments varchar(255),
                          current_actual_salary integer,
                          current_additional_increment_number integer,
                          current_designation varchar(255),
                          current_grade integer,
                          current_initial_grade_salary integer,
                          current_step varchar(255),
                          date_of_birth date,
                          email varchar(255),
                          employee_doc_group_id bigint,
                          employee_id varchar(255),
                          father_name varchar(255),
                          gender integer,
                          initial_grade_salary_of_join integer,
                          job_type integer,
                          joining_date date,
                          joining_designation varchar(255),
                          joining_grade integer,
                          marital_status integer,
                          mobile_number varchar(255),
                          mother_name varchar(255),
                          name varchar(255),
                          name_en varchar(255),
                          nid varchar(255),
                          number_of_children integer,
                          office_ministry varchar(255),
                          passport_number varchar(255),
                          password varchar(255),
                          password_again varchar(255),
                          permanent_address varchar(255),
                          permanent_date date,
                          personal_id varchar(255),
                          present_address varchar(255),
                          promotion_info varchar(255),
                          quota integer,
                          reporting_manager varchar(255),
                          retirement_date date,
                          step_of_join varchar(255),
                          transfer_info varchar(255),
                          primary key (id)
) engine=InnoDB;
    

create table employee_complains_map (
                                        employee_id bigint not null,
                                        complains_id bigint not null,
                                        primary key (employee_id, complains_id)
) engine=InnoDB;
    

create table employee_educational_info (
                                           id bigint not null auto_increment,
                                           degree_name varchar(255),
                                           education_doc_group_id bigint,
                                           education_level integer,
                                           educational_institute varchar(255),
                                           passing_year integer,
                                           result varchar(255),
                                           subject varchar(255),
                                           primary key (id)
) engine=InnoDB;
    

create table employee_employee_educational_info_map (
                                                        employee_id bigint not null,
                                                        employee_educational_info_id bigint not null,
                                                        primary key (employee_id, employee_educational_info_id)
) engine=InnoDB;
    

create table employee_employee_training_info_map (
                                                     employee_id bigint not null,
                                                     employee_training_info_id bigint not null,
                                                     primary key (employee_id, employee_training_info_id)
) engine=InnoDB;
    

create table employee_training_info (
                                        id bigint not null auto_increment,
                                        organization_name varchar(255),
                                        training_date date,
                                        training_doc_group_id bigint,
                                        training_name varchar(255),
                                        training_subject varchar(255),
                                        primary key (id)
) engine=InnoDB;
    

create table incident (
                          id bigint not null auto_increment,
                          version bigint,
                          created_at datetime(6),
                          created_by bigint,
                          is_deleted bit,
                          modified_at datetime(6),
                          modified_by bigint,
                          document_group_id bigint,
                          driver_name varchar(255),
                          incident_date date,
                          incident_details varchar(255),
                          incident_location varchar(255),
                          incident_time time,
                          incident_type integer,
                          user_name varchar(255),
                          primary key (id)
) engine=InnoDB;
    

create table marine (
                        id bigint not null auto_increment,
                        created_at datetime(6),
                        created_by bigint,
                        is_deleted bit,
                        modified_at datetime(6),
                        modified_by bigint,
                        allocation_date date,
                        brand varchar(255),
                        color varchar(255),
                        engine_number varchar(255),
                        engine_quantity integer,
                        fuel_type integer,
                        horsepower double precision,
                        marine_owner varchar(255),
                        marine_type integer,
                        model_year integer,
                        sales_org_name varchar(255),
                        seat_capacity integer,
                        primary key (id)
) engine=InnoDB;
    

create table marine_marine_service_record_map (
                                                  marine_id bigint not null,
                                                  marine_service_record_id bigint not null,
                                                  primary key (marine_id, marine_service_record_id)
) engine=InnoDB;
    

create table marine_service_record (
                                       id bigint not null auto_increment,
                                       servicing_cost double precision,
                                       servicing_date date,
                                       servicing_vendor_name varchar(255),
                                       primary key (id)
) engine=InnoDB;
    

create table mechanic (
                          id bigint not null auto_increment,
                          created_at datetime(6),
                          created_by bigint,
                          is_deleted bit,
                          modified_at datetime(6),
                          modified_by bigint,
                          address varchar(255),
                          average_ratings double precision,
                          count_of_people_rated double precision,
                          date_of_birth date,
                          document_group_id bigint,
                          enrolment_date date,
                          experience varchar(255),
                          expertise varchar(255),
                          father_name varchar(255),
                          mechanic_id bigint,
                          mechanic_status integer,
                          mother_name varchar(255),
                          name varchar(255),
                          nid_number varchar(255),
                          phone_number varchar(255),
                          qualification varchar(255),
                          total_ratings double precision,
                          primary key (id)
) engine=InnoDB;
    

create table noc_application (
                                 id bigint not null auto_increment,
                                 version bigint,
                                 created_at datetime(6),
                                 created_by bigint,
                                 is_deleted bit,
                                 modified_at datetime(6),
                                 modified_by bigint,
                                 collected_fine double precision,
                                 date date,
                                 designation varchar(255),
                                 document_group_id bigint,
                                 email varchar(255),
                                 fined_amount double precision,
                                 identity_number varchar(255),
                                 mechanic_incharge_id bigint,
                                 mobile_number varchar(255),
                                 name varchar(255),
                                 noc_application_type integer,
                                 vehicle_allotment integer,
                                 working_place varchar(255),
                                 state_id bigint,
                                 primary key (id)
) engine=InnoDB;
    

create table product (
                         id bigint not null auto_increment,
                         comments varchar(255),
                         product_identity varchar(255),
                         product_name varchar(255),
                         quantity integer,
                         rate_per_unit integer,
                         tender_number varchar(255),
                         total_rate integer,
                         primary key (id)
) engine=InnoDB;
    

create table project_vehicle (
                                 id bigint not null auto_increment,
                                 version bigint,
                                 created_at datetime(6),
                                 created_by bigint,
                                 is_deleted bit,
                                 modified_at datetime(6),
                                 modified_by bigint,
                                 comments varchar(255),
                                 project_organization_name varchar(255),
                                 received_date date,
                                 received_documents_name varchar(255),
                                 vehicle_id bigint,
                                 primary key (id)
) engine=InnoDB;
    

create table public_user_info (
                                  id bigint not null auto_increment,
                                  address varchar(255),
                                  designation varchar(255) not null,
                                  email varchar(255),
                                  employee_type integer,
                                  name varchar(255) not null,
                                  name_en varchar(255),
                                  phone_no varchar(255),
                                  service_id varchar(255) not null,
                                  workplace varchar(255),
                                  primary key (id)
) engine=InnoDB;
    

create table puchase_demand (
                                id bigint not null auto_increment,
                                created_at datetime(6),
                                created_by bigint,
                                is_deleted bit,
                                modified_at datetime(6),
                                modified_by bigint,
                                date date,
                                product_type integer,
                                total_price integer,
                                primary key (id)
) engine=InnoDB;
    

create table purchase_demand_product_map (
                                             purchase_demand_id bigint not null,
                                             product_id bigint not null,
                                             primary key (purchase_demand_id, product_id)
) engine=InnoDB;
    

create table role (
                      id bigint not null,
                      name varchar(255),
                      role_group_bn varchar(255),
                      role_group_en varchar(255),
                      role_text_bn varchar(255),
                      role_text_en varchar(255),
                      primary key (id)
) engine=InnoDB;
    

create table state (
                       id bigint not null auto_increment,
                       description TEXT,
                       name varchar(255) not null,
                       name_en varchar(255) not null,
                       state_type integer,
                       primary key (id)
) engine=InnoDB;
    

create table state_action_map (
                                  id bigint not null auto_increment,
                                  role_id bigint not null,
                                  view_only bit,
                                  action_id bigint not null,
                                  next_state_id bigint not null,
                                  state_id bigint not null,
                                  primary key (id)
) engine=InnoDB;
    

create table user (
                      id bigint not null auto_increment,
                      created_at datetime(6),
                      created_by bigint,
                      is_deleted bit,
                      modified_at datetime(6),
                      modified_by bigint,
                      is_enabled bit,
                      password varchar(255) not null,
                      user_type int default 0 not null,
                      username varchar(100) not null,
                      employee_id bigint,
                      public_user_info_id bigint,
                      user_level_id bigint,
                      primary key (id)
) engine=InnoDB;
    

create table user_level (
                            id bigint not null auto_increment,
                            level bigint,
                            name_bn varchar(255),
                            name_en varchar(255),
                            primary key (id)
) engine=InnoDB;
    

create table user_level_role (
                                 user_level_id bigint not null,
                                 role_id bigint not null,
                                 primary key (user_level_id, role_id)
) engine=InnoDB;
    

create table user_token (
                            user_id bigint not null,
                            created_at datetime(6),
                            expired_at datetime(6),
                            token varchar(255),
                            primary key (user_id)
) engine=InnoDB;
    

create table vehicle (
                         id bigint not null auto_increment,
                         created_at datetime(6),
                         created_by bigint,
                         is_deleted bit,
                         modified_at datetime(6),
                         modified_by bigint,
                         brand varchar(255),
                         chassis_number varchar(255),
                         color varchar(255),
                         document_group_id bigint,
                         engine_cc varchar(255),
                         engine_number varchar(255),
                         fitness_date date,
                         fuel_type integer,
                         model_year integer,
                         reg_date date,
                         reg_number varchar(255),
                         reg_type integer,
                         sales_org_name varchar(255),
                         seat_capacity integer,
                         tax_token_date date,
                         vehicle_owner varchar(255),
                         vehicle_type integer,
                         driver_id bigint,
                         user_id bigint,
                         primary key (id)
) engine=InnoDB;
    

create table vehicle_replacement_application (
                                                 id bigint not null auto_increment,
                                                 version bigint,
                                                 created_at datetime(6),
                                                 created_by bigint,
                                                 is_deleted bit,
                                                 modified_at datetime(6),
                                                 modified_by bigint,
                                                 application_date date,
                                                 designation varchar(255),
                                                 identity_number bigint,
                                                 mobile_number varchar(255),
                                                 name varchar(255),
                                                 registration_no varchar(255),
                                                 replacement_reason varchar(255),
                                                 status integer,
                                                 vehicle_id bigint,
                                                 vehicle_type integer,
                                                 working_place varchar(255),
                                                 state_id bigint,
                                                 primary key (id)
) engine=InnoDB;
    

create table vehicle_requistion_application (
                                                id bigint not null auto_increment,
                                                created_at datetime(6),
                                                created_by bigint,
                                                is_deleted bit,
                                                modified_at datetime(6),
                                                modified_by bigint,
                                                designation varchar(255),
                                                email varchar(255),
                                                name varchar(255),
                                                phone_no varchar(255),
                                                service_id varchar(255),
                                                workplace varchar(255),
                                                state_id bigint,
                                                primary key (id)
) engine=InnoDB;
    

create table vendor (
                        id bigint not null auto_increment,
                        version bigint,
                        created_at datetime(6),
                        created_by bigint,
                        is_deleted bit,
                        modified_at datetime(6),
                        modified_by bigint,
                        address varchar(255),
                        comments varchar(255),
                        contact_number varchar(255),
                        contract_end_date date,
                        contract_start_date date,
                        document_group_id bigint,
                        email varchar(255),
                        fax_number varchar(255),
                        item_supply_type integer,
                        total_experience integer,
                        vendor_identity varchar(255),
                        vendor_name varchar(255),
                        primary key (id)
) engine=InnoDB;
     create index idx_aduit_log_user_id on audit_log (user_id);
     create index idx_aduit_log_log_event on audit_log (log_event);
    

alter table driver drop index if exists uk_driver_driver_id;


alter table driver add constraint uk_driver_driver_id unique (driver_id);
    

alter table driver_driver_complaint_record_map drop index if exists UK_n19lp2hysk424sni1ka70q8kk;


alter table driver_driver_complaint_record_map add constraint UK_n19lp2hysk424sni1ka70q8kk unique (driver_complaint_record_id);
    

alter table driver_driver_fitness_record_map drop index if exists UK_qaoaxrr46e84c2m51rbft31oc;


alter table driver_driver_fitness_record_map add constraint UK_qaoaxrr46e84c2m51rbft31oc unique (driver_fitness_record_id);
    

alter table driver_driver_transfer_record_map drop index if exists UK_iec2dyq0veq351aw6xoinuhmf;


alter table driver_driver_transfer_record_map add constraint UK_iec2dyq0veq351aw6xoinuhmf unique (driver_transfer_record_id);
    

alter table employee_complains_map drop index if exists UK_i7hdp1mn6mmana7jburhapccy;


alter table employee_complains_map add constraint UK_i7hdp1mn6mmana7jburhapccy unique (complains_id);
    

alter table employee_employee_educational_info_map drop index if exists UK_tmcdkhnobehtscw487upwtpf1;


alter table employee_employee_educational_info_map
    add constraint UK_tmcdkhnobehtscw487upwtpf1 unique (employee_educational_info_id);
    

alter table employee_employee_training_info_map
    drop index if exists UK_3vdcdyvkr3bck9kjve8kn5ivm;


alter table employee_employee_training_info_map
    add constraint UK_3vdcdyvkr3bck9kjve8kn5ivm unique (employee_training_info_id);
    

alter table marine
    drop index if exists uk_marine_engine_number;


alter table marine
    add constraint uk_marine_engine_number unique (engine_number);
    

alter table marine_marine_service_record_map
    drop index if exists UK_ikmpmfnssrgg96o07r78tvpa;


alter table marine_marine_service_record_map
    add constraint UK_ikmpmfnssrgg96o07r78tvpa unique (marine_service_record_id);
    

alter table mechanic
    drop index if exists uk_mechanic_mechanic_id;


alter table mechanic
    add constraint uk_mechanic_mechanic_id unique (mechanic_id);
    

alter table public_user_info
    drop index if exists uk_public_user_info_service_id;


alter table public_user_info
    add constraint uk_public_user_info_service_id unique (service_id);
    

alter table purchase_demand_product_map
    drop index if exists UK_o7i97dhht9g5a5v82gd184tav;

alter table purchase_demand_product_map
    add constraint UK_o7i97dhht9g5a5v82gd184tav unique (product_id);
     create index idx_state_action_map_state_role_id on state_action_map (state_id, role_id);
     create index idx_state_action_map_state_action_id on state_action_map (state_id, action_id);
    

alter table state_action_map
    drop index if exists uk_state_action_map_state_action_id;


alter table state_action_map
    add constraint uk_state_action_map_state_action_id unique (state_id, action_id);
    

alter table vehicle
    drop index if exists uk_vehicle_engline_no_chassis_no;


alter table vehicle
    add constraint uk_vehicle_engline_no_chassis_no unique (engine_number, chassis_number);
    

alter table vehicle
    drop index if exists uk_vehicle_reg_number;


alter table vehicle
    add constraint uk_vehicle_reg_number unique (reg_number);
    

alter table vehicle
    drop index if exists uk_vehicle_driver;


alter table vehicle
    add constraint uk_vehicle_driver unique (driver_id);
    

alter table driver_driver_complaint_record_map
    add constraint fk_driver_id_driver_complaint_record_map_complaint_record_id
        foreign key (driver_complaint_record_id)
            references driver_complaint_record (id);
    

alter table driver_driver_complaint_record_map
    add constraint fk_driver_complaint_record_driver_id
        foreign key (driver_id)
            references driver (id);
    

alter table driver_driver_fitness_record_map
    add constraint fk_driver_id_driver_fitness_record_map_fitness_record_id
        foreign key (driver_fitness_record_id)
            references driver_fitness_record (id);
    

alter table driver_driver_fitness_record_map
    add constraint fk_driver_fitness_record_driver_id
        foreign key (driver_id)
            references driver (id);
    

alter table driver_driver_transfer_record_map
    add constraint fk_driver_id_driver_transfer_record_map_transfer_record_id
        foreign key (driver_transfer_record_id)
            references driver_transfer_record (id);
    

alter table driver_driver_transfer_record_map
    add constraint fk_driver_transfer_record_driver_id
        foreign key (driver_id)
            references driver (id);
    

alter table employee_complains_map
    add constraint fk_employee_id_complains_map_id
        foreign key (complains_id)
            references complains (id);
    

alter table employee_complains_map
    add constraint fk_complains_employee_id
        foreign key (employee_id)
            references employee (id);
    

alter table employee_employee_educational_info_map
    add constraint fk_employee_id_employee_educational_info_map_id
        foreign key (employee_educational_info_id)
            references employee_educational_info (id);
    

alter table employee_employee_educational_info_map
    add constraint fk_employee_educational_info_employee_id
        foreign key (employee_id)
            references employee (id);
    

alter table employee_employee_training_info_map
    add constraint fk_employee_id_employee_training_info_map_id
        foreign key (employee_training_info_id)
            references employee_training_info (id);
    

alter table employee_employee_training_info_map
    add constraint fk_employee_training_info_employee_id
        foreign key (employee_id)
            references employee (id);
    

alter table marine_marine_service_record_map
    add constraint fk_marine_id_marine_service_record_map_service_record_id
        foreign key (marine_service_record_id)
            references marine_service_record (id);
    

alter table marine_marine_service_record_map
    add constraint fk_marine_service_record_marine_id
        foreign key (marine_id)
            references marine (id);
    

alter table noc_application
    add constraint fk_noc_app_state_id
        foreign key (state_id)
            references state (id);
    

alter table project_vehicle
    add constraint fk_project_vehicle_vehicle_id
        foreign key (vehicle_id)
            references vehicle (id);
    

alter table purchase_demand_product_map
    add constraint fk_purchase_demand_id_purchase_demand_product_map_id
        foreign key (product_id)
            references product (id);
    

alter table purchase_demand_product_map
    add constraint fk_product_purchase_demand_id
        foreign key (purchase_demand_id)
            references puchase_demand (id);
    

alter table state_action_map
    add constraint fk_state_action_map_action_id
        foreign key (action_id)
            references action (id);
    

alter table state_action_map
    add constraint fk_state_action_map_state_id_next
        foreign key (next_state_id)
            references state (id);
    

alter table state_action_map
    add constraint fk_state_action_map_state_id
        foreign key (state_id)
            references state (id);
    

alter table user
    add constraint fk_user_employee_id
        foreign key (employee_id)
            references employee (id);
    

alter table user
    add constraint fk_user_public_user_info_id
        foreign key (public_user_info_id)
            references public_user_info (id);
    

alter table user
    add constraint fk_user_user_level_id
        foreign key (user_level_id)
            references user_level (id);
    

alter table user_level_role
    add constraint fk_role_id_user_level_role
        foreign key (role_id)
            references role (id);
    

alter table user_level_role
    add constraint fk_user_level_id_user_level_role
        foreign key (user_level_id)
            references user_level (id);
    

alter table vehicle
    add constraint fk_vehicle_driver_id
        foreign key (driver_id)
            references driver (id);

alter table vehicle
    add constraint fk_vehicle_user_id
        foreign key (user_id)
            references user (id);

alter table vehicle_replacement_application
    add constraint fk_replace_app_state_id
        foreign key (state_id)
            references state (id);

alter table vehicle_requistion_application
    add constraint fk_vehicle_requisition_app_state_id
        foreign key (state_id)
            references state (id);