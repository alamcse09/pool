function callAjax( url, param, method, successCallback, errorCallback ){

    $.ajax({

        url: url,
        data: param,
        method: method,
        beforeSend: function(request) {

        },
        success: function( data ){

            successCallback( data );
        },
        error: function( data ){

            errorCallback( data );
        }
    });
}

function callAjaxWithDefaultHandler( url, param, method ){

    callAjax( url, param, method, commonSuccessCallback, commonErrorCallback );
}

function commonSuccessCallback( data ){

    if( data['success'] ) {

        toastr.success( data[ 'message' ] );
    }
    else{

        toastr.error( data[ 'message' ] );
    }
}

function commonErrorCallback( data ){

    toastr.error( data[ 'message' ] );
}

function commonErrorCallbackSwal( data ){

    let message = "";
    let responseJson = JSON.parse( data.responseText );
    if( typeof responseJson.error != 'undefined' ){
        message = responseJson.error;
    }
    if( typeof responseJson.message != 'undefined' ){
        message = responseJson.message;
    }

    Swal.fire(
        "Deleted!",
        message,
        "error"
    )
}