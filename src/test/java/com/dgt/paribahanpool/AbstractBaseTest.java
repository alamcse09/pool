package com.dgt.paribahanpool;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest( properties = { "spring.config.activate.on-profile=alam" } )
public class AbstractBaseTest {

    @Autowired
    public MockMvc mvc;

    protected void testWithLogin( String url ) throws Exception {

        mvc.perform( get( "/vehicle/add" ) )
                .andExpect( status().isOk() );
    }

    protected void testWithoutLogin( String url ) throws Exception {

        mvc.perform( get( "/vehicle/add" ) )
                .andExpect( status().is3xxRedirection() )
                .andExpect( header().exists( "location" ) );
    }
}
