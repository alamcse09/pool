package com.dgt.paribahanpool.vehicle.controller;

import com.dgt.paribahanpool.AbstractBaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.security.test.context.support.WithUserDetails;

public class VehicleControllerTest extends AbstractBaseTest {

    @Test
    @WithUserDetails( "alam.cse09@gmail.com" )
    public void testGetAddPage() throws Exception {

        testWithLogin( "/vehicle/add" );
    }

    @Test
    public void testGetAddPageWithoutLogin() throws Exception {

        testWithoutLogin( "/vehicle/add" );
    }
}
